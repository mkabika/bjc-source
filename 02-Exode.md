# Shemot (Exode) (Ex.)

Signification : Noms

Auteur : Probablement Moshé (Moïse)

Thème : La délivrance

Date de rédaction : Env. 1450 – 1410 av. J.-C.

Les fils de Yaacov (Jacob) s'étaient retrouvés en Égypte pour survivre à une famine qui avait frappé la terre entière pendant plusieurs années. Grâce à leur frère Yossef, alors gouverneur d'Égypte, ils bénéficièrent d'un bon traitement. Mais la mort de ce dernier et la montée au pouvoir d'un nouveau pharaon (probablement Ramsès II) inaugurèrent une période de quatre siècles de souffrances pour le peuple élu.

En effet, les Hébreux avaient été réduits en esclavage. En réponse aux cris de douleur de son peuple, Elohîm suscita Moshé (Moïse), dont le nom signifie « tiré de ». Ce descendant de Lévi fut élevé dans le palais de pharaon, mais dut s'enfuir parce qu'il avait tué un Égyptien. Après 40 ans passés sur la terre de Madian, l'Elohîm qui s'appelle « JE SUIS » se révéla à Moshé sur la Montagne d'Horeb et lui confia la mission d'aller délivrer son peuple du joug égyptien.

Ce livre retrace la sortie d'Égypte, et le début de la traversée du désert, jalonnée de prodiges exceptionnels.

## Chapitre 1

### Après la mort de Yossef

1:1	Voici les noms des fils d'Israël qui entrèrent en Égypte avec Yaacov. Ils y entrèrent chaque homme avec sa famille :
1:2	Reouben, Shim’ôn, Lévi et Yéhouda,
1:3	Yissakar, Zebouloun et Benyamin,
1:4	Dan et Nephthali, Gad et Asher.
1:5	Toutes les âmes sorties des reins de Yaacov étaient 70 âmes. Yossef était alors en Égypte.
1:6	Yossef mourut ainsi que tous ses frères et toute cette génération-là.
1:7	Les fils d'Israël portèrent du fruit et grouillèrent. Ils se multiplièrent et devinrent extrêmement, extrêmement puissants, au point de remplir<!--De. 26:5 ; Ac. 7:17.--> la terre.

### Les fils d'Israël esclaves en Égypte

1:8	Il s’éleva sur l’Égypte un nouveau roi, qui n'avait pas connu Yossef.
1:9	Il dit à son peuple : Voici, le peuple des fils d'Israël est plus grand et plus puissant que nous.
1:10	Agissons sagement avec lui, de peur qu'il ne se multiplie, et que, s'il survenait une guerre, il ne se joigne à nos ennemis, ne fasse la guerre contre nous et qu'il ne monte hors de la terre.
1:11	Ils établirent sur le peuple des chefs de travaux forcés afin de l'affliger par des travaux forcés. C'est ainsi qu'il bâtit les villes de Pithom et de Ramsès, pour servir de magasins à pharaon.
1:12	Mais plus ils l'affligeaient, plus il se multipliait et croissait en toute abondance, c'est pourquoi ils avaient une aversion en face des fils d'Israël<!--Ps. 105:24.-->.
1:13	Les Égyptiens firent servir les fils d’Israël avec cruauté<!--Ge. 15:13.-->.
1:14	Ils leur rendirent la vie amère par un dur service, en argile et en briques, et par toute sorte de service aux champs : tout le service dans lequel on les faisait servir était avec cruauté.
1:15	Le roi d'Égypte parla aussi aux sages-femmes des Hébreux. Le nom de l'une était Shiphrah et le nom de la seconde Pouah.
1:16	Il leur dit : Quand vous accoucherez les femmes des Hébreux, et que vous les verrez sur les sièges, si c'est un fils, mettez-le à mort. Mais si c'est une fille, qu'elle vive.
1:17	Mais les sages-femmes craignirent Elohîm et ne firent pas ce que le roi d'Égypte leur avait dit, car elles laissèrent vivre les fils.
1:18	Le roi d'Égypte appela les sages-femmes et leur dit : Pourquoi avez-vous fait cela et avez-vous laissé vivre les fils ?
1:19	Les sages-femmes dirent à pharaon : Parce que les femmes des Hébreux ne sont pas comme les Égyptiennes, car elles sont vigoureuses. Elles accouchent avant que la sage-femme n'arrive auprès d'elles.
1:20	Elohîm fit du bien aux sages-femmes et le peuple multiplia et devint très puissant.
1:21	Et il arriva, parce que les sages-femmes craignirent Elohîm, qu'il leur fit des maisons.
1:22	Pharaon donna cet ordre à tout son peuple, disant : Jetez dans le fleuve tous les fils qui naîtront, mais laissez vivre toutes les filles.

## Chapitre 2

### Naissance de Moshé (Moïse)<!--Hé. 11:23-27.-->

2:1	Un homme de la maison de Lévi s'en alla et prit une fille de Lévi<!--No. 26:59.-->.
2:2	Cette femme devint enceinte et enfanta un fils. Voyant qu'il était beau, elle le cacha pendant trois mois<!--Hé. 11:23.-->.
2:3	Mais ne pouvant le cacher de nouveau, elle prit une arche en papyrus, qu'elle enduisit de bitume et de poix. Ensuite elle y mit l'enfant et le déposa parmi les roseaux sur le bord du fleuve.
2:4	Et la sœur de cet enfant se tenait loin pour savoir ce qui lui arriverait.
2:5	La fille de pharaon descendit à la rivière pour se baigner, et ses servantes se promenaient sur le bord de la rivière, et ayant vu l'arche au milieu des roseaux, elle envoya une de ses servantes pour la prendre.
2:6	Elle l'ouvrit et vit l'enfant, et voici, c'était un petit garçon qui pleurait. Elle en fut touchée de compassion et dit : C'est un enfant des Hébreux !
2:7	Sa sœur dit à la fille de pharaon : Veux-tu que j'aille te chercher une nourrice parmi les femmes des Hébreux afin qu'elle allaite cet enfant pour toi ?
2:8	La fille de pharaon lui dit : Va ! Et la vierge s'en alla et appela la mère de l'enfant.
2:9	La fille de pharaon lui dit : Emmène cet enfant et allaite-le pour moi, je te donnerai ton salaire. Et la femme prit l'enfant et l'allaita.
2:10	Quand l'enfant fut devenu grand, elle l'amena à la fille de pharaon et il fut pour elle comme un fils. Elle l'appela du nom de Moshé parce que, dit-elle : Je l'ai tiré des eaux.

### Moshé prend à cœur le sort d'Israël ; fuite à Madian

2:11	Or il arriva, en ce temps-là, que Moshé, étant devenu grand, sortit vers ses frères et vit leurs travaux forcés. Il vit un homme égyptien qui frappait un homme hébreu, un de ses frères<!--Hé. 11:24-25.-->.
2:12	Il se tourna de côté et d’autre et vit qu’il n’y avait personne, il tua l'Égyptien et le cacha dans le sable.
2:13	Il sortit le second jour et vit deux hommes hébreux qui se querellaient. Il dit au coupable : Pourquoi frappes-tu ton prochain ?
2:14	Celui-ci dit : Qui t'a établi homme, prince et juge sur nous ? Est-ce pour me tuer que tu le dis, comme tu as tué l'Égyptien ? Et Moshé eut peur et se dit : Sûrement l'affaire est connue.
2:15	Or pharaon entendit parler de cette affaire et chercha à tuer Moshé. Mais Moshé s'enfuit en face de pharaon, s'arrêta en terre de Madian et s'assit près d'un puits.
2:16	Or le prêtre de Madian avait sept filles qui vinrent puiser de l'eau, et elles remplirent les auges pour abreuver le troupeau de leur père.
2:17	Des bergers vinrent et les chassèrent. Moshé se leva, les sauva et fit boire leur troupeau.
2:18	Quand elles furent revenues chez Reouel<!--Reouel, dont le nom signifie « ami de El », était également appelé Yithro (Jéthro), le père de Tsipporah (Séphora, la femme de Moshé).-->, leur père, il leur dit : Comment se fait-il que vous soyez revenues si tôt aujourd'hui ?
2:19	Elles dirent : Un homme, un égyptien nous a délivrées de la main des bergers. Il a même puisé, puisé de l'eau pour nous et a fait boire le troupeau.
2:20	Il dit à ses filles : Où est-il ? Pourquoi avez-vous laissé là cet homme ? Appelez-le pour qu'il mange du pain !
2:21	Et Moshé consentit à demeurer auprès de cet homme, qui donna sa fille Tsipporah<!--Séphora.--> à Moshé.
2:22	Elle enfanta un fils et il l'appela du nom de Guershom car, dit-il : Je séjourne sur une terre étrangère.

### YHWH entend les cris de son peuple

2:23	Or il arriva en ces jours, qui furent nombreux, que le roi d'Égypte mourut. Les fils d'Israël soupirèrent à cause de la servitude et ils crièrent. Leur cri monta jusqu'à Elohîm, à cause de la servitude<!--No. 20:15-16.-->.
2:24	Elohîm entendit leurs gémissements, et Elohîm se souvint de l'alliance qu'il avait traitée avec Abraham, Yitzhak et Yaacov.
2:25	Elohîm regarda les fils d'Israël et Elohîm sut...

## Chapitre 3

### YHWH se révèle à Moshé dans le buisson ardent

3:1	Moshé devint berger du troupeau de Yithro<!--Jéthro.-->, son beau-père, prêtre de Madian. Il conduisit le troupeau derrière le désert et vint à la montagne d'Elohîm à Horeb.
3:2	L'Ange de YHWH se fit voir à lui dans une flamme de feu, du milieu d'un buisson. Il regarda, et voici, le buisson était tout en feu, et le buisson ne se consumait pas.
3:3	Moshé dit : Je me détournerai maintenant et je regarderai cette grande vision, pourquoi le buisson ne se consume pas.
3:4	YHWH vit que Moshé s'était détourné pour regarder. Elohîm l'appela du milieu du buisson, en disant : Moshé ! Moshé ! Il dit : Me voici !
3:5	Elohîm dit : N'approche pas d'ici, ôte tes sandales de tes pieds, car le lieu où tu te tiens est un sol saint.
3:6	Il dit : C'est moi l'Elohîm de ton père, l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov<!--Mt. 22:32 ; Mc. 12:26 ; Lu. 20:37 ; Ac. 7:32.-->. Moshé cacha ses faces, parce qu'il craignait de regarder vers Elohîm.
3:7	YHWH dit : J'ai vu, j'ai vu l'affliction de mon peuple qui est en Égypte et j'ai entendu le cri de détresse qu'ils ont poussé, face à leurs oppresseurs, car je connais leurs douleurs.
3:8	Je suis descendu pour le délivrer de la main des Égyptiens et pour le faire monter de cette terre vers une terre bonne et vaste, une terre où coulent le lait et le miel ; dans le lieu où sont les Kena'ânéens<!--Cananéens.-->, les Héthiens, les Amoréens, les Phéréziens, les Héviens et les Yebousiens.
3:9	Maintenant, voici le cri de détresse des fils d'Israël est venu jusqu'à moi, et j'ai vu aussi l'oppression dont les Égyptiens les oppriment.
3:10	Maintenant viens, et je t'enverrai vers pharaon et tu feras sortir mon peuple, les fils d'Israël, hors d'Égypte<!--Os. 12:14 ; Mi. 6:4.-->.
3:11	Moshé dit à Elohîm : Qui suis-je, moi, oui, pour aller vers pharaon, oui, pour faire sortir les fils d’Israël d’Égypte ?
3:12	Il dit : Oui, je serai avec toi. Et ceci sera pour toi le signe que c’est moi qui t’envoie : quand tu auras fait sortir mon peuple d'Égypte, vous servirez Elohîm sur cette montagne.

### YHWH révèle son Nom à Moshé

3:13	Moshé dit à Elohîm : Voici, j'irai vers les fils d'Israël et je leur dirai : L'Elohîm de vos pères m'a envoyé vers vous. Ils me diront : Quel est son nom ? Que leur dirai-je ?
3:14	Elohîm dit à Moshé : JE SUIS QUI JE SUIS<!--Ehyeh asher ehyeh ou Hayah asher hayah. Je suis qui je serai. Là où le français distingue le présent, le passé et le futur, l’hébreu biblique a une opposition de type : accompli et inaccompli ou « parfait » et « imparfait », c'est-à-dire : fini (ponctuel) et « en train de se faire » (duratif).-->. Il dit aussi : Tu parleras ainsi aux fils d'Israël : JE SUIS<!--Je suis (« ehyeh ou hayah » en hébreu), c'est de là que vient le Nom de YHWH. Or le Nom de Yéhoshoua signifie « YHWH est salut ». Elohîm révèle son Nom à Moshé : « JE SUIS QUI JE SERAI ». Or Yéhoshoua ha Mashiah s'est ouvertement attribué ce Nom en Jn. 8:58. N'ayant compris ni le plan d'Elohîm ni qui était celui qui les visitait, les religieux juifs ont voulu le lapider, car ils estimaient qu'il blasphémait. Car en déclarant être « JE SUIS », Yéhoshoua ha Mashiah proclamait ouvertement sa divinité (Ro. 9:5), chose que les Juifs ne pouvaient concevoir. Dans l'évangile de Yohanan (Jean), Yéhoshoua déclare clairement qu'il est le « JE SUIS » en Ex. 3:14. « JE SUIS le Pain de vie » (Jn. 6:35), « JE SUIS la Lumière du monde » (Jn. 8:12), « JE SUIS le Bon Berger » (Jn. 10:11), « JE SUIS la Porte » (Jn. 10:7), « JE SUIS la Résurrection » (Jn. 11:25), « JE SUIS le Chemin, la Vérité et la Vie » (Jn. 14:6), « JE SUIS le Véritable cep » (Jn. 15:1).--> m'a envoyé vers vous.
3:15	Elohîm dit encore à Moshé : Tu parleras ainsi aux fils d'Israël : YHWH, l'Elohîm de vos pères, l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov m'a envoyé vers vous. C'est là mon Nom<!--Am. 5:8.--> pour l'éternité, c'est là mon souvenir d'âges en âges<!--Yéhoshoua est le Roi des âges 1 Ti. 1:17.-->.
3:16	Va et rassemble les anciens d'Israël, et dis-leur : YHWH, l'Elohîm de vos pères, l'Elohîm d'Abraham, de Yitzhak et de Yaacov, m'est apparu en disant : Je vous ai visités<!--Ge. 50:24.-->, je vous ai visités, à cause de ce qu'on vous fait en Égypte.
3:17	Et j'ai dit : Je vous ferai monter hors de l'affliction de l'Égypte vers la terre des Kena'ânéens, des Héthiens, des Amoréens, des Phéréziens, des Héviens et des Yebousiens, qui est une terre où coulent le lait et le miel.
3:18	Ils écouteront ta voix. Tu iras toi et les anciens d'Israël vers le roi d'Égypte et vous lui direz : YHWH, l'Elohîm des Hébreux, est venu nous rencontrer. Maintenant, s’il te plaît, laisse-nous aller à trois jours de route dans le désert pour sacrifier à YHWH, notre Elohîm.
3:19	Pour moi, je sais que le roi d'Égypte ne vous permettra pas de vous en aller, sinon à main-forte.
3:20	J'étendrai ma main et je frapperai l'Égypte par toutes les merveilles que je ferai au milieu d'elle. Après cela, il vous laissera aller.
3:21	Je donnerai à ce peuple de la grâce aux yeux des Égyptiens, et il arrivera que, quand vous irez, vous n’irez pas à vide.
3:22	Chaque femme demandera à sa voisine et à celle qui séjourne dans sa maison, des objets d'argent, des objets d'or, et des vêtements, que vous mettrez sur vos fils et sur vos filles, et vous dépouillerez les Égyptiens.

## Chapitre 4

### Moshé résiste en évoquant l'incrédulité du peuple

4:1	Moshé répondit et dit : Voici, ils ne me croiront pas et n'écouteront pas ma voix. Car ils diront : YHWH ne t'est pas apparu.
4:2	YHWH lui dit : Qu'y a-t-il dans ta main ? Il dit : Un bâton.
4:3	Il lui dit : Jette-le par terre. Il le jeta par terre et il devint un serpent. Et Moshé fuyait devant lui.
4:4	YHWH dit à Moshé : Étends ta main et saisis sa queue. Il étendit sa main et le pressa, et il devint un bâton dans sa paume.
4:5	Afin qu'ils croient que YHWH, l'Elohîm de leurs pères, l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov, t'est apparu.
4:6	YHWH lui dit encore : Mets ta main, s’il te plaît, dans ton sein. Et il mit sa main dans son sein, puis il la retira, et voici, sa main était lépreuse, comme de la neige.
4:7	Il lui dit : Remets ta main dans ton sein. Il remit sa main dans son sein et la retira de son sein, et voici, elle était redevenue comme sa chair.
4:8	S'il arrivait qu'ils ne te croient pas et n’écoutent pas la voix du premier signe, ils croiront la voix du dernier signe.
4:9	S'il arrivait qu'ils ne croient pas même à ces deux signes et n’écoutent pas ta voix, tu prendras de l'eau du fleuve et tu la répandras sur la terre, et l’eau que tu auras prise du fleuve deviendra, elle deviendra du sang sur la terre.

### Moshé résiste en évoquant son incapacité à parler

4:10	Moshé dit à YHWH : Excuse-moi, Adonaï ! Je ne suis pas un homme à paroles, ni d'hier, ni d'avant-hier, ni depuis que tu parles à ton serviteur, car j'ai la bouche lourde et la langue lourde.
4:11	YHWH lui dit : Qui a mis une bouche à l'être humain ? Ou qui rend muet ou sourd, voyant ou aveugle ? N'est-ce pas moi YHWH<!--Ps. 94:9.--> ?
4:12	Maintenant, va ! Moi-même je serai avec ta bouche, je t’enseignerai ce dont tu parleras<!--Lu. 12:12 ; Mt. 10:19 ; Mc. 13:11.-->.
4:13	Il dit : Excuse-moi, Adonaï ! Envoie, s'il te plaît, par la main de qui tu enverras.
4:14	La colère de YHWH s'enflamma contre Moshé et il lui dit : Aaron le Lévite n'est-il pas ton frère ? Je sais qu’il parlera, il parlera, lui ! Et même le voilà qui vient à ta rencontre, et quand il te verra, il se réjouira dans son cœur.
4:15	Tu lui parleras et tu mettras ces paroles dans sa bouche. Et moi, je serai avec ta bouche et avec sa bouche, et je vous enseignerai ce que vous ferez.
4:16	Lui, il parlera pour toi au peuple. Et c’est lui qui deviendra pour toi une bouche, et toi, tu deviendras Elohîm pour lui.
4:17	Tu prendras aussi dans ta main ce bâton, avec lequel tu feras ces signes-là.

### Moshé accepte sa mission et part en Égypte

4:18	Moshé s'en alla et retourna vers Yether<!--Jéthro.-->, son beau-père et lui dit : S’il te plaît, que je m'en aille et que je retourne vers mes frères en Égypte, pour voir s'ils vivent encore. Et Yithro lui dit : Va en paix !
4:19	YHWH dit à Moshé en Madian : Va, retourne en Égypte, car tous ceux qui cherchaient ton âme sont morts.
4:20	Moshé prit sa femme et ses fils, les fit monter sur un âne et retourna en terre d'Égypte. Moshé prit aussi le bâton d'Elohîm dans sa main.
4:21	YHWH dit à Moshé : Quand tu t’en iras pour retourner en Égypte, considère tous les miracles que j’ai mis dans ta main, et tu les feras en face de pharaon. Moi, j'endurcirai son cœur et il ne laissera pas aller le peuple.
4:22	Tu diras à pharaon : Ainsi parle YHWH : Israël est mon fils, mon premier-né<!--Os. 11:1.-->.
4:23	Je te dis : Laisse aller mon fils pour qu'il me serve. Si tu refuses de le laisser aller, voici, je tuerai ton fils, ton premier-né.
4:24	Et il arriva qu'en route, au lieu de logement, YHWH le rencontra et chercha à le tuer.
4:25	Et Tsipporah prit un caillou dur, coupa le prépuce de son fils et lui en toucha les pieds en disant : En effet, tu es pour moi un époux de sang !
4:26	Il<!--YHWH.--> le laissa. Alors elle dit : Époux de sang, à cause de la circoncision.

### YHWH envoie Aaron vers Moshé

4:27	YHWH dit à Aaron : Va dans le désert, au-devant de Moshé. Il y alla et le rencontra sur la montagne d'Elohîm, et l'embrassa.
4:28	Moshé raconta à Aaron toutes les paroles de YHWH qui l'avait envoyé, et tous les signes qu’il lui ordonnait.
4:29	Moshé et Aaron s'en allèrent et rassemblèrent tous les anciens des fils d'Israël.
4:30	Aaron déclara toutes les paroles que YHWH avait dites à Moshé, et il fit les signes aux yeux du peuple.
4:31	Et le peuple crut. Ils apprirent que YHWH avait visité les fils d'Israël, qu'il avait vu leur affliction, et ils s'inclinèrent et se prosternèrent.

## Chapitre 5

### Pharaon s'oppose à Moshé<!--Ex. 5-14.-->

5:1	Après cela, Moshé et Aaron allèrent dire au pharaon : Ainsi parle YHWH, l'Elohîm d'Israël : Laisse aller mon peuple, afin qu'il me célèbre une fête dans le désert.
5:2	Pharaon dit : Qui est YHWH pour que j'obéisse à sa voix et que je laisse aller Israël ? Je ne connais pas YHWH et je ne laisserai pas aller Israël.
5:3	Et ils dirent : L'Elohîm des Hébreux est venu au-devant de nous. S’il te plaît, nous voulons aller à trois jours de route dans le désert et sacrifier à YHWH notre Elohîm, de peur qu'il ne nous frappe par la peste ou par l'épée.
5:4	Le roi d'Égypte leur dit : Moshé et Aaron, pourquoi détournez-vous le peuple de son ouvrage ? Allez maintenant à vos travaux forcés.
5:5	Pharaon dit : Voici, le peuple de cette terre est maintenant en grand nombre, et vous lui feriez cesser leurs travaux forcés !
5:6	Ce jour-là, pharaon donna cet ordre aux oppresseurs du peuple et à ses commissaires, en disant :
5:7	Vous ne donnerez plus de paille à ce peuple pour faire des briques comme hier et avant-hier, qu'ils aillent eux-mêmes se ramasser de la paille !
5:8	Vous leur imposerez la quantité de briques qu'ils faisaient hier et avant-hier, sans en rien diminuer. Car ce sont des paresseux. C'est pour cela qu'ils crient, en disant : Nous irons, nous sacrifierons à notre Elohîm !
5:9	Que l'on impose encore plus de travail à ces hommes, qu'ils s'en occupent et qu'ils ne prêtent plus attention aux paroles de mensonge.
5:10	Les oppresseurs du peuple et ses commissaires sortirent et parlèrent au peuple, disant : Ainsi parle pharaon : Je ne vous donnerai plus de paille.
5:11	Allez vous-mêmes, prenez de la paille où vous en trouverez, mais votre travail ne sera en rien diminué.
5:12	Le peuple se répandit dans toute la terre d'Égypte, pour ramasser du chaume au lieu de paille.
5:13	Et les oppresseurs les pressaient, en disant : Achevez vos ouvrages ! La parole du jour en son jour, comme lorsqu’il y avait de la paille.
5:14	On frappait même les commissaires des fils d'Israël, établis sur eux par les oppresseurs de pharaon, en disant : Pourquoi n'avez-vous pas achevé votre tâche en faisant des briques hier et aujourd'hui, comme hier et avant-hier<!--« Hier et avant-hier », c'est-à-dire « auparavant ».--> ?
5:15	Les commissaires des fils d'Israël vinrent crier à pharaon, en disant : Pourquoi agis-tu ainsi envers tes serviteurs ?
5:16	On ne donne pas de paille à tes serviteurs, et toutefois on nous dit : Faites des briques ! Voici qu'on frappe tes serviteurs, et c'est ton peuple qui pèche !
5:17	Il dit : Vous êtes des paresseux, des paresseux ! C'est pourquoi vous dites : Nous irons, nous sacrifierons à YHWH !
5:18	Maintenant : Allez servir ! On ne vous donnera pas de paille, mais vous fournirez la même quantité de briques.
5:19	Les commissaires des fils d'Israël virent qu'ils souffraient, puisqu'on leur disait : Vous ne diminuerez rien de vos briques, la parole du jour en son jour !
5:20	Et en sortant de chez pharaon, ils rencontrèrent Moshé et Aaron qui se tenaient là pour les rencontrer.
5:21	Ils leur dirent : Que YHWH vous regarde et qu'il juge ! Car vous nous avez mis en mauvaise odeur devant pharaon et devant ses serviteurs, leur mettant l'épée à la main pour nous tuer.
5:22	Moshé retourna vers YHWH et dit : Adonaï, pourquoi as-tu fait du mal à ce peuple ? Pourquoi m'as-tu envoyé ?
5:23	Car depuis que je suis allé vers pharaon pour parler en ton Nom, il a fait du mal à ce peuple et tu n'as pas délivré ton peuple.

## Chapitre 6

### YHWH fortifie Moshé et rappelle son alliance avec Israël

6:1	YHWH dit à Moshé : Tu verras maintenant ce que je ferai à pharaon. Par main forte, il les laissera partir, par main forte, il les chassera de sa terre !
6:2	Elohîm parla encore à Moshé et lui dit : Je suis YHWH.
6:3	Je suis apparu à Abraham, à Yitzhak et à Yaacov, comme El Shaddaï<!--Voir Ge. 17:1, 28:3, 35:11, 43:14, 48:3, 49:25.-->, mais je ne me suis pas fait connaître d'eux sous mon Nom de YHWH.
6:4	J'ai aussi établi mon alliance avec eux, pour leur donner la terre de Kena'ân<!--Canaan.-->, la terre de leurs pèlerinages, en laquelle ils ont demeuré comme étrangers.
6:5	J'ai aussi entendu les gémissements des fils d'Israël, que les Égyptiens font servir, et je me suis souvenu de mon alliance.
6:6	C'est pourquoi dis aux fils d'Israël : Je suis YHWH, et je vous ferai sortir de dessous les travaux forcés des Égyptiens, et je vous délivrerai de leur servitude, je vous rachèterai à bras étendu, et par de grands jugements.
6:7	Je vous prendrai pour mon peuple et je deviendrai votre Elohîm. Vous saurez que je suis YHWH votre Elohîm, qui vous fais sortir de dessous les travaux forcés de l'Égypte.
6:8	Je vous ferai entrer sur la terre au sujet de laquelle j'ai levé ma main pour la donner à Abraham, à Yitzhak et à Yaacov, et je vous la donnerai en héritage. Je suis YHWH.
6:9	Moshé parla de cette manière aux fils d'Israël. Mais ils n'écoutèrent pas Moshé, à cause de l'angoisse de leur esprit et à cause de leur dure servitude.
6:10	YHWH parla à Moshé, en disant :
6:11	Va et dis à pharaon, roi d'Égypte, qu'il laisse sortir les fils d'Israël de sa terre.
6:12	Moshé parla en face de YHWH, en disant : Voici, les fils d'Israël ne m'ont pas écouté. Comment pharaon m'écoutera-t-il, moi, l’incirconcis des lèvres ?
6:13	YHWH parla à Moshé et à Aaron et leur donna des ordres pour les fils d'Israël et pour pharaon, roi d'Égypte, pour faire sortir les fils d'Israël de la terre d'Égypte.

### Les chefs d'Israël

6:14	Voici les chefs des pères : les fils de Reouben, premier-né d'Israël : Hanowk et Pallou, Hetsron et Karmiy. Ce sont là les familles de Reouben<!--Ge. 46:9 ; No. 26:5 ; 1 Ch. 5:3.-->.
6:15	Les fils de Shim’ôn : Yemouel, Yamin, Ohad, Yakiyn et Tsochar, et Shaoul, fils d'une Kena'ânéenne ; ce sont là les familles de Shim’ôn.
6:16	Voici les noms des fils de Lévi selon leur naissance : Guershon, Qehath et Merari. Les années de la vie de Lévi furent de 137 ans.
6:17	Les fils de Guershon : Libni et Shimeï, selon leurs familles.
6:18	Les fils de Qehath : Amram, Yitshar, Hébron et Ouzziel. Et les années de la vie de Qehath furent de 133 ans.
6:19	Les fils de Merari : Machli et Moushi. Ce sont là les familles de Lévi selon leurs générations.
6:20	Or Amram prit Yokebed, sa tante, pour femme, qui lui enfanta Aaron et Moshé. Les années de la vie d'Amram furent de 137 ans.
6:21	Les fils de Yitshar : Koré, Népheg et Zicri.
6:22	Les fils d'Ouzziel : Miyshael, Éliytsaphan et Sithri.
6:23	Aaron prit pour femme Éliysheba, fille d'Amminadab, sœur de Nahshôn, qui lui enfanta Nadab, Abihou, Èl’azar et Ithamar.
6:24	Et les fils de Koré : Assir, Elqanah et Abiasaph. Ce sont là les familles des Korites.
6:25	Èl’azar, fils d'Aaron, prit pour femme une des filles de Poutiel, qui lui enfanta Phinées. Ce sont là les chefs des pères des Lévites selon leurs familles.
6:26	Ce sont eux, Moshé et Aaron, à qui YHWH avait dit : Faites sortir les fils d'Israël de la terre d'Égypte selon leurs armées.
6:27	Ce sont eux qui parlèrent à pharaon, roi d'Égypte, pour faire sortir d'Égypte les fils d'Israël. C'est ce Moshé et c'est cet Aaron.
6:28	Il arriva qu'au jour où YHWH parla à Moshé en terre d'Égypte,
6:29	YHWH parla à Moshé en disant : C'est moi YHWH ! Dis à pharaon, roi d'Égypte, tout ce que je te dis.
6:30	Moshé dit en face de YHWH : Voici, moi je suis incirconcis des lèvres, comment pharaon m'écoutera-t-il ?

## Chapitre 7

### L'appel de Moshé confirmé

7:1	Et YHWH dit à Moshé : Regarde, je t'ai fait Elohîm pour pharaon, et Aaron ton frère deviendra ton prophète.
7:2	Tu diras tout ce que je t'ordonnerai, et Aaron ton frère parlera à pharaon pour qu'il laisse aller les fils d'Israël hors de sa terre.
7:3	J'endurcirai le cœur de pharaon et je multiplierai mes signes et mes miracles en terre d'Égypte.
7:4	Pharaon ne vous écoutera pas. Je mettrai ma main sur l'Égypte et je sortirai mes armées, mon peuple, les fils d'Israël, de la terre d'Égypte, par de grands jugements.
7:5	Les Égyptiens sauront<!--Les nations sauront que Yéhoshoua ha Mashiah (Jésus-Christ) est l'Elohîm d'Israël lorsqu'il reviendra en Sion pour délivrer et restaurer son peuple (Za. 14).--> que je suis YHWH quand j'aurai étendu ma main sur l'Égypte, et que j'aurai fait sortir du milieu d'eux les fils d'Israël.
7:6	Moshé et Aaron firent comme YHWH leur avait ordonné. Ainsi firent-ils.
7:7	Or Moshé était fils de 80 ans, et Aaron fils de 83 ans, quand ils parlèrent à pharaon.

### Le bâton d'Aaron devient un serpent

7:8	YHWH parla à Moshé et à Aaron, en disant :
7:9	Quand pharaon vous parlera, en disant : Faites un miracle ! tu diras alors à Aaron : Prends ton bâton, jette-le devant pharaon il deviendra un serpent.
7:10	Moshé et Aaron allèrent auprès de pharaon et agirent comme YHWH le leur avait ordonné. Aaron jeta son bâton devant pharaon et devant ses serviteurs et il devint un serpent.
7:11	Pharaon appela aussi les sages et les sorciers, et les magiciens d'Égypte, eux aussi firent autant par leurs flammes<!--Le terme hébreu « lahat » signifie « flamme (d'une épée angélique) ». Terme que l'on trouve également dans Ge. 3:24.-->.
7:12	Ils jetèrent chaque homme son bâton, qui devint un serpent, mais le bâton d'Aaron engloutit leurs bâtons.
7:13	Le cœur de pharaon s'endurcit et il ne les écouta pas, comme YHWH l'avait dit.

### Les eaux du fleuve changées en sang

7:14	YHWH dit à Moshé : Le cœur de pharaon est endurci, il a refusé de laisser aller le peuple.
7:15	Va-t'en dès le matin vers pharaon. Voici, il sortira pour aller près de l'eau. Tu te présenteras devant lui sur le bord du fleuve. Tu prendras dans ta main le bâton qui a été changé en serpent.
7:16	Et tu lui diras : YHWH, l'Elohîm des Hébreux, m'avait envoyé vers toi pour dire : Laisse aller mon peuple, afin qu'il me serve dans le désert, mais voici, tu ne m'as pas écouté jusqu'ici.
7:17	Ainsi parle YHWH : À ceci tu sauras que je suis YHWH. Je vais frapper du bâton qui est dans ma main les eaux du fleuve, et elles seront changées en sang.
7:18	Les poissons qui sont dans le fleuve mourront, le fleuve deviendra puant, et les Égyptiens se lasseront de boire l'eau du fleuve.
7:19	YHWH dit à Moshé : Dis à Aaron : Prends ton bâton et étends ta main sur les eaux des Égyptiens, sur leurs rivières, sur leurs ruisseaux, et sur leurs marais, et sur toutes leurs masses d'eaux, et elles deviendront du sang. Il y aura du sang par toute la terre d'Égypte, dans les vases de bois et de pierre.
7:20	Moshé et Aaron firent ce que YHWH avait ordonné. Il leva le bâton, et il frappa les eaux du fleuve, aux yeux de pharaon et aux yeux de ses serviteurs. Toutes les eaux du fleuve furent changées en sang.
7:21	Les poissons qui étaient dans le fleuve moururent, le fleuve devint puant et les Égyptiens ne pouvaient plus boire l'eau du fleuve. Il y eut du sang dans toute la terre d'Égypte.
7:22	Les magiciens d'Égypte en firent de même par leurs enchantements. Le cœur de pharaon s'endurcit et il ne les écouta pas, comme YHWH l'avait dit.
7:23	Pharaon s'en retourna et alla dans sa maison, et il n’appliqua pas son cœur même à cela.
7:24	Or tous les Égyptiens creusèrent autour du fleuve pour avoir de l'eau à boire, parce qu'ils ne pouvaient plus boire de l'eau du fleuve.
7:25	7 jours s’accomplirent après que YHWH eut frappé le fleuve.

### Invasion de grenouilles

7:26	YHWH dit à Moshé : Va vers pharaon et dis-lui : Ainsi parle YHWH : Laisse aller mon peuple afin qu'il me serve.
7:27	Si tu refuses de le laisser aller, voici, je vais frapper par des grenouilles tout ton territoire.
7:28	Le fleuve grouillera de grenouilles. Elles monteront et entreront dans ta maison, dans la chambre où tu couches, sur ton lit, dans les maisons de tes serviteurs, parmi tout ton peuple, dans tes fours et dans tes cuves de pétrissage.
7:29	Sur toi, sur ton peuple et sur tous tes serviteurs monteront les grenouilles.

## Chapitre 8

8:1	YHWH dit à Moshé : Dis à Aaron : Étends ta main avec ton bâton sur les fleuves, sur les rivières et sur les marais, et fais monter les grenouilles sur la terre d'Égypte.
8:2	Aaron étendit sa main sur les eaux de l'Égypte, et les grenouilles montèrent et couvrirent la terre d'Égypte.
8:3	Mais les magiciens firent de même par leurs enchantements et firent monter des grenouilles sur la terre d'Égypte.
8:4	Pharaon appela Moshé et Aaron, et leur dit : Implorez YHWH afin qu'il détourne les grenouilles de moi et de mon peuple, et je laisserai aller le peuple afin qu'il sacrifie à YHWH.
8:5	Moshé dit à pharaon : Glorifie-toi à mon sujet ! Pour quand dois-je implorer pour toi, pour tes serviteurs et pour ton peuple, afin d'exterminer les grenouilles loin de toi et de tes maisons ? Il en restera seulement dans le fleuve.
8:6	Et il dit : Pour demain. Il dit : Selon ta parole ! Afin que tu saches que personne n'est semblable à YHWH notre Elohîm.
8:7	Les grenouilles se détourneront de toi, de tes maisons, de tes serviteurs et de ton peuple. Il en restera seulement dans le fleuve.
8:8	Moshé et Aaron sortirent de chez pharaon. Moshé cria à YHWH au sujet des grenouilles qu'il avait fait venir sur pharaon.
8:9	Et YHWH agit selon la parole de Moshé. Ainsi les grenouilles moururent dans les maisons, dans les villages et dans les champs.
8:10	On les entassa monceaux par monceaux, et la terre en devint puante.
8:11	Mais pharaon, voyant qu'il y avait du répit, endurcit son cœur et ne les écouta pas, comme YHWH l'avait dit.

### Invasion de poux

8:12	YHWH dit à Moshé : Dis à Aaron : Étends ton bâton et frappe la poussière de la terre. Elle deviendra des poux dans toute la terre d'Égypte.
8:13	Et ils firent ainsi. Aaron étendit sa main avec son bâton et frappa la poussière de la terre. Elle devint des poux sur les humains et sur les bêtes. Toute la poussière de la terre devint des poux dans toute la terre d'Égypte.
8:14	Les magiciens firent de même par leurs enchantements, pour faire sortir des poux, mais ils ne le purent pas. Les poux vinrent tant sur les humains que sur les bêtes.
8:15	Les magiciens dirent à pharaon : C'est le doigt d'Elohîm<!--Lu. 11:20.--> ! Mais le cœur de pharaon s'endurcit et il ne les écouta pas, comme YHWH l'avait dit.

### Invasion d'insectes

8:16	YHWH dit à Moshé : Lève-toi tôt le matin et présente-toi devant pharaon. Voici, il sortira près de l'eau. Tu lui diras : Ainsi parle YHWH : Laisse aller mon peuple afin qu'il me serve.
8:17	Car si tu ne laisses pas aller mon peuple, voici, je vais envoyer contre toi, contre tes serviteurs, contre ton peuple et contre tes maisons, des essaims d'insectes. Les maisons des Égyptiens seront remplies d'essaims d'insectes, et le sol aussi sur lequel ils seront<!--Ps. 78:43, 105:31.-->.
8:18	Et ce jour-là, je ferai une distinction pour la région de Goshen où se tient mon peuple, et là il n'y aura pas d'essaims d'insectes, afin que tu saches que moi, YHWH, je suis au milieu de cette région.
8:19	Et je fixerai une rançon afin de séparer ton peuple de mon peuple. Ce signe sera pour demain.
8:20	YHWH fit ainsi. Un grand essaim d'insectes entra dans la maison de pharaon et dans chaque maison de ses serviteurs, et dans toute la terre d'Égypte et la terre fut dévastée, face à l'essaim d'insectes.

### Pharaon trompe Moshé

8:21	Et pharaon appela Moshé et Aaron, et dit : Allez, sacrifiez à votre Elohîm sur cette terre.
8:22	Mais Moshé dit : Agir ainsi ne serait pas ferme. Car ce que nous sacrifierions à YHWH notre Elohîm, serait en abomination aux Égyptiens. Voici, si nous sacrifions ce qui est en abomination aux Égyptiens sous leurs yeux, ne nous lapideront-ils pas ?
8:23	Nous irons dans le désert, à trois jours de route, et nous sacrifierons à YHWH notre Elohîm, comme il nous le dira.
8:24	Pharaon dit : Je vous laisserai aller pour sacrifier dans le désert à YHWH votre Elohîm. Toutefois, vous ne vous éloignerez pas, vous ne vous éloignerez pas en y allant. Implorez pour moi.
8:25	Moshé dit : Voici, je sors de chez toi et j’implorerai YHWH afin que les essaims d'insectes se détournent demain de pharaon, de ses serviteurs et de son peuple. Mais que pharaon ne continue pas à se moquer en ne laissant pas aller le peuple pour sacrifier à YHWH.
8:26	Moshé sortit de chez pharaon et implora YHWH.
8:27	Et YHWH agit selon la parole de Moshé. Les essaims d'insectes se détournèrent de pharaon, de ses serviteurs et de son peuple. Il n'en resta pas un seul.
8:28	Mais pharaon endurcit son cœur cette fois encore et ne laissa pas aller le peuple.

## Chapitre 9

### La mort des troupeaux

9:1	YHWH dit à Moshé : Va vers pharaon et dis-lui : Ainsi parle YHWH, l'Elohîm des Hébreux : Laisse aller mon peuple afin qu'il me serve.
9:2	Car si tu refuses de le laisser aller et si tu le retiens encore,
9:3	voici, la main de YHWH sera sur ton bétail qui est dans les champs, tant sur les chevaux que sur les ânes, sur les chameaux, sur les bœufs et sur les brebis, et il y aura une très grande peste.
9:4	YHWH fera une distinction entre le bétail d’Israël et le bétail d’Égypte, afin que rien de ce qui est aux fils d'Israël ne meure.
9:5	Et YHWH fixa un temps, en disant : Demain, YHWH fera cela sur la terre.
9:6	YHWH fit cette chose-ci dès le lendemain : tout le bétail des Égyptiens mourut, mais du bétail des fils d'Israël, il n'en mourut pas un seul.
9:7	Pharaon envoya, et voici, pas de mort dans le bétail d'Israël, pas même un. Mais le cœur de pharaon s'endurcit et il ne laissa pas aller le peuple.

### Des ulcères sur les Égyptiens et les bêtes

9:8	YHWH dit à Moshé et à Aaron : Prenez pour vous pleines poignées de cendre de fournaise et que Moshé la jette vers les cieux sous les yeux de pharaon.
9:9	Et elle deviendra de la poussière sur toute la terre d'Égypte, et elle provoquera sur les humains et sur les bêtes, des ulcères bourgeonnant en pustules, dans toute la terre d'Égypte.
9:10	Ils prirent de la cendre de fournaise et se tinrent devant pharaon. Moshé la jeta vers les cieux et il se forma des ulcères bourgeonnant en pustules tant sur les humains que sur les bêtes.
9:11	Les magiciens ne pouvaient se tenir debout face à Moshé, face aux ulcères. Car les magiciens avaient des ulcères comme tous les Égyptiens.
9:12	Et YHWH endurcit le cœur de pharaon, et il ne les écouta pas, comme YHWH l'avait déclaré à Moshé.

### L'Égypte frappée par la grêle et le feu

9:13	YHWH parla à Moshé : Lève-toi tôt le matin et présente-toi devant pharaon, et dis-lui : Ainsi parle YHWH, l'Elohîm des Hébreux : Laisse aller mon peuple afin qu'il me serve.
9:14	Car cette fois, je vais envoyer toutes mes plaies contre ton cœur, contre tes serviteurs et contre ton peuple afin que tu saches que personne n'est semblable à moi sur toute la Terre.
9:15	Car maintenant si j'avais étendu ma main, je t'aurais frappé par la peste, toi et ton peuple, et tu serais effacé de la Terre.
9:16	Mais je t'ai laissé debout afin que tu voies ma force et qu'on publie mon Nom par toute la Terre<!--Ro. 9:17.-->.
9:17	T'élèves-tu encore contre mon peuple en ne le laissant pas partir ?
9:18	Voici, je ferai pleuvoir demain à cette même heure, une grêle tellement forte qu'il n'y en a pas eu de semblable en Égypte, depuis le jour où elle fut fondée jusqu'à maintenant.
9:19	Maintenant, envoie rassembler ton bétail et tout ce que tu as à la campagne. Car la grêle tombera sur tous les humains et sur toutes les bêtes qui se trouveront dans la campagne et qui n'auront pas été rassemblés à la maison, et ils mourront.
9:20	Ceux d'entre les serviteurs de pharaon qui craignirent la parole de YHWH, firent promptement retirer dans les maisons, leurs serviteurs et leurs bêtes.
9:21	Mais ceux qui ne prirent pas à cœur la parole de YHWH, laissèrent leurs serviteurs et leurs bêtes à la campagne.
9:22	YHWH dit à Moshé : Étends ta main vers les cieux et il y aura de la grêle sur toute la terre d'Égypte, sur les humains et sur les bêtes, et sur toutes les herbes des champs en terre d'Égypte.
9:23	Moshé étendit son bâton vers les cieux, et YHWH donna des voix et de la grêle, et le feu se promenait sur la terre. YHWH fit pleuvoir de la grêle sur la terre d'Égypte.
9:24	Il y eut de la grêle et le feu jaillissait au milieu de la grêle. Elle était si grosse qu'il n'y en avait pas eu de semblable sur toute la terre d'Égypte depuis qu'elle existe en tant que nation.
9:25	La grêle frappa dans toute la terre d'Égypte tout ce qui était aux champs, depuis les humains jusqu'aux bêtes. La grêle frappa aussi toutes les herbes des champs et brisa tous les arbres des champs.
9:26	Ce fut seulement dans la région de Goshen, où étaient les fils d'Israël, qu'il n'y eut pas de grêle.

### Pharaon continue d'endurcir son cœur

9:27	Pharaon envoya appeler Moshé et Aaron, et leur dit : Cette fois-ci, j'ai péché. YHWH est juste, mais mon peuple et moi sommes méchants.
9:28	Implorez YHWH, c’est assez des voix d'Elohîm et de la grêle ! Je vous laisserai aller, et vous ne resterez pas davantage.
9:29	Moshé dit : Aussitôt que je sortirai de la ville, j'étendrai mes paumes vers YHWH, les voix cesseront. Il n'y aura plus de grêle, afin que tu saches que la Terre est à YHWH<!--Ps. 24:1.-->.
9:30	Quant à toi et tes serviteurs, je sais que vous ne craignez pas encore en face de YHWH Elohîm.
9:31	Or le lin et l'orge avaient été frappés, car l'orge était en épis et c'était la floraison du lin.
9:32	Mais le blé et l'épeautre ne furent pas frappés, parce qu'ils sont tardifs.
9:33	Moshé sortit de chez pharaon pour aller hors de la ville. Il étendit ses paumes vers YHWH, et les voix cessèrent, et la grêle et la pluie ne tombèrent plus sur la terre.
9:34	Pharaon, voyant que la pluie, la grêle et les voix avaient cessé, continua encore de pécher, et il endurcit son cœur, lui et ses serviteurs.
9:35	Et le cœur de pharaon s'endurcit et il ne laissa pas aller les fils d'Israël, comme YHWH l'avait dit par la main de Moshé.

## Chapitre 10

### Invasion de sauterelles

10:1	YHWH dit à Moshé : Va vers pharaon, car j'ai endurci son cœur et le cœur de ses serviteurs, afin de placer mes signes au milieu d'eux.
10:2	Afin que tu racontes aux oreilles de ton fils et du fils de ton fils comment j'ai traité avec sévérité les Égyptiens et quels signes j'ai produits parmi eux, et que vous sachiez que je suis YHWH.
10:3	Moshé et Aaron allèrent chez pharaon et lui dirent : Ainsi parle YHWH, l'Elohîm des Hébreux : Jusqu'à quand refuseras-tu de t'humilier face à moi ? Laisse aller mon peuple afin qu'il me serve !
10:4	Car si tu refuses de laisser aller mon peuple, voici, je ferai venir demain des sauterelles dans ton territoire.
10:5	Elles couvriront l'œil de la terre et l'on ne pourra plus voir la terre. Elles dévoreront le reste de ce qui a échappé, ce que la grêle vous a laissé. Elles dévoreront tous les arbres qui poussent dans vos champs.
10:6	Elles rempliront tes maisons, les maisons de tous tes serviteurs et les maisons de tous les Égyptiens, ce que tes pères n'ont pas vu ni les pères de tes pères, depuis qu'ils existent sur le sol jusqu'à ce jour. Puis il se tourna et sortit de chez pharaon.
10:7	Les serviteurs de pharaon lui dirent : Jusqu'à quand cet homme sera-t-il un piège pour nous ? Laisse aller ces gens et qu'ils servent YHWH, leur Elohîm. Ne sais-tu pas encore que l'Égypte périt ?
10:8	On fit revenir Moshé et Aaron vers pharaon, il leur dit : Allez ! Servez YHWH, votre Elohîm. Qui sont ceux qui iront ?
10:9	Moshé dit : Nous irons avec nos jeunes hommes et nos vieillards, avec nos fils et nos filles. Nous irons avec nos brebis et nos bœufs, car nous avons à fêter YHWH.
10:10	Il leur dit : Que YHWH soit avec vous, comme je laisserai aller vos petits enfants ! Prenez garde, car le mal est devant vous.
10:11	Non, pas ainsi ! Allez, s’il vous plaît, vous, les hommes forts, et servez YHWH, car c'est ce que vous demandiez ! Et on les chassa de la présence de pharaon.
10:12	YHWH dit à Moshé : Étends ta main sur la terre d'Égypte pour faire venir les sauterelles, afin qu'elles montent sur la terre d'Égypte, qu'elles dévorent toute l'herbe de la terre, tout ce que la grêle a laissé.
10:13	Moshé étendit son bâton sur la terre d'Égypte et YHWH amena sur la terre, tout ce jour-là et toute la nuit un vent d'orient. Le matin vint et le vent d'orient enleva les sauterelles.
10:14	Les sauterelles montèrent sur toute la terre d'Égypte, elles se posèrent sur tout le territoire de l'Égypte en très grand nombre. Avant elles, il n'y avait pas eu de sauterelles semblables, et après elles, il n'y en aura pas de pareilles.
10:15	Elles couvrirent l'œil de toute la terre, et la terre fut obscurcie. Elles dévorèrent toute l'herbe de la terre, tout le fruit des arbres que la grêle avait laissé ; il ne resta aucune verdure aux arbres ni aux herbes des champs, dans toute la terre d'Égypte.
10:16	Pharaon se hâta d'appeler Moshé et Aaron et dit : J'ai péché contre YHWH votre Elohîm et contre vous.
10:17	Maintenant pardonne, s’il te plaît, mon péché pour cette fois seulement. Implorez YHWH votre Elohîm afin qu'il éloigne de moi encore cette mort.
10:18	Il sortit de chez pharaon et implora YHWH.
10:19	YHWH fit lever un vent d'occident très fort qui enleva les sauterelles et les précipita dans la Mer Rouge. Il ne resta pas une seule sauterelle dans tout le territoire de l'Égypte.
10:20	YHWH endurcit le cœur de pharaon et il ne laissa pas aller les fils d'Israël.

### Les ténèbres sur les Égyptiens

10:21	YHWH dit à Moshé : Étends ta main vers les cieux, qu’il y ait une ténèbre sur la terre d’Égypte, une ténèbre où l'on tâtonne !
10:22	Moshé étendit sa main vers les cieux, et il y eut de l'obscurité, une ténèbre sur toute la terre d'Égypte, pendant 3 jours<!--Ps. 105:28.-->.
10:23	L'homme ne voyait pas son frère et aucun homme ne se leva de sa place pendant 3 jours. Mais pour tous les fils d'Israël, il y avait de la lumière là où ils habitaient.

### Pharaon tente encore de compromettre Moshé

10:24	Pharaon appela Moshé et dit : Allez ! Servez YHWH. Seulement que vos brebis et vos bœufs restent. Vos petits enfants iront aussi avec vous.
10:25	Moshé dit : Tu mettras toi-même entre nos mains de quoi faire des sacrifices et des holocaustes à YHWH notre Elohîm.
10:26	Même nos troupeaux viendront avec nous, il n'en restera pas un sabot, car c’est d’eux que nous prendrons de quoi servir YHWH, notre Elohîm ; et nous-mêmes, jusqu'à ce que nous soyons arrivés là, nous ne savons pas comment servir YHWH. 
10:27	YHWH endurcit le cœur de pharaon et il ne voulut pas les laisser aller.
10:28	Pharaon lui dit : va-t-en d’auprès de moi ! Garde-toi ! Tu ne continueras pas à voir mes faces ! Car le jour où tu verras mes faces, tu mourras !
10:29	Moshé dit : Ainsi que tu l’as dit, je ne continuerai plus à voir tes faces<!--Hé. 11:27.--> !

## Chapitre 11

### Pharaon méprise l'avertissement sur la mort des premiers-nés

11:1	YHWH dit à Moshé : Je ferai venir encore une plaie sur pharaon et sur l'Égypte. Après cela, il vous renverra d'ici. Il vous renverra, et même, il vous chassera, il vous chassera complètement d'ici.
11:2	S’il te plaît, parle aux oreilles du peuple et dis-leur : Que chaque homme demande à son ami, et chaque femme à sa voisine, des objets d'argent et des objets d'or.
11:3	Or YHWH avait donné au peuple de la grâce aux yeux des Égyptiens. Cet homme, Moshé, était aussi très grand en terre d'Égypte, aux yeux des serviteurs de pharaon et aux yeux du peuple.
11:4	Moshé dit : Ainsi parle YHWH : Vers le milieu de la nuit, je passerai au travers de l'Égypte,
11:5	et tout premier-né mourra en terre d'Égypte, depuis le premier-né de pharaon assis sur son trône jusqu'au premier-né de la servante qui est derrière la meule, ainsi que tous les premiers-nés des bêtes.
11:6	Il y aura un grand cri de détresse dans toute la terre d'Égypte, tel qu'il n'y en a jamais eu et qu'il n'y en aura jamais de semblable.
11:7	Mais contre tous les fils d'Israël, pas même un chien ne remuera sa langue, depuis l'homme jusqu'aux bêtes, afin que vous sachiez qu'Elohîm fait une distinction entre l’Égypte et Israël.
11:8	Tous tes serviteurs viendront vers moi et se prosterneront devant moi, en disant : Sors, toi et tout le peuple qui est à tes pieds. Après cela, je sortirai. Ainsi, Moshé sortit de chez pharaon dans une ardente colère.
11:9	YHWH dit à Moshé : Pharaon ne vous écoutera pas, afin que mes miracles soient multipliés en terre d'Égypte.
11:10	Moshé et Aaron firent tous ces miracles-là devant pharaon, et YHWH endurcit le cœur de pharaon et il ne laissa pas aller les fils d'Israël hors de sa terre.

## Chapitre 12

### La première Pâque

12:1	YHWH parla à Moshé et à Aaron en terre d'Égypte, en disant :
12:2	Ce mois-ci sera pour vous la tête des mois, il sera pour vous le premier des mois de l'année.
12:3	Parlez à toute l'assemblée d'Israël, en disant : Le dixième jour de ce mois, qu’ils prennent, chaque homme, un agneau<!--Littéralement : « homme agneau ». Yéhoshoua est l'Agneau qui sauve.--> par maison de pères, un agneau par maison.
12:4	Si la maison est trop petite pour un agneau, on le prendra avec le voisin le plus proche de sa maison d’après le nombre des âmes. Vous compterez pour cet agneau selon ce que chaque homme peut manger.
12:5	Ce sera un agneau ou un chevreau sans défaut, mâle, fils d'un an<!--La Pâque juive était célébrée le 14ème jour du premier mois de l'année juive soit, le 14 du mois de Nisan (Ex. 12:2 ; No. 9:1-5). L'agneau pascal était une préfiguration de Yéhoshoua ha Mashiah (Jésus-Christ) : l'agneau d'Elohîm qui ôte le péché du monde (Jn. 1:29). Ses caractéristiques sont les suivantes : \\- L'agneau devait nécessairement être un mâle sans défaut (Ex. 12:5). Yéhoshoua est l'enfant mâle mis au monde par une vierge, il n'a pas été affecté par le sang corrompu d'Adam, il est donc sans défaut (Es. 7:14 ; Mt. 1:20-21). Pour être certains de la perfection de l'animal, les Hébreux devaient l'examiner pendant quatre jours avant de l'immoler (Ex. 12:3-6). Il est à noter que la torah exigeait que deux ou trois témoins soient présents pour constater un crime ou un péché (De. 17:6, 19:15), ces quatre jours font donc office de quatre témoins pour attester de la pureté de l'animal. De même, les 4 auteurs de l'Évangile attestent la sainteté du Seigneur. De plus, avant sa mise à mort, le Seigneur a été examiné par deux législations : juive (le sanhédrin) et romaine (Ponce Pilate, Hérode). Ces deux législations attestèrent, malgré elles, son innocence (Mt. 26:60, 27:24 ; Mc. 14:55-56, 15:14 ; Lu. 23:4 ; Jn. 18:31, 19:6) et confirmèrent qu'il était sans défaut et donc digne d'être offert en sacrifice.\\- YHWH avait prescrit aux Hébreux d'immoler l'agneau entre les deux soirs (Ex. 12:6). Yéhoshoua fut arrêté la nuit de Pâque (Mc. 14:12-41). Sa crucifixion eut lieu le lendemain, à la troisième heure (Mc. 15:25), et sa mort survint à la neuvième heure (Mt. 27:45). L'agneau devait être rôti au feu puis consommé avec du pain sans levain et des herbes amères (Ex. 12:8). Le feu symbolise le jugement que le Seigneur a pris sur lui à cause de nos péchés (Es. 53:5 ; Ro. 4:25 ; 1 Pi. 1:18-20). Le pain sans levain est une autre image de Yéhoshoua, le pain de vie (Jn. 6:35) sans aucun péché (1 Co. 5:8). Les herbes amères préfigurent, quant à elles, l'affliction et la souffrance du Seigneur (Hé. 2:10).-->. Vous le prendrez d'entre les brebis ou d'entre les chèvres.
12:6	Vous le tiendrez en dépôt jusqu'au quatorzième jour de ce mois, et toute la congrégation de l'assemblée d'Israël le tuera entre les deux soirs.
12:7	Ils prendront de son sang et le mettront sur les deux poteaux et sur le linteau de la porte des maisons, où ils le mangeront.
12:8	Ils en mangeront la chair rôtie au feu cette nuit-là. Ils la mangeront avec des pains sans levain et avec des herbes amères.
12:9	N'en mangez rien de cru, ni qui ait été bouilli dans l'eau, mais rôti au feu, sa tête, ses jambes et ses entrailles.
12:10	Ne laissez aucun reste jusqu'au matin, mais s'il en reste quelque chose le matin, vous le brûlerez au feu.
12:11	Vous le mangerez ainsi : vos reins seront ceints, vous aurez vos sandales à vos pieds et votre bâton à la main, et vous le mangerez à la hâte. C'est la Pâque de YHWH.

### Le sang qui sauve ; l'instauration de la fête de la Pâque

12:12	Je passerai cette nuit-là en terre d'Égypte, et je frapperai tout premier-né en terre d'Égypte, depuis les humains jusqu'aux bêtes et j'exercerai des jugements sur tous les elohîm de l'Égypte, moi, YHWH.
12:13	Le sang deviendra pour vous un signe sur les maisons où vous serez : quand je verrai le sang, je passerai par-dessus vous, et il n'y aura pas sur vous de plaie de destruction quand je frapperai la terre d'Égypte.
12:14	Ce jour deviendra pour vous un souvenir, vous le célébrerez comme une fête pour YHWH. Vous le célébrerez comme une fête solennelle en vos générations par un statut perpétuel.
12:15	Vous mangerez pendant 7 jours des pains sans levain, et dès le premier jour, vous ôterez le levain de vos maisons. Car quiconque mangera du pain levé, depuis le premier jour jusqu'au septième, cette âme-là sera retranchée d'Israël.
12:16	Le premier jour, il y aura une sainte convocation, et il y aura de même au septième jour une sainte convocation. On ne fera aucune œuvre, seulement ce que chaque âme mangera, cela seul se fera par vous.
12:17	Vous prendrez garde aux pains sans levain, parce qu'en ce même jour, j'ai fait sortir vos armées de la terre d'Égypte. Vous observerez ce jour-là en vos générations par un statut perpétuel.
12:18	Au premier mois, le quatorzième jour du mois, au soir, vous mangerez des pains sans levain jusqu'au vingt et unième jour du mois, au soir.
12:19	Il ne se trouvera pas de levain dans vos maisons pendant 7 jours, car quiconque mangera du pain levé, cette âme-là sera retranchée de l'assemblée d'Israël, tant celui qui habite comme étranger que l’autochtone de la terre.
12:20	Vous ne mangerez pas de pain levé dans tous les lieux où vous habiterez, vous mangerez des pains sans levain.
12:21	Moshé appela tous les anciens d'Israël et leur dit : Tirez et prenez pour vous un agneau, pour vos familles, et tuez-le pour la Pâque.
12:22	Vous prendrez un bouquet d'hysope et le tremperez dans le sang qui sera dans un bassin, et vous arroserez du sang qui sera dans le bassin, le linteau et les deux poteaux. Aucun de vous ne sortira de la porte de sa maison jusqu'au matin.
12:23	YHWH passera pour frapper l'Égypte et il verra le sang sur le linteau et sur les deux poteaux, et YHWH passera par-dessus la porte, et ne permettra pas que le destructeur entre dans vos maisons pour frapper.
12:24	Vous garderez cette parole comme une ordonnance perpétuelle, pour toi et pour tes fils.
12:25	Il arrivera que quand vous viendrez sur la terre que YHWH vous donnera, comme il l'a dit, vous observerez ce service.
12:26	Il arrivera que quand vos fils vous diront : Que signifie pour vous ce service ?
12:27	Vous direz : C'est le sacrifice de la Pâque pour YHWH, qui passa en Égypte par-dessus les maisons des fils d'Israël, quand il frappa l'Égypte et qu'il sauva nos maisons. Le peuple s'inclina et se prosterna.
12:28	Les fils d'Israël s'en allèrent et firent comme YHWH l'ordonna à Moshé et à Aaron. Ainsi firent-ils.

### Les premiers-nés d'Égypte frappés

12:29	Et il arriva qu'au milieu de la nuit, YHWH frappa tous les premiers-nés de la terre d'Égypte, depuis le premier-né de pharaon qui devait s'asseoir sur son trône, jusqu'aux premiers-nés des captifs qui étaient dans la prison, et tous les premiers-nés des bêtes.
12:30	Pharaon se leva pendant la nuit, lui avec tous ses serviteurs et tous les Égyptiens, et il y eut un grand cri de détresse en Égypte, car il n'y avait pas de maison où il n'y eût un mort<!--No. 8:17 ; Ps. 78:51, 105:36 ; Hé. 11:28.-->.

### Israël sort d'Égypte

12:31	Il appela Moshé et Aaron de nuit, et leur dit : Levez-vous ! Sortez du milieu de mon peuple, vous et les fils d'Israël. Allez et servez YHWH, comme vous l'avez dit.
12:32	Prenez aussi votre petit et votre gros bétail, comme vous l'avez dit, et allez-vous-en et bénissez-moi.
12:33	Les Égyptiens pressaient le peuple en se hâtant de le laisser aller de la terre, car ils disaient : Nous sommes tous morts !
12:34	Le peuple prit sa pâte avant qu'elle soit levée, ayant leurs cuves de pétrissage serrées dans leurs vêtements sur leurs épaules.
12:35	Les fils d'Israël firent selon la parole de Moshé, et demandèrent aux Égyptiens des vases d'argent et d'or, et des vêtements.
12:36	YHWH donna au peuple de la grâce aux yeux des Égyptiens, qui leur donnèrent ce qu'ils demandaient. Ainsi ils dépouillèrent les Égyptiens.
12:37	Les fils d'Israël étant partis de Ramsès, vinrent à Soukkoth, environ 600 000 hommes de pied, sans les enfants.
12:38	Beaucoup de peuples mélangés<!--Vient de l'hébreu " ereb » qui signifie : « entrelacé », « mélange », « personnes mixtes », « société mixte ».--> aussi montèrent avec eux, ainsi que des brebis, des bœufs et un bétail extrêmement nombreux.
12:39	Ils firent cuire des gâteaux sans levain avec la pâte qu'ils avaient emportée d'Égypte, et qui n'était pas levée. Car ils avaient été chassés d'Égypte, sans pouvoir s'attarder et sans avoir fait de provisions pour eux.
12:40	Le séjour des fils d'Israël, depuis qu’ils s’établirent en Égypte fut de 430 ans<!--Ge. 15:13 ; Ac. 7:6 ; Ga. 3:17.-->.
12:41	Il arriva au bout de 430 ans, il arriva en ce même jour, que toutes les armées de YHWH sortirent de la terre d'Égypte.
12:42	Ce fut là une nuit de veille pour YHWH, parce qu'il les fit sortir de la terre d'Égypte. Cette nuit-là est à observer pour YHWH, par tous les fils d'Israël en leurs générations<!--De. 16:1-6.-->.
12:43	YHWH dit à Moshé et à Aaron : Voici le statut de la Pâque : Aucun fils d'étranger n'en mangera.
12:44	Quant à tout esclave, homme acquis à prix d'argent, tu le circonciras, alors il en mangera.
12:45	L'étranger et le mercenaire n'en mangeront pas.
12:46	On la mangera dans une même maison, et vous n'emporterez pas de chair hors de la maison, et vous n'en briserez aucun os<!--Voir Jn. 19:36.-->.
12:47	Toute l'assemblée d'Israël la fera.
12:48	Et quand un étranger qui séjournera chez toi voudra faire la Pâque pour YHWH, que tout mâle qui lui appartient soit circoncis, et alors il s'approchera pour la faire, et il sera comme l’autochtone de la terre. Mais aucun incirconcis n'en mangera.
12:49	Il y aura une même torah pour l’autochtone et pour l'étranger qui habite parmi vous.
12:50	Tous les fils d'Israël agirent comme YHWH l'avait ordonné à Moshé et à Aaron. C'est ainsi qu'ils agirent.
12:51	Il arriva en ce même jour que YHWH fit sortir les fils d'Israël de la terre d'Égypte, selon leurs armées.

## Chapitre 13

### Consécration des premiers-nés à YHWH

13:1	YHWH parla à Moshé et dit :
13:2	Consacre-moi tout premier-né. Tout ce qui ouvre la matrice parmi les fils d’Israël, humain ou bête, est à moi<!--Lé. 27:26-27 ; No. 3:13, 8:17 ; Lu. 2:22-23.-->.
13:3	Moshé dit au peuple : Souvenez-vous de ce jour où vous êtes sortis d'Égypte, de la maison des esclaves. Car YHWH vous en a fait sortir par sa main puissante. On ne mangera pas de pain levé.
13:4	Vous sortez aujourd'hui, au mois d’Abib<!--Nisan (ou épis) = Mars-Avril. Voir De. 16:1. C'est le mois de la formation de l'épi, de l'herbe verte-->.
13:5	Il arrivera que quand YHWH t’aura fait entrer en terre des Kena'ânéens, des Héthiens, des Amoréens, des Héviens et des Yebousiens, qu'il a juré à tes pères de te donner, et qui est une terre où coulent le lait et le miel, tu feras ce service en ce mois-ci.
13:6	Pendant 7 jours tu mangeras des pains sans levain et le septième jour il y aura une fête pour YHWH.
13:7	On mangera durant 7 jours des pains sans levain. On ne verra pas de pain levé chez toi, et l'on ne verra pas de levain chez toi dans tout ton territoire.
13:8	Et ce jour-là tu expliqueras ces choses à tes enfants, en disant : C'est à cause de ce que YHWH a fait pour moi lorsque je suis sorti d'Égypte.
13:9	Cela deviendra un signe sur ta main et un souvenir entre tes yeux, afin que la torah de YHWH soit dans ta bouche, car YHWH t'a fait sortir d'Égypte par sa main puissante<!--De. 6:8, 11:18.-->.
13:10	Tu observeras ce statut au temps fixé d'année en année.
13:11	Et il arrivera, quand YHWH t’aura fait entrer en terre des Kena'ânéens, comme il l’a juré à toi et à tes pères, et qu’il te l’aura donnée,
13:12	que tu consacreras à YHWH tout premier-né qui ouvre la matrice, même tout premier-né des animaux que tu auras, les mâles appartiendront à YHWH.
13:13	Tu rachèteras avec un agneau ou un chevreau, tout premier-né de l'ânesse, et si tu ne le rachètes pas, tu lui briseras la nuque. Tu rachèteras aussi tout premier-né de l'être humain parmi tes fils.
13:14	Il arrivera que quand ton fils t'interrogera à l'avenir, en disant : Que signifie cela ? Tu lui diras : C'est par la force de sa main que YHWH nous a fait sortir d'Égypte, de la maison des esclaves.
13:15	Il arriva, comme pharaon s’endurcissait à ne pas nous laisser aller, YHWH tua tous les premiers-nés en terre d'Égypte, depuis les premiers-nés des humains jusqu'aux premiers-nés des bêtes. Voilà pourquoi je sacrifie à YHWH tout premier-né mâle qui ouvre la matrice et je rachète tout premier-né de mes fils.
13:16	Cela deviendra un signe sur ta main et un fronteau entre tes yeux, car c'est par la force de sa main que YHWH nous a fait sortir d'Égypte.

### Début du voyage, YHWH dirige son peuple

13:17	Et il arriva, quand pharaon laissa aller le peuple, qu'Elohîm ne les conduisit pas par le chemin de la terre des Philistins, parce qu'elle était trop proche. Car Elohîm dit : De peur que le peuple ne se repente, quand ils verront la guerre et qu'ils ne retournent en Égypte.
13:18	Mais Elohîm fit tourner le peuple par le chemin du désert, vers la Mer Rouge. Ainsi, les fils d'Israël montèrent en armes hors de la terre d'Égypte.
13:19	Moshé prit avec lui les ossements de Yossef, parce que Yossef avait fait jurer, il avait fait jurer les fils d'Israël en leur disant : Elohîm vous visitera, il vous visitera et vous ferez monter d'ici mes ossements avec vous<!--Ge. 50:25 ; Jos. 24:32.-->.
13:20	Ils partirent de Soukkoth et campèrent à Étham, qui est à l'extrémité du désert.
13:21	YHWH marchait devant eux, le jour dans une colonne de nuée pour les conduire par le chemin, et la nuit dans une colonne de feu pour être leur lumière<!--Es. 60:19.-->, afin qu'ils marchent jour et nuit<!--No. 9:13-23, 10:34 ; De. 1:33 ; Né. 9:12-19 ; 1 Co. 10:1.-->.
13:22	Il ne retira pas la colonne de nuée le jour, ni la colonne de feu la nuit de devant le peuple.

## Chapitre 14

### Pharaon et son armée à la poursuite d'Israël

14:1	YHWH parla à Moshé et dit :
14:2	Parle aux fils d'Israël : Qu'ils se détournent et qu'ils campent devant Pi-Hahiroth, entre Migdol et la mer, en face de Baal-Tsephon. Vous camperez en face de ce lieu-là, près de la mer<!--No. 33:7.-->.
14:3	Pharaon dira des fils d'Israël : Ils sont dans la confusion sur la terre, le désert les a enfermés.
14:4	Et j'endurcirai le cœur de pharaon et il vous poursuivra. Ainsi je serai glorifié par le moyen de pharaon et de toute son armée, et les Égyptiens sauront que je suis YHWH. C'est ce qu'ils firent.
14:5	On annonça au roi d'Égypte que le peuple s'enfuyait. Le cœur de pharaon et de ses serviteurs fut changé à l'égard du peuple et ils se dirent : Qu'est-ce que nous avons fait en laissant aller Israël, de sorte qu'il ne nous servira plus ?
14:6	Il fit atteler son char et il prit son peuple avec lui.
14:7	Il prit 600 chars sélectionnés et tous les chars de l'Égypte, chacun d'eux monté par des officiers.
14:8	Et YHWH endurcit le cœur de pharaon, roi d'Égypte, qui poursuivit les fils d'Israël. Or les fils d'Israël étaient sortis à main levée<!--Lé. 26:13 ; No. 33:3.-->.
14:9	Les Égyptiens les poursuivirent et tous les chevaux des chars de pharaon, ses cavaliers et son armée les atteignirent, comme ils étaient campés près de la mer, vers Pi-Hahiroth en face de Baal-Tsephon.
14:10	Et pharaon approchait. Les fils d'Israël levèrent leurs yeux, et voici, les Égyptiens marchaient après eux. Et les fils d'Israël eurent une grande frayeur et crièrent à YHWH.
14:11	Ils dirent à Moshé : Est-ce parce qu’il n’y a pas de sépulcre en Égypte que tu nous as pris pour mourir dans le désert ? Que nous as-tu fait en nous faisant sortir de l’Égypte ?
14:12	N’est-ce pas la parole que nous te déclarions en Égypte, en disant : Retire-toi de nous afin que nous servions les Égyptiens ? Oui, nous aimons mieux les servir que de mourir dans le désert.

### Délivrance miraculeuse par YHWH

14:13	Moshé dit au peuple : N'ayez pas peur ! Tenez-vous là et voyez le salut de YHWH, celui qu’il va accomplir pour vous aujourd’hui ! Car les Égyptiens que vous voyez aujourd'hui, vous ne les verrez plus !
14:14	YHWH combattra pour vous, et vous, tenez-vous tranquilles.
14:15	YHWH dit à Moshé : Pourquoi cries-tu vers moi ? Parle aux fils d'Israël et qu'ils marchent.
14:16	Et toi, lève ton bâton, étends ta main sur la mer, fends-la et que les fils d'Israël entrent au milieu de la mer à sec.
14:17	Quant à moi, voici, je vais endurcir le cœur des Égyptiens afin qu'ils entrent après eux, et je serai glorifié par le moyen de pharaon, de toute son armée, de ses chars et de ses cavaliers.
14:18	Et les Égyptiens sauront que je suis YHWH, quand j'aurai été glorifié par le moyen de pharaon, de ses chars et de ses cavaliers.
14:19	L'Ange d'Elohîm qui marchait devant le camp d'Israël partit et s'en alla derrière eux, et la colonne de nuée partit de devant eux et se tint derrière eux.
14:20	Elle vint entre le camp des Égyptiens et le camp d'Israël. Cette nuée était à la fois ténèbre et lumière dans la nuit. Ils ne s'approchèrent pas les uns des autres pendant toute la nuit.
14:21	Moshé étendit sa main sur la mer, et YHWH fit reculer la mer toute la nuit par un puissant vent d'orient et il mit la mer à sec. Les eaux se fendirent<!--Jos. 4:23 ; Ps. 66:6, 106:9 ; Hé. 11:29.-->.
14:22	Les fils d'Israël entrèrent au milieu de la mer à sec, et les eaux leur servaient de mur à droite et à gauche.
14:23	Les Égyptiens les poursuivirent, tous les chevaux du pharaon avec ses chars et ses cavaliers entrèrent après eux au milieu de la mer.
14:24	Mais il arriva, à la veille du matin, que YHWH, depuis la colonne de feu et de nuée, regarda le camp des Égyptiens et le mit en déroute.
14:25	Il ôta les roues de leurs chars et alourdit leur marche. Les Égyptiens dirent : Fuyons devant Israël, car YHWH combat pour eux contre les Égyptiens.
14:26	Et YHWH dit à Moshé : Étends ta main sur la mer, et les eaux retourneront sur les Égyptiens, sur leurs chars et sur leurs cavaliers.
14:27	Moshé étendit sa main sur la mer et, vers le matin, la mer revint à sa place habituelle. Les Égyptiens en fuyant la rencontrèrent, et YHWH se débarrassa des Égyptiens au milieu de la mer.
14:28	Les eaux revinrent et couvrirent les chars, les cavaliers et toute l'armée de pharaon qui étaient entrés derrière eux dans la mer. Il ne resta pas un seul d’entre eux.
14:29	Mais les fils d'Israël marchèrent au milieu de la mer à sec, et les eaux leur servaient de mur à droite et à gauche.
14:30	YHWH, en ce jour-là, sauva Israël de la main des Égyptiens. Israël vit sur le bord de la mer les Égyptiens morts.
14:31	Israël vit la grande main que YHWH avait déployée contre les Égyptiens. Le peuple craignit YHWH et il crut en YHWH et en Moshé, son serviteur.

## Chapitre 15

### Cantique de délivrance

15:1	Alors Moshé et les fils d'Israël chantèrent ce cantique à YHWH. Ils parlèrent, disant : Je chanterai à YHWH car il s'est levé, il s'est levé : il a jeté dans la mer le cheval et celui qui le montait.
15:2	Yah<!--Yah est la forme raccourcie de YHWH. Voir le dictionnaire en annexe.--> est ma force et ma musique<!--Ps. 118:14 ; Es. 12:2.-->, il est devenu mon salut<!--Il est devenu mon Yeshuw`ah.-->. C'est mon El : je le magnifierai<!--« Embellir », « orner ».-->, c'est l'Elohîm de mon père, je l'exalterai.
15:3	YHWH, homme de guerre, YHWH est son Nom !
15:4	Il a jeté dans la mer les chars de pharaon et son armée, les meilleurs de ses officiers se sont noyés dans la Mer Rouge.
15:5	Les profondeurs les ont couverts, ils sont descendus au fond des eaux comme une pierre<!--Né. 9:11.-->.
15:6	Ta droite, YHWH, est majestueuse en force ! Ta droite, YHWH, a brisé l'ennemi<!--Ps. 77:16, 118:15-16.--> !
15:7	Tu as renversé par la grandeur de ta majesté ceux qui s'élevaient contre toi. Tu as envoyé ta colère et elle les a dévorés comme du chaume.
15:8	Au souffle de tes narines, les eaux se sont amoncelées, celles qui coulaient se sont dressées comme une digue, les abîmes se sont figés au cœur de la mer.
15:9	L'ennemi disait : Je poursuivrai, j'atteindrai, je partagerai le butin, mon âme en sera pleine, je tirerai mon épée, ma main en héritera !
15:10	Tu as soufflé de ton vent, la mer les a couverts ! Ils se sont enfoncés comme du plomb au plus profond des eaux.
15:11	Qui est comme toi parmi les forts, YHWH ! Qui est comme toi, majestueux en sainteté, redoutable, digne de louanges, faisant des choses merveilleuses ?
15:12	Tu as étendu ta droite, la terre les a engloutis.
15:13	Tu as conduit par ta miséricorde ce peuple que tu as racheté. Tu l'as conduit par ta force à la demeure de ta sainteté.
15:14	Les peuples l'ont entendu et ils en ont tremblé : la douleur a saisi les habitants de la terre des Philistins.
15:15	Alors les princes d'Édom seront terrifiés, et le tremblement saisira les puissants de Moab, tous les habitants de Kena'ân se fondront.
15:16	La terreur et la crainte tomberont sur eux. Ils deviendront muets comme une pierre par la grandeur de ton bras, jusqu'à ce que ton peuple soit passé, YHWH ! jusqu'à ce que ce peuple que tu as acquis soit passé<!--De. 2:25, 11:25 ; Jos. 2:9.-->.
15:17	Tu les introduiras et les planteras sur la montagne de ton héritage, au lieu dont tu as fait ta demeure, YHWH ! au sanctuaire, Adonaï, que tes mains ont établi !
15:18	YHWH régnera à jamais et à perpétuité.
15:19	Car les chevaux de pharaon, ses chars et ses cavaliers sont entrés dans la mer, et YHWH a fait retourner sur eux les eaux de la mer, mais les fils d'Israël ont marché à sec au milieu de la mer.
15:20	Et Myriam, la prophétesse, sœur d'Aaron, prit à la main un tambourin, et toutes les femmes sortirent derrière elle, avec des tambourins et des danses.
15:21	Et Myriam leur répondait : Chantez à YHWH car il s'est levé, il s'est levé : il a jeté dans la mer le cheval et celui qui le montait.

### YHWH pourvoit pour son peuple

15:22	Moshé fit partir Israël de la Mer Rouge et ils sortirent vers le désert de Shour. Après 3 jours de marche dans le désert, ils ne trouvèrent pas d'eau.
15:23	Ils vinrent à Marah, mais ne purent boire l'eau de Marah parce qu'elle était amère. C'est pourquoi son nom fut appelé Marah.
15:24	Le peuple murmura contre Moshé en disant : Que boirons-nous ?
15:25	Il cria à YHWH, et YHWH lui montra<!--« Montra » de l'hébreu « yarah » qui veut également dire « enseigner », « signaler », « lancer », « instruire », « informer », « montrer », « jeter » etc.--> un arbre, qu’il jeta dans l’eau, et l’eau devint douce. C'est là qu'il lui fixa une ordonnance et un jugement. C'est là qu'il l'éprouva.
15:26	Il dit : Si tu écoutes, si tu écoutes la voix de YHWH, ton Elohîm, si tu fais ce qui est droit à ses yeux, si tu prêtes l'oreille à ses commandements, si tu gardes toutes ses ordonnances, je ne mettrai sur toi aucune des maladies que j’ai mises sur l'Égypte, car je suis YHWH-Rapha<!--YHWH qui te guérit. De. 7:12-15.-->.
15:27	Ils vinrent à Élim où il y avait 12 sources d'eau et 70 palmiers. Ils campèrent là, près de l'eau.

## Chapitre 16

### YHWH envoie la manne

16:1	Toute l'assemblée des fils d'Israël étant partie d'Élim, vint dans le désert de Sin, qui est entre Élim et Sinaï, le quinzième jour du second mois après qu'ils furent sortis de la terre d'Égypte.
16:2	Toute l'assemblée des fils d'Israël murmura dans ce désert contre Moshé et Aaron.
16:3	Les fils d'Israël leur dirent : Pourquoi ne nous a-t-il pas été permis de mourir par la main de YHWH en terre d'Égypte, assis près des pots de viande et mangeant du pain à satiété ? Oui, vous nous avez fait sortir vers ce désert pour faire mourir de faim toute cette assemblée<!--No. 11:4 ; 1 Co. 10:10.-->.
16:4	YHWH dit à Moshé : Voici que je fais pleuvoir des cieux du pain pour vous. Le peuple sortira et recueillera la parole du jour en son jour, pour que je l’éprouve : Marchera-t-il dans ma torah ou non ?
16:5	Et il arrivera que, le sixième jour, ils prépareront ce qu’ils auront rapporté, et ce sera le double de ce qu’ils recueilleront au jour le jour.
16:6	Moshé et Aaron dirent à tous les fils d'Israël : Ce soir vous saurez que YHWH vous a fait sortir de la terre d'Égypte.
16:7	Et au matin vous verrez la gloire de YHWH, parce qu'il a entendu vos murmures, qui sont contre YHWH. Car qui sommes-nous pour que vous murmuriez contre nous ?
16:8	Et Moshé dit : YHWH vous donnera ce soir de la viande à manger et, au matin, du pain à satiété, parce que YHWH entend vos murmures que vous murmurez contre lui. Car que sommes-nous ? Vos murmures ne sont pas contre nous, mais contre YHWH.
16:9	Moshé dit à Aaron : Dis à toute l'assemblée des fils d'Israël : Approchez-vous en face de YHWH, car il a entendu vos murmures.
16:10	Or il arriva qu'aussitôt qu'Aaron eut parlé à toute l'assemblée des fils d'Israël, ils regardèrent vers le désert, et voici, la gloire de YHWH se fit voir dans la nuée.
16:11	Et YHWH parla à Moshé, en disant :
16:12	J'ai entendu les murmures des fils d'Israël. Parle-leur et dis-leur : Entre les deux soirs, vous mangerez de la chair, et au matin vous serez rassasiés de pain. Vous saurez que je suis YHWH votre Elohîm.
16:13	Et il arriva, le soir, que des cailles montèrent et couvrirent le camp, et au matin, il y avait une couche de rosée tout autour du camp.
16:14	Quand cette couche de rosée se leva, voici, sur les faces du désert une chose petite et ronde, aussi petite que la gelée blanche sur la terre.
16:15	Quand les fils d'Israël la virent, chaque homme dit à son frère : Qu'est-ce que c'est ? Car ils ne savaient ce que c'était. Et Moshé leur dit : C'est le pain que YHWH vous donne pour nourriture<!--Ps. 105:40.-->.

### Récolte de la manne

16:16	Voici la parole que YHWH a ordonnée : Recueillez-en, chaque homme ce qu'il lui faut pour sa nourriture, un omer par crâne, selon le nombre de vos personnes. Chaque homme en prendra pour ceux qui sont dans sa tente.
16:17	Les fils d'Israël firent ainsi, et les uns en recueillirent plus, les autres moins.
16:18	Ils le mesuraient par omer. Celui qui en recueillait beaucoup n'avait rien de trop, et celui qui en recueillait peu, n'en manquait pas. Chaque homme recueillait ce qu'il lui fallait pour sa nourriture.
16:19	Moshé leur dit : Qu'aucun homme n’en laisse jusqu’au matin !
16:20	Ils n'obéirent pas à Moshé, et des hommes en laissèrent jusqu'au matin, mais les vers s'y mirent et cela sentait mauvais. Et Moshé se fâcha contre eux.
16:21	Matin après matin, chaque homme recueillait ce qu'il lui fallait pour sa nourriture, et lorsque la chaleur du soleil était venue, elle se fondait.
16:22	Et il arriva, qu'au sixième jour, ils recueillirent du pain en double, 2 omers pour chacun. Tous les princes de l'assemblée vinrent en informer Moshé.

### Le shabbat<!--Né. 9:13-14 ; Mt. 12:1.-->

16:23	Il leur dit : C'est ce que YHWH a dit : Demain est le shabatôn<!--Observation du shabbat, le « sabbatisme ».-->, le shabbat consacré à YHWH. Faites cuire ce que vous avez à cuire, faites bouillir ce que vous avez à bouillir, et tout ce qu'il y aura en surplus, mettez-le en réserve pour le garder jusqu'au matin.
16:24	Ils le mirent en réserve, comme Moshé l'avait ordonné, et il ne sentit pas mauvais, et il n'y eut pas de vers dedans.
16:25	Moshé dit : Mangez-le aujourd'hui, car c'est aujourd'hui le shabbat pour YHWH. Aujourd'hui vous n'en trouverez pas dans les champs.
16:26	Durant 6 jours vous le recueillerez, mais le septième jour est le shabbat, il n'y en aura pas.
16:27	Il arriva que le septième jour quelques-uns du peuples sortirent pour recueillirent, mais ils ne trouvèrent rien.
16:28	YHWH dit à Moshé : Jusqu'à quand refuserez-vous de garder mes commandements et ma torah ?
16:29	Considérez que YHWH vous a donné le shabbat, c'est pourquoi il vous donne au sixième jour du pain pour deux jours. Que chaque homme reste à sa place et qu'aucun homme ne sorte du lieu où il est le septième jour<!--Voir commentaire en Mt. 24:20.-->.
16:30	Le peuple se reposa le septième jour.
16:31	La maison d'Israël appelait cela du nom de manne<!--Le mot « manne » vient de l'hébreu « man » et veut dire « Qu'est-ce que cela ? ». La manne est une image de Yéhoshoua (Jésus) le Pain de vie descendu du ciel (Jn. 6:32-52). La consommation quotidienne du Pain de vie, qui est aussi la parole d'Elohîm, apporte la vie éternelle.-->. Elle était comme de la semence de coriandre blanche et ayant le goût d'un gâteau au miel.
16:32	Moshé dit : Ceci est la parole que YHWH a ordonnée : Qu'on en remplisse un omer en dépôt pour vos générations, afin qu'on voie le pain que je vous ai fait manger dans le désert, après vous avoir fait sortir de la terre d'Égypte.
16:33	Moshé dit à Aaron : Prends un vase et mets-y un plein d'omer de manne, et dépose-le en face de YHWH, afin qu'il soit préservé pour vos générations.
16:34	Comme YHWH l'avait ordonné à Moshé, Aaron le déposa en face du témoignage pour qu'il y soit préservé.
16:35	Les fils d'Israël mangèrent la manne durant 40 ans, jusqu'à leur arrivée dans une terre habitée. Ils mangèrent la manne, jusqu'à leur arrivée aux frontières de la terre de Kena'ân.
16:36	Or un omer est la dixième partie d'un épha.

## Chapitre 17

### Miracle de l'eau qui sort du rocher

17:1	Toute l'assemblée des fils d'Israël partit du désert de Sin, selon les marches que YHWH leur avait ordonnées. Ils campèrent à Rephidim, où il n'y avait pas d'eau à boire pour le peuple.
17:2	Le peuple contesta avec Moshé. Et ils lui dirent : Donnez-nous de l'eau à boire. Et Moshé leur dit : Pourquoi contestez-vous avec moi ? Pourquoi tentez-vous YHWH<!--No. 20:2-5.--> ?
17:3	Là, le peuple eut soif d'eau. Le peuple murmura contre Moshé et dit : Pourquoi cela, nous as-tu fait monter d'Égypte pour me faire mourir de soif avec mes fils et mes troupeaux ?
17:4	Moshé cria à YHWH, en disant : Que ferai-je à ce peuple ? Encore un peu et ils me lapideront.
17:5	YHWH dit à Moshé : Passe devant le peuple et prends avec toi des anciens d'Israël, prends aussi dans ta main le bâton avec lequel tu as frappé le fleuve, et viens !
17:6	Voici, je vais me tenir là devant toi sur le rocher d'Horeb. Tu frapperas le rocher, il en sortira des eaux et le peuple en boira. Moshé fit ainsi aux yeux des anciens d'Israël<!--De. 9:8 ; Ps. 78:15 ; 1 Co. 10:4.-->.
17:7	Il appela ce lieu du nom de Massah et Meriybah, à cause de la querelle des fils d'Israël, et parce qu'ils avaient tenté YHWH, en disant : YHWH est-il au milieu de nous ou non ?

### Bataille et victoire contre Amalek

17:8	Amalek vint et livra bataille contre Israël à Rephidim<!--De. 25:17-18.-->.
17:9	Moshé dit à Yéhoshoua : Choisis-nous des hommes et sors pour combattre contre Amalek. Je me tiendrai demain sur le sommet de la colline, et le bâton d'Elohîm sera dans ma main.
17:10	Yéhoshoua fit comme Moshé lui avait ordonné en combattant contre Amalek. Mais Moshé, Aaron et Hour montèrent au sommet de la colline.
17:11	Il arrivait que lorsque Moshé élevait sa main, Israël était le plus fort, mais lorsqu’il reposait sa main, Amalek était le plus fort.
17:12	Les mains de Moshé étant devenues pesantes, ils prirent une pierre et la mirent sous lui, et il s'assit dessus. Aaron et Hour soutenaient ses mains, l'un d'un côté, et l'autre de l'autre côté. Ainsi, ses mains furent fermes jusqu'au coucher du soleil.
17:13	Yéhoshoua affaiblit Amalek et son peuple à bouche d’épée.
17:14	YHWH dit à Moshé : Écris cela dans un livre pour en garder le souvenir et mets-le aux oreilles de Yéhoshoua, car j'effacerai, j'effacerai la mémoire d'Amalek de dessous les cieux.
17:15	Moshé bâtit un autel et l'appela du nom de : YHWH-Nissi<!--« YHWH est ma bannière ». C'est le nom donné par Moshé à l'autel qu'il construisit pour célébrer la défaite d'Amalek. En No. 21:8-9, Moshé éleva une bannière sur laquelle il avait fixé un serpent en cuivre pour la guérison des malades.-->.
17:16	Il dit aussi : Parce qu’une main est contre le trône de Yah, YHWH est en guerre contre Amalek d'âge en âge.

## Chapitre 18

### Yithro (Jéthro) conseille Moshé

18:1	Or Yithro, prêtre de Madian, beau-père de Moshé, apprit toutes les choses que YHWH avait faites pour Moshé et pour Israël, son peuple : comment YHWH avait fait sortir Israël de l'Égypte.
18:2	Yithro, beau-père de Moshé, prit Tsipporah la femme de Moshé, après qu'il l'eut renvoyée,
18:3	et les deux fils de cette femme, dont l'un s'appelait Guershom, car il avait dit : Je suis un étranger sur une terre étrangère,
18:4	et l'autre Éliy`ezer, car il avait dit : L'Elohîm de mon père m'a secouru et m'a délivré de l'épée de pharaon.
18:5	Yithro, beau-père de Moshé, vint vers Moshé avec ses fils et sa femme dans le désert, où il était campé, à la montagne d'Elohîm.
18:6	Il fit dire à Moshé : Yithro ton beau-père, vient vers toi, avec ta femme et ses deux fils avec elle.
18:7	Moshé sortit à la rencontre de son beau-père. Il se prosterna et l'embrassa. Ils se questionnèrent l’homme et son compagnon, sur la paix, puis ils entrèrent dans la tente.
18:8	Moshé raconta à son beau-père tout ce que YHWH avait fait à pharaon et aux Égyptiens à cause d'Israël, toute la détresse qui les avait atteints en chemin, et pourtant YHWH les délivrait.
18:9	Yithro se réjouit de tout le bien que YHWH avait fait à Israël, parce qu'il les avait délivrés de la main des Égyptiens.
18:10	Yithro dit : Béni soit YHWH qui vous a délivrés de la main des Égyptiens et de la main de pharaon, qui a délivré le peuple de la main des Égyptiens !
18:11	Maintenant je sais que YHWH est plus grand que tous les elohîm, car dans cette affaire où ils ont agi avec orgueil, il a eu le dessus sur eux.
18:12	Yithro, beau-père de Moshé, prit un holocauste et des sacrifices pour Elohîm. Et Aaron et tous les anciens d'Israël vinrent pour manger du pain avec le beau-père de Moshé, dans la présence d'Elohîm.
18:13	Et il arriva, le lendemain, comme Moshé siégeait pour juger le peuple, et que le peuple se tenait devant Moshé depuis le matin jusqu'au soir,
18:14	que le beau-père de Moshé vit tout ce qu'il faisait au peuple, et il lui dit : Qu'est-ce que tu fais à l'égard de ce peuple ? Pourquoi es-tu assis seul, et tout le peuple se tient devant toi depuis le matin jusqu'au soir ?
18:15	Et Moshé dit à son beau-père : Oui, le peuple vient à moi pour consulter Elohîm.
18:16	Quand ils ont quelque affaire, ils viennent vers moi. Je juge l'homme et son compagnon et je leur fais connaître les ordonnances d'Elohîm et sa torah.
18:17	Le beau-père de Moshé lui dit : La chose que tu fais n'est pas bonne.
18:18	Tu te faneras, tu te faneras, toi et ce peuple qui est avec toi. En effet, la chose est trop lourde pour toi, tu ne pourras l'accomplir tout seul.
18:19	Maintenant, écoute ma voix ! Je te donne un conseil et qu'Elohîm soit avec toi ! Sois pour ce peuple devant Elohîm et porte toi-même les affaires devant Elohîm.
18:20	Enseigne-leur les ordonnances et les torah, fais-leur connaître la voie par laquelle ils marcheront et l’œuvre qu’ils feront.
18:21	Regarde parmi tout le peuple des hommes talentueux, craignant Elohîm, des hommes de vérité, haïssant le gain injuste. Établis-les chefs de milliers, chefs de centaines, chefs de cinquantaines et chefs de dizaines.
18:22	Qu'ils jugent le peuple en tout temps : et il arrivera qu’ils porteront devant toi toutes les grandes affaires et jugeront eux-mêmes toutes les petites affaires. Allège ta charge, et qu'ils la portent avec toi !
18:23	Si tu fais cette chose, et qu'Elohîm te l'ordonne, tu pourras tenir, et tout ce peuple aussi arrivera en paix en son lieu.
18:24	Moshé obéit à la parole de son beau-père et fit tout ce qu'il lui avait dit.
18:25	Moshé choisit parmi tout Israël des hommes talentueux et les établit chefs sur le peuple, chefs de milliers, chefs de centaines, chefs de cinquantaines et chefs de dizaines.
18:26	Ils jugeaient le peuple en tout temps. Ils portaient devant Moshé les affaires difficiles et toutes les petites affaires ils les jugeaient eux-mêmes.
18:27	Moshé laissa aller son beau-père, qui s'en alla pour lui vers sa terre.

## Chapitre 19

### DÉBUT DE LA PÉRIODE DE LA LOI MOSAÏQUE OU DE LA PREMIÈRE ALLIANCE

19:1	Le troisième mois après que les fils d'Israël furent sortis de la terre d'Égypte, en ce même jour-là, ils vinrent dans le désert de Sinaï.
19:2	Étant partis de Rephidim, ils vinrent dans le désert de Sinaï et campèrent dans le désert. Et Israël campa vis-à-vis de la montagne.
19:3	Et Moshé monta vers Elohîm, car YHWH l'avait appelé de la montagne pour lui dire : Tu parleras ainsi à la maison de Yaacov et tu annonceras ceci aux fils d'Israël :
19:4	Vous avez vu ce que j'ai fait aux Égyptiens et comment je vous ai portés sur des ailes d'aigle et vous ai amenés à moi.
19:5	Maintenant, si vous obéissez, si vous obéissez à ma voix et si vous gardez mon alliance, vous deviendrez ma propriété parmi tous les peuples, car toute la Terre est à moi<!--C'est ici que débute l'âge de la loi. C'est inexact d'appeler les textes de Bereshit (Genèse) à Malakhi (Malachie) « Ancien Testament ». Tout d'abord, l'emploi du mot « testament » est inapproprié puisqu'on ne peut parler de testament sans qu'il n'y ait eu au préalable la mort du testateur (Hé. 9:16-17). Certes, des animaux étaient tués sous la torah pour couvrir les péchés. Toutefois, ces sacrifices étaient imparfaits et par conséquent prévus pour ne durer qu'un temps, en attendant le sacrifice parfait de Yéhoshoua ha Mashiah (Jésus-Christ) (Hé. 10:1-14). De plus, il est évident que les animaux sacrifiés ne nous ont rien légué.\\ Ensuite, il est à noter que tous les textes classés dans ce que l'on appelle à tort « Ancien Testament » ne se rapportent pas exclusivement et nécessairement à la loi. Ainsi, des prophètes, en commençant par Moshé en personne, ayant vécu sous la torah, ont prophétisé et écrit sur d'autres sujets que la loi, notamment sur la grâce et la fin des temps. N'oublions pas non plus que Yéhoshoua ha Mashiah est né et a vécu sous la torah (Ga. 4:4). En tant que Juif, il l'a scrupuleusement respectée de telle sorte qu'elle fut totalement accomplie en lui (Mt. 5:17-18 ; Jn. 19:30). En conséquence, la fin de la loi mosaïque eut lieu après la mort du Seigneur, précisément au moment où le Seigneur a dit « C'est accompli », et lorsque le voile du temple s'est déchiré de haut en bas (Mt. 27:50-51 ; Jn. 19:30). La nouvelle alliance ou le Testament de Yéhoshoua débuta avec l'effusion de l'Esprit (Ac. 2). De la mort du Seigneur à la pentecôte, une période de transition de 50 jours s'est écoulée. Yéhoshoua ha Mashiah s'est rendu pendant ce temps dans le sanctuaire céleste pour présenter son sang dans le Saint des saints. Une fois son sacrifice examiné et accepté, le Saint-Esprit qui avait été retiré de l'être humain (Ge. 6:3) put de nouveau revenir habiter le cœur des croyants.\\Mais qu'est-ce que la loi exactement ? Beaucoup de chrétiens sont dans la confusion à ce sujet. En réalité, il n'y avait pas qu'une loi mais trois sortes de lois : Les lois morales et les lois cérémonielles qui préexistaient depuis l'éternité ; et les lois civiles qui ont débuté avec Moshé car elles ne concernaient que son peuple.\\- Les lois civiles régissaient le fonctionnement de la vie en communauté des Hébreux. Elles étaient exclusivement réservées au peuple d'Israël dans le camp puis en terre de Kena'ân (Canaan) (Ex. 21:1-2 ; De. 23).\\- Les lois morales font référence à la nature d'Elohîm : Son amour, sa justice, sa sainteté, etc. Les dix commandements, à l'exception du shabbat tel que prescrit par Moshé (Ex. 16:28-29 ; Lé. 15:32), font partie des lois morales (Ex. 20:1-17). Les dix paroles ne constituent qu'une base, un résumé. Ainsi, d'autres règles morales sont énoncées tout au long des Écritures notamment sur la sexualité (Lé. 18:1-22), l'interdiction des sacrifices humains et de l'occultisme (De. 18:10-13), le respect d'autrui et l'entraide (Lé. 19:10-18, 29-36). Comme il est impossible de consigner dans un livre tous les péchés moraux, le Seigneur a inscrit les lois morales dans le cœur de l'être humain afin qu'il sache instinctivement faire la différence entre le bien et mal (Ro. 2:14-15). Yéhoshoua les a résumées en ces quelques mots : « Tu aimeras le Seigneur ton Elohîm, de tout ton cœur, de toute ton âme et de toute ta pensée. C'est là le premier et le grand commandement. Et voici le deuxième qui lui est semblable : Tu aimeras ton prochain comme toi-même. » (Mt. 22:37-39). Ces lois sont encore en vigueur aujourd'hui et le resteront pour toujours.\\- Les lois cérémonielles étaient relatives au culte et au sanctuaire terrestre, c'est-à-dire le tabernacle puis le temple de Yeroushalaim (Jérusalem) (Hé. 9:1-10). Elles regroupent toutes les ordonnances concernant les sacrifices, les ablutions, les shabbats, les fêtes de YHWH, la dîme des Lévites et des prêtres (voir commentaire en No. 18:21 et Mal. 3:10). Les livres de Lévitique (Vayiqra) et de Nombres (Bamidbar) exposent en détail toutes les ordonnances reçues par Moshé d'après le modèle céleste que YHWH lui avait montré sur le Mont Sinaï (Ex. 26:30). Les lois cérémonielles préexistaient donc depuis l'éternité.\\Les lois cérémonielles représentent la première alliance qui avait pour fondement la loi morale. Or cette alliance a vieilli puis disparu, car elle n'était que l'ombre des choses à venir (Col. 2:17 ; Hé. 8:13). En effet, elle était basée sur quatre points principaux : le temple, le culte centralisé, le sacrifice et les prêtres. En Mashiah, nous n'avons plus besoin d'un temple physique puisque nous sommes devenus les temples vivants de YHWH (1 Co. 6:19 ; Ep. 2:22). Nous pouvons désormais adorer le Seigneur en Esprit et en vérité, à tout moment et en tout lieu (Jn. 4:23). La prêtrise lévitique ayant été abolie, chaque enfant d'Elohîm est devenu un prêtre (Ap. 5:10) qui offre en sacrifice sa propre vie consacrée au Seigneur (Ro. 12:1).\\Les lois cérémonielles ont donc trouvé leur parfait accomplissement en Yéhoshoua ha Mashiah : tous les sacrifices sanglants le préfiguraient, toutes les solennités ont été réalisées en Lui (voir note en Lé. 23). Le Mashiah est donc la fin de la loi, non pas morale, mais cérémonielle (Ro. 10:4).\\Un lien étroit existe entre les lois morales et les lois cérémonielles. La loi morale est comme un diagnostic qui révèle une pathologie incurable comme le sida : Le péché (Ro. 5:13-20, 7:7-14). En la découvrant, l'être humain se sent condamné, car il réalise qu'il ne peut pas répondre aux exigences de la justice divine. La loi cérémonielle (le sang des animaux – Hé. 9:1-13, 10:11) a donné aux humains une sorte de trithérapie pour les soulager provisoirement de leurs péchés mais sans pour autant les ôter (guérir, délivrer, nettoyer, laver) définitivement. Seul le sang de la nouvelle alliance, c'est-à-dire le sang de Yéhoshoua ha Mashiah, a pu nous délivrer une fois pour toutes (Jn. 1:29 ; Hé. 9:11-26, 10:1-23 ; Ap. 1:6).-->.
19:6	Vous deviendrez pour moi un royaume de prêtres et une nation sainte. Voilà les paroles que tu diras aux fils d'Israël.
19:7	Moshé vint et appela les anciens du peuple et mit devant eux toutes ces paroles que YHWH lui avait ordonnées.
19:8	Tout le peuple ensemble répondit en disant : Nous ferons tout ce que YHWH a dit. Et Moshé rapporta les paroles du peuple à YHWH.

### Moshé doit sanctifier le peuple pour qu'il rencontre YHWH

19:9	YHWH dit à Moshé : Voici, je viendrai à toi dans une sombre nuée afin que le peuple entende quand je parlerai avec toi et qu'ils aient foi en toi aussi pour toujours. Moshé rapporta les paroles du peuple à YHWH.
19:10	YHWH dit à Moshé : Va vers le peuple. Sanctifie-le aujourd'hui et demain, et qu'ils lavent leurs vêtements.
19:11	Qu'ils soient tous prêts pour le troisième jour, car le troisième jour, YHWH descendra sur la Montagne de Sinaï aux yeux de tout le peuple.
19:12	Tu fixeras au peuple des limites tout autour et tu diras : Gardez-vous de monter sur la montagne et d’en toucher l’extrémité. Quiconque touchera la montagne, mourra, il mourra.
19:13	On ne mettra pas la main sur lui, mais il sera lapidé, il sera lapidé ou percé, percé de flèches. Soit bête, soit homme, il ne vivra pas. Quand la corne<!--Ce mot vient de l'hébreu « yowbel » qui signifie « bélier », « corne de bélier » ou « le Jubilé » (voir Lé. 25:10-15,28,30-33,40,50-52,54 ; Lé. 27:17-24 ; No. 36:4 ; Jos. 6:4-13).--> de bélier sonnera, ils monteront vers la montagne.
19:14	Moshé descendit de la montagne vers le peuple et sanctifia le peuple, et ils lavèrent leurs vêtements.
19:15	Il dit au peuple : Soyez tous prêts pour le troisième jour et ne vous approchez pas de vos femmes.
19:16	Et il arriva, le troisième jour, quand le matin fut venu, qu’il y eut des voix, des éclairs et une grosse nuée sur la montagne, avec un très fort son de shofar, et tout le peuple dans le camp fut effrayé.
19:17	Moshé fit sortir le peuple du camp pour aller au-devant d'Elohîm, et ils s'arrêtèrent au pied de la montagne.
19:18	La montagne de Sinaï était toute fumante, face à YHWH qui y était descendu dans le feu. Sa fumée montait comme la fumée d'une fournaise. Toute la montagne tremblait beaucoup.
19:19	Il arriva que comme le son du shofar se renforçait de plus en plus, Moshé parlait, et Elohîm lui répondait par une voix.
19:20	YHWH descendit sur la Montagne de Sinaï, au sommet de la montagne. YHWH appela Moshé au sommet de la montagne et Moshé y monta.
19:21	YHWH dit à Moshé : Descends et avertis le peuple de ne pas se précipiter vers YHWH afin de regarder, de peur qu'un grand nombre d'entre eux ne périsse.
19:22	Que les prêtres qui s'approchent de YHWH se sanctifient aussi, de peur que YHWH ne les brise.
19:23	Moshé dit à YHWH : Le peuple ne pourra pas monter sur la Montagne de Sinaï, car tu nous as toi-même avertis en disant : Fixe des limites autour de la montagne et sanctifie-la.
19:24	YHWH lui dit : Va, descends et tu monteras, toi et Aaron avec toi. Quant aux prêtres et au peuple, qu’ils ne se précipitent pas pour monter vers YHWH, de peur qu’il ne les brise.
19:25	Moshé descendit vers le peuple et lui dit ces choses.

## Chapitre 20

### Les dix paroles

20:1	Elohîm déclara toutes ces paroles en disant :
20:2	Je suis YHWH ton Elohîm, qui t'ai fait sortir de la terre d'Égypte, de la maison des esclaves.
20:3	Tu n'auras pas d'autres elohîm contre mes faces.
20:4	Tu ne te feras pas d’idole, ni une image des choses qui sont là-haut dans les cieux, ni ici-bas sur la Terre, ni dans les eaux sous la terre<!--Lé. 26:1.-->.
20:5	Tu ne te prosterneras pas devant elles et tu ne les serviras pas, car je suis YHWH ton Elohîm, le El jaloux, qui punis l'iniquité des pères sur les fils, sur la troisième et sur la quatrième génération<!--Une génération biblique est de 40 ans au minimum (No. 32:13 ; Ps. 95:10 ; 1 Ch. 29:26-28 ; Ac. 7:23,30,36,42 ; Hé. 3:9-11).--> de ceux qui me haïssent,
20:6	et qui use de bonté envers des milliers de ceux qui m'aiment et qui gardent mes commandements.
20:7	Tu ne prendras pas en vain le Nom de YHWH ton Elohîm, car YHWH ne tiendra pas pour innocent celui qui aura pris en vain son Nom<!--Lé. 19:12 ; Mt. 5:33.-->.
20:8	Souviens-toi du jour du shabbat pour le sanctifier.
20:9	Tu travailleras 6 jours, et tu feras toute ton œuvre.
20:10	Mais le septième jour est le shabbat pour YHWH ton Elohîm, tu ne feras aucune œuvre, ni toi, ni ton fils, ni ta fille, ni ton serviteur, ni ta servante, ni ton bétail, ni l'étranger qui est dans tes portes.
20:11	Car en 6 jours YHWH a fait les cieux, la Terre, la mer et tout ce qui est en eux, et s'est reposé le septième jour. C'est pourquoi YHWH a béni le jour du shabbat et l'a sanctifié<!--Ge. 2:3 ; Ex. 31:14 ; Ez. 20:12.-->.
20:12	Honore ton père et ta mère, afin que tes jours soient prolongés sur le sol que YHWH ton Elohîm te donne<!--Lé. 19:3 ; De. 5:16 ; Mt. 15:4 ; Ep. 6:2.-->.
20:13	Tu n'assassineras pas<!--Mt. 5:21.-->.
20:14	Tu ne commettras pas d'adultère<!--Lé. 20:10 ; De. 5:18 ; Pr. 6:32 ; Mt. 5:32 ; Ro. 7:3.-->.
20:15	Tu ne voleras pas.
20:16	Tu ne témoigneras pas contre ton prochain en faux témoin.
20:17	Tu ne convoiteras pas la maison de ton prochain. Tu ne convoiteras pas la femme de ton prochain, ni son serviteur, ni sa servante, ni son bœuf, ni son âne, ni aucune chose qui soit à ton prochain.

### Le peuple tout tremblant devant YHWH

20:18	Tout le peuple voyait les voix, les torches, la voix du shofar et la montagne fumante. Lorsque le peuple vit cela, il tremblait et se tenait loin.
20:19	Ils dirent à Moshé : Parle, toi avec nous et nous écouterons, mais qu'Elohîm ne parle pas avec nous, de peur que nous ne mourions<!--De. 5:23-24 ; Hé. 12:18-19.-->.
20:20	Moshé dit au peuple : N'ayez pas peur ! Car Elohîm est venu pour vous éprouver, afin que sa crainte soit devant vous et que vous ne péchiez pas.
20:21	Le peuple se tint loin, mais Moshé s'approcha des ténèbres épaisses où était Elohîm.
20:22	Et YHWH dit à Moshé : Tu parleras ainsi aux fils d'Israël : Vous avez vu que je vous ai parlé depuis les cieux.
20:23	Vous ne ferez pas à côté de moi des elohîm en argent ni des elohîm en or. Vous n'en ferez pas pour vous.
20:24	Tu me feras un autel du sol, sur lequel tu sacrifieras tes holocaustes et tes offrandes de paix<!--Voir commentaire en Lé. 3:1.-->, ton petit et ton gros bétail. En quelque lieu que ce soit où je rappellerai mon Nom, je viendrai à toi et je te bénirai.
20:25	Si tu me fais un autel de pierres, tu ne le bâtiras pas en pierres taillées, car en passant ton outil sur la pierre, tu la profanerais.
20:26	Et tu ne monteras pas à mon autel par des marches, de peur que ta nudité ne soit découverte.

## Chapitre 21

### Torah sur les maîtres et leurs esclaves

21:1	Voici les ordonnances que tu mettras en face d'eux.
21:2	Si tu achètes un esclave hébreu, il te servira six années, mais la septième, il sortira libre, gratuitement<!--Lé. 25:39-43 ; De. 15:12 ; Jé. 34:14.-->.
21:3	S'il est venu seul, il sortira seul. S'il était le mari d'une femme, sa femme sortira aussi avec lui.
21:4	Si son seigneur lui a donné une femme qui lui ait enfanté des fils ou des filles, sa femme et les enfants qu'il aura seront à son seigneur, et il sortira seul.
21:5	Si l'esclave dit, s'il dit : J'aime mon seigneur, ma femme et mes fils, je ne veux pas sortir libre,
21:6	alors son seigneur le fera venir devant les juges et le fera approcher de la porte ou du poteau, et son seigneur lui percera l'oreille avec un poinçon et il le servira pour toujours.
21:7	Si un homme vend sa fille comme esclave, elle ne sortira pas comme les esclaves sortent.
21:8	Si elle déplaît aux yeux de son seigneur qui ne l'aura pas fiancée, il la fera racheter. Mais il n'aura pas le pouvoir de la vendre à un peuple étranger, après lui avoir été infidèle.
21:9	Mais s'il l'a fiancée à son fils, il fera pour elle selon le droit des filles.
21:10	S'il en prend une autre pour lui, il ne diminuera en rien la nourriture, les vêtements et le droit conjugal.
21:11	S'il ne fait pas pour elle ces trois choses-là, elle sortira gratuitement, sans argent.

### Torah sur les dommages corporels

21:12	Si quelqu'un frappe un homme et qu'il en meure, il mourra, il mourra<!--Lé. 24:17 ; No. 35:11-16 ; De. 19:2-11 ; Jos. 20:2.-->.
21:13	S'il ne lui a pas dressé d'embûches, mais qu'Elohîm l'ait fait tomber entre ses mains, je t'établirai un lieu où il s'enfuira.
21:14	Mais si un homme agit avec arrogance contre son prochain pour le tuer par ruse, tu l'arracheras de mon autel, afin qu'il meure.
21:15	Celui qui aura frappé son père ou sa mère mourra, il mourra<!--Lé. 20:9 ; De. 27:16 ; Mt. 15:4.-->.
21:16	Si quelqu'un enlève un homme et le vend, ou s'il est retrouvé entre ses mains, il mourra, il mourra.
21:17	Celui qui aura maudit son père ou sa mère mourra, il mourra.
21:18	Si des hommes se querellent et qu'un homme frappe son compagnon avec une pierre ou avec le poing, et que celui-ci, sans mourir, tombe sur son lit,
21:19	s'il se lève et marche dehors en s'appuyant sur son bâton, celui qui l'aura frappé sera absous. Toutefois, il le dédommagera de son interruption de travail et il le guérira, il le guérira.
21:20	Si quelqu'un a frappé du bâton son serviteur ou sa servante, et qu'il soit mort sous sa main, on le vengera, on le vengera.
21:21	Mais s’il reste debout un jour ou deux, il ne sera pas vengé, car c'est son argent.
21:22	Si des hommes se querellent, et que l'un d'eux frappe une femme enceinte, et qu'elle en accouche, s'il n'y a pas cas de mort, il sera condamné, condamné à l'amende que le mari de la femme lui imposera, et il la donnera selon l'estimation des juges.
21:23	Mais si malheur arrive, tu donneras âme pour âme,
21:24	œil pour œil, dent pour dent, main pour main, pied pour pied<!--Lé. 24:20 ; De. 19:21 ; Mt. 5:38.-->,
21:25	brûlure pour brûlure, plaie pour plaie, meurtrissure pour meurtrissure.
21:26	Si un homme frappe l'œil de son esclave ou de sa servante et lui fait perdre son œil, il le renverra libre, pour son œil.
21:27	Et s'il fait tomber une dent à son serviteur ou à sa servante, il le laissera aller libre pour sa dent.
21:28	Si un bœuf heurte un homme ou une femme, et qu’ils en meurent, le bœuf sera lapidé, il sera lapidé et l'on ne mangera pas sa chair, mais le maître du bœuf sera jugé innocent.
21:29	Si le bœuf était d'hier et d'avant-hier sujet à donner des coups de corne, et que son maître en ait été averti avec protestation, et qu'il ne l'ait pas surveillé, s'il tue un homme ou une femme, le bœuf sera lapidé et on fera aussi mourir son maître.
21:30	Si on lui impose un prix pour se racheter, il donnera la rançon de son âme, selon tout ce qui lui sera imposé.
21:31	Qu’il heurte un fils ou qu’il heurte une fille, on le traitera selon cette ordonnance.
21:32	Si le bœuf heurte un esclave, soit homme, soit femme, on donnera 30 sicles d'argent au maître de l'esclave, et le bœuf sera lapidé.
21:33	Si un homme découvre une fosse ou si un homme creuse une fosse, et ne la couvre pas, et qu'il y tombe un bœuf ou un âne,
21:34	le maître de la fosse paiera et rendra l'argent à son maître, et la bête morte sera pour lui.
21:35	Si le bœuf d'un homme blesse le bœuf de son prochain et qu'il en meure, ils vendront le bœuf vivant et en partageront l’argent ; ils partageront aussi le mort.
21:36	Ou s'il est connu que le bœuf était d'hier et d'avant-hier sujet à donner des coups de corne, et que le maître ne l'ait pas gardé, il rendra, il rendra bœuf pour bœuf et la bête morte sera pour lui.

### Torah sur les torts causés à autrui

21:37	Lorsqu'un homme vole un bœuf, ou un agneau, s'il le tue ou le vend, il restituera 5 bœufs pour le bœuf, et 4 brebis pour l'agneau.

## Chapitre 22

22:1	Si le voleur est trouvé en effraction, s’il est frappé et meurt, celui qui l'aura frappé ne sera pas coupable de son sang.
22:2	Si le soleil est levé, on sera coupable de son sang. Il fera une restitution, il fera une restitution. S'il n'a rien, il sera vendu pour son vol.
22:3	Si ce qui a été dérobé est trouvé vivant entre ses mains, soit bœuf, soit âne, soit brebis ou chèvre, il rendra le double.
22:4	Si un homme fait brouter un champ ou une vigne, et envoie son bétail et qu’il broute dans le champ d’autrui, il rendra le meilleur de son champ et le meilleur de sa vigne.
22:5	Lorsqu'un feu sort et rencontre des épines, s'il dévore du blé en gerbes ou sur pied, ou le champ, celui qui aura allumé le feu restituera, il restituera ce qui aura été brûlé.
22:6	Lorsqu'un homme donne à son prochain de l'argent ou des vases à garder, et qu'on les vole dans la maison de cet homme, et si l'on trouve le voleur, il restituera le double<!--Lé. 5:20-26.-->.
22:7	Si le voleur n’est pas trouvé, le maître de la maison se présentera devant Elohîm, s’il n’a pas mis la main sur la propriété de son prochain.
22:8	Dans toute affaire d'infidélité concernant un bœuf, un âne, une brebis, une chèvre, un vêtement ou tout objet perdu, dont quelqu'un dira lui appartenir, l’affaire des deux viendra devant Elohîm et celui qu'Elohîm aura condamné, restituera le double à son prochain.
22:9	Lorsqu'un homme donne à garder à son prochain un âne, un bœuf, un agneau ou une autre bête, et que la bête meure, s’estropie ou soit emmenée sans que personne ne l'ait vu,
22:10	le serment de YHWH interviendra entre les deux<!--Hé. 6:16.-->, pour savoir s'il n'a pas mis sa main sur la propriété de son prochain. Le maître de la bête se contentera du serment, et l'autre ne la restituera pas.
22:11	Si elle a été volée, volée chez lui, il la restituera à son maître.
22:12	Si elle a été déchirée, déchirée, il l’apportera en témoignage. Il ne restituera pas ce qui a été déchiré.
22:13	Lorsqu'un homme emprunte à son prochain une bête et qu'elle se casse un membre ou qu'elle meure en l'absence de son maître, il la restituera, il la restituera.
22:14	Si son maître est avec elle, il n'y aura pas de restitution. Si elle a été louée, cela devra rentrer dans son prix de louage.

### Diverses torahs

22:15	Si un homme séduit une vierge qui n'est pas fiancée et couche avec elle, il paiera sa dot. Il paiera sa dot et la prendra pour femme<!--De. 22:28.-->.
22:16	Si le père de la fille refuse, s'il refuse de la lui donner, il lui paiera en argent la valeur de la dot des vierges.
22:17	Tu ne laisseras pas vivre la sorcière<!--De. 18:10-11 ; Lé. 20:27.-->.
22:18	Celui qui couche avec une bête, mourra, il mourra<!--Lé. 18:23, 20:15 ; De. 27:21.-->.
22:19	Celui qui sacrifie à d'autres elohîm qu'à YHWH seul, sera dévoué par interdit<!--Lé. 17:7 ; De. 13:6-16, 17:2-5.-->.
22:20	Tu ne fouleras ni n'opprimeras l'étranger, car vous avez été étrangers en terre d'Égypte<!--Lé. 19:34.-->.
22:21	Vous n'affligerez pas la veuve ni l'orphelin<!--De. 24:17-18 ; Za. 7:10.-->.
22:22	Si vous les affligez, les affligez et qu'ils crient, qu'ils crient à moi, j'entendrai, j'entendrai leur cri de détresse.
22:23	Ma colère s'embrasera et je vous ferai mourir par l'épée. Vos femmes seront veuves et vos fils orphelins.
22:24	Si tu prêtes de l'argent à mon peuple, au pauvre qui est avec toi, tu ne deviendras pas pour lui comme un créancier, tu ne lui imposeras pas d’intérêt.
22:25	Si tu prends en gage, si tu prends en gage le vêtement de ton ami, tu le lui rendras avant que le soleil soit couché<!--De. 24:10-13.-->.
22:26	Car c'est sa seule couverture, c'est son vêtement pour couvrir sa peau. Dans quoi coucherait-il ? S'il arrive qu'il crie à moi, je l'entendrai, car je suis miséricordieux.
22:27	Tu ne maudiras pas les juges et tu ne maudiras pas le prince de ton peuple<!--Lé. 24:15-16 ; Ac. 23:4.-->.
22:28	Tu ne tarderas pas à m'offrir la plénitude de tes jus. Tu me donneras le premier-né de tes fils<!--Ex. 13:12-15 ; De. 26:2-11.-->.
22:29	Tu feras la même chose de ta vache, de ta brebis et de ta chèvre. Il sera 7 jours avec sa mère, et le huitième jour tu me le donneras.
22:30	Vous serez des hommes saints pour moi, et vous ne mangerez pas de la chair déchirée dans les champs, mais vous la jetterez aux chiens.

## Chapitre 23

### Diverses torahs (suite)

23:1	Tu ne répandras pas de rumeur mensongère. Tu ne prêteras pas la main au méchant pour être témoin d'une violence<!--Ex. 20:16 ; De. 19:16-21.-->.
23:2	Tu ne seras pas derrière la multitude pour faire le mal, et tu ne témoigneras pas dans un procès pour te détourner derrière la multitude et faire détourner.
23:3	Tu n’honoreras pas le pauvre dans son procès<!--De. 1:17.-->.
23:4	Si tu rencontres le bœuf de ton ennemi ou son âne égaré, tu le lui ramèneras, tu le lui ramèneras.
23:5	Si tu vois l'âne de celui qui te hait, abattu sous sa charge, tu t'arrêteras pour le secourir et tu l'aideras, tu l'aideras.
23:6	Tu ne pervertiras pas le droit de l'indigent, qui est au milieu de toi, dans son procès.
23:7	Tu t'éloigneras de parole fausse et tu ne feras pas mourir l'innocent et le juste, car je ne justifierai pas le méchant.
23:8	Tu ne recevras pas de pot-de-vin<!--Le mot hébreu signifie aussi « présents ».-->, car les pots-de-vin aveuglent les voyants et tordent les paroles des justes.
23:9	Tu n'opprimeras pas l'étranger. Vous-mêmes, vous connaissez l’âme de l'étranger car vous avez été étrangers en terre d'Égypte.

### Le shabbat, le repos de la terre

23:10	Pendant 6 ans tu ensemenceras ta terre et en recueilleras le revenu.
23:11	Mais la septième année, tu lui donneras du relâche et tu la mettras en jachère, afin que les pauvres de ton peuple en mangent, et que les bêtes des champs mangent ce qui restera. Tu en feras de même de ta vigne et de tes oliviers.
23:12	Tu travailleras 6 jours, mais tu te reposeras au septième jour, afin que ton bœuf et ton âne se reposent, et que le fils de ta servante et l'étranger reprennent leur souffle.
23:13	Vous prendrez garde à toutes les choses que je vous ai ordonnées. Vous ne ferez pas mention du nom des elohîm étrangers, on ne l'entendra pas de ta bouche<!--Jos. 23:7 ; Ps. 16:4.-->.

### Les fêtes

23:14	Trois fois par année, tu me célébreras une fête<!--Lé. 23:4-44.-->.
23:15	Tu garderas la fête<!--Le mot hébreu signifie « festival », « festin », « rassemblement pour un festival », « pèlerin ».--> des pains sans levain<!--Ex. 29:2.-->. Tu mangeras des pains sans levain pendant 7 jours, comme je t'ai ordonné, au temps fixé<!--Voir Ge. 1:14.--> du mois d’Abib<!--Nisan (ou épis), mois de l'exode et de la Pâque (Mars-Avril). Voir Ex. 13:4 ; De. 16:1.-->, car c'est en ce mois-là que tu es sorti d'Égypte. Mes faces ne se verront pas à vide.
23:16	Et la fête<!--Ex. 23:1, 23:18, 32:5, 34:22.--> de la moisson, celle des prémices de ton travail, de ce que tu auras semé dans les champs, ainsi que la fête de la récolte, après la fin de l'année, quand tu auras recueilli des champs le produit de ton travail<!--Ex. 34:22.-->.
23:17	Trois fois l’année, tous les mâles seront vus aux faces du Seigneur YHWH.
23:18	Tu ne sacrifieras pas le sang de mon sacrifice avec du pain levé et la graisse de ma fête ne passera pas la nuit jusqu'au matin<!--Ex. 34:25-26.-->.
23:19	Tu amèneras à la maison de YHWH ton Elohîm, les prémices de tes premiers fruits du sol. Tu ne feras pas cuire le chevreau dans le lait de sa mère.

### Mises en garde et promesses de YHWH

23:20	Voici, j'envoie un ange en face de toi, afin qu'il te garde dans le chemin et qu'il t'introduise dans le lieu que je t'ai préparé.
23:21	Garde-toi face à lui, écoute sa voix et ne l'irrite pas, car il ne pardonnera pas votre transgression, car mon Nom est en lui.
23:22	Mais si tu écoutes, si tu écoutes sa voix et si tu fais tout ce que je te dirai, je serai l'ennemi de tes ennemis et j'affligerai ceux qui t'affligeront.
23:23	Car mon ange marchera en face de toi et t'introduira chez les Amoréens, les Héthiens, les Phéréziens, les Kena'ânéens, les Héviens et les Yebousiens, et je les exterminerai.
23:24	Tu ne te prosterneras pas devant leurs elohîm et tu ne les serviras pas. Tu n'imiteras pas leurs œuvres, mais tu les détruiras, tu les détruiras et tu briseras, tu briseras leurs monuments<!--Ex. 20:5, 34:13 ; No. 33:52.-->.
23:25	Vous servirez YHWH votre Elohîm. Et il bénira ton pain et tes eaux et j'ôterai les maladies du milieu de toi<!--Ex. 15:26 ; De. 6:13, 7:15-16 ; Mt. 4:10.-->.
23:26	Il n'y aura en ta terre ni femme qui avorte, ni femme qui soit stérile. Je remplirai le nombre de tes jours.
23:27	J’enverrai ma terreur en face de toi et je mettrai en déroute tout peuple contre lequel tu iras. Je te donnerai tous tes ennemis, la nuque vers toi<!--De. 7:23.-->.
23:28	J'enverrai des frelons en face de toi, qui chasseront les Héviens, les Kena'ânéens et les Héthiens, loin de tes faces<!--De. 7:20 ; Jos. 24:12.-->.
23:29	Je ne les chasserai pas en face de toi en une année, de peur que la terre ne devienne une dévastation, et que les bêtes des champs ne se multiplient contre toi.
23:30	Mais je les chasserai peu à peu loin de ta face, jusqu'à ce que tu te sois accru et que tu possèdes la terre.
23:31	Et je mettrai des bornes depuis la Mer Rouge jusqu'à la mer des Philistins, et depuis le désert jusqu'au fleuve. Car je livrerai entre tes mains les habitants de la terre et je les chasserai en face de toi.
23:32	Tu ne traiteras pas d'alliance avec eux<!--Jos. 9:1-27.--> ni avec leurs elohîm.
23:33	Ils n'habiteront pas sur ta terre, de peur qu'ils ne te fassent pécher contre moi, car tu servirais leurs elohîm, et ce serait un piège pour toi.

## Chapitre 24

### La torah lue au peuple ; le sang de l'alliance

24:1	Il dit à Moshé : Monte vers YHWH, toi et Aaron, Nadab et Abihou, et 70 des anciens d'Israël, et vous vous prosternerez de loin.
24:2	Et Moshé s'approchera seul de YHWH, mais eux ne s'en approcheront pas, et le peuple ne montera pas avec lui.
24:3	Moshé vint relater au peuple toutes les paroles de YHWH et toutes ses lois. Tout le peuple répondit d'une voix et dit : Nous ferons toutes les paroles que YHWH a dites.
24:4	Moshé écrivit toutes les paroles de YHWH et, s'étant levé tôt le matin, il bâtit un autel au bas de la montagne et 12 monuments pour les 12 tribus d'Israël.
24:5	Et il envoya des jeunes hommes, fils d'Israël, qui firent monter des holocaustes et qui sacrifièrent des jeunes taureaux à YHWH, en sacrifice d'offrande de paix.
24:6	Moshé prit la moitié du sang et le mit dans des bassins, et avec l'autre moitié il aspergea l'autel.
24:7	Il prit le livre d’alliance et le lut aux oreilles du peuple, et ils dirent : Nous ferons tout ce que YHWH a dit, et nous obéirons.
24:8	Moshé prit le sang, et en aspergea le peuple en disant : Voici le sang de l'alliance que YHWH a traitée avec vous, selon toutes ces paroles<!--Mt. 26:28 ; Mc. 14:24 ; Lu. 22:20 ; 1 Co. 11:25 ; Hé. 9:20.-->.

### YHWH fait monter Moshé sur la montagne

24:9	Moshé, Aaron, Nadab, Abihou, et les 70 anciens d'Israël montèrent.
24:10	Ils virent l'Elohîm d'Israël. Sous ses pieds, c’était comme un ouvrage de saphir blanc, comme l’os des cieux en pureté.
24:11	Il n'étendit pas sa main sur les nobles des fils d'Israël. Ils virent<!--« Voir comme un voyant dans un état extatique ».--> Elohîm, puis ils mangèrent et burent.
24:12	YHWH dit à Moshé : Monte vers moi sur la montagne et demeure là. Je te donnerai des tablettes de pierre, la torah et les commandements<!--Vient de l'hébreu « mitsvah ». Il est question du code de la sagesse.--> que j'ai écrits pour les enseigner.
24:13	Moshé se leva avec Yéhoshoua qui était à son service, et Moshé monta sur la montagne d'Elohîm.
24:14	Et il dit aux anciens : Attendez-nous ici jusqu'à ce que nous revenions auprès de vous. Voici, Aaron et Hour sont avec vous. Qui aura une affaire s'approchera d'eux.
24:15	Moshé monta sur la montagne, et une nuée couvrit la montagne<!--Ex. 19:9-16.-->.
24:16	La gloire de YHWH demeura sur la Montagne de Sinaï, et la nuée la couvrit pendant 6 jours. Et au septième jour, il appela Moshé du milieu de la nuée.
24:17	La gloire de YHWH avait l'apparence d'un feu dévorant au sommet de la montagne, aux yeux des fils d’Israël<!--De. 4:24, 9:3 ; Hé. 12:29.-->.
24:18	Moshé entra au milieu de la nuée et monta sur la montagne. Moshé fut sur la montagne 40 jours et 40 nuits.

## Chapitre 25

### Des offrandes volontaires pour les matériaux du tabernacle

25:1	YHWH parla à Moshé, en disant :
25:2	Parle aux fils d'Israël et qu'on prenne une contribution pour moi. Vous prendrez cette contribution de tout homme au cœur bien disposé.
25:3	Voici la contribution que vous prendrez d'eux : de l'or, de l'argent, du cuivre,
25:4	de l'étoffe violette, de la pourpre rouge, de l'écarlate de cochenille<!--La couleur écarlate de cochenille s'obtient grâce à la femelle cochenille aptère qui contient dans son corps et dans ses œufs un pigment rouge à base d'acide carminique qui permet à l'insecte et à ses larves de se protéger des prédateurs. Au moment de la ponte, cette dernière fixe fermement son corps au tronc d'un arbre puis libère ses œufs qui demeurent ainsi protégés en dessous d'elle jusqu'à leur éclosion. Ensuite, l'insecte meurt en libérant cette substance rouge qui se propage sur tout son corps et sur le bois hôte. C'est ce fluide que l'homme récupère pour en faire un colorant à la couleur caractéristique. Une subtile analogie peut être faite entre la cochenille et le Seigneur qui a versé son sang à la croix pour nous donner la vie. « Et moi, je suis un ver et non un homme, l'insulte des humains et le méprisé du peuple. » (Ps. 22:7).-->, du fin lin, du poil de chèvre,
25:5	des peaux de béliers teintes en rouge, des peaux de taissons<!--Le mot hébreu employé ici est « tachash », il désigne le matériau servant à fabriquer la couverture extérieure de la tente de réunion. Si tout le monde s'accorde pour dire qu'il s'agissait d'une fourrure ou d'une peau d'animal, un doute subsiste sur la race exacte de l'animal. On hésite entre le marsouin, le dauphin, le blaireau (taisson) ou peut-être le mouton. Dans de nombreuses bibles, le parti a été pris de traduire par « peaux de dauphins ». Cette hypothèse est cependant très peu probable. D'une part parce que le dauphin n'a pas de fourrure ; d'autre part parce que sa peau n'est absolument pas adaptée à la vie terrestre. Elle est donc impossible à conserver et à transformer, en particulier dans le contexte d'un climat propre au désert. Certains pensent qu'il s'agit tout simplement de peaux de béliers. Dans ce cas, comment expliquer qu'on n'ait pas employé le terme « 'ayil » comme cela est mentionné pour les peaux teintes en rouge ? Il reste donc les peaux de taissons, c'est-à-dire de blaireaux, dont la fourrure est utilisée depuis des siècles. On peut objecter qu'il est impossible que le Seigneur puisse accepter la peau d'un animal impur pour construire son sanctuaire. Si tel était le cas, la peau du dauphin, qui est également un animal impur, n'aurait pas non plus été autorisée. Toutefois, d'un point de vue prophétique, le symbole est important : la présence de cet animal impur préfigurait le Mashiah qui a pris une chair semblable à celle du péché (Ro. 8:3) ; mais aussi le levain que l'on peut trouver dans la pâte nouvelle (1 Co. 5:6-7).-->, du bois d'acacia,
25:6	de l'huile pour le luminaire, des aromates pour l'huile d'onction et pour l'encens aromatique,
25:7	des pierres d'onyx, et d'autres pierres pour la garniture de l'éphod et pour le pectoral.
25:8	Ils me feront un sanctuaire et j'habiterai au milieu d'eux<!--Ex. 29:45-46.-->.
25:9	Selon tout ce que, moi, je te fais voir, le modèle du tabernacle et le modèle de tous ses ustensiles, vous les ferez ainsi.

### L'arche de l'alliance

25:10	Ils feront une arche en bois d'acacia. Sa longueur sera de 2 coudées et demie, sa largeur d'une coudée et demie et sa hauteur d'une coudée et demie.
25:11	Tu la recouvriras d'or pur. Tu la recouvriras à l'intérieur et à l'extérieur, et tu feras sur elle une bordure d'or tout autour<!--Ex. 37:1-9.-->.
25:12	Tu fondras pour elle 4 anneaux en or que tu mettras à ses 4 pieds, 2 anneaux d'un côté et 2 anneaux de l'autre côté.
25:13	Tu feras aussi des barres de bois d'acacia et tu les recouvriras d'or.
25:14	Tu feras entrer les barres dans les anneaux aux côtés de l'arche pour porter l'arche avec elles.
25:15	Les barres seront dans les anneaux de l'arche et on ne les en tirera pas.
25:16	Tu mettras dans l'arche le témoignage que je te donnerai<!--Hé. 9:4.-->.
25:17	Tu feras aussi un propitiatoire d'or pur, dont la longueur sera de 2 coudées et demie, et la largeur d'une coudée et demie.
25:18	Tu feras 2 chérubins en or. Tu les feras en ouvrage martelé, aux 2 extrémités du propitiatoire.
25:19	Fais un chérubin à cette extrémité-ci et un chérubin à cette extrémité-là. Vous ferez les chérubins sur le propitiatoire à ses 2 extrémités.
25:20	Les chérubins auront les ailes étendues en haut, couvrant de leurs ailes le propitiatoire, et leurs faces, l’homme vers son frère. Les faces des chérubins seront vers le propitiatoire<!--1 R. 8:6-7 ; Hé. 9:5.-->.
25:21	Tu mettras le propitiatoire au-dessus de l'arche, et tu mettras dans l'arche le témoignage que je te donnerai.
25:22	Je me rencontrerai là avec toi, et je parlerai avec toi d'au-dessus le propitiatoire, d'entre les deux chérubins qui seront sur l'arche du témoignage, sur tout ce que je t’ordonnerai pour les fils d'Israël<!--Ex. 29:42-43 ; No. 7:89.-->.

### La table des pains des faces

25:23	Tu feras une table de bois d'acacia. Sa longueur sera de 2 coudées, et sa largeur d'une coudée, et sa hauteur d'une coudée et demie.
25:24	Tu la recouvriras d'or pur et tu lui feras une bordure d'or tout autour.
25:25	Tu y feras un rebord d’une paume tout autour, et tu feras une bordure d’or à son rebord, tout autour.
25:26	Tu lui feras 4 anneaux d'or que tu mettras aux 4 coins qui seront à ses 4 pieds.
25:27	Les anneaux seront à côté du rebord, pour loger les barres servant à porter la table.
25:28	Tu feras les barres de bois d'acacia et tu les recouvriras d'or, et on portera la table avec elles.
25:29	Tu feras ses plats, ses coupes, ses jarres et ses coupes sacrificielles pour faire des libations. Tu les feras d'or pur<!--Ex. 37:10-16.-->.
25:30	Tu mettras sur cette table le pain des faces<!--Douze pains de froment, correspondant au nombre des tribus d'Israël, lesquels (pains) étaient offerts à chaque Shabbat, et séparés en deux rangées, disposés pour sept jours sur une table placée dans le sanctuaire ou devant le tabernacle, et ensuite dans le temple. Le pain des faces est l'image du Seigneur Yéhoshoua notre Pain de vie (Jn. 6:31-58).-->, en face de moi, continuellement<!--Lé. 24:5-9.-->.

### Le chandelier d'or pur

25:31	Tu feras un chandelier en or pur<!--Le chandelier avait une double symbolique. D'une part, il préfigurait Yéhoshoua ha Mashiah (Jésus-Christ), notre Lumière (Jn. 1:4-5, 8:12). Les sept lampes évoquaient l'omniscience de l'Esprit de Yéhoshoua ha Mashiah (Za. 3:9 ; Jn. 16:29-30 ; Ap. 1:4, 3:1, 4:5, 5:6). Il est à noter que ce chandelier comportait des calices en forme de fleurs, de pommes et d'amandes (Ex. 25:33) qui symbolisaient le fruit de l'Esprit que nous devons nécessairement porter (Ga. 5:22). D'autre part, il est une image de l'Assemblée (Église) (Ap. 1:20). Voir commentaire en Ex. 37:17-24 ; Es. 8:13-17.-->. Ce chandelier sera fait en ouvrage martelé. Sa base, sa tige et ses branches, ses coupes, ses boutons et ses fleurs sortiront de lui.
25:32	6 branches sortiront de ses côtés : 3 branches d'un côté du chandelier, et 3 autres de l'autre côté du chandelier.
25:33	Il y aura sur l'une des branches, 3 petites coupes en forme d'amande, un bouton et une fleur. Sur l'autre branche, 3 petits plats en forme d'amande, un bouton et une fleur. Il en sera de même des 6 branches sortant du chandelier.
25:34	Il y aura au chandelier, 4 petites coupes en forme d'amande, ses boutons et ses fleurs :
25:35	il y aura un bouton sous ses 2 branches, un bouton sous les 2 autres branches et un bouton sous les 2 autres branches. Il en sera ainsi pour les 6 branches sortant du chandelier.
25:36	Leurs boutons et leurs branches sortiront de lui. Le tout sera d'une seule pièce en ouvrage martelé, en or pur.
25:37	Tu feras ses 7 lampes. On montera ses lampes, afin qu'elles soient de la lumière du côté de ses faces.
25:38	Et ses mouchettes et ses encensoirs, destinés à recevoir ce qui tombe des lampes, seront en or pur.
25:39	On le fera avec tous ses ustensiles d'un talent d'or pur.
25:40	Vois et fais selon le modèle que tu as vu sur la montagne.

## Chapitre 26

### Les tapis en fin lin retors

26:1	Pour le tabernacle, tu feras 10 tapis en fin lin retors, en étoffe violette, en pourpre rouge et en écarlate de cochenille. Tu y feras des chérubins, ouvrage d'ingénieur<!--Ex. 36:8-38.-->.
26:2	La longueur d'un tapis sera de 28 coudées, et la largeur du même tapis de 4 coudées. Tous les tapis auront une même mesure.
26:3	5 de ces tapis seront joints femme à sa sœur, et les 5 autres seront aussi joints femme à sa sœur.
26:4	Fais des lacets d'étoffe violette sur le bord d'un tapis, à l’extrémité de l’assemblage. Tu feras de même au bord du tapis qui sera à l’extrémité dans le second assemblage.
26:5	Tu feras 50 lacets au premier tapis, et tu feras 50 lacets au bord du tapis qui est dans le second assemblage. Ces lacets se correspondant comme une femme à sa sœur.
26:6	Tu feras 50 agrafes en or, et tu assembleras les tapis femme à sa sœur, par les agrafes. Ainsi le tabernacle ne fera qu'un.

### Les tapis de poils de chèvre

26:7	Tu feras des tapis de poils de chèvre pour servir de tente sur le tabernacle. Tu feras 11 de ces tapis.
26:8	La longueur d'un tapis sera de 30 coudées, et la largeur du même tapis sera de 4 coudées. Les 11 tapis auront une même mesure.
26:9	Tu joindras séparément 5 de ces tapis, et les 6 tapis à part, mais tu redoubleras le sixième tapis sur le devant du tabernacle.
26:10	Tu feras 50 lacets sur le bord de l'un des tapis, à l'extrémité de l'assemblage, et 50 lacets au bord du tapis de l'autre assemblage.
26:11	Tu feras 50 agrafes en cuivre et tu feras entrer les agrafes dans les lacets. Tu assembleras<!--Unir, joindre, lier ensemble, être joint. Ep. 2:21.--> ainsi la tente et elle deviendra une.
26:12	L'excédent, le surplus des tapis de la tente, avec la moitié du tapis, s'étalera sur le derrière du tabernacle.
26:13	Une coudée d’un côté et une coudée de l’autre de surplus dans la longueur des tapis de la tente, seront étalées sur les deux côtés du tabernacle, pour le couvrir.

### La couverture en peaux de béliers et la couverture en peaux de taissons

26:14	Tu feras pour ce tabernacle, une couverture en peaux de béliers teintes en rouge, et une couverture en peaux de taissons par-dessus<!--Ex. 35:7,23, 36:19, 39:34.-->.

### Les planches et leurs bases

26:15	Tu feras pour le tabernacle, des planches en bois d'acacia, qu'on fera tenir debout<!--Ex. 36:20-34.-->.
26:16	La longueur d'une planche sera de 10 coudées, et la largeur d'une même planche d'une coudée et demie.
26:17	Il y aura à chaque planche 2 tenons joints femme à sa sœur. Tu feras de même pour toutes les planches du tabernacle.
26:18	Tu feras les planches pour le tabernacle, 20 planches du côté du midi, vers le Théman.
26:19	Et au-dessous des 20 planches, tu feras 40 bases en argent, 2 bases sous une planche pour ses 2 tenons, et 2 bases sous l'autre planche pour ses 2 tenons.
26:20	Et 20 planches de l'autre côté du tabernacle, du côté nord.
26:21	Leurs 40 bases seront en argent, 2 bases sous une planche et 2 bases sous l'autre planche.
26:22	Et pour le fond du tabernacle, vers l'ouest, tu feras 6 planches.
26:23	Tu feras aussi deux planches pour les angles du tabernacle, aux 2 côtés du fond.
26:24	Elles seront jointes par le bas, et elles seront jointes ensemble jusqu'à leur sommet par un seul anneau. Il en sera de même des 2 planches qui seront aux 2 angles.
26:25	Il y aura 8 planches et 16 bases en argent, 2 bases sous une planche et 2 bases sous une autre planche.
26:26	Tu feras 5 barres en bois d'acacia pour les planches d'un des côtés du tabernacle,
26:27	et 5 barres pour les planches de l'autre côté du tabernacle et 5 barres pour les planches du côté du tabernacle, pour le fond, vers l'ouest.
26:28	Et la barre du milieu sera au milieu des planches d’extrémité en extrémité.
26:29	Tu recouvriras aussi d'or les planches, tu feras d'or leurs anneaux pour mettre les barres et tu recouvriras d'or les barres.
26:30	Tu dresseras le tabernacle selon son ordonnance qu’il t’a fait voir sur la montagne.

### Les voiles intérieur et extérieur

26:31	Tu feras un voile<!--Le voile intérieur symbolisait la chair de Yéhoshoua ha Mashiah (Jésus-Christ) qui a été brisée à cause de nos péchés (Es. 53:5 ; Hé. 10:20). Ex. 36:35-38 ; Mt. 27:51 ; Hé. 9:3.--> en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors. On y fera des chérubins, ouvrage d'ingénieur.
26:32	Tu le mettras sur 4 colonnes en bois d'acacia recouvertes d'or, ayant leurs crochets en or. Et elles seront sur 4 bases en argent.
26:33	Tu mettras le voile sous les agrafes et là, à l’intérieur du voile, tu feras entrer l'arche du témoignage. Le voile séparera pour vous le lieu saint d'avec le Saint des saints.
26:34	Tu mettras le propitiatoire sur l'arche du témoignage, dans le Saint des saints.
26:35	Tu placeras la table à l’extérieur du voile, et le chandelier vis-à-vis de la table, sur le côté du tabernacle, vers le Théman, et tu mettras la table du côté nord.
26:36	Et à l'entrée du tabernacle, tu feras un rideau en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, en ouvrage de broderie.
26:37	Tu feras pour ce rideau 5 colonnes en bois d'acacia, que tu recouvriras d'or. Leurs crochets seront en or et tu fondras pour elles 5 bases en cuivre.

## Chapitre 27

### L'autel de cuivre

27:1	Tu feras un autel en bois d'acacia, ayant 5 coudées de long et 5 coudées de large. L'autel sera carré et sa hauteur sera de 3 coudées.
27:2	Tu feras à ses 4 coins des cornes. Ses cornes sortiront de lui et tu le recouvriras de cuivre<!--C'est sur l'autel de cuivre que les animaux étaient sacrifiés. Il préfigurait la croix et le jugement que Yéhoshoua ha Mashiah a pris sur lui à notre place (Es. 53:5 ; 2 Co. 13:4 ; Ph. 2:8).-->.
27:3	Tu feras ses chaudrons pour recevoir ses cendres, et ses pelles, ses cuvettes, ses fourchettes et ses encensoirs. Tu feras tous ses ustensiles en cuivre.
27:4	Tu lui feras une grille en cuivre en forme de treillis, et tu feras au treillis, 4 anneaux de cuivre à ses 4 coins.
27:5	Tu le mettras au-dessous de l'enceinte de l'autel en bas, et le treillis s'étendra jusqu'au milieu de l'autel.
27:6	Tu feras des barres pour l'autel, des barres en bois d'acacia, et tu les recouvriras de cuivre.
27:7	Et on fera passer ses barres dans les anneaux. Les barres seront aux deux côtés de l'autel pour le porter.
27:8	Tu le feras creux, avec des planches. Comme il t’a montré dans la montagne, ainsi on le fera<!--Ex. 38:1-7.-->.

### Le parvis

27:9	Tu feras le parvis du tabernacle. Du côté du midi, vers le Théman, il y aura pour former le parvis, des rideaux en fin lin retors. La longueur de l'un des côtés sera de 100 coudées.
27:10	Il y aura 20 colonnes avec leurs 20 bases en cuivre, les crochets des colonnes et leurs filets seront d'argent.
27:11	Ainsi du côté nord, il y aura également des rideaux sur une longueur de 100 coudées, avec 20 colonnes avec leurs 20 bases en cuivre ; mais les crochets des colonnes avec leurs filets seront d'argent.
27:12	La largeur du parvis du côté de l'occident sera de 50 coudées de rideaux, qui auront 10 colonnes, avec leurs 10 bases.
27:13	Et la largeur du parvis du côté de l’est, vers le levant, sera de 50 coudées.
27:14	À l'un des côtés, il y aura 15 coudées de rideaux, avec leurs 3 colonnes et leurs 3 bases.
27:15	Et de l'autre côté, 15 coudées de rideaux, avec leurs 3 colonnes et leurs 3 bases.

### La porte du parvis

27:16	Il y aura pour la porte du parvis, un rideau de 20 coudées, fait en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, ouvrage de broderie, avec 4 colonnes et 4 bases.
27:17	Toutes les colonnes du parvis seront ceintes d'un filet en argent, et leurs crochets seront en argent, mais leurs bases seront en cuivre.
27:18	La longueur du parvis sera de 100 coudées, sa largeur de 50 de chaque côté, et sa hauteur de 5 coudées. Il sera en fin lin retors, et les bases en cuivre.
27:19	Que tous les ustensiles du tabernacle, pour tout son service, et tous ses pieux, avec les pieux du parvis, soient de cuivre<!--Ex. 38:9-20.-->.

### L'huile d'olive pure pour les lampes

27:20	Et toi, tu ordonneras aux fils d'Israël de t'apporter de l'huile pure d'olives concassées pour le luminaire, afin de faire monter les lampes continuellement<!--Ex. 35:8-28 ; Lé. 24:1-4.-->.
27:21	Aaron avec ses fils les prépareront en face de YHWH, depuis le soir jusqu'au matin, dans la tente de réunion, à l’extérieur du voile qui est devant le témoignage. Ce sera un statut perpétuel pour les fils d'Israël.

## Chapitre 28

### La prêtrise

28:1	Et toi, fais approcher de toi, Aaron ton frère et ses fils avec lui, d'entre les fils d'Israël, pour exercer la prêtrise pour moi : Aaron, Nadab et Abihou, Èl’azar et Ithamar, fils d'Aaron.
28:2	Tu feras pour Aaron ton frère des vêtements sacrés, pour la gloire et la beauté.

### Les vêtements sacrés des prêtres

28:3	Toi, tu parleras à tous les sages de cœur que j’ai remplis de l'Esprit de sagesse : ils feront les vêtements d’Aaron, afin que celui-ci soit consacré et qu’il exerce pour moi la prêtrise.
28:4	Voici les vêtements qu'ils feront : le pectoral, l'éphod, la robe, la tunique brodée, la tiare et la ceinture. Ils feront les vêtements sacrés pour Aaron ton frère, et pour ses fils, afin qu'ils exercent pour moi la prêtrise.
28:5	Ils prendront de l'or, de l'étoffe violette, de la pourpre rouge, de l'écarlate de cochenille et du fin lin.

### L'éphod

28:6	Ils feront l'éphod en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, ouvrage d'ingénieur.
28:7	2 épaulettes jointes seront à ses deux extrémités jointes.
28:8	La ceinture de l'éphod, celle qui est dessus, sera de travail identique : en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
28:9	Tu prendras 2 pierres d'onyx et tu graveras sur elles les noms des fils d'Israël :
28:10	6 de leurs noms sur une pierre et les noms des 6 qui restent sur la seconde pierre, selon leur naissance.
28:11	Tu graveras sur les deux pierres en ouvrage de graveur de pierres les noms des fils d'Israël, comme la gravure d'un sceau, tu les entoureras de montures d'or.
28:12	Tu placeras les deux pierres sur les épaulettes de l'éphod, afin qu'elles soient des pierres de souvenir pour les fils d'Israël. Aaron portera leurs noms sur ses deux épaules devant YHWH, pour souvenir.
28:13	Tu feras des montures en or,
28:14	et 2 chaînes en or pur que tu tresseras en forme de cordons, et tu fixeras aux montures les chaînes ainsi tressées.

### Le pectoral

28:15	Tu feras le pectoral du jugement d'un ouvrage d'ingénieur, comme l'ouvrage de l'éphod, en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
28:16	Il sera carré et double. Sa longueur sera d'un empan, et sa largeur d'un empan.
28:17	Tu le rempliras de garniture de pierres, à 4 rangées de pierres. La rangée de rubis, de topaze et d'émeraude, rangée une.
28:18	La seconde rangée d'escarboucle, de saphir et de diamant<!--Certains traduisent par jaspe.-->.
28:19	La troisième rangée d'opale, d'agate et d'améthyste.
28:20	La quatrième rangée de chrysolithe, d'onyx et de jaspe<!--Certains traduisent par béryl.-->. Elles seront enchâssées dans de l'or, selon leur garniture.
28:21	Les pierres seront aux noms des fils d’Israël, 12 à leurs noms, en gravure de cachet, l’homme à son nom, elles seront pour les 12 tribus.
28:22	Tu feras pour le pectoral des chaînes en or pur, tressées en forme de cordon.
28:23	Tu feras pour le pectoral deux anneaux en or, et tu mettras les 2 anneaux aux 2 extrémités du pectoral.
28:24	Tu mettras les 2 cordons en or dans les 2 anneaux à l'extrémité du pectoral.
28:25	Tu mettras les 2 autres extrémités des 2 chaînettes en cordon sur les deux montures, et tu les mettras sur les épaulettes de l'éphod, sur le devant de l'éphod.
28:26	Tu feras aussi 2 autres anneaux en or, que tu placeras aux 2 autres extrémités du pectoral, sur le bord qui sera du côté de l'éphod à l'intérieur.
28:27	Tu feras 2 autres anneaux en or, que tu mettras aux 2 épaulettes de l'éphod par le bas, sur le devant, à l'endroit où il se joint, au-dessus de la ceinture de l'éphod.
28:28	On liera le pectoral par ses anneaux aux anneaux de l'éphod avec un cordon d'étoffe violette, afin qu'il tienne au-dessus de la ceinture de l'éphod, et que le pectoral ne puisse pas se séparer de l'éphod.
28:29	Aaron portera sur son cœur les noms des fils d'Israël gravés sur le pectoral du jugement, quand il entrera dans le lieu saint, pour servir continuellement de souvenir devant YHWH.

### L'ourim et le thoummim

28:30	Tu mettras sur le pectoral de jugement l'ourim et le thoummim<!--L'ourim (« lumières ») et le thoummim (« perfections ») étaient deux pierres du pectoral que l'on utilisait ensemble pour déterminer la décision d'Elohîm sur certaines questions. Dans ce passage, l'ourim et le thoummim sont précédés par les lettres hébraïques aleph et tav.-->, qui seront sur le cœur d'Aaron, quand il viendra devant YHWH. Et Aaron portera le jugement des fils d'Israël sur son cœur devant YHWH, continuellement.

### La robe de l'éphod

28:31	Tu feras la robe de l'éphod entièrement d'étoffe violette.
28:32	La bouche de sa tête sera au milieu, une lèvre sera autour de sa bouche, en ouvrage tissu. Ce sera pour lui comme la bouche d'un corselet, afin qu'elle ne se déchire pas.
28:33	Tu feras sur ses bordures des grenades en étoffe violette, en pourpre rouge et en écarlate de cochenille, sur les bordures tout autour, et des clochettes d'or entre elles tout autour.
28:34	Une clochette en or et une grenade, une clochette en or et une grenade, sur les bordures de la robe tout autour.
28:35	Aaron en sera revêtu quand il fera le service, et on en entendra le son lorsqu'il entrera dans le lieu saint devant YHWH, et quand il en sortira, afin qu'il ne meure pas.

### La fleur d'or gravée : « Sainteté à YHWH »

28:36	Tu feras une fleur d’or pur, et tu y graveras en gravure de cachet : Sainteté à YHWH.
28:37	Tu l'attacheras avec un cordon d'étoffe violette sur la tiare, sur le devant de la tiare.
28:38	Elle sera sur le front d'Aaron, ainsi Aaron portera l'iniquité commise par les fils d'Israël en faisant leurs saintes offrandes : elle sera continuellement sur son front devant YHWH, pour qu'il leur soit favorable.

### Les vêtements de service d'Aaron et ses fils

28:39	Tu tisseras la tunique en fin lin, tu feras une tiare en fin lin et tu feras une ceinture d'ouvrage de broderie<!--Ex. 39:1-32.-->.
28:40	Tu feras aussi pour les fils d'Aaron des tuniques ainsi que des ceintures et des bonnets pour la gloire et la beauté.
28:41	Tu en revêtiras Aaron ton frère et ses fils avec lui. Tu les oindras, tu rempliras leur main, tu les sanctifieras et ils exerceront la prêtrise pour moi<!--Lé. 8:12, 16:32 ; No. 3:3.-->.
28:42	Tu leur feras des caleçons de lin, pour couvrir la nudité de leur chair : ils tiendront depuis les reins jusqu'au bas des cuisses.
28:43	Aaron et ses fils seront ainsi habillés quand ils entreront dans la tente de réunion, ou quand ils approcheront de l'autel pour faire le service dans le lieu saint, afin qu'ils ne portent pas d'iniquité et ne meurent pas. Ce sera un statut perpétuel pour lui et pour sa postérité après lui.

## Chapitre 29

### Les prêtres consacrés au service de YHWH

29:1	Voici ce que tu leur feras pour les consacrer afin qu'ils exercent la prêtrise pour moi : prends un jeune taureau du troupeau et deux béliers sans défaut<!--Lé. 8:2, 9:2 ; Hé. 7:26-28.-->,
29:2	des pains sans levain, des gâteaux sans levain mêlés d’huile et des galettes sans levain, ointes d'huile. Tu les feras de fine farine de froment<!--Lé. 6:13.-->.
29:3	Tu les mettras dans une corbeille et tu les présenteras dans la corbeille, et aussi le jeune taureau et les deux béliers.
29:4	Tu feras approcher Aaron et ses fils à l'entrée de la tente de réunion et tu les laveras avec de l'eau<!--Ex. 40:12.-->.
29:5	Tu prendras les vêtements, tu habilleras Aaron de la tunique, de la robe de l'éphod, de l'éphod et du pectoral et tu le ceindras par-dessus avec la ceinture de l'éphod.
29:6	Tu poseras la tiare sur sa tête et tu placeras la couronne<!--« Consécration », « couronne », « séparation », « Naziréat ».--> de sainteté sur la tiare.
29:7	Tu prendras l'huile d'onction, tu la répandras sur sa tête et tu l'oindras.
29:8	Puis tu feras approcher ses fils et tu les habilleras des tuniques.
29:9	Tu ceindras Aaron et ses fils d'une ceinture<!--Es. 11:5 ; Ep. 6:14.--> et tu leur attacheras des bonnets. Ils posséderont la prêtrise par statut perpétuel. Et tu rempliras la main d'Aaron et la main de ses fils.
29:10	Tu feras approcher le jeune taureau devant la tente de réunion, et Aaron et ses fils poseront leurs mains sur la tête du jeune taureau.
29:11	Tu tueras le jeune taureau devant YHWH, à l'entrée de la tente de réunion.
29:12	Tu prendras du sang du jeune taureau et le mettras avec ton doigt sur les cornes de l'autel, et tu répandras tout le reste du sang au pied de l'autel.
29:13	Tu prendras aussi toute la graisse qui couvre les entrailles, et le lobe du foie, les deux rognons, la graisse qui les entoure, et tu les feras fumer sur l'autel.
29:14	Mais tu brûleras au feu la chair du jeune taureau, sa peau et ses excréments, hors du camp. C'est un sacrifice pour le péché<!--Lé. 1:3-13 ; Hé. 9:11-22, 13:11.-->.
29:15	Tu prendras l'un des béliers, et Aaron et ses fils poseront leurs mains sur la tête du bélier.
29:16	Tu tueras le bélier, tu prendras son sang et tu en aspergeras le pourtour de l'autel.
29:17	Tu couperas le bélier en morceaux et tu laveras ses entrailles et ses jambes, tu les mettras sur ses morceaux et sur sa tête.
29:18	Tu brûleras tout le bélier sur l'autel. C'est un holocauste pour YHWH, un sacrifice au parfum tranquillisant, consumé par le feu pour YHWH.
29:19	Tu prendras l'autre bélier, et Aaron et ses fils mettront leurs mains sur sa tête.
29:20	Tu tueras le bélier, tu prendras de son sang, tu le mettras sur le lobe de l'oreille d'Aaron et sur le lobe de l'oreille de ses fils, la droite, sur le pouce de leur main droite, sur le gros orteil de leur pied droit. Tu aspergeras de sang l'autel, autour.
29:21	Tu prendras du sang qui sera sur l'autel, de l'huile d'onction, et tu en feras l'aspersion sur Aaron, sur ses vêtements, sur ses fils, et sur les vêtements de ses fils avec lui. Ainsi, lui, ses vêtements, ses fils et les vêtements de ses fils, seront sanctifiés avec lui.
29:22	Tu prendras la graisse du bélier, la queue, et la graisse qui couvre les entrailles, le grand lobe du foie, les deux rognons, la graisse qui est dessus et la cuisse droite, car c'est le bélier de consécration,
29:23	avec un pain, un gâteau de pain à l’huile et une galette, de la corbeille des pains sans levain qui sera devant YHWH.
29:24	Tu mettras toutes ces choses sur les paumes d'Aaron et sur les paumes de ses fils, et tu les remueras ça et là en offrande agitée devant YHWH<!--No. 6:19.-->.
29:25	Tu les prendras de leurs mains et tu les brûleras sur l'autel, sur l'holocauste, pour être un parfum tranquillisant devant YHWH. C'est un sacrifice consumé par le feu à YHWH.

### La part des prêtres

29:26	Tu prendras aussi la poitrine du bélier des consécrations, qui est pour Aaron, et tu la remueras ça et là en offrande agitée devant YHWH. Cela deviendra ta part.
29:27	Tu sanctifieras la poitrine de l'offrande remuée ça et là, et la cuisse de l'offrande élevée, de l'offrande agitée et ce qui aura été élevé du bélier de consécration, de celui d’Aaron et de celui de ses fils<!--Lé. 10:14 ; No. 18:18.-->.
29:28	Cela deviendra pour Aaron et pour ses fils une ordonnance perpétuelle, de la part des fils d’Israël, car c'est une offrande élevée. Ce sera une offrande élevée, de la part des fils d'Israël et, dans leurs offrandes de paix, l'offrande élevée sera pour YHWH.
29:29	Les vêtements sacrés d'Aaron seront pour ses fils après lui, pour les oindre avec et pour remplir leurs mains avec.
29:30	Pendant 7 jours, le prêtre qui sera à sa place parmi ses fils et qui viendra à la tente de réunion pour faire le service dans le lieu saint, en sera revêtu.
29:31	Tu prendras le bélier des consécrations et tu feras bouillir sa chair dans un lieu saint.
29:32	Aaron et ses fils mangeront à l'entrée de la tente de réunion la chair du bélier et le pain qui sera dans la corbeille.
29:33	Ils mangeront ces choses, par lesquelles la propitiation aura été faite, pour remplir leurs mains et les sanctifier. Mais un étranger n'en mangera pas, parce qu'elles sont saintes.
29:34	S'il reste de la chair des consécrations et du pain jusqu'au matin, tu brûleras ce reste au feu. On n'en mangera pas, parce que c'est une chose sainte.
29:35	Tu agiras ainsi à l'égard d'Aaron et de ses fils selon toutes les choses que je t'ai ordonnées. Tu rempliras leurs mains pendant 7 jours<!--Lé. 8:31-35.-->.
29:36	Chaque jour, tu offriras pour le péché un jeune taureau pour le Kippour. Tu purifieras l’autel en faisant la propitiation pour lui et tu l’oindras pour le sanctifier<!--Ez. 43:19-20.-->.
29:37	Pendant 7 jours, tu feras la propitiation pour l'autel et tu le sanctifieras. L'autel deviendra saint des saints et tout ce qui touchera l'autel sera saint<!--No. 28:3.-->.

### L'holocauste perpétuel

29:38	Voici ce que tu offriras sur l'autel : 2 agneaux d'un an, chaque jour, à perpétuité.
29:39	Tu offriras l'un des agneaux au matin, et l'autre agneau entre les deux soirs,
29:40	avec un dixième de fine farine, mêlée à un quart de hin d'huile d'olives concassées, et avec une libation d'un quart de hin de vin pour chaque agneau.
29:41	Tu offriras l'autre agneau entre les deux soirs, tu l'offriras avec une offrande de grain et une libation semblables à celles du matin, en parfum tranquillisant. C'est un sacrifice consumé par le feu pour YHWH.
29:42	Ce sera l'holocauste perpétuel qui sera offert dans vos générations, à l'entrée de la tente de réunion, devant YHWH, où je me trouverai avec vous pour te parler.
29:43	Je me trouverai là pour les fils d'Israël, cela sera sanctifié par ma gloire.
29:44	Je sanctifierai la tente de réunion et l'autel. Je sanctifierai aussi Aaron et ses fils, afin qu'ils exercent la prêtrise pour moi.
29:45	J'habiterai au milieu des fils d'Israël, et je deviendrai leur Elohîm.
29:46	Ils sauront que moi, YHWH, je suis leur Elohîm, qui les ai fait sortir de la terre d'Égypte, pour habiter au milieu d'eux, moi, YHWH, leur Elohîm.

## Chapitre 30

### L'autel des parfums ou de l'encens

30:1	Tu feras un autel pour brûler de l'encens. Tu le feras en bois d'acacia.
30:2	Sa longueur sera d'une coudée et sa largeur d'une coudée. Il sera carré et sa hauteur sera de 2 coudées. Ses cornes feront corps avec lui.
30:3	Tu le recouvriras d'or pur, tant le dessus que ses côtés tout autour, et ses cornes. Et tu lui feras une bordure d'or tout autour.
30:4	Tu lui feras 2 anneaux d'or au-dessous de sa bordure, à ses deux côtés, lesquels tu mettras aux 2 coins, pour y faire passer les barres qui serviront à le porter.
30:5	Tu feras les barres en bois d'acacia et tu les recouvriras d'or.
30:6	Tu les mettras devant le voile, qui est au-devant de l'arche du témoignage, à l'endroit du propitiatoire qui est sur le témoignage, où je me trouverai avec toi.
30:7	Aaron y fera brûler de l'encens aromatique. Il en fera brûler matin après matin, quand il préparera les lampes.
30:8	Quand Aaron fera monter les lampes entre les deux soirs, il y brûlera de l'encens. Ce sera un encens perpétuel devant YHWH dans vos générations<!--Ex. 37:25-29 ; 2 Ch. 13:11.-->.
30:9	Vous ne ferez monter sur cet autel ni encens étranger<!--Lé. 10:1-3.-->, ni holocauste, ni offrande, et vous n'y répandrez aucune libation.
30:10	Aaron fera une fois par an, la propitiation sur les cornes de cet autel, avec le sang pour le péché de Kippour, il y fera la propitiation une fois par an, dans toutes vos générations. Il<!--L'autel.--> sera le saint des saints pour YHWH.

### L'offrande pour faire la propitiation pour les âmes<!--Ex. 15:1-21 ; Ps. 107:1-2.-->

30:11	YHWH parla à Moshé et lui dit :
30:12	Quand tu lèveras la tête des fils d'Israël pour en faire le dénombrement, chaque homme donnera la rançon de son âme à YHWH lors du dénombrement ; ainsi, lors de ce dénombrement<!--No. 1:2.-->, il n'y aura pas de plaie parmi eux.
30:13	Voici ce que donneront tous ceux qui passeront par le dénombrement : un demi-sicle, selon le sicle du lieu saint, qui est de 20 guéras. Le demi-sicle sera la contribution pour YHWH<!--Lé. 27:25 ; No. 3:47 ; Ez. 45:12.-->.
30:14	Tous ceux qui passeront par le dénombrement, des fils de 20 ans et au-dessus, donneront cette offrande à YHWH.
30:15	Le riche n'augmentera rien, et le pauvre ne diminuera rien du demi-sicle, quand ils donneront à YHWH l'offrande pour faire la propitiation pour vos âmes.
30:16	Tu prendras des fils d'Israël l'argent de Kippour et tu le donneras pour le service de la tente de réunion, et il deviendra pour les fils d'Israël, un souvenir devant YHWH pour faire la propitiation pour vos âmes.

### Purification par l'eau de la cuve en cuivre<!--Jn. 13:3-10 ; Hé. 10:22 ; 1 Jn. 1:9.-->

30:17	YHWH parla encore à Moshé, en disant :
30:18	Fais une cuve en cuivre, avec sa base de cuivre, pour s'y laver. Et tu la mettras entre la tente de réunion et l'autel, et tu mettras de l'eau dedans ;
30:19	Aaron et ses fils y laveront leurs mains et leurs pieds.
30:20	Quand ils entreront dans la tente de réunion, ils se laveront avec de l'eau afin qu'ils ne meurent pas. Il en ira de même quand ils approcheront de l'autel pour faire le service afin de faire fumer l'offrande consumée par le feu pour YHWH.
30:21	Ils laveront leurs pieds et leurs mains, afin qu'ils ne meurent pas. Ce sera pour eux une ordonnance perpétuelle, tant pour lui que pour ses fils dans leur génération.

### L'huile pour l'onction sainte<!--Jn. 4:23 ; Ep. 2:18, 5:18-19.-->

30:22	YHWH parla à Moshé, en disant :
30:23	Prends des aromates de première qualité : 500 sicles de myrrhe, de celle qui coule d'elle-même : du cinnamome odoriférant, la moitié, soit 250 sicles et du roseau aromatique, 250 sicles :
30:24	de la casse, le poids de 500 sicles, selon le sicle du lieu saint, et un hin d'huile d'olive.
30:25	Tu en feras une huile pour l'onction sainte, un mélange de parfums, un mélange d'épices, ouvrage de parfumeur. Ce sera une huile d’onction sainte.
30:26	Puis tu en oindras la tente de réunion et l'arche du témoignage,
30:27	la table et tous ses ustensiles, le chandelier et ses ustensiles, et l'autel de l'encens,
30:28	et l'autel des holocaustes et tous ses ustensiles, la cuve et sa base.
30:29	Tu les sanctifieras et ils deviendront saints des saints. Tout ce qui les touchera sera saint.
30:30	Tu oindras aussi Aaron et ses fils, et les consacreras pour exercer la prêtrise pour moi.
30:31	Tu parleras aux fils d'Israël, en disant : Ce sera pour moi une huile d'onction sainte dans toutes vos générations.
30:32	On ne la versera pas sur la chair d'un être humain, et vous n'en ferez pas de semblable, de même composition. Elle est sainte, elle sera sainte pour vous.
30:33	L’homme qui composera un onguent semblable et qui en mettra sur un étranger, sera retranché de ses peuples.

### L'encens pur parfumé

30:34	YHWH dit à Moshé : Prends des aromates, du stacté, de l'onyx et du galbanum, des aromates et de l'encens pur. Ce sera part pour part.
30:35	Tu en feras un encens, un mélange d'épices, ouvrage de parfumeur, salé, pur et saint.
30:36	Tu le réduiras en poudre et tu le mettras dans la tente de réunion devant le témoignage, là où je te rencontrerai. Il deviendra pour vous saint des saints.
30:37	Quant à l'encens que tu feras, vous n'en ferez pas pour vous de semblable composition. Ce sera une chose sainte pour YHWH.
30:38	L'homme qui en fera un semblable pour en sentir l'odeur, sera retranché de son peuple.

## Chapitre 31

### YHWH suscite des artisans

31:1	YHWH parla à Moshé, en disant :
31:2	Regarde, j'ai appelé par son nom Betsaleel, fils d'Ouri, fils de Hour, de la tribu de Yéhouda.
31:3	Je l'ai rempli de l'Esprit d'Elohîm, de sagesse, d'intelligence et de connaissance pour toutes sortes d'ouvrages,
31:4	pour concevoir des inventions, pour travailler l'or, l'argent et le cuivre ;
31:5	pour tailler les pierres à enchâsser, pour tailler le bois afin de faire toutes sortes d'ouvrages.
31:6	Et moi, voici, j'ai donné avec lui Oholiab, fils d'Ahisamac, de la tribu de Dan. Dans le cœur de tout sage de cœur, j'ai mis de la sagesse, afin qu'ils fassent toutes les choses que je t'ai ordonnées :
31:7	la tente de réunion, l'arche du témoignage, et le propitiatoire qui sera dessus, et tous les ustensiles du tabernacle,
31:8	la table avec tous ses ustensiles, le chandelier pur avec tous ses ustensiles, l'autel de l'encens,
31:9	l'autel de l'holocauste avec tous ses ustensiles, la cuve et sa base,
31:10	les vêtements en ouvrage tressé, les vêtements sacrés pour le prêtre Aaron, et les vêtements de ses fils pour exercer la prêtrise,
31:11	l'huile d'onction et l'encens aromatique pour le lieu saint. Ils feront toutes les choses que je t'ai ordonnées.

### Le shabbat comme signe entre YHWH et Israël

31:12	YHWH parla encore à Moshé, en disant :
31:13	Toi parle aux fils d'Israël en disant : Vous garderez absolument mes shabbats, car c'est un signe entre moi et vous, et parmi vos générations, afin que vous sachiez que je suis YHWH qui vous sanctifie. 
31:14	Vous garderez le shabbat car il est saint pour vous. Celui qui le profanera mourra, il mourra. Toute personne qui fera une œuvre en ce jour-là sera retranchée du milieu de son peuple.
31:15	Pendant 6 jours le travail se fera, mais le septième jour est le shabbat, le shabatôn consacré à YHWH. Quiconque fera un travail le jour du shabbat mourra, il mourra.
31:16	Les fils d'Israël garderont le shabbat pour faire le shabbat en leur génération, par une alliance perpétuelle.
31:17	C'est un signe perpétuel entre moi et les fils d'Israël. Car YHWH a fait en 6 jours les cieux et la Terre, et il a cessé au septième et a repris son souffle<!--Ge. 2:2 ; Ez. 20:12.-->.
31:18	Lorsqu'il eut achevé de parler à Moshé, au Mont Sinaï, il lui donna les deux tablettes du témoignage, les tablettes de pierre écrites du doigt d'Elohîm<!--De. 9:10.-->.

## Chapitre 32

### Le culte du veau d'or

32:1	Quand le peuple vit que Moshé tardait à descendre de la montagne, le peuple se rassembla autour d'Aaron et lui dit : Lève-toi ! Fais-nous des elohîm qui marchent devant nous, car quant à ce Moshé, cet homme qui nous a fait monter de la terre d'Égypte, nous ne savons ce qui lui est arrivé<!--Ac. 7:40.-->.
32:2	Aaron leur dit : Mettez en pièces les anneaux d'or qui sont aux oreilles de vos femmes, de vos fils et de vos filles, et apportez-les-moi<!--Ex. 35:22.-->.
32:3	Tout le peuple mit en pièces les anneaux d'or qui étaient à leurs oreilles et ils les apportèrent à Aaron.
32:4	Ayant pris l'or de leurs mains, il le façonna au ciseau pour en faire un veau<!--Selon toute vraisemblance, les Israélites s'étaient inspirés d'une idole égyptienne, le taureau sacré Apis, pour faire le veau d'or. Elohîm de la puissance sexuelle, de la fertilité et de la force, il était souvent représenté sous la forme d'un homme avec une tête de taureau, puis avec un disque solaire entre les cornes à partir du Nouvel Empire.--> en métal fondu<!--Lé. 19:4, 26:1.-->. Et ils dirent : Voici tes elohîm, Israël, qui t'ont fait monter de la terre d'Égypte.
32:5	Aaron, voyant cela, bâtit un autel en face de lui et Aaron cria, en disant : Demain, il y aura une fête pour YHWH !
32:6	Le lendemain, s'étant levés tôt le matin, ils firent monter des holocaustes et présentèrent des offrandes de paix. Le peuple s'assit pour manger et boire, puis ils se levèrent pour jouer<!--1 Co. 10:7.-->.

### YHWH condamne l'idolâtrie d'Israël

32:7	YHWH dit à Moshé : Va, descends, car ton peuple que tu as fait monter de la terre d'Égypte s'est corrompu<!--De. 32:5.-->.
32:8	Ils se sont promptement détournés de la voie que je leur avais ordonnée, ils se sont fait un veau en métal fondu et se sont prosternés devant lui, ils lui ont sacrifié et ont dit : Voici tes elohîm, Israël, qui t'ont fait monter de la terre d'Égypte<!--1 R. 12:28.-->.
32:9	YHWH dit à Moshé : J’ai vu ce peuple, et voici, c'est un peuple au cou raide.
32:10	Maintenant laisse-moi faire ! Ma colère s'embrasera contre eux et je les consumerai, mais je te ferai devenir une grande nation.

### Moshé implore YHWH pour le peuple

32:11	Moshé supplia les faces de YHWH son Elohîm et dit : YHWH, pourquoi ta colère s'embraserait-elle contre ton peuple, que tu as fait sortir de la terre d'Égypte par une grande puissance et par une main forte<!--Ps. 106:23.--> ?
32:12	Pourquoi les Égyptiens parleraient-ils, en disant : C'est dans une mauvaise intention qu'il les a fait sortir, pour les tuer dans les montagnes et les consumer sur les faces du sol ? Reviens de la colère de ta narine et repens-toi du mal à l’égard de ton peuple<!--No. 14:11-15 ; De. 9:28.-->.
32:13	Souviens-toi d'Abraham, de Yitzhak et d'Israël, tes serviteurs, auxquels tu as juré par toi-même et auxquels tu as dit : Je multiplierai votre postérité comme les étoiles des cieux, et je donnerai à votre postérité toute cette terre dont j'ai parlé, et ils l'hériteront à perpétuité<!--De. 34:4.-->.
32:14	Et YHWH se repentit du mal qu'il avait dit qu'il ferait à son peuple.

### Jugement sur le peuple

32:15	Moshé regarda et descendit de la montagne, ayant dans sa main les deux tablettes du témoignage. Les tablettes étaient écrites des deux côtés, elles étaient écrites de part et d'autre.
32:16	Les tablettes étaient l'ouvrage d'Elohîm, et l'écriture était l'écriture d'Elohîm, gravée sur les tablettes.
32:17	Et Yéhoshoua, entendant le bruit des hurlements du peuple, dit à Moshé : Il y a un bruit de guerre dans le camp.
32:18	Il dit : Ce n'est pas le bruit d'un chant de bravoure, ni le bruit d'un chant de défaite. Moi, j'entends le bruit de chants.
32:19	Il arriva que lorsque Moshé fut approché du camp, il vit le veau et les danses. La colère de Moshé s'embrasa. Il jeta de ses mains les tablettes et les brisa au pied de la montagne.
32:20	Il prit ensuite le veau qu'ils avaient fait, le brûla au feu et l'écrasa jusqu'à ce qu'il soit réduit en poudre. Il répandit cette poudre dans de l'eau et il la fit boire aux fils d'Israël<!--De. 9:17-21.-->.
32:21	Moshé dit à Aaron : Que t'a fait ce peuple pour que tu aies fait venir sur lui un si grand péché ?
32:22	Aaron lui dit : Que la colère de mon seigneur ne s'embrase pas, tu sais toi-même que ce peuple est mauvais.
32:23	Ils m'ont dit : Fais-nous des elohîm qui marchent devant nous ! Car ce Moshé, cet homme qui nous a fait monter de la terre d'Égypte, nous ne savons ce qui lui est arrivé !
32:24	Je leur ai dit : Que celui qui a de l'or, le mette en pièces ! Et ils me l'ont donné. Je l'ai jeté au feu, et ce veau en est sorti.
32:25	Moshé vit que le peuple était en désordre<!--Le mot hébreu signifie aussi « laisser faire », « conduire », « agir comme un meneur », « laisser seul » voir Pr. 29:18.-->, car Aaron l’avait mis en désordre, en dérision auprès de leurs ennemis.
32:26	Moshé se tenant à la porte du camp, dit : Qui est pour YHWH ? Qu'il vienne vers moi ! Et tous les fils de Lévi se rassemblèrent autour de lui.
32:27	Il leur dit : Ainsi parle YHWH, l'Elohîm d'Israël : Que chaque homme mette son épée à son côté, passez et retournez de porte en porte dans le camp. Tuez, chaque homme son frère, chaque homme son compagnon, chaque homme son prochain !
32:28	Les fils de Lévi firent selon la parole de Moshé : ce jour-là il tomba parmi le peuple environ 3 000 hommes.
32:29	Moshé dit : Remplissez vos mains aujourd’hui pour YHWH, oui, chaque homme contre son fils et contre son frère, c’est pour vous donner aujourd’hui une bénédiction.

### Moshé intercède pour Israël

32:30	Et il arriva, le lendemain, que Moshé dit au peuple : Vous avez commis un grand péché. Maintenant je monterai vers YHWH, peut-être ferai-je propitiation pour votre péché.
32:31	Moshé retourna vers YHWH et dit : Oh ! Je te prie, ce peuple a commis un grand péché, en se faisant des elohîm d'or.
32:32	Mais maintenant, porte<!--Lever, porter, supporter, transporter, prendre.--> leur péché, sinon, efface-moi, s’il te plaît, de ton livre que tu as écrit.
32:33	YHWH dit à Moshé : C'est celui qui aura péché contre moi que j'effacerai de mon livre<!--Ap. 3:5, 20:15, 21:27.-->.
32:34	Maintenant va, conduis le peuple au lieu dont je t'ai parlé. Voici, mon ange ira en face de toi, mais le jour où j'exercerai la punition, je ferai venir sur eux la punition pour leur péché.
32:35	Et YHWH frappa le peuple parce qu'il avait fabriqué le veau, celui qu'avait fabriqué Aaron.

## Chapitre 33

### YHWH ne veut plus marcher avec Israël

33:1	YHWH dit à Moshé : Va, monte d'ici, toi et le peuple que tu as fait monter de la terre d'Égypte, vers la terre que j'ai juré de donner à Abraham, à Yitzhak et à Yaacov, en disant : Je la donnerai à ta postérité<!--Ge. 15:18-21.-->.
33:2	J'enverrai un ange face à toi et je chasserai les Kena'ânéens, les Amoréens, les Héthiens, les Phéréziens, les Héviens et les Yebousiens,
33:3	vers une terre où coulent le lait et le miel, mais je ne monterai pas au milieu de toi, parce que tu es un peuple au cou raide, de peur que je ne te consume en chemin.
33:4	Lorsque le peuple entendit cette parole de malheur, il prit le deuil et personne ne mit sur soi ses ornements.
33:5	YHWH dit à Moshé : Dis aux fils d'Israël : Vous êtes un peuple au cou raide. Qu'un seul instant je monte au milieu de vous, et je vous consumerais. Maintenant, ôtez vos ornements de dessus vous et je saurai ce que je ferai pour vous.
33:6	Les fils d'Israël se dépouillèrent de leurs ornements vers la Montagne d'Horeb.

### Moshé dresse la tente de réunion hors du camp

33:7	Moshé prit une tente et la dressa pour lui hors du camp, loin du camp. Il l'appela la tente de réunion. Tous ceux qui cherchaient YHWH sortaient vers la tente de réunion qui était hors du camp.
33:8	Il arriva que, dès que Moshé sortait vers la tente, tout le peuple se levait, et chaque homme se tenait à l'entrée de sa tente et regardait Moshé par-derrière, jusqu'à ce qu'il soit entré dans la tente.
33:9	Et il arriva que, comme Moshé entrait dans la tente, la colonne de nuée descendit et se tint à l'entrée de la tente et il parlait avec Moshé.
33:10	Tout le peuple voyait la colonne de nuée qui se tenait à l'entrée de la tente, et tout le peuple se levait et se prosternait, chaque homme à l'entrée de sa tente.
33:11	YHWH parlait à Moshé faces à faces, comme un homme parle avec son ami. Puis Moshé retournait dans le camp, mais son serviteur, le jeune Yéhoshoua, fils de Noun, ne quittait pas la tente<!--No. 12:8 ; De. 34:10 ; Jn. 15:14-15.-->.

### Moshé demande que YHWH marche avec Israël

33:12	Moshé dit à YHWH : Regarde, tu m'as dit : Fais monter ce peuple, et tu ne m'as pas fait connaître qui tu enverras avec moi. Tu as même dit : Je te connais par ton nom, et aussi, tu as trouvé grâce à mes yeux.
33:13	Maintenant, s’il te plaît, si j'ai trouvé grâce à tes yeux, fais-moi connaître ta voie<!--Jn. 14:6.-->, s'il te plaît, pour que je te connaisse et que je trouve grâce à tes yeux. Considère aussi que cette nation est ton peuple<!--Ps. 25:4.-->.
33:14	Il dit : Mes faces iront et je te ferai reposer.
33:15	Il lui dit : Si tes faces ne viennent pas, ne nous fais pas monter d’ici.
33:16	À quoi connaîtra-t-on que nous avons trouvé grâce à tes yeux, moi et ton peuple ? Ne sera-ce pas quand tu marcheras avec nous ? Ainsi, moi et ton peuple, nous serons distingués de tous les peuples qui sont sur les faces du sol.
33:17	YHWH dit à Moshé : Je ferai aussi ce que tu dis, car tu as trouvé grâce à mes yeux et je te connais par ton nom.

### Moshé veut voir la gloire de YHWH

33:18	Il dit : S’il te plaît, fais-moi voir ta gloire !
33:19	Et il dit : Moi, je ferai passer toute ma beauté sur tes faces et je proclamerai le Nom de YHWH en face de toi. Je ferai grâce à qui je ferai grâce et j'aurai compassion de celui de qui j'aurai compassion<!--Ro. 9:15.-->.
33:20	Il dit : Tu ne pourras pas voir mes faces, car l'être humain ne peut me voir et vivre<!--Jn. 1:18, 14:8-11.-->.
33:21	YHWH dit : Voici, il y a un lieu près de moi, et tu t'arrêteras sur le rocher<!--Le rocher préfigurait Yéhoshoua ha Mashiah (Jésus-Christ), le Roc sur lequel nous devons bâtir nos vies et le fondement de l'Assemblée (Église) (Ps. 18:32 ; Mt. 7:24-25, 16:18 ; 1 Co. 3:11). Voir commentaire en Es. 8:13-17.-->.
33:22	Et quand ma gloire passera, je te mettrai dans la crevasse<!--Es. 2:21.--> du rocher et te couvrirai de ma paume, jusqu'à ce que je sois passé.
33:23	Puis je retirerai ma paume et tu me verras par-derrière, mais mes faces ne se verront pas.

## Chapitre 34

### De nouvelles tables de pierre ; la gloire de YHWH<!--Ex. 33:18-23.-->

34:1	YHWH dit à Moshé : Taille toi-même deux tablettes de pierre comme les premières, et j'écrirai sur ces tablettes les paroles qui étaient sur les premières tablettes que tu as brisées<!--De. 10:1.-->.
34:2	Sois prêt au matin, et monte au matin sur la Montagne de Sinaï, et présente-toi là devant moi sur le haut de la montagne.
34:3	Nul homme ne montera avec toi, nul homme ne se verra aussi sur toute la montagne. Les bœufs mêmes et les brebis ne paîtront pas devant cette montagne<!--Ex. 19:12-13.-->.
34:4	Moshé tailla deux tablettes de pierre comme les premières. Il se leva tôt le matin et monta sur la Montagne de Sinaï, comme YHWH le lui avait ordonné. Il prit dans sa main les deux tablettes de pierre.
34:5	YHWH descendit dans la nuée, et s'arrêta là avec lui, et cria le Nom de YHWH.
34:6	Et YHWH passa sur ses faces en proclamant : YHWH, YHWH ! le El compatissant, miséricordieux, lent à la colère, grand en bonté et en fidélité<!--No. 14:18 ; 2 Ch. 30:9 ; Né. 9:17 ; Ps. 103:8.-->,
34:7	gardant sa bonté pour les milliers, porteur de l'iniquité, de la transgression et du péché, il n’innocente pas, il n’innocente pas, mais il punit l'iniquité des pères sur les fils, et sur les fils des fils sur les troisièmes et sur les quatrièmes<!--Ex. 20:6 ; De. 5:10 ; Jé. 32:18 ; La. 5:7.-->.
34:8	Et Moshé se hâta de s'incliner jusqu'à terre, en se prosternant.
34:9	Et il dit : Adonaï ! S’il te plaît, si j'ai trouvé grâce à tes yeux, qu'Adonaï marche maintenant au milieu de nous, car c'est un peuple au cou raide. Pardonne nos iniquités et notre péché, et prends-nous pour ta possession.

### YHWH renouvelle ses promesses<!--Ex. 33:18-23.-->

34:10	Il dit : Voici, moi-même je traite alliance devant tout ton peuple, je ferai des merveilles qui n’ont jamais été créées sur toute la Terre ni dans aucune nation. Et tout le peuple au milieu duquel tu es, verra l'œuvre de YHWH, car ce que je m'en vais faire avec toi sera une chose redoutable.
34:11	Garde pour toi ce que je t'ordonne aujourd'hui. Voici, je vais chasser devant toi les Amoréens, les Kena'ânéens, les Héthiens, les Phéréziens, les Héviens et les Yebousiens.
34:12	Garde-toi de traiter alliance avec les habitants de la terre où tu entreras, de peur qu'ils ne deviennent un piège pour toi<!--De. 7:2 ; Jos. 23:12-13 ; 2 Co. 6:14.-->.
34:13	Oui, vous démolirez leurs autels, vous briserez leurs monuments et vous couperez leurs poteaux d'Asherah<!--Cité au moins 40 fois dans le Tanakh, le terme hébreu « asherah » fait référence à un « arbre sacré, un pieu près d'un autel » ou encore « une idole », mots par lequels il est majoritairement traduit. Il s'agit également de l'objet en bois utilisé dans le culte de la parèdre de Baal. Menashè (Manassé), roi de Yéhouda, introduisit l'emblème d'Asherah dans le temple (2 R. 21:1-7) en dépit de l'interdiction formelle de YHWH (De. 16:21). Il ne fut enlevé que lors des réformes de Yehizqiyah (Ézéchias) (2 R. 18:3-4) et de Yoshiyah (Josias) (2 R. 23:6-14). Pourtant, YHWH a toujours exigé la destruction d'Asherah, celle qu'il a nommée « l'abomination des Sidoniens », de peur que son peuple y trouve une occasion de chute (Jg. 2:13, 10:6 ; 1 S. 31:10 ; 1 R. 11:5-33 ; 2 R. 23:13). Bien qu'étant majoritairement citée dans les Écritures en tant qu'objet de culte, Asherah est également associée à la divinité Astarté, connue pour être l'Artémis (Diane) des Éphésiens (Ac. 19:23-40), la reine du ciel (Jé. 7:18, 44:15-30), l'Isis des Égyptiens et l'épouse de Baal (voir commentaire en Jg. 2:13).-->.
34:14	Car tu ne te prosterneras pas devant un autre el, parce que YHWH a pour Nom Jaloux, c'est le El qui est jaloux.
34:15	Garde-toi de traiter alliance avec les habitants de la terre, de peur que lorsqu'ils se prostitueront après leurs elohîm, et sacrifieront à leurs elohîm, quelqu'un ne t'invite, et que tu ne manges de son sacrifice.
34:16	Et que tu ne prennes de leurs filles pour tes fils, lesquelles se prostituant après leurs elohîm, n'entraînent tes fils à se prostituer après leurs elohîm.
34:17	Tu ne te feras aucun elohîm de métal fondu.

### Les fêtes et le shabbat<!--Lé. 23:4-44.-->

34:18	Tu garderas la fête des pains sans levain. Pendant 7 jours, au temps fixé<!--Ge. 1:14.--> du mois d’Abib, tu mangeras des pains sans levain, comme je te l'ai ordonné, car c’est au mois d’Abib que tu es sorti d'Égypte.
34:19	Tout ce qui ouvre le premier la matrice m'appartient. Même le premier-né mâle de toutes les bêtes, tant du gros que du petit bétail.
34:20	Tu rachèteras avec un agneau ou un chevreau le premier-né d'un âne. Si tu ne le rachètes pas, tu lui couperas le cou. Tu rachèteras tout premier-né de tes fils. Mes faces ne se verront pas à vide.
34:21	Tu travailleras 6 jours, mais au septième tu te reposeras. Tu te reposeras au temps du labourage et de la moisson.
34:22	Tu feras la fête des semaines, des prémices de la moisson du froment et la fête de la récolte à la fin de l'année.
34:23	Trois fois l’année, tous les mâles seront vus aux faces du Seigneur YHWH, l'Elohîm d'Israël.
34:24	Car je déposséderai les nations en face de toi, j’élargirai tes limites, et nul homme ne convoitera ta terre quand tu monteras pour être vu des faces de YHWH ton Elohîm, trois fois l’année.
34:25	Tu ne tueras pas avec du pain levé le sang de mon sacrifice. Et le sacrifice de la fête de la Pâque ne passera pas la nuit jusqu’au matin.
34:26	Tu apporteras les prémices des premiers fruits du sol dans la maison de YHWH, ton Elohîm. Tu ne feras pas cuire le chevreau dans le lait de sa mère.
34:27	YHWH dit à Moshé : Écris pour toi ces paroles. Oui, sur la bouche de ces paroles, j'ai traité alliance avec toi et avec Israël.
34:28	Il fut là avec YHWH 40 jours et 40 nuits, sans manger de pain et sans boire d'eau. Il écrivit sur les tablettes les paroles de l'alliance, les dix paroles.

### La gloire de YHWH sur la face de Moshé

34:29	Il arriva que lorsque Moshé descendit de la montagne de Sinaï, les deux tablettes du témoignage étaient dans la main de Moshé. Lorsqu'il descendit de la montagne, Moshé ne savait pas que la peau de ses faces envoyait des rayons<!--« Briller », « avoir des cornes », « être cornu ».--> pendant qu'il parlait avec Elohîm.
34:30	Aaron et tous les fils d'Israël regardèrent Moshé, et voici, la peau de ses faces envoyait des rayons, et ils eurent peur de s'approcher de lui.
34:31	Moshé les appela. Aaron et tous les princes de l'assemblée revinrent vers lui et Moshé parla avec eux.
34:32	Après quoi, tous les fils d'Israël s'approchèrent, et il leur ordonna toutes les choses que YHWH lui avait dites sur la Montagne de Sinaï.
34:33	Moshé, ayant fini de leur parler, mit un voile<!--2 Co. 3:13.--> sur ses faces.
34:34	Quand Moshé entrait vers YHWH pour parler avec lui, il ôtait le voile jusqu'à ce qu'il sorte. En sortant, il disait aux fils d'Israël ce qui lui avait été ordonné.
34:35	Les fils d'Israël regardaient les faces de Moshé, car la peau de ses faces envoyait des rayons. C'est pourquoi Moshé remettait le voile sur ses faces<!--2 Co. 3:13.-->, jusqu'à ce qu'il entre pour parler avec YHWH.

## Chapitre 35

### Rappels sur le shabbat

35:1	Moshé réunit toute l'assemblée des fils d'Israël et leur dit : Voici les paroles que YHWH ordonne d'observer.
35:2	Pendant 6 jours le travail se fera, mais le septième jour sera saint pour vous, car c'est le shabbat, le shabatôn pour YHWH. Quiconque fera un travail ce jour-là sera mis à mort.
35:3	Vous n'allumerez pas de feu dans toutes vos demeures le jour du shabbat.

### Les offrandes pour le tabernacle<!--Ex. 25:1-8.-->

35:4	Moshé parla à toute l'assemblée des fils d'Israël et leur dit : Voici la parole que YHWH a ordonnée, en disant :
35:5	Prenez de ce que vous avez, une offrande pour YHWH. Toute personne dont le cœur est bien disposé apportera cette offrande pour YHWH : de l'or, de l'argent, du cuivre<!--Ex. 25:2 ; 2 Co. 8:12.-->,
35:6	des étoffes teintes en violet, en pourpre rouge, en écarlate de cochenille, du fin lin, du poil de chèvre,
35:7	des peaux de béliers teintes en rouge, des peaux de taissons, du bois d'acacia,
35:8	de l'huile pour le chandelier, des aromates pour l'huile d'onction et pour l'encens aromatique,
35:9	des pierres d'onyx et des pierres pour la garniture de l'éphod et pour le pectoral.
35:10	Tous les sages de cœur parmi vous viendront et feront tout ce que YHWH a ordonné :
35:11	le tabernacle, sa tente et sa couverture, ses agrafes, ses planches, ses barres, ses colonnes et ses bases,
35:12	l'arche et ses barres, le propitiatoire et le voile qui sert de rideau,
35:13	la table et ses barres, et tous ses ustensiles, et le pain des faces ;
35:14	le chandelier du luminaire, ses ustensiles, ses lampes et l'huile du luminaire,
35:15	l'autel de l'encens et ses barres, l'huile d'onction et l'encens aromatique, le rideau de la porte pour l'entrée du tabernacle,
35:16	l'autel de l'holocauste, sa grille de cuivre, ses barres et tous ses ustensiles, la cuve avec sa base,
35:17	les rideaux du parvis, ses colonnes, ses bases et le rideau de la porte du parvis,
35:18	les pieux du tabernacle, les pieux du parvis et leur cordage,
35:19	les vêtements en ouvrage tressé pour le service dans le lieu saint, les vêtements sacrés d'Aaron le prêtre, et les vêtements de ses fils pour exercer la prêtrise.
35:20	Toute l'assemblée des fils d'Israël sortit de la présence de Moshé.
35:21	Tous les hommes dont le cœur était bien disposé et l'esprit généreux, apportèrent une offrande à YHWH pour l'ouvrage de la tente de réunion, pour tout son service et pour les vêtements sacrés.
35:22	Les hommes vinrent avec les femmes. Tous ceux qui avaient un cœur bien disposé, apportèrent des boucles, des bagues, des anneaux, des bracelets et des joyaux d'or, chacun remuant ça et là l'offrande agitée d'or pour YHWH.
35:23	Tout homme chez qui se trouvait des étoffes teintes en violet, de la pourpre rouge, de l'écarlate de cochenille, du fin lin, du poil de chèvre, des peaux de béliers teintes en rouge et des peaux de taissons, les apporta.
35:24	Tous ceux qui avaient de quoi faire une offrande d'argent ou de cuivre, apportèrent l'offrande à YHWH. Tous ceux qui avaient du bois d'acacia pour tout l'ouvrage du service, l'apportèrent.
35:25	Toutes les femmes sages de cœur filèrent de leurs mains et firent venir ce qu'elles avaient filé : des fils en violet, de la pourpre rouge, de l'écarlate de cochenille et du fin lin<!--Pr. 31:19.-->.
35:26	Toutes les femmes habiles et que leur cœur y porta filèrent du poil de chèvre.
35:27	Les princes apportèrent des pierres d'onyx et d'autres pierres pour la garniture de l'éphod et du pectoral,
35:28	des aromates et de l'huile pour le chandelier, l'huile d'onction et l'encens aromatique.
35:29	Tout homme et toute femme que le cœur incita à la libéralité pour apporter de quoi faire l'ouvrage que YHWH avait ordonné par la main de Moshé, tous les fils d'Israël apportèrent volontairement des présents à YHWH.

### Betsaleel et Oholiab oints pour l'œuvre du tabernacle

35:30	Moshé dit aux fils d'Israël : Voyez, YHWH a appelé par son nom Betsaleel, fils d'Ouri, fils de Hour, de la tribu de Yéhouda.
35:31	Et il l'a rempli de l'Esprit d'Elohîm, de sagesse, d'intelligence, de connaissance, pour toutes sortes d'ouvrages :
35:32	afin de concevoir des inventions, pour travailler l'or, l'argent et le cuivre,
35:33	pour tailler les pierres à enchâsser, pour tailler le bois afin de faire toutes sortes d'ouvrages d'ingénieur.
35:34	Il lui a mis dans le cœur d’enseigner, à lui et à Oholiab le fils d'Ahisamac de la tribu de Dan.
35:35	Il a rempli leur cœur de sagesse pour faire toutes sortes de travaux de gravure et d'artisanat, pour broder et tisser les étoffes teintes en violet, en pourpre rouge, en écarlate de cochenille et le fin lin, pour faire toutes sortes de travaux et pour inventer toutes sortes d'inventions<!--Es. 28:26.-->.

## Chapitre 36

### Construction du tabernacle d'après le modèle donné par YHWH<!--Ex. 36-39.-->

36:1	Betsaleel, Oholiab et tous les hommes au cœur sage, auxquels YHWH avait donné de la sagesse et de l'intelligence pour savoir faire tout l'ouvrage du service du lieu saint, firent selon toutes les choses que YHWH avait ordonnées.
36:2	Moshé appela Betsaleel et Oholiab, ainsi que tout homme sage de cœur, dans le cœur duquel YHWH avait mis de la sagesse, tous ceux que leur cœur porta à s’approcher pour faire l'ouvrage.
36:3	Ils prirent face à Moshé toute l'offrande que les fils d'Israël avaient apportée pour faire l'ouvrage du service du lieu saint. Mais ceux-ci lui apportaient encore des offrandes volontaires, de matin en matin.
36:4	Tous les hommes sages qui faisaient tout l'ouvrage du lieu saint, vinrent, chaque homme de l'ouvrage qu'ils faisaient,
36:5	et parlèrent à Moshé, en disant : Le peuple apporte beaucoup plus qu’il n’en faut au service pour l'ouvrage que YHWH a ordonné de faire.
36:6	Sur l'ordre de Moshé, on fit passer dans le camp une voix, disant : Que personne, ni homme ni femme, ne fasse plus d'ouvrage pour l'offrande du lieu saint. On empêcha ainsi le peuple d'en apporter.
36:7	L’ouvrage était suffisant pour tout l’ouvrage à faire, et il y en avait même en surplus.

### Les tapis de fin lin retors

36:8	Tous les sages de cœur, parmi ceux qui faisaient l'ouvrage, firent le tabernacle avec 10 tapis de fin lin retors, de fil violet, de pourpre rouge et d'écarlate de cochenille. Et ils y firent des chérubins, ouvrage d'ingénieur.
36:9	La longueur d'un tapis était de 28 coudées, et la largeur du même tapis de 4 coudées. Tous les tapis avaient une même mesure<!--Ex. 26:1-6.-->.
36:10	Et l'on joignit 5 tapis l'un à l'autre, et l'on joignit 5 autres tapis l'un à l'autre.
36:11	On fit des lacets d'étoffe violette sur le bord d'un tapis, au bord de celui qui était attaché. Ils en firent ainsi au bord du dernier tapis dans l'assemblage de l'autre.
36:12	On fit 50 lacets à un tapis, et 50 lacets à l’extrémité du tapis qui était dans le second assemblage. Les lacets se correspondant l'un à l'autre.
36:13	On fit 50 agrafes en or et on attacha les tapis l'un à l'autre avec les agrafes. Cela devint un seul tabernacle.

### Les tapis de poils de chèvre

36:14	On fit des tapis de poils de chèvre, pour la tente au-dessus du tabernacle. On fit 11 de ces tapis.
36:15	La longueur d'un tapis était de 30 coudées, et la largeur du même tapis de 4 coudées. Les 11 tapis étaient d'une même mesure.
36:16	Et on assembla 5 de ces tapis à part, et 6 tapis à part.
36:17	On fit 50 lacets au bord du tapis à l'extrémité d'un assemblage, et on fit 50 lacets au bord du tapis du second assemblage.
36:18	On fit aussi 50 agrafes de cuivre pour assembler la tente afin qu'elle forme un tout.

### Les couvertures de peaux de béliers et de taissons

36:19	On fit pour la tente une couverture de peaux de béliers teintes en rouge, et une couverture de peaux de taissons par-dessus.

### Les planches et leurs bases

36:20	Et on fit pour le tabernacle des planches en bois d'acacia, qu'on fit tenir debout.
36:21	La longueur d'une planche était de 10 coudées, et la largeur de la même planche d'une coudée et demie.
36:22	Il y avait pour chaque planche 2 tenons, joints l'un à l'autre. On fit la même chose pour toutes les planches du tabernacle.
36:23	On fit les planches pour le tabernacle : 20 planches pour le côté sud, vers le Théman.
36:24	Et au-dessous des 20 planches, on fit 40 bases en argent, 2 bases sous une planche, pour ses deux tenons, et 2 bases sous l'autre planche, pour ses deux tenons.
36:25	On fit aussi 20 planches pour l'autre côté du tabernacle, du côté nord,
36:26	et leurs 40 bases en argent : 2 bases sous une planche et 2 bases sous l'autre planche.
36:27	Pour le fond du tabernacle, vers l'occident, on fit 6 planches.
36:28	On fit 2 planches pour les angles du tabernacle aux deux côtés du fond.
36:29	Elles étaient jointes par le bas, et elles étaient jointes ensemble jusqu'au sommet par un anneau. On fit la même chose aux deux planches qui étaient aux deux angles.
36:30	Il y avait 8 planches et 16 bases en argent, 2 bases sous chaque planche.
36:31	Puis on fit 5 barres en bois d'acacia pour les planches de l'un des côtés du tabernacle,
36:32	et 5 barres pour les planches de l'autre côté du tabernacle, et 5 barres pour les planches du tabernacle pour le fond, vers le côté de l'occident.
36:33	On fit la barre du milieu pour traverser par le milieu les planches d'extrémité en extrémité.
36:34	On recouvrit d'or les planches, et on fit leurs anneaux d'or pour y faire passer les barres, et on recouvrit d'or les barres.

### Le voile et le rideau extérieur

36:35	On fit aussi le voile en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors. On y fit des chérubins, ouvrage d'ingénieur.
36:36	Et on lui fit 4 colonnes en bois d'acacia, qu'on recouvrit d'or, ayant leurs crochets d'or. On fondit pour elles 4 bases d'argent.
36:37	On fit aussi à l'entrée de la tente un rideau en étoffe violette, en pourpre rouge, en écarlate de cochenille, en fin lin retors et en ouvrage de broderie ;
36:38	et ses 5 colonnes avec leurs crochets. On recouvrit d'or leurs sommets et leurs filets, mais leurs 5 bases étaient en cuivre.

## Chapitre 37

### L'arche de l'alliance

37:1	Betsaleel fit l'arche en bois d'acacia. Sa longueur était de 2 coudées et demie, et sa largeur d'une coudée et demie, et sa hauteur d'une coudée et demie<!--Ex. 23:10-31.-->.
37:2	Il la recouvrit d'or pur à l'intérieur et à l'extérieur et lui fit une bordure d'or tout autour.
37:3	Il fondit pour elle 4 anneaux en or pour les mettre sur ses 4 pieds : 2 anneaux à l'un de ses côtés, et 2 autres à l'autre côté.
37:4	Il fit aussi des barres en bois d'acacia et les recouvrit d'or.
37:5	Et il fit entrer les barres dans les anneaux aux côtés de l'arche, pour porter l'arche.

### Le propitiatoire

37:6	Il fit le propitiatoire en or pur ; sa longueur était de 2 coudées et demie, et sa largeur d'une coudée et demie.
37:7	Il fit 2 chérubins d'or. Il les fit en ouvrage martelé, aux 2 extrémités du propitiatoire :
37:8	un chérubin à une extrémité, l'autre chérubin à l'autre extrémité. Il fit sortir les chérubins du propitiatoire à ses 2 extrémités.
37:9	Les chérubins avaient les ailes étendues vers le haut, couvrant de leurs ailes le propitiatoire. Leurs faces étaient tournées l'homme vers son frère. Les faces des chérubins étaient vers le propitiatoire.

### La table des pains de proposition

37:10	Il fit aussi la table en bois d'acacia. Sa longueur était de 2 coudées, sa largeur d'une coudée et sa hauteur d'une coudée et demie.
37:11	Il la recouvrit d'or pur et lui fit une bordure d'or tout autour.
37:12	Il lui fit tout autour un rebord d’un palme et il fit une bordure d’or pour son rebord tout autour.
37:13	Il fondit pour elle 4 anneaux d'or et il mit les anneaux aux 4 coins, qui étaient à ses 4 pieds.
37:14	Les anneaux étaient à côté du rebord, pour y mettre les barres afin de porter la table.
37:15	Il fit les barres en bois d'acacia et les recouvrit d'or, pour porter la table.
37:16	Il fit en or pur les ustensiles qu'on devait mettre sur la table, ses plats, ses coupes, ses coupes sacrificielles et ses jarres pour faire des libations.

### Le chandelier

37:17	Il fit aussi le chandelier en or pur. Il le fit en ouvrage martelé. Sa base, sa tige, ses branches, ses coupes, ses boutons et ses fleurs sortaient de lui.
37:18	Et 6 branches sortaient de ses côtés, 3 branches d'un côté du chandelier, et 3 de l'autre côté du chandelier.
37:19	Il y avait sur l'une des branches 3 coupes en forme d'amande, un bouton et une fleur, et sur l'autre branche 3 plats en forme d'amande, un bouton et une fleur ; il fit la même chose aux 6 branches qui sortaient du chandelier.
37:20	Il y avait sur le chandelier 4 coupes en forme d'amande, ses boutons et ses fleurs :
37:21	Il y avait un bouton sous 2 branches qui sortaient de lui, un bouton sous 2 autres branches et un bouton sous 2 autres branches. Il en était ainsi pour les six branches qui en sortaient. 
37:22	Ses boutons et ses branches en étaient tirés. Le tout était d’une seule pièce en ouvrage martelé, en or pur.
37:23	Il fit aussi ses 7 lampes, ses mouchettes et ses encensoirs d'or pur.
37:24	Il le fit avec toute sa garniture, d'un talent d'or pur.

### L'autel des parfums ou de l'encens

37:25	Il fit aussi en bois d'acacia l'autel de l'encens. Sa longueur était d'une coudée, et sa largeur d'une coudée. Il était carré, et sa hauteur était de 2 coudées, et ses cornes procédaient de lui<!--Ex. 30:1-10.-->.
37:26	Il recouvrit d'or pur le dessus de l'autel, ses côtés tout autour et ses cornes. Il fit tout autour une bordure en or.
37:27	Il lui fit 2 anneaux d'or au-dessous de la bordure, sur ses 2 côtés, sur ses deux flancs, aux logis pour les barres, afin de le porter avec.
37:28	Il fit les barres en bois d'acacia et les recouvrit d'or.

### L'huile d'onction et l'encens

37:29	Il fit aussi l'huile pour l'onction sainte et l'encens aromatique et pur. C'était un ouvrage de parfumeur.

## Chapitre 38

### L'autel des holocaustes

38:1	Il fit aussi en bois d'acacia l'autel des holocaustes. Et sa longueur était de 5 coudées, et sa largeur de 5 coudées. Il était carré, et sa hauteur était de 3 coudées<!--Ex. 27:1-8.-->.
38:2	Il fit des cornes à ses 4 coins. Ses cornes sortaient de lui, et il le recouvrit de cuivre.
38:3	Il fit aussi tous les ustensiles de l'autel : les chaudrons, les pelles, les cuvettes, les fourchettes et les encensoirs. Il fit tous ses ustensiles en cuivre.
38:4	Il fit pour l'autel une grille de cuivre en forme de treillis, au-dessous de l'enceinte de l'autel, depuis le bas jusqu'au milieu.
38:5	Il fondit 4 anneaux aux 4 coins de la grille de cuivre, pour mettre les barres.
38:6	Il fit les barres en bois d'acacia et les recouvrit de cuivre.
38:7	Il fit passer les barres dans les anneaux, aux côtés de l'autel, pour le porter avec elles. Il le fit creux, avec des planches.

### La cuve en cuivre

38:8	Il fit aussi la cuve en cuivre et sa base en cuivre, avec les miroirs des femmes qui se rassemblaient à l'entrée de la tente de réunion<!--Ex. 30:14-18.-->.

### Le parvis

38:9	Il fit aussi le parvis. Du côté du sud, vers le Théman, les rideaux du parvis, en fin lin retors, avaient 100 coudées.
38:10	Il fit en cuivre leurs 20 colonnes avec leurs 20 bases, mais les crochets des colonnes et leurs filets étaient en argent.
38:11	Du côté du nord, il y avait 100 coudées. Leurs 20 colonnes et leurs 20 bases étaient en cuivre, les crochets des colonnes et leurs filets étaient en argent.
38:12	Du côté ouest, des rideaux de 50 coudées, leurs 10 colonnes et leurs 10 bases. Les crochets des colonnes et leurs filets étaient en argent.
38:13	Du côté de l’est, vers le levant, 50 coudées :
38:14	pour une aile, 15 coudées de rideaux et leurs 3 colonnes avec leurs 3 bases,
38:15	et pour la seconde aile, de ce côté-ci comme de ce côté-là de la porte du parvis, 15 coudées de rideaux, avec leurs 3 colonnes et leurs 3 bases.
38:16	Il fit tous les rideaux du parvis qui étaient tout autour en fin lin retors.
38:17	Les bases pour les colonnes étaient en cuivre, les crochets des colonnes et les filets étaient en argent, et le placage de leurs sommets était en argent. Toutes les colonnes du parvis furent ceintes tout autour d'un filet d'argent.

### La porte du parvis

38:18	Il fit le rideau de la porte du parvis en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, en ouvrage de broderie, de la longueur de 20 coudées et de la hauteur qui était comme la largeur de 5 coudées, correspondant aux rideaux du parvis.
38:19	Ses 4 colonnes avec leurs bases étaient en cuivre. Leurs crochets étaient en argent et le placage de leurs sommets et leurs filets étaient en argent.
38:20	Tous les pieux du tabernacle et du parvis tout autour étaient en cuivre.

### Les comptes du tabernacle

38:21	Voici le dénombrement du tabernacle, du tabernacle du témoignage, dénombré par la bouche de Moshé, par le service des Lévites, sous la main d'Ithamar, fils du prêtre Aaron.
38:22	Betsaleel, fils d'Ouri, fils de Hour, de la tribu de Yéhouda, fit toutes les choses que YHWH avait ordonnées à Moshé.
38:23	Avec lui Oholiab, fils d'Ahisamac, de la tribu de Dan. C'était un artisan, un brodeur sur les étoffes en violet, en pourpre rouge, en écarlate de cochenille et sur le fin lin.
38:24	Tout l'or utilisé pour l'ouvrage, pour tout l'ouvrage du lieu saint, l'or de l’offrande balancée, fut de 29 talents et 730 sicles, selon le sicle du lieu saint.
38:25	L'argent de ceux de l'assemblée qui furent dénombrés fut de 100 talents et 1 775 sicles, selon le sicle du lieu saint.
38:26	Un demi-sicle par crâne, la moitié d'un sicle selon le sicle du lieu saint. Tous ceux qui passèrent par le dénombrement des fils de 20 ans et au-dessus, furent 603 550.
38:27	Il y eut 100 talents d'argent pour fondre les bases du lieu saint et les bases du voile : 100 bases de 100 talents, un talent pour chaque base.
38:28	Avec les 1 775 sicles il fit les crochets pour les colonnes, recouvrit leurs sommets et les relia par des filets.
38:29	Le cuivre de l’offrande balancée fut de 70 talents et 2 400 sicles.
38:30	On en fit les bases de la porte de la tente de réunion, l'autel de cuivre avec sa grille en cuivre et tous les ustensiles de l'autel,
38:31	et les bases tout autour du parvis, les bases de la porte du parvis, tous les pieux du tabernacle et tous les pieux du parvis tout autour.

## Chapitre 39

### Les vêtements sacrés d'Aaron

39:1	Des étoffes teintes en violet, de pourpre rouge et d'écarlate de cochenille, on fit les vêtements en ouvrage tressé pour le service dans le lieu saint. On fit les vêtements sacrés pour Aaron, comme YHWH l'avait ordonné à Moshé<!--Ex. 28.-->.
39:2	On fit l'éphod en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
39:3	On étendit des lames d'or et on les coupa par filets pour les entrelacer dans les étoffes teintes en violet, en pourpre rouge, en écarlate de cochenille et dans le fin lin, ouvrage d'ingénieur.
39:4	On fit à l'éphod des épaulettes<!--Voir annexe « Les vêtements du grand-prêtre ».--> qui s'attachaient, en sorte qu'il était joint par ses deux extrémités.
39:5	La ceinture de l'éphod, celle qui est dessus, était de travail identique : en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, comme YHWH l'avait ordonné à Moshé.
39:6	On enchâssa aussi les pierres d'onyx dans leurs montures d'or, ayant les noms des fils d'Israël gravés, comme on grave les sceaux.
39:7	Et on les plaça sur les épaulettes de l'éphod, afin qu'elles soient des pierres de souvenir pour les fils d'Israël, comme YHWH l'avait ordonné à Moshé.
39:8	On fit aussi le pectoral<!--Voir annexe « Les vêtements du grand-prêtre ».--> d'ouvrage d'ingénieur, comme l'ouvrage de l'éphod, en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
39:9	On fit le pectoral carré et double. Sa longueur était d'un empan, et sa largeur d'un empan, double.
39:10	Et on le garnit de 4 rangs de pierres. La rangée de rubis, de topaze et d'émeraude, rangée une. 
39:11	La seconde rangée d'escarboucle, de saphir et de diamant<!--Certains traduisent l'hébreu « yahalom » par jaspe.-->.
39:12	La troisième rangée d'opale, d'agate et d'améthyste.
39:13	La quatrième rangée de chrysolithe, d'onyx et de jaspe<!--Certains traduisent par béryl.-->. Elles étaient enchâssées dans leur monture d'or.
39:14	Les pierres étaient aux noms des fils d’Israël, 12 à leurs noms, en gravure de cachet, l’homme à son nom, pour les 12 tribus.
39:15	Et on fit sur le pectoral des chaînes en or pur, tressées en forme de cordons.
39:16	On fit aussi 2 montures en or et 2 anneaux en or et on mit les 2 anneaux aux 2 extrémités du pectoral.
39:17	On mit les 2 chaînettes d'or faites à cordon dans les 2 anneaux à l'extrémité du pectoral.
39:18	Et on mit les 2 autres bouts des 2 chaînettes faites à cordon aux 2 montures, sur les épaulettes de l'éphod, sur le devant de l'éphod.
39:19	On fit 2 autres anneaux en or et on les plaça aux 2 autres extrémités du pectoral sur son bord, qui était du côté de l'éphod à l'intérieur.
39:20	On fit 2 autres anneaux d'or et on les mit aux 2 épaulettes de l'éphod par le bas, répondant sur le devant de l'éphod, à l'endroit où il se joignait au-dessus de la ceinture de l'éphod.
39:21	Et on joignit le pectoral élevé par ses anneaux, aux anneaux de l'éphod, avec un cordon d'étoffe violette afin qu'il tienne au-dessus de la ceinture de l'éphod, et que le pectoral ne bouge de dessus l'éphod, comme YHWH l'avait ordonné à Moshé.
39:22	On fit aussi la robe de l'éphod d'ouvrage tissé et entièrement d'étoffe violette :
39:23	La bouche de la robe était au milieu, comme la bouche d'un corselet avec, autour de la bouche, une lèvre pour qu’elle ne se déchire pas.
39:24	Aux bordures de la robe, on fit des grenades en étoffe violette, en pourpre rouge, en écarlate de cochenille, en fil retors.
39:25	On fit des clochettes en or pur. On mit les clochettes entre les grenades aux bordures de la robe tout autour, parmi les grenades :
39:26	une clochette et une grenade, une clochette et une grenade, sur les bordures de la robe tout autour, pour faire le service, comme YHWH l'avait ordonné à Moshé.
39:27	On fit à Aaron et à ses fils, des tuniques en fin lin, d'ouvrage tissé.
39:28	Le turban en fin lin, les bonnets servant de parures de la tête en fin lin, et les caleçons en lin, en fin lin retors.
39:29	Et la ceinture en fin lin retors, en violet, en pourpre rouge, en écarlate de cochenille, en ouvrage de broderie, comme YHWH l'avait ordonné à Moshé.
39:30	On fit la fleur de la sainte couronne, d’or pur, et on y écrivit en écriture de gravure de cachet : Sainteté à YHWH.
39:31	On mit sur elle un cordon d'étoffe violette pour l'appliquer à la tiare par-dessus, comme YHWH l'avait ordonné à Moshé.

### Le matériel pour exercer la prêtrise est prêt

39:32	Ainsi fut achevé tout l'ouvrage du tabernacle, de la tente de réunion. Les fils d'Israël firent selon tout ce que YHWH avait ordonné à Moshé. Ils firent ainsi.
39:33	Ils apportèrent à Moshé le tabernacle, la tente et tous ses ustensiles, ses agrafes, ses planches, ses barres, ses colonnes et ses bases ;
39:34	la couverture de peaux de béliers teintes en rouge, la couverture de peaux de taissons et le voile qui sert de rideau ;
39:35	l'arche du témoignage et ses barres, et le propitiatoire ;
39:36	la table avec tous ses ustensiles, et les pains des faces<!--Ex. 31:8-10.--> ;
39:37	et le chandelier d'or pur avec toutes ses lampes arrangées, et tous ses ustensiles, et l'huile du chandelier ;
39:38	et l'autel d'or, l'huile d'onction, l'encens aromatique, et le rideau de l'entrée de la tente ;
39:39	l'autel de cuivre avec sa grille de cuivre, ses barres et tous ses ustensiles, la cuve et sa base,
39:40	les rideaux du parvis, ses colonnes, ses bases, le rideau pour la porte du parvis, son cordage, ses pieux, et tous les ustensiles pour le service du tabernacle, pour la tente de réunion ;
39:41	les vêtements en ouvrage tressé pour le service du lieu saint, les vêtements sacrés pour le prêtre Aaron, et les vêtements de ses fils pour exercer la prêtrise.
39:42	Les fils d'Israël firent tout l'ouvrage, comme YHWH l'avait ordonné à Moshé.
39:43	Et Moshé vit tout l'ouvrage, et voici, on l'avait fait ainsi que YHWH l'avait ordonné, on l'avait fait ainsi. Et Moshé les bénit.

## Chapitre 40

### Moshé dresse le tabernacle

40:1	YHWH parla à Moshé, en disant :
40:2	Au jour du premier mois, le premier du mois, tu dresseras le tabernacle, la tente de réunion.
40:3	Tu y placeras l'arche du témoignage, et tu couvriras l'arche avec le voile.
40:4	Tu feras venir la table et tu l'arrangeras en ordre. Tu feras venir le chandelier et tu feras monter ses lampes.
40:5	Tu mettras l'autel d'or pour l'encens devant l'arche du témoignage, et tu placeras le rideau à l'entrée du tabernacle.
40:6	Tu mettras l'autel de l'holocauste vis-à-vis de l'entrée du tabernacle, de la tente de réunion.
40:7	Tu mettras la cuve entre la tente de réunion et l'autel, et y mettras de l'eau.
40:8	Tu placeras le parvis tout autour et tu mettras le rideau à la porte du parvis.
40:9	Tu prendras l'huile de l'onction, tu en oindras le tabernacle et tout ce qui y est, et tu le sanctifieras avec tous ses ustensiles, et il sera saint.
40:10	Tu oindras l'autel de l'holocauste et tous ses ustensiles, tu sanctifieras l'autel et l'autel deviendra saint des saints.
40:11	Tu oindras la cuve et sa base, et la sanctifieras.
40:12	Tu feras approcher Aaron et ses fils à l'entrée de la tente de réunion, et les laveras avec de l'eau.
40:13	Tu habilleras Aaron des vêtements sacrés, tu l'oindras et le sanctifieras et il exercera la prêtrise pour moi.
40:14	Tu feras approcher ses fils, tu les habilleras des tuniques.
40:15	Tu les oindras comme tu auras oint leur père. Ils me serviront en tant que prêtres, et leur onction sera destinée à être pour eux une prêtrise perpétuelle dans leur génération.
40:16	Moshé fit selon tout ce que YHWH lui avait ordonné. Ainsi fit-il.
40:17	Et il arriva, le premier mois, dans la seconde année, le premier du mois, que le tabernacle fut dressé.
40:18	Moshé dressa le tabernacle, mit ses bases, plaça ses planches, mit ses barres et dressa ses colonnes.
40:19	Il étendit la tente sur le tabernacle et plaça la couverture de la tente par-dessus, comme YHWH l'avait ordonné à Moshé.
40:20	Il prit et mit le témoignage dans l'arche et plaça les barres sur l'arche. Il mit aussi le propitiatoire au-dessus de l'arche.
40:21	Et il apporta l'arche dans le tabernacle, et mit le voile qui sert de rideau, et le mit au-devant de l'arche du témoignage, comme YHWH l'avait ordonné à Moshé.
40:22	Il mit aussi la table dans la tente de réunion, au côté du tabernacle vers le nord, en dehors du voile.
40:23	Il arrangea sur elle, en ordre, le pain devant YHWH, comme YHWH l'avait ordonné à Moshé.
40:24	Il plaça aussi le chandelier dans la tente de réunion, vis-à-vis de la table, du côté du tabernacle, vers le sud.
40:25	Il fit monter les lampes, face à YHWH, comme YHWH l'avait ordonné à Moshé.
40:26	Il plaça aussi l'autel d'or dans la tente de réunion, devant le voile.
40:27	Et il y fit brûler l'encens aromatique, comme YHWH l'avait ordonné à Moshé.
40:28	Il plaça aussi le rideau à l'entrée du tabernacle.
40:29	Et il plaça l'autel de l'holocauste à l'entrée du tabernacle, de la tente de réunion. Il y fit monter l'holocauste et l'offrande, comme YHWH l'avait ordonné à Moshé.
40:30	Il plaça la cuve entre la tente de réunion et l'autel, et il y mit de l'eau pour se laver.
40:31	Moshé et Aaron avec ses fils s'y lavèrent leurs mains et leurs pieds.
40:32	Quand ils entraient dans la tente de réunion et qu'ils approchaient de l'autel, ils se lavaient, comme YHWH l'avait ordonné à Moshé.
40:33	Il dressa aussi le parvis tout autour du tabernacle et de l'autel, et tendit le rideau de la porte du parvis. Ainsi Moshé acheva l'ouvrage.

### La gloire de YHWH sur le tabernacle

40:34	La nuée couvrit la tente de réunion, la gloire de YHWH remplit le tabernacle<!--No. 9:15 ; 1 R. 8:10.-->,
40:35	Moshé ne pouvait pas entrer dans la tente de réunion, car la nuée demeurait sur elle, la gloire de YHWH remplissait le tabernacle.
40:36	Quand la nuée montait au-dessus du tabernacle, pendant toutes leurs étapes, les fils d'Israël partaient.
40:37	Si la nuée ne montait pas, ils ne partaient pas, jusqu'au jour où elle montait.
40:38	Car la nuée de YHWH était sur le tabernacle pendant le jour mais, pendant la nuit, il y avait en elle du feu, aux yeux de toute la maison d'Israël, à toutes leurs étapes.
