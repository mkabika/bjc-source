# 2 Corinthiens (2 Co.)

Auteur : Paulos (Paul)

Thème : L'autorité de Paulos

Date de rédaction : Env. 57 ap. J.-C.

Dans l'antiquité, Corinthe, capitale de l'Achaïe, était la ville la plus prospère et puissante de Grèce. Située sur un isthme séparant la Mer Égée de la Mer Ionienne, Corinthe était au carrefour de l'Asie et de l'Italie et constituait un véritable centre commercial où les produits orientaux et occidentaux se croisaient.

Rédigée quelques mois après la première, la seconde lettre de Paulos (Paul) aux Corinthiens fait état d'une vague de méfiance à l'égard de Paulos et exprime les souffrances qui furent les siennes et, qui somme toute, authentifient son apostolat.

## Chapitre 1

### Introduction

1:1	Paulos, apôtre de Yéhoshoua Mashiah par la volonté d'Elohîm, et le frère Timotheos, à l'assemblée d'Elohîm qui est à Corinthe, avec tous les saints qui sont dans toute l'Achaïe :
1:2	à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !

### Elohîm nous console dans toutes nos afflictions

1:3	Béni soit l'Elohîm et Père de notre Seigneur Yéhoshoua Mashiah, le Père des miséricordes et l'Elohîm de toute consolation,
1:4	qui nous console dans toute notre tribulation, afin que par le moyen de la consolation dont nous sommes nous-mêmes consolés d'Elohîm, nous puissions consoler ceux qui sont en quelque tribulation que ce soit.
1:5	Parce que, comme les souffrances du Mashiah abondent en nous, de même notre consolation abonde aussi par le moyen du Mashiah.
1:6	Or, si nous sommes oppressés, c'est en faveur de votre consolation et de votre salut, qui s'opère par l'endurance des mêmes souffrances que nous aussi nous souffrons. Et si nous sommes réconfortés, c'est en faveur de votre consolation et de votre salut. Et notre espérance à votre égard est ferme, 
1:7	sachant que comme vous êtes participants des souffrances, vous l'êtes aussi de la consolation.
1:8	Car, frères, nous ne voulons pas que vous ignoriez la tribulation qui nous est arrivée en Asie, c'est que nous avons été accablés excessivement, au-delà de notre force, jusqu'à désespérer même de vivre.
1:9	Mais nous avions en nous-mêmes la sentence de mort, afin de ne pas placer notre confiance en nous-mêmes, mais en Elohîm qui ressuscite les morts.
1:10	C'est lui qui nous a délivrés d'une telle mort et qui nous en délivre. Et nous avons en lui cette espérance qu'il nous en délivrera encore,
1:11	étant aussi aidés par la supplication que vous faites en notre faveur, afin que le don de grâce obtenu pour nous par beaucoup soit pour beaucoup une occasion de rendre grâce à notre sujet.
1:12	Car voici notre gloire, c'est ce témoignage de notre conscience, nous nous sommes conduits dans le monde, et spécialement envers vous, avec la simplicité et la sincérité d'Elohîm, non pas avec une sagesse charnelle, mais avec la grâce d'Elohîm.
1:13	Car nous ne vous écrivons pas d'autres choses que celles que vous lisez et que même vous reconnaissez. Et j'espère que vous les reconnaîtrez aussi jusqu'à la fin,
1:14	selon que vous avez reconnu en partie que nous sommes votre gloire, comme vous êtes aussi la nôtre pour le jour du Seigneur Yéhoshoua.

### Paulos (Paul) souhaite aller en Macédoine

1:15	Et dans une telle confiance, je voulais premièrement aller vers vous, afin que vous ayez une seconde grâce.
1:16	Et par chez vous passer en Macédoine, et de Macédoine revenir vers vous, et être accompagné par vous en Judée.
1:17	En formant ce dessein, aurais-je donc agi avec légèreté ? Ou bien, mes desseins sont-ils des desseins selon la chair, en sorte qu'il y ait en moi le oui, oui, et le non, non ?
1:18	Mais Elohîm est fidèle, la parole que nous vous avons adressée n'a pas été oui et non.
1:19	Car le Fils d'Elohîm, Yéhoshoua Mashiah, qui a été prêché par notre moyen au milieu de vous, par le moyen de moi et de Silvanos et de Timotheos, n'a pas été oui et non, mais il n’y a eu que oui en lui.
1:20	Car autant qu'il y a de promesses d'Elohîm : en lui est le oui, et en lui est l'amen, à la gloire d'Elohîm par notre moyen.
1:21	Or celui qui nous affermit avec vous en Mashiah et qui nous a oints c'est Elohîm,
1:22	qui nous a aussi marqués de son sceau<!--« Sceller ». Ep. 1:13, 4:30 ; Ap. 7:3-8, 9:4. Voir Ez. 9:4.--> et nous a donné le gage<!--Du grec « arrhabon » : arrhes ; monnaie donnée en gage d'un futur paiement en attendant que le solde soit payé.--> de l'Esprit dans nos cœurs.
1:23	Or je prends Elohîm à témoin sur mon âme, que c'est pour vous épargner que je ne suis plus allé à Corinthe.
1:24	Non que nous dominions sur votre foi, mais nous sommes des compagnons d'œuvre pour votre joie, car c'est par la foi que vous tenez debout.

## Chapitre 2

### L'œuvre de la repentance

2:1	Mais j’ai jugé ceci en moi-même, de ne pas venir de nouveau chez vous dans la douleur.
2:2	Car si je vous attriste, qui est-ce qui me réjouira, sinon celui qui est attristé à partir de moi ?
2:3	Et je vous ai même écrit ceci, afin que, quand je viendrai, je n’aie pas de douleur de la part de ceux au sujet de qui je devais me réjouir, ayant cette confiance en vous tous que ma joie est celle de vous tous.
2:4	Car je vous ai écrit à partir d'une grande tribulation et angoisse de cœur, avec beaucoup de larmes, non afin que vous soyez attristés, mais afin que vous connaissiez l'amour que j'ai spécialement pour vous.
2:5	Or si quelqu’un a causé de la tristesse, ce n’est pas moi qu’il a attristé, mais vous tous, du moins en partie, afin que je ne vous surcharge pas.
2:6	Le châtiment par le plus grand nombre est suffisant pour un tel homme.
2:7	C'est pourquoi, bien au contraire, faites-lui plutôt grâce et consolez-le, de peur qu’un tel homme ne soit accablé par une douleur excessive.
2:8	C'est pourquoi je vous exhorte à confirmer publiquement votre amour pour lui.
2:9	Car c'est aussi pour cela que je vous ai écrit, afin de connaître, en vous mettant à l'épreuve, si vous êtes obéissants en toutes choses.
2:10	Or à qui vous pardonnez, je pardonne aussi. Et si j'ai pardonné quelque chose à quelqu'un, je l'ai fait à cause de vous, en présence du Mashiah,
2:11	afin que Satan n’ait pas l’avantage sur nous, car nous n'ignorons pas ses mauvais desseins.
2:12	Or étant venu à Troas pour l'Évangile du Mashiah, quoique le Seigneur m'y ait ouvert une porte,
2:13	je n’ai pas eu de repos en mon esprit, parce que je n'ai pas trouvé Titos, mon frère. Mais ayant pris congé d'eux, je suis parti pour la Macédoine.

### Le parfum du Mashiah

2:14	Mais grâce soit à Elohîm, qui nous fait toujours triompher dans le Mashiah et qui manifeste par notre moyen l'odeur de sa connaissance en tout lieu.
2:15	Parce que nous sommes la bonne odeur du Mashiah pour Elohîm, parmi ceux qui sont sauvés et parmi ceux qui périssent.
2:16	En effet, pour les uns, une odeur de mort pour la mort, mais pour les autres une odeur de vie pour la vie. Et qui est suffisant pour ces choses ?
2:17	Car nous ne sommes pas, comme beaucoup, qui font du commerce<!--Vient du grec « kapeleuo » qui signifie « être un détaillant », « colporter », « gagner de l'argent en vendant quelque chose », « faire un gain sordide en fournissant n'importe quoi », « faire une chose pour un gain ridicule », « faire du commerce avec la parole d'Elohîm », « essayer de faire un gain en enseignant la vérité divine », « corrompre », « frelater ». Les colporteurs avaient l'habitude de falsifier leurs produits pour l'amour du gain.--> avec la parole d'Elohîm. Mais nous parlons du Mashiah avec sincérité, comme à partir d'Elohîm et devant Elohîm.

## Chapitre 3

### La lettre du Mashiah

3:1	Commençons-nous de nouveau à nous recommander nous-mêmes ? Ou avons-nous besoin, comme quelques-uns, de lettres de recommandation auprès de vous, ou de lettres de recommandation de votre part ?
3:2	Vous êtes vous-mêmes notre lettre, écrite dans nos cœurs, connue et lue de tous les humains.
3:3	Car il est manifeste que vous êtes la lettre du Mashiah, gravée par notre service, non avec de l'encre, mais avec l'Esprit de l'Elohîm vivant, non sur des tablettes de pierres, mais sur les tablettes de chair, sur le cœur.

### Paulos (Paul) serviteur de la nouvelle alliance

3:4	Mais c'est par le moyen du Mashiah que nous avons une telle confiance envers Elohîm.
3:5	Non que nous soyons capables par nous-mêmes de penser quelque chose, comme venant de nous-mêmes, mais notre capacité vient d'Elohîm,
3:6	qui nous a aussi rendus capables d'être serviteurs de la nouvelle alliance<!--Le mot grec « diatheke », souvent traduit par « testament », signifie aussi « alliance ». On le retrouve notamment dans les passages suivants : Mt. 26:28 ; Mc. 14:24 ; Lu. 1:72, 22:20 ; Ac. 3:25, 7:8 ; Ga. 3:15,17, 4:24 ; 1 Co. 11:25 ; Ro. 9:4, 11:27 ; Ep. 2:12 ; Hé. 7:22, 8:6-9, 9:4,15-17,20, 10:16,29, 12:24, 13:20 ; Ap. 11:19. Le fait d'avoir regroupé les écrits de Bereshit (Genèse) à Malakhi (Malachie) sous l'appellatif « Ancien Testament » a induit beaucoup de chrétiens en erreur. L'ancienne alliance correspond uniquement à la loi cérémonielle de Moshé (Moïse) qui a été accomplie par le Mashiah (Christ) à la croix (Jn. 19:30). Ainsi, avant la mort du Seigneur, on ne peut pas parler de testament puisqu'il faut qu'il y ait au préalable la mort du testateur. Or il est évident que les animaux sacrifiés sous la loi ne nous ont rien légué (Hé. 9:1-16).-->, non de la lettre, mais de l'Esprit, car la lettre tue, mais l'Esprit vivifie.
3:7	Or si le service de la mort, écrit en lettres et gravé sur des pierres, est apparu avec gloire au point que les fils d'Israël ne pouvaient fixer les yeux sur la face de Moshé, à cause de la gloire de sa face, laquelle disparaît,
3:8	comment le service de l'Esprit ne sera-t-il pas beaucoup plus dans la gloire ?
3:9	Car si le service de la condamnation a été glorieux, le service de la justice le surpasse de beaucoup en gloire.
3:10	Car, même ce qui a été glorieux dans une certaine mesure, n'a pas été glorifié, à cause de la gloire qui la surpasse.
3:11	Car, si ce qui disparaît est passé par la gloire, combien plus sera dans la gloire ce qui demeure.
3:12	Ayant donc une telle espérance, nous usons d'une grande liberté dans les paroles,
3:13	et non comme Moshé, qui mettait un voile sur sa face, afin que les fils d'Israël ne fixent pas les yeux sur la fin de ce qui disparaît<!--Le mot grec « katargeo » signifie aussi « rendre vain », « inemployé », « inactif » ; « cessé », « abolir ». Voir Ep. 2:15 ; Ro. 3:3,31, 4:14.-->.
3:14	Mais leurs pensées<!--Voir 2 Co. 4:4, 10:5, 11:3 ; Ph. 4:7.--> ont été endurcies. Car jusqu'à ce jour le même voile demeure, sans être découvert<!--2 Co. 3:18.-->, dans la lecture de l'ancienne alliance, parce que c'est en Mashiah qu'il disparaît<!--Voir commentaire du verset 13.-->.
3:15	Mais jusqu'à ce jour, quand on lit Moshé, un voile se couche sur leur cœur.
3:16	Mais quand il se convertit au Seigneur, le voile est ôté.
3:17	Or le Seigneur est l'Esprit et là où est l'Esprit du Seigneur, là est la liberté.
3:18	Mais nous tous qui, à face découverte<!--2 Co. 3:14.-->, contemplons comme dans un miroir<!--« Se regarder », « se contempler dans un miroir ». Il est à noter que les miroirs étaient faits de métal poli, et le reflet n'avait pas la vérité qu'apportent nos miroirs actuels.--> la gloire du Seigneur, nous sommes transformés en la même image, de gloire en gloire, comme par le Seigneur, l'Esprit.

## Chapitre 4

4:1	C'est pourquoi, ayant ce ministère selon la miséricorde qui nous a été faite, nous ne perdons pas courage.
4:2	Mais nous avons rejeté les choses cachées et honteuses, ne marchant pas avec ruse et n'altérant pas la parole d'Elohîm, mais nous nous recommandons nous-mêmes à toute conscience humaine devant Elohîm, par la manifestation de la vérité.
4:3	Mais si notre Évangile est voilé, il est en effet voilé en ceux qui périssent,
4:4	en ceux dont l'elohîm de cet âge a aveuglé les pensées des incrédules, afin que ne brille pas pour eux l'illumination de l'Évangile de la gloire du Mashiah, lequel est l'image d'Elohîm.
4:5	Car nous ne nous prêchons pas nous-mêmes, mais Mashiah Yéhoshoua le Seigneur, et nous, vos esclaves à cause de Yéhoshoua.
4:6	Parce que l'Elohîm qui a dit : De la ténèbre brille la lumière<!--Ge. 1:3.--> ! a brillé dans nos cœurs, par l'illumination de la connaissance de la gloire d'Elohîm sur la face de Yéhoshoua Mashiah.

### Les souffrances de Paulos (Paul)

4:7	Mais nous avons ce trésor dans des vases de terre, afin que l'excellence de cette puissance soit d'Elohîm et non de nous.
4:8	Nous sommes oppressés en tout, mais non réduits entièrement à l'étroit ; nous sommes perplexes<!--« Être sans ressources », « dans l'embarras », « dans le besoin », « dans le doute », « ne pas savoir de quel côté se tourner », « être perdu », « affolé », « ne pas savoir quoi décider ». Voir Ga. 4:20.-->, mais non dans le désespoir.
4:9	Persécutés, mais non abandonnés ; abattus, mais non perdus.
4:10	Nous portons toujours partout dans notre corps la mort du Seigneur Yéhoshoua, afin que la vie de Yéhoshoua soit aussi manifestée dans notre corps.
4:11	Car nous qui vivons, nous sommes sans cesse livrés à la mort pour l'amour de Yéhoshoua, afin que la vie de Yéhoshoua soit aussi manifestée dans notre chair mortelle ;
4:12	de sorte que la mort opère en effet en nous, mais la vie en vous.
4:13	Mais ayant le même esprit de foi, selon ce qui est écrit : J'ai cru, c'est pourquoi j'ai parlé<!--Ps. 116:10.--> ! Nous aussi nous croyons, c'est pourquoi nous parlons,
4:14	sachant que celui qui a réveillé le Seigneur Yéhoshoua nous réveillera aussi par le moyen de Yéhoshoua, et nous présentera avec vous.
4:15	Car toutes choses sont à cause de vous, afin que la grâce, étant multipliée, abonde à la gloire d'Elohîm par le moyen de l'action de grâce d'un grand nombre.
4:16	C'est pourquoi nous ne perdons pas courage. Et même si notre homme extérieur se détruit, l’intérieur, cependant, se renouvelle de jour en jour.
4:17	Car notre légère tribulation d’un moment produit pour nous, d'excellence en excellence, un poids de gloire éternelle,
4:18	puisque nous ne regardons pas aux choses qui se voient, mais aux choses qui ne se voient pas, car les choses qui se voient sont pour un temps, mais les choses qui ne se voient pas sont éternelles.

## Chapitre 5

### L'héritage céleste, notre espoir

5:1	Car nous savons que si notre maison terrestre, une tente, est détruite, nous avons dans les cieux une construction issue d'Elohîm, une maison éternelle qui n'est pas faite par la main de l'homme.
5:2	Car nous gémissons aussi dans cette tente, désirant avec ardeur revêtir notre demeure issue du ciel,
5:3	puisque ayant été ainsi vêtus, nous ne serons pas trouvés nus.
5:4	Car aussi nous qui sommes dans cette tente, nous gémissons, étant accablés, parce que nous désirons, non pas enlever notre vêtement, mais nous revêtir, afin que ce qui est mortel soit englouti par la vie.
5:5	Or celui qui nous a formés<!--« Accomplir », « achever », « façonner : rendre quelqu'un capable pour une chose ».--> pour cela, c'est Elohîm, qui nous a donné le gage de l'Esprit.
5:6	Étant donc toujours pleins de confiance, et sachant qu'en demeurant dans ce corps, nous sommes loin du Seigneur,
5:7	car nous marchons par la foi, non par la vue<!--« L'apparence extérieure », « la forme du visage », « l'aspect ».-->.
5:8	Mais nous sommes pleins de courage et nous préférons plutôt être loin<!--« Aller à l'étranger », « émigrer », « s'en aller », « être ou vivre au loin ».--> de ce corps et demeurer auprès du Seigneur.
5:9	C'est pourquoi, que nous demeurions auprès de lui ou que nous soyons loin de lui, nous nous efforçons sérieusement<!--« Être fondé sur l'honneur », « être animé de l'amour de l'honneur », « amour de l'honneur qui fait lutter pour qu'une chose se fasse », « être ambitieux », « viser son but ». Voir Ro. 15:20.--> de lui être agréables.
5:10	Car il nous faut tous apparaître devant le tribunal<!--Le tribunal du Mashiah (tribunal de Christ) n'a pas vocation à statuer sur le salut des enfants d'Elohîm. Les chrétiens y seront jugés en fonction des œuvres produites sur la terre. Chacun devra rendre compte de ce qu'il aura fait et de la gestion des dons et services reçus. Voir 1 Co. 3:10-14, 4:4 ; Ro. 14:10 ; 2 Ti. 4:8. Voir Jn. 5:29--> du Mashiah, pour que chacun reçoive selon ce qu'il aura pratiqué avec son corps : soit du bien, soit du mal.
5:11	Connaissant donc la crainte du Seigneur, nous persuadons les humains et nous sommes manifestés à Elohîm. Mais j'espère que nous sommes aussi manifestés dans vos consciences.
5:12	Car nous ne nous recommandons pas de nouveau nous-mêmes à vous, mais nous vous donnons l'occasion de vous glorifier en faveur de nous, afin que vous l'ayez auprès de ceux qui se glorifient de l'apparence et non du cœur.
5:13	Car si nous étions hors de sens<!--Fou. Mc. 3:21.-->, c'est pour Elohîm, si nous sommes de bon sens, c'est pour vous.
5:14	Car l'amour du Mashiah nous comprime, parce que nous avons jugé que, si un seul est mort en faveur de tous, alors tous sont morts.
5:15	Et il est mort en faveur de tous, afin que ceux qui vivent ne vivent plus pour eux-mêmes, mais en faveur de celui qui est mort et qui a été réveillé pour eux.
5:16	C'est pourquoi dès maintenant nous ne connaissons personne selon la chair et même si nous avons connu Mashiah selon la chair, toutefois nous ne le connaissons plus ainsi maintenant.
5:17	Si donc quelqu'un est en Mashiah, il est une nouvelle créature. Les choses anciennes sont passées ; voici, toutes choses sont devenues nouvelles.

### Le service de la réconciliation

5:18	Or toutes choses sont issues d'Elohîm, qui nous a réconciliés avec lui-même par le moyen de Yéhoshoua Mashiah et qui nous a donné le service de la réconciliation.
5:19	Parce que comme Elohîm était en Mashiah, réconciliant le monde avec lui-même, en ne leur imputant pas leurs fautes, aussi il a mis en nous la parole de la réconciliation.
5:20	C’est donc en faveur du Mashiah que nous sommes ambassadeurs, comme Elohîm appelle par notre moyen : nous supplions en faveur du Mashiah : Soyez réconciliés avec Elohîm !
5:21	Car celui qui n'a pas connu le péché, il l’a fait péché en faveur de nous<!--Le Seigneur Yéhoshoua ha Mashiah a porté nos péchés sur la Croix. Voir Lé. 16:1-34 (le bouc Azazel) ; Es. 53:4-5 ; 1 Pi. 2:24. Son sacrifice était parfait et de bonne odeur. Voir Ep. 5:2.--> afin que nous devenions justice d'Elohîm en lui.

## Chapitre 6

### La persévérance dans la souffrance

6:1	Or, travaillant ensemble avec lui, nous vous prions aussi de ne pas recevoir la grâce d'Elohîm en vain.
6:2	Car il dit : Au temps favorable je t'ai entendu avec faveur, et au jour du salut je t'ai secouru<!--Es. 49:8.-->. Voici maintenant le temps favorable, voici maintenant le jour du salut.
6:3	Nous ne donnons aucune occasion de scandale en quoi que ce soit, afin que le service ne soit pas blâmé.
6:4	Mais en toute chose nous nous rendons nous-mêmes recommandables comme serviteurs d'Elohîm : dans une grande persévérance, dans les tribulations, dans les difficultés, dans les affreuses calamités,
6:5	dans les coups, dans les prisons, dans les désordres, dans les travaux, dans les veilles, dans les jeûnes,
6:6	dans la pureté, dans la connaissance, dans la patience, dans la bénignité, dans un esprit saint, dans un amour sincère,
6:7	dans la parole de la vérité, dans la puissance d'Elohîm, à travers les armes de justice que l'on porte à la main droite et à la main gauche,
6:8	à travers la gloire et l'ignominie, à travers la calomnie et la bonne réputation, comme trompeurs et toutefois vrais,
6:9	comme inconnus, mais bien connus, comme mourants, mais voici nous vivons, comme châtiés, mais non mis à mort,
6:10	comme attristés, mais toujours joyeux, comme pauvres, mais nous faisons beaucoup de riches<!--1 Co. 4:8 ; 2 Co. 8:9, 9:11.-->, comme n'ayant rien, et possédant toutes choses.

### Exhortation à la séparation et à la sanctification

6:11	Notre bouche s'est ouverte pour vous, Corinthiens ! notre cœur s'est élargi.
6:12	Vous n'êtes pas à l'étroit au-dedans de nous, mais vous êtes à l'étroit dans vos entrailles.
6:13	Mais, en juste récompense – je vous parle comme à mes enfants – élargissez-vous, vous aussi !
6:14	Ne portez pas un même joug avec les incrédules. Car quel rapport y a-t-il entre la justice et la violation de la torah ? Mais quelle communion y a-t-il entre la lumière et la ténèbre ?
6:15	Mais quel accord y a-t-il entre Mashiah et Bélial<!--Bélial, de l'hébreu « beliya'al » : « méchant, pervers, perverti, vil, destruction, dangereusement ». Il s'agit de l'un des noms de Satan (qui signifie « indignité, méchanceté, impiété »). Voir De. 13:14.--> ? Ou quelle part a le fidèle avec l'incrédule ?
6:16	Mais quel accord y a-t-il entre le temple d'Elohîm et les idoles ? Car vous êtes le temple de l'Elohîm vivant, selon ce qu'Elohîm a dit : J'habiterai et je marcherai au milieu d'eux. Je serai leur Elohîm et ils seront mon peuple<!--Lé. 26:12 ; Ez. 37:27.-->.
6:17	C'est pourquoi, sortez du milieu d'eux et séparez-vous, dit le Seigneur. Ne touchez pas à ce qui est impur et je vous recevrai avec bonté<!--Es. 52:11 ; Ap. 18:4.-->.
6:18	Et je serai pour vous un Père et vous serez pour moi des fils et des filles<!--Jn. 1:12 ; Ap. 21:7.-->, dit le Seigneur Tout-Puissant.

## Chapitre 7

7:1	Ayant donc ces promesses, bien-aimés, purifions-nous de toute souillure de la chair et de l'esprit, achevant la sainteté dans la crainte d'Elohîm.
7:2	Laissez-nous un espace ! Nous n'avons fait de tort à personne, nous n'avons corrompu personne, nous n'avons pillé personne.
7:3	Je ne parle pas pour condamner, car je vous ai déjà dit que vous êtes dans nos cœurs pour mourir ensemble et pour vivre ensemble.
7:4	J'ai une grande liberté envers vous, j'ai grand sujet de me glorifier en votre faveur. Je suis rempli de consolation, je suis comblé de joie dans toute notre tribulation.
7:5	Car, depuis notre arrivée en Macédoine, notre chair n'eut aucun repos, mais nous avons été oppressés de toutes parts. Des combats au-dehors et des craintes au-dedans.

### La douleur selon Elohîm conduit à la repentance

7:6	Mais celui qui console les humbles, Elohîm, nous a consolés en la parousie de Titos,
7:7	et non seulement en sa parousie, mais aussi par la consolation dont il a été consolé à votre sujet, nous faisant connaître votre ardent désir, vos larmes, votre affection ardente en ma faveur, de sorte que je m'en suis extrêmement réjoui.
7:8	Parce que si je vous ai attristés par ma lettre, je ne m'en repens pas. Si même je m'en suis repenti – car je vois que cette lettre vous a attristés, bien que pour une heure –
7:9	je me réjouis maintenant, non de ce que vous avez été attristés, mais de ce que vous avez été attristés pour la repentance, car vous avez été attristés selon Elohîm, de sorte que vous n'avez reçu aucun dommage de notre part.
7:10	Car la douleur selon Elohîm produit une repentance à salut dont on n'a pas de regret, tandis que la douleur du monde produit la mort.
7:11	Car voici, cette même tristesse qui est selon Elohîm, quel empressement n'a-t-elle pas produit en vous ! Mais quelle plaidoirie ! Mais quelle indignation ! Mais quelle crainte ! Mais quel grand désir ! Mais quel zèle ! Mais quelle vengeance ! Vous vous êtes montrés de toute manière purs dans cette affaire.
7:12	Si donc je vous ai écrit, ce n'était ni à cause de celui qui a fait le mal, ni à cause de celui à qui le mal a été fait, mais pour rendre manifeste chez vous, en présence d'Elohîm, votre empressement pour nous.
7:13	Raison pour laquelle, nous avons été consolés par votre consolation. Mais nous avons été réjouis d’autant plus abondamment de la joie de Titos, dont l'esprit a été mis en repos par vous tous.
7:14	Parce que si en quelque chose je me suis glorifié en votre faveur auprès de lui, je n'en ai pas été confus ; mais comme nous avons toujours dit toutes choses selon la vérité, ainsi ce dont je m'étais glorifié auprès de Titos est devenu vérité.
7:15	Et ses entrailles sont au plus haut degré pour vous, se souvenant de votre obéissance à tous, et la crainte, le tremblement avec lequel vous l’avez reçu.
7:16	Je me réjouis parce qu'en toute chose j'ai confiance en vous.

## Chapitre 8

### Collecte des Macédoniens en faveur des pauvres

8:1	Or nous vous faisons connaître, frères, la grâce d'Elohîm donnée aux assemblées de la Macédoine.
8:2	Parce que dans une grande épreuve de tribulation, leur joie débordante et leur profonde pauvreté ont abondé dans la richesse de leur libéralité,
8:3	selon leur pouvoir, parce que j'en suis témoin, même au-delà de leur pouvoir et volontairement,
8:4	nous suppliant avec beaucoup d'instance de recevoir la grâce et la contribution volontaire de ce service en faveur des saints.
8:5	Et non seulement comme nous l’avions espéré, mais ils se sont d'abord donnés eux-mêmes au Seigneur et à nous, par la volonté d'Elohîm.
8:6	En sorte que nous avons exhorté Titos à achever chez vous aussi cette grâce<!--« Un don de grâce ».-->, comme il l'avait auparavant commencée.

### L'exemple du Mashiah

8:7	Mais comme vous abondez en tout, en foi, et en parole, et en connaissance, et en tout empressement, et dans votre amour pour nous, abondez aussi en cette grâce.
8:8	Je ne dis pas cela comme un ordre, mais afin que, par le moyen de l'empressement des autres, je mette aussi à l'épreuve la sincérité de votre amour.
8:9	Car vous connaissez la grâce de notre Seigneur Yéhoshoua Mashiah qui, étant riche, s'est fait pauvre pour vous, afin que par sa pauvreté vous deveniez riches<!--1 Co. 4:8 ; 2 Co. 6:10, 9:11.-->.
8:10	Et en cela je vous donne mon avis, parce qu'il vous est profitable, à vous qui avez déjà commencé dès l'année passée, non seulement de faire, mais aussi de vouloir.
8:11	Mais maintenant aussi, achevez de la faire, afin que, comme il y a eu promptitude du vouloir, il y ait aussi l'achèvement selon votre avoir.
8:12	Car, la promptitude quand elle existe, elle est agréable selon ce qu'elle a, et non selon ce qu'elle n'a pas.
8:13	Or ce n'est pas afin qu'il y ait soulagement pour d'autres et tribulation pour vous, mais sur un principe d’égalité. Que dans le temps présent, votre abondance serve à leur insuffisance,
8:14	afin que leur abondance serve aussi à votre insuffisance, pour qu'il y ait égalité,
8:15	selon ce qui est écrit : Celui qui avait beaucoup n'a rien eu de superflu, et celui qui avait peu n'en a pas eu moins<!--Ex. 16:18.-->.

### L'exemple des assemblées

8:16	Et grâce soit à Elohîm qui a mis dans le cœur de Titos le même empressement en votre faveur,
8:17	parce qu'il a reçu en effet l'exhortation, étant prêt et zélé, il est allé chez vous de son plein gré.
8:18	Et nous avons aussi envoyé avec lui le frère dont la louange qu'il s'est acquise dans la prédication de l'Évangile est répandue dans toutes les assemblées.
8:19	Et non seulement cela, mais il a été aussi désigné par le vote des assemblées pour être notre compagnon de voyage pour cette grâce<!--Également traduit par « les aumônes ».--> qui est administrée par nous à la gloire du Seigneur lui-même et selon votre promptitude.
8:20	Nous nous préparons afin que personne ne nous blâme dans cette abondante collecte qui est administrée par nous.
8:21	Nous nous préoccupons de ce qui est convenable, non seulement devant le Seigneur, mais aussi devant les humains.
8:22	Mais nous avons envoyé avec eux notre frère, dont nous avons souvent éprouvé le zèle en beaucoup de choses, mais qui maintenant est beaucoup plus zélé à cause de sa grande confiance en vous.
8:23	Ainsi, en faveur de Titos, il est mon associé et mon compagnon d'œuvre auprès de vous ; pour ce qui est de nos frères, ils sont les apôtres<!--Vient du grec « apostolos » qui signifie « apôtre », « envoyé », « un délégué », « un messager », « celui qui est envoyé avec des ordres ».--> des assemblées et la gloire du Mashiah.
8:24	Montrez donc envers eux et devant les assemblées une preuve de votre amour et du sujet que nous avons de nous glorifier en votre faveur.

## Chapitre 9

### La libéralité

9:1	Car il est vraiment superflu pour moi que je vous écrive au sujet du service<!--Vient du grec « diakonia » qui signifie « service » ou « ministère » de ceux qui répondent aux besoins des autres. C'est l'office du diacre dans l'assemblée.--> en faveur des saints.
9:2	Car je connais votre promptitude, dont je me glorifie en votre faveur devant ceux de Macédoine, leur disant que l'Achaïe est prête depuis l'année dernière, et le zèle de chez vous a stimulé la plupart d'entre eux.
9:3	Mais j'ai envoyé les frères, afin que notre sujet de nous glorifier en votre faveur ne soit pas réduit à néant sur ce point-là, et que vous soyez prêts, comme je l’ai dit.
9:4	De peur que si les Macédoniens venaient avec moi et ne vous trouvaient pas prêts, nous (pour ne pas dire vous) soyons honteux de l'assurance dont nous nous sommes glorifiés.
9:5	C'est pourquoi j'ai estimé nécessaire de prier les frères d'aller d'avance chez vous, et d'achever de préparer votre bénédiction que vous avez déjà promise, afin qu'elle soit prête comme une bénédiction et non pas comme une cupidité.
9:6	Mais je vous dis ceci : Celui qui sème économiquement moissonnera aussi économiquement, et celui qui sème avec bénédictions moissonnera aussi avec bénédictions :
9:7	chacun, selon qu'il l'a résolu dans son cœur, non pas avec douleur, ni par contrainte, car Elohîm aime celui qui donne avec joie.
9:8	Et Elohîm est puissant pour vous combler de toute grâce, afin que, possédant toujours tout ce qui vous est nécessaire, vous abondiez en toute bonne œuvre,
9:9	selon qu'il est écrit : Il a répandu, il a donné aux pauvres. Sa justice demeure à perpétuité<!--Ps. 112:9.-->.
9:10	Mais que celui qui fournit de la semence au semeur et du pain pour sa nourriture vous fournisse et vous multiplie la semence<!--Voir Es. 55:10.-->, et qu’il augmente les fruits de votre justice.
9:11	En sorte que vous soyez riches<!--1 Co. 4:8 ; 2 Co. 6:10, 8:9.--> en tout, pour une entière libéralité qui, par notre moyen, produit l'action de grâce à Elohîm.
9:12	Parce que le service de ce don ne comble pas seulement les besoins des saints, mais il abonde aussi au moyen de beaucoup d’actions de grâces à Elohîm.
9:13	À travers la preuve de ce service, ils glorifient Elohîm pour la soumission dont vous faites profession pour l'Évangile du Mashiah et de la libéralité de votre communion envers eux et envers tous.
9:14	Et par leur supplication en votre faveur, ils soupirent<!--Le verbe « soupirer » provient du grec « epipotheo » : « désirer, chérir ».--> après vous à cause de l’excellente grâce d'Elohîm sur vous.
9:15	Mais grâce soit rendue à Elohîm pour son don inexprimable !

## Chapitre 10

### L'autorité apostolique de Paulos (Paul)

10:1	Mais moi-même, Paulos, je vous prie, par la douceur et la bonté du Mashiah, moi qui, en face suis en effet humble parmi vous, mais qui, absent, suis plein de hardiesse envers vous :
10:2	mais je vous supplie que, lorsque je serai présent, je n'aie pas à user de cette confiance dont je compte faire preuve, avec hardiesse, contre quelques-uns qui estiment que nous marchons selon la chair.
10:3	Mais en marchant dans la chair, nous ne combattons<!--« Faire une expédition militaire », « conduire des soldats à la guerre ou la bataille ». Voir 1 Ti. 1:18 ; 2 Ti. 2:4.--> pas selon la chair.
10:4	Car les armes de notre combat<!--« Une expédition », « une campagne », « un service militaire ».--> ne sont pas charnelles, mais puissantes devant Elohîm, pour la destruction des forteresses,
10:5	nous renversons les raisonnements et toute hauteur qui s'élève contre la connaissance d'Elohîm, et amenant toute pensée captive à l'obéissance du Mashiah.
10:6	Et étant prêts à tirer vengeance de toute désobéissance, lorsque votre obéissance aura été accomplie.
10:7	Regardez les choses en face ! Si quelqu'un se persuade lui-même d'être à Mashiah, qu'il considère encore ceci en lui-même, que, comme il est à Mashiah, nous aussi nous sommes à Mashiah.
10:8	Car même si je me glorifiais encore un peu plus au sujet de l'autorité que le Seigneur nous a donnée pour votre construction et non pour votre destruction, je n'en aurai pas honte,
10:9	afin de ne pas paraître vouloir vous effrayer à travers ces lettres.
10:10	Parce que les lettres, dit-on, sont vraiment pesantes et puissantes, mais la parousie du corps est faible et la parole méprisable.
10:11	Que celui qui est tel considère que, tels que nous sommes en parole par lettres, étant absents, tels aussi nous sommes à l'œuvre, étant présents.
10:12	Car nous n’osons pas nous ranger parmi certains qui se recommandent eux-mêmes ou nous comparer à eux. Mais, en se mesurant eux-mêmes avec eux-mêmes et en se comparant eux-mêmes à eux-mêmes, ils ne comprennent pas.
10:13	Mais nous, nous ne nous glorifierons pas sans mesure, mais selon la mesure de la limite définie<!--Vient du grec « kanon » qui signifie « verge », « règle », « limite définie ou espace fixé entre des limites sur lesquelles un pouvoir ou une influence sont impartis ». Voir 2 Co. 10:15-16 ; Ga. 6:16 ; Ph. 3:16.-->, qu'Elohîm nous a départie pour mesure en nous faisant venir même jusqu'à vous.
10:14	Car nous ne nous étendons pas nous-mêmes au-delà des limites prescrites, comme si nous n'étions pas venus jusqu'à vous. Car nous sommes même parvenus jusqu'à vous avec l'Évangile du Mashiah.
10:15	Nous ne nous glorifions pas sans mesure, dans les travaux d'autrui, mais ayant espérance que, votre foi s'accroissant, nous grandirons davantage en vous selon notre limite définie,
10:16	pour évangéliser les contrées situées au-delà de chez vous, sans nous glorifier dans ce qui a déjà été fait dans la limite définie des autres.
10:17	Mais que celui qui se glorifie, se glorifie dans le Seigneur !
10:18	Car ce n'est pas celui qui se recommande lui-même qui est approuvé<!--Vient du grec « Dokimos ». Voir 2 Ti. 2:15.-->, mais celui que le Seigneur recommande<!--Le Seigneur recommande ses serviteurs, il témoigne d'eux auprès des autres (Ac. 10:1-48). Un véritable serviteur d'Elohîm laisse au Seigneur le soin de témoigner de lui auprès des autres, alors que les faux ouvriers se recommandent eux-mêmes (2 Co. 3:1).-->.

## Chapitre 11

11:1	Si seulement vous pouviez me supporter un peu dans ma folie... mais vous me supportez !
11:2	Car je suis jaloux pour vous d'une jalousie d'Elohîm, car je vous ai fiancés à un seul époux, pour présenter<!--Le mot grec signifie aussi « offrir », « présenter une personne à une autre pour la voir et la questionner ». Voir Ro. 6:16,19, 12:1 ; Col 1:28.--> une vierge pure au Mashiah.

### Mise en garde contre les serviteurs de Satan

11:3	Mais je crains que, comme le serpent a trompé Chavvah<!--Ève. Ge. 3:1-6.--> par sa ruse, vos pensées aussi ne se corrompent en se détournant de la simplicité qui est en Mashiah.
11:4	Car, si quelqu'un en effet vient vous prêcher un autre Yéhoshoua que nous n'avons pas prêché, ou si vous recevez un autre esprit que vous n'avez pas reçu, ou un autre évangile que vous n’avez pas embrassé, vous le supporteriez fort bien.
11:5	Car j'estime que je n'ai été inférieur en rien à ces grands apôtres !
11:6	Et même si je suis un ignorant pour la parole, je ne le suis pas pour la connaissance, mais de toute façon nous avons été manifestés en toutes choses auprès de vous.
11:7	Ou bien ai-je fait un péché en m'abaissant moi-même pour que vous soyez élevés, parce que je vous ai annoncé l'Évangile d'Elohîm gratuitement<!--Voir Mt. 10:8 ; Ap. 21:6, 22:17.--> ?
11:8	J'ai dépouillé d'autres assemblées en recevant un salaire<!--« La paye d'un soldat », « allocation », « la part du soldat donnée en supplément à la paye (les rations) et la monnaie dans laquelle il est payé ». Voir Lu. 3:14.--> pour votre service. 
11:9	Et lorsque j'étais chez vous et que je me suis trouvé dans le besoin, je n'étais un fardeau<!--« Devenir engourdi ou dans la torpeur », « sombrer dans la torpeur », « être inactif », « vivre aux crochets de quelqu'un », « peser lourdement sur », « être un fardeau ». 2 Co. 12:13-14.--> pour personne, car les frères venus de la Macédoine comblèrent mon insuffisance. Et en toutes choses, je me suis gardé d'être à votre charge et je m'en garderai encore.
11:10	La vérité du Mashiah est en moi, parce qu'on ne me fera pas cacher ce sujet de gloire dans les régions de l'Achaïe.
11:11	En-raison-de quoi ? Est-ce parce que je ne vous aime pas ? Elohîm le sait !
11:12	Mais ce que je fais, je le ferai encore, pour ôter l'occasion à ceux qui veulent une occasion, afin qu'ils soient trouvés tels que nous dans les choses dont ils se glorifient.
11:13	Car de tels hommes sont de faux apôtres, des ouvriers trompeurs qui se transforment en apôtres du Mashiah.
11:14	Et cela n'est pas étonnant, car Satan lui-même se transforme en ange de lumière<!--Satan est maître en matière de déguisement et d'imitation.-->.
11:15	Ce n'est donc pas une grande chose si ses serviteurs aussi se transforment en serviteurs de justice. Mais leur fin sera selon leurs œuvres.

### Paulos (Paul) se glorifie de sa faiblesse<!--2 Co. 11:16-12:18.-->

11:16	Je le dis encore, afin que personne ne me regarde comme un insensé. Ou bien, recevez-moi comme un insensé, afin que je me glorifie aussi un peu.
11:17	Ce que je dis, je ne le dis pas selon le Seigneur, mais comme par folie, avec l'assurance d'avoir de quoi me glorifier.
11:18	Puisque beaucoup se glorifient selon la chair, moi aussi je me glorifierai.
11:19	Car vous supportez volontiers les insensés, vous qui êtes sages.
11:20	Car si quelqu'un vous réduit en esclavage<!--Voir Ga. 2:4 ; Ro. 8:15.-->, si quelqu'un vous dévore, si quelqu'un s'empare de vous, si quelqu'un s'élève au-dessus de vous, si quelqu'un vous frappe au visage, vous le supportez.
11:21	Je le dis avec honte, comme si nous avons été faibles. Mais si quelqu'un ose se glorifier en quelque chose, (je parle en insensé), moi aussi, je l'ose !
11:22	Sont-ils Hébreux ? Moi aussi. Sont-ils Israélites ? Moi aussi. Sont-ils de la postérité d'Abraham ? Moi aussi.
11:23	Sont-ils serviteurs du Mashiah ? – je parle comme un aliéné –, je le suis plus qu'eux ! Par les travaux, bien plus ! Par les blessures, plus qu'eux. Par les emprisonnements, bien plus ! Dans les morts souvent,
11:24	cinq fois j'ai reçu des Juifs quarante coups<!--De. 25:2-3.--> moins un,
11:25	j'ai été battu de verges trois fois, j'ai été lapidé une fois<!--Voir Ac. 14:19-20.-->, j'ai fait naufrage trois fois, j'ai passé un jour et une nuit dans la mer profonde.
11:26	Souvent en voyage, en péril sur les fleuves, en péril de la part des brigands, en péril de la part de ceux de ma race, en péril parmi les nations, en péril dans les villes, en péril dans les régions inhabitées, en péril sur la mer<!--Voir Ac. 27 et 28.-->, en péril parmi les faux frères ;
11:27	dans la peine et dans le travail dur et difficile, dans de fréquentes veilles, dans la faim, dans la soif, souvent dans les jeûnes<!--Vient du grec « nesteia » qui fait allusion à un exercice volontaire et religieux d'abstinence de nourriture. Le jeûne public fut prescrit par la loi mosaïque, observé annuellement le grand jour de l'expiation, le dixième jour du mois de Tishri (Septembre-Octobre). C'était un jeûne d'automne, quand la navigation était en général dangereuse à cause des tempêtes. Voir Ac. 27:9. Le terme grec fait aussi allusion à l'abstinence causée par le besoin ou la pauvreté. La racine de « nesteia » est « nesteuo » qui signifie « s'abstenir à titre religieux de nourriture et de boisson ». Le jeûne peut être total ou partiel, par de la nourriture simple et frugale. Voir Mt. 6:16.-->, dans le froid et dans la nudité.
11:28	Outre les choses de dehors, ce qui me tient assiégé tous les jours, c'est le souci que j'ai de toutes les assemblées.
11:29	Qui est malade, que je ne sois aussi malade ? Qui est scandalisé, que moi aussi je ne brûle ?
11:30	S'il faut se glorifier, c'est de ma faiblesse que je me glorifierai.
11:31	L'Elohîm et Père de notre Seigneur Yéhoshoua Mashiah, qui est béni pour l’éternité, sait que je ne mens pas.
11:32	À Damas, le gouverneur du roi Arétas gardait la ville des Damascéniens, voulant se saisir de moi,
11:33	et à travers une fenêtre, dans une corbeille, on me descendit à travers la muraille, et j'échappai de ses mains<!--Voir Ac. 9:23-25.-->.

## Chapitre 12

12:1	Se glorifier n'est vraiment pas utile, car j'en viendrai aux apparitions et aux révélations du Seigneur.
12:2	Je connais un être humain en Mashiah, il y a 14 ans passés, si ce fut dans le corps je ne sais, si ce fut hors du corps je ne sais, Elohîm le sait, qui a été enlevé jusqu'au troisième ciel.
12:3	Et je sais que cet être humain, si ce fut dans le corps ou si ce fut hors du corps, je ne sais, Elohîm le sait,
12:4	a été enlevé dans le paradis et a entendu des paroles ineffables<!--Qui ne peut être exprimé par des paroles (à cause de son caractère sacré).-->, ce qui n'est pas légal pour l'être humain d'en parler.
12:5	En faveur d’une telle personne, je me glorifierai ; mais en ma propre faveur, je ne me glorifierai pas - sinon dans mes faiblesses.
12:6	Car si je voulais me glorifier, je ne serais pas un insensé, car je dirais la vérité, mais je m'en abstiens, afin que personne ne m'estime au-dessus de ce qu'il me voit être, ou de ce qu'il entend dire de moi.

### L'écharde de Paulos (Paul)

12:7	Et de peur que je ne m'élève à cause de l'excellence de ces révélations, il m'a été mis une écharde<!--La nature exacte de l'écharde de Paulos (Paul) n'est pas précisée. Elle lui avait été infligée par un « ange de Satan », par la volonté d'Elohîm, pour le garder dans l'humilité.--> dans la chair, un ange de Satan pour me frapper, afin que je ne m'élève pas.
12:8	À ce sujet, trois fois j'ai prié le Seigneur afin qu’il s’éloigne de moi.
12:9	Et il m'a dit : Ma grâce te suffit, car ma puissance s'accomplit dans la faiblesse. Je me glorifierai donc très volontiers plutôt dans mes faiblesses, afin que la puissance du Mashiah fixe sa tente sur moi.
12:10	À cause de cela je prends plaisir dans les faiblesses, dans les injures, dans les difficultés, dans les persécutions, et dans les affreuses calamités en faveur du Mashiah, car quand je suis faible, c'est alors que je suis fort.
12:11	Je suis devenu insensé en me glorifiant, vous m'y avez contraint, car je devais être recommandé par vous. Car je n'ai été inférieur en aucune chose à ces grands apôtres, quoique je ne sois rien.
12:12	En effet, les signes d'un apôtre se sont accomplis parmi vous par une persévérance entière, par des signes, des prodiges et des miracles.
12:13	Car en quoi avez-vous été inférieurs aux autres assemblées, sinon en ce que je n’ai pas été un fardeau pour vous ? Pardonnez-moi cette injustice !
12:14	Voici que pour la troisième fois je suis prêt à aller chez vous, et je ne serai pas un fardeau, car ce ne sont pas vos biens que je cherche, mais vous-mêmes. Car ce ne sont pas les enfants qui doivent accumuler des richesses pour les parents, mais les parents pour les enfants<!--Des bons parents accumulent des richesses dans le but de préparer l'avenir de leurs enfants et non l'inverse.-->.
12:15	Mais moi, je dépenserai très volontiers et je me dépenserai entièrement en faveur de vos âmes, bien qu'en vous aimant au plus haut degré, je sois moins aimé.
12:16	Mais soit ! Je n'ai pas été une charge pour vous, mais, étant habile, je vous ai pris par ruse !
12:17	Est-ce que je vous ai dupés par quelqu'un de ceux que je vous ai envoyés ?
12:18	J'ai exhorté Titos, et j'ai envoyé le frère avec lui. Est-ce que Titos vous a dupés ? N'avons-nous pas marché dans le même esprit, sur les mêmes traces ?
12:19	Pensez-vous encore que nous nous défendons devant vous ? C'est devant Elohîm, en Mashiah, que nous parlons, et tout cela, mes bien-aimés, en faveur de votre construction.
12:20	Car j'ai peur qu'à mon arrivée, je ne vous trouve pas tels que je veux, et que moi je ne sois trouvé par vous tel que vous ne vouliez pas, et qu'il n'y ait des querelles, des jalousies, des colères, des esprits de parti<!--Voir Ph. 2:3.-->, des diffamations, des calomnies, des gonflements de l'âme et des désordres,
12:21	que, venant de nouveau chez vous, mon Elohîm ne m'humilie devant vous et que je n’aie à pleurer sur beaucoup de ceux qui ont péché précédemment, et qui ne se sont pas repentis de l'impureté, de relation sexuelle illicite et de la luxure sans bride<!--Voir commentaire en Mc. 7:22.--> qu'ils ont commises.

## Chapitre 13

13:1	C'est la troisième fois que je viens chez vous. Par la bouche de deux ou de trois témoins<!--De. 19:15 ; Mt. 18:15-16.-->, toute parole sera établie.
13:2	J'ai déjà dit, et je dis encore d'avance, comme si j'étais présent, quoique je sois maintenant absent, j'écris à ceux qui ont péché précédemment et à tous les autres, que si je viens de nouveau, je n'épargnerai rien.
13:3	Puisque vous cherchez la preuve que Mashiah parle par moi, lui qui n'est pas faible envers vous, mais qui est puissant en vous.
13:4	Car même s'il a été crucifié en raison de la faiblesse, il est néanmoins vivant par la puissance d'Elohîm. Car nous aussi, nous sommes faibles en lui, mais nous vivrons avec lui par la puissance qu'Elohîm a déployée envers vous.
13:5	Examinez-vous vous-mêmes, si vous êtes dans la foi. Éprouvez-vous vous-mêmes. Ne reconnaissez-vous pas que Yéhoshoua Mashiah est en vous ? À moins peut-être que vous ne soyez réprouvés<!--1 Co. 9:27.-->.
13:6	Mais j'espère que vous reconnaîtrez que pour nous, nous ne sommes pas réprouvés.
13:7	Et je prie Elohîm que vous ne fassiez aucun mal, non pour paraître nous-mêmes approuvés, mais afin que vous fassiez ce qui est bon et que nous soyons comme réprouvés.
13:8	Car nous n'avons pas de pouvoir contre la vérité, mais en faveur de la vérité.
13:9	Car nous nous réjouissons quand nous sommes faibles et que vous, vous êtes forts. Mais nous prions aussi pour votre perfectionnement.
13:10	C'est pourquoi j'écris ces choses étant absent, afin qu'étant présent je n'agisse pas sévèrement, selon l'autorité que le Seigneur m'a donnée pour la construction<!--Ep. 4:12.--> et non pour la destruction.

### Conclusion

13:11	Au reste, frères, réjouissez-vous, perfectionnez-vous, consolez-vous, ayez une même pensée, vivez en paix. Et l'Elohîm d'amour et de paix sera avec vous.
13:12	Saluez-vous les uns les autres par un saint baiser.
13:13	Tous les saints vous saluent.
13:14	Que la grâce du Seigneur Yéhoshoua Mashiah, l'amour d'Elohîm et la communion du Saint-Esprit soient avec vous tous ! Amen !
