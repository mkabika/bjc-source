# Yonah (Jonas) (Jon.)

Signification : Colombe

Auteur : Yonah (Jonas)

Thème : La miséricorde divine

Date de rédaction : 8ème siècle av. J.-C.

Yonah, contemporain d'Amowc (Amos), exerça son service dans le royaume du nord sous le règne de Yarobam II (Jéroboam II). Alors que YHWH lui ordonna d'aller prêcher la repentance à Ninive (capitale de l'Empire d'Assyrie), sous peine de la détruire, il refusa et tenta de s'enfuir loin de lui. Toutefois, dans sa miséricorde, Elohîm généra une situation pour pousser Yonah à accomplir sa mission. À travers ce récit, on comprend que la voie de la repentance est ouverte à tous et qu'Elohîm veut sauver tous les humains sans exception.

## Chapitre 1

### Appel, désobéissance et fuite de Yonah

1:1	La parole de YHWH apparut à Yonah, fils d'Amitthaï, en disant :
1:2	Lève-toi, va à Ninive<!--Capitale de l'Empire d'Assyrie.-->, la grande ville, et crie contre elle ! Car leur méchanceté est montée jusqu'à moi.
1:3	Yonah se leva pour s'enfuir à Tarsis, loin des faces de YHWH et il descendit à Yapho, où il trouva un navire qui allait à Tarsis. Il paya le prix du transport et y entra pour aller à Tarsis, loin des faces de YHWH.
1:4	Mais YHWH fit lever un grand vent sur la mer, et il y eut une grande tempête sur la mer, de sorte que le navire semblait se briser.
1:5	Les mariniers eurent peur, ils crièrent, chaque homme vers ses elohîm. Ils jetèrent dans la mer les objets qui étaient dans le navire pour l'alléger. Yonah descendit au fond du navire, se coucha et s'endormit profondément.
1:6	Le chef des marins s'approcha de lui et lui dit : Qu'as-tu dormeur ? Lève-toi, crie vers ton Elohîm ! Peut-être ton Elohîm pensera à nous et nous ne périrons pas.
1:7	Ils se dirent, chaque homme à son compagnon : Venez, tirons au sort pour savoir qui est la cause de ce malheur. Ils tirèrent au sort et le sort tomba sur Yonah.
1:8	Ils lui dirent : Dis-nous, s’il te plaît, qui nous attire ce mal. Quelle est ton occupation et d'où viens-tu ? Quelle est ta terre et de quel peuple es-tu ?
1:9	Il leur dit : Je suis hébreu et je crains YHWH, l'Elohîm des cieux, qui a fait la mer et la terre sèche.
1:10	Ces hommes eurent peur, une grande terreur et lui dirent : Pourquoi as-tu fait cela ? Car ces hommes savaient qu'il fuyait loin des faces de YHWH, parce qu'il le leur avait déclaré.
1:11	Ils lui dirent : Que te ferons-nous pour que la mer se calme envers nous ? Car la mer allait et tempêtait de plus belle.
1:12	Il leur dit : Prenez-moi et jetez-moi dans la mer, et la mer se calmera, car je sais que c’est moi qui attire sur vous cette grande tempête.
1:13	Ces hommes ramaient pour revenir sur la terre sèche, mais ils ne le purent, car la mer allait et tempêtait contre eux.
1:14	Ils invoquèrent YHWH et dirent : Oh YHWH, s’il te plaît, ne nous fais pas périr à cause de l'âme de cet homme, et ne mets pas sur nous le sang innocent ! Car toi, YHWH, tu agis comme il te plaît<!--Ps. 115:3.-->.
1:15	Ils prirent Yonah et le jetèrent dans la mer. Et la fureur de la mer s'arrêta.
1:16	Ces hommes eurent peur, une grande terreur de YHWH. Ils sacrifièrent des sacrifices à YHWH et firent des vœux.

## Chapitre 2

### Yonah dans le ventre du poisson

2:1	YHWH ordonna à un grand poisson d'engloutir Yonah, et Yonah fut dans le ventre du poisson trois jours et trois nuits.
2:2	Yonah pria YHWH, son Elohîm, dans le ventre du poisson.
2:3	Il dit : Dans ma détresse, j'ai invoqué YHWH et il m'a répondu. Du sein du shéol, j'ai crié au secours et tu as entendu ma voix<!--Ps. 18:5-7.-->.
2:4	Tu m'as jeté dans les profondeurs, au cœur de la mer, et le courant m'a environné. Tous tes flots et toutes tes vagues ont passé sur moi.
2:5	Je disais : Je suis chassé loin de tes yeux ! Cependant, je verrai encore le temple de ta sainteté.
2:6	Les eaux m'ont environné jusqu'à l'âme. L'abîme m'a enveloppé, les roseaux ont lié ma tête.
2:7	Je suis descendu jusqu'aux bases des montagnes, la terre fermait sur moi ses barres pour toujours, mais tu m'as fait monter vivant de la fosse, YHWH, mon Elohîm !
2:8	Quand mon âme s'était affaiblie en moi, je me suis souvenu de YHWH, et ma prière est parvenue à toi, jusqu'au palais de ta sainteté.
2:9	Ceux qui observent des vanités mensongères abandonnent ta miséricorde.
2:10	Moi, avec la voix de louange, je sacrifierai pour toi, j'accomplirai les vœux que j'ai faits. Le salut vient de YHWH<!--Le mot « salut », de l'hébreu « Yeshuw'ah », a la même racine que le Nom de notre Seigneur : « Yéhoshoua ». Yonah (Jonas) a compris que seul Yéhoshoua (Jésus) pouvait le sauver d'une mort certaine.-->.
2:11	YHWH parla au poisson, et le poisson vomit Yonah sur la terre sèche.

## Chapitre 3

### Repentance nationale à Ninive

3:1	La parole de YHWH apparut à Yonah une seconde fois, en disant :
3:2	Lève-toi, va à Ninive, la grande ville, et proclames-y la publication que je t'ordonne !
3:3	Yonah se leva et alla à Ninive, suivant la parole de YHWH. Or Ninive était une grande ville devant Elohîm, de trois jours de marche.
3:4	Yonah entra dans la ville et commença par y marcher toute une journée. Il criait et disait : Encore 40 jours, et Ninive sera renversée !
3:5	Les hommes de Ninive crurent en Elohîm, ils publièrent un jeûne et se vêtirent de sacs, depuis le plus grand d'entre eux jusqu'au plus petit.
3:6	Et cette parole parvint au roi de Ninive. Il se leva de son trône, ôta son manteau, se couvrit d'un sac et s'assit sur la cendre.
3:7	Il fit crier et dire dans Ninive, par décret du roi et de ses grands, en disant : Que les humains, les bêtes, les bœufs et les brebis, ne goûtent de rien, ne paissent pas et ne boivent pas d'eau !
3:8	Que les humains et les bêtes soient couverts de sacs, qu'ils crient à Elohîm avec force, et que chacun revienne de sa mauvaise voie, des actions violentes que ses mains ont commises !
3:9	Qui sait si Elohîm ne reviendra pas et ne se repentira pas, et s'il ne reviendra pas de la colère de ses narines, en sorte que nous ne périssions pas ?
3:10	Et Elohîm vit leurs œuvres : ils revenaient de leur mauvaise voie. Alors Elohîm se repentit du mal qu'il avait dit qu'il leur ferait et ne le fit pas.

## Chapitre 4

### La miséricorde est accordée à Ninive ; Yonah mécontent

4:1	Yonah le prit mal, très mal et il se fâcha.
4:2	Il pria YHWH et dit : Oh ! Je te prie YHWH, n'est-ce pas là ce que je disais quand j'étais encore sur mon sol ? C'est pourquoi je suis allé de l’avant et me suis enfui à Tarsis. Car je savais que tu es le El compatissant, miséricordieux, lent à la colère et grand en bonté, et qui te repens du mal<!--Joë. 2:13.-->.
4:3	Maintenant, YHWH, s’il te plaît, enlève-moi mon âme, car la mort vaut mieux pour moi que la vie.

### YHWH réprimande Yonah

4:4	YHWH dit : Fais-tu bien de te fâcher ?
4:5	Yonah sortit de la ville et s'assit à l'orient de la ville, là il se fit une cabane et y resta à l'ombre, jusqu'à ce qu'il vît ce qui arriverait à la ville.
4:6	YHWH Elohîm ordonna à un ricin de croître au-dessus de Yonah, pour donner de l'ombre sur sa tête et pour le délivrer de son mal. Yonah éprouva une grande joie à cause de ce ricin.
4:7	Elohîm prépara pour le lendemain, lorsque l'aube du jour monta, un ver qui frappa le ricin et il sécha.
4:8	Il arriva que quand le soleil fut levé, Elohîm prépara un vent chaud d'orient qu'on n'apercevait pas, et le soleil frappa la tête de Yonah, au point qu'il s'évanouit. Il demanda la mort pour son âme et dit : La mort vaut mieux pour moi que la vie.
4:9	Elohîm dit à Yonah : Fais-tu bien de te fâcher ainsi au sujet de ce ricin ? Il dit : Je fais bien de me fâcher jusqu'à la mort.
4:10	Et YHWH dit : Toi, tu as pitié d’un ricin pour lequel tu n'as pas travaillé et que tu n'as pas fait croître, qui a été le fils d'une nuit et a péri comme le fils d'une nuit.
4:11	Et moi, je n’aurais pas pitié de Ninive, cette grande ville, où il y a plus de 120 000 humains qui ne connaissent ni leur droite ni leur gauche, et des animaux en grand nombre !
