#!/bin/bash

error=false
files="[0-9][0-9]-*.md"

echo "Trouver les doubles espaces"
if grep --exclude README.md --color=always -P " {2,}." $files; then
  error=true
fi
echo "Trouver les ponctuations collées entre elles avec un commentaire au milieu"
if grep --exclude README.md --color=always -P '[\.\?\;\!-,] *<!--.+--> *[\.\?\;\!-,]' $files; then
  error=true
fi
if grep --exclude README.md --color=always -P ': *<!--.+--> *:' $files; then
  error=true
fi

echo "Trouver les ponctuations écrits en double"
if grep --exclude README.md --color=always -P "\? *\?" $files; then
  error=true
fi
if grep --exclude README.md --color=always -P "\! *\!" $files; then
  error=true
fi
if grep --exclude README.md --color=always -P ": *:" $files; then
  error=true
fi


if $error; then
  # red
  echo -e "\033[0;31mTests failed\033[0m"
  exit 1
else
  # green
  echo -e "\033[0;32mSuccess\033[0m"
  exit 0
fi
