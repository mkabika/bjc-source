# Obadyah (Abdias) (Ab.)

Signification : Adorateur ou serviteur de Yah

Auteur : Obadyah (Abdias)

Thème : Condamnation d'Édom

Date de rédaction : 6ème siècle av. J.-C.

Prophète ayant exercé son service en Yéhouda (Juda), Obadyah reçut un message court, mais clair sur le jour de YHWH, et plus particulièrement sur le jugement d'Édom à la suite de ses violences envers Israël.

## Chapitre 1

### Malédiction prononcée sur Édom

1:1	La vision d'Obadyah. Ainsi parle Adonaï YHWH sur Édom : Nous avons entendu une publication de la part de YHWH, et un messager a été envoyé parmi les nations : Levez-vous ! Levons-nous contre ce peuple pour lui faire la guerre<!--Jé. 49:14-16.--> !
1:2	Voici, je te rendrai petit parmi les nations, tu seras très méprisé.
1:3	L'orgueil de ton cœur t'a séduit, toi qui habites dans les fentes des rochers, qui sont ta haute demeure, et qui dis en ton cœur : Qui me renversera par terre ?
1:4	Quand tu élèverais ton nid comme l'aigle, et quand bien même tu le mettrais entre les étoiles, je te jetterai de là par terre, – déclaration de YHWH.
1:5	Si des voleurs entraient chez toi ou des pillards de nuit (comme tu es ravagé !) n'auraient-ils pas dérobé jusqu'à ce qu'ils en aient eu assez ? Si des vendangeurs venaient chez toi, ne laisseraient-ils pas des grappillages ?
1:6	Comment a été fouillé Ésav ? Comment ses trésors cachés ont été découverts ?
1:7	Tous les hommes de ton alliance t'ont chassé jusqu'à la frontière. Les hommes qui étaient en paix avec toi t'ont trompé, ils t'ont vaincu. Ils se sont servis de ton pain comme d'un piège pour toi, par-dessous : Il n'y a plus d'intelligence en lui !
1:8	N'est-ce pas en ce jour-là, – déclaration de YHWH –, que je ferai périr les sages au milieu d'Édom, et l'intelligence de la montagne d'Ésav ?
1:9	Tes guerriers seront effrayés, Théman ! afin qu'ils soient tous retranchés de la montagne d'Ésav par le carnage<!--Ez. 25:13.-->.

### Les causes de sa malédiction

1:10	À cause de la violence contre ton frère Yaacov, la honte te couvrira, et tu seras retranché à jamais.
1:11	Le jour où tu te tenais vis-à-vis de lui, le jour où des étrangers emmenaient captive son armée, où des étrangers entraient dans ses portes et jetaient le sort sur Yeroushalaim, toi aussi, tu étais comme l'un d'eux.
1:12	Tu n’avais pas à regarder le jour de ton frère, le jour de son désastre, ni te réjouir sur les fils de Yéhouda, le jour où ils ont été détruits, ni ouvrir une grande bouche le jour de la détresse !
1:13	N'entre pas dans la porte de mon peuple le jour de sa détresse ! Ne regarde pas son malheur le jour de sa détresse ! Ne te jette pas sur ses richesses le jour de sa détresse !
1:14	Ne te tiens pas sur les passages pour exterminer ses fugitifs ! N’enferme pas ses survivants au jour de la détresse !

### Châtiment d'Édom au jour de YHWH

1:15	Car le jour de YHWH est proche pour toutes les nations. Et l'on te traitera comme tu as traité les autres : ta récompense retombera sur ta tête<!--Jé. 50:15-29 ; Ez. 35:15.-->.
1:16	Car comme vous avez bu sur la montagne de ma sainteté, ainsi toutes les nations boiront continuellement. Elles boiront, elles avaleront et elles seront comme si elles n'avaient jamais existé<!--Jé. 25:15-28.-->.

### Édom jugé, Yaacov délivré

1:17	Mais la délivrance<!--La délivrance sera sur la montagne de Sion. Cette prophétie fait allusion au Royaume messianique. Voir Ro. 11:26.--> sera sur la montagne de Sion, elle deviendra sainte, et la maison de Yaacov héritera de ses possessions.
1:18	La maison de Yaacov deviendra un feu, et la maison de Yossef une flamme, mais la maison d'Ésav du chaume, qu'elles allumeront et dévoreront, et il n'y aura aucun survivant pour la maison d'Ésav, car YHWH a parlé.
1:19	Ceux du midi hériteront de la montagne d'Ésav, et ceux de la plaine, les Philistins. Ils hériteront le champ d'Éphraïm, le champ de Samarie, et Benyamin, Galaad.
1:20	Les exilés de cette armée des fils d'Israël, des Kena'ânéens jusqu'à Sarepta, et les exilés de Yeroushalaim qui sont à Sépharad hériteront les villes du midi.
1:21	Des sauveurs monteront à la montagne de Sion pour juger la montagne d'Ésav, et le royaume sera à YHWH.
