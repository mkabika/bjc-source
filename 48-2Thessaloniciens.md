# 2 Thessaloniciens (2 Th.)

Auteur : Paulos (Paul)

Thème : Le jour du Mashiah

Date de rédaction : Env. 51 ap. J.-C.

Autrefois appelée Therme ou Therma, qui signifie « source chaude », Thessalonique reçut son nouveau nom de Cassandre, en l'honneur de sa femme Thessalonike qui était aussi la sœur d'Alexandre le Grand (356 – 323 av. J.-C.), à qui il succéda.

Cette ville est située au nord de la Grèce actuelle, sur la côte de la Mer Égée. Du temps de Paulos (Paul), la Grèce était divisée en deux parties. Dans la région du nord, la Macédoine, se trouvaient les villes de Philippes, Thessalonique et Bérée. Quant à la région du sud, l'Achaïe, elle comportait les villes d'Athènes et de Corinthe. 

La seconde lettre de Paulos aux Thessaloniciens fut rédigée peu de temps après la première. Elle fut motivée par des troubles survenus dans la communauté à la suite d'une annonce basée sur une lettre faussement attribuée à Paulos prétendant que le « jour du Seigneur » était arrivé. Dans cette seconde lettre, l'apôtre exhorte les chrétiens de Thessalonique à tenir ferme dans leur foi malgré la persécution, leur expliquant que le « jour du Mashiah (Christ) » devait être précédé par l'apostasie et la venue de l'homme impie. Il conclut sa lettre en demandant aux chrétiens de s'éloigner de ceux qui vivent dans le désordre.

## Chapitre 1

### Introduction

1:1	Paulos, et Silvanos, et Timotheos à l'assemblée des Thessaloniciens<!--Thessalonique. Voir Ac. 17:1-9.--> qui est en Elohîm notre Père et Seigneur Yéhoshoua Mashiah :
1:2	à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !

### La persévérance dans l'affliction ; Elohîm, le juste Juge

1:3	Frères, nous devons toujours rendre grâces à Elohîm à votre sujet, comme il est juste, parce que votre foi augmente au-delà de la mesure et que l'amour de chacun de vous tous envers les autres se multiplie,
1:4	de sorte que nous-mêmes, nous nous glorifions de vous dans les assemblées d'Elohîm, en faveur de votre persévérance et de votre foi, au milieu de toutes vos persécutions et des tribulations que vous supportez.
1:5	C'est la preuve du juste jugement d'Elohîm, pour que vous soyez jugés dignes du Royaume d'Elohîm, en faveur duquel aussi vous souffrez.

### La fin de ceux qui ne connaissent pas Elohîm et qui n'obéissent pas à l'Évangile

1:6	Car il est juste devant Elohîm qu'il rende la tribulation à ceux qui vous oppressent,
1:7	et à vous, qui êtes oppressés, le repos avec nous, lors de la révélation<!--Révélation, du grec « apokalupsis », signifie « mettre à nu, révélation d'une vérité ». Le fait de rendre visible ce qui était caché.--> du Seigneur Yéhoshoua du ciel avec les anges de sa puissance,
1:8	dans une flamme de feu, exerçant la vengeance sur ceux qui ne connaissent pas Elohîm et sur ceux qui n'obéissent pas à l'Évangile de notre Seigneur Yéhoshoua Mashiah.
1:9	Ils auront pour juste châtiment une destruction éternelle, loin de la face du Seigneur et de la gloire de sa force,
1:10	quand il viendra pour être glorifié en ce jour-là dans ses saints, et pour être admiré dans tous ceux qui croient, parce que le témoignage que nous avons rendu auprès de vous a été cru.
1:11	C'est aussi pour cela que nous prions toujours pour vous, afin que notre Elohîm vous juge dignes de la vocation et qu'il accomplisse puissamment en vous, tout le bon plaisir de sa bonté et l'œuvre de la foi,
1:12	afin que le Nom de notre Seigneur Yéhoshoua Mashiah soit glorifié en vous, et vous en lui, selon la grâce de notre Elohîm et Seigneur Yéhoshoua Mashiah.

## Chapitre 2

### Le jour du Seigneur et l'apparition de l'homme impie

2:1	Mais nous vous supplions frères, en faveur de la parousie<!--La parousie du Seigneur Yéhoshoua ha Mashiah. Voir Mt. 24:1-3.--> de notre Seigneur Yéhoshoua Mashiah et de notre rassemblement<!--Voir commentaire en Hé. 10:25.--> auprès de lui,
2:2	de ne pas être rapidement agités hors de votre pensée ni troublés par un esprit<!--Voir 1 Ti. 4:1. Mouvement de l'air ou des manifestations de l'Esprit.-->, par une parole ou par une lettre comme si c’était par nous, comme quoi le jour du Mashiah était imminent.
2:3	Que personne ne vous trompe d'aucune manière. Car il faut d'abord que vienne l'apostasie et que se révèle l'homme<!--Il est question ici de l'homme impie, de l'Anti-Mashiah (Antichrist), qui est la bête qui monte de la mer décrite par Yohanan (Ap. 13:11-18). Voir aussi Da. 11:36-38.--> de péché, le fils de la perdition,
2:4	l’adversaire et celui qui s’élève contre tout ce que l'on nomme Elohîm ou que l'on adore, il va jusqu'à s'asseoir comme Elohîm dans le temple d'Elohîm<!--D'après ce passage, le troisième temple de Yeroushalaim (Jérusalem) sera bel et bien reconstruit. Actuellement, des Juifs religieux militent activement pour la réalisation de ce projet. L'organisation juive la plus connue est l'Institut du temple (fondé en 1987), qui a déjà restauré un grand nombre d'objets servant au culte. Toutefois, il ne faut pas sous-estimer la ruse de Satan, car au-delà du temple physique, il cherche prioritairement à s'asseoir dans les temples spirituels que sont les chrétiens (1 Co. 6:19). Pour parvenir à ses fins, Satan a envoyé plusieurs de ses émissaires pour prêcher un autre évangile et un autre mashiah (christ). C'est ainsi que de nombreuses assemblées, séduites et captivées par de faux docteurs, n'ont plus Yéhoshoua ha Mashiah (Jésus-Christ) comme Seigneur, mais Satan en personne. L'apostasie étant installée premièrement dans les cœurs, l'Anti-Mashiah (Antichrist) n'aura donc aucun mal à se faire passer pour le Mashiah et à s'asseoir dans le temple physique, où il usurpera l'adoration qui revient à l'Elohîm véritable.--> se montrant lui-même comme étant Elohîm.
2:5	Ne vous souvenez-vous pas que je vous disais ces choses lorsque j'étais encore chez vous ?
2:6	Et maintenant vous savez ce qui le retient afin qu'il ne soit révélé qu'en son temps.
2:7	Car le mystère de la violation de la torah<!--Le mystère de la violation de la torah. Paulos (Paul) nous enseigne que ce mystère était déjà à l'œuvre au sein des assemblées primitives. Le prophète Zekaryah (Zacharie), au chapitre 5 de son livre éponyme, l'avait personnifié en relatant une vision dans laquelle il avait vu « deux femmes avec des ailes comme les ailes de la cigogne » emportant l'épha de l'iniquité des fils d'Israël. Sur cet épha était assise une femme personnifiant l'iniquité, c'est-à-dire la femme de l'homme impie, la Babel (Babylone) religieuse. Ces deux femmes aux ailes comme les ailes de la cigogne allaient lui bâtir une maison sur la terre de Shinéar (Babylonie selon Ge. 10:6-14).--> est déjà à l'œuvre, seulement celui qui le retient en ce moment le fera jusqu'à ce qu'il soit hors du chemin.
2:8	Et alors sera révélé le violeur de la torah<!--« Destitué de la torah », « sans loi », « violeur de la torah », « méchant ». Voir Mc. 15:28.--> que le Seigneur détruira par l'Esprit de sa bouche<!--Es. 11:4.--> et qu'il anéantira par l'apparition de sa parousie.
2:9	Lui dont la parousie<!--Il y a une autre parousie, celle de l'homme impie.--> est selon l'efficacité de Satan, avec toute puissance, avec des signes et des prodiges de mensonge,
2:10	et avec toutes les séductions de l'injustice pour ceux qui périssent parce qu'ils n'ont pas reçu l'amour de la vérité pour être sauvés.
2:11	Et à cause de cela, Elohîm leur envoie l'efficacité d'égarement<!--L'esprit d'égarement. Voir De. 28:28 ; 1 R. 22 ; Ro. 1:24-28.--> pour qu'ils croient au mensonge,
2:12	afin que tous ceux qui n'ont pas cru à la vérité, mais qui ont pris plaisir à l'injustice soient condamnés.

### Encouragements

2:13	Mais pour nous, frères bien-aimés du Seigneur, nous devons toujours rendre grâce à Elohîm pour vous parce qu'Elohîm vous a élus dès le commencement pour le salut par la sanctification de l'Esprit et par la foi en la vérité.
2:14	C'est à cela qu'il vous a appelés par le moyen de notre Évangile, pour l'acquisition de la gloire de notre Seigneur Yéhoshoua Mashiah.
2:15	Ainsi donc, frères, demeurez fermes et retenez les traditions qui vous ont été enseignées, soit de vive voix, soit par notre lettre.
2:16	Mais que lui-même, notre Seigneur Yéhoshoua Mashiah et notre Elohîm et Père, qui nous a aimés et qui nous a donné une consolation éternelle et une bonne espérance par sa grâce,
2:17	console vos cœurs et vous affermisse en toute bonne parole et en toute bonne œuvre.

## Chapitre 3

3:1	Au reste, frères, priez au sujet-de nous, afin que la parole du Seigneur coure et qu'elle soit glorifiée, comme aussi chez vous,
3:2	et que nous soyons délivrés des gens méchants et pervers, car tous n'ont pas la foi.
3:3	Mais le Seigneur est fidèle, il vous affermira et vous gardera du mal.
3:4	Et nous avons à votre égard cette confiance dans le Seigneur que vous faites et que vous ferez les choses que nous vous ordonnons.
3:5	Mais que le Seigneur dirige vos cœurs vers l'amour d'Elohîm et vers la persévérance du Mashiah !

### Se séparer des mauvaises compagnies ; être un modèle ; subvenir à ses besoins

3:6	Mais nous vous ordonnons frères, au Nom de notre Seigneur Yéhoshoua Mashiah, de vous éloigner<!--La séparation d'avec la mauvaise compagnie. Voir 1 Co. 5:9-13, 15:33 ; 2 Co. 6:14-18 ; Ro. 16:17-18 ; Tit. 3:10-11 ; 2 Jn. 1:2-11. S'abstenir de relation familière avec quelqu'un.--> de tout frère qui marche dans le désordre<!--Désordonné, hors des rangs (souvent pour des soldats), irrégulier, démesuré, plaisirs immodérés, déviant de l'ordre prescrit ou des règlements. Utilisé dans la société Grecque pour ceux qui ne pouvaient attester d'un travail.--> et non selon la tradition qu'il a reçue de nous.
3:7	Car vous savez vous-mêmes comment il faut nous imiter, parce que nous n'avons pas mené une vie désordonnée parmi vous
3:8	et que nous n'avons mangé gratuitement le pain de personne. Mais dans le labeur et dans le travail dur et difficile, nous avons travaillé nuit et jour pour n'être à la charge<!--Les véritables ouvriers d'Elohîm ne s'attendent pas aux êtres humains pour avoir leur salaire. Ils mettent leur confiance en Elohîm qui est leur rémunérateur. Voir Ac. 20:33-35.--> d'aucun de vous.
3:9	Ce n'est pas que nous n'en ayons pas le droit, mais c’est pour vous donner en nous-mêmes un modèle à imiter.
3:10	Car aussi, lorsque nous étions avec vous, nous vous ordonnions ceci, que si quelqu'un ne veut pas travailler qu'il ne mange pas non plus.
3:11	Car nous apprenons qu’il y en a quelques-uns parmi vous qui marchent dans le désordre, qui ne travaillent en rien mais qui s'occupent de choses insignifiantes<!--Utilisé apparemment pour une personne qui s'enquiert officieusement des affaires des autres.-->.
3:12	Mais nous ordonnons à ceux qui sont tels, et nous les exhortons par notre Seigneur Yéhoshoua Mashiah, à manger leur propre pain en travaillant avec tranquillité<!--Description de la vie de celui qui reste chez lui à faire son travail, et ne se mêle pas des affaires des autres.-->.
3:13	Mais pour vous, frères, ne perdez pas courage en faisant le bien.
3:14	Et si quelqu'un n'obéit pas à notre parole par le moyen de cette lettre, faites-le connaître, et ne vous mêlez pas à lui afin qu'il éprouve de la honte.
3:15	Et ne le considérez pas comme un ennemi, mais avertissez-le comme un frère.

### Conclusion

3:16	Mais que le Seigneur de paix lui-même vous donne la paix en tout temps de toute manière ! Que le Seigneur soit avec vous tous !
3:17	La salutation est de ma propre main, à moi Paulos, ce qui est un signe dans toutes mes lettres, c'est ainsi que j'écris.
3:18	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous tous ! Amen !
