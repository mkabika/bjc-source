# Eikha (Lamentations de Jérémie) (La.)

Signification : Comment !

Auteur : Yirmeyah (Jérémie)

Thème : Affliction pour Yeroushalaim (Jérusalem)

Date de rédaction : 6ème siècle av. J.-C.

Recueil de pièces poétiques, les Lamentations de Yirmeyah furent composées selon un procédé visant à accentuer le caractère funèbre, de façon à ce qu'elles soient récitées avec gémissements. Ses complaintes exposent la profonde désolation du prophète Yirmeyah face au fardeau du peuple qu'il portait dans ses entrailles tout comme la douleur et la tristesse de YHWH face à Israël.

Très différentes des prophéties retrouvées dans le livre de Yirmeyah, les Lamentations reflètent l'affliction convenant à la gravité du châtiment subi : famine, pillage et ruine du temple, déportation, cessation du culte, diverses calamités... Yirmeyah rappelle ainsi les conséquences de l'endurcissement du cœur face aux appels à la repentance. Il présente aussi les bontés éternelles de YHWH.

## Chapitre 1

### Pleurs et désolation de Yeroushalaim (Jérusalem)

1:1	[Aleph.] Comment est-il arrivé que la ville si peuplée se trouve si solitaire ? Que celle qui était grande entre les nations est devenue comme une veuve ? Que celle qui était noble dame entre les provinces a été rendue tributaire ?
1:2	[Beth.] Elle pleure, elle pleure pendant la nuit et ses larmes sont sur ses joues. Il n'y a pas un de tous ses amis qui la console. Tous ses amis l'ont trahie, ils sont devenus ses ennemis.
1:3	[Guimel.] Yéhouda a été emmenée en captivité, elle est dans l'affliction et dans une grande servitude. Elle demeure maintenant parmi les nations, et ne trouve pas de repos. Tous ses persécuteurs l'ont atteinte dans sa détresse<!--Jé. 52:26.-->.
1:4	[Daleth.] Les chemins de Sion sont dans le deuil parce que personne ne vient plus aux fêtes solennelles. Toutes ses portes sont désolées, ses prêtres sanglotent, ses vierges sont accablées de tristesse, elle est remplie d'amertume.
1:5	[He.] Ses adversaires sont devenus la tête<!--Ce mot vient de l'hébreu « rosh » qui signifie aussi « dessus ». Voir De. 28:43-44.-->, ses ennemis tranquilles, car YHWH l'a humiliée à cause de la multitude de ses transgressions. Ses petits enfants ont marché captifs devant l'adversaire<!--Jé. 30:14.-->.
1:6	[Vav.] Toute la splendeur de la fille de Sion est sortie d'elle ; ses chefs sont devenus semblables à des cerfs qui ne trouvent pas de pâture et qui fuient sans force devant celui qui les poursuit.
1:7	[Zayin.] Yeroushalaim, dans les jours de son affliction et de son inquiétude, s'est souvenue de toutes ses choses précieuses qu'elle avait dès les jours anciens. Lorsque son peuple est tombé par la main de l'ennemi, sans aucun secours, les ennemis l'ont vue et se sont moqués de ses shabbats.
1:8	[Heth.] Yeroushalaim a péché, elle a péché, c'est pourquoi elle est devenue un objet de dégoût. Tous ceux qui l'honoraient l'ont méprisée, parce qu'ils ont vu son ignominie. Elle-même gémit et retourne en arrière.
1:9	[Teth.] Sa souillure était dans les pans de sa robe, et elle ne s'est pas souvenue de sa fin. Elle a été extraordinairement abaissée, et elle n'a pas de consolateur. Vois ma misère, YHWH, car l'ennemi s'est élevé avec orgueil !
1:10	[Yod.] L'ennemi a étendu sa main sur toutes ses choses désirables. Car elle a vu entrer dans son sanctuaire les nations au sujet desquelles tu avais donné cet ordre : elles n'entreront pas dans ton assemblée<!--De. 23:4.-->.
1:11	[Kaf.] Tout son peuple gémit, cherchant du pain<!--Jé. 52:6.-->. Ils ont donné leurs choses désirables pour des aliments, afin de ranimer leur âme. Vois, YHWH ! Regarde combien je suis méprisée.
1:12	[Lamed.] Cela ne vous touche-t-il pas vous tous passants ? Contemplez et voyez s'il est une douleur comme ma douleur, celle dont j'ai été frappée, moi que YHWH a accablée de douleur au jour de l'ardeur de sa colère !
1:13	[Mem.] D'en haut il a envoyé un feu dans mes os qui les domine, il a tendu un filet sous mes pieds et m'a fait revenir en arrière. Il m'a mise dans la désolation, dans une souffrance de l'âme de tous les jours.
1:14	[Noun.] Le joug de mes transgressions est lié par sa main, elles sont entrelacées et appliquées sur mon cou. Il a renversé ma force. Adonaï m'a livrée entre les mains de ceux contre qui je ne pourrai pas me lever.
1:15	[Samech.] Adonaï a abattu tous les hommes vaillants que j'avais au milieu de moi. Il a appelé contre moi, au temps fixé, une armée pour détruire mes jeunes hommes. Adonaï a foulé au pressoir la vierge, fille de Yéhouda.
1:16	[Ayin.] À cause de ces choses, je pleure. Mon œil, mon œil se fond en larmes, car le consolateur qui restaurait mon âme est loin de moi. Mes fils sont dans la désolation parce que l'ennemi a été plus fort.
1:17	[Pe.] Sion a étendu les mains et personne ne l'a consolée. YHWH a ordonné aux ennemis de Yaacov de l'entourer de toutes parts. Yeroushalaim a été comme une impureté au milieu d'eux.
1:18	[Tsade.] YHWH est juste car j'ai été rebelle à ses ordres. Écoutez, s'il vous plaît, vous tous, peuples, et voyez ma douleur ! Mes vierges et mes jeunes hommes sont allés en captivité.
1:19	[Qof.] J'ai appelé mes amis, mais ils m'ont trompée. Mes prêtres et mes anciens sont morts dans la ville : ils cherchaient de la nourriture afin de restaurer leur âme.
1:20	[Resh.] Regarde YHWH car je suis dans la détresse ! Mes entrailles bouillonnent, mon cœur palpite au-dedans de moi, car j'ai été rebelle, rebelle ! Au-dehors l'épée m'a privée d'enfants, au-dedans, c'est comme la mort.
1:21	[Shin.] On m’entend gémir... Personne qui me console ! Tous mes ennemis entendent mon malheur, ils se réjouissent de ce que tu l'as fait. Tu feras venir le jour que tu as proclamé, et ils seront comme moi.
1:22	[Tav.] Que toute leur méchanceté vienne devant toi. Et traite-les comme tu m'as traitée à cause de toutes mes transgressions, car mes sanglots sont en grand nombre et mon cœur est languissant.

## Chapitre 2

### Le jour de la colère de YHWH

2:1	[Aleph.] Comment est-il arrivé qu'Adonaï a couvert de sa colère la fille de Sion tout à l'entour, comme d'une nuée, et qu'il a précipité des cieux sur la Terre la beauté d'Israël, et ne s'est pas souvenu du marchepied de ses pieds<!--Ez. 43:7.--> au jour de sa colère ?
2:2	[Beth.] Adonaï a englouti sans épargner tous les pâturages de Yaacov. Il a dans sa fureur renversé les forteresses de la fille de Yéhouda, il les a jetées par terre. Il a profané le royaume et ses chefs.
2:3	[Guimel.] Il a retranché toute la force d'Israël par l'ardeur de sa colère. Il a ramené sa droite en arrière devant l'ennemi. Il s'est allumé dans Yaacov comme un feu flamboyant qui le consume de toutes parts.
2:4	[Daleth.] Il a tendu son arc comme un ennemi, sa droite s'est dressée comme celle d'un adversaire. Il a tué tout ce qui était agréable à l'œil dans la tente de la fille de Sion. Il a répandu sa fureur comme un feu.
2:5	[He.] Adonaï a été comme un ennemi. Il a englouti Israël, il a englouti tous ses palais, il a détruit toutes ses forteresses. Il a multiplié chez la fille de Yéhouda le deuil et l'affliction.
2:6	[Vav.] Il a mis en pièces avec violence sa tente comme un jardin ; il a détruit le lieu de son assemblée. YHWH a fait oublier dans Sion la fête solennelle et le shabbat, et dans sa violente colère, il a rejeté le roi et le prêtre.
2:7	[Zayin.] Adonaï a rejeté au loin son autel, il a dédaigné son sanctuaire. Il a livré entre les mains de l'ennemi les murailles de ses palais. Ils ont poussé des cris dans la maison de YHWH, comme aux jours des fêtes solennelles.
2:8	[Heth.] YHWH avait projeté de détruire les murailles de la fille de Sion. Il a étendu le cordeau, il n'a pas fait revenir sa main sans les avoir engloutis. Il a plongé dans le deuil remparts et murailles, ils ont été ruinés tous ensemble.
2:9	[Teth.] Ses portes sont enfoncées dans la terre, il en a détruit et brisé les barres. Son roi et ses chefs sont parmi les nations. Il n'y a plus de torah. Même les prophètes ne reçoivent plus aucune vision de YHWH<!--Ez. 7:26.-->.
2:10	[Yod.] Les anciens de la fille de Sion sont assis à terre, ils sont muets. Ils ont couvert leur tête de poussière, ils se sont ceints de sacs. Les vierges de Yeroushalaim baissent leurs têtes vers la terre.
2:11	[Kaf.] Mes yeux se consument à force de larmes, mes entrailles bouillonnent, mon foie se répand sur la terre à cause des ruines de la fille de mon peuple, des enfants et des nourrissons qui tombent en défaillance dans les rues de la ville.
2:12	[Lamed.] Ils disaient à leur mère : Où y a-t-il du blé et du vin ? Et ils tombaient comme morts dans les rues de la ville, comme un homme blessé mortellement, ils rendaient l'âme sur le sein de leur mère.
2:13	[Mem] Quel témoignage t'apporterai-je ? À qui te comparer, fille de Yeroushalaim ? Qui pourrait t'égaler, et quelle consolation te donner, vierge, fille de Sion ? Car ta ruine est grande comme une mer : qui pourrait te guérir<!--Es. 51:19-20.--> ?
2:14	[Noun.] Tes prophètes ont eu pour toi des visions vaines et insensées. Ils n'ont pas dévoilé ton iniquité afin de détourner ta captivité. Ils t'ont prophétisé des oracles mensongers et trompeurs<!--Jé. 2:8, 5:31, 14:14.-->.
2:15	[Samech.] Tous les passants claquent des paumes contre toi, ils sifflent, ils secouent leur tête contre la fille de Yeroushalaim, en disant : Est-ce ici la ville de laquelle on disait : La parfaite en beauté, la joie de toute la Terre<!--Na. 3:19.--> ?
2:16	[Pe.] Tous tes ennemis ouvrent la bouche contre toi, ils sifflent, ils grincent des dents, ils disent : Nous l'avons engloutie ! C'est ici le jour que nous attendions, nous l'avons atteint, nous le voyons !
2:17	[Ayin.] YHWH a fait ce qu'il avait projeté, il a accompli sa parole qu'il avait ordonnée depuis les jours anciens. Il a détruit sans épargner, il a fait de toi la joie de l'ennemi, il a donné de la force à tes adversaires.
2:18	[Tsade.] Leur cœur crie à Adonaï. Muraille de la fille de Sion, fais couler des larmes jour et nuit, comme un torrent<!--Jé. 14:17.--> ! Ne te donne pas de repos et que la fille de tes yeux ne se repose pas !
2:19	[Qof.] Lève-toi, pousse des cris dès le commencement des veilles de la nuit ! Répands ton cœur comme de l'eau devant les faces d'Adonaï ! Lève tes paumes vers lui pour l'âme de tes enfants qui meurent de faim aux coins de toutes les rues !
2:20	[Resh.] Vois, YHWH ! Regarde qui tu as ainsi traité ! Les femmes n'ont-elles pas mangé leur fruit : leurs petits enfants objets de leur tendresse ? Le prêtre et le prophète n'ont-ils pas été tués dans le sanctuaire d'Adonaï<!--Lé. 26:29 ; De. 28:53 ; Jé. 19:9.--> ?
2:21	[Shin.] Les jeunes hommes et les vieillards sont couchés par terre dans les rues, mes vierges et mes jeunes hommes sont tombés par l'épée. Tu as tué au jour de ta colère, tu as massacré sans épargner.
2:22	[Tav.] Tu as convié comme pour un jour solennel mes frayeurs de toutes parts. Au jour de la colère de YHWH, il n'y a eu ni rescapé ni survivant. Ceux que j'avais langés et élevés, mon ennemi les a consumés.

## Chapitre 3

### Yirmeyah (Jérémie) partage l'affliction des siens

3:1	[Aleph.] Je suis l'homme fort qui a vu l'affliction par la verge de sa fureur<!--Jé. 15:15-18.-->.
3:2	Il m'a conduit et fait marcher dans les ténèbres et non dans la lumière.
3:3	En effet, c'est contre moi qu'il a tout le jour tourné et retourné sa main.
3:4	[Beth.] Il a fait vieillir ma chair et ma peau, il a brisé mes os<!--Es. 38:13.-->.
3:5	Il a bâti contre moi, il m'a environné de venin et de détresse.
3:6	Il me fait habiter dans les lieux ténébreux, comme ceux qui sont morts depuis longtemps.
3:7	[Guimel.] Il a fait une cloison autour de moi afin que je ne sorte pas ; il a appesanti mes chaînes.
3:8	Même quand je crie et que j'élève ma voix, il arrête ma prière.
3:9	Il a muré mon chemin avec des pierres de taille, il a déformé mes sentiers.
3:10	[Daleth.] Il a été pour moi un ours en embuscade, un lion dans un lieu caché<!--Os. 13:8.-->.
3:11	Il a détourné mes chemins, il m'a mis en pièces, il m'a mis dans la désolation.
3:12	Il a tendu son arc et il m'a placé comme une cible pour sa flèche.
3:13	[He.] Il a fait entrer dans mes reins les fils de son carquois.
3:14	Je suis devenu un objet de dérision pour tout mon peuple, leur musique<!--Ps. 69:13 ; Job 30:9.--> tout le jour.
3:15	Il m'a rassasié d'amertume, il m'a enivré d'absinthe.
3:16	[Vav.] Il a brisé mes dents avec du gravier, il m'a couvert de cendres.
3:17	Tu as rejeté mon âme loin de la paix, j’ai oublié le bonheur !
3:18	Et j'ai dit : Ma force est perdue, et mon espérance aussi que j'avais en YHWH.
3:19	[Zayin.] Souviens-toi de mon affliction et de mon inquiétude : absinthe et fiel !
3:20	Elle se rappelle, mon âme, elle se rappelle, et elle est abattue en moi.
3:21	Je rappellerai ces choses à mon cœur, c'est pourquoi j'aurai de l'espérance :
3:22	[Heth.] non, les bontés de YHWH ne sont pas finies, non, ses compassions ne sont pas épuisées<!--Ps. 103:10.-->.
3:23	Elles sont nouvelles chaque matin. Grande est ta fidélité !
3:24	YHWH est ma portion, dit mon âme, c'est pourquoi j'espère en lui<!--Ps. 16:5.-->.
3:25	[Teth.] YHWH est bon pour ceux qui espèrent en lui, pour l'âme qui le cherche.
3:26	Il est bon d’attendre en silence la délivrance de YHWH.
3:27	Il est bon pour l'homme fort de porter le joug dans sa jeunesse.
3:28	[Yod.] Qu’il s’assoie solitaire et silencieux, car c’est ce qui lui est imposé.
3:29	Qu’il mette sa bouche dans la poussière ! Peut-être y aura-t-il de l’espoir.
3:30	Qu’il présente la joue à celui qui le frappe, qu’il soit rassasié d’insultes !
3:31	[Kaf.] Car Adonaï ne rejette pas pour toujours<!--Es. 57:16 ; Ps. 77:8.-->.
3:32	Mais s'il afflige quelqu'un, il a aussi compassion selon la grandeur de sa miséricorde.
3:33	Car ce n'est pas sa volonté d'affliger et d'humilier les fils d'humains.
3:34	[Lamed.] Lorsqu'on foule aux pieds tous les prisonniers de la Terre,
3:35	lorsqu'on pervertit<!--Ou violer le droit, la justice humaine.--> le droit d'un homme devant Élyon,
3:36	lorsqu'on fait tort à un être humain dans son procès, Adonaï ne le voit-il pas ?
3:37	[Mem.] Qui a parlé, et la chose est arrivée, sans qu'Adonaï l'ait ordonnée ?
3:38	Le malheur et le bonheur<!--Es. 45:7 ; Am. 3:6 ; Job 1:21.--> ne sortent-ils pas de la bouche d'Élyon ?
3:39	Pourquoi l'être humain vivant se plaindrait-il, l'homme fort, à cause du châtiment de ses péchés ?

### Le peuple appelé à s'examiner pour revenir à YHWH

3:40	[Noun.] Recherchons nos voies, sondons-les et retournons à YHWH<!--Ps. 119:59 ; 2 Co. 13:5.--> !
3:41	Élevons nos cœurs et nos paumes vers El qui est aux cieux :
3:42	Nous avons péché, nous avons été rebelles ! Tu n'as pas pardonné !
3:43	[Samech.] Tu nous as couverts de colère et tu nous as poursuivis, tu as tué sans épargner.
3:44	Tu t'es couvert d'une nuée pour que les prières ne te parviennent pas.
3:45	Tu as fait de nous la raclure et le rebut au milieu des peuples.
3:46	[Pe.] Tous nos ennemis ouvrent leur bouche contre nous.
3:47	La terreur et la fosse, le dégât et la calamité nous sont arrivés<!--Es. 24:18 ; Jé. 48:44.-->.
3:48	Des torrents d'eau coulent de mon œil à cause de la ruine de la fille de mon peuple.
3:49	[Ayin.] Mon œil verse des larmes sans cesse, sans arrêt,
3:50	jusqu'à ce que YHWH regarde et voie des cieux<!--Ps. 80:15, 102:20.-->.
3:51	Mon œil traite avec sévérité mon âme à cause de toutes les filles de ma ville.

### YHWH, le soutien de Yirmeyah (Jérémie) dans la détresse

3:52	[Tsade.] Ceux qui sont mes ennemis sans cause m'ont chassé, ils m'ont chassé comme un oiseau.
3:53	Ils ont voulu anéantir ma vie dans une fosse<!--Jé. 37:16.-->, et ils ont jeté des pierres sur moi.
3:54	Les eaux ont coulé par-dessus ma tête. Je disais : Je suis retranché !
3:55	[Qof.] J'ai invoqué ton Nom, YHWH, du fond de la fosse<!--Jé. 38:6.-->.
3:56	Tu as entendu ma voix : ne ferme pas tes oreilles à mes soupirs, à mes cris !
3:57	Au jour où je t'ai invoqué, tu t'es approché, et tu as dit : Ne crains rien !
3:58	[Resh.] Adonaï, tu as plaidé la cause de mon âme, tu as racheté ma vie.
3:59	Tu as vu, YHWH, comme on me fait plier : juge à mon jugement !
3:60	Tu as vu toutes leurs vengeances, tous leurs projets contre moi.
3:61	[Shin.] YHWH, tu as entendu leurs insultes, tous leurs projets contre moi,
3:62	les discours de ceux qui se lèvent contre moi, et leur conspiration qu'ils forment contre moi tout au long du jour.
3:63	Qu'ils s'asseyent ou se lèvent, regarde : je suis leur chant de moquerie.
3:64	[Tav.] Rends-leur une récompense, YHWH, selon l'œuvre de leurs mains.
3:65	Livre-les à l'endurcissement de leur cœur, à ta malédiction.
3:66	Poursuis-les dans ta colère et extermine-les de dessous les cieux, YHWH !

## Chapitre 4

### Crimes et apostasie du peuple

4:1	[Aleph.] Comment l'or est-il devenu obscur, et l'or pur s'est-il altéré ? Comment les pierres du sanctuaire sont-elles répandues aux coins de toutes les rues ?
4:2	[Beth.] Comment se fait-il que les précieux fils de Sion estimés comme de l'or pur soient maintenant considérés comme des vases de terre, ouvrage des mains du potier ?
4:3	[Guimel.] Même les monstres marins présentent leurs mamelles et allaitent leurs petits. Mais la fille de mon peuple est devenue cruelle comme les autruches du désert.
4:4	[Daleth.] La langue de celui qui tétait s'est attachée à son palais dans sa soif, les enfants demandent du pain et personne ne leur en donne<!--Jé. 52:6.-->.
4:5	[He.] Ceux qui mangeaient des délices sont ruinés dans les rues. Ceux qui se nourrissaient sur la cochenille<!--Vient de l'hébreu « towla » qui désigne un ver – la femelle « coccus ilicis » qui donne « cochenille ». Ce mot peut être traduit par « étoffe écarlate » ou « cramoisi ». La teinture cramoisie était extraite d'un fluide provenant de ce ver.--> embrassent le fumier.
4:6	[Vav.] L'iniquité de la fille de mon peuple est plus grande que le péché de Sodome, renversée en un instant, sans que personne n'ait tourné la main sur elle.
4:7	[Zayin.] Ses naziréens étaient plus purs que la neige, plus blancs que le lait. Leur corps était plus rouge que les objets précieux, ils étaient polis comme un saphir.
4:8	[Heth.] Leur apparence est devenue plus sombre que la noirceur, on ne les reconnaît plus dans les rues. Ils ont la peau collée sur les os, elle est devenue sèche comme du bois<!--Job 30:30.-->.
4:9	[Teth.] Ceux qui sont tués par l'épée sont plus heureux que ceux qui sont tués par la famine, car ceux-ci dépérissent peu à peu, transpercés par le manque des produits des champs.
4:10	[Yod.] Les mains des femmes compatissantes font cuire leurs enfants. Ils leur servent de nourriture dans la ruine de la fille de mon peuple<!--De. 28:57 ; 2 R. 6:29.-->.
4:11	[Kaf.] YHWH a accompli sa fureur, il a répandu l'ardeur de sa colère. Il a allumé dans Sion un feu qui en dévore les fondements.
4:12	[Lamed.] Les rois de la Terre et tous les habitants du monde n'auraient jamais cru que l'adversaire, l'ennemi, entrerait dans les portes de Yeroushalaim.
4:13	[Mem.] Cela est arrivé à cause des péchés de ses prophètes, et des iniquités de ses prêtres qui répandaient le sang des justes au milieu d'elle<!--Jé. 5:29-31. Le péché des conducteurs donne accès à l'ennemi pour les détruire ainsi que les biens qui leur ont été confiés (Lu. 11:21-22).-->.
4:14	[Noun.] Ils erraient comme des aveugles dans les rues, souillés de sang, au point qu'on ne pouvait pas toucher leurs vêtements.
4:15	[Samech.] On leur criait : Retirez-vous, impurs ! Retirez-vous, retirez-vous, ne nous touchez pas ! Quand ils se sont enfuis, ils ont erré çà et là. On a dit parmi les nations : Ils n'auront plus leur demeure !
4:16	[Pe.] Les faces de YHWH les ont dispersés, il ne veut plus les regarder. Ils n'ont pas eu de respect pour les prêtres et n'ont pas été miséricordieux envers les vieillards.
4:17	[Ayin.] Pour nous, nos yeux se consumaient après un vain secours. Nous regardions du haut de nos lieux élevés vers une nation qui ne pouvait pas délivrer<!--Jé. 18:15.-->.
4:18	[Tsade.] Ils ont épié nos pas afin de nous empêcher d'aller sur nos places. Notre fin s'approchait, nos jours étaient accomplis... Notre fin est arrivée !
4:19	[Qof.] Nos persécuteurs étaient plus légers que les aigles des cieux. Ils nous ont poursuivis sur les montagnes, ils nous ont tendu une embuscade dans le désert.
4:20	[Resh.] Le souffle de nos narines, le mashiah de YHWH<!--Le mashiah en question est le roi Yoshiyah (Josias) (2 R. 21:24 ; 2 R. 22 ; 2 R. 23).-->, a été pris dans leurs fosses, celui de qui nous disions : Nous vivrons sous son ombre parmi les nations.
4:21	[Shin.] Réjouis-toi, sois dans l'allégresse, fille d'Édom, habitante de la terre d'Outs ! La coupe passera aussi vers toi ; tu en seras enivrée et tu seras mise à nu<!--Jé. 25:15-18 ; Ps. 137:7.-->.
4:22	[Tav.] Fille de Sion, la peine de ton iniquité a pris fin : il ne t'enverra plus en exil. Fille d'Édom, il châtiera ton iniquité, il dévoilera tes péchés.

## Chapitre 5

### Supplications de Yirmeyah (Jérémie) à YHWH

5:1	Souviens-toi, YHWH, de ce qui nous est arrivé ! Regarde et vois notre insulte !
5:2	Notre héritage a été renversé par des étrangers, nos maisons par des inconnus.
5:3	Nous sommes devenus des orphelins sans pères, et nos mères sont comme des veuves.
5:4	Nous buvons notre eau à prix d'argent, et nous rentrons notre bois à prix d'argent.
5:5	Ceux qui nous poursuivent sont sur notre cou. Nous sommes épuisés, nous n'avons pas de repos.
5:6	Nous avons tendu la main vers l'Égypte, vers l'Assyrie pour nous rassasier de pain.
5:7	Nos pères ont péché, ils ne sont plus, et c'est nous qui portons leurs iniquités<!--Ex. 34:7 ; Es. 65:7 ; Ps. 79:12.-->.
5:8	Les esclaves dominent sur nous, personne ne nous délivre de leurs mains.
5:9	Nous faisons venir notre pain au péril de notre âme face à l'épée du désert.
5:10	Notre peau est brûlante comme un four face à la chaleur brûlante de la faim.
5:11	Ils ont humilié les femmes dans Sion, les vierges dans les villes de Yéhouda.
5:12	Des chefs ont été pendus par leurs mains, et ils n'ont pas honoré les faces des vieillards.
5:13	Ils ont pris les jeunes hommes pour moudre, et les enfants sont tombés sous le bois.
5:14	Les vieillards ont délaissé la porte, et les jeunes hommes leur musique.
5:15	La joie a disparu de notre cœur, et notre danse est changée en deuil.
5:16	La couronne de notre tête est tombée ! Malheur maintenant à nous, parce que nous avons péché !
5:17	C'est pourquoi notre cœur est devenu souffrant. À cause de ces choses nos yeux sont obscurcis.
5:18	À cause de la montagne de Sion qui est dévastée, les renards s'y promènent.
5:19	Toi, YHWH, tu demeures à jamais, et ton trône subsiste d'âges en âges.
5:20	Pourquoi nous oublierais-tu à jamais ? Pourquoi nous délaisserais-tu si longtemps ?
5:21	Fais-nous revenir à toi, YHWH, et nous reviendrons ! Renouvelle nos jours comme ils étaient autrefois<!--Jé. 30:20, 31:18 ; Ps. 80:3.-->.
5:22	Ou bien, nous aurais-tu entièrement rejetés ? Serais-tu extrêmement fâché contre nous ?
