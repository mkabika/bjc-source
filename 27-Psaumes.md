# Tehilim (Psaumes) (Ps.)

Signification : Louanges

Auteurs : David essentiellement et d'autres écrivains

Thème : La louange et l'adoration

Date de rédaction : À compter du 10ème siècle av. J.-C. et au-delà

Le terme « psaume » désigne un poème chanté avec l'accompagnement d'un instrument. C'est ainsi que furent initialement contés les récits de la création divine, la captivité ou encore la gloire de Yeroushalaim (Jérusalem). Expressions de joie, de reconnaissance, de repentance, d'angoisse ou de vulnérabilité de l'être humain, ces hymnes étaient des prières adressées à Elohîm.

Prophétiques, certains psaumes annoncent les événements de l'achèvement des âges (fin des temps), notamment les souffrances du Mashiah (Christ). Utilisé comme recueil de chants, le livre des Psaumes exalte la grandeur d'Elohîm, sa souveraineté, sa miséricorde et son omniscience. Il est le fruit d'une grande variété d'expériences spirituelles du fait de la diversité de ses auteurs. De plus, il contient une richesse de styles considérable, ce qui en fait le chef-d'œuvre de la poésie hébraïque.

## Chapitre 1

### La voie du juste et celle du pécheur

1:1	Heureux l'homme qui ne marche pas selon le conseil des méchants, qui ne s'arrête pas sur la voie des pécheurs et qui ne s'assied pas dans l'assemblée des moqueurs<!--Jé. 15:17 ; 1 Co. 15:33 ; Ep. 5:11.-->,
1:2	mais qui prend plaisir dans la torah de YHWH, et qui médite sa torah jour et nuit<!--De. 6:6, 17:19 ; Jos. 1:8.--> !
1:3	Il devient un arbre planté près des ruisseaux d'eaux, qui donne son fruit en son temps et dont le feuillage ne se flétrit pas<!--Jé. 17:7-8 ; Ez. 47:12 ; Jn. 15:8 ; Ap. 22:2.-->, et tout ce qu'il fait réussit.
1:4	Il n'en est pas ainsi des méchants : ils sont comme la balle que le vent chasse au loin<!--Job 21:17-18 ; Os. 13:3.-->.
1:5	C'est pourquoi les méchants ne se lèveront pas au jugement, ni les pécheurs dans l'assemblée des justes.
1:6	Car YHWH connaît la voie des justes, mais la voie des méchants périra.

## Chapitre 2

### Complot des nations contre le Mashiah

2:1	Pourquoi cette agitation parmi les nations, ces vains complots parmi les peuples ?
2:2	Les rois de la Terre se présentent et les princes se concertent ensemble contre YHWH et contre son Mashiah<!--Cette prophétie concerne le complot des Juifs, de Pilate et d'Hérode contre Yéhoshoua ha Mashiah (Jésus-Christ). Il est également question du gouvernement mondial dirigé par Satan. Mt. 12:14, 26:3-4, 26:59-66, 27:1-2 ; Mc. 3:6, 11:18 ; Ac. 4:23-29.--> :
2:3	Brisons leurs liens, jetons loin de nous leurs chaînes !
2:4	Celui qui habite dans les cieux se rit d'eux, Adonaï se moque d'eux.
2:5	Alors il leur parle avec colère, et sa fureur les terrifie<!--Pr. 1:26.--> :
2:6	C'est moi qui ai oint mon Roi sur Sion, la montagne de ma sainteté<!--Mi. 4:7.--> !
2:7	Je vous réciterai cette ordonnance. YHWH m'a dit : Tu es mon Fils ! Je t'ai engendré aujourd'hui<!--Ac. 13:33 ; Hé. 1:5, 5:5.-->.
2:8	Demande-moi, et je te donnerai les nations pour héritage, et les extrémités de la Terre pour propriété.
2:9	Tu les briseras avec un sceptre de fer, et tu les mettras en pièces comme un vase de potier<!--Da. 2:44 ; Ap. 2:27.-->.
2:10	Maintenant, rois, ayez de l'intelligence ! Juges de la Terre, recevez instruction !
2:11	Servez YHWH avec crainte, et réjouissez-vous avec tremblement<!--Ps. 19:10.-->.
2:12	Donnez un baiser au Fils, de peur qu'il ne soit en colère et que vous ne périssiez dans cette conduite, quand sa colère s'embrasera promptement. Heureux tous ceux qui se confient en lui !

## Chapitre 3

### YHWH, le véritable secours

3:1	Psaume de David au sujet de sa fuite devant Abshalôm, son fils.
3:2	YHWH, que mes adversaires sont nombreux ! Beaucoup de gens se lèvent contre moi !
3:3	Beaucoup disent à mon âme : Plus de salut pour lui auprès d'Elohîm ! Sélah<!--Le mot hébreu « sélah » signifie « élever, exalter ». Il peut aussi traduire une pause dans le cantique ou le texte. C'est sûrement un terme technique musical montrant probablement une accentuation, une pause, une interruption.-->.
3:4	Mais toi, YHWH, tu es un bouclier autour de moi ! Tu es ma gloire, et tu relèves ma tête.
3:5	De ma voix je crie à YHWH, et il me répond de la montagne de sa sainteté. Sélah.
3:6	Je me couche, je m'endors, je me réveille, car YHWH me soutient<!--Lé. 26:6.-->.
3:7	Je n'ai pas peur des myriades de peuples quand ils se rangent contre moi de toutes parts.
3:8	Lève-toi, YHWH, mon Elohîm ! Délivre-moi ! Car tu frappes à la joue tous mes ennemis, tu brises les dents des méchants.
3:9	Le salut vient de YHWH<!--Es. 43:11 ; Jé. 3:23 ; Pr. 21:31 ; Ap. 7:10.--> ! Que ta bénédiction soit sur ton peuple ! Sélah.

## Chapitre 4

### YHWH, la joie et la paix du juste

4:1	Au chef de musique. Psaume de David.
4:2	Elohîm de ma justice, quand j’appelle, réponds-moi ! Quand j'étais à l'étroit, tu m'as mis au large ! Aie pitié de moi, et écoute ma prière<!--Ps. 28:1-2.--> !
4:3	Fils des hommes, jusqu'à quand ma gloire sera-t-elle diffamée, aimerez-vous la vanité et chercherez-vous le mensonge ? Sélah.
4:4	Sachez que YHWH distingue celui qui lui est fidèle. YHWH m’entend quand je l’appelle<!--1 Jn. 5:14.-->.
4:5	Tremblez et ne péchez pas ! Parlez dans vos cœurs sur votre couche, et taisez-vous. Sélah.
4:6	Sacrifiez des sacrifices de justice<!--Ps. 51:19.-->, et confiez-vous en YHWH !
4:7	Beaucoup disent : Qui nous fera voir le bonheur ? Lève sur nous la lumière de tes faces, YHWH !
4:8	Tu mets plus de joie dans mon cœur qu'ils n'en ont, quand abondent leur blé et leur vin nouveau.
4:9	Je me couche et je m'endors en paix, car toi seul, YHWH, me fais reposer en sécurité<!--Pr. 3:24.-->.

## Chapitre 5

### Recours à la protection de YHWH

5:1	Au chef. Psaume de David. Sur Nechiylah<!--Sens incertain. Peut-être le nom d'une mélodie ou un instrument de musique.-->.
5:2	Prête l'oreille à mes paroles, YHWH ! Écoute ma méditation !
5:3	Mon Roi et mon Elohîm ! Sois attentif à la voix de mon cri, car c'est à toi que j'adresse ma requête.
5:4	YHWH, le matin tu entends ma voix, dès le matin je me tourne vers toi et je veille,
5:5	car tu n'es pas un El qui prend plaisir à la méchanceté. Le mal n'a pas sa demeure auprès de toi,
5:6	les orgueilleux ne subsistent pas devant tes yeux. Tu hais tous ceux qui pratiquent la méchanceté<!--Ha. 1:13 ; Ps. 1:5.-->,
5:7	tu fais périr ceux qui disent le mensonge. YHWH a en abomination l'homme de sangs et de tromperie.
5:8	Mais moi, dans l’abondance de ta miséricorde, j'entrerai dans ta maison, je me prosternerai dans le temple de ta sainteté avec crainte.
5:9	YHWH, conduis-moi dans ta justice, à cause de mes ennemis, aplanis ta voie sous mes pas<!--Ps. 25:4-5, 27:11.-->,
5:10	car il n'y a rien de droit dans leur bouche. Leur cœur est rempli de malice, leur gosier est un sépulcre ouvert, ils flattent de leur langue<!--Ps. 10:7, 12:3 ; Ro. 3:13.-->.
5:11	Elohîm ! Traite-les en coupables, qu'ils échouent dans leurs projets ! Bannis-les, à cause du grand nombre de leurs transgressions ! Car ils se sont rebellés contre toi.
5:12	Tous ceux qui t'ont pour refuge se réjouiront. Ils seront pour toujours dans l'allégresse et tu les protégeras. Ceux qui aiment ton Nom exulteront en toi !
5:13	Car tu bénis le juste, YHWH ! Et tu l'environnes de ta faveur comme d'un bouclier.

## Chapitre 6

### La miséricorde de YHWH

6:1	Au chef de musique. Sur Sheminith<!--« Peut-être un instrument de musique à huit cordes », « peut-être une notation musicale, c'est-à-dire une octave ».-->. Psaume de David.
6:2	YHWH, ne me punis pas dans ta colère, et ne me châtie pas dans ta fureur<!--Jé. 10:24.-->.
6:3	Aie pitié de moi, YHWH ! car je suis sans aucune force. Guéris-moi, YHWH ! car mes os sont terrifiés.
6:4	Mon âme est extrêmement terrifiée, mais toi, YHWH, jusqu'à quand ?
6:5	Reviens, YHWH ! Délivre mon âme. Sauve-moi, à cause de ta miséricorde.
6:6	Car dans la mort on ne se souvient pas de toi : qui te célébrera dans le shéol<!--Es. 38:18 ; Ps. 88:11, 115:17.--> ?
6:7	Je me lasse de mon soupir. Je nage chaque nuit dans mon lit<!--Job 7:3-4.-->, ma couche est liquéfiée par mes larmes.
6:8	Mon œil dépérit de chagrin<!--Ps. 31:10.--> : il a vieilli à cause de tous mes oppresseurs.
6:9	Retirez-vous loin de moi, vous tous qui pratiquez la méchanceté<!--Mt. 7:23, 25:41 ; Lu. 13:27.--> ! Car YHWH a entendu la voix de mes pleurs.
6:10	YHWH a entendu ma supplication, YHWH a reçu ma prière.
6:11	Tous mes ennemis sont honteux, extrêmement terrifiés, ils reculent soudain, honteux.

## Chapitre 7

### La délivrance se trouve auprès de YHWH

7:1	Shiggayown<!--Terme musical, complainte ou hymne lyrique.--> de David, chanté à YHWH, au sujet de Koush, fils de la tribu de Benyamin.
7:2	YHWH, mon Elohîm ! je cherche en toi mon refuge. Sauve-moi de tous mes persécuteurs, et délivre-moi,
7:3	de peur qu'ils ne déchirent mon âme comme un lion qui dévore sans qu'il n'y ait personne qui me secoure.
7:4	YHWH, mon Elohîm ! si j'ai commis une telle action, s'il y a de l'injustice dans mes paumes,
7:5	si j'ai rendu le mal à celui qui était en paix avec moi, si j'ai dépouillé celui qui m'opprimait sans cause,
7:6	qu’un ennemi poursuive mon âme, qu'il atteigne et piétine à terre ma vie, et qu'il couche ma gloire dans la poussière ! Sélah.
7:7	Lève-toi, YHWH ! Dans ta colère, lève-toi contre la fureur de mes adversaires. Réveille-toi pour me secourir, ordonne un jugement !
7:8	Que l'assemblée des peuples t'environne ! Monte au-dessus d'elle vers les lieux élevés !
7:9	YHWH juge les peuples : rends-moi justice, YHWH<!--Ps. 9:5.-->, selon ma droiture et selon mon intégrité !
7:10	S’il te plaît, que la malice des méchants prenne fin, et affermis le juste, toi qui sondes les cœurs et les reins<!--Jé. 11:20, 17:10.-->, Elohîm juste !
7:11	Mon bouclier est en Elohîm, qui délivre ceux qui sont droits de cœur.
7:12	Elohîm est le juste Juge, El s'irrite tous les jours.
7:13	S’il ne retourne pas, il aiguise son épée<!--De. 32:41.-->, bande son arc, le prépare,
7:14	il prépare contre lui les armes de mort, il fabrique des flèches brûlantes.
7:15	Voici qu'il est en travail pour la méchanceté, il conçoit le malheur et enfante le mensonge<!--Ja. 1:15.-->.
7:16	Il fait une fosse, il la creuse, et il tombe dans la fosse qu'il a faite<!--Ps. 9:16.-->.
7:17	Son malheur retourne sur sa tête, et sa violence descend sur son cuir chevelu.
7:18	Je célébrerai YHWH selon sa justice, je chanterai le Nom de YHWH, d'Élyon.

## Chapitre 8

### Magnificence d'Elohîm et vanité de l'homme

8:1	Au chef. Sur Guitthith<!--Un pressoir.-->. Psaume de David.
8:2	YHWH, notre Seigneur, que ton Nom est magnifique sur toute la Terre ! Tu as établi ta majesté au-dessus des cieux<!--Es. 6:3.-->.
8:3	Par la bouche des enfants et de ceux qu’on allaite<!--Mt. 21:16.-->, tu as fondé la puissance contre tes adversaires, afin de faire cesser l'ennemi et le vindicatif.
8:4	Quand je regarde tes cieux, l'ouvrage de tes doigts, la lune et les étoiles que tu as fixées :
8:5	Qu'est-ce que le mortel, pour que tu te souviennes de lui, le fils d'humain, pour que tu le visites<!--Dans ce passage, il est question de l'incarnation de YHWH afin de nous sauver (1 Co. 15:45-49 ; 1 Ti. 3:16 ; Hé. 2:14). Yéhoshoua ha Mashiah (Jésus-Christ) s'est lui-même nommé « fils d'humain » (Lu. 9:22-26), littéralement « fils d'Adam ». D'ailleurs cette expression apparaît dans les évangiles plus de 80 fois.--> ?
8:6	Tu l'as fait de peu inférieur aux anges<!--Le mot hébreu est « elohîm » qui signifie aussi « juges », « divinités ». Voir Hé. 2:7.-->, et tu l'as couronné de gloire et d'honneur.
8:7	Tu l'as fait dominer sur les œuvres de tes mains, tu as tout mis sous ses pieds<!--1 Co. 15:27.-->,
8:8	les brebis comme les bœufs, les animaux des champs,
8:9	les oiseaux des cieux et les poissons de la mer, tout ce qui parcourt les sentiers des mers.
8:10	YHWH, notre Seigneur ! Que ton Nom est magnifique sur toute la Terre !

## Chapitre 9

### Louange à YHWH, l'auteur de nos victoires

9:1	Au chef. Psaume de David. Sur Mouth-Labben<!--Meurs pour le fils.-->.
9:2	[Aleph.] Je célébrerai de tout mon cœur YHWH, je raconterai toutes tes merveilles.
9:3	Je me réjouirai et je m'égaierai en toi, je chanterai ton Nom, Élyon !
9:4	[Beth.] Parce que mes ennemis retournent en arrière, ils trébuchent, ils périssent en face de toi.
9:5	Car tu soutiens mon droit et ma cause, tu sièges sur ton trône en juste Juge.
9:6	[Guimel.] Tu châties les nations, tu détruis le méchant, tu effaces leur nom pour toujours, et à perpétuité.
9:7	Plus d'ennemis ! Les désolations sont arrivées à leur fin pour toujours ! Tu as déraciné leurs villes, et leur mémoire a péri avec elles.
9:8	[He.] Mais YHWH sera assis pour toujours, il a établi son trône pour juger.
9:9	Il juge le monde avec justice, il juge les peuples avec droiture<!--Ps. 96:13, 98:9.-->.
9:10	[Vav.] YHWH est le refuge pour l'opprimé, son refuge au temps où il sera dans la détresse<!--Ps. 37:39, 46:2, 91:2.-->.
9:11	Ceux qui connaissent ton Nom se confient en toi<!--Pr. 3:5.-->, car tu n'abandonnes pas ceux qui te cherchent, YHWH !
9:12	[Zayin.] Chantez à YHWH qui habite en Sion, annoncez ses œuvres parmi les peuples !
9:13	Lorsqu'il recherche le sang versé, il se souvient des pauvres, il n'oublie pas le cri de détresse des affligés.
9:14	[Heth.] Aie pitié de moi, YHWH ! Vois la misère où me réduisent mes ennemis, enlève-moi des portes de la mort,
9:15	afin que je raconte toutes tes louanges, dans les portes de la fille de Sion. Je me réjouirai de la délivrance<!--Voir commentaire en Es. 26:1.--> que tu m'auras donnée.
9:16	[Teth.] Les nations tombent dans la fosse qu'elles ont faite<!--Ps. 10:2, 35:7.-->, leur pied se prend au filet qu'elles ont caché.
9:17	YHWH se fait connaître, il fait justice, le méchant est enlacé dans l'ouvrage de ses paumes. Méditation. Sélah.
9:18	[Yod.] Les méchants retournent dans le shéol, toutes les nations qui oublient Elohîm.
9:19	Car l'indigent n'est pas oublié à jamais, l'espérance des affligés ne périt pas pour toujours.
9:20	[Kaf.] Lève-toi, YHWH ! Que le mortel ne triomphe pas ! Que les nations soient jugées devant tes faces !
9:21	Frappe-les de terreur, YHWH ! Que les peuples sachent qu'ils ne sont que des mortels<!--Es. 51:12.--> ! Sélah.

## Chapitre 10

### Appel au jugement d'Elohîm sur les méchants

10:1	[Lamed.] Pourquoi, YHWH, restes-tu loin, te caches-tu aux temps de la détresse<!--Ps. 13:2, 44:24.--> ?
10:2	Dans son orgueil le méchant poursuit ardemment l’affligé. Ils seront pris par les complots qu'ils ont imaginés<!--Ps. 7:15-16, 9:16, 35:8.-->.
10:3	Car le méchant se glorifie du désir de son âme, il estime heureux l'avare et il méprise YHWH.
10:4	Le méchant, narine haute, ne cherche rien ! Tous ses desseins sont : Il n’y a pas d’Elohîm<!--Ps. 14:1, 53:2.--> !
10:5	Ses voies réussissent en tout temps. Tes jugements sont éloignés de lui, il souffle contre tous ses adversaires.
10:6	Il dit en son cœur : Je ne serai pas ébranlé, car d'âges en âges je n'aurai pas de malheur !
10:7	Sa bouche est pleine de malédictions, de tromperies et de fraudes. Sous sa langue sont le malheur et la méchanceté<!--Ps. 59:7-8, 64:3-4 ; Job 20:12.-->.
10:8	Il est assis en embuscade près des villages, il tue l'innocent dans des lieux cachés, ses yeux épient le malheureux.
10:9	Il se tient aux aguets dans un lieu caché, comme un lion dans sa tanière. Il se tient aux aguets pour attraper l'affligé. Il attrape l'affligé, en le tirant dans son filet.
10:10	Il se courbe, il se baisse, et les malheureux tombent sous sa puissance.
10:11	Il dit en son cœur : El oublie ! Il cache ses faces, il ne le verra jamais<!--Ps. 94:7.--> !
10:12	[Qof.] Lève-toi, YHWH ! El, lève ta main ! N'oublie pas les malheureux !
10:13	Pourquoi le méchant méprise-t-il Elohîm ? Il dit en son cœur que tu ne le rechercheras pas.
10:14	[Resh.] Tu l'as vu, car lorsqu'on afflige ou qu'on maltraite quelqu'un, tu regardes pour le mettre entre tes mains. C'est auprès de toi que se réfugie le malheureux, tu es le secours de l'orphelin.
10:15	[Shin.] Brise le bras du méchant, du mauvais, recherche sa méchanceté jusqu'à ce que tu n'en trouves plus !
10:16	YHWH est Roi pour toujours et à perpétuité<!--Ps. 29:10, 145:13, 146:10 ; La. 5:19.-->, les nations sont exterminées de sa terre.
10:17	[Tav.] Tu entends les vœux de ceux qui souffrent, YHWH ! Tu affermis leur cœur, ton oreille les écoute attentivement
10:18	pour rendre justice à l'orphelin et à l'opprimé, afin que le mortel de la terre ne fasse plus trembler.

## Chapitre 11

### YHWH, le refuge des justes

11:1	Au chef. De David. C'est en YHWH que je cherche un refuge. Comment pouvez-vous dire à mon âme : Oiseau, erre dans les montagnes !
11:2	En effet, les méchants bandent l'arc<!--Ps. 37:14.-->, ils ajustent leur flèche sur la corde, pour tirer dans l'ombre sur ceux dont le cœur est droit.
11:3	Si les fondements sont renversés, le juste, que fera-t-il ?
11:4	YHWH est dans son saint temple, YHWH a son trône dans les cieux. Ses yeux voient, ses paupières examinent les fils des humains.
11:5	YHWH examine le juste et le méchant, son âme hait celui qui aime la violence.
11:6	Il fait pleuvoir sur les méchants des pièges, du feu, du soufre<!--Ez. 38:22.--> et un vent de chaleur brûlante : la portion de leur coupe.
11:7	Car YHWH est juste, il aime la justice. Ceux qui sont droits voient ses faces.

## Chapitre 12

### Le langage des lèvres arrogantes

12:1	Au chef. Psaume de David. Sur Sheminith<!--Peut-être un instrument de musique à huit cordes ; peut-être une notation musicale, c'est-à-dire une octave.-->.
12:2	Sauve, YHWH ! car les hommes pieux n'existent plus, les fidèles disparaissent parmi les fils d'humains.
12:3	L'homme profère la fausseté à l’égard de son compagnon. Ils se parlent avec des lèvres flatteuses et un cœur double.
12:4	Que YHWH retranche toutes les lèvres flatteuses, la langue qui parle fièrement<!--Ps. 17:10.-->,
12:5	parce qu'ils disent : Nous sommes puissants par nos langues, nous avons nos lèvres avec nous. Qui serait notre maître ?
12:6	À cause du mauvais traitement que l'on fait aux malheureux, à cause du gémissement des pauvres, je me lèverai maintenant, dit YHWH, je mettrai en sûreté celui à qui l'on tend des pièges.
12:7	Les paroles de YHWH sont des paroles pures, c'est un argent éprouvé sur terre au creuset<!--Ps. 19:10, 119:140 ; Pr. 30:5.-->, et sept fois épuré.
12:8	Toi, YHWH ! tu les garderas, tu les préserveras de cette génération pour toujours. 
12:9	Les méchants se promènent de toutes parts quand la bassesse s’élève parmi les fils d'humains.

## Chapitre 13

### Savoir attendre le secours d'Elohîm

13:1	Au chef. Psaume de David. 
13:2	YHWH, jusqu'à quand m'oublieras-tu ? Pour toujours ? Jusqu'à quand me cacheras-tu tes faces<!--Ps. 10:1, 27:9.--> ?
13:3	Jusqu'à quand imposerai-je des conseils en mon âme, et affligerai-je mon cœur tous les jours ? Jusqu'à quand mon ennemi s'élèvera-t-il contre moi ?
13:4	YHWH, mon Elohîm ! regarde, réponds-moi, illumine mes yeux, de peur que je ne dorme du sommeil de la mort,
13:5	de peur que mon ennemi ne dise : J'ai eu le dessus ! et que mes adversaires ne se réjouissent, si je venais à tomber<!--Ps. 25:2.-->.
13:6	Mais moi, je me confie en ta bonté, mon cœur se réjouira de la délivrance que tu m'auras donnée. Je chanterai à YHWH, parce qu'il m'a fait du bien.

## Chapitre 14

### L'insensé ne cherche pas Elohîm

14:1	Au chef. De David. L'insensé dit en son cœur : Il n'y a pas d'Elohîm<!--Ceux qui ne croient pas en l'existence d'Elohîm sont appelés insensés. En effet, la création révèle l'existence du Créateur (Ro. 1:19-20).--> ! Ils se sont corrompus, ils se sont rendus abominables dans leurs actions ; il n'y a personne qui fasse le bien.
14:2	YHWH regarde des cieux les fils d'humain, pour voir s'il y a quelqu'un qui soit intelligent, qui cherche Elohîm<!--Ps. 33:13 ; Job 28:24.-->.
14:3	Ils se sont tous égarés, ils se sont tous ensemble rendus odieux, il n'y a personne qui fasse le bien, pas même un seul<!--Tous les humains naissent pécheurs (Ro. 3:10-23).-->.
14:4	Tous ceux qui pratiquent la méchanceté n'ont-ils pas de connaissance ? Ils dévorent mon peuple comme s'ils dévoraient du pain. Ils n'invoquent pas YHWH.
14:5	Là, ils seront saisis d'une grande frayeur, car Elohîm est au milieu de la génération juste.
14:6	Vous jetez l'opprobre sur le conseil du malheureux parce que YHWH est son refuge.
14:7	Qui donne, depuis Sion, la victoire à Israël<!--C'est le Mashiah qui délivrera Israël (Ro. 11:25-27).--> ? Quand YHWH ramènera son peuple captif, Yaacov se réjouira, Israël se réjouira.

## Chapitre 15

### L'homme que YHWH agrée

15:1	Psaume de David. YHWH ! qui séjournera dans ta tente ? Qui habitera sur la montagne de ta sainteté<!--Ps. 24:3-4.--> ?
15:2	C'est celui qui marche dans l'intégrité, qui fait ce qui est juste, et qui profère la vérité telle qu'elle est dans son cœur :
15:3	qui ne calomnie pas avec sa langue, qui ne fait pas de mal à son ami, et qui n'élève pas d'insulte contre son prochain,
15:4	qui regarde avec dédain celui qui est méprisable, mais qui honore ceux qui craignent YHWH, qui ne se rétracte pas s'il fait un serment à son préjudice,
15:5	qui ne prête pas son argent à intérêt et qui n'accepte pas de pot-de-vin contre l'innocent<!--Lé. 25:36 ; De. 16:19, 27:25.-->. Celui qui fait ces choses ne sera jamais ébranlé.

## Chapitre 16

### YHWH, la source de la vie

16:1	Miktam<!--Le mot hébreu « miktam » est généralement traduit par « hymne ».--> de David. Garde-moi, El ! car je cherche en toi mon refuge.
16:2	Je dis à YHWH : Tu es Adonaï, je n’ai pas de bien au-dessus de toi !
16:3	Les saints qui sont sur la terre, les majestueux, tous mes délices étaient en eux.
16:4	Les angoisses de ceux qui courent après un autre elohîm seront multipliées : je ne répands pas leurs libations de sang et je ne mets pas leurs noms sur mes lèvres.
16:5	YHWH est la part de mon héritage et ma coupe, tu maintiens mon lot.
16:6	Un héritage agréable m'est attribué, une belle possession m'est accordée.
16:7	Je bénirai YHWH qui me donne conseil. Même durant les nuits mes reins m’instruisent.
16:8	Je place continuellement YHWH devant moi, parce qu'il est à ma droite, je ne serai pas ébranlé<!--Ps. 109:31, 110:5 ; Ac. 2:25.-->.
16:9	C'est pourquoi mon cœur se réjouit, mon esprit se réjouit et mon corps repose en sécurité.
16:10	Car tu n'abandonneras pas mon âme au shéol, tu ne permettras pas que ton fidèle voie la corruption<!--Le roi David prophétise ici la résurrection du Mashiah.-->.
16:11	Tu me feras connaître le chemin de vies. Il y a un rassasiement de joies devant tes faces, des délices éternelles à ta droite.

## Chapitre 17

### L'assurance en Elohîm

17:1	Prière de David. YHWH, écoute la droiture, sois attentif à mon cri, prête l'oreille à ma prière faite avec des lèvres sans tromperie !
17:2	Que ma justice paraisse devant tes faces, que tes yeux contemplent mon intégrité !
17:3	Tu as sondé mon cœur<!--Ps. 139:1 ; Jé. 12:3.-->, tu l'as visité de nuit, tu m'as examiné, tu n'as rien trouvé : ma pensée ne va pas au-delà de ma parole.
17:4	Quant aux actions des humains, selon la parole de tes lèvres, je me tiens en garde contre la voie du violent.
17:5	Mes pas sont fermes dans tes sentiers, mes pieds ne chancellent pas.
17:6	Je t'invoque, car tu me réponds, El ! Incline ton oreille vers moi, écoute mes paroles !
17:7	Signale ta bonté, toi qui sauves ceux qui cherchent un refuge, et qui par ta droite les délivres de leurs adversaires !
17:8	Garde-moi comme la prunelle, la fille de l'œil, cache-moi à l'ombre de tes ailes<!--Mt. 23:37 ; Lu. 13:34-35.-->
17:9	contre les méchants qui me traitent violemment, contre mes ennemis qui entourent mon âme.
17:10	Ils sont enfermés dans leur propre graisse, leur bouche parle avec orgueil.
17:11	Maintenant, ils nous environnent à chaque pas que nous faisons, ils fixent de leurs yeux pour nous étendre à terre.
17:12	Ils ressemblent au lion qui ne demande qu'à déchirer, et au lionceau qui se tient dans les lieux cachés.
17:13	Lève-toi, YHWH, va au-devant de leurs faces, fais-les plier ! Délivre mon âme du méchant par ton épée,
17:14	des hommes par ta main, YHWH, des hommes de ce monde ! Leur part est dans cette vie et tu remplis leur ventre de tes biens ; leurs enfants sont rassasiés, et ils laissent leurs restes à leurs petits-enfants.
17:15	Moi, je verrai tes faces avec justice<!--Ps. 16:10-11 ; Job 19:26-27.-->. Réveillé, je me rassasierai de ton image.

## Chapitre 18

### Louange à Elohîm, le bouclier des saints

18:1	Au chef. Du serviteur de YHWH, de David, qui déclara à YHWH les paroles de ce cantique, le jour où YHWH l'eut délivré de la paume de tous ses ennemis et de la main de Shaoul.
18:2	Il dit : YHWH qui es ma force, je t'aimerai profondément.
18:3	YHWH est mon Rocher<!--YHWH est le Rocher sur lequel s'appuyait David. Paulos (Paul) enseigne que ce Rocher était Yéhoshoua ha Mashiah (Jésus-Christ) (1 Co. 10:1-4). Voir commentaire en Es. 8:13-17.-->, ma forteresse et mon Sauveur ! Mon El est mon Rocher où je trouve un refuge ! Il est mon bouclier, la corne de mon salut<!--Voir 2 S. 22:3 ; Lu. 1:69.--> et ma haute retraite !
18:4	Je crie : Loué soit YHWH ! Et je suis sauvé de mes ennemis.
18:5	Les cordes de la mort m'avaient entouré et les torrents de Bélial<!--Voir commentaire en De. 13:14.--> m'avaient terrifié.
18:6	Les liens du shéol m'avaient entouré, les filets de la mort étaient devant moi<!--Ps. 116:3.-->.
18:7	Dans ma détresse, j'ai invoqué YHWH, j'ai crié au secours à mon Elohîm. Il a entendu ma voix de son palais, mon cri est parvenu devant lui à ses oreilles.
18:8	La Terre fut ébranlée et trembla, les fondements des montagnes s'agitèrent et s'ébranlèrent<!--Es. 5:25, 64:1-3 ; Jé. 4:24 ; Ps. 104:32.-->, parce qu'il était irrité.
18:9	Une fumée montait de ses narines, un feu dévorant sortait de sa bouche, des charbons ardents qui en jaillissaient, brûlaient.
18:10	Il abaissa les cieux et descendit : il y avait une profonde obscurité sous ses pieds.
18:11	Il était monté sur un chérubin, et il volait, il était porté sur les ailes du vent<!--Ps. 104:3.-->.
18:12	Il faisait des ténèbres sa couverture<!--Voir Ps. 91:1.-->, autour de lui était sa tente, il était enveloppé des eaux obscures et de sombres nuages.
18:13	De la splendeur qui le précédait, s'échappaient les nuées, lançant de la grêle et des charbons de feu.
18:14	YHWH tonna dans les cieux, Élyon fit retentir sa voix avec de la grêle et des charbons de feu.
18:15	Il envoya ses flèches pour les disperser, il lança des éclairs et les mit en déroute<!--Ps. 77:18.-->.
18:16	Le canal des eaux apparut, les fondements du monde furent découverts, par ta menace, YHWH ! par le souffle du vent de tes narines.
18:17	Il étendit la main d'en haut, il m'enleva et me retira des grandes eaux<!--2 S. 22:17.-->.
18:18	Il me délivra de mon puissant ennemi, et de ceux qui me haïssaient, car ils étaient plus forts que moi.
18:19	Ils se tenaient devant moi le jour de ma détresse, mais YHWH fut mon appui.
18:20	Il m'a mis au large, il m'a délivré, parce qu'il m'aime.
18:21	YHWH m'a traité selon ma justice, il m'a récompensé selon la pureté de mes mains<!--Ps. 7:9, 18:25.-->,
18:22	car j'ai observé les voies de YHWH et je ne me suis pas détourné de mon Elohîm.
18:23	Car j'ai eu devant moi toutes ses ordonnances et je ne me suis pas écarté de ses statuts.
18:24	J'ai été intègre envers lui, et je me suis tenu en garde contre mon iniquité.
18:25	YHWH m'a rendu selon ma justice, selon la pureté de mes mains devant ses yeux.
18:26	Avec le fidèle, tu es bon, avec l'homme intègre tu agis avec intégrité.
18:27	Avec celui qui est pur, tu es pur, avec le pervers tu es tordu.
18:28	Oui, tu sauves le peuple affligé, et tu abaisses les yeux hautains<!--Es. 2:11, 5:15.-->.
18:29	Oui, c'est toi qui fais briller ma lampe<!--Jn. 5:35.-->. YHWH, mon Elohîm, éclaire mes ténèbres.
18:30	Oui, avec toi je cours vers une troupe, avec mon Elohîm je saute la muraille.
18:31	Les voies de El sont sans défaut, la parole de YHWH est éprouvée<!--De. 32:4 ; Ps. 19:8-9 ; Da. 4:34.--> ; il est un bouclier pour tous ceux qui se confient en lui.
18:32	Car qui est Éloah, en dehors<!--Voir Es. 43:11.--> de YHWH ? Et qui est un rocher, excepté notre Elohîm<!--1 S. 2:2 ; 2 S. 22:32 ; Es. 44:8 ; Mt. 16:18 ; 1 Co. 10:4.--> ?
18:33	C'est le El qui me ceint de force, et qui me conduit dans la voie droite.
18:34	Il rend mes pieds semblables à ceux des biches<!--2 S. 2:18.-->, et il me place sur mes lieux élevés.
18:35	Il exerce tellement mes mains au combat que mes bras ont plié un arc de cuivre<!--Job 20:24.-->.
18:36	Tu me donnes le bouclier de ton salut, ta droite me soutient, et je deviens puissant par ta bonté.
18:37	Tu élargis le chemin sous mes pas, et mes pieds ne chancellent pas.
18:38	Je poursuis mes ennemis, je les atteins, et je ne reviens pas avant de les avoir anéantis.
18:39	Je les brise, et ils ne se relèvent plus, ils tombent sous mes pieds.
18:40	Car tu m'as ceint de force pour le combat, tu fais plier sous moi ceux qui s'élevaient contre moi.
18:41	Tu fais tourner le dos à mes ennemis devant moi, et j'extermine ceux qui me haïssaient.
18:42	Ils crient au secours... pas de sauveur ! Vers YHWH... il ne leur répond pas !
18:43	Je les brise comme la poussière qui est dispersée par le vent et je les foule comme la boue des rues.
18:44	Tu me délivres des querelles du peuple, tu m'établis chef des nations. Un peuple que je ne connais pas m'est asservi.
18:45	Ils m'obéissent au premier ordre, les fils de l'étranger me flattent.
18:46	Les fils de l’étranger se fanent, ils tremblent de peur dans leurs forteresses.
18:47	YHWH est vivant, et béni soit mon Rocher ! Que l'Éloah de mon salut soit exalté !
18:48	C'est le El qui me donne la vengeance et qui m'assujettit les peuples,
18:49	c'est lui qui me délivre de mes ennemis ! Tu m'élèves au-dessus de mes adversaires, tu me sauves de l'homme de violence.
18:50	C'est pourquoi, YHWH, je te confesserai parmi les nations ! Et je chanterai des louanges à ton Nom.
18:51	Qui accorde de grandes délivrances à son roi, qui fait miséricorde à son mashiah, à David, et à sa postérité, pour toujours.

## Chapitre 19

### La création exalte la grandeur d'Elohîm

19:1	Au chef. Psaume de David.
19:2	Les cieux relatent la gloire de El, et le firmament met en évidence l'œuvre de ses mains.
19:3	Le jour fait jaillir la parole au jour, et la nuit montre sa connaissance la nuit.
19:4	Ce n'est pas un langage, ce ne sont pas des paroles dont le cri ne soit pas entendu :
19:5	leur retentissement couvre toute la Terre, et leur voix est allée jusqu'aux extrémités du monde<!--Ro. 10:18.-->. Il a dressé une tente pour le soleil.
19:6	Et lui, comme un époux qui sort de sa chambre nuptiale, se réjouit, homme vaillant, de courir sur la voie.
19:7	Il se lève à l'extrémité des cieux et achève sa course à l'autre extrémité<!--Ec. 1:5.--> : rien ne se dérobe à sa chaleur.
19:8	La torah de YHWH est parfaite, elle fait revenir l'âme<!--Ps. 23:3.-->. Le témoignage de YHWH est fidèle, il donne la sagesse au stupide<!--2 S. 22:31 ; Ps. 18:31, 119:130.-->.
19:9	Les préceptes de YHWH sont droits, ils réjouissent le cœur. Les commandements de YHWH sont purs, ils éclairent les yeux.
19:10	La crainte de YHWH est pure, elle subsiste pour toujours. Les jugements de YHWH sont vrais, et ils sont tous justes.
19:11	Ils sont plus précieux que l'or, que beaucoup d'or fin, et plus doux que le miel, que celui qui coule des rayons de miel<!--Ps. 119:103.-->.
19:12	Aussi ton serviteur est averti par eux, en les gardant, la récompense est grande.
19:13	Qui discerne ses erreurs ? Purifie-moi de celles qui me sont cachées.
19:14	Garde aussi ton serviteur des péchés d'orgueil : qu'ils ne dominent pas sur moi ! Alors je serai parfait, je serai pur de grandes transgressions !
19:15	Que les paroles de ma bouche et la méditation de mon cœur deviennent des délices devant toi, YHWH ! mon Rocher et mon Racheteur<!--Voir commentaire en Es. 60:16.--> !

## Chapitre 20

### Recours à l'intervention d'Elohîm

20:1	Au chef. Psaume de David. 
20:2	Que YHWH te réponde au jour de la détresse, que le Nom de l'Elohîm de Yaacov te protège !
20:3	Qu'il envoie ton secours du saint lieu, et qu'il te soutienne de Sion !
20:4	Qu'il se souvienne de toutes tes offrandes, qu'il réduise en cendres ton holocauste ! Sélah.
20:5	Qu'il te donne ce que ton cœur désire, et qu'il fasse réussir tes desseins !
20:6	Nous vaincrons en ton salut, nous lèverons la bannière au Nom de notre Elohîm. YHWH accomplira tous tes désirs.
20:7	Maintenant je sais que YHWH sauve son mashiah, il lui répondra des cieux, de sa sainte demeure, par le salut puissant de sa droite.
20:8	Ceux-ci dans leurs chars, ceux-là dans leurs chevaux. Mais nous nous souviendrons du Nom de YHWH, notre Elohîm.
20:9	Eux, ils plient, et ils tombent, nous, nous tenons ferme, et restons debout.
20:10	YHWH sauve-nous ! Que le roi nous réponde quand nous crions à lui !

## Chapitre 21

### La protection d'Elohîm sur le roi

21:1	Au chef. Psaume de David.
21:2	YHWH, le roi se réjouit de ta puissance, ton salut le remplit d'allégresse !
21:3	Tu lui as donné ce que désirait son cœur et tu n'as pas refusé ce que demandaient ses lèvres. Sélah.
21:4	Car tu es venu au-devant de lui avec des bénédictions de ta bonté, et tu as mis sur sa tête une couronne d'or pur.
21:5	Il te demandait la vie, et tu la lui as donnée, la longueur des jours, pour toujours, à perpétuité.
21:6	Sa gloire est grande à cause de ton salut, tu l'as couvert de majesté et d'honneur.
21:7	Car tu l’as établi en bénédiction perpétuelle, tu le combles de joie et de gaieté par tes faces<!--Ps. 16:11.-->.
21:8	Parce que le roi se confie en YHWH, et par la bonté d'Élyon, il ne sera pas ébranlé<!--Ps. 16:8.-->.
21:9	Ta main trouvera tous tes ennemis, ta droite trouvera tous ceux qui te haïssent.
21:10	Tu les rendras tels qu’un four en feu, au temps où se montreront tes faces. YHWH les engloutira dans sa colère, et le feu les consumera.
21:11	Tu feras périr leur fruit de la terre et leur postérité du milieu des fils d'humains.
21:12	Car ils ont projeté du mal contre toi, ils ont conçu de mauvais desseins, mais ils ne prévaudront pas.
21:13	Parce que tu les mettras sur le dos, tu dirigeras les cordes de ton arc contre leurs faces.
21:14	Lève-toi, YHWH, par ta force ! Nous chanterons et célébrerons ta puissance.

## Chapitre 22

### Les souffrances du Mashiah

22:1	Au chef. Psaume de David. Sur Ayeleth-Hashachar<!--Biche de l'Aurore.-->.
22:2	Mon El ! Mon El ! Pourquoi m'as-tu abandonné<!--Le Psaume 22 est une description détaillée de la mort par crucifixion du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) (Mt. 27:45-46).-->, te tenant loin de mon salut et des paroles de mon rugissement ?
22:3	Mon Elohîm ! Je crie le jour, mais tu ne réponds pas, la nuit, et je n'ai pas de repos.
22:4	Pourtant tu es le Saint, tu habites les louanges d’Israël.
22:5	Nos pères se sont confiés en toi ; ils se sont confiés, et tu les as délivrés.
22:6	Ils ont crié vers toi, et ils ont été délivrés ; ils se sont appuyés sur toi, et ils n'ont pas été confus<!--Es. 49:23 ; Ps. 25:3, 31:2.-->.
22:7	Et moi, je suis un ver<!--La couleur écarlate de cochenille s'obtient grâce à la femelle cochenille aptère qui contient dans son corps et dans ses œufs un pigment rouge à base d'acide carminique qui permet à l'insecte et à ses larves de se protéger des prédateurs. Au moment de la ponte, cette dernière fixe fermement son corps au tronc d'un arbre puis libère ses œufs qui demeurent ainsi protégés en dessous d'elle jusqu'à leur éclosion. Ensuite, l'insecte meurt en libérant cette substance rouge qui se propage sur tout son corps et sur le bois hôte. C'est ce fluide que l'homme récupère pour en faire un colorant à la couleur caractéristique. Une subtile analogie peut être faite entre la cochenille et le Seigneur qui a versé son sang à la croix pour nous donner la vie. Voir Ex. 25:4.--> et non un homme, l'insulte des humains et le méprisé du peuple<!--Es. 53:2-3.-->.
22:8	Tous ceux qui me voient se moquent de moi, ils ouvrent les lèvres, secouent la tête<!--Ps. 109:25 ; Mt. 27:39.--> :
22:9	Il roule vers YHWH ! Qu'il le délivre, qu'il le sauve, puisqu’il a pris plaisir en lui<!--Mt. 27:43.--> !
22:10	Oui, c'est toi qui m'as tiré hors du ventre, qui m'as mis en sûreté lorsque j'étais sur les mamelles de ma mère.
22:11	J'ai été sous ta garde, dès le sein maternel, tu as été mon El dès le ventre de ma mère<!--Es. 49:1.-->.
22:12	Ne t'éloigne pas de moi, car la détresse m’approche, car il n'y a personne pour me secourir<!--Ps. 69:21.--> !
22:13	Beaucoup de taureaux m’entourent, de puissants de Bashân m'environnent.
22:14	Ils ouvrent leur bouche contre moi, pareils au lion qui déchire et rugit.
22:15	Je me répands comme de l’eau, et tous mes os se séparent. Mon cœur est comme de la cire, il se fond dans mes entrailles.
22:16	Ma force se dessèche comme l'argile, et ma langue s'attache à mon palais. Tu me réduis à la poussière de la mort.
22:17	Car des chiens m’entourent, une assemblée de méchants tournent autour de moi, ils ont percé<!--Jn. 20:25.--> mes mains et mes pieds.
22:18	Je compte tous mes os. Eux, ils m'examinent, ils me regardent.
22:19	Ils se partagent mes vêtements et tirent au sort mon habit<!--Mt. 27:35 ; Mc. 15:24 ; Lu. 23:34 ; Jn. 19:24.-->.
22:20	Et toi, YHWH, ne t'éloigne pas ! Ma force, hâte-toi de me secourir !
22:21	Délivre mon âme de l'épée, mon unique de la main des chiens !
22:22	Sauve-moi de la bouche du lion, délivre-moi des cornes du taureau sauvage !
22:23	Je déclarerai ton Nom à mes frères, je te louerai au milieu de l'assemblée<!--Hé. 2:12.-->.
22:24	Vous qui craignez YHWH, louez-le ! Toute la postérité de Yaacov, glorifiez-le ! Toute la postérité d'Israël, redoutez-le !
22:25	Car il n'a ni mépris ni dédain pour les peines du misérable, et il ne lui cache pas ses faces, mais il l'écoute quand il crie au secours à lui.
22:26	Ta louange commencera par moi dans la grande assemblée ; j'accomplirai mes vœux en présence de ceux qui te craignent<!--Ps. 56:13.-->.
22:27	Les malheureux mangeront et seront rassasiés, ceux qui cherchent YHWH le loueront. Votre cœur vivra à perpétuité !
22:28	Toutes les extrémités de la Terre se souviendront et se tourneront vers YHWH, et toutes les familles des nations se prosterneront en face de toi<!--Ps. 72:8-11, 86:9.-->.
22:29	Car le règne appartient à YHWH : il domine sur les nations.
22:30	Ils mangeront et se prosterneront, tous les gras de la Terre ; devant lui s'inclineront tous ceux qui descendent dans la poussière, ceux qui ne peuvent faire vivre leur âme.
22:31	La postérité le servira, on parlera d'Adonaï à cette génération<!--Es. 59:21, 65:23 ; Ps. 110:3.-->.
22:32	Ils viendront et ils publieront sa justice au peuple qui naîtra, parce qu'il aura fait ces choses.

## Chapitre 23

### Le Bon Berger

23:1	Psaume de David. YHWH est mon Berger<!--YHWH, le Bon Berger, est notre Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) (Es. 40:11 ; Jé. 23:4 ; Jn. 10:11). Voir aussi Ps. 28:9.--> : je ne manquerai de rien.
23:2	Il me fait reposer dans des pâturages d'herbes vertes, il me dirige près des eaux de repos.
23:3	Il fait revenir mon âme<!--Ps. 19:8.-->, il me conduit dans les sentiers de la justice pour l'amour de son Nom.
23:4	Même quand je marcherais dans la vallée de l'ombre de la mort, je ne craindrais aucun mal<!--Ps. 118:6.-->, car tu es avec moi : ton bâton et ta houlette me consolent.
23:5	Tu dresses devant moi une table, en face de mes adversaires ; tu oins d'huile ma tête et ma coupe déborde.
23:6	En effet, la bonté et la miséricorde m'accompagneront tous les jours de ma vie, et j'habiterai dans la maison de YHWH pour toujours.

## Chapitre 24

### Accueil de YHWH, le Roi de gloire

24:1	Psaume de David. La Terre appartient à YHWH, avec tout ce qui est en elle<!--Ex. 19:5 ; De. 10:14 ; Ps. 50:12 ; Job 41:3 ; 1 Co. 10:26.-->, le monde et ceux qui l’habitent !
24:2	Car il l'a fondée sur les mers, et l'a établie sur les fleuves.
24:3	Qui montera sur la montagne de YHWH ? Qui s’élèvera dans son lieu saint<!--Ps. 15:1-2, 118:19.--> ?
24:4	Celui qui a les paumes innocentes et le cœur pur, qui ne livre pas son âme à la fausseté, et qui ne jure pas pour tromper.
24:5	Il portera la bénédiction de YHWH et la justice de l'Elohîm de son salut.
24:6	Telle est la génération de ceux qui le consultent, de ceux qui cherchent tes faces, Yaacov. Sélah.
24:7	Portes, levez vos têtes ! Levez-vous ouvertures éternelles ! Et le Roi de gloire entrera !
24:8	Qui est ce Roi de gloire ? C'est YHWH, le Fort et le Puissant, YHWH, le Puissant à la guerre.
24:9	Portes, levez vos têtes ! Levez-les, ouvertures éternelles ! Et le Roi de gloire entrera !
24:10	Qui est ce Roi de gloire ? YHWH Tsevaot : c'est lui qui est le Roi de gloire ! Sélah.

## Chapitre 25

### La crainte d'Elohîm mène à la voie de YHWH

25:1	De David. [Aleph.] YHWH, j'élève mon âme vers toi.
25:2	[Beth.] Mon Elohîm ! je me confie en toi : fais que je ne sois pas honteux<!--Ps. 22:5, 31:2.--> ! Et que mes ennemis ne triomphent pas de moi !
25:3	[Guimel.] En effet, tous ceux qui espèrent en toi ne seront pas confus<!--Ro. 10:11.-->. Ceux-là seront confus, qui agissent avec tromperie sans cause.
25:4	[Daleth.] YHWH ! Fais-moi connaître tes voies, enseigne-moi tes sentiers<!--Ps. 27:11, 86:11, 143:10.-->.
25:5	[He. Vav.] Fais-moi marcher selon la vérité, et instruis-moi, car tu es l'Elohîm de ma délivrance, je m'attends à toi tous les jours.
25:6	[Zayin.] YHWH ! Souviens-toi de tes compassions et de ta bonté, car elles sont éternelles<!--Jé. 33:11 ; Ps. 103:17, 106:1, 107:1, 117:2, 136:1-2.-->.
25:7	[Heth.] Ne te souviens pas des péchés de ma jeunesse ni de mes transgressions ! Souviens-toi de moi selon ta miséricorde, à cause de ta bonté, YHWH !
25:8	[Teth.] YHWH est bon et droit : C'est pourquoi il montre aux pécheurs la voie.
25:9	[Yod.] Il conduit les humbles dans la justice, et il leur montre sa voie.
25:10	[Kaf.] Tous les sentiers de YHWH sont miséricorde et vérité, pour ceux qui gardent son alliance et ses témoignages.
25:11	[Lamed.] Pour l'amour de ton Nom, YHWH, tu pardonneras mon iniquité, car elle est grande<!--2 S. 24:10.-->.
25:12	[Mem.] Quel est cet homme qui craint YHWH ? Il lui montrera quelle voie choisir.
25:13	[Noun.] Son âme demeurera dans le bonheur, et sa postérité prendra possession de la Terre.
25:14	[Samech.] Les secrets de YHWH sont pour ceux qui le craignent, et son alliance leur donne le savoir.
25:15	[Ayin.] Mes yeux sont continuellement sur YHWH, car c'est lui qui sortira mes pieds du filet.
25:16	[Pe.] Tourne tes faces vers moi, et aie pitié de moi, car je suis seul et affligé.
25:17	[Tsade.] Les détresses de mon cœur se sont agrandies : fais-moi sortir de mes angoisses !
25:18	[Resh.] Vois ma misère et ma peine, et pardonne tous mes péchés.
25:19	[Resh.] Vois combien mes ennemis sont nombreux, ils me haïssent d’une haine violente<!--Jn. 15:25.-->.
25:20	[Shin.] Garde mon âme et délivre-moi ! Que je ne sois pas confus, car je me suis réfugié en toi !
25:21	[Tav.] Que l'innocence et la droiture me protègent, car je m'attends à toi !
25:22	[Pe.] Elohîm ! rachète Israël de toutes ses détresses !

## Chapitre 26

### Demeurer dans l'intégrité

26:1	De David. YHWH, rends-moi justice<!--Ps. 43:1, 54:3.--> ! Car je marche dans mon intégrité, je me confie en YHWH, je ne chancelle pas.
26:2	Sonde-moi et éprouve-moi<!--Ps. 11:4-5, 17:3, 139:23.-->, YHWH ! Fais passer au creuset mes reins et mon cœur,
26:3	car ta grâce est devant mes yeux, et je marche dans ta vérité.
26:4	Je ne m'assieds pas avec les hommes faux<!--Ps. 1:1 ; 1 Co. 5:9-11, 15:33.-->, et je ne vais pas avec les gens dissimulés.
26:5	Je hais la compagnie de ceux qui font le mal<!--Ps. 101:2-5, 119:113.-->, et je ne m'assieds pas avec les méchants.
26:6	Je lave mes paumes dans l'innocence et je fais le tour de ton autel<!--Ps. 73:13.-->, YHWH !
26:7	pour faire entendre une voix de louange et raconter toutes tes merveilles.
26:8	YHWH, j'aime la demeure de ta maison, le lieu dans lequel est le tabernacle de ta gloire.
26:9	N'enlève pas mon âme avec les pécheurs, ma vie avec les hommes de sang,
26:10	dans les mains desquels il y a de la méchanceté préméditée, et dont la main est pleine de pot-de-vin.
26:11	Mais moi, je marche dans mon intégrité. Délivre-moi et aie pitié de moi !
26:12	Mon pied se tient dans la droiture. Je bénirai YHWH dans les assemblées.

## Chapitre 27

### La foi qui triomphe des épreuves

27:1	De David. YHWH est ma lumière<!--Es. 60:19-20 ; Mi. 7:8 ; Ps. 118:6 ; Jn. 8:12 ; Ap. 21:23.--> et mon salut : de qui aurai-je peur ? YHWH est le soutien de ma vie : de qui aurai-je peur ?
27:2	Lorsque les méchants s'avancent contre moi pour dévorer ma chair, ce sont mes adversaires, mes ennemis qui trébuchent et tombent.
27:3	Si toute une armée campait contre moi, mon cœur n'aurait pas peur. Si une guerre s'élevait contre moi, je serais plein de confiance.
27:4	Je demande une chose à YHWH, je la désire : c'est d'habiter dans la maison de YHWH tous les jours de ma vie, pour contempler la beauté de YHWH et pour admirer son temple.
27:5	Car il me cachera dans sa tanière au jour du malheur, il me cachera sous la couverture<!--Voir Ps. 91:1.--> de sa tente, il m'élèvera sur un rocher.
27:6	Maintenant même, ma tête s'élève par-dessus mes ennemis qui m'entourent. Je sacrifierai dans sa tente des sacrifices au son de la trompette, je chanterai, je célèbrerai YHWH.
27:7	Écoute, YHWH, ma voix qui t’appelle, aie pitié de moi et réponds-moi !
27:8	Mon cœur dit de ta part : Cherche mes faces ! Je chercherai tes faces, YHWH !
27:9	Ne me cache pas tes faces, ne rejette pas avec colère ton serviteur ! Tu es mon secours, ne me laisse pas, ne m'abandonne pas, Elohîm de mon salut !
27:10	Car mon père et ma mère m'abandonnent, mais YHWH me recueillera<!--Es. 49:15.-->.
27:11	YHWH, enseigne-moi ta voie, et conduis-moi dans le sentier de la droiture, à cause de mes ennemis<!--Ps. 5:9, 25:4-5.-->.
27:12	Ne livre pas mon âme au désir de mes adversaires, car contre moi se sont levés de faux témoins et ceux qui ne respirent que la violence.
27:13	Oh ! Si je n'étais pas sûr de voir la bonté de YHWH sur la Terre des vivants...
27:14	Attends-toi à YHWH ! Demeure ferme et que ton cœur<!--Es. 33:2 ; Ps. 31:25.--> se fortifie ! Attends-toi à YHWH.

## Chapitre 28

### Louange à YHWH, le Rocher de son peuple

28:1	De David. Je crie à toi, YHWH ! Mon Rocher ! Ne reste pas sourd envers moi, de peur que si tu ne me réponds pas, je ne sois semblable à ceux qui descendent dans la fosse<!--Ps. 4:2, 143:7. Voir commentaire en Es. 8:13-17.-->.
28:2	Écoute la voix de mes supplications quand je crie au secours vers toi, quand j'élève mes mains vers ta sainteté, ton lieu très-saint.
28:3	Ne m'emporte pas avec les méchants ni avec ceux qui pratiquent la méchanceté, qui parlent de paix avec leur prochain pendant que la malice est dans leur cœur<!--Jé. 9:7 ; Ps. 26:9.-->.
28:4	Donne-leur selon leurs œuvres et selon la méchanceté de leurs actions, donne-leur selon l'ouvrage de leurs mains, rends-leur ce qu'ils ont mérité<!--2 Ti. 4:14.-->.
28:5	Car ils ne discernent pas les œuvres de YHWH, ni l’ouvrage de ses mains : qu'il les renverse et ne les édifie pas !
28:6	Béni soit YHWH ! Car il entend la voix de mes supplications.
28:7	YHWH est ma force et mon bouclier. Mon cœur se confie en lui, et je suis secouru. Mon cœur se réjouit, c'est pourquoi je le loue par mes chants.
28:8	YHWH est la force de son peuple, il est le refuge des délivrances de son mashiah.
28:9	Sauve ton peuple et bénis ton héritage ! Sois leur berger<!--Ps. 23:1.--> et porte-les pour toujours !

## Chapitre 29

### La suprématie d'Elohîm

29:1	Psaume de David. Fils de El, donnez à YHWH, donnez à YHWH la gloire et la force<!--Ps. 96:7-8.--> !
29:2	Donnez à YHWH la gloire due à son Nom ! Prosternez-vous devant YHWH avec des ornements sacrés !
29:3	La voix de YHWH est sur les eaux, El Kabowd<!--El de gloire.--> fait tonner, YHWH est sur les grandes eaux.
29:4	La voix de YHWH est forte, la voix de YHWH est majestueuse.
29:5	La voix de YHWH brise les cèdres, YHWH brise les cèdres du Liban,
29:6	il les fait sauter comme un veau, le Liban et le Shiryown<!--Un des noms du Mont Hermon, utilisé en particulier par les Sidoniens.--> comme des fils de taureaux sauvages.
29:7	La voix de YHWH fait jaillir des flammes de feu.
29:8	La voix de YHWH fait trembler le désert, YHWH fait trembler le désert de Qadesh.
29:9	La voix de YHWH fait naître les biches, et dépouille les forêts. Dans son palais tout s'écrie : Gloire<!--Kabowd.--> !
29:10	YHWH était assis lors du déluge, YHWH est assis en Roi pour toujours<!--Ps. 146:10.-->.
29:11	YHWH donne de la force à son peuple, YHWH bénit son peuple en paix.

## Chapitre 30

### De la délivrance découle la louange

30:1	Psaume. Cantique pour la dédicace de la maison de David.
30:2	YHWH, je t'exalte parce que tu m’as relevé, et que tu n’as pas réjoui mes ennemis à mon sujet.
30:3	YHWH, mon Elohîm ! j'ai crié au secours à toi, et tu m'as guéri.
30:4	YHWH ! tu as fait monter mon âme du shéol, tu m'as fait vivre, afin que je ne descende pas dans la fosse.
30:5	Chantez à YHWH, vous ses fidèles, et célébrez la mémoire de sa sainteté<!--Ps. 97:12.--> !
30:6	Car sa colère dure un instant, mais sa grâce toute la vie. Au soir les pleurs se logent, mais au matin c’est un cri de joie.
30:7	Dans ma sécurité, je disais : Je ne serai jamais ébranlé<!--Ps. 10:6.--> !
30:8	YHWH ! Ta faveur m'a établi sur de fortes montagnes... Tu caches tes faces, et je suis terrifié<!--Ps. 13:2, 88:15, 102:3, 143:7.-->.
30:9	YHWH ! J'ai crié à toi, j'ai présenté ma supplication à Adonaï :
30:10	Que gagnes-tu à verser mon sang si je descends dans la fosse ? La poussière te célébrera-t-elle ? Racontera-t-elle ta fidélité<!--Es. 38:18.--> ?
30:11	YHWH, écoute, et aie pitié de moi ! YHWH, secours-moi !
30:12	Tu as changé mon gémissement en allégresse, tu as détaché mon sac, et tu m'as ceint de joie,
30:13	afin que la gloire chante ta louange<!--Ps. 57:10.--> et ne se taise pas. YHWH, mon Elohîm, je te célébrerai à jamais.

## Chapitre 31

### Recherche de la protection divine

31:1	Au chef. Psaume de David. 
31:2	YHWH ! Tu es mon refuge : que jamais je ne sois honteux ! Délivre-moi par ta justice<!--Ps. 25:2-20, 71:1-2.--> !
31:3	Incline ton oreille vers moi, hâte-toi de me délivrer ! Sois pour moi un rocher de protection, une forteresse, afin que je puisse m'y sauver !
31:4	Car tu es mon Rocher et ma forteresse. Tu me dirigeras et tu me donneras du repos, pour l'amour de ton Nom.
31:5	Fais-moi sortir du filet qu'ils m'ont tendu en secret, car tu es ma protection.
31:6	Je dépose mon esprit entre tes mains<!--Lu. 23:46.-->. Tu me rachèteras, YHWH, le El de vérité !
31:7	Je hais ceux qui s'adonnent aux vanités trompeuses, et je me confie en YHWH.
31:8	Je suis heureux et je me réjouis en ta bonté ! Tu vois mon affliction et tu sais les angoisses de mon âme.
31:9	Tu ne m'as pas livré aux mains de l'ennemi, tu as fait tenir mes pieds au large.
31:10	YHWH, aie pitié de moi ! Car je suis dans la détresse. Mes yeux, mon âme et mon corps dépérissent de chagrin<!--Ps. 6:8, 88:10.-->.
31:11	Car ma vie se consume dans la douleur, et mes années dans les soupirs ; ma force chancelle à cause de mon iniquité, et mes os sont consumés.
31:12	Je suis devenu une insulte pour tous mes adversaires, et plus pour mes voisins, une terreur pour ceux qui me connaissent. Ceux qui me voient dehors s'enfuient loin de moi<!--Ps. 38:12 ; Job 19:13-14.-->.
31:13	Je suis oublié de leurs cœurs comme un mort, je suis comme un vase brisé.
31:14	Car j'entends les diffamations de plusieurs, la crainte m'environne, quand ils se concertent unis contre moi : c’est pour enlever mon âme qu’ils complotent<!--Jé. 20:10.-->.
31:15	Toutefois, je me confie en toi, YHWH ! Je dis : Tu es mon Elohîm !
31:16	Mes temps<!--Destinées.--> sont dans tes mains : délivre-moi de la main de mes ennemis et de ceux qui me poursuivent !
31:17	Fais briller tes faces sur ton serviteur<!--Ps. 4:7, 67:2.-->, délivre-moi par ta bonté !
31:18	YHWH, que je ne sois pas confus puisque je t'ai invoqué. Que les méchants soient confus, qu'ils soient couchés dans le shéol !
31:19	Que les lèvres menteuses soient muettes, elles profèrent des paroles dures contre le juste, avec orgueil et avec mépris !
31:20	Que ta bonté est grande<!--Ps. 36:6.--> ! Toi qui la réserves pour ceux qui te craignent, tu leur fais un refuge à la vue des fils d'humains !
31:21	Tu les caches sous la couverture<!--Ps. 91:1.--> de tes faces, loin du complot des hommes, tu les caches sous ton abri contre les langues querelleuses.
31:22	Béni soit YHWH ! Car il a rendu merveilleuse sa bonté envers moi, comme si j'avais été dans une ville retranchée.
31:23	Et moi, je disais, dans ma précipitation : Je suis chassé loin de tes yeux ! Mais tu as entendu la voix de mes supplications quand j'ai crié au secours vers toi.
31:24	Aimez YHWH, vous tous, ses bien-aimés ! YHWH garde les fidèles, et il rétribue abondamment celui qui agit avec orgueil.
31:25	Vous tous qui espérez en YHWH<!--Ps. 27:14.-->, demeurez fermes et il fortifiera votre esprit !

## Chapitre 32

### La puissance du pardon

32:1	De David. Poésie. Heureux celui à qui la transgression est pardonnée, et dont le péché est couvert !
32:2	Heureux l'homme à qui YHWH ne tient pas compte de son iniquité<!--Ro. 4:6-8.-->, et dans l'esprit duquel il n'y a pas de tromperie !
32:3	Quand je me suis tu, mes os se sont consumés, je n'ai fait que gémir tout le jour,
32:4	parce que jour et nuit ta main s'appesantissait sur moi<!--Ps. 38:3.-->, ma vigueur s'est changée en une sécheresse d'été. Sélah.
32:5	Je t'ai fait connaître mon péché et je n'ai pas caché mon iniquité. J'ai dit : J'avouerai mes transgressions à YHWH<!--Pr. 28:13 ; 1 Jn. 1:9.--> ! Et tu as ôté la peine de mon péché. Sélah.
32:6	C'est pourquoi tout fidèle te prie au temps qu'il trouve<!--Es. 55:6 ; So. 2:3 ; Ps. 69:14.-->. Même si les grandes eaux débordent, elles ne l'atteindront pas.
32:7	Tu es ma couverture<!--Ps. 91:1.-->, tu me gardes de la détresse, tu m'environnes de cris harmonieux de délivrance. Sélah.
32:8	Je te rendrai intelligent, je t'enseignerai la voie dans laquelle tu dois marcher, je te guiderai de mon œil.
32:9	Ne devenez pas comme le cheval ni comme le mulet qui sont sans intelligence. Sans le mors et le frein dont on l’orne pour le maîtriser, il n’approche pas de toi<!--Ja. 3:3.-->.
32:10	Beaucoup de douleurs atteindront le méchant<!--Pr. 19:29.-->, mais la bonté environne l'homme qui se confie en YHWH.
32:11	Vous justes, réjouissez-vous en YHWH, soyez dans l'allégresse ! Criez de joie, vous tous qui êtes droits de cœur<!--Ps. 33:1, 64:11.--> !

## Chapitre 33

### Louanges à YHWH, l'Elohîm fidèle

33:1	Vous justes, poussez un cri de joie à cause de YHWH<!--Ps. 32:11, 97:12, 147:1.--> ! La louange convient aux gens droits.
33:2	Célébrez YHWH avec la harpe, chantez-lui des psaumes sur le luth à dix cordes !
33:3	Chantez-lui un cantique nouveau<!--Ps. 40:4, 96:1, 98:1, 144:9 ; Ap. 5:9, 14:3.--> ! Jouez bien de vos instruments avec un cri de joie !
33:4	Car la parole de YHWH est droite et toutes ses œuvres s'accomplissent avec fidélité !
33:5	Il aime la justice et la droiture<!--Ps. 45:8 ; Hé. 1:9.-->. La Terre est remplie de la bonté de YHWH.
33:6	Par la parole de YHWH les cieux ont été faits, et par l'esprit de sa bouche toute leur armée<!--Ge. 2:1-2.-->.
33:7	Il amoncelle en un tas les eaux de la mer, il met les abîmes dans des réservoirs.
33:8	Que toute la Terre craigne YHWH ! Que tous les habitants du monde le redoutent !
33:9	Car il dit, et la chose arrive, il ordonne, et la chose se présente.
33:10	YHWH rompt le conseil des nations, il anéantit les desseins des peuples,
33:11	le conseil de YHWH tient pour toujours, les desseins de son cœur, d'âges en âges<!--Pr. 19:21.-->.
33:12	Heureuse la nation dont YHWH est l'Elohîm<!--Ps. 144:15.-->, le peuple qu'il s'est choisi pour héritage !
33:13	YHWH regarde des cieux, il voit tous les fils d'humains<!--Job 28:24.-->.
33:14	Du lieu de sa demeure, il observe tous les habitants de la Terre.
33:15	C'est lui qui forme également leur cœur et qui prend garde à toutes leurs actions.
33:16	Le roi n'est pas sauvé par une grande armée, l'homme puissant n'échappe pas par la grande force.
33:17	Pour le salut, le cheval n’est que mensonge, et il ne délivre pas par la grandeur de sa force<!--Ps. 147:10.-->.
33:18	Voici, l'œil de YHWH est sur ceux qui le craignent<!--Ps. 34:16 ; 1 Pi. 3:12.-->, sur ceux qui s'attendent à sa bonté,
33:19	pour délivrer leur âme de la mort et pour les faire vivre durant la famine.
33:20	Notre âme espère en YHWH : il est notre aide et notre bouclier.
33:21	Oui, notre cœur se réjouit en lui, oui, nous avons mis notre confiance en son saint Nom.
33:22	Que ta bonté soit sur nous, YHWH ! Selon l'espérance que nous avons eue en toi !

## Chapitre 34

### YHWH libère les siens

34:1	De David, lorsqu'il altéra son goût<!--« Jugement », « décision ». 1 S. 21:14.--> en face d'Abiymélek, qui s'en alla, chassé par lui.
34:2	[Aleph.] Je bénirai YHWH en tout temps ; sa louange sera continuellement dans ma bouche.
34:3	[Beth.] Mon âme se glorifie en YHWH ! Que les pauvres écoutent et se réjouissent !
34:4	[Guimel.] Glorifiez YHWH avec moi ! Élevons son Nom tous ensemble !
34:5	[Daleth.] J'ai cherché YHWH et il m'a répondu, il m'a délivré de toutes mes frayeurs.
34:6	[He. Vav.] Regarde-t-on vers lui, on est illuminé et les faces ne sont pas confuses.
34:7	[Zayin.] Cet affligé a appelé, et YHWH a entendu, et l'a sauvé de toutes ses détresses.
34:8	[Heth.] L'Ange de YHWH campe autour de ceux qui le craignent, et les délivre.
34:9	[Teth.] Goûtez et voyez combien YHWH est bon<!--1 Pi. 2:3.--> ! Heureux l'homme fort qui se confie en lui !
34:10	[Yod.] Craignez YHWH, vous ses saints ! Car rien ne manque à ceux qui le craignent.
34:11	[Kaf.] Les lionceaux éprouvent la disette et la faim, mais ceux qui cherchent YHWH ne manquent d'aucun bien.
34:12	[Lamed.] Venez, mes fils, écoutez-moi ! Je vous enseignerai la crainte de YHWH.
34:13	[Mem.] Qui est l'homme qui prend plaisir à la vie, qui aime la prolonger pour jouir du bonheur ?
34:14	[Noun.] Garde ta langue du mal, et tes lèvres des paroles trompeuses<!--1 Pi. 3:10.--> ;
34:15	[Samech.] détourne-toi du mal et fais-le bien, cherche la paix et poursuis-la<!--Hé. 12:14.-->.
34:16	[Ayin.] Les yeux<!--Voir Ps. 33:18 et 1 Pi. 3:12.--> de YHWH sont sur les justes, et ses oreilles sont attentives à leur cri.
34:17	[Pe.] Les faces de YHWH sont contre ceux qui font le mal, pour retrancher de la Terre leur mémoire<!--Jé. 44:11 ; Lé. 17:10.-->.
34:18	[Tsade.] Ils crient... YHWH entend et les délivre de toutes leurs détresses.
34:19	[Qof.] YHWH est près de ceux qui ont le cœur déchiré par la douleur, et il délivre ceux qui ont l'esprit abattu.
34:20	[Resh.] Le juste passe par beaucoup de souffrances, mais YHWH le délivre de toutes<!--2 Ti. 3:11.-->.
34:21	[Shin.] Il garde tous ses os, pas un d’eux n’est brisé.
34:22	[Tav.] Le mal tue le méchant, et ceux qui haïssent le juste sont détruits.
34:23	[Pe.] YHWH rachète l'âme de ses serviteurs, aucun de ceux qui se confient en lui ne sera détruit.

## Chapitre 35

### Prière au juste Juge

35:1	De David. YHWH ! Défends-moi contre mes adversaires, combats ceux qui me combattent !
35:2	Prends le petit et le grand bouclier, et lève-toi pour me secourir !
35:3	Tire la lance et ferme le passage à ceux qui me poursuivent ! Dis à mon âme : Je suis ton salut<!--Yeshuw`ah.--> !
35:4	Que ceux qui cherchent mon âme soient honteux et confus<!--Jé. 17:18 ; Ps. 40:15, 70:3.--> ! Que ceux qui méditent ma perte se retirent en arrière et rougissent !
35:5	Qu'ils deviennent comme la balle emportée par le vent<!--Es. 29:5 ; Os. 13:3.-->, et que l'Ange de YHWH les chasse !
35:6	Que leur chemin soit ténébreux et glissant, et que l'Ange de YHWH les poursuive !
35:7	Car sans cause ils ont caché une fosse sous un filet, sans cause ils l'ont creusée pour mon âme<!--Jé. 18:20 ; Ps. 57:7, 140:6, 141:9.-->.
35:8	Que la ruine les atteigne sans qu'ils le sachent, qu'ils soient capturés dans le filet qu'ils ont caché ! Qu'ils y tombent et soient dans la ruine !
35:9	Mon âme se réjouira en YHWH, elle exultera en son salut.
35:10	Tous mes os diront : YHWH, qui est semblable à toi ? Qui délivre l'affligé de celui qui est plus fort que lui, l'affligé et le pauvre de celui qui le pille ?
35:11	De faux témoins s'élèvent contre moi : on m'interroge sur ce que j'ignore.
35:12	Ils me rendent le mal pour le bien : privation pour mon âme<!--Ps. 38:21, 109:5.-->.
35:13	Mais moi, quand ils étaient malades, je me couvrais d'un sac, j'affligeais mon âme par le jeûne, je priais dans mon sein.
35:14	Comme pour un ami, pour un frère, j'étais abattu, en pleurs, comme pour le deuil d'une mère.
35:15	Quand je boite, ils se réjouissent et s'assemblent, ils s'assemblent contre moi sans que je le sache pour me frapper, ils me déchirent pour que je sois silencieux ;
35:16	les athées qui bégaient pour un gâteau ont grincé les dents contre moi.
35:17	Adonaï ! jusqu'à quand le verras-tu ? Détourne mon âme de leurs tempêtes, mon unique des lionceaux.
35:18	Je te célébrerai dans la grande assemblée, je te louerai parmi un peuple nombreux<!--Ps. 111:1.-->.
35:19	Que ceux qui sont mes ennemis par leurs mensonges ne se réjouissent pas de moi, que ceux qui me haïssent sans cause ne clignent pas l'œil<!--Jn. 15:25.--> !
35:20	Car ils ne parlent pas de paix, ils inventent des paroles de tromperies contre les gens tranquilles de la Terre.
35:21	Ils élargissent leur bouche contre moi et disent : Ah ! Ah ! Nos yeux l'ont vu !
35:22	YHWH, tu le vois ! Ne te tais pas<!--Ps. 83:2.--> ! Adonaï, ne t'éloigne pas de moi !
35:23	Réveille-toi, réveille-toi pour mon jugement<!--Ps. 44:24.-->, Adonaï mon Elohîm, pour ma cause.
35:24	Juge-moi selon ta justice, YHWH, mon Elohîm ! Qu'ils ne se réjouissent pas de moi !
35:25	Qu'ils ne disent pas dans leur cœur : Ah ! Notre âme ! Qu'ils ne disent pas : Nous l'avons englouti !
35:26	Que ceux qui se réjouissent de mon mal soient honteux et confondus ensemble ! Que ceux qui s'élèvent contre moi soient couverts de honte et de confusion !
35:27	Qu'ils poussent des cris de joie, qu'ils se réjouissent, ceux qui prennent plaisir à ma justice, qu'ils disent sans cesse : Grand est YHWH qui désire la paix de son serviteur !
35:28	Ma langue méditera ta justice et ta louange tous les jours.

## Chapitre 36

### Opposition : les justes et les méchants

36:1	De David, serviteur de YHWH. Au chef de musique.
36:2	La déclaration, la transgression du méchant est au milieu de son cœur. Il n'y a pas de crainte d'Elohîm devant ses yeux.
36:3	Car il se flatte lui-même à ses yeux pour trouver et haïr son iniquité.
36:4	Les paroles de sa bouche ne sont que méchanceté et tromperie, il cesse d'être sage et de faire le bien.
36:5	Il projette le malheur sur sa couche, il se tient sur un chemin qui n'est pas bon, il ne rejette pas le mal.
36:6	YHWH ! ta bonté atteint jusqu'aux cieux, ta fidélité jusqu'aux nuages<!--Ps. 57:11, 108:5.-->.
36:7	Ta justice est comme les montagnes de El, tes jugements sont un grand abîme. YHWH, tu sauves les humains et les bêtes.
36:8	Elohîm ! Combien est précieuse ta bonté ! Aussi les fils d'humains se retirent à l'ombre de tes ailes<!--Ps. 17:8, 57:2.-->.
36:9	Ils seront abondamment rassasiés de la graisse de ta maison, et tu les abreuveras au fleuve de tes délices.
36:10	Car la source de vies est auprès de toi, et par ta lumière nous voyons la lumière.
36:11	Étends ta bonté sur ceux qui te connaissent, et ta justice sur ceux qui ont le cœur droit !
36:12	Que le pied de l'orgueilleux ne s'avance pas sur moi, et que la main des méchants ne m'ébranle pas !
36:13	Là sont tombés ceux qui pratiquent la méchanceté, ils sont renversés et ne peuvent se relever.

## Chapitre 37

### Se confier en la justice de YHWH

37:1	De David. [Aleph.] Ne t'irrite pas contre les méchants, ne jalouse pas ceux qui s'adonnent à l'injustice<!--Pr. 23:17, 24:19.-->.
37:2	Car ils seront soudainement retranchés comme le foin, et ils se faneront comme l'herbe verte.
37:3	[Beth.] Confie-toi en YHWH, et fais ce qui est bon, aie la terre pour demeure et la fidélité pour pâture.
37:4	Fais de YHWH tes délices, et il te donnera ce que ton cœur désire.
37:5	[Guimel.] Recommande tes voies à YHWH, confie-toi en lui et il agira<!--Ps. 22:9, 55:23 ; Pr. 16:3.-->.
37:6	Il manifestera ta justice comme la lumière, et ton droit comme le soleil à son midi<!--Pr. 4:18.-->.
37:7	[Daleth.] Garde le silence devant YHWH, et tremble devant lui. Ne te fâche pas contre celui qui réussit dans ses voies, contre l'homme faiseur de complot.
37:8	[He.] Laisse la colère, abandonne la rage<!--Ep. 4:26.--> : ne te fâche pas seulement pour faire du mal.
37:9	Car les méchants seront retranchés, mais ceux qui se confient en YHWH hériteront la Terre.
37:10	[Vav.] Encore un peu de temps : plus de méchant ! Tu regardes sa place : plus personne<!--Job 7:10, 18:5, 20:9.--> !
37:11	Les pauvres hériteront la Terre, et ils feront leurs délices de leur abondante paix.
37:12	[Zayin.] Le méchant complote contre le juste, et grince ses dents contre lui.
37:13	Adonaï se rit de lui, car il voit que son jour approche.
37:14	[Heth.] Les méchants tirent leur épée et bandent leur arc, pour faire tomber le malheureux et le pauvre, pour massacrer ceux qui marchent dans la droiture<!--Ps. 11:2.-->.
37:15	Mais leur épée entre dans leur propre cœur, et leurs arcs se brisent.
37:16	[Teth.] Mieux vaut au juste le peu qu'il a, que l'abondance de beaucoup de méchants<!--Pr. 15:16-17 ; Ec. 4:6.-->,
37:17	car les bras des méchants seront brisés, mais YHWH soutient les justes.
37:18	[Yod.] YHWH connaît les jours de ceux qui sont intègres, et leur héritage demeure à jamais.
37:19	Ils ne sont pas honteux au jour du malheur, mais ils sont rassasiés au jour de la famine.
37:20	[Kaf.] Mais les méchants périssent, et les ennemis de YHWH, comme les beaux pâturages, s'évanouissent ; ils s'évanouissent en fumée.
37:21	[Lamed.] Le méchant emprunte et ne rend pas, mais le juste a compassion et donne.
37:22	Car ceux qu'il bénit hériteront la Terre, mais ceux qu'il maudit seront retranchés.
37:23	[Mem.] YHWH affermit les pas de l'homme fort, et il prend plaisir à ses voies.
37:24	S'il tombe, il ne sera pas abattu, car YHWH le soutient de sa main.
37:25	[Noun.] J'ai été jeune, j'ai vieilli, et je n'ai pas vu le juste abandonné ni sa postérité cherchant du pain.
37:26	Il est compatissant tout le temps, il prête, et sa postérité est bénie.
37:27	[Samech.] Détourne-toi de ce qui est mauvais et fais ce qui est bon, et tu demeureras éternellement.
37:28	Car YHWH aime ce qui est juste, et il n'abandonne pas ses fidèles. C'est pourquoi ils sont sous sa garde pour toujours, mais la postérité des méchants est retranchée.
37:29	[Ayin.] Les justes hériteront la Terre et y habiteront à perpétuité.
37:30	[Pe.] La bouche du juste prononce la sagesse, et sa langue déclare la justice.
37:31	La torah de son Elohîm est dans son cœur<!--Ps. 40:8-9.-->, aucun de ses pas ne chancellera.
37:32	[Tsade.] Le méchant épie le juste, et cherche à le faire mourir.
37:33	YHWH ne l'abandonne pas entre ses mains et ne le laisse pas condamner quand on le juge.
37:34	[Qof.] Espère en YHWH, et garde sa voie, et il t'élèvera pour que tu hérites la Terre, tu verras les méchants retranchés.
37:35	[Resh.] J'ai vu un méchant terrifiant s'étendre comme un arbre verdoyant.
37:36	Il a passé, et voici, il n'est plus. Je le cherche, et il ne se trouve plus.
37:37	[Shin.] Observe celui qui est intègre, et considère le juste, car il y a une issue pour l'homme de paix.
37:38	Mais les rebelles seront tous détruits, et ce qui sera resté des méchants sera retranché.
37:39	[Tav.] En YHWH est le salut des Justes, il est leur protection au temps de la détresse.
37:40	YHWH les secourt et les délivre, il les délivre des méchants et les sauve, parce qu'ils se confient en lui.

## Chapitre 38

### La tristesse du péché mène à la repentance

38:1	Psaume de David. Pour souvenir.
38:2	YHWH ! ne me juge pas dans ta colère et ne me châtie pas dans ta fureur.
38:3	Car tes flèches m'ont atteint, et ta main s'est appesantie sur moi.
38:4	Il n'y a rien de sain dans ma chair à cause de ta colère, ni de paix dans mes os à cause de mon péché.
38:5	Car mes iniquités s'élèvent au-dessus de ma tête, elles se sont appesanties comme un pesant fardeau, au-delà de mes forces<!--Ps. 40:13.-->.
38:6	Mes plaies ont une mauvaise odeur et sont purulentes, à cause de ma folie.
38:7	Je suis déformé, courbé à l’extrême, je marche en pleurant tout le jour.
38:8	Car un mal brûlant remplit mes reins, et dans ma chair il n'y a rien de sain.
38:9	Je suis affaibli et brisé, je rugis le cœur troublé.
38:10	Adonaï, tout mon désir est devant toi, et mon soupir ne t'es pas caché.
38:11	Mon cœur est agité çà et là, ma force m'abandonne, et la lumière de mes yeux n'est plus avec moi.
38:12	Ceux qui m'aiment et mes amis se tiennent loin de ma plaie, mes proches se tiennent loin<!--Job 19:13-14.-->.
38:13	Ceux qui cherchent mon âme tendent des pièges. Ceux qui cherchent mon malheur parlent de calamités et méditent des tromperies tous les jours.
38:14	Mais moi, je suis comme un sourd, comme un muet qui n'ouvre pas sa bouche.
38:15	Je suis comme un homme qui n'entend pas, et qui n'a pas de réplique dans sa bouche.
38:16	Oui, je m'attends à toi, YHWH ! tu me répondras, Adonaï mon Elohîm !
38:17	Oui, j'ai dit : Qu'ils ne se réjouissent pas à mon sujet ! Dès que mon pied chancelle, ils s'élèvent orgueilleusement contre moi<!--Ps. 94:18.--> !
38:18	Oui, moi, prêt à boiter, ma douleur est continuellement devant moi.
38:19	Oui, je confesse mon iniquité, je suis dans la crainte à cause de mon péché.
38:20	Mes ennemis sont vivants et puissants, ceux qui me haïssent à tort sont nombreux.
38:21	Ils me rendent le mal pour le bien, ils sont mes adversaires parce que je poursuis ce qui est bon<!--Jé. 18:20 ; Ps. 109:5.-->.
38:22	Ne m'abandonne pas YHWH ! Mon Elohîm, ne t'éloigne pas de moi !
38:23	Hâte-toi de venir à mon secours ! Adonaï, tu es ma délivrance !

## Chapitre 39

### La faiblesse de l'être humain

39:1	Au chef. À Yedoutoun. Psaume de David.
39:2	J'ai dit : Je prends garde à mes voies, de peur de pécher par ma langue. Je garderai sur ma bouche une muselière, tant que le méchant sera devant moi.
39:3	Je suis resté muet, dans le silence, je me suis tu à propos de ce qui est bon, et ma douleur a été agitée.
39:4	Mon cœur s'enflammait au-dedans de moi, un feu intérieur me consumait, j’ai parlé avec ma langue :
39:5	YHWH, fais-moi connaître ma fin, quelle est la mesure de mes jours<!--Ps. 119:84.-->, afin que je sache à quel point je suis éphémère<!--« Rejeté », « abstention », « transitoire », « fugitif ».--> !
39:6	Voici, tu as donné à mes jours la largeur de la main, la durée de ma vie est comme un rien devant toi. Certainement, tout être humain quoiqu'il soit debout n'est qu'une vapeur<!--Ja. 4:14.-->. Sélah.
39:7	Certainement, l'homme se promène comme une ombre ! Certainement on s'agite inutilement ! On amasse des biens et on ne sait pas qui les recueillera.
39:8	Maintenant, que puis-je espérer, Adonaï ? Mon espérance est en toi<!--Pr. 3:26 ; Ps. 71:5 ; 1 Ti. 1:1.-->.
39:9	Délivre-moi de toutes mes transgressions ! Ne m'expose pas aux insultes de l'insensé !
39:10	Je me suis tu et je n'ai pas ouvert ma bouche, parce que c'est toi qui agis.
39:11	Détourne de moi tes coups ! Je suis consumé par les attaques de ta main.
39:12	Tu châties l'homme en le punissant à cause de son iniquité, et, comme les mites, tu consumes ce qu’il a de précieux. Certainement, tout être humain n'est qu'une vapeur. Sélah.
39:13	YHWH, écoute ma prière et prête l'oreille à mon cri ! Ne sois pas sourd à mes larmes ! Car je suis un voyageur et un étranger chez toi, comme tous mes pères<!--Lé. 25:23 ; Ps. 119:19 ; 1 Pi. 2:11 ; Hé. 11:13.-->.
39:14	Détourne ton regard de moi, afin que je paraisse agréable, avant que je m'en aille et que je ne sois plus.

## Chapitre 40

### Un cantique nouveau à YHWH

40:1	Au chef. Psaume de David. 
40:2	J'ai espéré, j'ai espéré en YHWH, et il s'est tourné vers moi et a entendu mon cri.
40:3	Il m'a fait monter de la fosse du vacarme, de l'argile boueuse, il a dressé mes pieds sur le rocher, il a affermi mes pas.
40:4	Il a mis dans ma bouche un cantique nouveau, la louange de notre Elohîm. Beaucoup verront cela et ils craindront et se confieront en YHWH.
40:5	Heureux l'homme fort qui place sa confiance en YHWH et qui ne se tourne pas vers les orgueilleux, vers ceux qui s'adonnent au mensonge !
40:6	YHWH, mon Elohîm ! Qu'ils sont grands les merveilles et les desseins que tu as faits pour nous ! Personne n'est comparable à toi. Je voudrais les raconter et les proclamer, mais leur nombre est trop grand pour en faire le compte<!--Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH dont les œuvres ne pouvaient être racontées en détails ni par David, ni par Yohanan, tant elles sont nombreuses. Voir Jn. 21:25.-->.
40:7	Tu ne désires ni sacrifice ni offrande, tu m'as creusé les oreilles, tu ne demandes ni holocauste ni sacrifice pour le péché<!--Hé. 10:5.-->.
40:8	Alors je dis : Voici, je viens. Il est écrit de moi dans le rouleau du livre.
40:9	Mon Elohîm, je prends plaisir à faire ta volonté, et ta torah est au fond de mes entrailles<!--Es. 51:7 ; Ps. 37:31.-->.
40:10	Je prêche ta justice dans la grande assemblée. Vois, je ne ferme pas mes lèvres, YHWH, tu le sais !
40:11	Je ne cache pas ta justice, qui est dans mon cœur, je déclare ta fidélité et ta délivrance. Je ne cache pas ta bonté ni ta vérité dans la grande assemblée.
40:12	Toi, YHWH ! ne m'épargne pas tes compassions. Que ta bonté et ta vérité me gardent continuellement !
40:13	Car des maux sans nombre m'entourent : mes iniquités m'atteignent, et je ne supporte pas leur vue ; elles surpassent en nombre les cheveux de ma tête, et mon cœur m'abandonne.
40:14	Qu’il te plaise, YHWH, de me délivrer ! YHWH, hâte-toi de venir à mon secours !
40:15	Que tous ensemble ils soient honteux et confondus, ceux qui cherchent mon âme pour la perdre ! Et que ceux qui prennent plaisir à mon malheur retournent en arrière et rougissent !
40:16	Que ceux qui disent de moi : Ah ! Ah ! soient consumés en récompense de la honte qu'ils m'ont faite.
40:17	Que tous ceux qui te cherchent soient dans l'allégresse et se réjouissent en toi<!--Ps. 70:5.--> ! Que ceux qui aiment ta délivrance disent continuellement : Grand est YHWH !
40:18	Moi, je suis affligé et misérable, Adonaï prend soin de moi. Tu es mon secours et mon Sauveur. Mon Elohîm, ne tarde pas<!--Ps. 70:6.--> !

## Chapitre 41

### Secours de YHWH dans le malheur

41:1	Au chef. Psaume de David. 
41:2	Heureux celui qui s'intéresse au pauvre ! YHWH le délivrera au jour du malheur,
41:3	YHWH le gardera et conservera sa vie. Il sera heureux sur la Terre, et tu ne le livreras pas à l'âme de ses ennemis.
41:4	YHWH le soutiendra sur son lit de douleur, tu transformeras toute sa couche dans sa maladie.
41:5	Je dis : YHWH, aie pitié de moi ! Guéris mon âme ! car j'ai péché contre toi.
41:6	Mes ennemis disent du mal de moi : Quand mourra-t-il ? Et quand périra son nom ?
41:7	Si quelqu'un vient me voir, c'est pour dire des mensonges : son cœur fait provision de méchancetés<!--Ps. 5:10, 10:7, 12:3.-->. Il sort et parle dehors.
41:8	Tous ceux qui me haïssent chuchotent ensemble contre moi, ils projettent du mal contre moi :
41:9	Quelque chose de Bélial<!--Voir commentaire en De. 13:14.--> est attachée à lui. Le voilà couché, disent-ils, il ne se relèvera plus !
41:10	Même l'homme avec qui j'étais en paix, qui avait ma confiance et qui mangeait mon pain, a levé le talon contre moi<!--Il est question ici de la trahison du Mashiah par Yéhouda Iscariot (Judas) (Jn. 13:18-19).-->.
41:11	Mais toi, YHWH ! aie pitié de moi et relève-moi ! Je les paierai.
41:12	À ceci je saurai que tu prends plaisir en moi, si mon ennemi ne triomphe pas de moi.
41:13	Pour moi, tu m'as soutenu dans mon intégrité, et tu m'as établi pour toujours en ta présence.
41:14	Béni soit YHWH, l'Elohîm d'Israël, d'éternité en éternité ! Amen ! Amen !

## Chapitre 42

### Avoir soif d'Elohîm

42:1	Au chef. Poésie des fils de Koré.
42:2	Comme une biche halète après les cours d'eau, ainsi mon âme halète après toi, Elohîm !
42:3	Mon âme a soif d'Elohîm, d'El Haï<!--El vivant. Ps. 63:2, 84:3.--> : quand viendrai-je et verrai-je les faces d'Elohîm ?
42:4	Mes larmes sont devenues ma nourriture jour et nuit, quand on me dit tout le jour : Où est ton Elohîm<!--Ps. 80:6, 115:2.--> ?
42:5	Je me souviens de ces choses, en répandant mon âme au-dedans de moi, quand je passais avec la multitude, je me déplaçais lentement devant elle jusqu'à la maison d'Elohîm, avec des cris de joie et la louange d'une foule célébrant une fête.
42:6	Mon âme, pourquoi t'abats-tu, et murmures-tu au-dedans de moi ? Espère en Elohîm, car je le célébrerai encore ; ses faces sont le salut !
42:7	Mon Elohîm ! mon âme est abattue au-dedans de moi, aussi je me souviens de toi, depuis la terre du Yarden<!--Jourdain.-->, depuis l'Hermon, et depuis la montagne de Mitsear.
42:8	Un abîme appelle un autre abîme au bruit de tes ondées, toutes tes vagues et tes flots passent sur moi.
42:9	YHWH enverra de jour sa bonté, et de nuit son cantique sera avec moi, et ma prière sera au El qui est ma vie.
42:10	Je dis à El, mon Rocher : Pourquoi m'oublies-tu ? Pourquoi marcherai-je dans la tristesse à cause de l'oppression de l'ennemi ?
42:11	Comme avec une épée dans mes os, mes ennemis m'outragent, tandis qu'ils me disent tout le jour : Où est ton Elohîm ?
42:12	Mon âme, pourquoi t'abats-tu, et pourquoi murmures-tu au-dedans de moi ? Espère en Elohîm ! Oui, je le célébrerai encore, lui, le salut de mes faces et mon Elohîm.

## Chapitre 43

### Espérer dans la délivrance d'Elohîm

43:1	Juge-moi, Elohîm ! Défends ma cause contre une nation infidèle<!--Ps. 26:1, 35:1.--> ! Délivre-moi de l'homme trompeur et injuste.
43:2	Car c'est toi, Elohîm, qui es ma protection. Pourquoi me repousses-tu ? Pourquoi marcherai-je dans la tristesse à cause de l'oppression de l'ennemi ?
43:3	Envoie ta lumière et ta vérité, afin qu'elles me conduisent et m'introduisent dans la montagne de ta sainteté, et dans tes tabernacles !
43:4	Je viendrai à l'autel d'Elohîm, de El, ma joie et mon allégresse, et je te célébrerai sur la harpe, Elohîm, mon Elohîm !
43:5	Mon âme, pourquoi t'abats-tu, et pourquoi murmures-tu au-dedans de moi ? Attends-toi à Elohîm, car je le célébrerai encore : il est le salut de ma face et mon Elohîm<!--Ps. 42:6.-->.

## Chapitre 44

### Prière des affligés

44:1	Au chef. Poésie des fils de Koré. 
44:2	Elohîm, nous avons entendu de nos oreilles, nos pères nous ont raconté l'œuvre que tu as faite de leur temps, aux jours d'autrefois<!--Jg. 6:13 ; Ps. 77:12.-->.
44:3	De ta main tu as dépossédé des nations pour les établir, tu as brisé des peuples pour les étendre.
44:4	Car ce n'est pas par leur épée qu'ils ont pris possession de la terre, et ce n'est pas leur bras qui les a délivrés, mais c'est ta droite, c'est ton bras, c'est la lumière de tes faces, parce que tu les aimais.
44:5	Elohîm, tu es mon Roi : ordonne le salut de Yaacov !
44:6	Avec toi nous battrons nos adversaires, par ton Nom nous foulerons ceux qui s'élèvent contre nous.
44:7	Car je ne me confie pas en mon arc, et ce n'est pas mon épée qui me délivrera.
44:8	Mais tu nous délivreras de nos adversaires, et tu rendras confus ceux qui nous haïssent.
44:9	Nous nous glorifierons en Elohîm tout le jour et nous célébrerons à jamais ton Nom. Sélah.
44:10	Mais tu nous rejettes, tu nous confonds, et tu ne sors plus avec nos armées.
44:11	Tu nous fais reculer devant l'adversaire, et ceux qui nous haïssent enlèvent nos dépouilles.
44:12	Tu nous livres comme des brebis destinées à être dévorées, et tu nous as dispersés parmi les nations.
44:13	Tu as vendu ton peuple pour rien, et tu ne l’estimes pas à un grand prix<!--Es. 52:3 ; Jé. 15:13.-->.
44:14	Tu fais de nous une insulte pour nos voisins, une moquerie et une dérision pour ceux qui habitent autour de nous<!--Jé. 24:9 ; Ps. 79:4.-->.
44:15	Tu fais de nous le proverbe<!--Ou parabole.--> des nations, on hoche la tête sur nous parmi les peuples.
44:16	Ma confusion est tout le jour devant moi, et la honte couvre mes faces,
44:17	à cause des discours de celui qui fait des reproches et qui nous injurie, et à cause de l'ennemi et du vindicatif.
44:18	Tout cela nous est arrivé, et cependant nous ne t'avons pas oublié, et nous n'avons pas trompé ton alliance.
44:19	Notre cœur ne s'est pas détourné, nos pas ne se sont pas éloignés de tes sentiers,
44:20	pour que tu nous écrases dans le territoire des serpents, et que tu nous couvres de l'ombre de la mort<!--Ps. 23:4.-->.
44:21	Si nous avions oublié le Nom de notre Elohîm et étendu nos paumes vers un el étranger,
44:22	Elohîm ne le saurait-il pas, lui qui connaît les secrets du cœur ?
44:23	Mais nous sommes tous les jours mis à mort pour l'amour de toi, nous sommes regardés comme des brebis destinées à la boucherie<!--Es. 53:7.-->.
44:24	Lève-toi, pourquoi dors-tu Adonaï ? Réveille-toi ! Ne nous rejette pas à perpétuité !
44:25	Pourquoi caches-tu tes faces ? Pourquoi oublies-tu notre affliction et notre oppression ?
44:26	Car notre âme est abattue dans la poussière et notre ventre est attaché à la terre.
44:27	Lève-toi, aide-nous ! Rachète-nous en raison de ta bonté !

## Chapitre 45

### La beauté du Roi

45:1	Au chef. Poésie des fils de Koré. Chant du bien-aimé. Sur les lis<!--Probablement une fleur semblable au lis.-->.
45:2	Une parole agréable remue mon cœur. Je dis : Mon œuvre est pour le roi ! Ma langue sera comme la plume d'un scribe habile !
45:3	Tu es le plus beau des fils des humains ! La grâce est répandue sur tes lèvres : c'est pourquoi Elohîm t'a béni éternellement.
45:4	Homme vaillant, ceins ton épée sur ta hanche, ta majesté et ta magnificence,
45:5	et prospère dans ta magnificence ! Sois porté sur la parole de vérité, de douceur, et de justice, et ta droite lancera des choses terribles !
45:6	Tes flèches sont aiguisées, les peuples tomberont sous toi, elles iront au cœur des ennemis du roi.
45:7	Ton trône Elohîm, subsiste d'éternité en éternité<!--Voir Hé. 1:8.--> ! Le sceptre de ton règne est un sceptre d'équité.
45:8	Tu aimes la justice et tu hais le crime envers la torah. C'est pourquoi, Elohîm, ton Elohîm t'a oint d'une huile d'allégresse plus que tes associés<!--Voir Hé. 1:8-9.-->.
45:9	Tous tes vêtements sont parfumés de myrrhe, d'aloès et de casse. Dans les palais d'ivoire les instruments à cordes te réjouissent.
45:10	Des filles de rois sont parmi tes bien-aimées. La reine est à ta droite, parée d'or d'Ophir.
45:11	Écoute, jeune fille ! regarde et tends l'oreille : oublie ton peuple et la maison de ton père.
45:12	Le roi désire ta beauté ; puisqu'il est ton Seigneur, prosterne-toi devant lui.
45:13	La fille de Tyr et les plus riches des peuples te supplieront avec des présents.
45:14	La fille du roi est toute glorieuse à l’intérieur. Son vêtement est fait de broderies d'or.
45:15	Elle sera présentée au roi en étoffes brodées. Derrière elle, des jeunes filles, ses compagnes, sont amenées auprès de toi.
45:16	Elles seront amenées avec joie et allégresse, et elles entreront au palais du roi.
45:17	Tes fils seront à la place de tes pères, tu les établiras pour princes sur toute la Terre.
45:18	Je rappellerai ton Nom d’âge en âge. Alors tous les peuples te célébreront pour toujours et à perpétuité<!--Ps. 67:3-5.-->.

## Chapitre 46

### L'assurance du peuple d'Elohîm

46:1	Au chef. Cantique des fils de Koré. Sur Alamoth<!--Jeunes filles.-->.
46:2	Elohîm est notre retraite, notre force, et notre secours qui ne manque jamais dans les détresses<!--Ps. 9:10.-->.
46:3	C'est pourquoi nous ne craindrons pas quand la Terre est bouleversée, et que les montagnes chancellent au cœur des mers<!--Es. 54:10.-->,
46:4	quand ses eaux mugissent, écument et se soulèvent jusqu'à faire trembler les montagnes. Sélah.
46:5	Le fleuve et ses ruisseaux réjouissent la cité d'Elohîm, qui est le lieu saint, le tabernacle d'Élyon<!--Ez. 47:1-2 ; Za. 14:8-9 ; Jn. 7:38 ; Ap. 22:1-2.-->.
46:6	Elohîm est au milieu d'elle : Elle n'est pas ébranlée. Elohîm la secourt au tournant du matin<!--So. 3:16-17.-->.
46:7	Les nations murmurent, les royaumes s'ébranlent, il a fait entendre sa voix et la terre se fond.
46:8	YHWH Tsevaot est avec nous, l'Elohîm de Yaacov est pour nous une haute retraite. Sélah.
46:9	Venez, contemplez les œuvres de YHWH, quels ravages il a fait sur la Terre !
46:10	Il a fait cesser les guerres jusqu'au bout de la Terre, il a brisé l'arc et rompu la lance, il a consumé par le feu les chariots de guerre<!--Es. 2:4.-->.
46:11	Arrêtez, et sachez que je suis Elohîm ! Je suis élevé parmi les nations, je suis élevé sur toute la Terre !
46:12	YHWH Tsevaot est avec nous, l'Elohîm de Yaacov est pour nous une haute retraite. Sélah.

## Chapitre 47

### YHWH, l'Elohîm élevé

47:1	Au chef. Psaume des fils de Koré.
47:2	Peuples battez tous des paumes ! Poussez vers Elohîm des cris de joie avec une voix de triomphe !
47:3	car YHWH, Élyon, est redoutable. Il est le Grand Roi sur toute la Terre.
47:4	Il nous assujettit des peuples et des nations sous nos pieds.
47:5	Il nous choisit notre héritage, la gloire de Yaacov qu'il aime. Sélah.
47:6	Elohîm est monté avec un cri de joie, YHWH, avec le son du shofar.
47:7	Chantez à Elohîm, chantez ! Chantez à notre Roi, chantez !
47:8	Car Elohîm est le Roi de toute la Terre : chantez un cantique !
47:9	Elohîm règne sur les nations, Elohîm est assis sur son saint trône.
47:10	Les princes des peuples se rassemblent vers le peuple de l'Elohîm d'Abraham, car les boucliers de la Terre sont à Elohîm : il est puissamment élevé.

## Chapitre 48

### Sion, splendeur du grand Roi

48:1	Cantique. Psaume des fils de Koré.
48:2	YHWH est grand et extrêmement glorifié, dans la ville de notre Elohîm, sur la montagne de sa sainteté.
48:3	Belle est la colline, joie de toute la Terre, la montagne de Sion, le côté nord, c'est la ville du grand Roi !
48:4	Elohîm est connu dans ses palais pour une haute retraite.
48:5	Car voici, les rois se sont rencontrés, ils ont passé outre ensemble.
48:6	Ils ont regardé, ainsi stupéfaits, terrifiés, ils se sont enfuis !
48:7	Là un tremblement les a saisis, une douleur comme celle de l'enfantement<!--Es. 13:8.-->.
48:8	Par le vent d'orient, tu brises les navires de Tarsis !
48:9	Comme nous l'avions entendu, ainsi l'avons-nous vu dans la ville de YHWH Tsevaot, dans la ville de notre Elohîm : Elohîm l'établira pour toujours. Sélah.
48:10	Elohîm ! nous pensons à ta bonté au milieu de ton temple.
48:11	Elohîm ! comme ton Nom, ta louange retentit jusqu'aux extrémités de la Terre, ta droite est pleine de justice.
48:12	La montagne de Sion se réjouit, et les filles de Yéhouda sont dans la joie, à cause de tes jugements.
48:13	Entourez Sion, faites-en le tour, comptez ses tours.
48:14	Fixez votre cœur sur son rempart, examinez ses palais pour le raconter à la génération future.
48:15	Car cet Elohîm-là est notre Elohîm pour toujours et à perpétuité. C’est lui qui nous guidera par-dessus la mort.

## Chapitre 49

### Vanité des richesses terrestres

49:1	Au chef. Psaume des fils de Koré.
49:2	Vous tous peuples, entendez ceci, vous habitants du monde, prêtez l'oreille,
49:3	vous, fils d'Adam aussi bien que vous, fils de l'homme, riches et pauvres ensemble !
49:4	Ma bouche parlera de sagesse, et la méditation de mon cœur, de l'intelligence.
49:5	Je prêterai l'oreille à la parabole, j'expliquerai mes énigmes au son de la harpe.
49:6	Pourquoi craindrai-je au jour du malheur, quand l'iniquité de mes adversaires m'entoure ?
49:7	Ils mettent leur confiance dans leurs biens et se glorifient de l'abondance de leurs richesses.
49:8	Un homme ne peut racheter, racheter son frère ni donner à Elohîm sa rançon<!--Mt. 16:26 ; Mc. 8:36-37 ; Lu. 12:15-21.-->.
49:9	Le rachat de leur âme est trop coûteux, et il cessera d'être pour toujours.
49:10	Vivrait-on toujours, sans voir la fosse ?
49:11	Car on voit que les sages meurent, l'insensé et l'abruti périssent également, et ils laissent à d'autres leurs biens<!--Ec. 2:21, 6:2.-->.
49:12	Dans leur pensée, leurs maisons seraient éternelles, et leurs tabernacles d'âges en âges, ils appellent leurs sols par leurs noms.
49:13	Mais l'humain ne demeure pas dans ses honneurs, il est semblable aux bêtes qui périssent.
49:14	Tel est leur chemin, leur folie, et ceux qui les suivent se plaisent à leurs discours. Sélah.
49:15	Ils seront mis dans le shéol comme des brebis, la mort se nourrira d'eux, et au matin les justes les fouleront aux pieds, leur beau rocher s'usera, le shéol sera leur résidence<!--Job 24:19.-->.
49:16	Mais Elohîm rachètera mon âme de la main du shéol, car il me prendra<!--Ps. 68:19 ; Ep. 4:8-9.-->. Sélah.
49:17	N'aie pas peur si tu vois un homme s'enrichir et quand les trésors de sa maison se multiplient.
49:18	Car lorsqu'il mourra, il n'emportera rien, ses trésors ne descendront pas après lui<!--Job 27:16-19 ; 1 Ti. 6:7.-->.
49:19	Quoiqu’il ait béni son âme pendant sa vie, quoiqu’on te loue parce que tu te fais du bien,
49:20	tu iras pourtant vers la génération de tes pères, qui ne verront jamais la lumière.
49:21	L'humain qui est en honneur, qui n'a pas d'intelligence, est semblable aux bêtes qui périssent.

## Chapitre 50

### YHWH, le juste Juge

50:1	Psaume d'Asaph. El, Elohîm, YHWH a parlé et il a appelé toute la Terre, depuis le soleil levant jusqu'au soleil couchant.
50:2	De Sion, la perfection de la beauté, Elohîm resplendit.
50:3	Notre Elohîm viendra et ne gardera pas le silence. Devant lui un feu dévorant, et tout autour de lui une grosse tempête.
50:4	Il appellera les cieux d'en haut, et la Terre pour juger son peuple, en disant :
50:5	Rassemblez-moi mes bien-aimés, qui ont traité alliance avec moi par le sacrifice<!--Mt. 24:29-31.--> !
50:6	Les cieux proclament sa justice, car c'est Elohîm lui-même qui va juger ! Sélah.
50:7	Écoute, mon peuple ! et je parlerai, Israël, et je t'avertirai, moi, Elohîm, ton Elohîm.
50:8	Je ne te réprimande pas pour tes sacrifices, tes holocaustes sont continuellement devant moi.
50:9	Je ne prendrai pas de taureaux de ta maison ni de boucs de tes enclos<!--Ps. 40:7.-->.
50:10	Car tous les animaux des forêts sont à moi, toutes les bêtes qui paissent sur 1 000 montagnes.
50:11	Je connais tous les oiseaux des montagnes, et tout ce qui se meut dans les champs m'appartient.
50:12	Si j'avais faim, je ne t'en dirais rien, car le monde est à moi et tout ce qu'il renferme<!--1 Co. 10:26.-->.
50:13	Mangerais-je la chair des gros taureaux ? Et boirais-je le sang des boucs ?
50:14	Sacrifie pour Elohîm la louange et accomplis tes vœux envers Élyon !
50:15	Appelle-moi au jour de ta détresse, je te délivrerai, et tu me glorifieras<!--Ps. 37:5.-->.
50:16	Elohîm dit au méchant : Quoi ? Tu énumères mes lois et tu as mon alliance dans ta bouche !
50:17	Toi qui hais la correction, et qui jettes mes paroles derrière toi !
50:18	Si tu vois un voleur, tu te plais avec lui, et ta part est avec les adultères.
50:19	Ta bouche s'abandonne au mal, et ta langue s'attache à la tromperie.
50:20	Tu t'assieds et tu parles contre ton frère, tu couvres d'opprobre le fils de ta mère.
50:21	Tu as fait ces choses-là et j'ai gardé le silence. Tu t’es imaginé que je deviendrai, que je deviendrai comme toi ! Je te reprendrai, et je te les placerai en ordre devant tes yeux.
50:22	Comprenez cela, s’il vous plaît, vous qui oubliez Éloah, de peur que je ne déchire sans que personne ne vous délivre.
50:23	Celui qui sacrifie la louange me glorifie, et à celui qui veille sur sa voie, je lui ferai voir le salut d'Elohîm.

## Chapitre 51

### Le cœur repentant, sacrifice agréable à Elohîm

51:1	Au chef. Psaume de David. 
51:2	Lorsque Nathan, le prophète, vint à lui, après que David fut allé vers Bath-Shéba<!--2 S. 11 ; 2 S. 12.-->.
51:3	Elohîm ! Aie pitié de moi dans ta bonté ! Selon ta grande miséricorde, efface mes transgressions !
51:4	Lave-moi complètement de mon iniquité et purifie-moi de mon péché.
51:5	Car je connais mes transgressions, et mon péché est continuellement devant moi<!--Es. 59:12.-->.
51:6	Contre toi, toi seul, j’ai péché, et j'ai fait ce qui est mal à tes yeux, ainsi tu seras juste quand tu parleras, pur quand tu jugeras.
51:7	Voici, je suis né dans l'iniquité, et ma mère m'a conçu dans le péché.
51:8	Mais tu prends plaisir à la vérité au fond du cœur, et tu me fais connaître la sagesse au-dedans de moi.
51:9	Purifie-moi de mon péché avec de l'hysope, et je serai pur, lave-moi, et je serai plus blanc que la neige !
51:10	Fais-moi entendre la joie et l'allégresse, et les os que tu as brisés se réjouiront.
51:11	Détourne tes faces de mes péchés, et efface toutes mes iniquités.
51:12	Elohîm ! Crée en moi un cœur pur, et renouvelle en moi un esprit ferme<!--Mt. 5:8.-->.
51:13	Ne me chasse pas loin de tes faces, et ne m'enlève pas ton Esprit Saint.
51:14	Rends-moi la joie de ton salut et qu'un esprit bien disposé me soutienne.
51:15	J'enseignerai tes voies aux transgresseurs et les pécheurs reviendront à toi.
51:16	Elohîm, Elohîm de mon salut, délivre-moi du sang, et ma langue criera ta justice.
51:17	Adonaï, ouvre mes lèvres, et ma bouche annoncera ta louange.
51:18	Car tu ne prends pas plaisir aux sacrifices, autrement je t'en donnerais. L'holocauste ne t'est pas agréable.
51:19	Les sacrifices à Elohîm, c'est un esprit brisé. Elohîm ! tu ne méprises pas un cœur brisé et contrit.
51:20	Par ta faveur, fais du bien à Sion, bâtis les murs de Yeroushalaim !
51:21	Alors tu prendras plaisir aux sacrifices de justice, à l'holocauste, et aux sacrifices entièrement consumés. Alors on fera monter des taureaux sur ton autel.

## Chapitre 52

### Le sort de l'homme qui se confie en ses richesses

52:1	Au chef. Poésie de David. 
52:2	À l'occasion du rapport que Doëg, l'Édomite, vint faire à Shaoul, en lui disant : David s'est rendu dans la maison d'Achiymélek.
52:3	Pourquoi te vantes-tu du mal, vaillant homme ? La bonté de El est pour tous les jours !
52:4	Ta langue n'invente que des méchancetés, elle est aiguisée comme un rasoir, elle agit avec tromperie !
52:5	Tu aimes plus le mal que le bien, le mensonge plutôt que de dire la vérité. Sélah.
52:6	Tu aimes tous les discours pernicieux, le langage trompeur !
52:7	Aussi El te détruira pour toujours, il t'enlèvera et t'arrachera de ta tente, il te déracinera de la Terre des vivants. Sélah.
52:8	Les justes le verront, et auront de la crainte, et ils se riront d'un tel homme, disant :
52:9	Voilà cet homme fort qui n'a pas pris Elohîm pour sa protection, mais qui se confiait en ses grandes richesses et qui mettait sa force dans ses mauvais désirs<!--Es. 47:10 ; Lu. 12:15-21.-->.
52:10	Mais moi, je serai dans la maison d'Elohîm comme un olivier verdoyant. Je me confie dans la bonté d'Elohîm, pour toujours et à jamais.
52:11	Je te célébrerai à jamais, car tu agis. Je mettrai mon espérance en ton Nom, parce qu'il est bon envers tes fidèles.

## Chapitre 53

### Égarement des impies

53:1	Au chef. Poésie de David. Sur Machalath<!--Trouvé dans l'en-tête des PS 53 et PS 81. Sens douteux, probablement un mot clé dans un chant, qui donne le ton.-->.
53:2	L'insensé dit en son cœur : Il n'y a pas d'Elohîm ! Ils se sont corrompus, ils ont commis des injustices abominables ; il n'y a personne qui fasse ce qui est bon<!--Ps. 10:4 ; Ro. 1:20-21, 3:12.-->.
53:3	Elohîm a regardé des cieux les fils d'humains, pour voir s'il y a quelqu'un qui soit intelligent, qui cherche Elohîm.
53:4	Ils se sont tous détournés et se sont tous rendus odieux. Il n'y a personne qui fasse ce qui est bon, pas même un seul.
53:5	Ceux qui pratiquent la méchanceté n'ont-ils pas de connaissance ? Ils dévorent mon peuple comme s'ils dévoraient du pain. Ils n'invoquent pas Elohîm.
53:6	Ils trembleront de terreur là où il n'y a pas sujet de terreur, car Elohîm a dispersé les os de celui qui campe contre toi. Tu les confondras, car Elohîm les a rejetés.
53:7	Oh ! Qui fera partir de Sion le salut<!--Voir commentaire en Ge. 49:18.--> d'Israël ? Quand Elohîm aura ramené son peuple captif, Yaacov sera dans l'allégresse, Israël se réjouira.

## Chapitre 54

### La délivrance vient de YHWH

54:1	Au chef de musique. Poésie de David.
54:2	Lorsque les Ziyphiens vinrent dire à Shaoul : David n'est-il pas caché parmi nous<!--1 S. 23:19, 26:1.--> ?
54:3	Elohîm ! Délivre-moi par ton Nom et fais-moi justice par ta puissance.
54:4	Elohîm ! Écoute ma prière, prête l'oreille aux paroles de ma bouche !
54:5	Car des étrangers se sont levés contre moi, des gens terrifiants, qui ne mettent pas Elohîm devant eux, cherchent mon âme. Sélah.
54:6	Voici, Elohîm m'accorde son secours, Adonaï est avec ceux qui soutiennent mon âme.
54:7	Il fera retourner le mal sur mes ennemis. Détruis-les selon ta vérité !
54:8	Je t'offrirai de bon cœur des sacrifices. YHWH, je célébrerai ton Nom parce qu'il est bon !
54:9	Car il m'a délivré de toute détresse, et mes yeux voient mes ennemis.

## Chapitre 55

### Se garder des méchants

55:1	Au chef de musique. Poésie de David.
55:2	Elohîm ! Prête l'oreille à ma prière, et ne te cache pas loin de mes supplications !
55:3	Écoute-moi, et réponds-moi ! J'erre çà et là dans ma méditation et je suis agité
55:4	à la voix de l’ennemi, face à l'oppression du méchant. Oui, ils font chanceler sur moi le malheur, ils me haïssent avec colère.
55:5	Mon cœur tremble au-dedans de moi, et les terreurs de la mort tombent sur moi.
55:6	La crainte et le tremblement viennent sur moi, le frissonnement me couvre.
55:7	Je dis : Qui me donnera des ailes de colombe ? Je m'envolerais et je demeurerais ailleurs.
55:8	Voici, je m'enfuirais au loin et je demeurerais dans le désert. Sélah.
55:9	Je me hâterais de m'échapper loin du vent d'orage, loin de la tempête.
55:10	Adonaï, réduis à néant, divise leurs langues ! Car j'ai vu la violence et les querelles dans la ville ;
55:11	elles font jour et nuit le tour sur les murailles ; l'iniquité et la malice sont dans son sein.
55:12	Les calamités sont au milieu d'elle, et la tromperie et la fraude ne partent pas de ses places.
55:13	Car ce n'est pas mon ennemi qui m’insulte, je le supporterais. Ce n'est pas celui qui me hait qui s'élève contre moi, je me cacherais de lui.
55:14	Mais c'est toi, un mortel, que j'estimais, mon ami, toi que je connais bien<!--Ps. 41:10.--> !
55:15	Ensemble nous rendions si doux nos conseils, nous allions avec la multitude à la maison d'Elohîm.
55:16	Que la mort les séduise ! Qu'ils descendent vivants dans le shéol ! Car le mal est dans leur demeure, parmi eux dans leur assemblée.
55:17	Mais moi je crie à Elohîm, et YHWH me sauvera.
55:18	Le soir, le matin, et à midi je me plains et je gémis, et il entendra ma voix.
55:19	Il délivrera mon âme de la guerre et me rendra la paix, car ils sont nombreux contre moi.
55:20	El entendra et leur répondra, lui qui demeure depuis l'antiquité. Sélah. Ils ne changeront pas, ils ne craignent pas Elohîm.
55:21	Ils étendent leurs mains sur ceux qui sont en paix avec eux, ils profanent leur alliance.
55:22	Leur bouche est glissante comme le lait caillé et la guerre est dans leur cœur. Leurs paroles sont plus douces que l'huile, mais ce sont des épées nues.
55:23	Remets ton sort à YHWH, et il te soutiendra. Il ne permettra jamais que le juste tombe.
55:24	Mais toi, Elohîm ! tu les feras descendre dans la fosse de la destruction. Les hommes de sangs et de tromperie n’atteindront pas la moitié de leurs jours. Mais moi, je mets en toi ma confiance.

## Chapitre 56

### Se glorifier en la parole de YHWH

56:1	Au chef. Miktam<!--Hymne.--> de David. Sur Yonathelem-Rehoqiym<!--Signifie « Colombe des térébinthes lointains ».-->. Lorsque les Philistins le saisirent à Gath<!--1 S. 21:11-15.-->.
56:2	Elohîm, aie pitié de moi ! Car des mortels m'écrasent et m'oppriment, me faisant tout le jour la guerre, ils m'oppressent.
56:3	Mes adversaires me piétinent tout le jour ! Car ils sont nombreux ceux qui me font la guerre là-haut.
56:4	Le jour où j'aurai peur, je me confierai en toi.
56:5	Je me glorifierai en Elohîm, en sa parole. Je me confie en Elohîm, je n’ai peur de rien : que peuvent me faire les hommes<!--Ps. 118:6 ; Hé. 13:6.--> ?
56:6	Tous les jours ils déforment mes paroles, ils ne pensent qu'à me faire du mal.
56:7	Ils complotent, ils se tiennent cachés, ils observent mes pas, car ils cherchent ardemment mon âme.
56:8	C'est par la méchanceté qu'ils échapperaient ! Dans ta colère, Elohîm, fais tomber les peuples !
56:9	Tu comptes mes allées et venues, recueille mes larmes dans tes outres : ne sont-elles pas écrites dans ton livre ?
56:10	Le jour où je crierai à toi, mes ennemis retourneront en arrière ; je sais qu'Elohîm est pour moi.
56:11	Je me glorifierai en Elohîm, en sa parole, je me glorifierai en YHWH, en sa parole.
56:12	Je me confie en Elohîm, je ne craindrai rien : que peut me faire l'être humain ?
56:13	Elohîm ! J'accomplirai les vœux que je t'ai faits, je te rendrai des louanges.
56:14	Car tu as délivré mon âme de la mort, et mes pieds de la chute, afin que je marche devant Elohîm, à la lumière des vivants.

## Chapitre 57

### Avoir confiance en Elohîm dans les difficultés

57:1	Au chef. Miktam<!--Hymne.--> de David. Sur Al-Thasheth<!--Commande pour le chef musicien, ou peut-être titre d'une mélodie utilisée pour plusieurs psaumes. Voir Ps. 58:1, 59:1, 75:1.-->. Lorsqu'il s'enfuit de devant Shaoul dans la caverne<!--1 S. 22:1.-->.
57:2	Aie pitié de moi, Elohîm ! Aie pitié de moi ! Car mon âme cherche un refuge ; je cherche un refuge à l'ombre de tes ailes jusqu'à ce que les calamités soient passées<!--Ps. 17:8.-->.
57:3	J’appelle Elohîm Élyon, le El qui accomplit tout pour moi.
57:4	Il m'enverra des cieux la délivrance, il rendra honteux celui qui veut me dévorer. Sélah. Elohîm enverra sa bonté et sa vérité.
57:5	Mon âme est au milieu des lions, je suis couché au milieu des gens qui sont enflammés, parmi les fils d'Adam dont les dents sont des lances et des flèches, et dont la langue est une épée tranchante<!--Ps. 59:8, 64:4 ; Ja. 3:5-12.-->.
57:6	Elohîm, élève-toi sur les cieux ! Que ta gloire soit sur toute la Terre !
57:7	Ils ont préparé un filet pour mes pas : mon âme s’est courbée. Ils ont creusé une fosse devant moi, mais ils sont tombés dedans. Sélah.
57:8	Mon cœur est ferme, Elohîm ! Mon cœur est ferme, je chanterai et je ferai retentir mes instruments.
57:9	Réveille-toi, ma gloire ! Réveillez-vous, mon luth et ma harpe ! Je me réveillerai à l'aube du jour.
57:10	Adonaï, je te célébrerai parmi les peuples, je te chanterai parmi les nations.
57:11	Car ta bonté est grande jusqu'aux cieux, et ta vérité jusqu'aux nuages<!--Ps. 118:4-5.-->.
57:12	Elohîm ! Élève-toi sur les cieux ! Que ta gloire soit sur toute la Terre !

## Chapitre 58

### YHWH rend justice sur la Terre

58:1	Au chef. Miktam<!--Hymne.--> de David. Sur Al-Thasheth<!--Commande pour le chef musicien, ou peut-être titre d'une mélodie utilisée pour plusieurs psaumes.-->.
58:2	En effet, quand vous parlez, la justice est silencieuse. Vous, fils d'humains, jugez-vous avec droiture ?
58:3	Au contraire, vous pratiquez l’injustice dans votre cœur. Sur la Terre, c'est la violence de vos mains que vous pesez.
58:4	Les méchants se sont aliénés dès le sein maternel, ils se sont égarés dès le ventre, en parlant faussement.
58:5	Ils ont un venin semblable au venin du serpent, ils sont comme le cobra sourd, qui ferme son oreille,
58:6	qui n'entend pas la voix des enchanteurs, du charmeur le plus habile aux incantations.
58:7	Elohîm, brise-leur les dents dans leur bouche ! YHWH, brise les mâchoires des lionceaux !
58:8	Qu’ils soient rejetés comme des eaux et s’en aillent ! Quand ils bandent leur arc, que leurs flèches soient émoussées !
58:9	Qu'ils s'en aillent comme un escargot qui se fond ! Qu'ils ne voient pas le soleil comme l'avorton d'une femme !
58:10	Avant que vos chaudières aient senti le feu des épines, vertes ou enflammées, la tempête les emportera.
58:11	Le juste se réjouira quand il aura vu la vengeance, il lavera ses pieds avec le sang du méchant.
58:12	Les humains diront : Oui, il est un fruit pour le juste, Oui, il y a un Elohîm qui juge sur la Terre.

## Chapitre 59

### Intervention divine

59:1	Au chef. Miktam<!--Hymne.--> de David. Sur Al-Thasheth<!--Voir Ps. 57:1.-->. Lorsque Shaoul envoya garder la maison pour le tuer<!--1 S. 19:11.-->.
59:2	Mon Elohîm ! Délivre-moi de mes ennemis, protège-moi de ceux qui s'élèvent contre moi !
59:3	Délivre-moi de ceux qui pratiquent la méchanceté et garde-moi des hommes de sang !
59:4	Car les voici en embuscade contre mon âme. Des puissants complotent contre moi, bien qu'il n'y ait pas en moi de transgression ni de péché, YHWH !
59:5	Ils courent et se tiennent ferme contre celui qui est sans iniquité. Réveille-toi ! Viens à ma rencontre et regarde !
59:6	Toi, YHWH ! Elohîm Tsevaot, Elohîm d'Israël, réveille-toi pour visiter toutes les nations ! Ne fais grâce à aucun de ceux qui me trahissent ! Sélah.
59:7	Ils reviennent le soir, ils hurlent comme des chiens, ils font le tour de la ville.
59:8	Voici, de leur bouche ils font jaillir le mal, il y a des épées sur leurs lèvres<!--Ja. 3:5-12.-->. Car, disent-ils : Qui nous entend ?
59:9	Mais toi, YHWH, tu te riras d'eux, tu te moqueras de toutes les nations<!--Ps. 2:4.-->.
59:10	Quelle que soit leur force, je m'attends à toi, car Elohîm est ma haute retraite.
59:11	Elohîm qui me favorise me préviendra, Elohîm me fera voir mes adversaires<!--Ps. 118:7.-->.
59:12	Ne les tue pas, de peur que mon peuple ne l'oublie, fais-les errer par ta puissance et abats-les, Adonaï, notre bouclier !
59:13	Leur bouche pèche à chaque parole de leurs lèvres, qu'ils soient pris par leur orgueil ! Ils ne tiennent que des discours de malédiction et de mensonge.
59:14	Consume-les avec fureur, consume de sorte qu'ils ne soient plus ! Qu'on sache qu'Elohîm domine sur Yaacov et jusqu'aux extrémités de la Terre ! Sélah.
59:15	Qu'ils reviennent le soir, et qu'ils hurlent comme des chiens, et qu'ils fassent le tour de la ville !
59:16	Que ceux-là errent çà et là pour manger, qu'ils passent la nuit sans être rassasiés !
59:17	Mais moi je chanterai ta force, je célébrerai dès le matin ta bonté<!--Ps. 88:14.-->. Car tu es pour moi une haute retraite, et mon asile au jour de ma détresse.
59:18	Ma force ! je te chanterai, car Elohîm est ma haute retraite, mon Elohîm de bonté.

## Chapitre 60

### YHWH, le meilleur secours

60:1	Au chef. Miktam<!--Hymne.--> de David, pour enseigner. Sur Shoushannim-Edouth<!--Les lis.-->.
60:2	Quand il luttait avec les Syriens de Mésopotamie et avec les Syriens de Tsoba, et que Yoab revint et frappa 12 000 Édomites dans la vallée du sel<!--2 S. 8:3-13 ; 1 Ch. 18:3-12.-->.
60:3	Elohîm ! tu nous as rejetés, tu nous as dispersés, tu étais en colère, reviens vers nous !
60:4	Tu as secoué la Terre, tu l'as brisée : répare ses brèches, car elle chancelle !
60:5	Tu as fait voir à ton peuple des choses dures, tu nous as fait boire un vin d'étourdissement<!--Es. 51:17-21 ; Ap. 14:10.-->.
60:6	Tu as donné une bannière à ceux qui te craignent afin qu’ils s'enfuient de devant l’arc. Sélah.
60:7	Afin que ceux que tu aimes soient délivrés. Sauve par ta droite et réponds-nous<!--Ps. 108:6.--> !
60:8	Elohîm a parlé dans son lieu saint : Je me réjouirai, je partagerai Shekem, je mesurerai la vallée de Soukkoth.
60:9	Galaad est à moi, Menashè aussi est à moi. Éphraïm est la protection de ma tête, et Yéhouda, mon sceptre.
60:10	Moab est le bassin où je me lave, je jette ma sandale sur Édom. Terre des Philistins, pousse des cris de guerre à mon sujet<!--2 S. 8:2 ; 1 Ch. 18:2.--> !
60:11	Qui me conduira dans la ville forte ? Qui me conduira jusqu'en Édom ?
60:12	Ne sera-ce pas toi, Elohîm, qui nous avais rejetés, et qui ne sortais plus, Elohîm, avec nos armées ?
60:13	Donne-nous du secours contre la détresse ! Car le salut de l'humain est vain<!--Jé. 17:5 ; Ps. 108:13, 118:8.-->.
60:14	Avec Elohîm, nous agirons avec force, et c'est lui qui foule aux pieds nos ennemis<!--Ps. 108:14.-->.

## Chapitre 61

### Elohîm, le parfait refuge

61:1	Au chef de musique. Psaume de David.
61:2	Elohîm, je crie à toi, sois attentif à ma prière !
61:3	De l'extrémité de la Terre je crie à toi. Quand mon cœur devint faible, conduis-moi sur le rocher qui est trop haut pour moi !
61:4	Car tu es devenu mon refuge<!--Voir Ps. 32:7, 91:1.-->, une tour forte en face de l’ennemi.
61:5	Je séjournerai éternellement dans ta tente, je me réfugierai sous la couverture<!--Voir Ps. 32:7, 91:1, 119:114.--> de tes ailes. Sélah.
61:6	Car toi, Elohîm, tu entends mes vœux. Donne-moi l’héritage de ceux qui craignent ton Nom !
61:7	Ajoute des jours aux jours du roi, et que ses années soient d’âge en âge !
61:8	Qu'il demeure toujours dans la présence d'Elohîm ! Que la bonté et la vérité le gardent !
61:9	Ainsi je chanterai ton Nom à perpétuité, en accomplissant mes vœux jour après jour.

## Chapitre 62

### La confiance en Elohîm

62:1	Au chef. Psaume de David. D'après Yedoutoun.
62:2	Oui, mon âme se repose en Elohîm. C'est de lui que vient mon salut.
62:3	Oui, il est mon Rocher, mon salut, ma haute retraite, je ne serai pas beaucoup ébranlé.
62:4	Jusqu'à quand accablerez-vous de maux un homme ? Vous serez tous mis à mort, et vous serez comme le mur qui penche, comme une cloison qui a été ébranlée.
62:5	Ils complotent pour le bannir de son élévation. Ils prennent plaisir au mensonge, ils bénissent de leur bouche, mais au-dedans ils maudissent. Sélah.
62:6	Oui, sois tranquille près d'Elohîm, mon âme ! car mon espérance est en lui.
62:7	Oui, il est mon Rocher, mon salut, ma haute retraite, je ne serai pas ébranlé.
62:8	En Elohîm se trouvent mon salut et ma gloire, en Elohîm sont le Rocher de ma force et mon refuge.
62:9	Peuples, confiez-vous en lui en tout temps, déchargez votre cœur sur lui ! Elohîm est notre retraite. Sélah.
62:10	Oui, les fils d'humain ne sont qu'une vapeur<!--Ja. 4:14.--> ! Les fils de l'homme, un mensonge ! Sur la balance s'ils montaient ensemble, ils seraient moins qu'une vapeur.
62:11	Ne mettez pas votre confiance dans l'oppression et ne placez pas un vain espoir dans le vol. Quand les richesses s'accroissent, n'y fixez pas votre cœur.
62:12	Un, Elohîm a parlé, deux<!--Job 33:14.-->, j'ai entendu ceci : Oui, la force est à Elohîm.
62:13	Et c'est à toi, Adonaï, qu'appartient la bonté. Car tu rendras à chacun selon son œuvre<!--Jé. 32:19 ; Pr. 24:12 ; Job 34:11 ; Mt. 16:27 ; Ro. 2:6.-->.

## Chapitre 63

### Soif de la présence d'Elohîm

63:1	Psaume de David, lorsqu'il était dans le désert de Yéhouda<!--1 S. 22:5, 23:14-15.-->.
63:2	Elohîm ! tu es mon El, je te cherche au point du jour. Mon âme a soif de toi, ma chair soupire après toi sur cette terre aride, desséchée, et sans eau<!--Ps. 42:2, 84:3, 143:6.-->.
63:3	Ainsi je te contemple dans ton lieu saint pour voir ta force et ta gloire.
63:4	Car ta bonté vaut mieux que la vie, mes lèvres te louent.
63:5	Ainsi, je te bénirai toute ma vie<!--Ps. 104:33.-->, j'élèverai mes paumes en ton Nom.
63:6	Mon âme sera rassasiée comme de graisse et de moelle, et ma bouche te louera avec un chant de réjouissance.
63:7	Quand je me souviens de toi sur mon lit, je médite sur toi durant les veilles de la nuit<!--Ps. 16:7, 119:55.-->.
63:8	Car tu es devenu un secours pour moi, je me réjouirai à l'ombre de tes ailes.
63:9	Mon âme s'est attachée à toi pour te suivre, ta droite me soutient.
63:10	Mais ceux qui cherchent à ruiner mon âme iront dans les parties inférieures de la Terre.
63:11	Ils seront livrés au pouvoir de l'épée, ils seront la portion des renards.
63:12	Mais le roi se réjouira en Elohîm. Quiconque jure par lui s'en glorifiera, car la bouche de ceux qui mentent sera fermée<!--Ps. 107:42 ; Job 5:16.-->.

## Chapitre 64

### YHWH, le seul abri

64:1	Au chef. Psaume de David.
64:2	Elohîm ! écoute ma voix quand je m'écrie ! Protège ma vie contre l'ennemi que je crains !
64:3	Cache-moi des complots des méchants, de l'assemblée tumultueuse de ceux qui pratiquent la méchanceté !
64:4	Ils aiguisent leur langue comme une épée<!--Jé. 9:2 ; Ps. 11:2, 59:8.-->, ils tendent leurs flèches, leurs paroles amères,
64:5	pour tirer de leurs cachettes contre celui qui est parfait. Ils tirent soudainement sur lui et n'ont aucune crainte.
64:6	Ils se fortifient dans des paroles mauvaises, ils ne parlent que de cacher des pièges et disent : Qui les verra<!--Job 24:15.--> ?
64:7	Ils conçoivent de l'injustice : ils achèvent les complots qu'ils conçoivent. L'intérieur de l'homme et son cœur sont insondables !
64:8	Mais Elohîm lance contre eux ses traits, soudain les voilà frappés.
64:9	Leur langue a causé leur chute, tous ceux qui les voient secouent leur tête.
64:10	Et tous les humains craindront et raconteront l'œuvre d'Elohîm, et considéreront ce qu'il aura fait.
64:11	Le juste se réjouira en YHWH et cherchera en lui son refuge, et tous ceux qui sont droits de cœur s'en glorifieront<!--Ps. 63:12, 97:12.-->.

## Chapitre 65

### Le règne de YHWH sur la nature

65:1	Au chef. Psaume de David. Cantique. 
65:2	Elohîm ! dans le calme, on te louera dans Sion, et l'on accomplira nos vœux<!--Ps. 50:14, 66:13.-->.
65:3	Tu entends nos prières. Toute chair viendra jusqu'à toi.
65:4	Les actions d'iniquités prévalent sur moi, tu feras la propitiation pour nos transgressions.
65:5	Heureux celui que tu choisis et que tu fais approcher de toi pour qu'il habite dans tes parvis ! Nous serons rassasiés des biens de ta maison, des biens du saint lieu de ton temple.
65:6	Dans ta justice, tu nous réponds par des choses terribles, Elohîm de notre salut, espoir de toutes les extrémités lointaines de la Terre et de la mer.
65:7	Il affermit les montagnes par sa force, il est ceint de puissance.
65:8	Il apaise le mugissement de la mer, le mugissement de ses flots et le vacarme des peuples.
65:9	Ceux qui habitent aux extrémités de la Terre ont peur de tes prodiges. Tu réjouis l'orient et l'occident.
65:10	Tu visites la Terre, tu lui donnes l'abondance et tu l’enrichis beaucoup. Le ruisseau d'Elohîm est plein d'eau, tu prépares le blé, car c’est ainsi que tu la prépares.
65:11	Tu arroses ses sillons, et tu aplanis ses mottes. Tu l'amollis par de grosses averses, et tu bénis son germe<!--Es. 55:10 ; Ps. 104:13-14.-->.
65:12	Tu couronnes l'année de tes bienfaits, et de tes pistes coule la graisse.
65:13	Elle coule sur les pâturages du désert et les collines se ceignent d’allégresse.
65:14	Les prés s'habillent de brebis et les vallées s'enveloppent de grain. Ils poussent des cris de triomphe, oui ils chantent.

## Chapitre 66

### Louange à l'Elohîm de grâces

66:1	Au chef. Cantique. Psaume. Vous tous habitants de toute la Terre, poussez des cris de triomphe vers Elohîm,
66:2	chantez la gloire de son Nom, faites éclater sa gloire par vos louanges !
66:3	Dites à Elohîm : Que tes œuvres sont redoutables ! Tes ennemis seront trouvés menteurs devant la grandeur de ta force.
66:4	Toute la Terre se prosterne devant toi et te chante, elle chante ton Nom. Sélah.
66:5	Venez et voyez les œuvres d'Elohîm ! Il est redoutable quand il agit sur les fils d'humains.
66:6	Il a fait de la mer une terre sèche, on a passé le fleuve à pied sec. Là, nous nous sommes réjouis en lui<!--Ex. 14:21 ; Jos. 3:14-17.-->.
66:7	Il domine par sa puissance pour toujours, ses yeux observent les nations<!--Ps. 14:2, 33:13 ; Job 28:24.-->. Que les rebelles ne s'élèvent pas ! Sélah.
66:8	Peuples, bénissez notre Elohîm et faites entendre la voix de sa louange !
66:9	Il met notre âme dans la vie, et il n'a pas permis que nos pieds chancellent.
66:10	Car tu nous as éprouvés, Elohîm, tu nous fonds comme l'argent se fond.
66:11	Tu nous as amenés dans le filet, tu as mis sur nos reins un pesant fardeau.
66:12	Tu as fait monter des mortels sur notre tête, et nous sommes entrés dans le feu et dans l'eau. Mais tu nous as fait sortir dans l'abondance.
66:13	J'entrerai dans ta maison avec des holocaustes, j'accomplirai mes vœux envers toi<!--Ps. 22:26, 76:12, 116:14.-->.
66:14	Pour eux, mes lèvres se sont ouvertes, et ma bouche les a déclarés dans ma détresse.
66:15	Je te ferai monter en holocauste des brebis grasses, avec l'encens des béliers, je te sacrifierai des taureaux et des boucs. Sélah.
66:16	Vous tous qui craignez Elohîm, venez, écoutez, et je raconterai ce qu'il a fait pour mon âme.
66:17	Je l'ai appelé de ma bouche, et sa louange était sur ma langue.
66:18	Si j'avais conçu l'iniquité dans mon cœur, Adonaï ne m'aurait pas écouté<!--Jn. 9:31.-->.
66:19	Mais Elohîm m'a écouté, il a été attentif à la voix de ma prière.
66:20	Béni soit Elohîm, qui n'a pas rejeté ma prière, et qui n'a pas éloigné de moi sa bonté.

## Chapitre 67

### Louange des peuples

67:1	Au chef de musique. Psaume. Cantique.
67:2	Qu'Elohîm ait pitié de nous et qu'il nous bénisse, qu'il fasse briller ses faces sur nous<!--No. 6:25 ; Ps. 4:7, 31:17, 119:135.-->. Sélah.
67:3	Afin que ta voie soit connue sur la Terre et ton salut parmi toutes les nations.
67:4	Les peuples te célébreront, Elohîm ! Tous les peuples te célébreront<!--Ps. 22:27, 68:33.--> !
67:5	Les peuples se réjouiront et chanteront de joie, car tu juges les peuples avec droiture et tu conduis les nations sur la Terre<!--Ps. 96:10.-->. Sélah.
67:6	Les peuples te célébreront, Elohîm ! Tous les peuples te célébreront !
67:7	La Terre produira son fruit. Elohîm, notre Elohîm, nous bénira.
67:8	Elohîm nous bénira, et toutes les extrémités de la terre le craindront.

## Chapitre 68

### YHWH, l'Elohîm glorieux

68:1	Au chef. Psaume. Cantique de David.
68:2	Qu'Elohîm se lève, et ses ennemis seront dispersés, et ceux qui le haïssent s'enfuiront devant lui<!--No. 10:35.-->.
68:3	Tu les chasseras comme la fumée est chassée par le vent, comme la cire se fond devant le feu ! Les méchants périront devant Elohîm<!--Ps. 37:20, 97:5.-->.
68:4	Mais les justes se réjouiront, ils exulteront devant Elohîm, ils tressailliront de joie<!--Ps. 67:4-5.-->.
68:5	Chantez à Elohîm, célébrez son Nom ! Frayez le chemin à celui qui est monté à cheval<!--Es. 40 ; Ap. 19.--> par les régions arides ! Son Nom est Yah ! Réjouissez-vous dans sa présence !
68:6	Il est le père des orphelins et le juge des veuves. Elohîm est dans sa demeure sainte<!--Ps. 146:9.-->.
68:7	Elohîm donne une famille à ceux qui étaient abandonnés, il fait sortir les captifs pour leur prospérité, mais les rebelles habitent sur une terre déserte.
68:8	Elohîm, tu es sorti devant ton peuple, tu as marché dans le désert ! Sélah.
68:9	La Terre a tremblé et les cieux ont distillé leurs eaux devant Elohîm, le Mont Sinaï a tremblé devant Elohîm, l'Elohîm d'Israël<!--Ex. 19:18 ; Jg. 5:5.-->.
68:10	Elohîm ! tu as fait tomber une pluie abondante sur ton héritage, et quand il était épuisé, tu l'as rétabli.
68:11	Ton troupeau a habité sur la terre que dans ta bonté, Elohîm, tu avais préparée pour les malheureux.
68:12	Adonaï donne une parole, et les messagères de bonnes nouvelles sont une grande armée.
68:13	Les rois des armées se sont enfuis, ils se sont enfuis, et celle qui se tenait à la maison a partagé le butin<!--1 S. 30:16.-->.
68:14	Tandis que vous vous couchez dans les étables, les ailes de la colombe sont couvertes d'argent, et son plumage est d'un jaune d'or.
68:15	Quand Shaddaï dispersa les rois sur la terre, il devint blanc comme la neige du Tsalmon.
68:16	La montagne d'Elohîm est une montagne de Bashân<!--« Fertile », « large ». District à l'est du Yarden (Jourdain) connu pour sa fertilité et attribué à la demi-tribu de Menashè (Manassé).-->, une montagne élevée, une montagne de Bashân.
68:17	Pourquoi l'insultez-vous, montagnes dont le sommet est élevé ? Elohîm a désiré cette montagne pour y habiter, et YHWH y demeurera à jamais.
68:18	Les chars d'Elohîm sont deux myriades, des milliers. Adonaï est au milieu d'eux, le Sinaï est dans le sanctuaire.
68:19	Tu es monté dans la hauteur, tu as emmené des captifs, tu as pris des dons pour les distribuer parmi les humains, et même parmi les rebelles, afin qu'ils habitent dans le lieu de Yah Elohîm<!--Ep. 4:8-10. Cette prophétie concerne la résurrection du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ).-->.
68:20	Béni soit Adonaï qui, jour après jour, nous comble de ses biens ! El est notre salut<!--yeshuw`ah.-->. Sélah.
68:21	El est pour nous le El de délivrance ! C'est à YHWH Adonaï de faire sortir de la mort<!--Jn. 11:25 et 44.-->.
68:22	En effet, Elohîm brisera la tête de ses ennemis<!--Ge. 3:15.-->, le sommet de la tête chevelue de celui qui vit dans sa culpabilité.
68:23	Adonaï dit : Je les ramènerai de Bashân<!--No. 21:33-35.-->, je les ramènerai du fond de la mer,
68:24	afin que tu plonges ton pied dans le sang<!--Ps. 58:11.-->, et que la langue de tes chiens ait sa part des ennemis.
68:25	Ils voient ta marche, Elohîm ! Ils ont vu ta marche dans le lieu saint, la marche de mon El, mon Roi.
68:26	Les chanteurs allaient devant, ensuite les joueurs d'instruments, et au milieu les vierges jouant du tambour<!--Ex. 15:20 ; 1 S. 18:6.-->.
68:27	Bénissez Elohîm dans les assemblées, bénissez Adonaï, vous qui êtes descendants d'Israël !
68:28	Là sont Benyamin, le plus jeune, qui domine sur eux, les chefs de Yéhouda et leur multitude, les chefs de Zebouloun, et les chefs de Nephthali.
68:29	Ton Elohîm ordonne que tu sois puissant. Affermis, Elohîm, ce que tu as fait pour nous.
68:30	Dans ton temple, à Yeroushalaim, les rois t'amèneront des présents<!--1 R. 10:10 ; Ps. 72:10 ; 2 Ch. 32:23.-->.
68:31	Épouvante les bêtes sauvages des roseaux, la troupe des taureaux, et les veaux des peuples, et ceux qui se prosternent avec des pièces d'argent ! Disperse les peuples qui prennent plaisir à la guerre !
68:32	Des princes viendront de l'Égypte ; l'Éthiopie se hâtera d'étendre ses mains vers Elohîm.
68:33	Royaumes de la Terre, chantez à Elohîm, célébrez Adonaï ! Sélah.
68:34	Chantez à celui qui est monté dans les cieux des cieux, les cieux éternels ! Voici, il fait retentir de sa voix un son puissant.
68:35	Donnez la force à Elohîm ! Sa majesté est sur Israël, et sa force est dans les nuages.
68:36	Elohîm, tu es redouté à cause de ton sanctuaire. Le El d'Israël est celui qui donne la force et la puissance à son peuple. Béni soit Elohîm !

## Chapitre 69

### Elohîm, attentif à la prière de ceux qui s'humilient

69:1	Au chef. De David. Sur les lis.
69:2	Délivre-moi, Elohîm ! car les eaux menacent mon âme<!--Ps. 124:4, 144:7.-->.
69:3	Je suis enfoncé dans un bourbier profond, sans appui. Je suis entré dans les profondeurs des eaux, et les courants d'eau me submergent.
69:4	Je suis las de crier, mon gosier se dessèche, mes yeux se consument pendant que je m'attends à Elohîm.
69:5	Ceux qui me haïssent sans cause<!--Jn. 15:25.-->, dépassent en nombre les cheveux de ma tête. Ils sont puissants, ceux qui veulent me détruire, qui sont à tort mes ennemis. Je dois rendre ce que je n'avais pas ravi.
69:6	Elohîm ! tu connais ma folie, et mes délits ne te sont pas cachés.
69:7	Adonaï YHWH Tsevaot ! que ceux qui se confient en toi ne soient pas honteux à cause de moi ! Et que ceux qui te cherchent ne soient pas humiliés à cause de moi, Elohîm d'Israël !
69:8	Car c'est pour toi que je porte l'insulte, que la honte couvre mon visage.
69:9	Je suis devenu un étranger pour mes frères, et un homme de dehors pour les fils de ma mère<!--Ge. 31:14-15 ; Jn. 7:3-5.-->.
69:10	Car le zèle de ta maison me dévore<!--Jn. 2:17 ; Ro. 15:3.-->, et les insultes de ceux qui t'insultaient sont tombées sur moi.
69:11	Je pleure, mon âme est dans le jeûne, et je suis devenu une insulte.
69:12	Je prends un sac pour vêtement, et je suis devenu le sujet de leurs proverbes.
69:13	Ceux qui sont assis à la porte parlent de moi, et je suis la musique<!--Job 30:9 ; La. 3:14.--> des buveurs de boissons fortes.
69:14	Mais je t'adresse ma prière, YHWH<!--Ps. 102:2.--> ! Que ce soit le temps favorable, Elohîm ! Par ta grande bonté, réponds-moi en m'assurant ta délivrance.
69:15	Délivre-moi de la boue, que je ne m'y enfonce pas<!--Ps. 40:3.-->, que je sois délivré de ceux qui me haïssent et des profondeurs des eaux.
69:16	Que les courants d'eau ne me submergent plus, que les profondeurs ne m'engloutissent pas, et que le puits ne ferme pas sa bouche sur moi !
69:17	YHWH, réponds-moi, car ta bonté est agréable ! Dans tes grandes compassions, tourne tes faces vers moi !
69:18	Ne cache pas tes faces à ton serviteur, car je suis en détresse. Hâte-toi, réponds-moi !
69:19	Approche-toi de mon âme, rachète-la ! Délivre-moi à cause de mes ennemis !
69:20	Tu connais toi-même mon opprobre, ma honte et mon ignominie. Tous mes ennemis sont devant toi.
69:21	L'insulte m'a brisé le cœur, et je suis languissant ; j'ai attendu que quelqu'un ait compassion de moi, mais il n'y en a pas eu. J'ai attendu des consolateurs, mais je n'en ai pas trouvé.
69:22	Ils m'ont au contraire donné du fiel<!--Mt. 27:34,48.--> pour mon repas et, dans ma soif, ils m'ont abreuvé de vinaigre.
69:23	Que leur table soit pour eux un piège et un appât au sein de leur paix !
69:24	Que leurs yeux soient tellement obscurcis, qu'ils ne puissent pas voir. Fais continuellement chanceler leurs reins !
69:25	Répands ton indignation sur eux, et que l'ardeur de ta colère les saisisse !
69:26	Que leur campement soit désolé, et qu'il n'y ait personne qui habite dans leurs tentes !
69:27	Car ils persécutent celui que tu avais frappé, et racontent les souffrances de ceux que tu blesses.
69:28	Mets des iniquités à leurs iniquités, qu'ils n'entrent pas dans ta justice !
69:29	Qu'ils soient effacés du livre de vie, et qu'ils ne soient pas inscrits avec les justes !
69:30	Mais pour moi, qui suis affligé et dans la douleur, ton salut, Elohîm, m'élèvera en une haute retraite.
69:31	Je louerai le Nom d'Elohîm par des cantiques et je le glorifierai par des louanges.
69:32	Cela est agréable à YHWH, plus qu'un taureau avec des cornes et des sabots fendus.
69:33	Les humbles le voient et ils se réjouissent, que votre cœur vive, vous qui cherchez Elohîm !
69:34	Car YHWH entend les indigents, il ne méprise pas ses prisonniers.
69:35	Que les cieux et la Terre le louent, la mer et tout ce qui y rampe<!--Ps. 96:11.--> !
69:36	Car Elohîm délivrera Sion, et bâtira les villes de Yéhouda. On y habitera et l’on en prendra possession.
69:37	Et la postérité de ses serviteurs en fera son héritage, et ceux qui aiment son Nom y auront leur demeure.

## Chapitre 70

### Le pauvre et l'indigent

70:1	Au chef. De David, pour souvenir.
70:2	Elohîm, hâte-toi de me délivrer ! YHWH, hâte-toi de venir à mon secours<!--Ps. 40:14, 71:12.--> !
70:3	Que ceux qui cherchent mon âme soient honteux et confondus<!--Ps. 35:4, 71:13.--> ! Et que ceux qui prennent plaisir à mon mal soient repoussés en arrière et soient confus !
70:4	Que ceux qui disent : Ah ! Ah ! retournent en arrière par l'effet de leur honte.
70:5	Que tous ceux qui te cherchent, exultent et se réjouissent en toi ! Et que ceux qui aiment ta délivrance disent toujours : Glorifié soit Elohîm !
70:6	Moi, je suis affligé et misérable. Elohîm ! hâte-toi de venir vers moi ! Tu es mon secours et mon Sauveur. YHWH ! ne tarde pas !

## Chapitre 71

### Demeurer en Elohîm jusqu'au bout

71:1	YHWH, je cherche en toi mon refuge : que je ne sois jamais confus !
71:2	Délivre-moi par ta justice et sauve-moi ! Incline ton oreille vers moi, mets-moi en sûreté !
71:3	Sois pour moi le Rocher de mon refuge, afin que je puisse toujours m'y retirer ! Tu as donné l'ordre de me mettre en sûreté, car tu es mon Rocher et ma forteresse.
71:4	Mon Elohîm, délivre-moi de la main du méchant, de la paume du pervers et de l'oppresseur !
71:5	Car tu es mon espérance<!--Ps. 39:8 ; Pr. 3:26 ; 1 Ti. 1:1.-->, Adonaï YHWH ! ma confiance dès ma jeunesse.
71:6	Je m'appuie sur toi dès le ventre : c'est toi qui m'as fait sortir des entrailles de ma mère<!--Ps. 22:10-11.-->. Tu es ma louange à perpétuité.
71:7	Je suis pour beaucoup comme un miracle, mais tu es mon puissant refuge.
71:8	Que ma bouche soit remplie de ta louange et de ta gloire tout le jour !
71:9	Ne me rejette pas au temps de ma vieillesse ! Ne m'abandonne pas maintenant que ma force est consumée !
71:10	Car mes ennemis ont parlé de moi, et ceux qui épient mon âme ont pris conseil ensemble,
71:11	disant : Elohîm l'a abandonné. Poursuivez-le et saisissez-le, car il n'y a personne qui le délivre.
71:12	Elohîm, ne t'éloigne pas de moi ! Mon Elohîm, hâte-toi de venir à mon secours !
71:13	Que ceux qui sont les ennemis de mon âme soient honteux et défaits ! Et que ceux qui cherchent mon malheur soient enveloppés d'insulte et de honte !
71:14	Mais moi, j'espérerai toujours et je te louerai tous les jours davantage.
71:15	Ma bouche racontera tout le jour ta justice et ta délivrance, bien que je n'en sache pas le nombre.
71:16	Je marcherai par la force d'Adonaï YHWH, je raconterai ta justice, la tienne seule.
71:17	Elohîm ! tu m'as enseigné dès ma jeunesse, et j'ai annoncé jusqu'à présent tes merveilles.
71:18	Même jusqu’à la vieillesse et aux cheveux gris, Elohîm, ne m'abandonne pas, afin que j'annonce ton bras<!--Voir Es. 30:30, 33:2, 40:10, 40:11, 48:14, 51:5 et 9, 52:10, 53:1, 59:16, 62:8, 63:5 et 12.--> à la génération présente, ta puissance à la génération à venir.
71:19	Car ta justice, Elohîm, est haut élevée, car tu as fait de grandes choses ! Elohîm, qui est semblable à toi ?
71:20	Tu m'as fait éprouver bien des détresses et des malheurs, mais tu me redonneras la vie et tu me feras monter hors des abîmes de la Terre.
71:21	Relève ma grandeur et console-moi encore !
71:22	Je te louerai au son du luth, je chanterai ta fidélité, mon Elohîm, je te célèbrerai avec la harpe, Saint d'Israël !
71:23	Mes lèvres et mon âme, que tu as rachetées, pousseront des cris de joie quand je te chanterai ;
71:24	ma langue aussi publiera tout le jour ta justice, car ceux qui cherchent mon malheur seront honteux et confondus.

## Chapitre 72

### Le Royaume messianique

72:1	De Shelomoh. Elohîm, donne tes jugements au roi et ta justice au fils du roi.
72:2	Qu'il juge avec justice ton peuple, et tes malheureux avec équité !
72:3	Que les montagnes portent la paix pour le peuple, et les collines la justice !
72:4	Qu'il fasse droit aux malheureux du peuple, qu'il délivre les fils du misérable, et qu'il écrase l'oppresseur !
72:5	Ils te craindront tant que le soleil et la lune dureront, d'âges en âges.
72:6	Il descendra comme la pluie sur l'herbe fauchée, comme les grosses averses qui arrosent la Terre.
72:7	En son temps, le juste fleurira, et il y aura abondance de paix jusqu'à ce qu'il n'y ait plus de lune.
72:8	Il dominera de la mer à la mer, et du fleuve jusqu'aux extrémités de la Terre.
72:9	Les habitants des déserts se courberont devant lui, et ses ennemis lécheront la poussière.
72:10	Les rois de Tarsis et des îles ramèneront des dons, les rois de Saba et de Séba présenteront des présents.
72:11	Tous les rois aussi se prosterneront devant lui, toutes les nations le serviront.
72:12	Car il délivrera le pauvre qui crie au secours, l'affligé et celui qui n'a personne pour l'aider<!--Ps. 34:18 ; Job 29:12.-->.
72:13	Il aura compassion du pauvre et du misérable, et il sauvera les âmes des misérables,
72:14	il rachètera leur âme de la fraude et de la violence, et leur sang sera précieux à ses yeux.
72:15	Il vivra, et on lui donnera de l'or de Séba, et on fera des prières pour lui continuellement. On le bénira tout le jour.
72:16	Il y aura abondance du blé sur la terre, au sommet des montagnes. Leurs fruits trembleront comme le Liban, ils fleuriront dans les villes comme l'herbe de la terre.
72:17	Son Nom est éternel, son Nom se perpétuera devant le soleil, et on se bénira en lui ! Toutes les nations le diront béni.
72:18	Béni soit YHWH Elohîm, l'Elohîm d'Israël, qui seul fait des choses merveilleuses !
72:19	Béni soit éternellement son Nom glorieux ! Et que toute la Terre soit remplie de sa gloire ! Amen ! Oui, amen !
72:20	Fin des prières de David, fils d'Isaï.

## Chapitre 73

### L'orgueil des méchants

73:1	Psaume d'Asaph. En effet, Elohîm est bon pour Israël, pour ceux qui ont le cœur pur<!--Mt. 5:8.-->.
73:2	Et quant à moi, pour un peu mes pieds allaient se détourner, il s'en est fallu d'un rien que mes pas ne glissent,
73:3	car j'étais envieux des vantards en voyant la paix des méchants.
73:4	Parce que rien ne les tourmente jusqu'à leur mort, et leur corps est gras,
73:5	ils n'ont pas de part aux peines des mortels, et ils ne sont pas frappés comme les humains.
73:6	C'est pourquoi l'orgueil leur sert de collier, et la violence les couvre comme un vêtement.
73:7	Les yeux leur sortent dehors à force de graisse, ils dépassent les imaginations du cœur.
73:8	Ils sont moqueurs et parlent méchamment d'opprimer, ils parlent avec hauteur.
73:9	Ils élèvent leur bouche jusqu'aux cieux, et leur langue parcourt la Terre.
73:10	C'est pourquoi son peuple se tourne de leur côté, et on draine vers eux un plein d'eaux.
73:11	Ils disent : Comment El saurait-il ? Existe-t-il une connaissance chez Élyon<!--Es. 29:15 ; Ez. 8:12 ; Ps. 94:7 ; Job 22:12-13.--> ?
73:12	Voilà, ceux-ci sont méchants, ils prospèrent toujours dans ce monde et acquièrent de plus en plus de richesses.
73:13	En effet, c'est en vain que j'ai purifié mon cœur et que j'ai lavé mes paumes dans l'innocence<!--Mal. 3:14 ; Job 35:3.--> :
73:14	je suis frappé tous les jours, et tous les matins mon châtiment est là.
73:15	Si je disais : Je veux parler comme eux. Voici, je trahirais la génération de tes fils.
73:16	Alors j'ai médité pour comprendre ces choses, mais cela m'a paru extrêmement difficile,
73:17	jusqu'à ce que je sois entré dans le sanctuaire de El, et que j'aie considéré la fin de telles gens.
73:18	En effet, tu les as mis sur des voies glissantes, tu les fais tomber dans des précipices.
73:19	Comment ! En un instant les voilà ruinés, ils sont finis, consumés par la terreur.
73:20	Ils sont comme un rêve lorsqu'on s'est réveillé. Adonaï, tu méprises leur image à ton réveil.
73:21	Quand mon cœur s'aigrissait et que je me sentais percé dans les entrailles,
73:22	j'étais alors abruti et je n'avais aucune connaissance, j'étais comme une bête dans ta présence.
73:23	Je serai toujours avec toi : tu m'as pris par la main droite.
73:24	Tu me conduiras par ton conseil, et tu me recevras dans la gloire.
73:25	Qui ai-je dans les cieux ? Avec toi, je ne désire rien sur la Terre.
73:26	Ma chair et mon cœur étaient consumés, mais Elohîm est le Rocher de mon cœur et ma part pour l'éternité.
73:27	Car voilà, ceux qui s'éloignent de toi périront, tu retrancheras tous ceux qui se détournent de toi.
73:28	Mais pour moi, m'approcher d'Elohîm, c'est mon bonheur : j'ai mis toute mon espérance dans Adonaï YHWH, afin de raconter toutes tes œuvres.

## Chapitre 74

### Appel au secours du peuple d'Elohîm

74:1	Poésie d'Asaph. Elohîm, pourquoi nous as-tu rejetés pour toujours ? Et pourquoi ta colère fume-t-elle contre le troupeau de ton pâturage<!--Ps. 79:5.--> ?
74:2	Souviens-toi de ton assemblée que tu as acquise<!--Ac. 20:28.--> autrefois, la tribu de ton héritage que tu as rachetée, ce mont Sion où tu demeures.
74:3	Élève tes pas vers les lieux constamment dévastés ! L'ennemi a tout renversé dans le lieu saint.
74:4	Tes adversaires ont rugi au milieu de ton assemblée ; ils ont mis leurs signes pour signes.
74:5	On les a vus pareils à celui qui lève la cognée dans une épaisse forêt.
74:6	Et maintenant, avec des haches et des marteaux, ils brisent les sculptures.
74:7	Ils ont mis le feu à ton sanctuaire, ils ont profané par terre le tabernacle de ton Nom<!--2 R. 25:9.-->.
74:8	Ils disaient dans leur cœur : Traitons-les tous avec violence ! Ils ont brûlé sur la terre tous les lieux de El.
74:9	Nous ne voyons plus nos signes, il n'y a plus de prophètes, et personne parmi nous qui sache jusqu'à quand<!--La. 2:9-10.-->...
74:10	Elohîm ! jusqu'à quand l'adversaire te couvrira-t-il d'opprobres ? Et l'ennemi méprisera-t-il ton Nom à jamais ?
74:11	Pourquoi retires-tu ta main, ta droite ? Tire-la du milieu de ton sein ! Détruis !
74:12	Elohîm est mon Roi dès les temps anciens, faisant des délivrances au milieu de la Terre.
74:13	Tu as fendu la mer par ta force, tu as brisé les têtes des monstres marins sur les eaux,
74:14	tu as brisé les têtes du léviathan, tu l'as donné pour nourriture au peuple du désert.
74:15	Tu as ouvert la fontaine et le torrent, tu as desséché les grosses rivières.
74:16	À toi est le jour, à toi aussi est la nuit ! Tu as établi la lumière et le soleil.
74:17	Tu as posé toutes les limites de la Terre, tu as formé l'été et l'hiver.
74:18	Souviens-toi de ceci : que l'ennemi a blasphémé YHWH, et qu'un peuple insensé a méprisé ton Nom !
74:19	Ne livre pas aux vivants l'âme de la tourterelle, n'oublie pas pour toujours la vie de tes affligés !
74:20	Regarde à ton alliance, car les lieux ténébreux de la Terre sont remplis de pâturages de violence.
74:21	Que l’opprimé ne s’en retourne pas confus. Que l'affligé et le pauvre louent ton Nom !
74:22	Elohîm, lève-toi, défends ta cause ! Souviens-toi de l'insulte qui t'est faite tous les jours par l'insensé !
74:23	N'oublie pas le cri de tes adversaires, le vacarme de ceux qui s'élèvent contre toi monte continuellement !

## Chapitre 75

### L'élévation vient de YHWH

75:1	Au chef. Psaume d'Asaph. Cantique. Sur Al-Thasheth<!--Voir Ps. 57:1.-->.
75:2	Nous te louons, Elohîm, nous te louons. Ton Nom est près de nous, nous racontons tes merveilles.
75:3	Au temps que j'aurai fixé, je jugerai avec droiture.
75:4	La Terre se dissout avec tous ceux qui y habitent, mais j'affermis ses colonnes. Sélah.
75:5	Je dis à ceux qui se vantent : Ne vous vantez pas ! Et aux méchants : N'élevez pas la corne !
75:6	N'élevez pas si haut votre corne, et ne parlez pas, le cou arrogant.
75:7	Car l'élévation ne vient ni de l'orient, ni de l'occident, ni du désert.
75:8	Mais c'est Elohîm qui gouverne : il abaisse l'un, et il élève l'autre<!--1 S. 2:7.-->.
75:9	Mais il y a dans la main de YHWH<!--Es. 51:17-22 ; Jé. 25:27-28 ; Ap. 14:10, 16:19.--> une coupe où fermente un vin plein de mélange. Il en verse en effet, et tous les méchants de la Terre en suceront, ils en boiront jusqu'à la lie.
75:10	Mais moi, je raconterai ces choses à jamais, je chanterai à l'Elohîm de Yaacov.
75:11	J'abattrai toutes les cornes des méchants, mais les cornes du juste s'élèveront.

## Chapitre 76

### La puissance d'Elohîm redoutable

76:1	Au chef de musique. Psaume d’Asaph. Cantique.
76:2	Elohîm est connu en Yéhouda, son Nom est grand en Israël !
76:3	Sa tanière est à Shalem<!--« Paix, paisible ». Voir Ge. 14:18, 33:18.-->, sa demeure à Sion.
76:4	Là il a brisé les arcs étincelants, le bouclier, l'épée et les armes de guerre. Sélah.
76:5	Tu es brillant, plus majestueux que les montagnes de proie.
76:6	Les hommes au cœur puissant ont été dépouillés, ils se sont endormis de leur sommeil, et ils n'ont plus trouvé leurs mains, tous ces hommes talentueux.
76:7	Elohîm de Yaacov, les cavaliers et les chevaux se sont endormis quand tu les as menacés.
76:8	Tu es redoutable, toi ! Qui peut se tenir devant toi quand ta colère éclate ?
76:9	Des cieux tu fais entendre ton jugement, la Terre a peur et se tient tranquille,
76:10	quand tu te lèves, Elohîm ! pour faire justice, pour délivrer tous les affligés de la Terre ! Sélah.
76:11	L'être humain te célèbre, même dans sa fureur, quand tu te ceins de toute ta colère.
76:12	Faites vos vœux à YHWH votre Elohîm et accomplissez-les ! Que tous ceux qui l'environnent apportent des dons à l'Elohîm redoutable !
76:13	Il coupe le souffle des princes, il est redoutable pour les rois de la Terre !

## Chapitre 77

### Se souvenir des prodiges de YHWH

77:1	Au chef. Psaume d'Asaph. D'après Yedoutoun.
77:2	De ma voix je crie vers Elohîm : de ma voix, vers Elohîm, et il m'écoutera.
77:3	Je cherche Adonaï au jour de ma détresse. Sans cesse mes mains s'étendent durant la nuit, mon âme refuse d'être consolée.
77:4	Je me souviens d'Elohîm, et je gémis ; je médite, et mon esprit est affaibli. Sélah.
77:5	Tu as saisi les paupières de mes yeux ; agité, je ne parle pas.
77:6	Je pense aux jours d'autrefois, aux années des temps passés<!--Ps. 143:5.-->.
77:7	Je me souviens de ma musique pendant la nuit, je médite en mon cœur, et mon esprit fait des recherches.
77:8	Adonaï m'a-t-il rejeté pour toujours ? Ne me sera-t-il plus favorable ?
77:9	Sa bonté a-t-elle disparu à jamais ? Sa parole a-t-elle pris fin d'âges en âges ?
77:10	El a-t-il oublié d'avoir compassion ? A-t-il dans sa colère retiré sa miséricorde ? Sélah.
77:11	Puis je dis : C'est bien ce qui m'affaiblit, la droite d'Élyon a changé.
77:12	Je me souviens des actions de Yah. Oui, je me souviens de tes merveilles d'autrefois.
77:13	Je méditerai toutes tes œuvres, et je parlerai de tes œuvres.
77:14	Elohîm ! tes voies sont saintes. Quel el est grand comme Elohîm ?
77:15	Tu es le El qui fait des merveilles ! Tu as fait connaître ta force parmi les peuples.
77:16	Tu as racheté par ton bras ton peuple, les fils de Yaacov et de Yossef. Sélah.
77:17	Les eaux t'ont vu, Elohîm ! Les eaux t'ont vu et ont tremblé : même les abîmes ont été agités.
77:18	Les nuées ont versé un déluge d'eau, les nuées ont fait retentir leur son, tes flèches ont volé de toutes parts.
77:19	La voix de ton tonnerre était dans le tourbillon, les éclairs ont brillé sur le monde, la Terre a été agitée et a tremblé.
77:20	Tu te frayas un chemin par la mer, un sentier par les grosses eaux, et tes traces ne furent plus reconnues.
77:21	Tu as conduit ton peuple comme un troupeau, par la main de Moshé et d'Aaron<!--Mi. 6:4.-->.

## Chapitre 78

### Les œuvres d'Elohîm dans l'histoire d'Israël

78:1	Poésie d'Asaph. Mon peuple, écoute ma torah ! Prêtez vos oreilles aux paroles de ma bouche !
78:2	J'ouvrirai ma bouche en paraboles, je ferai jaillir les énigmes cachées des temps anciens<!--Mt. 13:35.-->.
78:3	Ce que nous avons entendu et connu, et que nos pères nous ont raconté<!--Ps. 44:2.-->,
78:4	nous ne le cacherons pas à leurs fils. Ils raconteront à la génération à venir les louanges de YHWH, sa puissance et les merveilles qu'il a faites.
78:5	Il a établi le témoignage en Yaacov, il a mis la torah en Israël, qu'il a ordonné à nos pères de faire connaître à leurs fils<!--De. 4:9.-->,
78:6	pour qu'elle soit connue de la génération future, des fils qui naîtraient, et pour que lorsqu'ils seront grands, ils la relatent à leurs fils,
78:7	afin qu'ils mettent leur confiance en Elohîm, et qu'ils n'oublient pas les œuvres de El, et qu'ils gardent ses commandements.
78:8	Afin qu'ils ne deviennent pas comme leurs pères, une génération réfractaire et rebelle, une génération dont le cœur n'est pas ferme et dont l'esprit n'est pas fidèle à El<!--Ex. 32:9 ; Ac. 7:51.-->.
78:9	Les fils d'Éphraïm, armés et tirant de l'arc, tournèrent le dos le jour de la bataille.
78:10	Ils ne gardèrent pas l'alliance d'Elohîm et refusèrent de marcher selon sa torah.
78:11	Ils oublièrent ses œuvres et ses merveilles qu'il leur avait fait voir.
78:12	Il avait fait des miracles en présence de leurs pères, en terre d'Égypte, dans le champ de Tsoan.
78:13	Il fendit la mer et les fit passer au travers, il fit arrêter les eaux comme un monceau de pierres.
78:14	Il les conduisit de jour par la nuée, et toute la nuit par une lumière de feu<!--Ex. 13:21.-->.
78:15	Il fendit les rochers dans le désert, et leur donna à boire comme des abîmes abondants.
78:16	Il fit sortir des ruisseaux de la roche<!--Ex. 17:6 ; No. 20:11 ; 1 Co. 10:4.--> et fit couler des eaux comme des rivières.
78:17	Toutefois, ils continuèrent à pécher contre lui, irritant Élyon dans le désert.
78:18	Ils tentèrent El dans leurs cœurs en demandant de la viande pour leur âme.
78:19	Ils parlèrent contre Elohîm, en disant : El pourrait-il dresser une table dans ce désert<!--No. 11:4.--> ?
78:20	Voici, il a frappé le rocher, et les eaux ont coulé et des torrents ont débordé. Pourrait-il aussi donner du pain, ou préparer de la chair pour son peuple ?
78:21	C'est pourquoi, YHWH, entendant cela, fut transporté : un feu s'alluma contre Yaacov, la colère aussi monta contre Israël.
78:22	Parce qu'ils n'avaient pas cru en Elohîm et ne s'étaient pas confiés en sa délivrance.
78:23	Il donna des ordres aux nuages d'en haut, il ouvrit les portes des cieux ;
78:24	il fit pleuvoir la manne sur eux pour leur nourriture et il leur donna le blé des cieux<!--Ex. 16:14 ; Jn. 6:31.-->.
78:25	Les hommes mangèrent le pain des grands. Il leur envoya de la nourriture à satiété.
78:26	Il excita dans les cieux le vent d'orient et il amena par sa puissance le vent du Théman.
78:27	Il fit pleuvoir sur eux de la chair comme de la poussière, des oiseaux ailés comme le sable des mers.
78:28	Il les fit tomber au milieu du camp, autour de leurs tabernacles.
78:29	Ils mangèrent et furent pleinement rassasiés, il fit venir pour eux leur désir.
78:30	Leur désir n'était pas dégoûtant, leur viande était encore dans leur bouche,
78:31	que la colère d'Elohîm monta contre eux : il tua les plus gras d'entre eux, il abattit les jeunes hommes d'Israël<!--1 Co. 10:5.-->.
78:32	Malgré cela, ils péchèrent encore et ne crurent pas à ses prodiges<!--No. 14:2.-->.
78:33	C'est pourquoi il consuma leurs jours par la vanité et leurs années par une fin soudaine.
78:34	Quand il les tuait, alors ils le recherchaient, ils se repentaient et ils cherchaient El dès le matin.
78:35	Ils se souvenaient qu'Elohîm était leur Rocher<!--1 Co. 10:4.-->, que El Élyon était leur Racheteur.
78:36	Mais ils le trompaient de leur bouche et ils lui mentaient de leur langue<!--Es. 29:13 ; Jé. 12:2 ; Mt. 15:8.-->.
78:37	Leur cœur n’était pas ferme avec lui, et ils ne furent pas fidèles à son alliance.
78:38	Mais lui, étant compatissant, faisait propitiation<!--« Couvrir », « purger », « faire une expiation », « réconciliation », « recouvrir de poix ».--> pour l'iniquité et ne les détruisait pas, mais il détourna souvent sa colère et ne réveilla pas toute sa fureur.
78:39	Il se souvenait qu'ils n'étaient que chair, un souffle<!--Le mot hébreu signifie aussi « esprit ».--> qui s'en va et ne revient pas.
78:40	Que de fois ils lui furent rebelles dans le désert, ils l'affligèrent dans les solitudes !
78:41	Ils revenaient tenter El, blesser le Saint<!--Voir commentaire en Ac. 3:14.--> d'Israël.
78:42	Ils ne se souvinrent pas de sa main, du jour où il les racheta de l'ennemi,
78:43	des miracles qu'il accomplit en Égypte, et de ses merveilles dans les champs de Tsoan.
78:44	Il changea en sang leurs fleuves et leurs ruisseaux, et ils ne purent en boire les eaux<!--Ex. 7:20.-->.
78:45	Il envoya contre eux des essaims d'insectes qui les dévorèrent, et des grenouilles qui les détruisirent<!--Ex. 8:2-20.-->.
78:46	Il livra leurs récoltes aux sauterelles, le produit de leur travail aux sauterelles<!--Ex. 10:13.-->.
78:47	Il détruisit leurs vignes par la grêle, et leurs sycomores par les orages<!--Ex. 9:23.-->.
78:48	Il livra leur bétail à la grêle, et leurs troupeaux aux foudres étincelantes.
78:49	Il envoya sur eux l'ardeur de sa colère, la fureur, la rage et la détresse, une troupe d'anges de malheur.
78:50	Il aplanit un sentier pour sa colère : il ne retira pas leur âme de la mort, il livra leur vie à la peste<!--Ex. 9:6.-->.
78:51	Il frappa tout premier-né en Égypte, les premiers de la vigueur sous les tentes de Cham<!--Ex. 12:29.-->.
78:52	Il fit partir son peuple comme des brebis, il les conduisit comme un troupeau dans le désert.
78:53	Il les dirigea sûrement, pour qu'ils soient sans crainte, et la mer couvrit leurs ennemis.
78:54	Il les fit venir à sa sainte frontière, à cette montagne que sa droite a acquise<!--Ex. 15:17.-->.
78:55	Il chassa devant eux les nations, leur assigna par le sort leur part d'héritage, et fit habiter les tribus d'Israël dans les tentes de ces nations.
78:56	Mais ils tentèrent et irritèrent Elohîm Élyon, et ne gardèrent pas ses témoignages.
78:57	Et ils se retirèrent en arrière et furent infidèles comme leurs pères. Ils tournèrent comme un arc trompeur.
78:58	Ils l'irritèrent par leurs hauts lieux, et ils provoquèrent sa jalousie par leurs images gravées<!--De. 32:16-21.-->.
78:59	Elohîm l'entendit : il s'emporta et il méprisa fortement Israël.
78:60	Il abandonna le tabernacle de Shiyloh, la tente où il habitait parmi les humains.
78:61	Il livra en captivité sa force et son ornement entre les mains de l'ennemi.
78:62	Il livra son peuple à l'épée et s'emporta contre son héritage.
78:63	Le feu dévora leurs jeunes hommes, et leurs vierges ne furent pas vantées<!--Voir Ge. 12:15. Le mot hébreu signifie aussi « briller », « louer ».-->.
78:64	Leurs prêtres tombèrent par l'épée, et leurs veuves ne les pleurèrent pas.
78:65	Puis Adonaï se réveilla comme un homme qui se serait endormi, comme un puissant homme vaincu par le vin.
78:66	Il frappa ses adversaires par derrière, et les mit en insulte perpétuelle.
78:67	Il rejeta la tente de Yossef, et ne choisit pas la tribu d'Éphraïm.
78:68	Mais il choisit la tribu de Yéhouda, la montagne de Sion, celle qu'il aime.
78:69	Il bâtit son sanctuaire dans les lieux élevés, et l'établit comme la Terre qu'il a fondée pour toujours.
78:70	Il choisit David, son serviteur, il le prit de l'enclos des moutons<!--1 S. 16:11 ; 2 S. 7:8.-->,
78:71	derrière les brebis qui allaitent et l'amena pour paître Yaacov, son peuple, Israël, son héritage.
78:72	Et David les fit paître selon l'intégrité de son cœur et les conduisit par l'intelligence de ses paumes.

## Chapitre 79

### Appel au jugement d'Elohîm

79:1	Psaume d'Asaph. Elohîm, les nations sont entrées dans ton héritage. On a rendu impur ton saint temple, on a mis Yeroushalaim en monceaux de pierres.
79:2	On a livré les cadavres de tes serviteurs pour viande aux oiseaux des cieux, et la chair de tes fidèles aux bêtes de la Terre.
79:3	On a répandu leur sang comme de l'eau autour de Yeroushalaim, et il n'y a eu personne pour les enterrer.
79:4	Nous sommes devenus une insulte pour nos voisins, une dérision et une moquerie pour ceux qui habitent autour de nous<!--Ps. 44:14, 80:7.-->.
79:5	Jusqu'à quand, YHWH, seras-tu en colère sans cesse et ta jalousie s'embrasera-t-elle comme un feu<!--Ps. 89:47.--> ?
79:6	Répands ton courroux sur les nations qui ne te connaissent pas et sur les royaumes qui n'invoquent pas ton Nom<!--Jé. 10:25.--> !
79:7	Car on a dévoré Yaacov, et on a ravagé ses demeures.
79:8	Ne te souviens pas contre nous des iniquités premières. Que tes compassions viennent en hâte au-devant de nous ! Car nous sommes dans une extrême détresse.
79:9	Elohîm de notre délivrance ! aide-nous pour l'amour de la gloire de ton Nom et délivre-nous ! Fais la propitiation pour nos péchés pour l'amour de ton Nom !
79:10	Pourquoi les nations diraient-elles : Où est leur Elohîm ? Que la vengeance du sang de tes serviteurs, qui a été répandu, soit manifestée parmi les nations en notre présence !
79:11	Que le gémissement des captifs parvienne jusqu'à toi ! Par ton bras puissant, sauve tes fils, ceux qui vont périr !
79:12	Et rends à nos voisins, dans leur sein, sept fois les insultes par lesquelles ils t'ont insulté, Adonaï !
79:13	Mais nous, ton peuple, le troupeau de ton pâturage, nous te louerons pour toujours, d'âge en âge nous publierons tes louanges.

## Chapitre 80

### Implorer YHWH

80:1	Au chef. Psaume d'Asaph. Sur Shoushannim-Edouth<!--Les lis.-->.
80:2	Toi qui pais Israël, prête l'oreille ! Toi qui mènes Yossef comme un troupeau, toi qui es assis entre les chérubins<!--2 S. 6:2 ; Es. 37:16 ; Ps. 99:1.-->, fais briller ta splendeur !
80:3	Réveille ta puissance au-devant d'Éphraïm, de Benyamin et de Menashè et viens pour notre délivrance !
80:4	Elohîm, ramène-nous et fais briller tes faces ! Et nous serons délivrés !
80:5	YHWH Elohîm Tsevaot, jusqu'à quand seras-tu irrité contre la prière de ton peuple ?
80:6	Tu les nourris de pain de larmes, et tu les abreuves de larmes à pleine mesure.
80:7	Tu fais de nous un sujet de dispute entre nos voisins, et nos ennemis se moquent de nous.
80:8	Elohîm Tsevaot ! ramène-nous et fais briller tes faces, et nous serons délivrés.
80:9	Tu avais retiré une vigne hors d'Égypte, tu as chassé les nations, et tu l'as plantée<!--Es. 5:1-7 ; Os. 10:1 ; Mt. 20:1, 21:28-33.-->.
80:10	Tu as préparé une place devant elle, tu lui as fait prendre racine, et elle a rempli la Terre.
80:11	Les montagnes étaient couvertes de son ombre, et ses rameaux étaient comme de hauts cèdres de El.
80:12	Elle étendait ses branches jusqu'à la mer, et ses rameaux jusqu'au fleuve.
80:13	Pourquoi as-tu rompu ses clôtures, de sorte que tous les passants sur la route cueillent ses raisins ?
80:14	Les sangliers de la forêt l'ont détruite, et toutes les bêtes des champs en font leur pâture.
80:15	Elohîm Tsevaot, reviens, s'il te plaît ! Regarde des cieux et vois, visite cette vigne,
80:16	la racine que ta droite avait plantée, le fils que tu t'es choisi.
80:17	Elle est brûlée par le feu, elle est coupée ! À la menace de tes faces, ils périssent !
80:18	Que ta main soit sur l'homme de ta droite, sur le fils d'humain que tu t'es choisi.
80:19	Et nous ne nous éloignerons plus de toi. Rends-nous la vie, et nous invoquerons ton Nom.
80:20	YHWH Elohîm Tsevaot, ramène-nous ! Fais briller tes faces et nous serons délivrés !

## Chapitre 81

### Interdiction des elohîm étrangers

81:1	Au chef. Psaume d'Asaph. Sur la Guitthith<!--Un pressoir.-->.
81:2	Chantez avec allégresse à notre Elohîm, notre force ! Poussez des cris de joie en l'honneur de l'Elohîm de Yaacov !
81:3	Sonnez du shofar, prenez le tambour, la harpe mélodieuse et le luth !
81:4	Sonnez du shofar à la nouvelle lune, à la pleine lune, au jour de notre fête<!--No. 10:10.--> !
81:5	Car c'est une loi pour Israël, une ordonnance de l'Elohîm de Yaacov,
81:6	un témoignage qu’il mit en Yehossef<!--Yossef.--> quand il sortit contre la terre d'Égypte, où j’entendais un langage que je ne connaissais pas.
81:7	J'ai déchargé son épaule du fardeau, et ses paumes ont lâché les corbeilles.
81:8	Tu as crié dans la détresse, et je t'ai sauvé. Je t'ai répondu dans le lieu caché du tonnerre, je t'ai éprouvé auprès des eaux de Meriybah. Sélah.
81:9	Écoute, mon peuple ! Je te relèverai. Israël, si tu m'écoutais !
81:10	Il n'y aura pas au milieu de toi de el étranger, et tu ne te prosterneras pas devant le el des étrangers.
81:11	Je suis YHWH, ton Elohîm, qui t'ai fait monter hors de la terre d'Égypte. Ouvre ta bouche et je la remplirai.
81:12	Mais mon peuple n'a pas écouté ma voix, et Israël ne m'a pas obéi.
81:13	C'est pourquoi je les ai abandonnés à la dureté<!--Mt. 19:8 ; Mc. 10:5.--> de leur cœur, et ils ont suivi leurs propres conseils<!--Es. 63:17, 65:2 ; Ro. 1:24-26 ; 2 Pi. 3:3.-->.
81:14	Si mon peuple m'écoutait ! Si Israël marchait dans mes voies !
81:15	J'abattrais en un instant leurs ennemis et je tournerais ma main contre leurs adversaires.
81:16	Ceux qui haïssent YHWH le flatteraient, et le bonheur de mon peuple durerait toujours.
81:17	Il le nourrirait du meilleur froment, et je le rassasierais du miel du rocher.

## Chapitre 82

### Elohîm dénonce l'injustice des hommes

82:1	Psaume d'Asaph. Elohîm se tient dans l'assemblée de El, il juge au milieu des elohîm<!--Ou des juges.-->.
82:2	Jusqu'à quand jugerez-vous avec injustice et porterez-vous les faces des méchants<!--Ps. 58:2.--> ? Sélah.
82:3	Défendez le faible et l'orphelin, rendez justice à l'affligé et au pauvre,
82:4	délivrez le faible et l’indigent, sauvez-les de la main des méchants.
82:5	Ils ne connaissent ni n'entendent rien, ils marchent dans l'obscurité, tous les fondements de la Terre sont ébranlés.
82:6	J'ai dit : Vous êtes des elohîm<!--Jn. 10:34.-->, et vous êtes tous fils d'Élyon<!--Très Haut.-->.
82:7	Toutefois, vous mourrez comme les humains<!--Ou Adam.-->, et vous les princes vous tomberez comme les autres.
82:8	Elohîm, lève-toi, juge la Terre, car tu auras en héritage toutes les nations<!--Ps. 2:8 ; Hé. 1:2.--> !

## Chapitre 83

### Dessein et confusion des ennemis d'Israël

83:1	Cantique. Psaume d'Asaph.
83:2	Elohîm ! ne garde pas le silence, ne te tais pas, et ne te tiens pas en repos, El<!--Ps. 35:22.--> !
83:3	Car voici, tes ennemis s'agitent, et ceux qui te haïssent lèvent la tête.
83:4	En secret, ils prennent des conseils rusés contre ton peuple, ils se consultent ensemble contre ceux qui se retirent vers toi pour se cacher<!--Ps. 2:2.-->.
83:5	Venez, disent-ils, détruisons-les, de sorte qu'ils ne soient plus une nation, et qu'on ne fasse plus mention du nom d'Israël<!--Ce passage fait allusion aux désirs qu'ont certaines nations de voir Israël détruite. Mi. 4:11 ; Ap. 11:1-2.--> !
83:6	Car ils se consultent ensemble d'un même cœur, ils font alliance contre toi.
83:7	Les tentes d'Édom et des Yishmaélites, des Moabites et des Hagaréniens,
83:8	de Guebal, d'Ammon, d'Amalek, les Philistins avec les habitants de Tyr.
83:9	L'Assyrie aussi se joint à eux, elle sert de bras aux fils de Lot. Sélah.
83:10	Traite-les comme Madian<!--Jg. 7:15.-->, comme Sisera<!--Jg. 4:15.-->, et comme Yabiyn, au torrent de Kison !
83:11	Ils ont été détruits à En-Dor, ils sont devenus du fumier pour le sol.
83:12	Que leurs chefs soient traités comme Oreb et comme Zeeb, et que tous leurs princes soient comme Zébach et Tsalmounna<!--Jg. 7:25.--> !
83:13	Parce qu'ils disent : Prenons possession des pâturages d'Elohîm !
83:14	Mon Elohîm ! rends-les semblables au tourbillon et au chaume chassé par le vent,
83:15	comme le feu brûle une forêt, et comme la flamme embrase les montagnes.
83:16	Poursuis-les ainsi par ta tempête et terrifie-les par ton vent d'orage !
83:17	Couvre leurs visages d'ignominie afin qu'on cherche ton Nom, YHWH !
83:18	Qu'ils soient honteux et terrifiés pour toujours, qu'ils soient confondu et qu'ils périssent !
83:19	Afin qu'on sache que toi seul, dont le Nom est YHWH, tu es Élyon<!--Très-Haut.--> sur toute la Terre.

## Chapitre 84

### Délices pour ceux qui ont YHWH comme appui

84:1	Au chef. Psaume des fils de Koré. Sur la Guitthith<!--Un pressoir.-->.
84:2	YHWH Tsevaot, que tes tabernacles sont aimables !
84:3	Mon âme soupire et languit après les parvis de YHWH, mon cœur et ma chair poussent des cris de joie vers le El Haï<!--El vivant.-->.
84:4	Le passereau même trouve sa maison, et l'hirondelle son nid où elle a mis ses petits... Tes autels, YHWH Tsevaot ! Mon Roi et mon Elohîm !
84:5	Heureux ceux qui habitent ta maison, et qui te louent sans cesse ! Sélah.
84:6	Heureux l'homme dont la force est en toi, et ceux dans le cœur desquels sont les chemins tout tracés !
84:7	En passant par la vallée de Baca, ils en font une source, et la pluie la couvre de bénédictions.
84:8	Ils marchent avec force pour se présenter devant Elohîm à Sion.
84:9	YHWH Elohîm Tsevaot, écoute ma prière, Elohîm de Yaacov, prête l'oreille. Sélah.
84:10	Elohîm, notre bouclier, vois et regarde les faces de ton mashiah !
84:11	Oui, un jour dans tes parvis vaut mieux que mille. J’ai choisi d’être au seuil de la maison de mon Elohîm, plutôt que de demeurer sous les tentes de la méchanceté.
84:12	Car YHWH Elohîm est un soleil<!--YHWH, notre soleil, se révèle en la personne de Yéhoshoua. Voir Lu. 1:78.--> et un bouclier<!--Ge. 15:1 ; Ps. 89:19, 144:2.-->. YHWH donne la grâce et la gloire, et il ne refuse pas le bonheur à ceux qui marchent dans l'intégrité.
84:13	YHWH Tsevaot, heureux l'homme qui se confie en toi<!--Ps. 2:12.--> !

## Chapitre 85

### Supplication des rescapés de l'exil

85:1	Au chef. Psaume des fils de Koré.
85:2	YHWH, tu as été favorable à ta terre ! Tu as fait revenir les captifs de Yaacov.
85:3	Tu as pardonné l'iniquité de ton peuple, tu as couvert tous leurs péchés. Sélah.
85:4	Tu as retiré toute ta colère, tu es revenu de l'ardeur de ton indignation.
85:5	Elohîm de notre délivrance, rétablis-nous et fais cesser la colère que tu as contre nous.
85:6	Seras-tu irrité à jamais contre nous ? Feras-tu durer ta colère d'âge en âge ?
85:7	Ne reviendras-tu pas nous rendre la vie<!--Ps. 71:20.-->, afin que ton peuple se réjouisse en toi ?
85:8	YHWH, fais-nous voir ta miséricorde et accorde-nous ta délivrance !
85:9	J'écouterai ce que dira El, YHWH, car il parlera de paix à son peuple et à ses bien-aimés, pour qu’ils ne retournent pas à la folie. 
85:10	Certainement, sa délivrance est proche de ceux qui le craignent, la gloire habite sur notre terre.
85:11	La grâce et la vérité<!--Jn. 1:17.--> se rencontrent, la justice et la paix s'embrassent<!--Hé. 7:2.-->.
85:12	La vérité germe de la Terre, et la justice regarde des cieux.
85:13	YHWH donnera ce qui est bon, et notre terre rendra son fruit.
85:14	La justice marchera devant lui, et il la mettra sur le chemin où il passera.

## Chapitre 86

### Cœur disposé à la crainte d'Elohîm

86:1	Prière de David. YHWH, écoute, réponds-moi ! Car je suis affligé et misérable.
86:2	Garde mon âme, car je suis fidèle ! Toi, mon Elohîm, délivre ton serviteur qui se confie en toi !
86:3	Adonaï, aie pitié de moi, car je crie à toi tout le jour.
86:4	Réjouis l'âme de ton serviteur, car j'élève mon âme à toi, Adonaï.
86:5	Oui, toi, Adonaï, tu es bon et prêt à pardonner, et grand en bonté envers tous ceux qui t'invoquent<!--Joë. 2:13.-->.
86:6	YHWH, prête l'oreille à ma prière, et sois attentif à la voix de mes supplications !
86:7	Je t'invoque au jour de ma détresse, car tu me réponds<!--Ps. 50:15.-->.
86:8	Adonaï, nul n'est comme toi parmi les elohîm, et rien ne ressemble à tes œuvres<!--De. 3:24 ; Ps. 95:3.-->.
86:9	Adonaï, toutes les nations que tu as faites viendront et se prosterneront devant toi, et glorifieront ton Nom.
86:10	Car tu es grand, et tu fais des choses merveilleuses. Tu es Elohîm, toi seul.
86:11	YHWH, enseigne-moi tes voies et je marcherai dans ta vérité<!--Ps. 25:4, 27:11.-->. Unifie mon cœur pour que je craigne ton Nom !
86:12	Adonaï, mon Elohîm, je te célébrerai de tout mon cœur, et je glorifierai ton Nom pour toujours !
86:13	Car ta bonté est grande envers moi, et tu as délivré mon âme du shéol, de ses parties inférieures.
86:14	Elohîm, des gens orgueilleux se sont élevés contre moi, et une troupe de terrifiants en veut à mon âme. Ils ne t’ont pas placé devant eux.
86:15	Mais toi, Adonaï, tu es le El compatissant, miséricordieux, lent à la colère, grand en bonté et en vérité.
86:16	Tourne-toi vers moi, et aie pitié de moi ! Donne ta force à ton serviteur, délivre le fils de ta servante !
86:17	Accorde-moi un signe de ta faveur, et que ceux qui me haïssent le voient et soient honteux ! Parce que tu m'aideras, YHWH ! Tu me consoleras !

## Chapitre 87

### Sion, la cité d'Elohîm

87:1	Psaume. Cantique des fils de Koré. Sa fondation est sur les montagnes saintes.
87:2	YHWH aime les portes de Sion plus que tous les tabernacles de Yaacov.
87:3	Ce qui se dit de toi, cité d'Elohîm, sont des choses glorieuses ! Sélah.
87:4	Je ferai mention de Rahab et de Babel<!--Babylone.--> parmi ceux qui me connaissent ; voici la terre des Philistins, et Tyr avec l'Éthiopie : c'est dans Sion qu'ils sont nés.
87:5	Et de Sion il est dit : Un homme, un homme y est né, Élyon lui-même l'établira.
87:6	Quand YHWH enregistrera les peuples, il écrira : C'est là qu'ils sont nés. Sélah.
87:7	Et les chanteurs, de même que les joueurs de flûte s'écrient : Toutes mes sources sont en toi.

## Chapitre 88

### Lamentation dans l'affliction

88:1	Cantique. Psaume des fils de Koré. Au chef, sur Machalath<!--Trouvé dans l'en-tête des PS 53 et PS 81. Sens douteux, probablement un mot clé dans un chant, qui donne le ton.-->, pour s'humilier. Poésie d'Héman, l'Ezrachite.
88:2	YHWH, Elohîm de mon salut ! je crie jour et nuit devant toi<!--Lu. 18:7.-->.
88:3	Que ma prière parvienne en ta présence ! Étends ton oreille à mon cri !
88:4	Car mon âme est rassasiée de maux, et ma vie atteint le shéol<!--Lu. 16:23.-->.
88:5	On m'a mis au rang de ceux qui descendent dans la fosse<!--Ps. 28:1, 31:13.-->. Je suis devenu comme un homme fort qui n'a plus de vigueur.
88:6	Je suis étendu parmi les morts, semblable à ceux qui sont tués et couchés dans la tombe, à ceux dont tu n'as plus le souvenir, et qui sont séparés par ta main.
88:7	Tu m'as mis dans la fosse inférieure, dans les ténèbres, dans les profondeurs.
88:8	Ta fureur se pose sur moi, et tu m'as accablé de tous tes flots. Sélah.
88:9	Tu as éloigné de moi ceux de qui j'étais connu, tu m'as mis en abomination devant eux ; je suis enfermé et je ne peux sortir.
88:10	Mes yeux se consument dans la souffrance. YHWH ! Je crie à toi tout le jour ! J'étends mes paumes vers toi<!--Ex. 9:29 ; 1 R. 8:22 ; Job 17:7.--> !
88:11	Est-ce pour les morts que tu fais des miracles ? Les fantômes<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».--> se relèveront-ils pour te célébrer<!--1 Co. 15:12-13 ; 1 Th. 4:16.--> ? Sélah.
88:12	Parle-t-on de ta bonté dans le sépulcre, de ta fidélité dans le tombeau<!--Ep. 4:9-10 ; 1 Pi. 3:18-20.--> ?
88:13	Connaîtra-t-on tes merveilles dans les ténèbres, et ta justice dans la terre de l'oubli ?
88:14	Mais moi, YHWH, j'implore ton secours, dès le matin, ma prière va au-devant de toi.
88:15	YHWH, pourquoi rejettes-tu mon âme ? Pourquoi me caches-tu tes faces<!--Mt. 27:46 ; Mc. 15:34.--> ?
88:16	Je suis malheureux et moribond dès ma jeunesse, j'ai été exposé à tes terreurs, et je ne sais pas où j'en suis.
88:17	Tes colères ont passé sur moi, et tes terreurs m’ont anéanti<!--Es. 53:5.-->.
88:18	Elles m'environnent tout le jour comme des eaux, elles m'enveloppent toutes à la fois.
88:19	Tu as éloigné de moi mon ami intime et mon compagnon ; mes connaissances ont disparu<!--Mt. 26:56.-->.

## Chapitre 89

### Béni est le peuple qui connaît le son de la trompette

89:1	Poésie d'Éthan, l'Ezrachite.
89:2	Je chanterai toujours les bontés de YHWH, je ferai connaître de ma bouche ta fidélité d'âge en âge.
89:3	Car je dis : Ta bonté a des fondements éternels ; tu établis ta fidélité dans les cieux.
89:4	J'ai traité alliance avec mon élu, j'ai fait serment à David, mon serviteur :
89:5	j'affermirai ta postérité pour toujours, et j'établirai ton trône d'âge en âge<!--2 S. 7:8-16.-->. Sélah.
89:6	Les cieux célèbrent tes merveilles, YHWH ! Ta fidélité aussi est célébrée dans l'assemblée des saints.
89:7	Car qui, dans le nuage, peut se comparer à YHWH ? Qui est semblable à YHWH parmi les fils de El ?
89:8	El fait trembler dans la grande assemblée des saints, il est plus redoutable que tous ceux qui sont autour de lui.
89:9	Yah, Elohîm Tsevaot ! qui est semblable à toi, puissant YHWH ? Aussi ta fidélité t'environne.
89:10	Tu domines l'élévation des flots de la mer ; quand ses vagues s'élèvent, tu les calmes<!--Job 26:12, 38:8-12.-->.
89:11	Tu écrasas Rahab<!--Ce terme hébreu fait référence au nom emblématique de l'Égypte et signifie « largeur », « arrogance ».--> comme un homme blessé à mort ; tu dispersas tes ennemis par le bras de ta force.
89:12	À toi sont les cieux, à toi aussi est la Terre. Tu as fondé le monde, et tout ce qui est en lui.
89:13	Tu as créé le nord et le sud, le Thabor et l'Hermon se réjouissent en ton Nom.
89:14	Ton bras est puissant, ta main est forte, ta droite est haut élevée.
89:15	La justice et l'équité sont la base de ton trône ; la bonté et la vérité marchent devant ta face.
89:16	Heureux le peuple qui connaît le son de la trompette<!--1 Co. 15:52 ; Ap. 10:7.--> ! Il marche, YHWH ! à la lumière de tes faces !
89:17	Il se réjouit tout le jour en ton Nom, et il se glorifie de ta justice.
89:18	Parce que tu es la gloire de leur puissance, c'est par ta faveur que s'élève notre corne.
89:19	Car notre bouclier est YHWH, et notre Roi est le Saint d'Israël.
89:20	Tu as autrefois parlé en vision concernant ton fidèle, et tu as dit : J'ai ordonné mon secours en faveur d'un homme vaillant ; j'ai élevé l'élu du milieu du peuple.
89:21	J'ai trouvé David, mon serviteur, je l'ai oint de ma sainte huile<!--1 S. 16:13 ; Ac. 13:22.-->.
89:22	Ma main sera ferme avec lui et mon bras le rendra fort.
89:23	L'ennemi ne le surprendra pas, et le fils de l'injustice ne l'affligera pas ;
89:24	mais j'écraserai devant lui ses adversaires, et je détruirai ceux qui le haïssent.
89:25	Ma fidélité et ma bonté seront avec lui, et sa corne sera élevée en mon Nom.
89:26	Je mettrai sa main sur la mer, et sa droite sur les fleuves.
89:27	Il m'invoquera : Tu es mon Père, mon El, le Rocher<!--Voir commentaire en Es. 8:14.--> de mon salut.
89:28	Et moi, je ferai de lui le premier-né<!--Col. 1:15.-->, le plus élevé des rois de la Terre.
89:29	Je lui garderai ma bonté pour toujours, et mon alliance lui sera assurée.
89:30	Je rendrai éternelle sa postérité, et son trône comme les jours des cieux.
89:31	Mais si ses fils abandonnent ma torah et ne marchent pas dans mes statuts,
89:32	s'ils violent mes statuts et qu'ils ne gardent pas mes commandements,
89:33	je punirai de la verge leur transgression, et de plaie leur iniquité.
89:34	Mais je ne retirerai pas de lui ma bonté, et je ne tromperai pas ma fidélité.
89:35	Je ne violerai pas mon alliance, et je ne changerai pas ce qui est sorti de mes lèvres.
89:36	J'ai une fois juré par ma sainteté : mentirai-je à David<!--Hé. 6:13.--> ?
89:37	Sa postérité sera pour toujours, et son trône sera devant moi comme le soleil,
89:38	il aura une durée éternelle comme la lune. Le témoin qui est dans le nuage est fidèle. Sélah.
89:39	Néanmoins, tu l'as rejeté et dédaigné<!--Es. 53:3.--> ! Tu t'es mis en grande colère contre ton mashiah !
89:40	Tu as rejeté l'alliance faite avec ton serviteur, tu as souillé sa couronne par terre.
89:41	Tu as fait des brèches à toutes ses clôtures, tu as mis en ruine ses forteresses.
89:42	Tous ceux qui passaient par le chemin l'ont pillé ; il est devenu une insulte pour ses voisins.
89:43	Tu as élevé la droite de ses adversaires, tu as réjoui tous ses ennemis.
89:44	Tu as fait reculer le tranchant de son épée, et tu ne l'as pas élevé dans le combat.
89:45	Tu as fait cesser sa splendeur, et tu as jeté par terre son trône.
89:46	Tu as abrégé les jours de sa jeunesse, et l'as couvert de honte. Sélah.
89:47	Jusqu'à quand, YHWH ? Te cacheras-tu à jamais ? Ta fureur s'embrasera-t-elle comme un feu ?
89:48	Souviens-toi quelle est la durée de ma vie. Pourquoi aurais-tu créé en vain tous les fils d'humains ?
89:49	Qui est l'homme fort qui vivra sans voir la mort ? Qui peut délivrer son âme de la main du shéol<!--1 Co. 15:54-57.--> ? Sélah.
89:50	Adonaï, où sont tes bontés premières que tu juras à David dans ta fidélité ?
89:51	Adonaï, souviens-toi de tes serviteurs qu'on insulte, je porte sur mon sein tous ces grands peuples.
89:52	Souviens-toi des outrages de tes ennemis, YHWH ! des outrages contre les pas de ton mashiah.
89:53	Béni soit pour toujours YHWH ! Amen ! Oui, amen !

## Chapitre 90

### Mortalité de l'homme

90:1	Prière de Moshé, homme d'Elohîm<!--De. 33:1.-->. Adonaï ! Tu as été pour nous un refuge d'âge en âge.
90:2	Avant que les montagnes soient nées et que tu aies formé la Terre et le monde, d'éternité en éternité, tu es El<!--Ge. 17:1 ; Es. 40:28.-->.
90:3	Tu fais retourner le mortel à la poussière, et tu dis : Retournez, fils de l’humain<!--Ge. 3:19 ; Ec. 12:5.--> !
90:4	Car mille ans sont, à tes yeux, comme le jour d'hier qui est passé, et comme une veille de la nuit<!--Ps. 39:5 ; 2 Pi. 3:8.-->.
90:5	Tu les emportes semblables à un sommeil qui, le matin, passe comme l'herbe :
90:6	elle fleurit au matin et reverdit ; le soir on la coupe, et elle se fane<!--1 Pi. 1:24.-->.
90:7	Car nous sommes consumés par ta colère, et nous sommes terrifiés par ta fureur.
90:8	Tu as mis devant toi nos iniquités, et à la lumière de tes faces nos fautes cachées.
90:9	Car tous nos jours s'en vont par ta grande colère, et nos années se consument dans un soupir.
90:10	Les jours de nos années s'élèvent à 70 ans, et pour les plus forts, à 80 ans, et l'orgueil qu'ils en tirent n'est que peine et misère, car le temps passe vite et nous nous envolons.
90:11	Qui connaît, selon ta crainte, la force de ton indignation et de ta grande colère ?
90:12	Enseigne-nous à compter nos jours, afin que notre cœur vienne à la sagesse !
90:13	YHWH, reviens ! Jusqu'à quand ? Sois apaisé envers tes serviteurs !
90:14	Rassasie-nous, au matin, de ta bonté, afin que nous nous réjouissions et que nous soyons joyeux tout le long de nos jours.
90:15	Réjouis-nous autant de jours que tu nous as affligés, autant d'années que nous avons vu le malheur.
90:16	Que ton œuvre se voie sur tes serviteurs, et ta gloire sur leurs fils !
90:17	Que la grâce d'Adonaï, notre Elohîm, soit sur nous ! Et affermis l'œuvre de nos mains, oui, affermis l'œuvre de nos mains !

## Chapitre 91

### La sécurité et la fidélité de YHWH

91:1	Celui qui demeure sous la couverture<!--Elohîm seul est notre couverture spirituelle. Voir Ps. 18:12, 27:5, 31:21, 32:7, 61:4-5.--> d'Élyon, se loge à l'ombre de Shaddaï.
91:2	Je dis à YHWH : Tu es mon refuge et ma forteresse, tu es mon Elohîm en qui je me confie !
91:3	Car il te délivrera du piège de l'oiseleur, de la peste et de la calamité.
91:4	Il te couvrira de ses plumes, et tu trouveras un refuge sous ses ailes. Sa fidélité est un bouclier et une cuirasse.
91:5	Tu ne craindras ni les terreurs de la nuit, ni la flèche qui vole le jour<!--Pr. 3:23-24.-->,
91:6	ni la peste qui marche dans les ténèbres, ni la destruction qui dévaste en plein midi.
91:7	Que 1 000 tombent à ton côté, et 10 000 à ta droite, elle n'approchera pas de toi.
91:8	De tes yeux tu regarderas, et tu verras la rétribution des méchants.
91:9	Car tu es mon refuge, YHWH ! Tu fais d'Élyon ta demeure.
91:10	Aucun malheur ne s'approchera de toi, aucun fléau n'approchera de ta tente<!--Ex. 8:14-15 ; Ps. 121:6-8.-->.
91:11	Car il ordonnera à ses anges de te garder dans toutes tes voies.
91:12	Ils te porteront sur les paumes, de peur que ton pied ne heurte contre une pierre<!--Mt. 4:5-6 ; Lu. 4:9-11.-->.
91:13	Tu marcheras sur le lion et sur le cobra, tu piétineras le lionceau et le dragon.
91:14	Puisqu'il s'est attaché à moi, je le délivrerai, je le mettrai sur les hauteurs, parce qu'il connaît mon Nom.
91:15	Il m'invoquera et je lui répondrai. Je serai moi-même avec lui dans la détresse, je le délivrerai et le glorifierai.
91:16	Je le rassasierai de jours, et je lui ferai voir mon salut<!--Yeshuw`ah.-->.

## Chapitre 92

### Proclamer la louange d'Elohîm

92:1	Psaume. Cantique pour le jour du shabbat.
92:2	C'est une belle chose que de célébrer YHWH, et de chanter ton Nom, Élyon<!--Ps. 147:1.--> !
92:3	Afin d'annoncer chaque matin ta bonté, et ta fidélité toutes les nuits<!--Ps. 59:17, 88:14, 89:2.-->,
92:4	sur l'instrument à dix cordes et le luth, avec une méditation de harpe.
92:5	Car YHWH, tu me réjouis par tes œuvres, je me réjouis des œuvres de tes mains !
92:6	YHWH, que tes œuvres sont magnifiques ! Tes pensées sont merveilleusement profondes<!--Es. 55:8-9 ; Job 5:9.-->.
92:7	L'homme abruti n'y connaît rien, et le fou n'y prend pas garde<!--Es. 5:12 ; Ro. 1:21.-->.
92:8	Les méchants poussent comme l'herbe, et tous ceux qui pratiquent la méchanceté fleurissent pour être exterminés éternellement<!--Jé. 12:1-2 ; Mal. 3:15 ; Ps. 37:2, 73:1-20.-->.
92:9	Mais toi, YHWH, tu es élevé pour toujours !
92:10	Car voici, tes ennemis, YHWH ! Car voici, tes ennemis périssent : tous ceux qui pratiquent la méchanceté sont dispersés.
92:11	Mais tu élèves ma corne comme celle d'un taureau sauvage, je suis mêlé à l'huile fraîche<!--Ps. 23:5 ; Hé. 1:9.-->.
92:12	Mes yeux se plaisent à regarder ceux qui m'épient, et mes oreilles à entendre les méchants qui s'élèvent contre moi.
92:13	Le juste fleurit comme le palmier, il croît comme le cèdre au Liban.
92:14	Étant plantés dans la maison de YHWH, ils fleurissent dans les parvis de notre Elohîm.
92:15	Ils portent encore des fruits à l'âge des cheveux gris, ils sont gras et verdoyants<!--Os. 14:7 ; Ps. 1:3.-->,
92:16	afin d'annoncer que YHWH est droit. C'est mon Rocher, et il n'y a pas d'injustice en lui.

## Chapitre 93

### Majesté et puissance de YHWH

93:1	YHWH règne, il est revêtu de majesté, YHWH est revêtu de force, il s'en est ceint. Aussi le monde est tellement ferme, qu'il ne sera pas ébranlé.
93:2	Ton trône est établi dès lors, tu es dès l’éternité<!--Ps. 9:8 ; Hé. 1:8.-->.
93:3	Les fleuves ont élevé, YHWH ! Les fleuves augmentent leur bruit, les fleuves élèvent leurs flots<!--Ps. 46:4, 65:7-8.-->.
93:4	YHWH, qui est dans les lieux élevés, est plus puissant que le bruit des grandes eaux, et que les fortes vagues de la mer<!--Es. 57:15 ; Ac. 7:49.-->.
93:5	Tes témoignages sont entièrement fidèles. YHWH ! La sainteté orne ta maison pour de longs jours.

## Chapitre 94

### À El seul la vengeance

94:1	El des vengeances, YHWH, El des vengeances, fais briller ta splendeur !
94:2	Toi, juge de la Terre, lève-toi ! Rends aux orgueilleux selon leurs œuvres !
94:3	Jusqu'à quand les méchants, YHWH, jusqu'à quand les méchants se réjouiront-ils ?
94:4	Jusqu'à quand tous ceux qui pratiquent la méchanceté discourront-ils, et diront-ils des paroles rudes et se vanteront-ils ?
94:5	YHWH ! Ils écrasent ton peuple, et affligent ton héritage.
94:6	Ils tuent la veuve et l'étranger, et ils mettent à mort les orphelins.
94:7	Ils disent : Yah ne le voit pas, l'Elohîm de Yaacov n'entend rien !
94:8	Vous les plus abrutis d'entre les peuples, prenez garde à ceci ! Et vous insensés, quand serez-vous intelligents ?
94:9	Celui qui a planté l'oreille, n'entendrait-il pas ? Celui qui a formé l'œil, ne verrait-il pas<!--Ex. 4:11 ; Pr. 20:12.--> ?
94:10	Celui qui châtie les nations ne punirait-il pas, lui qui apprend la connaissance aux humains<!--Ap. 19:15.--> ?
94:11	YHWH sait que les pensées des humains sont une vapeur.
94:12	Heureux l'homme fort que tu châties, Yah<!--Hé. 12:6.--> ! que tu instruis par ta torah,
94:13	afin qu'il soit dans la paix aux jours du malheur, jusqu'à ce que la fosse soit creusée pour le méchant !
94:14	Car YHWH ne délaisse pas son peuple, et n'abandonne pas son héritage<!--Es. 49:15 ; Ro. 11:2.-->.
94:15	C'est pourquoi le jugement s'unira à la justice, et tous ceux qui sont droits de cœur le suivront.
94:16	Qui se lèvera pour moi contre les méchants<!--Job 19:25 ; Ro. 8:31.--> ? Qui m'assistera contre ceux qui pratiquent la méchanceté ?
94:17	Si YHWH n'était pas mon secours, mon âme serait bien vite dans la demeure du silence.
94:18	Quand je dis : Mon pied chancelle ! Ta bonté me soutient, YHWH !
94:19	Quand j'ai beaucoup de pensées au-dedans de moi, tes consolations font les délices de mon âme.
94:20	Serais-tu l'allié du trône de méchanceté, qui forge des injustices contre les règles de la justice ?
94:21	Ils se rassemblent contre l'âme du juste, et condamnent le sang innocent<!--Mt. 27:1-4,24.-->.
94:22	Or YHWH est pour moi une haute retraite, mon Elohîm est le Rocher de mon refuge.
94:23	Il fera retourner sur eux leur iniquité, et les détruira par leur propre méchanceté. YHWH, notre Elohîm, les détruira<!--Mt. 13:30 ; Ap. 20:14-15.-->.

## Chapitre 95

### Adoration à YHWH

95:1	Venez, chantons à YHWH ! Poussons des cris de réjouissance au Rocher de notre salut.
95:2	Approchons de ses faces avec des louanges, poussons vers lui des cris de joie avec des chants !
95:3	Oui, YHWH est le Grand El, il est le Grand Roi au-dessus de tous les elohîm.
95:4	Les lieux les plus profonds de la Terre sont dans sa main, et les sommets des montagnes sont à lui.
95:5	C'est à lui qu'appartient la mer, car lui-même l'a faite, et ses mains ont formé la Terre.
95:6	Venez, prosternons-nous, inclinons-nous, et mettons-nous à genoux devant YHWH qui nous a faits<!--Ps. 96:9 ; Ph. 2:10-11.--> !
95:7	Oui, il est notre Elohîm. Nous sommes le peuple de son pâturage et les brebis que sa main conduit<!--Ps. 23:1, 100:3 ; Jn. 10:11.-->. Si vous entendez aujourd'hui sa voix,
95:8	n'endurcissez pas votre cœur<!--Hé. 3:8, 4:7.-->, comme à Meriybah, comme au jour de Massah<!--Voir Ex. 17:7.--> dans le désert,
95:9	là où vos pères m'ont tenté et éprouvé bien qu'ils virent mes œuvres<!--Ex. 17:7.-->.
95:10	J'ai eu cette génération en dégoût durant 40 ans et j'ai dit : C'est un peuple dont le cœur s'égare, ils n'ont pas connu mes voies.
95:11	C'est pourquoi j'ai juré dans ma colère : ils n'entreront pas dans mon repos<!--No. 14:22-23 ; Hé. 3:15-19, 4:3.--> !

## Chapitre 96

### La grandeur et la gloire d'Elohîm

96:1	Chantez à YHWH un cantique nouveau<!--Es. 42:10 ; Ps. 98:1 ; Ap. 5:9, 14:3.--> ! Vous tous habitants de la Terre chantez à YHWH !
96:2	Chantez à YHWH, bénissez son Nom ! Prêchez de jour en jour son salut !
96:3	Racontez sa gloire parmi les nations, ses merveilles parmi tous les peuples<!--Ps. 67:5.--> !
96:4	Oui, YHWH est grand et digne d'être loué, il est redoutable au-dessus de tous les elohîm<!--Ph. 2:9 ; Ap. 5:9.-->.
96:5	Oui, tous les elohîm des peuples sont des faux elohîm, mais YHWH a fait les cieux.
96:6	La splendeur et la magnificence sont devant ses faces, la force et la beauté sont dans son sanctuaire.
96:7	Familles des peuples, rendez à YHWH, rendez à YHWH la gloire et la puissance !
96:8	Donnez à YHWH la gloire de son Nom ! Apportez des offrandes, entrez dans ses parvis !
96:9	Prosternez-vous devant YHWH avec des ornements sacrés. Tremblez devant lui, vous toute la Terre !
96:10	Dites parmi les nations : YHWH est roi. Oui, le monde est affermi, il ne sera pas ébranlé. Il jugera les peuples avec équité.
96:11	Que les cieux se réjouissent, et que la Terre soit dans l'allégresse ! Que la mer tonne avec tout ce qui la remplit !
96:12	Que les champs s'égayent avec tout ce qui est en eux ! Alors tous les arbres de la forêt chanteront de joie,
96:13	devant YHWH. Car il vient, car il vient pour juger<!--Voir Ps. 98 et Jud. 1:14.--> la Terre. Il jugera avec justice le monde, et les peuples selon sa fidélité.

## Chapitre 97

### Aimer Elohîm, c'est haïr le mal

97:1	YHWH règne ! Que la Terre soit dans l'allégresse, et que les îles nombreuses s'en réjouissent<!--Es. 42:10 ; Ps. 86:9, 93:1, 99:1.--> !
97:2	La nuée et la profonde obscurité sont autour de lui, la justice et le jugement sont la base de son trône.
97:3	Le feu marche devant lui, et embrase tout autour ses adversaires.
97:4	Ses éclairs illuminent le monde, et la Terre le voit et tremble tout étonnée<!--Job 38:35 ; Ap. 4:5.-->.
97:5	Les montagnes se fondent comme de la cire<!--Mi. 1:4.-->, à cause de la présence de YHWH, à cause de la présence du Seigneur de toute la Terre.
97:6	Les cieux annoncent sa justice, et tous les peuples voient sa gloire.
97:7	Que tous ceux qui servent les idoles, et qui se glorifient des faux elohîm soient confus<!--De. 4:25-26 ; 1 S. 5:1-5.--> ! Vous, tous les elohîm, prosternez-vous devant lui !
97:8	Sion l'a entendu, et s'en est réjouie, les filles de Yéhouda se sont égayées pour l'amour de tes jugements, YHWH !
97:9	Car toi, YHWH, tu es Élyon<!--Très-Haut.--> sur toute la Terre, tu es infiniment élevé au-dessus de tous les elohîm.
97:10	Vous qui aimez YHWH, haïssez le mal<!--Am. 5:14-15 ; Ro. 12:9.--> ! Il garde les âmes de ses bien-aimés, et les délivre de la main des méchants<!--Ps. 34:8 ; Jn. 10:28-29.-->.
97:11	La lumière est semée pour le juste<!--Mt. 5:15-16.-->, et la joie pour ceux dont le cœur est droit.
97:12	Justes, réjouissez-vous en YHWH, et célébrez la mémoire de sa sainteté !

## Chapitre 98

### Invitation à la louange

98:1	Psaume. Chantez à YHWH un cantique nouveau ! Car il a fait des choses merveilleuses. Sa droite et le bras de sa sainteté l'ont délivré<!--Es. 52:10, 53:1, 63:3-5.-->.
98:2	YHWH a fait connaître son salut<!--Il est question de la révélation de Yéhoshoua. Voir commentaire en Es. 26:1.-->, il a révélé sa justice devant les yeux des nations.
98:3	Il s'est souvenu de sa bonté et de sa fidélité envers la maison d'Israël. Toutes les extrémités de la Terre ont vu le salut<!--La translitération du mot hébreu est « Yeshuw`ah ». Voir commentaire en Ge. 49:18.--> de notre Elohîm<!--Es. 49:6 ; Lu. 1:72 ; Ac. 13:47.-->.
98:4	Vous tous, habitants de la Terre, poussez des cris de réjouissance à YHWH ! Faites retentir vos cris, et chantez de joie !
98:5	Chantez à YHWH avec la harpe, avec la harpe et avec une voix mélodieuse !
98:6	Poussez des cris de réjouissance avec le shofar et le son du cor devant le Roi, YHWH !
98:7	Que la mer tonne avec tout ce qu'elle contient, que la Terre et ceux qui y habitent fassent éclater leurs cris !
98:8	Que les fleuves frappent des paumes, et que les montagnes chantent de joie
98:9	devant YHWH ! Car il vient pour juger la Terre<!--YHWH, qui vient pour juger la Terre, est Yéhoshoua ha Mashiah (Ps. 96 ; Za. 14:1-7 ; 2 Ti. 4:1 ; Jud. 1:14 ; Ap. 19:15).-->. Il jugera le monde avec justice, et les peuples avec équité.

## Chapitre 99

### Grandeur, justice, et sainteté d'Elohîm

99:1	YHWH règne : que les peuples tremblent ! Il est assis entre les chérubins : que la Terre soit ébranlée<!--Ex. 25:22 ; Es. 37:16.--> !
99:2	YHWH est grand en Sion, et il est élevé au-dessus de tous les peuples.
99:3	On louera ton Nom grand et redoutable. Il est saint !
99:4	Et la force du Roi qui aime la justice. Tu as ordonné l'équité, tu as prononcé des jugements justes en Yaacov.
99:5	Exaltez YHWH, notre Elohîm, et prosternez-vous devant le marchepied de ses pieds ! Il est saint !
99:6	Moshé et Aaron étaient parmi ses prêtres<!--Ex. 31:10 ; Lé. 2:2.-->, et Shemouél parmi ceux qui invoquaient son Nom, ils invoquaient YHWH et il leur répondait<!--1 S. 12:18-19.-->.
99:7	Il leur parlait de la colonne de nuée, ils ont gardé ses témoignages et l'ordonnance qu'il leur avait donnée.
99:8	YHWH, notre Elohîm, c'est toi qui leur as répondu, tu as été pour eux le El qui pardonne<!--Hé. 10:16-17.-->, mais qui tire vengeance de leurs actions.
99:9	Exaltez YHWH, notre Elohîm, prosternez-vous sur la montagne de sa sainteté ! Car YHWH, notre Elohîm, est saint !

## Chapitre 100

### Célébrez et bénissez le Nom de YHWH

100:1	Psaume de louange. Vous tous habitants de la Terre, poussez des cris de réjouissance à YHWH !
100:2	Servez YHWH avec allégresse, venez devant lui avec un chant de joie !
100:3	Sachez que YHWH est Elohîm ! C’est lui qui nous a faits, et non pas nous-mêmes, nous sommes son peuple, le troupeau de son pâturage<!--Ps. 79:13, 80:2, 95:6, 119:73.-->.
100:4	Entrez dans ses portes avec des des louanges, dans ses parvis avec des cantiques ! Célébrez-le, bénissez son Nom !
100:5	Car YHWH est bon : sa bonté demeure pour toujours, et sa fidélité d'âge en âge.

## Chapitre 101

### Appel à l'intégrité

101:1	Psaume de David. Je chanterai la miséricorde et la justice. YHWH, je te chanterai !
101:2	Je me rendrai attentif à une conduite pure jusqu'à ce que tu viennes à moi. Je marcherai dans l'intégrité de mon cœur au milieu de ma maison.
101:3	Je ne mettrai pas devant mes yeux des choses de Bélial<!--Voir commentaire en De. 13:14 et Ps. 41:9.-->. Je hais les actions de ceux qui se détournent : elles ne s'attacheront pas à moi !
101:4	Le cœur mauvais s'éloignera de moi, je ne connaîtrai pas le méchant.
101:5	J'exterminerai celui qui calomnie en secret son prochain, je ne supporterai pas celui qui a les yeux élevés et le cœur enflé<!--Pr. 6:16-17.-->.
101:6	J'aurai les yeux sur les fidèles de la terre afin qu'ils demeurent avec moi. Celui qui marche dans la voie de l'intégrité, me servira.
101:7	Celui qui usera de tromperie ne demeurera pas dans ma maison. Celui qui proférera des mensonges ne sera pas affermi devant mes yeux.
101:8	J'exterminerai chaque matin tous les méchants de la terre, afin d'exterminer de la cité de YHWH tous ceux qui pratiquent la méchanceté.

## Chapitre 102

### YHWH, l'Elohîm immuable

102:1	Prière de l'affligé étant dans l'angoisse et répandant sa plainte devant YHWH.
102:2	YHWH, écoute ma prière, et que mon cri parvienne jusqu'à toi<!--Ps. 69:14.--> !
102:3	Ne me cache pas tes faces le jour où je suis dans la détresse, prête l'oreille à ma prière ! Au jour où je t'invoque, hâte-toi de me répondre !
102:4	Car mes jours se sont évanouis comme la fumée et mes os brûlent comme dans un foyer.
102:5	Mon cœur est frappé et se dessèche comme l'herbe, car j'ai oublié de manger mon pain<!--Mt. 4:4 ; Lu. 4:4.-->.
102:6	Le gémissement de ma voix est tel que mes os s'attachent à ma chair<!--Job 19:20.-->.
102:7	Je ressemble au pélican du désert. Je suis devenu comme la chouette des lieux sauvages.
102:8	Je veille, et je suis semblable au passereau solitaire sur le toit.
102:9	Mes ennemis m'outragent tous les jours, et ceux qui sont furieux contre moi, jurent contre moi.
102:10	Car j'ai mangé la cendre comme le pain et j'ai mêlé des larmes à ma boisson,
102:11	à cause de ta colère et de ta fureur, car après m'avoir élevé bien haut, tu m'as jeté par terre.
102:12	Mes jours sont comme l'ombre qui décline, et je deviens sec comme l'herbe.
102:13	Mais toi, YHWH ! tu demeures éternellement, et ta mémoire est d'âge en âge.
102:14	Tu te lèveras, et tu auras compassion de Sion car il est temps d'en avoir pitié, parce que le temps assigné est échu.
102:15	Car tes serviteurs aiment ses pierres et chérissent sa poussière.
102:16	Alors les nations redouteront le Nom de YHWH, et tous les rois de la Terre, ta gloire.
102:17	Quand YHWH aura édifié Sion, quand il aura été vu dans sa gloire,
102:18	quand il aura eu égard à la prière de ceux qu'on a dépouillés, et qu'il n'aura pas méprisé leur supplication.
102:19	Cela sera enregistré pour la génération à venir, le peuple qui sera créé louera Yah.
102:20	Car il regarde du lieu élevé de sa sainteté. Du haut des cieux, YHWH regarde la Terre,
102:21	pour entendre le gémissement des prisonniers, pour délier ceux qui étaient voués à la mort<!--Es. 42:6-7, 61:1 ; Lu. 4:18-19.-->,
102:22	afin qu'on annonce le Nom de YHWH dans Sion, et sa louange dans Yeroushalaim,
102:23	quand les peuples seront réunis tous ensemble, et les royaumes aussi, pour servir YHWH.
102:24	Il a abattu ma force en chemin, il a abrégé mes jours.
102:25	J'ai dit : Mon El, ne m'enlève pas au milieu de mes jours, toi dont les années durent d'âges en âges !
102:26	Tu as jadis fondé la Terre, et les cieux sont l'ouvrage de tes mains.
102:27	Ils périront, mais tu subsisteras. Ils s'useront tous comme un vêtement. Tu les changeras comme un habit, et ils seront changés.
102:28	Mais toi, tu es le même<!--Voir Hé. 13:8. Yéhoshoua est l'Elohîm qui ne change pas.-->, et tes années ne seront jamais achevées.
102:29	Les fils de tes serviteurs habiteront près de toi, et leur postérité sera établie devant toi.

## Chapitre 103

### YHWH, l'Elohîm miséricordieux et compatissant

103:1	De David. Mon âme, bénis YHWH ! Et que tout ce qui est en moi bénisse son saint Nom !
103:2	Mon âme, bénis YHWH, et n'oublie aucun de ses bienfaits<!--De. 6:12.--> !
103:3	C'est lui qui pardonne toutes tes iniquités, qui guérit toutes tes maladies<!--Es. 33:24, 53:5 ; Jé. 17:14 ; Ps. 130:3-4 ; Mt. 9:6 ; Lu. 7:47.--> ;
103:4	qui rachète ta vie de la fosse<!--Es. 59:20 ; Ps. 106:10.-->, qui te couronne de bonté et de compassions.
103:5	C'est lui qui rassasie ta bouche de ce qui est bon, et ta jeunesse est renouvelée comme celle de l'aigle<!--Es. 40:31.-->.
103:6	YHWH fait justice et droit à tous les opprimés<!--Ps. 146:7.-->.
103:7	Il a fait connaître ses voies à Moshé, et ses œuvres aux fils d'Israël<!--Ex. 33:12-17.-->.
103:8	YHWH est compatissant, miséricordieux, lent à la colère, et grand en bonté.
103:9	Il ne conteste pas éternellement, et il ne garde pas pour toujours sa colère<!--Es. 57:16 ; Jé. 3:5 ; Mi. 7:18.-->.
103:10	Il ne nous traite pas selon nos péchés, et ne nous rend pas selon nos iniquités<!--Esd. 9:13.-->.
103:11	Car autant les cieux sont élevés au-dessus de la Terre, autant sa bonté est grande sur ceux qui le craignent.
103:12	Il éloigne de nous nos transgressions, autant que l'orient est éloigné de l'occident<!--Es. 38:17.-->.
103:13	Comme un père a compassion de ses fils, YHWH a compassion de ceux qui le craignent<!--Mal. 3:17 ; Lu. 11:11-13.-->.
103:14	Car il sait bien de quoi nous sommes faits, il se souvient que nous ne sommes que poussière.
103:15	Le mortel ! Ses jours sont comme l'herbe<!--Es. 40:6-8 ; Job 14:1-2 ; 1 Pi. 1:24.-->, il fleurit comme la fleur d'un champ.
103:16	Lorsque le vent passe dessus, elle n'est plus, et son lieu ne la reconnaît plus.
103:17	Mais la miséricorde de YHWH dure d'éternité en éternité pour ceux qui le craignent, et sa justice pour les fils de leurs fils ;
103:18	pour ceux qui gardent son alliance, et qui se souviennent de ses préceptes pour les accomplir<!--De. 7:9.-->.
103:19	YHWH a établi son trône dans les cieux, et son règne domine sur tout.
103:20	Bénissez YHWH, vous, ses anges puissants en force, qui accomplissez sa parole, en obéissant à la voix de sa parole !
103:21	Bénissez YHWH, vous toutes ses armées, qui êtes ses serviteurs, qui accomplissez sa volonté !
103:22	Bénissez YHWH, vous toutes ses œuvres, par tous les lieux de sa domination ! Mon âme, bénis YHWH !

## Chapitre 104

### YHWH, l'Elohîm de toute la création

104:1	Mon âme, bénis YHWH ! YHWH, mon Elohîm, tu es merveilleusement grand, tu es revêtu de majesté et de splendeur.
104:2	Il s'enveloppe de lumière comme d'un vêtement. Il étend les cieux comme un voile<!--Es. 40:22 ; Job 9:8 ; 1 Ti. 6:16.-->.
104:3	Avec les eaux, il pose la charpente de sa chambre haute, faisant des nuées<!--Ap. 1:7.--> son char, marchant sur les ailes du vent<!--Es. 19:1 ; Ps. 18:10 ; Ap. 14:14.-->.
104:4	Il fait des vents ses messagers, et des flammes de feu ses serviteurs<!--Ps. 148:8 ; Jn. 3:8 ; Hé. 1:7.-->.
104:5	Il a fondé la Terre sur ses bases, elle ne sera jamais ébranlée<!--Ps. 24:1-2, 78:69, 93:1 ; Job 26:7, 38:4-6.-->.
104:6	Tu l'avais couverte de l'abîme comme d'un vêtement, les eaux se tenaient sur les montagnes<!--Ge. 1:2.-->.
104:7	Elles s'enfuirent à ta menace, et se mirent promptement en fuite au son de ton tonnerre.
104:8	Les montagnes s'élevèrent, et les vallées s'abaissèrent au même lieu que tu leur avais fixé.
104:9	Tu as posé une limite que les eaux ne doivent pas franchir, afin qu'elles ne reviennent plus couvrir la Terre<!--Ge. 1:9 ; Jé. 5:22 ; Pr. 8:29 ; Job 26:10.-->.
104:10	C'est lui qui conduit les sources par les vallées, elles se promènent entre les monts.
104:11	Elles abreuvent toutes les bêtes des champs, les ânes sauvages y étanchent leur soif.
104:12	Les oiseaux des cieux se tiennent auprès d'elles, et font résonner leur voix parmi les rameaux.
104:13	Il abreuve les montagnes de ses chambres hautes. La Terre est rassasiée du fruit de tes œuvres.
104:14	Il fait germer l'herbe pour le bétail, et les plantes pour le besoin de l'homme, faisant sortir le pain de la terre,
104:15	et le vin qui réjouit le cœur du mortel<!--Jg. 9:13 ; Pr. 31:6-7.-->, qui fait resplendir son visage avec l'huile, et qui soutient le cœur du mortel avec le pain.
104:16	Les hauts arbres de YHWH en sont rassasiés, ainsi que les cèdres du Liban qu'il a plantés,
104:17	afin que les oiseaux y fassent leurs nids. Quant à la cigogne, les sapins sont sa maison.
104:18	Les hautes montagnes sont pour les chèvres de montagne, et les rochers sont la retraite des damans<!--Blaireaux des rochers ou lapins.-->.
104:19	Il a fait la lune pour les temps fixés<!--Ge. 1:14.-->, et le soleil sait quand il doit se coucher<!--Ge. 1:16.-->.
104:20	Tu amènes les ténèbres, et c'est la nuit où tous les animaux de la forêt se mettent à grouiller.
104:21	Les lionceaux rugissent après la proie, pour demander à El leur nourriture.
104:22	Le soleil se lève-t-il ? Ils se retirent et se couchent dans leurs tanières.
104:23	L'être humain sort pour son ouvrage, pour son travail jusqu’au soir.
104:24	YHWH, que tes œuvres sont en grand nombre ! Tu les as toutes faites avec sagesse. La Terre est pleine de tes richesses.
104:25	Voici la grande mer aux larges mains. Là où des animaux sans nombre se meuvent, des petites bêtes avec des grandes !
104:26	Là se promènent les navires, et ce léviathan que tu as formé pour y jouer.
104:27	Ils s'attendent tous à toi, afin que tu leur donnes la nourriture en leur temps.
104:28	Quand tu la leur donnes, ils la recueillent, et quand tu ouvres ta main, ils sont rassasiés de biens.
104:29	Caches-tu tes faces ? Ils sont terrifiés. Retires-tu leur esprit ? Ils défaillent et retournent dans leur poussière.
104:30	Tu envoies ton Esprit, ils sont créés, et tu renouvelles les faces du sol.
104:31	Que la gloire de YHWH subsiste pour toujours ! Que YHWH se réjouisse dans ses œuvres !
104:32	Il jette son regard sur la Terre, et elle tremble, il touche les montagnes et elles fument !
104:33	Je chanterai à YHWH durant ma vie, je chanterai à mon Elohîm tant que j'existerai.
104:34	Ma méditation lui sera agréable, et je me réjouirai en YHWH.
104:35	Que les pécheurs soient consumés de dessus la terre et qu'il n'y ait plus de méchants ! Mon âme, bénis YHWH ! Allélou-Yah !

## Chapitre 105

### YHWH, l'Elohîm fidèle

105:1	Célébrez YHWH, invoquez son Nom ! Faites connaître parmi les peuples ses œuvres !
105:2	Chantez-lui, chantez-lui des psaumes, parlez de toutes ses merveilles !
105:3	Glorifiez-vous de son saint Nom, et que le cœur de ceux qui cherchent YHWH se réjouisse !
105:4	Recherchez YHWH et sa puissance, cherchez continuellement ses faces !
105:5	Souvenez-vous des merveilles qu'il a faites, de ses miracles, et des jugements de sa bouche !
105:6	Postérité d'Abraham, son serviteur, enfants de Yaacov, ses élus !
105:7	Il est YHWH, notre Elohîm, ses jugements sont sur toute la Terre.
105:8	Il s'est souvenu pour toujours de son alliance, de la parole qu'il a ordonnée en mille générations,
105:9	de l’alliance qu’il a traitée avec Abraham, et de son serment à Yitzhak<!--Ge. 17:2, 22:16, 26:3, 28:13, 33:11 ; Lu. 1:73.-->.
105:10	Il l'a établi en ordonnance pour Yaacov, pour Israël en alliance éternelle,
105:11	en disant : Je te donnerai la terre de Kena'ân, comme héritage qui vous est échu<!--Ge. 13:15, 15:18.-->.
105:12	Ils étaient alors un petit nombre de gens, très peu nombreux, et étrangers sur la terre.
105:13	Car ils allaient de nation en nation, et d'un royaume vers un autre peuple.
105:14	Il ne permit à personne de les opprimer, et il châtia des rois à cause d'eux<!--Ge. 35:5.--> :
105:15	Ne touchez pas à mes mashiah<!--Oints.--> ! Ne faites pas de mal à mes prophètes<!--1 Ch. 16:22.--> !
105:16	Il appela aussi la famine sur la Terre, et rompit le bâton du pain<!--Lé. 26:26 ; Es. 3:1 ; Ez. 4:16.-->.
105:17	Il envoya un homme devant eux : Yossef fut vendu comme esclave<!--Ge. 37:28-36.-->.
105:18	On serra ses pieds dans des ceps, son âme entra dans les fers,
105:19	jusqu'au temps où vint sa parole, où la parole de YHWH l'éprouva.
105:20	Le roi le relâcha et le laissa aller, le dominateur des peuples le délivra.
105:21	Il l'établit seigneur sur sa maison et gouverneur de tous ses biens<!--Ge. 41:40.-->,
105:22	pour lier ses princes à son âme et pour enseigner la sagesse à ses anciens.
105:23	Puis Israël entra en Égypte, et Yaacov séjourna en terre de Cham<!--Ge. 46:6 ; Ps. 78:51.-->.
105:24	Il rendit son peuple très fécond et plus puissant que ceux qui l'opprimaient.
105:25	Il changea leur cœur, de sorte qu'ils se mirent à haïr son peuple et traitèrent ses serviteurs avec perfidie<!--Ex. 1:7-12.-->.
105:26	Il envoya Moshé, son serviteur, et Aaron, qu'il avait choisi<!--Ex. 4:14.-->.
105:27	Il mit en eux les paroles de ses signes, des miracles en terre de Cham.
105:28	Il envoya les ténèbres et fit venir l'obscurité, et ils ne furent pas rebelles à sa parole.
105:29	Il changea leurs eaux en sang et fit mourir leurs poissons.
105:30	Leur terre grouilla de grenouilles, jusque dans les chambres de leurs rois.
105:31	Il parla, et des essaims d'insectes vinrent, des poux sur toute leur terre.
105:32	Il leur donna pour pluie de la grêle, et un feu flamboyant sur la Terre.
105:33	Il frappa leurs vignes et leurs figuiers, et il brisa les arbres de leur terre.
105:34	Il ordonna et les sauterelles vinrent, des jeunes sauterelles sans nombre
105:35	qui dévorèrent toute l'herbe de la terre, et qui dévorèrent le fruit de leur sol.
105:36	Il frappa tous les premiers-nés de leur terre, les premiers de toute leur vigueur<!--Lire Ex. 7-12.-->.
105:37	Puis il les fit sortir avec de l'or et de l'argent, et personne ne chancela parmi ses tribus.
105:38	L'Égypte se réjouit à leur départ, car la peur qu'ils avaient d'eux les avait saisis.
105:39	Il étendit la nuée pour couverture, et le feu pour être la lumière la nuit.
105:40	Le peuple demanda et il fit venir des cailles, et il les rassasia du pain des cieux<!--Ex. 16:12-13.-->.
105:41	Il ouvrit le rocher et les eaux en coulèrent, elles se répandirent comme un fleuve dans les lieux arides<!--Ex. 17:6.-->.
105:42	Car il se souvint de sa parole sainte qu'il avait donnée à Abraham, son serviteur<!--Ge. 15:13-16.-->.
105:43	Il fit sortir son peuple dans l'allégresse, ses élus au milieu des cris retentissants<!--Ex. 15:1.-->.
105:44	Il leur donna les terres des nations, ils prirent possession du labeur des peuples,
105:45	afin qu'ils gardent ses statuts et qu'ils observent sa torah. Allélou-Yah !

## Chapitre 106

### L'infidélité d'Israël

106:1	Allélou-Yah ! Célébrez YHWH car il est bon, car sa bonté demeure pour toujours !
106:2	Qui parlera des actions puissantes de YHWH ? Qui fera entendre toute sa louange ?
106:3	Heureux ceux qui observent la justice, qui font en tout temps ce qui est juste !
106:4	YHWH, souviens-toi de moi, dans ta faveur pour ton peuple ! Aie soin de moi selon ta délivrance !
106:5	Afin que je voie le bien de tes élus, que je me réjouisse dans la joie de ta nation, que je me glorifie avec ton héritage.
106:6	Nous avons péché avec nos pères, nous avons commis l'iniquité, nous avons agi méchamment<!--Da. 9:16 ; Esd. 9:7 ; Né. 1:6.-->.
106:7	Nos pères n'ont pas été attentifs à tes merveilles en Égypte. Ils ne se sont pas souvenus de la multitude de tes faveurs, mais ils furent rebelles près de la mer, vers la Mer Rouge<!--Ex. 14:11.-->.
106:8	Toutefois, il les délivra pour l'amour de son Nom, afin de faire connaître sa puissance.
106:9	Car il menaça la Mer Rouge et elle se dessécha. Il les conduisit à travers les profondeurs de la mer comme dans un désert ;
106:10	il les délivra de la main de ceux qui les haïssaient, et les racheta de la main de l'ennemi.
106:11	Les eaux couvrirent leurs oppresseurs, il n'en resta pas un seul<!--Ex. 14:27.-->.
106:12	Ils crurent à ses paroles, et ils chantèrent sa louange.
106:13	Mais ils oublièrent vite ses œuvres, et ne s'attendirent pas à son conseil.
106:14	Ils brûlèrent de désir dans le désert, ils tentèrent El dans le lieu de désolation.
106:15	Il leur donna ce qu'ils avaient demandé, toutefois il leur envoya le dépérissement dans leur âme.
106:16	Ils jalousèrent dans le camp Moshé et Aaron, le saint de YHWH.
106:17	La Terre s'ouvrit et engloutit Dathan, elle recouvrit l'assemblée d'Abiram<!--No. 16.-->.
106:18	Le feu s'alluma au milieu de leur assemblée, et la flamme brûla les méchants.
106:19	Ils firent un veau en Horeb, et se prosternèrent devant une image de métal fondu<!--Ex. 32.-->.
106:20	Ils échangèrent leur gloire contre la figure d'un bœuf<!--Voir Ro. 1:22.--> qui mange l'herbe.
106:21	Ils oublièrent El, leur Sauveur, qui avait fait de grandes choses en Égypte,
106:22	des choses merveilleuses en terre de Cham, et des choses terribles sur la Mer Rouge.
106:23	Il parla de les détruire, mais Moshé, son élu, se tint à la brèche devant lui pour détourner sa fureur, afin qu'il ne les détruisît pas<!--Ex. 32:11.-->.
106:24	Ils méprisèrent la terre désirable, et ne crurent pas à sa parole.
106:25	Ils murmurèrent dans leurs tentes, et n'obéirent pas à la voix de YHWH.
106:26	Il leva la main contre eux, pour les faire tomber dans le désert,
106:27	pour faire tomber leur postérité parmi les nations, et les disperser au milieu des terres<!--No. 14:22.-->.
106:28	Ils s'attachèrent à Baal-Peor, et mangèrent les sacrifices des morts.
106:29	Et ils irritèrent Elohîm par leurs actions, au point qu'une plaie fit une brèche parmi eux.
106:30	Mais Phinées se présenta et fit justice, et la plaie fut arrêtée.
106:31	Et cela lui fut imputé à justice d'âge en âge, pour toujours<!--No. 25:3-8.-->.
106:32	Ils excitèrent aussi sa colère près des eaux de Meriybah, et Moshé fut puni à cause d'eux.
106:33	Car ils se révoltèrent contre son esprit, et il parla avec légèreté de ses lèvres<!--No. 20:12.-->.
106:34	Ils ne détruisirent pas les peuples que YHWH leur avait dit de détruire,
106:35	mais ils se mêlèrent<!--« Gager », « échanger », « hypothèque », « engagement », « occuper », « entreprendre », « donner des garanties », « être en sécurité ».--> avec ces nations et apprirent leurs œuvres.
106:36	Ils servirent leurs idoles qui furent un piège pour eux.
106:37	Car ils sacrifièrent leurs fils et leurs filles aux démons<!--Lé. 18:21 ; De. 12:31, 32:17 ; 2 R. 16:3 ; Ez. 20:26.-->.
106:38	Ils répandirent le sang innocent, le sang de leurs fils et de leurs filles, qu'ils sacrifièrent aux idoles de Kena'ân, et la terre fut souillée par le sang<!--No. 35:33.-->.
106:39	Ils se rendirent impurs par leurs œuvres, et se prostituèrent par leurs actions.
106:40	La colère de YHWH s’enflamma contre son peuple, et il eut en abomination son héritage.
106:41	Il les livra entre les mains des nations, et ceux qui les haïssaient dominèrent sur eux.
106:42	Leurs ennemis les opprimèrent, et ils furent humiliés sous leur main.
106:43	Il les délivra souvent, mais ils se montrèrent rebelles dans leurs desseins et furent humiliés par leur iniquité.
106:44	Il vit leur détresse lorsqu'il entendit leurs supplications.
106:45	Et il se souvint en leur faveur de son alliance, et se repentit selon la grandeur de ses compassions.
106:46	Et il leur fit trouver compassion auprès de tous ceux qui les avaient emmenés captifs.
106:47	YHWH, notre Elohîm, délivre-nous et rassemble-nous du milieu des nations, afin que nous célébrions ton saint Nom et que nous mettions notre gloire à te louer !
106:48	Béni soit YHWH, l'Elohîm d'Israël, d'éternité en éternité ! Et que tout le peuple dise : Amen ! Allélou-Yah !

## Chapitre 107

### La grâce de YHWH pour ses rachetés

107:1	Célébrez YHWH car il est bon, parce que sa bonté demeure pour toujours !
107:2	Qu'ainsi disent les rachetés de YHWH, ceux qu'il a rachetés de la main de l'oppresseur,
107:3	et qu'il a rassemblés de toutes les terres, de l'orient et de l'occident, du nord et du sud.
107:4	Ils étaient errants dans le désert, dans un chemin solitaire, et ils ne trouvaient aucune ville habitée.
107:5	Ils étaient affamés et assoiffés, leur âme en eux défaillait.
107:6	Ils ont crié vers YHWH dans leur détresse et il les a délivrés de leurs angoisses.
107:7	Il les a conduits sur le droit chemin pour aller dans une ville habitée.
107:8	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:9	Parce qu'il a rassasié l'âme altérée, et rempli de biens l'âme affamée<!--Ps. 146:7 ; Lu. 1:53.-->.
107:10	Ceux qui habitaient dans les ténèbres et l'ombre de la mort étaient prisonniers de l'affliction et des fers,
107:11	parce qu'ils étaient rebelles aux paroles de El, et parce qu'ils avaient rejeté le conseil d'Élyon<!--De. 31:20 ; La. 3:42.-->.
107:12	Il humilia leur cœur par le travail : ils trébuchaient, et personne ne les secourait.
107:13	Ils ont crié vers YHWH dans leur détresse, et il les a délivrés de leurs angoisses.
107:14	Il les a fait sortir hors des ténèbres et de l'ombre de la mort, et il a brisé leurs liens<!--Ps. 68:19 ; Ep. 4:8 ; Col. 1:12-13.-->.
107:15	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:16	Parce qu'il a brisé les portes de cuivre et cassé les barreaux de fer.
107:17	Les fous sont affligés à cause de leur transgression et à cause de leurs iniquités.
107:18	Leur âme avait en horreur toute nourriture, et ils touchaient aux portes de la mort.
107:19	Ils ont crié vers YHWH dans leur détresse, et il les a délivrés de leurs angoisses<!--Ps. 50:15 ; Os. 5:15.-->.
107:20	Il a envoyé sa parole et les a guéris, et il les a fait échapper de leurs tombeaux.
107:21	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:22	Qu'ils sacrifient des sacrifices des louanges, et qu'ils racontent ses œuvres avec des cris de joie !
107:23	Ceux qui descendaient sur la mer dans des navires, faisant des affaires sur les grandes eaux,
107:24	ceux-là ont vu les œuvres de YHWH et ses merveilles dans les profondeurs.
107:25	Il parla et fit lever un vent de tempête qui souleva des grosses vagues.
107:26	Ils montaient vers les cieux, ils descendaient dans l'abîme, sous le mal leur âme fondait ;
107:27	saisis de vertiges, ils chancelaient comme un homme ivre et toute leur sagesse était anéantie<!--Es. 51:17-21 ; Jé. 13:13.-->.
107:28	Ils ont crié vers YHWH dans leur détresse, et il les a fait sortir de leurs angoisses.
107:29	Il a arrêté la tempête, ramené le calme, et les vagues sont restées tranquilles<!--Mc. 4:35-41.-->.
107:30	Ils se réjouirent, car elles s’étaient tues, et il les a conduits au havre de leur désir.
107:31	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:32	Et qu'ils l'exaltent dans l'assemblée du peuple, et le louent dans l'assemblée des anciens !
107:33	Il transforma les fleuves en désert et les sources d'eaux en sol assoiffé,
107:34	la terre fertile en terre salée, à cause de la méchanceté de ses habitants<!--Jé. 12:4, 17:6.-->.
107:35	Il transforma le désert en étangs d'eaux, et la terre sèche en des sources d'eaux<!--Es. 41:18.-->.
107:36	Il fit habiter là les affamés, ils établirent une ville pour y habiter.
107:37	Ils ensemencèrent des champs, plantèrent des vignes, ils en récoltèrent les fruits.
107:38	Il les bénit et ils se multiplièrent extrêmement, et leur bétail ne diminua pas.
107:39	Ils étaient diminués et humiliés par l'oppression, le malheur et la souffrance.
107:40	Il répandit le mépris sur les princes et les fit errer dans un tohu sans chemin.
107:41	Mais il releva l'indigent et le délivra de la misère, il établit les familles comme des troupeaux<!--1 S. 2:8 ; Ps. 113:7.-->.
107:42	Les justes le virent et se réjouirent, et toute injustice ferma sa bouche.
107:43	Que celui qui est sage prenne garde à ces choses, et considère les bontés de YHWH.

## Chapitre 108

### YHWH, le secours

108:1	Cantique. Psaume de David.
108:2	Mon cœur est ferme, Elohîm ! Ma gloire l'est aussi, je chanterai et je jouerai des psaumes !
108:3	Réveillez-vous, mon luth et ma harpe ! Je me réveillerai à l'aube du jour.
108:4	YHWH, je te célébrerai parmi les peuples et je te chanterai parmi les nations.
108:5	Car ta bonté est grande par-dessus les cieux, et ta vérité atteint jusqu'aux nuages.
108:6	Elohîm ! élève-toi sur les nuages, et que ta gloire soit sur toute la Terre !
108:7	Afin que ceux que tu aimes soient délivrés. Sauve-moi par ta droite et réponds-moi !
108:8	Elohîm a dit dans sa sainteté : Je me réjouirai, je partagerai Shekem et mesurerai la vallée de Soukkoth.
108:9	Galaad sera à moi, Menashè sera à moi, et Éphraïm sera le sommet de ma forteresse, Yéhouda, mon législateur.
108:10	Moab sera le bassin où je me laverai, je jetterai ma sandale sur Édom, je triompherai des Philistins !
108:11	Qui me conduira dans la ville forte ? Qui me conduira jusqu'en Édom ?
108:12	N'est-ce pas toi, Elohîm, qui nous avais rejetés, et qui ne sortais plus, Elohîm, avec nos armées ?
108:13	Donne-nous du secours contre la détresse ! Car le salut de l'humain est vain.
108:14	Avec Elohîm, nous agirons avec force, et c'est lui qui foule aux pieds nos ennemis<!--Ps. 60:5-14.-->.

## Chapitre 109

### La méchanceté de l'homme

109:1	Au chef<!--Les Psaumes d'imprécations (Ps. chapitres 35, 52, 55, 58, 59, 79, 109 et 137) sont des demandes faites à Elohîm pour qu'il punisse les méchants. Le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) nous demande aujourd'hui de bénir nos ennemis (Lu. 6:27-37).-->. Psaume de David. Elohîm de ma louange, ne te tais pas !
109:2	Car la bouche du méchant et la bouche du perfide s’ouvrent contre moi. Ils parlent contre moi avec une langue mensongère,
109:3	ils m'entourent de paroles pleines de haine et ils me font la guerre sans cause !
109:4	En échange de mon amour, ils sont mes adversaires, mais chez moi il y a la prière.
109:5	Ils me rendent le mal pour le bien, et la haine pour l'amour que je leur porte.
109:6	Établis le méchant sur lui, et que Satan se tienne à sa droite !
109:7	Quand il sera jugé, fais qu'il soit déclaré méchant, et que sa prière soit regardée comme un péché !
109:8	Que ses jours soient peu nombreux, et qu'un autre prenne sa fonction<!--Ce passage fait allusion à Yéhouda (Judas) Iscariot.--> !
109:9	Que ses enfants soient orphelins, et sa femme veuve !
109:10	Que ses enfants soient errants, qu'ils soient errants et mendiants, qu'ils aillent consulter loin de leur maison en désolation !
109:11	Que le créancier tende un piège sur tout ce qui est à lui, et que les étrangers pillent son travail !
109:12	Que personne n'étende sa compassion sur lui, et que personne n'ait pitié de ses orphelins !
109:13	Que sa postérité soit retranchée, que leur nom soit effacé dans la génération suivante ! 
109:14	Que l'iniquité de ses pères revienne en mémoire à YHWH, et que le péché de sa mère ne soit pas effacé !
109:15	Qu'ils soient continuellement devant YHWH, et qu'il retranche leur mémoire de la Terre<!--Ps. 34:17.-->,
109:16	parce qu'il ne s'est pas souvenu de faire miséricorde, mais il a persécuté l'homme affligé et l'indigent, dont le cœur est brisé, pour le faire mourir !
109:17	Il a aimé la malédiction : qu'elle vienne sur lui ! Il n'a pas pris plaisir à la bénédiction : qu'elle s'éloigne de lui !
109:18	Qu'il soit revêtu de la malédiction comme de sa robe, qu'elle entre dans son corps comme de l'eau, et dans ses os comme de l'huile !
109:19	Qu’elle soit pour lui comme un vêtement dont il se couvre et comme une ceinture dont il se ceigne continuellement !
109:20	Tel sera, de la part de YHWH, le salaire de mes adversaires, et de ceux qui parlent mal de mon âme !
109:21	Mais toi, YHWH Adonaï, agis avec moi pour l'amour de ton Nom ! Et parce que ta miséricorde est grande, délivre-moi !
109:22	Car je suis affligé et indigent, et mon cœur est blessé au-dedans de moi.
109:23	Je m'en vais comme l'ombre qui s’étire, je suis secoué comme une sauterelle.
109:24	Mes genoux sont affaiblis par le jeûne, ma chair est devenue maigre faute d'huile.
109:25	Je suis devenu pour eux une insulte. Quand ils me voient, ils secouent la tête.
109:26	YHWH, mon Elohîm ! aide-moi, délivre-moi selon ta miséricorde.
109:27	Afin qu'on sache que c'est ta main, que c'est toi, YHWH, qui as fait cela ! 
109:28	Ils maudiront, mais tu béniras. Ils s'élèveront, mais ils seront confus, et ton serviteur se réjouira.
109:29	Que mes adversaires soient revêtus de confusion, et couverts de leur honte comme d'une robe !
109:30	Je célébrerai grandement de ma bouche YHWH, et je le louerai au milieu de la multitude,
109:31	car il se tient debout<!--Ac. 7:55.--> à la droite de l'indigent pour le sauver de ceux qui jugent son âme.

## Chapitre 110

### YHWH, le Roi et le prêtre

110:1	Psaume de David. Déclaration de YHWH à mon Seigneur : Assieds-toi à ma droite, jusqu'à ce que je fasse de tes ennemis le marchepied de tes pieds<!--Ce psaume affirme la divinité de Yéhoshoua ha Mashiah (Jésus-Christ) (Mt. 22:41-46 ; Mc. 12:35-37 ; Lu. 20:41-44 ; Ac. 2:34-35 ; Hé. 1:13, 10:12-13).-->.
110:2	YHWH étendra de Sion le sceptre de ta puissance : domine au milieu de tes ennemis<!--Es. 2:2-3 ; Da. 7:14.--> !
110:3	Ton peuple sera bien disposé au jour de ton armée. Avec des ornements sacrés, du sein de l'aurore, ta jeunesse vient à toi comme une rosée.
110:4	YHWH l'a juré et il ne s'en repentira pas : Tu es prêtre éternellement, à la manière de Malkiy-Tsédeq<!--Melchisédek. Ge. 14:18 ; Hé. 5:6, 6:20, 7:17.-->.
110:5	Adonaï est à ta droite, il brisera les rois au jour de sa colère.
110:6	Il exercera le jugement sur les nations, il les remplira de cadavres, il brisera la tête d'une vaste terre<!--Ap. 14 et 16.-->.
110:7	Il boit au torrent pendant la marche : c'est pourquoi il lève haut la tête.

## Chapitre 111

### Les œuvres magnifiques d'Elohîm

111:1	Allélou-Yah. [Aleph.] Je célébrerai YHWH de tout mon cœur, [Beth.] dans le conseil secret des justes et dans l'assemblée.
111:2	[Guimel.] Les œuvres de YHWH sont grandes, [Daleth.] elles sont recherchées par tous ceux qui y prennent plaisir.
111:3	[He.] Son œuvre n'est que majesté et magnificence, [Vav.] et sa justice demeure à perpétuité.
111:4	[Zayin.] Il a fait un mémorial pour ses merveilles. [Heth.] YHWH est miséricordieux et compatissant.
111:5	[Teth.] Il a donné de la nourriture à ceux qui le craignent, [Yod.] il s'est souvenu pour toujours de son alliance.
111:6	[Kaf.] Il a manifesté à son peuple la puissance de ses œuvres, [Lamed.] en leur donnant l'héritage des nations.
111:7	[Mem.] Les œuvres de ses mains ne sont que vérité et équité. [Noun.] Tous ses préceptes sont véritables,
111:8	[Samech.] posés pour l'éternité, pour toujours, [Ayin.] faits avec fidélité et droiture.
111:9	[Pe.] Il a envoyé la rançon à son peuple<!--Ex. 6:6 ; Jn. 3:16.-->, [Tsade.] il lui a donné une alliance éternelle ; [Qof.] son Nom est saint et redoutable.
111:10	[Resh.] Le commencement de la sagesse, c'est la crainte de YHWH : [Shin.] Tous ceux qui l'observent ont une bonne compréhension<!--De. 4:6 ; Pr. 1:7, 9:10, 8:13.-->. [Tav.] Sa louange demeure à perpétuité.

## Chapitre 112

### La crainte de YHWH enrichit et donne de l'assurance

112:1	Allélou-Yah ! [Aleph.] Heureux l'homme qui craint YHWH [Beth.] et qui prend un grand plaisir à ses commandements !
112:2	[Guimel.] Sa postérité sera puissante sur la Terre, [Daleth.] la génération des justes sera bénie<!--Pr. 20:7.-->.
112:3	[He.] Il y aura des biens et des richesses dans sa maison, [Vav.] et sa justice demeure à perpétuité.
112:4	[Zayin.] La lumière s'est levée dans les ténèbres sur ceux qui sont justes<!--Ps. 37:6 ; Pr. 4:18.-->, [Heth.] il est compatissant, miséricordieux et juste.
112:5	[Teth.] Il est bon l'homme qui exerce la miséricorde et qui prête, [Yod.] qui règle ses actions avec justice !
112:6	[Kaf.] Car il ne chancelle jamais. [Lamed.] La mémoire du juste sera éternelle<!--Pr. 10:7.-->.
112:7	[Mem.] Il ne craint pas les mauvaises nouvelles, [Noun.] son cœur est ferme, confiant en YHWH.
112:8	[Samech.] Son cœur est bien affermi, il ne craint pas, [Ayin.] jusqu'à ce qu'il mette son plaisir à regarder ses adversaires.
112:9	[Pe.] Il a répandu, il a donné aux indigents, [Tsade.] sa justice demeure à perpétuité<!--Voir 2 Co. 9:9.-->, [Qof.] sa corne s'élève en gloire.
112:10	[Resh.] Le méchant le voit et s'irrite, [Shin.] il grince des dents et fond. [Tav.] Les désirs des méchants périssent.

## Chapitre 113

### YHWH, l'Elohîm élevé au-dessus de tout

113:1	Allélou-Yah<!--Les psaumes disant « Allélou-Yah » sont les Ps. 104 à 106, 111 à 113, 115 à 117, 135 à 136, 146 à 150. Parmi eux, les Ps. 135 et 146 à 150, étaient chantés durant le service quotidien d'adoration dans la synagogue. Les psaumes 115 à 118, appelés « le grand Hallel », étaient chantés lors des fêtes de Pâque. Allélou-Yah veut dire « Louez Yah » (Ap. 19:1).--> ! Louez, vous serviteurs de YHWH, louez le Nom de YHWH !
113:2	Que le Nom de YHWH soit béni dès maintenant et pour toujours !
113:3	Le Nom de YHWH est digne de louanges depuis le soleil levant jusqu'au soleil couchant.
113:4	YHWH est élevé au-dessus de toutes les nations, sa gloire est au-dessus des cieux.
113:5	Qui est semblable à YHWH notre Elohîm, qui habite dans les lieux très hauts ?
113:6	Il s'abaisse pour regarder dans les cieux et sur la Terre.
113:7	Il relève le pauvre de la poussière, et retire l'indigent<!--1 S. 2:8 ; Ps. 107:41.--> du fumier,
113:8	pour les faire asseoir avec les nobles, avec les nobles de son peuple<!--Job 36:7.-->.
113:9	Il donne une maison à la femme stérile, il en fait une mère joyeuse au milieu de ses enfants<!--Ge. 17:17-21 ; 1 S. 2:5 ; Ps. 68:6.-->. Allélou-Yah !

## Chapitre 114

### La création tremble devant le Tout-Puissant

114:1	Quand Israël sortit d'Égypte, la maison de Yaacov de chez un peuple qui parle indistinctement,
114:2	Yéhouda devint son lieu saint, Israël son domaine<!--Jé. 2:2-3.-->.
114:3	La mer le vit et s'enfuit, le Yarden retourna en arrière<!--Jos. 3:13-16 ; Ps. 77:17.-->.
114:4	Les montagnes sautèrent comme des béliers, les collines comme des jeunes moutons<!--Jg. 5:5 ; Ha. 3:10 ; Ps. 68:9.-->.
114:5	Qu’as-tu, mer, pour t’enfuir, Yarden, pour retourner en arrière ?
114:6	Vous, montagnes, pour sauter comme des béliers ? Et vous collines, comme des agneaux ?
114:7	Face au Seigneur, tremble, Terre, face à l'Éloah de Yaacov !
114:8	Il change le rocher en étang d’eaux, la pierre dure en source d'eaux.

## Chapitre 115

### Louange à l'Elohîm de gloire

115:1	Non pas à nous, YHWH ! non pas à nous, mais à ton Nom donne gloire, à cause de ta bonté, à cause de ta fidélité !
115:2	Pourquoi les nations diraient-elles : Où est maintenant leur Elohîm ?
115:3	Notre Elohîm est dans les cieux, il fait tout ce qu'il désire<!--Ps. 135:6 ; Job 23:13.-->.
115:4	Leurs idoles sont de l'argent et de l'or, ouvrage de mains humaines.
115:5	Elles ont une bouche mais ne parlent pas, elles ont des yeux mais ne voient pas,
115:6	elles ont des oreilles mais n'entendent pas, elles ont un nez mais ne sentent pas,
115:7	elles ont des mains mais ne touchent pas, elles ont des pieds mais ne marchent pas, elles ne poussent pas des cris de leur gosier<!--Ex. 32:2-8 ; 1 R. 18:25-26 ; Es. 44:9 ; Ez. 8:8-12.-->.
115:8	Ils leur ressemblent, ceux qui les fabriquent, tous ceux qui se confient en elles.
115:9	Israël, confie-toi en YHWH ! Il est le secours et le bouclier de ceux qui se confient en lui.
115:10	Maison d'Aaron, confie-toi en YHWH ! Il est leur secours et leur bouclier.
115:11	Vous qui craignez YHWH, confiez-vous en YHWH ! Il est leur secours et leur bouclier.
115:12	YHWH s'est souvenu de nous, il bénira, il bénira la maison d'Israël, il bénira la maison d'Aaron.
115:13	Il bénira ceux qui craignent YHWH, tant les petits que les grands.
115:14	Que YHWH vous fasse croître, vous et vos enfants !
115:15	Vous êtes bénis de YHWH, qui a fait les cieux et la Terre.
115:16	Les cieux sont les cieux de YHWH, mais il a donné la Terre aux fils d'Adam.
115:17	Ce ne sont pas les morts qui célèbrent Yah, ce n'est aucun de ceux qui descendent dans le lieu du silence<!--Es. 38:18-19 ; Ps. 6:6, 88:11.-->.
115:18	Mais nous, nous bénirons Yah dès maintenant et pour toujours. Allélou-Yah !

## Chapitre 116

### Psaume des rachetés

116:1	J'aime YHWH, car il a entendu ma voix et mes supplications,
116:2	car il a incliné son oreille vers moi, c'est pourquoi je l'invoquerai durant mes jours.
116:3	Les liens de la mort m'avaient entouré, et les angoisses du shéol m'avaient trouvé<!--2 S. 22:5 ; Ps. 18:5.-->. J'avais trouvé la détresse et la douleur.
116:4	Mais j'invoquai le Nom de YHWH : Oh ! Je t'en prie YHWH, délivre mon âme !
116:5	YHWH est compatissant et juste, notre Elohîm fait miséricorde.
116:6	YHWH garde les stupides. J'étais devenu misérable et il m'a sauvé.
116:7	Mon âme, retourne dans ton repos, car YHWH t'a fait du bien.
116:8	Car tu as retiré mon âme de la mort, mes yeux des larmes et mes pieds de la chute.
116:9	Je marcherai en face de YHWH, sur la Terre des vivants.
116:10	J'ai cru, c'est pourquoi j'ai parlé<!--2 Co. 4:13.-->. J'ai été extrêmement affligé.
116:11	Je disais dans ma précipitation : Tout homme est menteur<!--Ro. 3:4.--> !
116:12	Que rendrai-je à YHWH pour tous ses bienfaits envers moi ?
116:13	J'élèverai la coupe des délivrances et j'invoquerai le Nom de YHWH.
116:14	J'accomplirai maintenant mes vœux envers YHWH, devant tout son peuple.
116:15	La mort des bien-aimés de YHWH est précieuse à ses yeux.
116:16	Oh ! Je t'en prie YHWH ! car je suis ton serviteur, je suis ton serviteur, fils de ta servante. Tu as délié mes liens.
116:17	Pour toi, je sacrifierai le sacrifice des louanges et j'appellerai le Nom de YHWH.
116:18	J'accomplirai maintenant mes vœux envers YHWH, devant tout son peuple,
116:19	dans les parvis de la maison de YHWH, au milieu de toi, Yeroushalaim ! Allélou-Yah !

## Chapitre 117

### Toutes les nations louent YHWH

117:1	Toutes les nations, louez YHWH ! Tous les peuples, célébrez-le !
117:2	Car sa miséricorde est grande envers nous, et sa fidélité dure pour toujours. Louez Yah !

## Chapitre 118

### YHWH, l'Elohîm de mon secours

118:1	Célébrez YHWH, car il est bon, parce que sa bonté est pour toujours !
118:2	Qu'Israël dise maintenant : Car sa bonté est pour toujours !
118:3	Que la maison d'Aaron dise maintenant : Car sa bonté est pour toujours !
118:4	Que ceux qui craignent YHWH disent maintenant : Car sa bonté est pour toujours !
118:5	Dans ma détresse j'ai invoqué Yah<!--Ps. 120:1.--> : Yah m'a répondu et m'a mis au large.
118:6	YHWH est pour moi, je n'aurai pas peur. Que peut me faire un être humain<!--Hé. 13:6.--> ?
118:7	YHWH est pour moi, il vient à mon secours. C'est pourquoi je regarde fixement ceux qui me haïssent.
118:8	Mieux vaut mettre sa confiance en YHWH que de se confier aux humains<!--Es. 2:22 ; Jé. 17:5 ; Ps. 62:9.-->.
118:9	Mieux vaut mettre sa confiance en YHWH que de se confier aux nobles.
118:10	Toutes les nations m'avaient environné, mais au Nom de YHWH je les taille en pièces.
118:11	Elles m'avaient environné, elles m'avaient environné, au Nom de YHWH je les taille en pièces.
118:12	Elles m'avaient environné comme des abeilles, elles s'éteignent comme un feu d'épines<!--De. 1:44.-->, car au Nom de YHWH je les taille en pièces.
118:13	Tu me poussais, tu me poussais pour me faire tomber, mais YHWH m'a secouru.
118:14	Yah est ma force et ma musique<!--« Chant de louange », « chant », « musique », « mélodie ». Ps. 119:54 ; Ex. 15:2 ; Es. 12:2.-->, il est devenu mon salut<!--Ex. 15:2 ; Es. 12:2.-->.
118:15	La voix du cri de joie et du salut est dans les tentes des justes : la droite de YHWH agit avec puissance !
118:16	La droite de YHWH est élevée ! La droite de YHWH agit avec puissance !
118:17	Je ne mourrai pas, je vivrai et je raconterai les œuvres de Yah.
118:18	Yah m'a châtié, il m'a châtié mais il ne m'a pas livré à la mort.
118:19	Ouvrez-moi les portes de la justice, j'y entrerai et je célébrerai Yah !
118:20	C'est ici la porte de YHWH, les justes y entreront.
118:21	Je te célébrerai, car tu m'as répondu et tu es devenu mon salut<!--Ex. 15:2.-->.
118:22	La pierre que les bâtisseurs avaient rejetée est devenue la tête de l'angle<!--Le Mashiah est présenté comme la pierre ou le rocher (cp. Es. 8:13-17 ; 1 Pi. 2:7).-->.
118:23	Ceci a été fait par YHWH, c'est une chose merveilleuse à nos yeux.
118:24	C'est le jour que YHWH a fait, nous nous réjouirons et nous serons dans l'allégresse en lui.
118:25	Oh ! YHWH, sauve, s’il te plaît ! Oh ! YHWH, fais réussir, s’il te plaît !
118:26	Béni soit celui qui vient<!--Mt. 11:3, 21:9, 23:39 ; Lu. 13:35 ; Jn. 12:13.--> au Nom de YHWH ! Nous vous bénissons de la maison de YHWH.
118:27	YHWH El est notre lumière<!--Jn. 8:12.--> ! Attachez la victime de la fête avec des cordes, jusqu'aux cornes de l'autel !
118:28	Tu es mon El, c'est pourquoi je te célébrerai. Tu es mon Elohîm, je t'exalterai.
118:29	Célébrez YHWH car il est bon, car sa bonté est pour toujours !

## Chapitre 119

### La parole de YHWH éclaire

119:1	[Aleph.] Heureux ceux qui sont intègres dans leur voie, qui marchent selon la torah de YHWH.
119:2	Heureux ceux qui gardent ses témoignages et qui le cherchent de tout leur cœur<!--Jos. 1:8.--> ;
119:3	qui ne commettent pas d'injustice, qui marchent dans ses voies<!--1 Jn. 3:9, 5:18.-->.
119:4	Tu as donné tes préceptes afin qu'on les garde soigneusement.
119:5	Oh ! que mes voies soient stables pour garder tes statuts !
119:6	Alors je ne serai pas honteux en regardant tous tes commandements.
119:7	Je te célébrerai dans la droiture de mon cœur quand j'aurai appris les ordonnances de ta justice.
119:8	Je veux garder tes statuts, ne m'abandonne pas entièrement !
119:9	[Beth.] Comment le jeune homme rendra-t-il pure sa voie ? En observant ta parole.
119:10	Je te recherche de tout mon cœur, ne me laisse pas m'égarer loin de tes commandements !
119:11	Je garde ta parole cachée dans mon cœur afin de ne pas pécher contre toi.
119:12	YHWH ! tu es béni, enseigne-moi tes statuts !
119:13	De mes lèvres je raconte toutes les ordonnances de ta bouche.
119:14	Je me réjouis dans le chemin de tes préceptes comme si je possédais toutes les richesses du monde.
119:15	Je médite tes préceptes et j'observe tes voies.
119:16	Je prends plaisir à tes statuts et je n'oublie pas tes paroles.
119:17	[Guimel.] Fais du bien à ton serviteur afin que je vive, et je garderai ta parole<!--Ps. 116:7.-->.
119:18	Ouvre mes yeux afin que je regarde aux merveilles de ta torah<!--Ep. 1:18.--> !
119:19	Je suis un étranger sur la Terre, ne me cache pas tes commandements.
119:20	Mon âme est brisée par le désir qu’elle a en tout temps pour tes ordonnances.
119:21	Tu réprimandes les orgueilleux, ces maudits, qui se détournent de tes commandements.
119:22	Roule de dessus moi l'insulte et le mépris, car j'ai gardé tes témoignages<!--Ps. 3:9.-->.
119:23	Même les princes s'assoient et parlent contre moi pendant que ton serviteur médite tes statuts.
119:24	Tes témoignages font mes délices, ce sont les hommes de mon conseil.
119:25	[Daleth.] Mon âme est attachée à la poussière, fais-moi revivre selon ta parole<!--Ps. 44:26, 143:11.-->.
119:26	Je te raconte mes voies et tu me réponds. Enseigne-moi tes statuts !
119:27	Fais-moi entendre la voie de tes préceptes, et je parlerai de tes merveilles<!--Ps. 145:6.-->.
119:28	Mon âme pleure de chagrin, relève-moi selon tes paroles.
119:29	Éloigne de moi la voie du mensonge et accorde-moi la faveur de ta torah.
119:30	Je choisis la voie de la vérité et je place tes ordonnances sous mes yeux.
119:31	Je m'attache à tes préceptes, YHWH ! Ne me rends pas honteux !
119:32	Je courrai dans la voie de tes commandements, quand tu auras mis mon cœur au large.
119:33	[He.] YHWH, enseigne-moi la voie de tes statuts, et je la garderai jusqu'au bout.
119:34	Donne-moi de l'intelligence, je garderai ta torah et je l'observerai de tout mon cœur<!--Pr. 2:6 ; Ja. 1:5.--> !
119:35	Fais-moi marcher sur le sentier de tes commandements car j'y prends plaisir.
119:36	Incline mon cœur à tes préceptes et non pas au profit<!--Ez. 33:31 ; Mc. 7:21-22 ; Hé. 13:5.-->.
119:37	Fais passer mes yeux pour ne pas voir la vanité, fais-moi vivre dans ta voie !
119:38	Accomplis ta parole envers ton serviteur, afin qu'on te craigne.
119:39	Éloigne de moi l'insulte que je redoute, car tes ordonnances sont bonnes.
119:40	Voici, je désire ardemment pratiquer tes préceptes, fais-moi vivre dans ta justice.
119:41	[Vav.] Que ta miséricorde vienne sur moi, YHWH ! et ta délivrance aussi, selon ta promesse !
119:42	Pour que je réponde par une parole à celui qui m’outrage, car je me confie en ta parole.
119:43	N’arrache pas hors de ma bouche la parole de vérité<!--Es. 59:15 ; Jé. 7:28.-->, car j'espère beaucoup en tes jugements.
119:44	Je garderai continuellement ta torah, pour toujours et à perpétuité.
119:45	Je marcherai au large parce que je recherche tes préceptes.
119:46	Je parlerai de tes témoignages devant les rois et je ne rougirai pas de honte<!--Ps. 138:1-4 ; Mt. 10:18-19 ; Ac. 26.-->.
119:47	Je fais mes délices de tes commandements que j'aime.
119:48	J'étends mes paumes vers tes commandements que j'aime, et je médite tes statuts.
119:49	[Zayin.] Souviens-toi de la parole donnée à ton serviteur, sur laquelle tu m'as fait espérer.
119:50	C'est ici ma consolation dans mon affliction, car ta parole me fait vivre.
119:51	Les orgueilleux se sont beaucoup moqués de moi : je ne me suis pas détourné de ta torah.
119:52	YHWH, je me souviens de tes jugements anciens et je me suis consolé en eux.
119:53	Une chaleur brûlante me saisit à cause des méchants qui abandonnent ta torah.
119:54	Tes statuts sont devenus pour moi des chants<!--Ps. 118:14.--> dans la maison où je suis étranger.
119:55	YHWH, je me souviens de ton Nom pendant la nuit et je garde ta torah.
119:56	Cela m'arrive parce que je garde tes préceptes.
119:57	[Heth.] Ma part, YHWH, je le dis, c'est de garder tes paroles.
119:58	Je supplie tes faces de tout mon cœur : Aie pitié de moi selon ta parole.
119:59	Je fais le compte de mes voies et je rebrousse chemin vers tes témoignages<!--Os. 6:3 ; La. 3:40.-->.
119:60	Je me hâte, et je ne tarde pas à garder tes commandements.
119:61	Les cordes des méchants m'entourent, je n'oublie pas ta torah.
119:62	Je me lève au milieu de la nuit pour te célébrer à cause des ordonnances de ta justice.
119:63	Je suis l'ami de tous ceux qui te craignent et qui gardent tes préceptes.
119:64	YHWH, la Terre est pleine de ta bonté, enseigne-moi tes statuts.
119:65	[Teth.] YHWH, tu fais du bien à ton serviteur selon ta parole.
119:66	Enseigne-moi le bon sens et la connaissance car je crois à tes commandements.
119:67	Avant d'avoir été humilié, je m'égarais, mais maintenant j'observe ta parole.
119:68	Tu es bon et plaisant, enseigne-moi tes statuts.
119:69	Les orgueilleux plâtrent contre moi le mensonge, moi, de tout cœur, je garde tes préceptes.
119:70	Leur cœur est insensible comme la graisse, mais moi, je prends plaisir dans ta torah<!--De. 32:15 ; Jé. 5:28.-->.
119:71	Il est bon que je sois humilié afin que j'apprenne tes statuts.
119:72	La torah de ta bouche est meilleure pour moi que des milliers d'or et d'argent.
119:73	[Yod.] Tes mains m'ont façonné, elles m'ont formé<!--Jé. 1:5 ; Job 10:9.-->. Donne-moi l'intelligence afin que j'apprenne tes commandements.
119:74	Ceux qui te craignent me verront et se réjouiront, parce que j'espère en tes promesses.
119:75	Je sais, YHWH, que tes jugements sont justes, et que tu m'as humilié par ta fidélité<!--Hé. 12:10.-->.
119:76	Que ta bonté soit ma consolation, s’il te plaît, comme tu l'as promis à ton serviteur !
119:77	Que tes compassions viennent sur moi et je vivrai, car ta torah fait mes délices !
119:78	Que les orgueilleux rougissent de honte, de ce qu'ils m'oppriment sans cause ! Mais moi, je médite sur tes préceptes.
119:79	Que ceux qui te craignent et ceux qui connaissent tes témoignages reviennent vers moi !
119:80	Que mon cœur soit intègre dans tes statuts afin que je ne sois pas couvert de honte !
119:81	[Kaf.] Mon âme se consume après ton salut, j'espère en ta parole.
119:82	Mes yeux se consument après ton discours, disant : Quand me consoleras-tu ?
119:83	Car je suis comme une outre dans la fumée épaisse, je n'oublie pas tes statuts.
119:84	Quel est le nombre de jours de ton serviteur ? Quand jugeras-tu ceux qui me poursuivent<!--Ap. 6:10.--> ?
119:85	Les orgueilleux creusent pour moi des fosses, ce qui n’est pas selon ta torah.
119:86	Tous tes commandements ne sont que fidélité. On me persécute sans cause, aide-moi<!--Mt. 5:10.--> !
119:87	Encore un peu, ils m'auraient exterminé sur la Terre, mais je n'ai pas abandonné tes préceptes.
119:88	Fais-moi vivre selon ta miséricorde et je garderai les préceptes de ta bouche.
119:89	[Lamed.] YHWH ! ta parole est établie pour toujours dans les cieux.
119:90	Ta fidélité dure d'âges en âges : tu as affermi la Terre, et elle tient<!--Ec. 1:4.-->.
119:91	Selon tes jugements, tout tient jusqu'à ce jour, car toutes choses te servent.
119:92	Si ta torah n'avait pas fait mes délices, j'aurais déjà péri dans mon affliction.
119:93	Je n'oublierai jamais tes préceptes car c'est par eux que tu m'as fait vivre.
119:94	Je suis à toi : sauve-moi ! Car je recherche tes préceptes.
119:95	Les méchants m'attendent pour me faire périr, je suis attentif à tes témoignages.
119:96	J'ai vu une fin à toute perfection, mais ton commandement est extrêmement vaste.
119:97	[Mem.] Combien j'aime ta torah<!--Ps. 1:2.--> ! Elle est tout le jour ma méditation.
119:98	Par tes commandements, tu m'as rendu plus sage que mes ennemis, parce que tes commandements sont toujours avec moi.
119:99	Je suis plus prudent que tous ceux qui m'avaient enseigné, parce que tes préceptes sont ma méditation.
119:100	Je suis devenu plus intelligent que les vieillards parce que j'observe tes préceptes.
119:101	Je garde mes pieds de toute mauvaise voie afin d'observer ta parole.
119:102	Je ne me suis pas détourné de tes ordonnances parce que tu me les enseignes.
119:103	Que ta parole est douce à mon palais ! plus douce que le miel à ma bouche.
119:104	Je suis devenu intelligent par tes préceptes, c'est pourquoi je hais toute voie de mensonge.
119:105	[Noun.] Ta parole est une lampe à mes pieds et une lumière sur mon sentier<!--Pr. 6:23 ; 2 Pi. 1:19.-->.
119:106	J'ai juré et je l'accomplirai : j'observerai les lois de ta justice<!--Né. 10:30.-->.
119:107	YHWH, je suis extrêmement affligé, fais-moi vivre selon ta parole.
119:108	YHWH, s'il te plaît, agrée les offrandes volontaires de ma bouche, et enseigne-moi tes ordonnances<!--Os. 14:3 ; Hé. 13:15.-->.
119:109	Mon âme est continuellement dans ma paume, toutefois je n'oublie pas ta torah.
119:110	Les méchants m'ont tendu des pièges, toutefois je ne me suis pas égaré de tes préceptes.
119:111	J'ai pris pour héritage perpétuel tes préceptes car ils sont la joie de mon cœur.
119:112	J'ai incliné mon cœur à accomplir toujours tes statuts jusqu'à la fin.
119:113	[Samech.] Je hais les hommes au cœur partagé<!--1 R. 18:21 ; Ja. 1:6, 4:8.-->, mais j'aime ta torah.
119:114	Tu es ma couverture<!--Ps. 32:7, 91:1.--> et mon bouclier, je m'attends à ta parole.
119:115	Méchants, retirez-vous de moi<!--Ps. 6:9 ; Mt. 7:23.--> ! et je garderai les commandements de mon Elohîm.
119:116	Soutiens-moi selon ta parole et je vivrai. Ne me rends pas honteux dans mon espérance !
119:117	Soutiens-moi et je serai sauvé, j'aurai continuellement les yeux sur tes statuts.
119:118	Tu rejettes tous ceux qui s’égarent de tes statuts, car leur tromperie n’est que mensonge.
119:119	Tu enlèves comme des scories tous les méchants de la Terre, c'est pourquoi j'aime tes témoignages.
119:120	Ma chair frissonne de la terreur que j'ai de toi, je crains tes jugements<!--Ha. 3:16.-->.
119:121	[Ayin.] J'ai exercé le jugement et la justice. Ne m'abandonne pas à ceux qui m'oppriment !
119:122	Garantis le bonheur de ton serviteur ! Que les orgueilleux ne m'oppriment pas.
119:123	Mes yeux se consument après ton salut et la parole de ta justice.
119:124	Agis envers ton serviteur selon ta miséricorde et enseigne-moi tes statuts.
119:125	Je suis ton serviteur, donne-moi l'intelligence, et je connaîtrai tes témoignages<!--Pr. 1:4, 6:23.-->.
119:126	Il est temps que YHWH agisse : on a violé ta torah.
119:127	C'est pourquoi j'aime tes commandements, plus que l'or et l'or fin.
119:128	C'est pourquoi je trouve justes tous tes préceptes, je hais toute voie de mensonge.
119:129	[Pe.] Tes préceptes sont merveilleux, c'est pourquoi mon âme les garde.
119:130	L'ouverture<!--Vient de l'hébreu « pethach » qui signifie aussi « entrée », « encadrement d'une porte ».--> de tes paroles apporte la lumière, elle donne de l'intelligence aux stupides.
119:131	J'ouvre grandement ma bouche et je soupire, car je désire tes commandements.
119:132	Regarde-moi, et aie pitié de moi, selon tes jugements à l'égard de ceux qui aiment ton Nom.
119:133	Affermis mes pas sur ta parole, que toute méchanceté ne domine sur moi.
119:134	Délivre-moi de l'oppression des hommes afin que je garde tes préceptes.
119:135	Fais luire tes faces sur ton serviteur et enseigne-moi tes statuts.
119:136	Mes yeux répandent des torrents d'eau parce qu'on n'observe pas ta torah.
119:137	[Tsade.] Tu es juste, YHWH, et droit dans tes jugements.
119:138	Tu as prescrit tes témoignages avec justice et grande fidélité.
119:139	Mon zèle me consume parce que mes adversaires oublient tes paroles.
119:140	Ta parole est entièrement éprouvée, c'est pourquoi ton serviteur l'aime.
119:141	Je suis petit et méprisé, je n'oublie pas tes préceptes.
119:142	Ta justice est une justice éternelle, et ta torah est la vérité.
119:143	La détresse et l'angoisse m'atteignent, tes commandements font mes délices.
119:144	Tes préceptes ne sont que justice éternelle, donne-moi l'intelligence afin que je vive !
119:145	[Qof.] Je crie de tout mon cœur, réponds-moi, YHWH ! Je garde tes statuts.
119:146	Je crie vers toi, sauve-moi afin que j'observe tes témoignages.
119:147	Je devance l'aurore et je crie au secours, je m'attends à ta parole.
119:148	Mes yeux ont devancé les veilles de la nuit pour méditer ta parole.
119:149	Écoute ma voix selon ta miséricorde, YHWH ! Fais-moi vivre selon ton ordonnance.
119:150	Ceux qui poursuivent la méchanceté s'approchent de moi, et ils s'éloignent de ta torah.
119:151	YHWH, tu es près de moi, et tous tes commandements ne sont que vérité.
119:152	Je sais depuis longtemps que tu as établi tes témoignages pour toujours.
119:153	[Resh.] Regarde mon affliction et sauve-moi, car je n'oublie pas ta torah.
119:154	Soutiens ma cause et rachète-moi, fais-moi vivre selon ta parole !
119:155	Le salut est loin des méchants parce qu'ils ne recherchent pas tes statuts.
119:156	Tes compassions sont en grand nombre, YHWH ! Fais-moi vivre selon tes ordonnances.
119:157	Ceux qui me persécutent et qui me pressent sont en grand nombre, je ne me détourne pas de tes préceptes.
119:158	Je vois avec dégoût les traîtres et je suis rempli de tristesse car ils n'observent pas ta parole.
119:159	Regarde combien j'aime tes préceptes, YHWH ! Fais-moi vivre selon ta miséricorde !
119:160	La tête de ta parole est la vérité, et toutes les lois de ta justice sont éternelles.
119:161	[Shin.] Les princes du peuple me persécutent sans cause, mais mon cœur tremble à cause de ta parole.
119:162	Je me réjouis de ta parole comme ferait celui qui aurait trouvé un grand butin.
119:163	Je hais le mensonge, je l'ai en abomination, mais j'aime ta torah.
119:164	Sept fois le jour je te loue à cause des ordonnances de ta justice.
119:165	Il y a une grande paix pour ceux qui aiment ta torah, et rien ne peut les renverser<!--Es. 32:17 ; Ph. 4:7.-->.
119:166	YHWH, j'espère en ton salut et je pratique tes commandements.
119:167	Mon âme observe tes témoignages, je les aime beaucoup.
119:168	J'observe tes préceptes et tes témoignages, car toutes mes voies sont devant toi.
119:169	[Tav.] YHWH, que mon cri parvienne jusqu'à toi, donne-moi l'intelligence selon ta parole.
119:170	Que ma supplication vienne devant toi, délivre-moi selon ta parole.
119:171	Mes lèvres publieront ta louange quand tu m'auras enseigné tes statuts.
119:172	Ma langue ne parlera que de ta parole, parce que tous tes commandements ne sont que justice !
119:173	Que ta main vienne me secourir, parce que j'ai choisi tes préceptes.
119:174	YHWH, je désire ardemment ton salut, et ta torah fait mes délices.
119:175	Que mon âme vive afin qu'elle te loue, et que tes ordonnances me viennent en aide !
119:176	J’erre comme une brebis qui périt<!--Es. 53:6 ; Lu. 15:4 ; 1 Pi. 2:25.--> : cherche ton serviteur, car je n'oublie pas tes commandements.

## Chapitre 120

### Cri de détresse

120:1	Cantique des marches<!--Les psaumes 120 à 134 sont appelés « psaumes des marches ou des degrés », « des montées » ou de « l'ascension ». Ces psaumes furent chantés par les Israélites montant à Yeroushalaim au retour de la captivité de Babel (Babylone).-->. J'ai invoqué YHWH dans ma grande détresse, et il m'a répondu.
120:2	YHWH, délivre mon âme des lèvres mensongères et de la langue trompeuse.
120:3	Que te donne, que te rapporte la langue trompeuse ?
120:4	Des flèches aiguisées d'un puissant, avec les charbons ardents du genêt<!--Jé. 9:2 ; Ja. 3:5-6.-->.
120:5	Malheureux que je suis de séjourner en Méshek et de demeurer aux tentes de Qedar !
120:6	Trop longtemps mon âme a demeuré auprès de ceux qui haïssent la paix !
120:7	Je suis pour la paix, mais quand j'en parle, ils sont pour la guerre.

## Chapitre 121

### YHWH ne sommeille ni ne dort

121:1	Cantique pour les marches. Je lève mes yeux vers les montagnes... D'où me viendra le secours ?
121:2	Mon secours vient de YHWH qui a fait les cieux et la Terre<!--Ps. 124:8.-->.
121:3	Il ne permettra pas que ton pied chancelle, celui qui te garde ne sommeillera pas<!--Es. 27:3 ; Pr. 3:23.-->.
121:4	Voici, il ne sommeille ni ne dort celui qui garde Israël.
121:5	YHWH est celui qui te garde, YHWH est ton ombre à ta main droite<!--Es. 25:4.-->.
121:6	Pendant le jour, le soleil ne te frappera pas, ni la lune pendant la nuit<!--Es. 49:10 ; Ap. 7:16.-->.
121:7	YHWH te gardera de tout mal, il gardera ton âme.
121:8	YHWH gardera ton départ et ton arrivée, dès maintenant et à jamais<!--De. 28:6.-->.

## Chapitre 122

### Yeroushalaim, la ville de YHWH

122:1	Cantique des marches de David. Je me réjouis à cause de ceux qui me disent : Allons à la maison de YHWH<!--Ps. 84:1-5.--> !
122:2	Nos pieds s'arrêtent dans tes portes, Yeroushalaim !
122:3	Yeroushalaim est bâtie comme une ville qui forme un ensemble bien uni,
122:4	à laquelle montent les tribus, les tribus de Yah, selon le témoignage d'Israël, pour célébrer le Nom de YHWH.
122:5	Car c'est là que sont placés les trônes pour le jugement<!--Mt. 19:28.-->, les trônes de la maison de David.
122:6	Demandez la paix de Yeroushalaim. Que ceux qui t'aiment aient du repos !
122:7	Que la paix soit dans tes murs, et la tranquillité dans tes palais !
122:8	Pour l'amour de mes frères et de mes amis, je parlerai maintenant pour ta paix.
122:9	À cause de la maison de YHWH, notre Elohîm, je fais une requête pour ton bonheur.

## Chapitre 123

### Les yeux fixés sur YHWH

123:1	Cantique des marches. Je lève mes yeux vers toi qui habites dans les cieux.
123:2	Voici, comme les yeux des serviteurs regardent la main de leur maître, comme les yeux de la servante regardent la main de sa maîtresse, ainsi nos yeux regardent à YHWH, notre Elohîm, jusqu'à ce qu'il ait pitié de nous<!--Ps. 25:15.-->.
123:3	Aie pitié de nous, YHWH ! aie pitié de nous ! car nous sommes assez rassasiés de mépris !
123:4	Notre âme est assez rassasiée des moqueries des orgueilleux, du mépris des hautains.

## Chapitre 124

### YHWH, l'Elohîm qui secourt et protège son peuple

124:1	Cantique des marches de David. Sans YHWH qui était de notre côté, qu’Israël le dise maintenant !
124:2	Sans YHWH qui était de notre côté quand les hommes s'élevèrent contre nous,
124:3	alors ils nous auraient engloutis tout vivants, quand leur colère s'enflamma contre nous.
124:4	Alors les eaux nous auraient submergés, les torrents auraient passé sur notre âme,
124:5	alors les flots impétueux auraient passé sur notre âme.
124:6	Béni soit YHWH qui ne nous a pas livrés en proie à leurs dents !
124:7	Notre âme s'est échappée comme l'oiseau du filet des oiseleurs ; le filet a été rompu, et nous nous sommes échappés<!--Pr. 6:5.-->.
124:8	Notre secours est dans le Nom de YHWH<!--Ac. 4:11-12.-->, qui a fait les cieux et la Terre.

## Chapitre 125

### YHWH entoure tous ceux qui se confient en lui

125:1	Cantique des marches. Ceux qui se confient en YHWH sont comme la montagne de Sion : elle ne chancelle pas, elle demeure pour toujours.
125:2	Comme Yeroushalaim est entourée de montagnes, de même YHWH entoure son peuple, dès maintenant et pour toujours.
125:3	Car la verge de la méchanceté ne restera pas sur le lot des justes, de peur que les justes ne tendent leurs mains vers l'injustice<!--Es. 14:5.-->.
125:4	YHWH, sois favorable avec ceux qui sont bons, ceux dont le cœur est droit.
125:5	Quant à ceux qui se détournent vers des voies tortueuses, que YHWH les fasse marcher avec ceux qui pratiquent la méchanceté<!--Mt. 7:23.--> ! La paix sera sur Israël.

## Chapitre 126

### YHWH, le libérateur

126:1	Cantique des marches. Quand YHWH ramena les captifs de Sion, ce fut pour nous comme un rêve.
126:2	Alors notre bouche était remplie de joie, et notre langue poussait des cris de joie, alors on disait parmi les nations : YHWH a fait de grandes choses pour eux !
126:3	YHWH a fait de grandes choses pour nous. Nous sommes devenus joyeux.
126:4	YHWH ! ramène nos captifs, comme des canaux dans le midi<!--Os. 6:11 ; Joë. 4:11.--> !
126:5	Ceux qui sèment avec larmes, moissonneront avec chants d'allégresse<!--Ga. 6:9.-->.
126:6	Celui qui marche en pleurant, quand il porte la semence pour la mettre en terre, revient avec des chants d'allégresse quand il porte ses gerbes.

## Chapitre 127

### YHWH, le plus grand architecte

127:1	Cantique des marches de Shelomoh. Si YHWH ne bâtit la maison, ceux qui la bâtissent travaillent en vain. Si YHWH ne garde la ville, celui qui la garde fait le guet en vain.
127:2	C'est en vain que vous vous levez de grand matin, que vous vous couchez tard, et que vous mangez le pain de douleurs. C’est ainsi qu’il donne le sommeil à son bien-aimé<!--Ez. 20:20 ; Mc. 2:27.-->.
127:3	Voici, les fils sont un héritage de YHWH, le fruit du ventre est une récompense<!--Ps. 113:9, 128:3-6.-->.
127:4	Telles sont les flèches dans la main d'un homme puissant, tels sont les fils de la jeunesse.
127:5	Heureux l'homme fort qui en a rempli son carquois ! Ils ne seront pas honteux quand ils parleront avec leurs ennemis à la porte.

## Chapitre 128

### YHWH assure la paix à celui qui le craint

128:1	Cantique des marches. Heureux tout homme qui craint YHWH, qui marche dans ses voies !
128:2	Car tu mangeras du travail de tes paumes ! Tu seras heureux et tu prospéreras<!--Es. 3:10.-->.
128:3	Ta femme sera dans ta maison comme une vigne qui porte du fruit. Tes fils seront comme des plants d'oliviers autour de ta table.
128:4	Voilà ! C’est ainsi que sera béni en effet l'homme fort qui craint YHWH.
128:5	YHWH te bénira de Sion et tu verras le bien de Yeroushalaim tous les jours de ta vie.
128:6	Tu verras les fils de tes fils. Paix sur Israël !

## Chapitre 129

### L'opprimé plus que vainqueur en YHWH

129:1	Cantique des marches. Qu'Israël dise maintenant : Ils m'ont souvent tourmenté dès ma jeunesse.
129:2	Ils m'ont beaucoup opprimé dès ma jeunesse, ils ne m'ont pas vaincu.
129:3	Des laboureurs ont labouré mon dos, ils y ont tracé de longs sillons.
129:4	YHWH est juste, il a coupé les cordes des méchants.
129:5	Ils auront honte et retourneront en arrière, tous ceux qui haïssent Sion !
129:6	Ils deviendront comme l'herbe des toits qui sèche avant qu'on l'arrache,
129:7	dont le moissonneur ne remplit pas sa paume, ni celui qui lie les gerbes, ses bras.
129:8	Les passants ne disent pas : Que la bénédiction de YHWH soit sur vous ! Nous vous bénissons au Nom de YHWH !

## Chapitre 130

### La rédemption en abondance auprès de YHWH

130:1	Cantique des marches. YHWH, je t'invoque depuis les profondeurs !
130:2	Adonaï, écoute ma voix ! Que tes oreilles soient attentives à la voix de mes supplications !
130:3	Yah ! si tu prenais garde aux iniquités, Adonaï, qui pourrait tenir debout ?
130:4	Mais le pardon se trouve auprès de toi, afin qu'on te craigne<!--Mt. 26:28 ; Ro. 3:24 ; Col. 1:12-14.-->.
130:5	J'espère en YHWH, mon âme espère, et j'attends sa parole.
130:6	Mon âme attend Adonaï, plus que les sentinelles le matin, plus que les sentinelles le matin.
130:7	Israël, attends-toi à YHWH, car YHWH est miséricordieux et la rançon<!--1 Ti. 2:6.--> est auprès de lui en abondance.
130:8	C'est lui qui rachètera Israël de toutes ses iniquités.

## Chapitre 131

### Mettre son espoir en YHWH seul

131:1	Cantique des marches de David. YHWH ! je n'ai ni un cœur qui s'élève ni un regard hautain<!--Pr. 6:17, 16:5.-->, je ne m'occupe pas de choses trop grandes et trop extraordinaires pour moi.
131:2	N'ai-je pas soumis et fait taire mon âme, comme un enfant sevré auprès de sa mère ? Mon âme est en moi comme un enfant sevré.
131:3	Israël attends-toi à YHWH dès maintenant et pour toujours !

## Chapitre 132

### Sion, le trône de YHWH

132:1	Cantique des marches. YHWH ! souviens-toi de David et de toute son affliction !
132:2	Il a juré à YHWH et fait ce vœu au puissant de Yaacov :
132:3	Je n'entrerai pas dans la tente où j'habite, je ne monterai pas sur le lit où je couche,
132:4	je n’accorderai pas de sommeil à mes yeux, d’assoupissement à mes paupières,
132:5	jusqu'à ce que j'aie trouvé un lieu pour YHWH, un tabernacle pour le puissant de Yaacov<!--1 Ch. 15:1.--> !
132:6	Voici, nous l’avons entendu en Éphrata, nous l'avons trouvé dans les champs de Yaar<!--Ou « Champs-de-la-Forêt ».-->.
132:7	Entrons dans son tabernacle, prosternons-nous devant le marchepied de ses pieds.
132:8	Lève-toi, YHWH, dans ton lieu de repos, toi et l'arche de ta force<!--No. 10:35-36 ; 2 Ch. 6:41.-->.
132:9	Que tes prêtres soient revêtus de justice, et que tes bien-aimés chantent de joie<!--Es. 11:5 ; Ap. 19:8.--> !
132:10	Pour l'amour de David, ton serviteur, ne repousse pas les faces de ton mashiah !
132:11	YHWH a juré la vérité à David, et il n’en reviendra pas : Je mettrai le fruit de tes entrailles<!--2 S. 7:12 ; 1 R. 8:25 ; 2 Ch. 6:16 ; Lu. 1:69 ; Ac. 2:30.--> sur ton trône.
132:12	Si tes fils gardent mon alliance et mes témoignages que je leur enseignerai, leurs fils aussi seront assis à perpétuité sur ton trône.
132:13	Car YHWH a choisi Sion, il l'a préférée pour être son trône.
132:14	Elle est mon lieu de repos à perpétuité, j'y habiterai parce que je l'ai désirée.
132:15	Je bénirai, je bénirai sa nourriture, je rassasierai de pain ses pauvres.
132:16	Je revêtirai de salut ses prêtres, et ses bien-aimés chanteront avec des cris de joie.
132:17	Je ferai germer en elle une corne à David, je préparerai une lampe à mon mashiah,
132:18	je revêtirai de honte ses ennemis, et sur lui fleurira son diadème.

## Chapitre 133

### La bénédiction dans la communion fraternelle

133:1	Cantique des marches de David. Voici, qu’il est bon, qu’il est agréable pour des frères de demeurer unis ensemble<!--Ac. 2:46 ; Hé. 13:1.--> !
133:2	C'est comme l’huile excellente répandue sur la tête qui coule sur la barbe, sur la barbe d'Aaron<!--Ex. 30:22-30.-->, sur le bord de ses vêtements.
133:3	C'est comme la rosée de l'Hermon qui descend sur les montagnes de Sion. Car c'est là que YHWH a ordonné la bénédiction et la vie, pour toujours.

## Chapitre 134

### Bénissez YHWH, vous tous ses serviteurs

134:1	Cantique des marches. Voici, bénissez YHWH, vous tous, serviteurs de YHWH, qui vous tenez debout durant les nuits dans la maison de YHWH !
134:2	Élevez vos mains vers le lieu saint, et bénissez YHWH !
134:3	Que YHWH, qui a fait les cieux et la Terre, te bénisse de Sion !

## Chapitre 135

### La souveraineté d'Elohîm

135:1	Allélou-Yah, vous serviteurs de YHWH ! Louez-le !
135:2	Vous qui vous tenez debout dans la maison de YHWH, dans les parvis de la maison de notre Elohîm !
135:3	Allélou-Yah ! car YHWH est bon ! Chantez son Nom, car il est agréable !
135:4	Oui, Yah s'est choisi Yaacov et Israël pour sa propriété<!--Ex. 19:5 ; De. 7:6 ; Tit. 2:14 ; 1 Pi. 2:9.-->.
135:5	Oui, je sais que YHWH est grand et que notre Adonaï est au-dessus de tous les elohîm.
135:6	YHWH fait tout ce qu'il lui plaît, dans les cieux et sur la Terre, dans la mer et dans tous les abîmes.
135:7	C'est lui qui fait monter les vapeurs des extrémités de la Terre, il fait les éclairs et la pluie, il tire le vent hors de ses trésors.
135:8	C'est lui qui a frappé les premiers-nés d'Égypte, tant des hommes que des bêtes,
135:9	qui a envoyé des prodiges et des miracles au milieu de toi, Égypte ! Contre pharaon et contre tous ses serviteurs,
135:10	qui a frappé des nations nombreuses et tué des rois puissants :
135:11	Sihon, roi des Amoréens, et Og, roi de Bashân, et tous les royaumes de Kena'ân<!--No. 21:33-35 ; De. 3:11.-->.
135:12	Il a donné leur terre en héritage, en héritage à Israël, son peuple.
135:13	YHWH, ton Nom est pour toujours ! YHWH, ta mémoire d'âge en âge !
135:14	Oui, YHWH jugera son peuple et se repentira à l'égard de ses serviteurs.
135:15	Les idoles des nations ne sont que de l'or et de l'argent, un ouvrage de mains d’humain.
135:16	Elles ont une bouche et ne parlent pas, elles ont des yeux et ne voient pas,
135:17	elles ont des oreilles et n'entendent pas, il n'y a pas de souffle dans leur bouche.
135:18	Ceux qui les fabriquent, tous ceux qui se confient en elles, deviennent comme elles.
135:19	Maison d'Israël, bénissez YHWH ! Maison d'Aaron, bénissez YHWH !
135:20	Maison des Lévites, bénissez YHWH ! Vous qui craignez YHWH, bénissez YHWH !
135:21	Béni soit de Sion, YHWH qui habite dans Yeroushalaim ! Allélou-Yah !

## Chapitre 136

### La bonté de YHWH demeure pour toujours

136:1	Célébrez YHWH, car il est bon, car sa bonté est éternelle !
136:2	Célébrez l'Elohîm des elohîm, car sa bonté est éternelle !
136:3	Célébrez le Seigneur des seigneurs, car sa bonté est éternelle !
136:4	Célébrez celui qui seul fait de grandes merveilles, car sa bonté est éternelle !
136:5	Celui qui a fait avec intelligence les cieux, car sa bonté est éternelle !
136:6	Celui qui a étendu la terre sur les eaux, car sa bonté est éternelle !
136:7	Celui qui a fait les grands luminaires, car sa bonté est éternelle !
136:8	Le soleil pour dominer sur le jour, car sa bonté est éternelle !
136:9	La lune et les étoiles pour dominer la nuit, car sa bonté est éternelle !
136:10	Celui qui a frappé l'Égypte dans ses premiers-nés, car sa bonté est éternelle !
136:11	Qui a fait sortir Israël du milieu d'eux, car sa bonté est éternelle !
136:12	Et cela avec main forte et bras étendu, car sa bonté est éternelle !
136:13	Il a fendu la Mer Rouge en deux, car sa bonté est éternelle !
136:14	Il a fait passer Israël par le milieu d'elle, car sa bonté est éternelle !
136:15	Il a renversé pharaon et son armée dans la Mer Rouge, car sa bonté est éternelle !
136:16	Il a conduit son peuple dans le désert, car sa bonté est éternelle !
136:17	Il a frappé de grands rois, car sa bonté est éternelle !
136:18	Il a tué des grands rois, car sa bonté est éternelle !
136:19	Sihon, roi des Amoréens, car sa bonté est éternelle !
136:20	Og, roi de Bashân, car sa bonté est éternelle !
136:21	Il a donné leur terre en héritage, car sa bonté est éternelle<!--Jos. 12:7.--> !
136:22	En héritage à Israël, son serviteur, car sa bonté est éternelle !
136:23	Et qui, lorsque nous étions humiliés, s'est souvenu de nous, car sa bonté est éternelle !
136:24	Il nous a délivrés de nos adversaires, car sa bonté est éternelle !
136:25	Il donne la nourriture à toute chair, car sa bonté est éternelle<!--Ps. 104:21, 147:9 ; Mt. 6:26.--> !
136:26	Célébrez le El des cieux, car sa bonté est éternelle !

## Chapitre 137

### Le cœur des captifs

137:1	Sur les bords des fleuves de Babel, nous étions assis et nous pleurions en nous souvenant de Sion.
137:2	Nous avions suspendu nos harpes au milieu des saules.
137:3	Là, ceux qui nous avaient emmenés en captivité, nous ont demandé des paroles de chants, et nos oppresseurs de la joie : Chantez-nous quelques cantiques de Sion !
137:4	Comment chanterions-nous les cantiques de YHWH sur un sol étranger ?
137:5	Si je t'oublie, Yeroushalaim, que ma droite s'oublie elle-même !
137:6	Que ma langue soit attachée à mon palais<!--Ez. 3:26.-->, si je ne me souviens pas de toi, si je n’élève Yeroushalaim en tête de ma joie !
137:7	YHWH, souviens-toi des fils d'Édom, qui, au jour de Yeroushalaim, disaient : Rasez, rasez jusqu'à ses fondements<!--Jé. 25:15-21, 49:7-8 ; Ez. 25:12 ; La. 4:21 ; Am. 1:11.--> !
137:8	Fille de Babel, la dévastée, heureux qui te rendra ce que tu nous as fait<!--Jé. 50:15-29 ; Ap. 18:6.--> !
137:9	Heureux qui saisira tes petits enfants et les écrasera contre le rocher<!--Es. 13:16.--> !

## Chapitre 138

### La renommée de YHWH dans les nations

138:1	De David. Je te célèbre de tout mon cœur, je te chante des louanges en face des elohîm.
138:2	Je me prosterne dans le palais de ta sainteté et je célèbre ton Nom, à cause de ta bonté et de ta vérité, car tu as glorifié ta parole au-dessus de tout nom.
138:3	Le jour où je t'ai invoqué, tu m'as répondu, tu m’as rassuré, la force est dans mon âme.
138:4	YHWH ! Tous les rois de la Terre te célèbrent, quand ils entendent les paroles de ta bouche.
138:5	Ils chantent les voies de YHWH, car la gloire de YHWH est grande.
138:6	Car YHWH est haut élevé, il voit les humbles et il reconnaît de loin les orgueilleux.
138:7	Quand je marche au milieu de l'adversité, tu me fais vivre, tu étends ta main sur la colère de mes ennemis, ta droite me sauve.
138:8	YHWH achèvera ce qui me concerne. YHWH, ta bonté demeure toujours : tu n'abandonnes pas l'œuvre de tes mains<!--Ph. 1:6.-->.

## Chapitre 139

### L'omniscience de YHWH

139:1	Au chef. Psaume de David. YHWH, tu me sondes et tu me connais<!--Jé. 12:3 ; Ps. 17:3.-->.
139:2	Tu sais quand je m'assieds et quand je me lève, tu discernes de loin ma pensée.
139:3	Tu me cribles sur le chemin et quand je me couche, tu connais intimement toutes mes voies.
139:4	Car la parole n’est pas encore sur ma langue, que voici, YHWH, tu la connais entièrement.
139:5	Derrière et devant, tu m'assièges, tu poses sur moi ta paume.
139:6	Ta connaissance est trop merveilleuse pour moi, elle est si haute que je ne peux l'atteindre<!--Ps. 92:6 ; Job 42:3 ; Ro. 11:33.-->.
139:7	Où irai-je loin de ton Esprit, où fuirai-je loin de tes faces<!--Jé. 23:24 ; Am. 9:2-4 ; Jon. 1:3.--> ?
139:8	Si je monte aux cieux, tu y es ! Si je me couche dans le shéol, t'y voilà !
139:9	Si je prends les ailes de l'aurore, et que je demeure à l'extrémité de la mer,
139:10	là aussi ta main me conduira, et ta droite me saisira.
139:11	Si je dis : En effet les ténèbres m'écraseront, la nuit même devient lumière autour de moi !
139:12	Même les ténèbres ne sont pas sombres pour toi : la nuit brille comme le jour, et l'obscurité comme la lumière.
139:13	Oui, toi, tu as acheté<!--1 Co. 7:23.--> mes reins, tu m'as enfermé dans le ventre de ma mère.
139:14	Je te loue, parce que je suis respecté et distingué. Tes œuvres sont merveilleuses, et mon âme le sait très bien.
139:15	Mon corps n'était pas caché devant toi, lorsque j'ai été fait dans un lieu secret et brodé dans les parties inférieures de la Terre<!--Ps. 119:73 ; Ec. 11:5.-->.
139:16	Tes yeux me voyaient quand je n'étais qu'un embryon, et sur ton livre les jours sont tous inscrits, avant qu’un seul ne soit formé<!--Ph. 4:3 ; Ap. 3:5, 20:15.-->.
139:17	C'est pourquoi, El ! que tes pensées me sont précieuses ! Que le nombre en est grand !
139:18	Si je les compte, elles sont plus nombreuses que les grains de sable. Je m'éveille, et je suis encore avec toi.
139:19	Éloah ! ne tueras-tu pas le méchant ? C'est pourquoi, hommes de sang, retirez-vous loin de moi !
139:20	Ils parlent de toi selon leur complot, tes adversaires se chargent en vain.
139:21	YHWH, n'aurais-je pas de la haine pour ceux qui te haïssent, et ne serais-je pas irrité contre ceux qui s'élèvent contre toi ?
139:22	Je les hais d'une haine extrême, ils sont devenus mes propres ennemis.
139:23	El ! sonde-moi et considère mon cœur ! Éprouve-moi et considère mes discours !
139:24	Regarde si la voie de l’idole se trouve en moi, conduis-moi sur la voie<!--Jn. 14:6.--> d'éternité !

## Chapitre 140

### YHWH, le protecteur

140:1	Au chef. Psaume de David.
140:2	YHWH, Arrache-moi à l'homme méchant, garde-moi de l'homme violent.
140:3	Ils méditent des méchancetés dans leur cœur, tous les jours ils complotent des guerres !
140:4	Ils aiguisent leur langue comme un serpent, il y a du venin de vipère sous leurs lèvres. Sélah.
140:5	YHWH, garde-moi de la main du méchant ! Préserve-moi de l'homme violent, de ceux qui méditent de faire chanceler mes pas !
140:6	Des orgueilleux ont dissimulé des pièges et des cordes devant moi, ils ont étendu un filet au bord de la piste, ils ont posé des pièges contre moi. Sélah.
140:7	Je dis à YHWH : Tu es mon El, YHWH, prête l'oreille à la voix de mes supplications !
140:8	YHWH Adonaï, la force de mon salut ! Tu couvres ma tête au jour de la bataille.
140:9	YHWH, n'accorde pas au méchant ses désirs, ne fais pas réussir ses méchants desseins, il s'élèverait. Sélah.
140:10	Quant aux têtes de ceux qui m’entourent, que le malheur de leurs lèvres les couvre !
140:11	Que des charbons ardents tombent sur eux ! Qu'ils soient jetés dans le feu, dans une fosse d'eau d'où ils ne se relèvent plus<!--Pr. 25:21-22 ; Ro. 12:20.--> !
140:12	Que l’homme de langue<!--Un calomniateur.--> ne soit jamais établi sur la Terre ! Que l'homme violent soit chassé et poussé par le mal !
140:13	Je sais que YHWH s'occupera de la cause du pauvre, du jugement des indigents.
140:14	En effet, les justes célébreront ton Nom, ceux qui sont droits habiteront devant tes faces !

## Chapitre 141

### YHWH, garde-moi du mal !

141:1	Psaume de David. YHWH, je t'invoque, hâte-toi vers moi ! Prête l'oreille à ma voix, lorsque je crie à toi !
141:2	Que ma prière te soit agréable comme l'encens, et l'élévation de mes paumes comme l'offrande du soir<!--Ex. 30:1 ; Ap. 5:8, 8:3.--> !
141:3	YHWH, mets une garde à ma bouche, garde l'entrée de mes lèvres !
141:4	N'incline pas mon cœur vers des choses mauvaises. Que je ne me livre pas à des méchantes actions par malice avec les hommes qui font le mal ! Que je ne mange pas de leurs délices !
141:5	Que le juste me frappe, c'est de la bonté. Qu'il me réprimande, c'est de l'huile sur ma tête<!--Pr. 27:6 ; Ec. 7:5.--> : Ma tête ne la rejettera pas, car je continuerai de prier contre le mal.
141:6	Quand leurs juges seront précipités aux flancs des rochers, on écoutera mes paroles, car elles sont belles.
141:7	Nos os sont dispersés dans la bouche du shéol comme quand on laboure la terre et on fend le bois.
141:8	Car c'est sur toi, YHWH Adonaï, que sont mes yeux, c'est en toi que je cherche un refuge : n'abandonne pas mon âme !
141:9	Garde-moi du pouvoir de piège qu'ils m'ont tendu et des appâts de ceux qui font le mal.
141:10	Que les méchants tombent ensemble dans leurs filets, jusqu'à ce que je sois passé !

## Chapitre 142

### YHWH, mon refuge

142:1	Poésie de David. Prière qu'il fit lorsqu'il était dans la caverne<!--1 S. 24:4.-->.
142:2	Je crie de ma voix à YHWH, je supplie de ma voix YHWH.
142:3	Je répands devant lui ma complainte, je déclare mon angoisse devant lui<!--1 S. 1:15 ; La. 2:19.-->.
142:4	Quand mon esprit est abattu en moi, toi, tu connais mon sentier. Sur le chemin où je marche, ils m’ont tendu un piège.
142:5	Je contemple à ma droite, et je regarde ! Et il n'y a personne qui me reconnaît : tout refuge s'évanouit devant moi, il n'y a personne qui prend soin de mon âme.
142:6	YHWH, je crie vers toi. Je dis : Tu es mon refuge, ma part sur la Terre des vivants.
142:7	Sois attentif à mon cri car je suis devenu très affaibli. Délivre-moi de ceux qui me poursuivent car ils sont plus puissants que moi.
142:8	Fais sortir mon âme du cachot et je célébrerai ton Nom ! Les justes m'environnent, parce que tu m'auras amené à maturité.

## Chapitre 143

### YHWH, enseigne-moi à faire ta volonté

143:1	Psaume de David. YHWH, écoute ma prière, prête l'oreille à mes supplications ! Réponds-moi dans ta fidélité, dans ta justice !
143:2	N'entre pas en jugement avec ton serviteur ! Car aucun vivant n'est juste devant toi.
143:3	Car l'ennemi poursuit mon âme, il foule ma vie par terre ; il me fait habiter dans les ténèbres comme ceux qui sont morts depuis longtemps.
143:4	Et mon esprit est abattu au-dedans de moi, mon cœur est épouvanté en mon sein.
143:5	Je me souviens des jours anciens, je médite sur toutes tes œuvres, je médite sur l'ouvrage de tes mains<!--Ps. 77:11-13.-->.
143:6	J'étends mes mains vers toi ; mon âme s'adresse à toi comme une terre desséchée<!--Ps. 28:1, 42:1-3.-->. Sélah.
143:7	YHWH ! hâte-toi, réponds-moi ! Mon esprit se consume. Ne me cache pas tes faces ! Je deviendrais pareil à ceux qui descendent dans la fosse !
143:8	Fais-moi entendre dès le matin ta bonté ! Car je me confie en toi. Fais-moi connaître la voie dans laquelle je marche, car c’est vers toi que j’élève mon âme<!--Ps. 25:1.-->.
143:9	YHWH, délivre-moi de mes ennemis ! Je me suis réfugié auprès de toi !
143:10	Enseigne-moi à faire ta volonté ! Car tu es mon Éloah ! Que ton bon Esprit me conduise en terre de la droiture<!--Jn. 16:13.--> !
143:11	À cause de ton nom, YHWH, fais-moi vivre ! Dans ta justice, fais sortir mon âme de la détresse !
143:12	Dans ta bonté, extermine mes ennemis et fais périr tous les oppresseurs de mon âme – car je suis ton serviteur.

## Chapitre 144

### Se confier en YHWH, le Rocher

144:1	De David. Béni soit YHWH, mon Rocher<!--Voir commentaire en Es. 8:13-17.--> qui exerce mes mains au combat et mes doigts à la bataille,
144:2	ma bonté et ma forteresse, ma haute retraite, mon Sauveur<!--Es. 59:20-21 ; Ro. 11:26.-->, mon bouclier<!--Ep. 6:16.-->, celui en qui je me suis réfugié<!--Ps. 91 ; Mt. 11:28-30.-->, celui qui m'assujettit mon peuple !
144:3	YHWH ! qu'est-ce que l'être humain, pour que tu le connaisses<!--Ps. 8:5 ; Job 7:17 ; Hé. 2:6-7.-->, le fils du mortel pour que tu penses à lui ?
144:4	L'être humain est semblable à une vapeur<!--Voir Ja. 4:14.-->, ses jours sont comme une ombre qui passe<!--Ps. 102:12 ; Job 14:1-2 ; Ec. 6:12.-->.
144:5	YHWH, abaisse tes cieux, et descends ! Touche les montagnes, et qu'elles fument<!--Es. 63:19 ; Ps. 18:7-8.--> !
144:6	Fais briller les éclairs, et disperse mes ennemis ! Envoie tes flèches, et mets-les en déroute !
144:7	Étends tes mains d'en haut, sauve-moi, délivre-moi des grandes eaux, de la main des fils de l'étranger,
144:8	dont la bouche profère le mensonge, et dont la droite est une droite trompeuse !
144:9	Elohîm ! je chanterai un cantique nouveau ! Je te célébrerai sur le luth à dix cordes !
144:10	Toi, qui donnes la délivrance aux rois et qui délivres de l'épée d'adversité David, ton serviteur,
144:11	délivre-moi et sauve-moi de la main des fils de l'étranger, dont la bouche profère le mensonge et dont la droite est une droite trompeuse !
144:12	Que nos fils soient comme des plantes qui croissent dans leur jeunesse, nos filles comme des colonnes sculptées, des modèles pour un palais !
144:13	Que nos greniers soient pleins, fournissant toutes sortes de provisions ! Que nos troupeaux multiplient par milliers, même par dix milliers dans nos rues !
144:14	Que nos bœufs soient chargés de graisse ! Qu'il n'y ait ni brèche, ni sortie, ni cri sur nos places !
144:15	Heureux le peuple pour qui il en est ainsi ! Heureux le peuple dont YHWH est l'Elohîm !

## Chapitre 145

### Louange à YHWH pour tout ce qu'il est

145:1	Louange. De David. [Aleph.] Mon Éloah, mon roi, je t'exalterai et je bénirai ton Nom pour toujours, et à perpétuité !
145:2	[Beth.] Je te bénirai tout le jour, et je louerai ton Nom pour toujours, et à perpétuité !
145:3	[Guimel.] YHWH est grand et très digne de louanges, il n'est pas possible de sonder sa grandeur.
145:4	[Daleth.] Qu’un âge à l’autre âge célèbre tes œuvres, et publie tes hauts faits !
145:5	[He.] Je parlerai de la splendeur glorieuse de ta majesté, et de tes paroles merveilleuses !
145:6	[Vav.] On parlera de ta puissance redoutable, et je raconterai ta grandeur.
145:7	[Zayin.] Ils proclameront le souvenir de ton immense bonté, et ils pousseront des cris de joie pour ta justice !
145:8	[Heth.] YHWH est miséricordieux et compatissant, lent à la colère et grand en bonté.
145:9	[Teth.] YHWH est bon envers tous, et ses compassions sont au-dessus de toutes ses œuvres.
145:10	[Yod.] YHWH ! toutes tes œuvres te célébreront, et tes fidèles te béniront !
145:11	[Kaf.] Ils parleront de la gloire de ton règne, et ils proclameront ta puissance
145:12	[Lamed.] pour faire connaître aux fils d'humain ta puissance, et la splendeur glorieuse de ton règne.
145:13	[Mem.] Ton règne est un règne de tous les siècles, ta domination est pour tous les âges des âges.
145:14	[Samech.] YHWH soutient tous ceux qui tombent, et redresse tous ceux qui sont courbés<!--Ps. 146:8.-->.
145:15	[Ayin.] Les yeux de tous les animaux s'attendent à toi, et tu leur donnes leur nourriture en leur temps.
145:16	[Pe.] Tu ouvres ta main, et tu rassasies de délices toute créature vivante.
145:17	[Tsade.] YHWH est juste dans toutes ses voies, fidèle dans toutes ses œuvres<!--Da. 4:34.-->.
145:18	[Qof.] YHWH est près de tous ceux qui l’appellent, de tous ceux qui l’appellent en vérité<!--Ps. 34:18.-->.
145:19	[Resh.] Il accomplit la volonté de ceux qui le craignent, il entend leur cri et les délivre.
145:20	[Shin.] YHWH garde tous ceux qui l'aiment, mais il exterminera tous les méchants.
145:21	[Tav.] Ma bouche racontera la louange de YHWH, et toute chair bénira le Nom de sa sainteté pour toujours, et à perpétuité<!--Ps. 103:1.--> !

## Chapitre 146

### La fidélité de YHWH dure pour toujours

146:1	Allélou-Yah ! Mon âme, loue YHWH !
146:2	Je louerai YHWH tant que je vivrai, je chanterai mon Elohîm tant que j'existerai !
146:3	Ne vous confiez pas aux nobles, aux fils de l'être humain auprès duquel il n’y a pas de salut.
146:4	Son esprit sort, il retourne au sol et, ce même jour, ses pensées périssent.
146:5	Heureux celui qui a pour secours le El de Yaacov et dont l'espérance est en YHWH son Elohîm !
146:6	Il a fait les cieux et la Terre, la mer et tout ce qui s'y trouve. Il garde la vérité pour toujours !
146:7	Il fait justice aux opprimés, il donne du pain aux affamés. YHWH délie ceux qui sont liés<!--Jn. 11:43-44.-->,
146:8	YHWH ouvre les yeux des aveugles<!--Les miracles de Yéhoshoua ha Mashiah (Jésus le Christ) confirment sa divinité (Es. 35:4-6 ; Lu. 7:19-23).-->, YHWH redresse ceux qui sont courbés<!--Lu. 13:11-13.-->, YHWH aime les justes.
146:9	YHWH garde les étrangers, il relève l'orphelin et la veuve, mais il rend tortueuse la voie des méchants.
146:10	YHWH règne éternellement. Sion ! ton Elohîm subsiste d'âges en âges ! Allélou-Yah !

## Chapitre 147

### YHWH aime ceux qui le craignent et qui s'attendent à sa bonté

147:1	Allélou-Yah ! Car il est bon de chanter notre Elohîm ! Car il est plaisant et bienséant de le louer !
147:2	YHWH est celui qui bâtit Yeroushalaim, il rassemblera ceux d'Israël qui sont dispersés çà et là.
147:3	Il guérit ceux qui ont le cœur brisé, et il bande leurs plaies<!--Ex. 15:26 ; De. 32:39 ; Job 5:18.-->.
147:4	Il compte le nombre des étoiles, il les appelle toutes par leur nom.
147:5	Notre Seigneur est grand, puissant par sa force, son intelligence n'a pas de limites.
147:6	YHWH relève les malheureux, il abaisse les méchants jusqu'à terre.
147:7	Répondez à YHWH avec des louanges ! Chantez notre Elohîm avec la harpe !
147:8	Il couvre les cieux de nuées, il prépare la pluie pour la Terre, il fait germer l'herbe sur les montagnes.
147:9	Il donne la nourriture au bétail, et aux petits du corbeau qui crient.
147:10	Il ne se complaît pas dans la force du cheval, il ne met pas son plaisir dans les jambes de l'homme.
147:11	YHWH aime ceux qui le craignent, ceux qui s'attendent à sa bonté.
147:12	Yeroushalaim, loue YHWH ! Sion, loue ton Elohîm !
147:13	Car il a affermi les barres de tes portes, il a béni tes fils au milieu de toi.
147:14	C'est lui qui met la paix dans ton territoire et qui te rassasie de la graisse du blé.
147:15	C'est lui qui envoie sa parole sur la Terre, et sa parole court avec beaucoup de vitesse<!--Es. 55:10-11.-->.
147:16	C'est lui qui donne la neige comme de la laine et qui répand la gelée blanche comme de la cendre.
147:17	C'est lui qui lance sa glace comme par morceaux. Qui peut résister devant son froid ?
147:18	Il envoie sa parole et il les fond, il fait souffler son vent et les eaux coulent<!--Ps. 135:7.-->.
147:19	Il a fait connaître sa parole à Yaacov, ses statuts et ses ordonnances à Israël<!--Ps. 78:5 ; Ro. 3:1-2.-->.
147:20	Il n'a pas agi de même pour toutes les nations, c'est pourquoi elles ne connaissent pas ses ordonnances. Allélou-Yah !

## Chapitre 148

### La création loue son Elohîm

148:1	Allélou-Yah ! Louez des cieux YHWH ! Louez-le dans les lieux élevés !
148:2	Louez-le, vous tous ses anges ! Louez-le, vous toutes ses armées !
148:3	Louez-le, vous, soleil et lune ! Louez-le, vous toutes, étoiles de lumière !
148:4	Louez-le, vous, cieux des cieux, et les eaux qui êtes au-dessus des cieux !
148:5	Qu'ils louent le Nom de YHWH ! Car il a commandé, et ils ont été créés<!--Ge. 1:3-6 ; Jé. 31:35.-->.
148:6	Il les a établis à perpétuité, pour toujours. Il a donné des lois et il ne les transgressera pas<!--Ps. 104:5, 119:91 ; Job 14:5.-->.
148:7	Louez YHWH de la Terre, monstres marins et tous les abîmes !
148:8	Feu et grêle, neige et brouillard, vent impétueux qui exécutez ses ordres,
148:9	montagnes et toutes les collines, arbres fruitiers et tous les cèdres,
148:10	bêtes sauvages et tout le bétail, reptiles et oiseaux ailés,
148:11	rois de la Terre et tous les peuples, princes et tous les juges de la Terre,
148:12	jeunes garçons et vierges, vieillards avec les jeunes hommes !
148:13	Qu'ils louent le Nom de YHWH ! car son Nom seul est haut élevé ! Sa majesté est au-dessus de la Terre et des cieux.
148:14	Il a élevé la corne de son peuple, la louange de tous ses fidèles, des fils d'Israël, le peuple qui lui est proche. Allélou-Yah !

## Chapitre 149

### Adorons YHWH

149:1	Allélou-Yah ! Chantez à YHWH un cantique nouveau : sa louange dans l’assemblée de ses bien-aimés !
149:2	Qu'Israël se réjouisse en celui qui l'a fait ! Que les fils de Sion exultent pour leur Roi<!--Ps. 100:3 ; Za. 9:9 ; Mt. 21:5.--> !
149:3	Qu'ils louent son Nom avec des danses ! Qu'ils le chantent avec le tambourin et la harpe !
149:4	Car YHWH prend plaisir à son peuple, il glorifie les pauvres en les délivrant.
149:5	Que les bien-aimés se réjouissent dans la gloire, qu'ils poussent des cris de joie sur leur couche !
149:6	Les louanges de El sont dans leur bouche, et l'épée à deux tranchants dans leur main,
149:7	pour se venger des nations, pour châtier les peuples,
149:8	pour lier leurs rois avec des chaînes, et les plus honorables parmi eux avec des ceps de fer,
149:9	pour exercer sur eux le jugement qui est écrit ! Cet honneur est pour tous ses bien-aimés. Allélou-Yah !

## Chapitre 150

### Que tout ce qui respire loue YHWH !

150:1	Allélou-Yah ! Louez El pour sa sainteté ! Louez-le à cause de ce firmament qu'il a fait par sa puissance !
150:2	Louez-le pour ses actions puissantes ! Louez-le selon la grandeur de sa magnificence !
150:3	Louez-le au son du shofar ! Louez-le avec le luth et la harpe !
150:4	Louez-le avec le tambour et avec des danses ! Louez-le avec des instruments à cordes et la flûte !
150:5	Louez-le avec les cymbales bruyantes ! Louez-le avec les cymbales de cri de joie !
150:6	Que tout ce qui respire loue Yah ! Allélou-Yah !
