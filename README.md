[![pipeline status](https://gitlab.com/anjc/bjc-source/badges/master/pipeline.svg)](https://gitlab.com/anjc/bjc-source/-/commits/master)

Fichier source de la Bible de Yéhoshoua ha Mashiah (BYM)

[https://www.bibledeyehoshouahamashiah.org/](https://www.bibledeyehoshouahamashiah.org/)

