# Shoftim (Juges) (Jg.)

Signification : Être juge, prononcer, punir

Auteur : Inconnu

Thème : Défaites et délivrances

Date de rédaction : Env. 1100 av. J.-C.

À la mort de Yéhoshoua (Josué) et des anciens, il s'éleva en Israël une nouvelle génération qui n'avait pas connu l'expérience du désert. Elle fit ce qui est mal aux yeux d'Elohîm, l'abandonna et tomba dans l'idolâtrie. Ainsi, la colère de YHWH s'abattit sur Israël et il livra le peuple entre les mains de ses ennemis. Dans ces temps de troubles, Elohîm suscita des juges – douze hommes et une femme – pour délivrer Israël de ses oppresseurs. Aussi longtemps que le juge était en vie, Israël était en paix. Mais dès qu'il venait à mourir, le peuple se corrompait de nouveau et ses oppressions recommençaient.

## Chapitre 1

### Poursuite de la conquête de Kena'ân (Canaan)

1:1	Or il arriva qu'après la mort de Yéhoshoua, les fils d'Israël consultèrent YHWH, en disant : Qui de nous montera le premier contre les Kena'ânéens pour leur faire la guerre ?
1:2	YHWH dit : Yéhouda montera. Voici, j'ai livré la terre entre ses mains.
1:3	Yéhouda dit à Shim’ôn, son frère : Monte avec moi dans mon lot et nous ferons la guerre aux Kena'ânéens, et j'irai aussi avec toi dans ton lot. Shim’ôn alla avec lui.

### Victoires de Yéhouda ; Kaleb prend possession d'Hébron

1:4	Yéhouda monta, et YHWH livra les Kena'ânéens et les Phéréziens entre leurs mains. Ils battirent 10 000 hommes à Bézek.
1:5	Et ils trouvèrent Adoni-Bézek à Bézek. Ils l'attaquèrent et frappèrent les Kena'ânéens et les Phéréziens.
1:6	Adoni-Bézek s'enfuit, mais ils le poursuivirent, et l'ayant pris, ils lui coupèrent les pouces des mains et des pieds.
1:7	Adoni-Bézek dit : 70 rois, dont les pouces des mains et des pieds avaient été coupés, ramassaient sous ma table. Elohîm me rend ce que j'ai fait. On l'amena à Yeroushalaim et il y mourut<!--Es. 33:1.-->.
1:8	Les fils de Yéhouda firent la guerre contre Yeroushalaim et la prirent : ils la frappèrent à bouche d’épée et mirent le feu à la ville.
1:9	Après cela les fils de Yéhouda descendirent pour faire la guerre aux Kena'ânéens qui habitaient la montagne, la contrée du midi et la plaine.
1:10	Et Yéhouda marcha contre les Kena'ânéens qui habitaient à Hébron, or le nom d'Hébron était auparavant Qiryath-Arba, et il battit Sheshaï, Ahiman et Talmaï<!--Jos. 15:14.-->.
1:11	Et de là, il marcha contre les habitants de Debir. Debir s'appelait auparavant Qiryath-Sépher<!--Jos. 15:15.-->.
1:12	Et Kaleb dit : Je donnerai ma fille Akcah pour femme à celui qui frappera Qiryath-Sépher et qui la prendra<!--Jos. 15:16.-->.
1:13	Othniel, fils de Qenaz, frère cadet de Kaleb, la prit. Il lui donna sa fille Akcah pour femme.
1:14	Il arriva que comme elle s'en allait, elle l'incita à demander à son père un champ. Elle descendit ensuite de son âne et Kaleb lui dit : Qu'as-tu ?<!--Jos. 15:18.-->
1:15	Elle lui dit : Donne-moi un présent, puisque tu m'as donné une terre du midi. Donne-moi aussi des sources d'eau. Et Kaleb lui donna les sources supérieures et les sources inférieures.
1:16	Les fils du Qeynien, beau-père de Moshé, montèrent de la ville des palmiers avec les fils de Yéhouda, dans le désert de Yéhouda, qui est au midi d'Arad, et ils allèrent et demeurèrent avec le peuple<!--Jg. 4:11.-->.
1:17	Yéhouda se mit en marche avec Shim’ôn, son frère, et ils frappèrent les Kena'ânéens qui habitaient à Tsephath. Ils détruisirent la ville par interdit, c'est pourquoi on appela la ville du nom de Hormah.
1:18	Yéhouda prit aussi Gaza avec ses territoires, Askalon avec ses territoires, et Ékron avec ses territoires.
1:19	YHWH fut avec Yéhouda et il prit possession de la montagne, mais il ne put déposséder les habitants de la vallée, parce qu'ils avaient des chars de fer.
1:20	On donna Hébron à Kaleb, comme Moshé l'avait dit, et il en déposséda les trois fils d'Anak<!--No. 14:24.-->.
1:21	Quant aux fils de Benyamin, ils ne dépossédèrent pas les Yebousiens qui habitaient à Yeroushalaim. C'est pourquoi les Yebousiens ont habité avec les fils de Benyamin à Yeroushalaim jusqu'à ce jour.
1:22	Ceux de la maison de Yossef montèrent aussi contre Béth-El, et YHWH fut avec eux.
1:23	Ceux de la maison de Yossef firent explorer Béth-El : le nom de la ville était auparavant Louz.
1:24	Les espions virent un homme qui sortait de la ville, et ils dirent : Fais-nous voir, s’il te plaît, par où entrer dans la ville et nous te traiterons avec bonté.
1:25	Il leur fit voir par où entrer dans la ville. Ils frappèrent la ville du tranchant de l'épée, mais ils laissèrent aller cet homme et toute sa famille.
1:26	Cet homme s’en alla en terre des Héthiens. Il bâtit une ville qu’il appela du nom de Louz. C’est son nom jusqu’à ce jour.
1:27	Menashè ne prit pas possession de Beth-Shean et ses filles, de Thaanac et ses filles, de Dor et ses filles, les habitants de Yible`am et ses filles, les habitants de Meguiddo et ses filles. Les Kena'ânéens consentirent à habiter cette terre-là.
1:28	En effet, il arriva que, quand Israël fut devenu plus fort, il réduit les Kena'ânéens à la servitude, mais il ne les déposséda pas, il ne les déposséda pas.
1:29	Éphraïm aussi ne déposséda pas les Kena'ânéens qui habitaient à Guézer, mais les Kena'ânéens habitèrent au milieu d'eux à Guézer.
1:30	Zebouloun ne déposséda pas les habitants de Kitron, ni les habitants de Nahalol. Les Kena'ânéens habitèrent au milieu d'eux et furent réduits à la servitude.
1:31	Asher ne déposséda pas les habitants d'Acco, ni les habitants de Sidon, ni ceux d'Achlal, ni d'Aczib, ni d'Helbah, ni d'Aphik, ni de Rehob.
1:32	Les Ashérites habitèrent au milieu des Kena'ânéens, habitants de la terre, car ils ne les chassèrent pas.
1:33	Nephthali ne déposséda pas les habitants de Beth-Shémesh, ni les habitants de Beth-Anath. Il habita au milieu des Kena'ânéens habitants de la terre, mais les habitants de Beth-Shémesh et de Beth-Anath furent réduits à la servitude.
1:34	Les Amoréens serrèrent les fils de Dan dans la montagne et ne leur permirent pas de descendre dans la vallée.
1:35	Les Amoréens consentirent à habiter à Har-Hérès, à Ayalon et à Shaalbim. Mais la main de la maison de Yossef devint pesante, ils furent réduits à la servitude.
1:36	Le territoire des Amoréens s'étendait depuis la montée d'Akrabbim, depuis Séla et au-dessus.

## Chapitre 2

### Le peuple repris à cause de sa désobéissance

2:1	L'Ange de YHWH monta de Guilgal à Bokiym et dit : Je vous ai fait monter hors d'Égypte et vous ai fait entrer sur la terre que j'avais jurée à vos pères. J'avais dit : Je ne romprai jamais mon alliance que j'ai traitée avec vous<!--Ge. 17:7.-->,
2:2	et vous, vous ne traiterez pas alliance avec les habitants de cette terre, vous démolirez leurs autels. Mais vous n'avez pas obéi à ma voix. Pourquoi avez-vous fait cela<!--Ex. 23:32 ; De. 7:2, 12:3.--> ?
2:3	Aussi ai-je dit : Je ne les chasserai pas en face de vous, mais ils seront à vos côtés et leurs elohîm deviendront un piège pour vous<!--Ex. 23:33 ; Jos. 23:13.-->.
2:4	Il arriva que comme l'Ange de YHWH disait ces paroles à tous les fils d'Israël, le peuple éleva la voix et pleura.
2:5	Ils appelèrent ce lieu du nom de Bokiym<!--Pleureurs.-->, et ils y sacrifièrent à YHWH.
2:6	Yéhoshoua renvoya le peuple, et les fils d'Israël allèrent chaque homme dans son héritage pour prendre possession de la terre<!--Jos. 24:28-32.-->.
2:7	Le peuple servit YHWH tous les jours de Yéhoshoua, et tous les jours des anciens dont les jours se prolongèrent après Yéhoshoua, qui avaient vu toutes les grandes œuvres que YHWH avait faites en faveur d'Israël<!--Jos. 24:31.-->.
2:8	Yéhoshoua, fils de Noun, serviteur de YHWH, mourut fils de 110 ans<!--Jos. 24:29.-->.
2:9	On l’enterra dans le territoire qu'il avait eu en partage à Timnath-Hérès, dans la montagne d'Éphraïm, au nord de la montagne de Ga`ash<!--Jos. 24:30.-->.

### La nouvelle génération abandonne YHWH

2:10	Toute cette génération fut recueillie auprès de ses pères, et il s'éleva après elle une autre génération, qui ne connaissait pas YHWH ni les œuvres qu'il avait faites en faveur d'Israël.
2:11	Les fils d'Israël firent ce qui est mal aux yeux de YHWH et ils servirent les Baalim<!--L'appellatif « Baal » en hébreu, ou « bel » en phénicien, signifie « seigneur », « maître », ou « possesseur », et désigne parfois « l'époux ». Ce terme qui sert avant tout de titre, signale aussi bien des divinités, des êtres humains ou encore des villes portant le nom du dieu tutélaire de la cité. Comme il existait une multitude de « baalim » (Jg. 2:13), ce nom était la plupart du temps associé à un autre nom ou à un qualificatif : Baal-Hanan, seigneur de compassion (Ge. 36:38-39 ; 1 Ch. 1:49-50, 27:28), Baal-Tsephon, seigneur du nord (Ex. 14:2,9 ; No. 33:7), Baal-Peor, seigneur de la brèche (No. 25:3,5 ; De. 4:3 ; Ps. 106:28 ; Os. 9:10), Baal-Gad, seigneur des richesses (Jos. 11:17, 12:7, 13:5), Qiryath-Baal, ville de Baal (Jos. 15:60, 18:14), Beth-Baal-Meon, maison de Baal (Jos. 13:17 ; Jé. 48:23), Baal-Berith, seigneur de l'alliance (Jg. 8:33, 9:4), Baal-Zeboub, seigneur des mouches (2 R. 1:2-3, 1:6, 1:16). Toutefois, l'histoire antique du Proche-Orient fut marquée par la figure emblématique d'un Baal, équivalent de Seth chez les Égyptiens à l'époque des Ramessides. Connu pour être le dieu de la vie, le seigneur de la terre et du ciel, « le chevaucheur des nuées », c'est à lui qu'on attribuait la fertilisation du sol par l'envoi de la pluie. Selon la mythologie kena'ânéenne (cananéenne), il était condamné à livrer une guerre perpétuelle à « Mot », dieu de la guerre et de la stérilité. Si Baal était vainqueur, la terre bénéficiait d'un cycle de sept ans de fertilité ; s'il perdait, Mot installait un cycle de sept ans de sécheresse et de famine. C'est précisément ce Baal et ses prophètes qu'Éliyah défia au Nom de YHWH (1 R. 17:1, 18:21-46). Toujours accompagné d'une déesse (« baalat »), le plus souvent Astarté, son culte était licencieux et se déroulait dans les hauts lieux, près des bosquets ou des bocages (« asherah » en hébreu, 2 R. 17:10). Baal avait ses propres prêtres (So. 1:4), ses prophètes (2 R. 18), et exigeait, selon la circonstance, diverses offrandes, des sacrifices d'animaux ou d'humains (Jé. 7:9, 19:5).-->.
2:12	Ils abandonnèrent YHWH, l'Elohîm de leurs pères, qui les avait fait sortir de la terre d'Égypte, ils allèrent après d'autres elohîm, d'entre les elohîm des peuples qui les entouraient. Ils se prosternèrent devant eux, irritant ainsi YHWH.
2:13	Ils abandonnèrent YHWH, et servirent Baal et les Astartés<!--Le nom « Astarté » vient de l'hébreu « ashtarowth » qui signifie « étoiles » ou encore « accroissement ». Employé au pluriel, ce terme renvoie généralement aux divinités féminines (Jg. 2:13, 10:6 ; 1 S. 7:3-4, 12:10, 13:10 ; 2 Ch. 24:18). Manifestation sémitique de la déesse Ishtar des Babyloniens, ou encore d'Inanna chez les Sumériens, elle était connue dans l'Égypte des Ramessides comme la fille de Rê ou de Ptah, puis la compagne de Seth, équivalent de Baal, auquel elle était immanquablement associée. Initialement connue pour son caractère belliqueux, elle était représentée à cheval, faisant bénéficier au souverain de sa protection. Déesse de la terre et de la nature, on lui attribuait la fertilité du sol et l'opulence des moissons. Comme pour Baal, son culte licencieux était célébré sur les hauteurs, et accompagné de sacrifices sanglants, y compris humains (2 R. 23:7). Au fil du temps, elle prit une telle ampleur que Shelomoh (Salomon), entraîné par ses concubines, devint son adorateur (1 R. 11:5). Devenue l'incontournable « reine des cieux », les Hébreux continuèrent à la vénérer avec assiduité, et ce, en dépit de la déportation babylonienne qui était la conséquence directe de leur idolâtrie (Jé. 7:18, 44:15-26).-->.
2:14	La colère de YHWH s'enflamma contre Israël. Il les livra entre les mains de pillards<!--Lorsqu'un enfant d'Elohîm ouvre la porte au péché, il s'expose aux pillards, c'est-à-dire à Satan et ses démons (Jn. 10:10).--> qui les pillèrent, et il les vendit entre les mains de leurs ennemis d'alentour, de sorte qu'ils ne furent plus capables de tenir devant leurs ennemis<!--Es. 50:1 ; Ps. 44:12-13.-->.
2:15	Partout où ils allaient, la main de YHWH était contre eux pour leur faire du mal, comme YHWH en avait parlé et comme YHWH le leur avait juré, ils furent très déprimés<!--Lé. 26:25 ; De. 28:25.-->.

### YHWH suscite des libérateurs : Les juges

2:16	YHWH leur suscita des juges<!--Les juges étaient principalement des libérateurs de l'oppression des ennemis d'Israël.--> et ils les sauvèrent de la main de ceux qui les pillaient.
2:17	Mais ils n’écoutèrent pas leurs juges, car ils se prostituèrent à d’autres elohîm et se prosternèrent devant eux. Ils se détournèrent promptement de la voie où leurs pères avaient marché en écoutant les commandements de YHWH. Ils n'agirent pas ainsi.
2:18	Quand YHWH leur suscitait des juges, YHWH était avec le juge, et il les sauvait de la main de leurs ennemis pendant tous les jours du juge. En effet, YHWH se repentait à cause de leurs gémissements contre ceux qui les opprimaient et les serraient.
2:19	Il arrivait que quand le juge mourrait, ils se corrompaient de nouveau plus que leurs pères en allant après d'autres elohîm pour les servir et se prosterner devant eux, et ils persévéraient dans les mêmes pratiques et dans la même voie obstinée<!--Jg. 3:9-12.-->.

### YHWH éprouve Israël

2:20	La colère de YHWH s'enflamma contre Israël, et il dit : Puisque cette nation a transgressé mon alliance que j'avais prescrite à leurs pères, et puisqu'ils n'ont pas obéi à ma voix,
2:21	moi aussi je ne déposséderai plus un homme devant eux, d’entre les nations que Yéhoshoua laissa quand il mourut<!--Jos. 23:13.-->,
2:22	afin d'éprouver par elles Israël : garderont-ils la voie de YHWH en y marchant, comme l'avaient gardée leurs pères, ou non ?
2:23	YHWH laissa en repos ces nations en ne les dépossédant<!--Jg. 3:1-3.--> pas rapidement, et il ne les livra pas entre les mains de Yéhoshoua.

## Chapitre 3

3:1	Voici les nations que YHWH laissa pour éprouver par elles Israël, tous ceux qui n'avaient pas connu toutes les guerres de Kena'ân<!--Jg. 2:21-23.-->,
3:2	seulement pour que les générations des fils d'Israël les connaissent, pour leur apprendre la guerre, seulement à ceux qui ne l'avaient pas connue auparavant :
3:3	Les cinq seigneurs des Philistins, tous les Kena'ânéens, les Sidoniens et les Héviens qui habitaient la montagne du Liban, depuis la montagne de Baal-Hermon, jusqu'à l'entrée de Hamath<!--No. 13:22.-->.
3:4	Elles servirent à éprouver Israël, pour savoir s'ils obéiraient aux commandements que YHWH avait donnés à leurs pères par la main de Moshé.

### Israël se mélange aux nations païennes

3:5	Les fils d'Israël habitèrent parmi les Kena'ânéens, les Héthiens, les Amoréens, les Phéréziens, les Héviens et les Yebousiens.
3:6	Ils prirent leurs filles pour femmes, ils donnèrent leurs filles à leurs fils et servirent leurs elohîm.
3:7	Les fils d'Israël firent ce qui est mal aux yeux de YHWH, ils oublièrent YHWH et servirent les Baalim et les Asherah<!--Jg. 2:11.-->.

### Othniel, premier juge suscité par YHWH

3:8	La colère de YHWH s'enflamma contre Israël et il les vendit aux mains de Koushân-Rish`athayim, roi de Mésopotamie. Et les fils d'Israël servirent Koushân-Rish`athayim pendant 8 ans.
3:9	Les fils d'Israël crièrent à YHWH, et YHWH suscita pour les fils d'Israël un sauveur qui les sauva. C'était Othniel, fils de Qenaz, frère cadet de Kaleb.
3:10	L'Esprit de YHWH vint sur lui, et il jugea Israël. Il sortit pour la guerre et YHWH livra entre ses mains Koushân-Rish`athayim, le roi de Mésopotamie, et sa main fut forte contre Koushân-Rish`athayim.
3:11	La terre fut en repos pendant 40 ans, et Othniel, fils de Qenaz, mourut.

### Éhoud, juge en Israël

3:12	Les fils d'Israël firent encore ce qui est mal aux yeux de YHWH et YHWH fortifia Églon, roi de Moab, contre Israël, parce qu'ils avaient fait ce qui est mal aux yeux de YHWH.
3:13	Il réunit auprès de lui les fils d'Ammon et les Amalécites. Ils allèrent frapper Israël et prirent possession de la ville des palmiers<!--Palmiers : un autre nom de Yeriycho (Jéricho).-->.
3:14	Les fils d'Israël servirent Églon, roi de Moab, pendant 18 ans.
3:15	Les fils d'Israël crièrent à YHWH, et YHWH leur suscita un sauveur. C'était Éhoud, fils de Guéra, Benyamite, un homme qui était empêché de la main droite. Les fils d'Israël envoyèrent par sa main un présent à Églon, roi de Moab.
3:16	Éhoud se fit une épée qui avait deux bouches, de la longueur d'une coudée<!--Voir tableau mesures de poids.--> et il la ceignit sous ses vêtements, sur sa cuisse droite.
3:17	Il présenta le présent à Églon, roi de Moab. Églon était un homme excessivement gras.
3:18	Or il arriva que, lorsqu'il eut achevé de présenter le présent, il renvoya le peuple qui avait apporté le présent.
3:19	Mais lui-même revint depuis les images gravées, qui étaient près de Guilgal et il dit : J’ai pour toi une parole secrète, roi ! Et il lui dit : Tais-toi ! Et tous ceux qui se tenaient debout près de lui sortirent d’auprès de lui.
3:20	Éhoud vint vers lui. Il était assis seul dans sa chambre haute rafraîchissante et il dit : J'ai une parole d'Elohîm pour toi. Celui-ci se leva de son trône.
3:21	Éhoud étendit sa main gauche, prit l’épée de dessus sa cuisse droite et la lui enfonça dans le ventre.
3:22	Même la poignée entra après la lame et la graisse se referma sur la lame, car il n’avait pas retiré l'épée du ventre, et des excréments en sortirent.
3:23	Éhoud sortit par le portique, ferma après lui les portes de la chambre haute et tira le verrou.
3:24	Lui sorti, les serviteurs d'Églon vinrent et regardèrent, et voici, les portes de la chambre haute étaient fermées au verrou. Ils dirent : Sûrement il se couvre les pieds dans sa chambre rafraîchissante.
3:25	Ils attendirent jusqu’à en être déconcertés. Mais voici, il n'ouvrait pas les portes de la chambre haute. Ils prirent la clé et ouvrirent, et voici, leur maître était mort, étendu à terre.
3:26	Éhoud se sauva pendant qu'ils s'attardaient. Il dépassa les images gravées et se sauva à Seïra.
3:27	Il arriva qu'à son arrivée, il sonna du shofar dans la montagne d'Éphraïm et les fils d'Israël descendirent avec lui de la montagne, lui en face d’eux.
3:28	Il leur dit : Suivez-moi, car YHWH a livré entre vos mains les Moabites, vos ennemis ! Ils descendirent derrière lui, se saisirent des gués du Yarden vis-à-vis de Moab et ne permirent à personne de passer.
3:29	Ils battirent en ce temps-là environ 10 000 hommes de Moab, tous robustes, tous hommes talentueux, et pas un homme ne se sauva.
3:30	En ce jour, Moab fut humilié sous la main d'Israël. Et la terre fut en repos pendant 80 ans.

### Shamgar, juge en Israël

3:31	Après lui, il y eut Shamgar, fils d'Anath. Il battit 600 Philistins avec un aiguillon à bœufs. Il sauva, lui aussi, Israël.

## Chapitre 4

### Déborah et Barak, juges en Israël

4:1	Les fils d'Israël firent encore ce qui est mal aux yeux de YHWH, après la mort d'Éhoud.
4:2	YHWH les vendit aux mains de Yabiyn, roi de Kena'ân, qui régnait à Hatsor. Le chef de son armée était Sisera, qui habitait à Harosheth-Goïm<!--Jos. 11:11-13 ; 1 S. 12:9.-->.
4:3	Les fils d'Israël crièrent à YHWH, car il avait 900 chars de fer et il avait violemment opprimé les fils d'Israël pendant 20 ans<!--Jg. 1:19.-->.
4:4	Déborah, une femme, une prophétesse, la femme de Lappidoth, jugeait Israël.
4:5	Elle s’asseyait sous le palmier de Déborah, entre Ramah et Béth-El, dans la montagne d'Éphraïm, et les fils d'Israël montaient vers elle pour être jugés.
4:6	Elle envoya appeler Barak, fils d'Abinoam, de Kédesh-Nephthali et elle lui dit : YHWH, l'Elohîm d'Israël, n'a-t-il pas donné cet ordre : Va, et dirige-toi sur la montagne de Thabor et prends avec toi 10 000 hommes des fils de Nephthali et des fils de Zebouloun<!--Hé. 11:32.-->.
4:7	J'attirerai vers toi, vers le torrent de Kison, Sisera, chef de l'armée de Yabiyn, avec ses chars et ses troupes et je le livrerai entre tes mains<!--Ps. 83:9-10.--> ?
4:8	Barak lui dit : Si tu viens avec moi, j'irai, mais si tu ne viens pas avec moi, je n'irai pas.
4:9	Elle dit : J'irai, j'irai avec toi, mais tu n'auras pas d'honneur sur le chemin où tu marches, car YHWH livrera Sisera entre les mains d'une femme. Déborah se leva et elle alla avec Barak à Kédesh.
4:10	Barak convoqua Zebouloun et Nephthali à Kédesh. Il monta à pied avec 10 000 hommes et Déborah monta avec lui.
4:11	Héber, le Qeynien, s'était séparé des Qeyniens, fils de Hobab, beau-père<!--Beau frère. Voir No. 10:29.--> de Moshé, et il avait dressé ses tentes à côté du grand arbre de Tsaannaïm, près de Kédesh<!--No. 10:29.-->.

### YHWH accorde la victoire à Israël

4:12	On informa Sisera que Barak, fils d'Abinoam, était monté sur la montagne de Thabor.
4:13	Et Sisera rassembla tous ses chars, 900 chars de fer, et tout le peuple qui était avec lui, depuis Harosheth-Goïm, jusqu'au torrent de Kison.
4:14	Déborah dit à Barak : Lève-toi, car voici le jour où YHWH livre Sisera entre tes mains. YHWH ne marche-t-il pas devant toi ? Barak descendit de la montagne de Thabor, ayant 10 000 hommes derrière lui.
4:15	YHWH mit en déroute Sisera, tous ses chars et toute l'armée, à bouche d’épée, devant Barak. Sisera descendit du char et s'enfuit à pied<!--Ps. 83:9-10.-->.
4:16	Barak courut après les chars et après l'armée jusqu'à Harosheth-Goïm. Toute l'armée de Sisera tomba à bouche d’épée, il n'en resta pas un seul.
4:17	Sisera se sauva à pied dans la tente de Ya`el, femme de Héber, le Qeynien. Car il y avait la paix entre Yabiyn, roi de Hatsor et la maison de Héber, le Qeynien.
4:18	Ya`el étant sortie au-devant de Sisera, lui dit : Entre, mon seigneur, entre chez moi, ne crains pas. Il entra chez elle dans la tente et elle le cacha sous une couverture.
4:19	Il lui dit : S’il te plaît, donne-moi un peu d'eau à boire, car j'ai soif. Et elle ouvrit une outre de lait, lui donna à boire et le couvrit<!--Jg. 5:25.-->.
4:20	Il lui dit : Tiens-toi à l'entrée de la tente et s’il arrive qu’un homme vienne et t’interroge, en disant : Y a-t-il un homme ici ? Dis-lui : Non !
4:21	Ya`el, femme de Héber, prit un pieu de la tente, mit le marteau dans sa main, vint vers lui doucement et lui enfonça dans la tempe le pieu, qui pénétra en terre. Lui, profondément endormi, voleta ici et là et mourût.
4:22	Et voici, Barak poursuivait Sisera, Ya`el sortit au-devant de lui et lui dit : Viens, et je te montrerai l'homme que tu cherches. Barak entra chez elle, et voici, Sisera était étendu mort, et le pieu était dans sa tempe.
4:23	En ce jour-là, Elohîm humilia Yabiyn, roi de Kena'ân, devant les fils d'Israël.
4:24	La main des fils d'Israël avançait toujours et pesait durement sur Yabiyn, roi de Kena'ân, jusqu'à ce qu'ils l'eurent exterminé.

## Chapitre 5

### Cantique à la gloire de YHWH, l'Elohîm qui délivre

5:1	En ce jour-là, Déborah chanta ce cantique avec Barak, fils d'Abinoam, en disant :
5:2	Des chefs ont agi comme des meneurs en Israël, et le peuple s'est rendu bien disposé, bénissez YHWH !
5:3	Vous, rois, écoutez ! Vous, princes, prêtez l'oreille ! Moi, je chanterai à YHWH, je chanterai un hymne à YHWH, l'Elohîm d'Israël.
5:4	YHWH ! quand tu sortis de Séir, quand tu t'avanças des champs d'Édom, la Terre trembla, les cieux se fondirent, les nuées fondirent en eaux.
5:5	Les montagnes s'ébranlèrent devant YHWH, ce Sinaï devant YHWH, l'Elohîm d'Israël<!--Ps. 68:8-9.-->.
5:6	Aux jours de Shamgar, fils d'Anath, aux jours de Ya`el, les grandes routes étaient délaissées, et ceux qui voyageaient prenaient des chemins tortueux.
5:7	Le peuple de villages sans murailles se désistait en Israël, il se désistait jusqu'à ce que je me sois levée, moi Déborah, jusqu'à ce que je me sois levée pour être mère en Israël.
5:8	Choisissait-il des elohîm nouveaux ? Alors la guerre était aux portes. On ne voyait ni bouclier ni lance chez quarante milliers en Israël.
5:9	Mon cœur est avec les chefs d'Israël, qui se sont portés volontairement parmi le peuple. Bénissez YHWH,
5:10	vous qui montez sur les ânesses blanches, vous qui avez pour sièges des tapis et vous qui marchez sur la route, chantez !
5:11	Par la voix des archers entre les abreuvoirs, qu'on célèbre là les justices de YHWH et les justices du peuple de villages sans murailles en Israël. Alors le peuple de YHWH descendra aux portes.
5:12	Réveille-toi, réveille-toi, Déborah ! Réveille-toi, réveille-toi, dis le cantique, lève-toi Barak et emmène en captivité ceux que tu as faits captifs, toi fils d'Abinoam.
5:13	Alors un reste du peuple a dominé sur les puissants. YHWH m'a fait dominer sur les hommes vaillants.
5:14	Leur racine est depuis Éphraïm jusqu'à Amalek. Derrière toi Benyamin, parmi ta troupe. De Makir descendirent les chefs, et de Zebouloun ceux qui saisissent la verge du scribe.
5:15	Les chefs de Yissakar ont été avec Déborah, Yissakar est à côté de Barak. Il a été envoyé sur ses pas dans la vallée. Près des cours d'eau de Reouben, grandes ont été les résolutions du cœur !
5:16	Pourquoi es-tu resté entre les barres des étables, à écouter le sifflement des troupeaux ? Près des cours d'eau de Reouben, grandes ont été les investigations du cœur !
5:17	Galaad est resté au-delà du Yarden. Pourquoi Dan est-il resté sur ses navires ? Asher s'est tenu sur le rivage de la mer et s'est reposé dans ses ports.
5:18	Zebouloun est un peuple dont l'âme a défié la mort, Nephthali de même, sur les hauteurs des champs.
5:19	Les rois vinrent, ils combattirent. Alors combattirent les rois de Kena'ân, à Thaanac, près des eaux de Meguiddo. Mais ils ne remportèrent ni butin ni argent.
5:20	On a combattu des cieux, de leurs grandes routes les étoiles ont combattu contre Sisera.
5:21	Le torrent de Kison les a emportés, le torrent des anciens temps, le torrent de Kison. Mon âme tu as foulé aux pieds leur force !
5:22	Alors les talons des chevaux battirent le sol à cause de la course rapide, de la course rapide de ses puissants chevaux.
5:23	Maudissez Méroz, dit l'Ange de YHWH, maudissez, maudissez ses habitants, car ils ne sont pas venus au secours de YHWH, au secours de YHWH, avec les hommes vaillants.
5:24	Bénie soit parmi les femmes Ya`el, femme de Héber, le Qeynien, parmi les femmes qui habitent sous la tente, sois bénie ! 
5:25	Il demanda de l'eau, elle lui a donné du lait, elle lui a présenté de la crème dans la coupe des chefs.
5:26	Elle étendit sa main vers le pieu et sa droite vers le marteau des ouvriers. Elle a frappé Sisera, lui a fendu la tête, brisé et transpercé la tempe.
5:27	Entre ses pieds il s'est courbé, il est tombé, il s'est couché, entre ses pieds il s'est courbé, il est tombé. Là même où il s'est courbé, il est tombé détruit.
5:28	La mère de Sisera regardait par la fenêtre et criait d'une manière aiguë à travers le treillis : Pourquoi son char tarde-t-il à venir ? Pourquoi la marche de ses chars est-elle si lente ?
5:29	Les plus sages de ses princesses lui répondent, et elle se répète à elle-même leurs paroles :
5:30	N'est-ce pas parce qu'ils ont trouvé du butin ? N'est-ce pas parce qu'ils se le partagent ? Une fille, deux filles par tête d'homme fort, du butin en étoffes teintes pour Sisera, du butin en étoffes teintes brodées, deux étoffes teintes brodées, pour le cou des captifs.
5:31	Qu'ainsi périssent tous tes ennemis, YHWH ! Et que ceux qui t'aiment soient comme le soleil quand il sort dans sa force ! Et la terre fut en repos pendant 40 ans.

## Chapitre 6

### Israël assujetti par Madian

6:1	Or les fils d'Israël firent ce qui est mal aux yeux de YHWH et YHWH les livra entre les mains de Madian pendant 7 ans.
6:2	La main de Madian fut forte contre Israël. Pour échapper aux Madianites, les fils d'Israël se retiraient dans les ravins des montagnes, dans des cavernes et sur les rochers fortifiés.
6:3	Il arrivait que, quand Israël avait semé, Madian montait avec Amalek et les fils de l'orient, et ils montaient contre lui.
6:4	Ils faisaient un camp contre lui, ravageaient les fruits de la terre jusqu'à Gaza et ne laissaient en Israël ni vivres, ni brebis, ni bœufs, ni ânes.
6:5	Car ils montaient avec leurs troupeaux et leurs tentes, ils arrivaient comme une multitude de sauterelles, ils étaient innombrables, eux et leurs chameaux et ils venaient sur la terre pour la ravager.
6:6	Israël fut très appauvri en face de Madian, et les fils d'Israël crièrent à YHWH.
6:7	Il arriva que lorsque les fils d'Israël crièrent à YHWH à cause de Madian,
6:8	YHWH envoya un homme, un prophète aux fils d'Israël, qui leur dit : Ainsi parle YHWH, l'Elohîm d'Israël : Je vous ai fait monter hors d'Égypte et je vous ai fait sortir de la maison des esclaves.
6:9	Je vous ai délivrés de la main des Égyptiens et de la main de tous ceux qui vous opprimaient. Je les ai chassés devant vous et je vous ai donné leur terre.
6:10	Je vous ai dit : Je suis YHWH, votre Elohîm. Vous ne craindrez pas les elohîm des Amoréens, en terre desquels vous habitez. Mais vous n'avez pas obéi à ma voix.

### Guid'ôn (Gédéon) rencontre l'Ange de YHWH

6:11	L'Ange de YHWH vint et s'assit sous le térébinthe d'Ophrah, qui appartenait à Yoash, de la famille d'Abiézer. Guid'ôn, son fils, battait du blé au pressoir pour le mettre à l'abri de Madian.
6:12	L'Ange de YHWH se fit voir à lui et lui dit : Homme vaillant et talentueux, YHWH est avec toi !
6:13	Guid'ôn lui dit : Excuse-moi, mon Seigneur ! Si YHWH est avec nous, pourquoi toutes ces choses nous sont-elles arrivées ? Et où sont tous ces prodiges que nos pères nous ont racontés, en disant : YHWH ne nous a-t-il pas fait monter hors d'Égypte ? Maintenant YHWH nous a abandonnés et nous a livrés entre les mains des Madianites.
6:14	YHWH se tourna vers lui et dit : Va avec cette force que tu as et tu sauveras Israël de la main des Madianites. Ne t'ai-je pas envoyé<!--Hé. 11:32.--> ?
6:15	Et il lui dit : Excuse-moi, Adonaï ! Avec quoi sauverais-je Israël ? Voici, mon millier de bétail est le plus pauvre en Menashè et je suis le plus petit de la maison de mon père<!--1 S. 9:21, 16:11.-->.
6:16	YHWH lui dit : Parce que je serai avec toi, tu frapperas les Madianites comme s'ils n'étaient qu'un seul homme.
6:17	Et il lui dit : S’il te plaît, si j'ai trouvé grâce à tes yeux, accomplis pour moi un signe de ce dont, toi, tu me parles.
6:18	S’il te plaît, ne t'éloigne pas d'ici jusqu'à ce que je vienne vers toi. Je ferai sortir mon offrande de grain et la poserai devant toi. YHWH dit : Je resterai jusqu'à ce que tu reviennes.
6:19	Guid'ôn alla préparer un chevreau et fit avec un épha de farine des pains sans levain. Il mit la chair dans un panier et le jus dans un pot, puis il les sortit vers lui sous le térébinthe. Comme il s'approchait de lui, 
6:20	l'Ange d'Elohîm lui dit : Prends la chair et les pains sans levain et pose-les sur ce rocher<!--Voir commentaire en Es. 8:13-17.--> et répands le jus. Et il fit ainsi.
6:21	L'Ange de YHWH avança l'extrémité du bâton qu'il avait à la main, et toucha la chair et les pains sans levain. Le feu monta du rocher, et consuma la chair et les pains sans levain. Et l'Ange de YHWH s’en alla de devant ses yeux.
6:22	Guid'ôn, voyant que c'était l'Ange de YHWH dit : Malheur ! Adonaï YHWH ! car j'ai vu l'Ange de YHWH faces à faces.
6:23	YHWH lui dit : Shalôm ! N'aie pas peur, tu ne mourras pas.
6:24	Guid'ôn bâtit là un autel à YHWH, et l’appela YHWH-Shalôm. Il est encore à Ophrah d'Abiézer, jusqu’à ce jour.

### Guid'ôn (Gédéon) détruit les idoles ; YHWH lui confirme sa mission

6:25	Il arriva que pendant cette nuit-là, YHWH lui dit : Prends un jeune taureau d'entre les bœufs qui sont à ton père et un deuxième taureau de 7 ans. Détruis l'autel de Baal qui est à ton père et découpe l'Asherah qui est dessus.
6:26	Tu bâtiras un autel pour YHWH, ton Elohîm, sur le haut de ce rocher, avec le bois arrangé et tu prendras le second taureau, et tu le feras monter en holocauste avec le bois d'asherah que tu auras coupé.
6:27	Guid'ôn ayant pris 10 hommes parmi ses serviteurs, fit comme YHWH lui avait parlé. Mais il arriva que comme il craignait la maison de son père et les hommes de la ville, il ne le fit pas de jour, il le fit de nuit.
6:28	Lorsque les hommes de la ville se levèrent de bon matin, voici, l'autel de Baal était renversé, l'Asherah qui est dessus était découpé, et le deuxième taureau était monté en holocauste sur l'autel qui avait été bâti.
6:29	Ils se dirent, chaque homme à son compagnon : Qui a fait cela ? Et ils s'informèrent et firent des recherches. On leur dit : C'est Guid'ôn, fils de Yoash, qui a fait cela.
6:30	Les hommes de la ville dirent à Yoash : Fais sortir ton fils et qu'il meure, car il a renversé l'autel de Baal et découpé l'Asherah qui était dessus.
6:31	Yoash dit à tous ceux qui se tenaient près de lui : Est-ce à vous à combattre pour Baal ? Est-ce à vous de le sauver ? Quiconque combattra pour Baal sera mis à mort avant le matin. Si Baal est un elohîm, qu’il combatte pour lui-même, puisqu'on a renversé son autel.
6:32	Et on l'appela ce jour-là Yeroubbaal, en disant : Baal combattra contre lui, puisqu'il a démoli son autel.
6:33	Tout Madian, Amalek, et les fils de l'orient se réunirent ensemble. Ils passèrent le Yarden et campèrent dans la vallée de Yizre`e'l.
6:34	Guid'ôn fut revêtu de l'Esprit de YHWH. Il sonna du shofar et Abiézer fut convoqué derrière lui<!--Jg. 11:29, 13:25.-->.
6:35	Il envoya des messagers dans tout Menashè qui, lui aussi, fut convoqué derrière lui. Il envoya des messagers dans Asher, dans Zebouloun et dans Nephthali, qui montèrent à leur rencontre.
6:36	Guid'ôn dit à Elohîm : Si tu veux sauver Israël par ma main, comme tu l'as déclaré,
6:37	voici, je mets une toison de laine dans l'aire de battage. Si la rosée est sur la toison seule et que la sécheresse soit sur toute la terre, je saurai que tu sauveras Israël par ma main, comme tu l'as déclaré.
6:38	Et il arriva ainsi. Le lendemain, il se leva de bonne heure, pressa la toison et remplit une coupe de la rosée qui sortit de la toison.
6:39	Guid'ôn dit à Elohîm : Que ta colère ne s'enflamme pas contre moi ! Je parlerai seulement cette fois. S’il te plaît, que je fasse une épreuve avec la toison, pour cette fois seulement. S’il te plaît, qu’il n’y ait de la sécheresse que sur la toison et que sur toute la terre il y ait de la rosée.
6:40	Et Elohîm fit ainsi cette nuit-là : il n’y eut de sécheresse que sur la seule toison, et sur toute la terre il y eut de la rosée.

## Chapitre 7

### YHWH sélectionne un petit nombre pour le combat

7:1	Yeroubbaal qui est Guid'ôn, et tout le peuple qui était avec lui, se levèrent de bon matin et campèrent près de la source de Harod. Le camp de Madian était au nord, vers la colline de Moré, dans la vallée.
7:2	YHWH dit à Guid'ôn : Le peuple qui est avec toi est trop nombreux pour que je livre Madian entre ses mains, de peur qu'Israël ne se glorifie contre moi, en disant : C'est ma main qui m'a sauvé.
7:3	Maintenant, s’il te plaît, crie aux oreilles du peuple, en disant : Que celui qui est craintif et qui a peur s'en retourne et s'éloigne de la montagne de Galaad. 22 000 hommes parmi le peuple s'en retournèrent et il en resta 10 000<!--De. 20:8.-->.
7:4	YHWH dit à Guid'ôn : Le peuple est encore trop nombreux. Fais-les descendre vers l'eau et là je les épurerai<!--C'est Elohîm qui qualifie ses ouvriers, il les éprouve et les épure pour les rendre inébranlables. Voir l'épreuve des Hébreux dans le désert de Sinaï (De. 8).--> pour toi. Celui à propos duquel je te dirai : Que celui-ci aille avec toi, ira avec toi. Et celui à propos duquel je te dirai : Que celui-ci n'aille pas avec toi, n'ira pas avec toi.
7:5	Il fit descendre le peuple vers l'eau et YHWH dit à Guid'ôn : Tous ceux qui laperont l'eau avec la langue comme lape le chien, tu les sépareras de tous ceux qui plieront les genoux pour boire.
7:6	Ceux qui lapèrent l'eau en la portant à la bouche avec leur main furent au nombre de 300 hommes et tout le reste du peuple plia les genoux pour boire.
7:7	YHWH dit à Guid'ôn : C'est par les 300 hommes qui ont lapé, que je vous sauverai et que je livrerai Madian entre tes mains. Que tout le reste du peuple s'en aille, chaque homme chez soi.
7:8	Le peuple prit entre ses mains des provisions et ses shofars. Guid'ôn renvoya tous les hommes d'Israël, chaque homme dans sa tente et il retint les 300 hommes. Or le camp de Madian était au-dessous de lui, dans la vallée.

### Victoire de Guid'ôn (Gédéon) sur Madian

7:9	Et il arriva cette nuit-là que YHWH lui dit : Lève-toi, descends au camp car je l'ai livré entre tes mains.
7:10	Et si tu crains de descendre, descends vers le camp, toi et Poura, ton serviteur.
7:11	Tu écouteras ce qu'ils diront et après cela, tes mains seront fortifiées : descends au camp. Il descendit avec Poura, son serviteur, jusqu'aux avant-postes du camp.
7:12	Or Madian, Amalek et tous les fils de l'orient s'étalaient dans la vallée aussi nombreux que des sauterelles. Leurs chameaux étaient sans nombre, aussi nombreux que les grains de sable sur le bord de la mer<!--Jg. 6:3-5.-->.
7:13	Guid'ôn arriva, et voici, un homme racontait à son compagnon un rêve. Il lui disait : Voici un rêve que j'ai rêvé. Il me semblait qu'un gâteau de pain d'orge roulait dans le camp de Madian. Il est arrivé jusqu’à la tente, et l'a frappée, et elle est tombée. Il l'a retournée sens dessus dessous et la tente est tombée.
7:14	Son compagnon répondit et dit : Ce n'est pas autre chose que l'épée de Guid'ôn, fils de Yoash, homme d'Israël. Elohîm a livré Madian et tout le camp entre ses mains.
7:15	Il arriva que lorsque Guid'ôn entendit le récit du rêve et son interprétation, il se prosterna, revint au camp d'Israël, et dit : Levez-vous car YHWH a livré le camp de Madian entre vos mains.
7:16	Il divisa les 300 hommes en 3 corps et il leur donna à chacun des shofars à la main et des cruches vides, avec des torches dans les cruches.
7:17	Il leur dit : Regardez-moi faire et faites comme moi. Dès que je serai arrivé à l'extrémité du camp, ce que je ferai, vous le ferez aussi.
7:18	Quand je sonnerai du shofar, moi et tous ceux qui sont avec moi, vous aussi vous sonnerez du shofar tout autour du camp et vous direz : L'épée de YHWH et de Guid'ôn !
7:19	Guid'ôn et les 100 hommes qui étaient avec lui arrivèrent à l'extrémité du camp, au commencement de la veille de la nuit, alors qu'ils avaient fait lever, lever les gardes. Ils sonnèrent du shofar et brisèrent les cruches qu'ils avaient à la main.
7:20	Les trois corps sonnèrent du shofar et brisèrent les cruches. Ils saisirent de la main gauche les torches et de la main droite les shofars pour sonner et ils s'écrièrent : L'épée de YHWH et de Guid'ôn !
7:21	Ils se tenaient chaque homme à sa place autour du camp, et tout le camp se mit à courir çà et là, à pousser des cris et à prendre la fuite.
7:22	Les 300 hommes sonnèrent du shofar et, dans tout le camp, YHWH tourna l’épée de l’homme contre son compagnon. Le camp s'enfuit jusqu'à Beth-Shitta, vers Tseréra, jusqu'au bord d'Abel-Meholah, près de Tabbath<!--1 S. 14:20 ; Ez. 38:21.-->.
7:23	Les hommes d'Israël furent convoqués de Nephthali, d'Asher et de tout Menashè et ils poursuivirent Madian.
7:24	Guid'ôn envoya des messagers dans toute la montagne d'Éphraïm pour leur dire : Descendez à la rencontre de Madian, et saisissez-vous des eaux jusqu’à Beth-Bara et le Yarden. Tous les hommes d'Éphraïm furent convoqués et ils se saisirent des eaux jusqu'à Beth-Bara et du Yarden.
7:25	Ils saisirent deux des chefs de Madian, Oreb et Zeeb. Ils tuèrent Oreb au rocher d'Oreb, et ils tuèrent Zeeb au pressoir de Zeeb. Ils poursuivirent Madian, et ils apportèrent les têtes d'Oreb et de Zeeb à Guid'ôn de l'autre côté du Yarden<!--Es. 10:26 ; Ps. 83:11-12.-->.

## Chapitre 8

### Poursuite de Zébach et Tsalmounna ; exécution des rois de Madian

8:1	Les hommes d'Éphraïm dirent à Guid'ôn : Que signifie cette manière d'agir envers nous ? Pourquoi ne pas nous avoir appelés quand tu es allé à la guerre contre Madian ? Ils le contestèrent avec violence<!--Jg. 12:1.-->.
8:2	Il leur dit : Qu'ai-je fait en comparaison de vous ? Les grappillages d'Éphraïm ne sont-ils pas meilleurs que la vendange d'Abiézer ?
8:3	Elohîm a livré entre vos mains les chefs de Madian, Oreb et Zeeb. Qu'ai-je pu faire en comparaison de vous ? Et leur esprit fut apaisé envers lui, quand il leur eut dit cette parole.
8:4	Guid'ôn arriva au Yarden et il le passa, lui et les 300 hommes qui étaient avec lui, épuisés, mais poursuivant toujours.
8:5	Il dit aux hommes de Soukkoth : Donnez, s’il vous plaît, des pains ronds au peuple qui suit mes pas, car ils sont épuisés. Je suis à la poursuite de Zébach et de Tsalmounna, rois de Madian.
8:6	Mais les chefs de Soukkoth répondirent : Les paumes de Zébach et de Tsalmounna sont-elles déjà en ta main pour que nous donnions du pain à ton armée ?
8:7	Et Guid'ôn dit : Eh bien ! quand YHWH aura livré Zébach et Tsalmounna entre mes mains, je foulerai au pied votre chair avec des épines du désert et avec des chardons.
8:8	Et de là, il monta à Penouel et leur parla de la même manière. Et les hommes de Penouel lui répondirent comme les hommes de Soukkoth.
8:9	Et il parla aussi aux hommes de Penouel en disant : Quand je reviendrai en paix, je démolirai cette tour.
8:10	Zébach et Tsalmounna étaient à Karkor et leurs camps avec eux, environ 15 000 hommes, tous ceux qui étaient restés de tout le camp des fils de l'orient. Ceux qui étaient tombés étaient 120 000 hommes tirant l'épée.
8:11	Guid'ôn monta par le chemin de ceux qui habitent sous les tentes, à l'orient de Nobach et de Yogbehah, et il frappa le camp, pendant que le camp était en sécurité.
8:12	Et comme Zébach et Tsalmounna s'enfuyaient, il les poursuivit et prit les deux rois de Madian, Zébach et Tsalmounna, et mit en déroute tout leur camp<!--Ps. 83:11-12.-->.

### Vengeance sur Soukkoth et Penouel ; exécution de Zébach et Tsalmounna

8:13	Guid'ôn, fils de Yoash, revint de la bataille par la montée de Hérès.
8:14	Il saisit un garçon d'entre les hommes de Soukkoth, il l'interrogea. Celui-ci lui donna par écrit le nom des chefs et des anciens de Soukkoth, 77 hommes.
8:15	Et il vint auprès des hommes de Soukkoth, et leur dit : Voici Zébach et Tsalmounna, au sujet desquels vous m'avez insulté, en disant : Les paumes de Zébach et de Tsalmounna sont-elles déjà en ta main pour que nous donnions du pain à tes hommes fatigués ?
8:16	Il prit les anciens de la ville et châtia les hommes de Soukkoth avec des épines du désert et des chardons.
8:17	Il démolit la tour de Penouel et tua les hommes de la ville.
8:18	Il dit à Zébach et à Tsalmounna : Comment étaient les hommes que vous avez tués à Thabor ? Ils dirent : Tel tu es, tels étaient-ils, chacun d'eux avait l'air d'un fils de roi.
8:19	Il leur dit : C'étaient mes frères, fils de ma mère. YHWH est vivant, si vous les aviez laissés vivre, je ne vous tuerais pas.
8:20	Il dit à Yether, son premier-né : Lève-toi, tue-les ! Mais le jeune homme ne tira pas son épée, car il avait peur, parce qu'il était encore un jeune homme.
8:21	Zébach et Tsalmounna dirent : Lève-toi toi-même et jette-toi sur nous ! car tel est l'homme, telle est sa force. Et Guid'ôn se leva et tua Zébach et Tsalmounna. Il prit ensuite les croissants qui étaient aux cous de leurs chameaux.

### Guid'ôn (Gédéon) recommande au peuple le règne de YHWH

8:22	Les hommes d'Israël dirent tous d'un commun accord à Guid'ôn : Domine sur nous, et toi, et ton fils, et le fils de ton fils, car tu nous as sauvés de la main de Madian.
8:23	Guid'ôn leur dit : Je ne dominerai pas sur vous et mon fils ne dominera pas sur vous. C'est YHWH qui dominera sur vous<!--De. 17:15.-->.

### Guid'ôn (Gédéon) introduit une occasion de chute en Israël

8:24	Guid'ôn leur dit : J'ai une demande à vous faire : Donnez-moi chacun les anneaux que vous avez eus pour butin. Les ennemis avaient des anneaux d'or car ils étaient yishmaélites.
8:25	Ils dirent : Nous donnerons, nous donnerons. Et ils étendirent un manteau sur lequel chacun jeta les anneaux de son butin.
8:26	Le poids des anneaux d'or que Guid'ôn demanda fut de 1 700 sicles d'or, sans les croissants, les pendants d'oreilles, et les vêtements d'écarlate que portaient les rois de Madian, et sans les colliers qui étaient aux cous de leurs chameaux.
8:27	Guid'ôn en fit un éphod<!--Sous Moshé (Moïse), il y avait deux sortes d'éphods, le premier était de simple lin pour les prêtres, et le deuxième de broderie pour le grand-prêtre. Comme celui des simples prêtres n'avait rien de particulier, Moshé ne s'est pas arrêté à le décrire. Mais il a longuement décrit celui du grand-prêtre (Ex. 28:6-9). Il était composé d'or, d'étoffe violette, de pourpre rouge, d'écarlate de cochenille et de fin lin retors. C'était un tissu de différentes couleurs. Il y avait à l'endroit de l'éphod qui venait sur les deux épaules du grand-prêtre, deux grosses pierres précieuses, qui étaient chargées du nom des douze tribus d'Israël, six noms sur chaque pierre. À l'endroit où l'éphod se croisait sur la poitrine du grand-prêtre, il y avait un ornement carré, nommé le pectoral, en hébreu « choshen », dans lequel étaient enchâssées douze pierres précieuses, où l'on avait gravé les noms des douze tribus d'Israël ; un sur chacune des pierres.-->, et le mit dans sa ville, à Ophrah. Tout Israël s'y prostitua après lui et cela devint un piège pour Guid'ôn et pour sa maison.

### Fin de la vie de Guid'ôn (Gédéon) ; rechute d'Israël après sa mort

8:28	Madian fut humilié devant les fils d'Israël et il ne leva plus la tête. La terre fut en repos pendant 40 ans, durant les jours de Guid'ôn.
8:29	Yeroubbaal, fils de Yoash s’en alla et demeura dans sa maison.
8:30	Guid'ôn eut 70 fils, issus de ses reins, car il eut beaucoup de femmes.
8:31	Et sa concubine, qui était à Shekem, lui enfanta, elle aussi, un fils, et on l’appela du nom d'Abiymélek.
8:32	Guid'ôn, fils de Yoash, mourut après une heureuse vieillesse. Il fut enterré dans le sépulcre de Yoash, son père, à Ophrah, qui appartenait à la famille d'Abiézer.

### Retour à l'idolâtrie

8:33	Il arriva qu’après que Guid'ôn fut mort, que les fils d'Israël se détournèrent et se prostituèrent aux Baalim, et ils établirent Baal-Berith pour leur elohîm<!--Jg. 2:11-17, 10:6.-->.
8:34	Les fils d'Israël ne se souvinrent pas de YHWH, leur Elohîm, qui les avait délivrés de la main de tous leurs ennemis qui les entouraient.
8:35	Ils n'usèrent pas de bonté envers la maison de Yeroubbaal, de Guid'ôn, après tout le bien qu'il avait fait à Israël.

## Chapitre 9

### Conspiration d'Abiymélek pour régner sur Israël

9:1	Abiymélek, fils de Yeroubbaal, s'en alla à Shekem vers les frères de sa mère et leur parla, ainsi qu'à toute la maison du père de sa mère. Il leur dit :
9:2	S’il vous plaît, parlez aux oreilles de tous les seigneurs de Shekem : Qu'est-ce qui est bon pour vous ? Être dominés par 70 hommes, tous fils de Yeroubbaal, ou être dominés par un seul homme ? Souvenez-vous que je suis votre os et votre chair<!--Ge. 29:14.-->.
9:3	Les frères de sa mère dirent de sa part toutes ces paroles aux oreilles de tous les seigneurs de Shekem et leur cœur se tourna après Abiymélek, car ils disaient : C'est notre frère !
9:4	Ils lui donnèrent 70 sicles d'argent de la maison de Baal-Berith. Abiymélek s'en servit pour acheter des hommes misérables et turbulents, qui allèrent après lui.
9:5	Et il vint dans la maison de son père à Ophrah et tua sur une seule pierre ses frères, fils de Yeroubbaal, qui étaient 70 hommes. Il ne resta que Yotham, le plus jeune fils de Yeroubbaal, parce qu'il s'était caché.
9:6	Et tous les seigneurs de Shekem se rassemblèrent avec toute la maison de Millo. Ils vinrent et firent d'Abiymélek leur roi près du grand arbre érigé à Shekem.
9:7	On le rapporta à Yotham qui alla se tenir au sommet de la montagne de Garizim et les appelant, il dit en élevant la voix : Écoutez-moi, seigneurs de Shekem, et qu'Elohîm vous entende !
9:8	Les arbres allèrent, ils allèrent pour oindre un roi et ils dirent à l'olivier : Règne sur nous !
9:9	Mais l'olivier leur dit : Renoncerais-je à mon huile par laquelle Elohîm et les hommes sont honorés, pour aller m'agiter sur les arbres<!--Ps. 104:15.--> ?
9:10	Les arbres dirent au figuier : Viens, toi, règne sur nous !
9:11	Mais le figuier leur dit : Renoncerais-je à ma douceur et à mon bon fruit, pour aller m'agiter sur les arbres ?
9:12	Les arbres dirent à la vigne : Viens, toi, et règne sur nous !
9:13	La vigne leur dit : Renoncerais-je à mon vin nouveau qui réjouit Elohîm et les hommes, pour aller m'agiter sur les arbres ?
9:14	Tous les arbres dirent à l'épine : Viens, toi, et règne sur nous !
9:15	L'épine dit aux arbres : Si c'est en vérité que vous m'oignez pour roi, venez, et réfugiez-vous sous mon ombrage. Sinon, que le feu sorte de l'épine et qu'il dévore les cèdres du Liban !
9:16	Maintenant, si c'est avec vérité et avec intégrité que vous avez agi en proclamant Abiymélek comme roi, si vous avez usé de bonté à l'égard de Yeroubbaal et de sa maison, si vous avez agi envers lui comme le méritaient les bienfaits de ses mains,
9:17	alors que mon père a combattu pour vous, qu'il a exposé son âme au front, qu'il vous a délivrés de la main de Madian,
9:18	mais vous, vous vous êtes levés aujourd'hui contre la maison de mon père, vous avez tué ses fils, 70 hommes, sur une même pierre, et vous avez établi pour roi Abiymélek, fils de sa servante, sur les habitants de Shekem, parce qu'il est votre frère.
9:19	Si vous avez agi aujourd'hui avec vérité et intégrité envers Yeroubbaal et sa maison, réjouissez-vous au sujet d'Abiymélek et que lui aussi se réjouisse à votre sujet !
9:20	Sinon, que le feu sorte d'Abiymélek et qu'il dévore les seigneurs de Shekem et la maison de Millo, et que le feu sorte des seigneurs de Shekem, et de la maison de Millo, et qu'il dévore Abiymélek !
9:21	Et Yotham s'enfuit rapidement. Il s'en alla à Beer, où il demeura loin de la présence d'Abiymélek, son frère.

### Shekem se retourne contre Abiymélek

9:22	Abiymélek domina sur Israël pendant 3 ans.
9:23	Elohîm envoya un mauvais esprit entre Abiymélek et les seigneurs de Shekem, et les seigneurs de Shekem furent infidèles à Abiymélek.
9:24	Afin que la violence faite aux 70 fils de Yeroubbaal vienne et que leur sang soit mis sur Abiymélek, leur frère, qui les avait tués, et sur les seigneurs de Shekem, qui avaient fortifié ses mains pour tuer ses frères.
9:25	Les seigneurs de Shekem mirent des embûches sur le sommet des montagnes, des gens pillaient tous ceux qui passaient près d'eux sur le chemin. Cela fut rapporté à Abiymélek.
9:26	Gaal, fils d'Ébed, vint avec ses frères, et ils passèrent à Shekem. Les seigneurs de Shekem eurent confiance en lui.
9:27	Ils sortirent aux champs, vendangèrent leurs vignes, foulèrent les raisins et se livrèrent à des réjouissances. Ils entrèrent dans la maison de leur elohîm, ils mangèrent et burent, et ils maudirent Abiymélek.
9:28	Gaal, fils d'Ébed, dit : Qui est Abiymélek, et qui est Shekem pour que nous le servions ? N'est-il pas le fils de Yeroubbaal, et Zeboul n'est-il pas son commissaire ? Servez plutôt les hommes de Hamor, père de Shekem, mais pour quelle raison servirions-nous Abiymélek ?
9:29	Qui me mettra ce peuple dans la main ? Je chasserais Abiymélek ! Et il disait d'Abiymélek : Multiplie ton armée et sors !
9:30	Zeboul, gouverneur de la ville, entendit les paroles de Gaal, fils d'Ébed, et sa colère s'enflamma.
9:31	Il envoya secrètement des messagers vers Abiymélek pour lui dire : Voici, Gaal, fils d'Ébed, et ses frères sont entrés dans Shekem, et voici, ils assiègent la ville contre toi.
9:32	Maintenant, lève-toi de nuit, toi et le peuple qui est avec toi, et mets-toi en embuscade dans les champs.
9:33	Il arrivera que le matin, au lever du soleil, tu te lèveras et tu fondras sur la ville. Voici, lui et le peuple qui est avec lui sortiront contre toi, et tu lui feras selon ce que ta main trouvera.
9:34	Abiymélek et tout le peuple qui était avec lui se levèrent de nuit, et ils se mirent en embuscade contre Shekem, divisés en 4 bandes.
9:35	Gaal, fils d'Ébed, sortit et se tint à l'entrée de la porte de la ville. Abiymélek et tout le peuple qui était avec lui se levèrent de l'embuscade.
9:36	Gaal voyant le peuple, dit à Zeboul : Voici un peuple qui descend du sommet des montagnes. Zeboul lui dit : Tu vois l'ombre des montagnes comme des hommes.
9:37	Gaal parla encore et dit : C'est bien un peuple qui descend du nombril<!--Ez. 38:12.--> de la terre et une bande vient du chemin du grand arbre de ceux qui pratiquent le spiritisme.
9:38	Et Zeboul lui dit : Où est ta bouche, toi qui disais : Qui est Abiymélek, pour que nous le servions ? N'est-ce pas ici ce peuple que tu méprisais ? Sors maintenant, s’il te plaît, et combats !
9:39	Gaal sortit conduisant les seigneurs de Shekem et combattit contre Abiymélek.
9:40	Abiymélek le poursuivit et il s'enfuit de devant lui, et plusieurs tombèrent morts jusqu'à l'entrée de la porte.
9:41	Abiymélek s'arrêta à Arouma. Zeboul repoussa Gaal et ses frères afin qu'ils ne restent plus à Shekem.
9:42	Et il arriva, dès le lendemain, que le peuple sortit aux champs. Cela fut rapporté à Abiymélek,
9:43	qui prit son peuple, et le divisa en 3 bandes, et les mit en embuscade dans les champs. Ayant vu que le peuple sortait de la ville, il se leva contre eux, et les battit.
9:44	Abiymélek et la bande qui était avec lui se répandirent, et se tinrent à l'entrée de la porte de la ville ; mais les deux autres bandes se jetèrent sur tous ceux qui étaient aux champs, et les battirent.
9:45	Abiymélek combattit contre la ville toute la journée. Il prit la ville et tua le peuple qui y était. Il la rasa et y sema du sel.
9:46	Ayant appris cela, tous les seigneurs de la tour de Shekem entrèrent dans la forteresse de la maison d'Él-Berit<!--Jg. 8:33, 9:4.-->.
9:47	On rapporta à Abiymélek que tous les seigneurs de la tour de Shekem s'étaient rassemblés.
9:48	Abiymélek monta sur la montagne de Tsalmon, lui et tout le peuple qui était avec lui. Il prit en main une hache, coupa une branche d'arbre, et l'ayant mise sur son épaule, la porta, et dit au peuple qui était avec lui : Avez-vous vu ce que j'ai fait ? Hâtez-vous de faire comme moi.
9:49	Tout le peuple coupa aussi chaque homme une branche, et ils marchèrent derrière Abiymélek. Ils les mirent tout autour de la forteresse, et ils brûlèrent par le feu la forteresse sur eux, et tous les hommes de la tour de Shekem moururent aussi, environ 1 000, hommes et femmes.

### Abiymélek meurt

9:50	Abiymélek marcha contre Thébets, y mit son camp et la prit.
9:51	Il y avait au milieu de la ville une forte tour, où s'enfuirent tous les hommes et toutes les femmes, et tous les seigneurs de la ville, et ayant fermé les portes après eux, ils montèrent sur le toit de la tour.
9:52	Abiymélek alla jusqu'à la tour, l'attaqua et s'approcha jusqu'à la porte de la tour pour la brûler par le feu.
9:53	Mais une femme jeta une pièce de meule de moulin sur la tête d'Abiymélek et lui brisa le crâne<!--2 S. 11:21.-->.
9:54	Rapidement, il appela le garçon qui portait ses armes et lui dit : Tire ton épée, et tue-moi, de peur qu'on ne dise de moi : C'est une femme qui l'a tué. Le garçon le transperça et il mourut<!--1 S. 31:4.-->.
9:55	Quand les hommes d'Israël virent qu'Abiymélek était mort, ils s'en allèrent chaque homme en son lieu.
9:56	Elohîm fit retourner à Abiymélek le mal qu'il avait fait contre son père en tuant ses 70 frères,
9:57	et tout le mal des hommes de Shekem, Elohîm le fit revenir sur leur tête. C'est ainsi qu'ils furent atteints par la malédiction de Yotham<!--Jg. 9:10-21.-->, fils de Yeroubbaal.

## Chapitre 10

### Thola, juge en Israël

10:1	Après Abiymélek, Thola fils de Pouah, fils de Dodo, homme de Yissakar, se leva pour sauver Israël. Il habitait à Shamir, dans la montagne d'Éphraïm.
10:2	Il jugea Israël pendant 23 ans, puis il mourut et fut enterré à Shamir.

### Yaïr, juge en Israël

10:3	Après lui, se leva Yaïr, le Galaadite, qui jugea Israël pendant 22 ans.
10:4	Il avait 30 fils qui montaient sur 30 ânons et qui avaient 30 villes que l'on appelle jusqu'à ce jour villages de Yaïr. Elles sont situées en terre de Galaad.
10:5	Et Yaïr mourut et fut enterré à Kamon.

### Idolâtrie d'Israël et oppression par ses ennemis

10:6	Les fils d'Israël firent encore ce qui est mal aux yeux de YHWH. Ils servirent les Baalim et les Astartés, les elohîm de Syrie, les elohîm de Sidon, les elohîm de Moab, les elohîm des fils d'Ammon et les elohîm des Philistins, et ils abandonnèrent YHWH, et ne le servirent plus<!--Jg. 2:11, 3:7, 8:33.-->.
10:7	La colère de YHWH s'enflamma contre Israël et il les vendit entre les mains des Philistins et entre les mains des fils d'Ammon.
10:8	Ils brisèrent et écrasèrent les fils d'Israël cette année-là, et pendant 18 ans tous les fils d'Israël qui étaient au-delà du Yarden, en terre des Amoréens en Galaad.
10:9	Même les fils d'Ammon passèrent le Yarden pour combattre contre Yéhouda, contre Benyamin et contre la maison d'Éphraïm. Israël fut très déprimé.
10:10	Les fils d'Israël crièrent à YHWH en disant : Nous avons péché contre toi, car nous avons abandonné notre Elohîm et nous avons servi les Baalim.
10:11	YHWH dit aux fils d'Israël : N'avez-vous pas été opprimés par les Égyptiens, les Amoréens, les fils d'Ammon et les Philistins ?
10:12	Lorsque les Sidoniens, Amalek et Maon vous ont opprimés, et que vous avez crié vers moi, ne vous ai-je pas sauvés de leurs mains ?
10:13	Mais vous, vous m'avez abandonné, et vous avez servi d'autres elohîm. C'est pourquoi je n’ajouterai pas à vous sauver.
10:14	Allez et criez vers les elohîm que vous avez choisis. Qu’ils vous sauvent, eux, au temps de votre détresse !
10:15	Mais les fils d'Israël dirent à YHWH : Nous avons péché. Traite-nous selon tout ce qui sera bon à tes yeux. Seulement, s’il te plaît, délivre-nous aujourd’hui !
10:16	Ils ôtèrent du milieu d'eux les elohîm étrangers et servirent YHWH, et son âme fut affligée à cause du malheur d’Israël.
10:17	Les fils d'Ammon furent convoqués et campèrent en Galaad, et les fils d'Israël se rassemblèrent et campèrent à Mitspah.
10:18	Le peuple et les chefs de Galaad se dirent, chaque homme à son compagnon : Qui sera l'homme qui commencera à combattre contre les fils d'Ammon ? Il deviendra la tête de tous les habitants de Galaad.

## Chapitre 11

### Yiphtah (Jephthé), juge en Israël

11:1	Or Yiphtah le Galaadite était devenu un homme vaillant et talentueux. Il était le fils d'une femme prostituée et c'est Galaad qui l'avait engendré.
11:2	La femme de Galaad lui enfanta des fils. Quand les fils de cette femme devinrent grands, ils chassèrent Yiphtah, en lui disant : Tu n'auras pas d'héritage dans la maison de notre père, car tu es fils d'une autre femme.
11:3	Yiphtah s’enfuit, loin, face à ses frères et habita en terre de Tob. Des hommes sans valeurs se rassemblèrent auprès de Yiphtah, et ils sortirent dehors avec lui<!--Jg. 9:4 ; 1 S. 22:2.-->.
11:4	Il arriva, quelque temps après, que les fils d'Ammon firent la guerre à Israël.
11:5	Il arriva quand les fils d'Ammon faisaient la guerre à Israël, que les anciens de Galaad allèrent prendre Yiphtah en terre de Tob.
11:6	Ils dirent à Yiphtah : Viens, et deviens notre chef et nous combattrons contre les fils d'Ammon.
11:7	Yiphtah dit aux anciens de Galaad : N'est-ce pas vous qui m'avez haï et chassé de la maison de mon père ? Pourquoi êtes-vous venus à moi maintenant que vous êtes dans la détresse ?
11:8	Les anciens de Galaad dirent à Yiphtah : La raison pour laquelle nous revenons vers toi maintenant, c'est afin que tu viennes avec nous, que tu combattes contre les fils d'Ammon, et que tu deviennes pour nous la tête de tous les habitants de Galaad.
11:9	Yiphtah dit aux anciens de Galaad : Si vous me faites revenir pour combattre contre les fils d'Ammon et que YHWH les livre devant moi, je deviendrai votre tête.
11:10	Les anciens de Galaad dirent à Yiphtah : Que YHWH nous entende et qu'il juge, si nous ne faisons pas ce que tu dis.
11:11	Yiphtah s'en alla avec les anciens de Galaad. Le peuple le mit à sa tête et l'établit pour chef, et Yiphtah déclara devant YHWH, à Mitspah, toutes les paroles qu'il avait dites.
11:12	Yiphtah envoya des messagers au roi des fils d'Ammon pour lui dire : Qu'y a-t-il entre toi et moi, que tu viennes contre moi pour faire la guerre à ma terre ?
11:13	Le roi des fils d'Ammon dit aux messagers de Yiphtah : C'est parce qu'Israël a pris ma terre quand il est monté d'Égypte, depuis l'Arnon jusqu'à Yabboq, et même jusqu'au Yarden. Maintenant rends-la en paix.
11:14	Yiphtah continua à envoyer encore des messagers au roi des fils d'Ammon,
11:15	pour lui dire : Ainsi parle Yiphtah : Israël n’a pas pris la terre de Moab ni la terre des fils d'Ammon.
11:16	En effet, lorsque Israël est monté d'Égypte, il a marché dans le désert jusqu'à la Mer Rouge et il a atteint Qadesh.
11:17	Israël envoya des messagers au roi d'Édom, pour lui dire : Laisse-moi, s’il te plaît, passer par ta terre. Mais le roi d'Édom ne l'écouta pas. Il en envoya aussi au roi de Moab qui ne consentit pas, et Israël demeura à Qadesh.
11:18	Il marcha dans le désert, contourna la terre d'Édom et la terre de Moab, et vint du côté du soleil levant en terre de Moab. Il campa au-delà de l'Arnon, sans entrer dans les frontières de Moab, car l'Arnon était la frontière de Moab.
11:19	Israël envoya des messagers à Sihon, roi des Amoréens, roi de Hesbon, auquel Israël dit : Laisse-nous, s’il te plaît, passer par ta terre jusqu'à mon lieu.
11:20	Mais Sihon n'eut pas confiance en Israël pour le laisser passer sur son territoire. Il rassembla tout son peuple, ils campèrent vers Yahats, et combattirent contre Israël.
11:21	Et YHWH, l'Elohîm d'Israël, livra Sihon et tout son peuple entre les mains d'Israël, qui les battit. Israël prit possession de toute la terre des Amoréens qui habitaient cette terre.
11:22	Ils prirent possession de toute la terre des Amoréens, depuis l'Arnon jusqu'à Yabboq, et depuis le désert jusqu'au Yarden.
11:23	Et maintenant YHWH, l'Elohîm d'Israël, a dépossédé les Amoréens devant son peuple d'Israël, et toi, tu nous en déposséderais ?
11:24	N’est-ce pas celui que Kemosh, ton elohîm te fait déposséder que tu déposséderas ? Et tous ceux que YHWH, notre Elohîm, a dépossédés devant nous, c’est eux que nous déposséderons.
11:25	Et maintenant vaux-tu mieux que Balak, fils de Tsippor, roi de Moab ? A-t-il combattu, combattu contre Israël ?
11:26	Voilà 300 ans qu'Israël demeure à Hesbon et ses filles, à Aroër et ses filles, et dans toutes les villes qui sont le long de l'Arnon : Pourquoi ne les avez-vous pas saisies pendant ce temps-là ?
11:27	Ce n'est pas moi qui ai péché contre toi, c'est toi qui agis mal envers moi en me faisant la guerre. Que YHWH, le Juge, juge aujourd’hui entre les fils d’Israël et les fils d'Ammon !
11:28	Le roi des fils d'Ammon n'écouta pas les paroles que Yiphtah lui avait envoyées.
11:29	L'Esprit de YHWH vint sur Yiphtah. Il passa par Galaad et Menashè, et passa par Mitspé de Galaad. De Mitspé de Galaad, il passa chez les fils d'Ammon.

### Yiphtah (Jephthé) fait un vœu ; Ammon livré entre ses mains

11:30	Yiphtah fit un vœu à YHWH, et dit : Si tu livres, si tu livres les fils d'Ammon entre mes mains,
11:31	il arrivera que quiconque sortira, sortira des portes de ma maison à ma rencontre, quand je reviendrai en paix de chez les fils d'Ammon, sera consacré à YHWH et je le ferai monter en holocauste.
11:32	Yiphtah passa chez les fils d'Ammon pour combattre contre eux, et YHWH les livra entre ses mains.
11:33	Il les battit par une très grande défaite, depuis Aroër jusqu'à Minnith – 20 villes – et jusqu'à Abel-Keramim. Et les fils d'Ammon furent humiliés devant les fils d'Israël.
11:34	Comme Yiphtah venait à Mitspah dans sa maison, voici, sa fille sortit à sa rencontre avec des tambourins et des danses. Elle était seule, unique : il n’avait, à part elle, ni fils ni fille.
11:35	Il arriva qu'aussitôt qu'il l'eut aperçue, il déchira ses vêtements, et dit : Ah ! Ma fille ! Tu me fais fléchir les genoux, tu me fais fléchir les genoux, tu es du nombre de ceux qui me troublent ! J'ai ouvert ma bouche devant YHWH, et je ne peux pas revenir en arrière.
11:36	Elle dit : Mon père, si tu as ouvert ta bouche devant YHWH, traite-moi selon ce qui est sorti de ta bouche, puisque YHWH a accompli pour toi la vengeance contre tes ennemis, les fils d'Ammon.
11:37	Elle dit à son père : Que cette chose me soit faite : Laisse-moi pendant deux mois ! Je m'en irai, je descendrai par les montagnes, et je pleurerai ma virginité avec mes compagnes.
11:38	Il dit : Va ! Et il la laissa aller pour deux mois. Elle s'en alla avec ses compagnes, et pleura sa virginité dans les montagnes.
11:39	Et il arriva, au bout de deux mois, qu’elle retourna vers son père, et il accomplit sur elle le vœu qu'il avait fait<!--YHWH interdit les sacrifices humains (Lé. 18:21, 20:2-5, 21 ; De. 12:31, 18:10).-->. Elle n'avait pas connu d'homme. Dès lors, ce fut une coutume en Israël,
11:40	d'année en année, les filles d'Israël s'en vont célébrer la fille de Yiphtah, le Galaadite, 4 jours par an.

## Chapitre 12

### Querelle entre Yiphtah (Jephthé) et Éphraïm

12:1	Les hommes d'Éphraïm se rassemblèrent et passèrent vers le nord pour dire à Yiphtah : Pourquoi es-tu passé pour combattre contre les fils d'Ammon, sans nous avoir appelés pour aller avec toi ? Nous brûlerons ta maison, et toi aussi<!--Jg. 8:1.-->.
12:2	Et Yiphtah leur dit : J'ai été un homme de grande lutte, moi et mon peuple, contre les fils d'Ammon, et quand je vous ai appelés, vous ne m'avez pas délivré de leurs mains.
12:3	Voyant que vous ne me délivriez pas, j'ai pris mon âme dans ma paume et je suis passé chez les fils d'Ammon. YHWH les a livrés entre mes mains. Pourquoi aujourd'hui montez-vous vers moi pour me faire la guerre ?
12:4	Yiphtah rassembla tous les hommes de Galaad et combattit contre Éphraïm. Les hommes de Galaad battirent Éphraïm, parce qu'ils disaient : Vous êtes des fugitifs d'Éphraïm ! Galaad est au milieu d'Éphraïm, au milieu de Menashè !
12:5	Les Galaadites se saisirent des gués du Yarden du côté d'Éphraïm. Il arriva que, quand l'un des fuyards d'Éphraïm disait : Laissez-moi passer, les hommes de Galaad lui disaient : Es-tu Éphraïmite ? Il disait : Non.
12:6	On lui disait : « S’il te plaît, dis “Shibboleth” » et il disait « Sibboleth », n’étant pas prêt à parler correctement, ils le saisissaient et le tuaient aux gués du Yarden. En ce temps-là 42 000 hommes d'Éphraïm tombèrent.
12:7	Yiphtah jugea Israël pendant 6 ans, puis Yiphtah, le Galaadite, mourut et fut enterré dans sa ville en Galaad.

### Ibtsan, juge en Israël

12:8	Après lui, Ibtsan de Bethléhem jugea Israël.
12:9	Il eut 30 fils, et 30 filles qu'il envoya au dehors, et il fit venir du dehors 30 filles pour ses fils. Il jugea Israël pendant 7 ans.
12:10	Et Ibtsan mourut et fut enterré à Bethléhem.

### Eylon, juge en Israël

12:11	Après lui, Eylon de Zebouloun jugea Israël pendant 10 ans.
12:12	Et Eylon de Zebouloun mourut et fut enterré à Ayalon, en terre de Zebouloun.

### Abdon, juge en Israël

12:13	Après lui, Abdon fils d'Hillel, le Pirathonite, jugea Israël.
12:14	Il eut 40 fils et 30 fils des fils, qui montaient sur 70 ânons. Il jugea Israël pendant 8 ans<!--Jg. 10:4.-->.
12:15	Et Abdon, fils d'Hillel, le Pirathonite, mourut et fut enterré à Pirathon, en terre d'Éphraïm, sur la montagne des Amalécites.

## Chapitre 13

### YHWH livre Israël aux Philistins

13:1	Les fils d'Israël firent encore ce qui est mal aux yeux de YHWH et YHWH les livra entre les mains des Philistins pendant 40 ans.

### Annonce de la naissance de Shimshôn (Samson)

13:2	Il y avait un homme de Tsor`ah, de la famille des Danites, dont le nom était Manoach. Sa femme était stérile et n'enfantait pas.
13:3	L'Ange de YHWH se fit voir à la femme, et lui dit : Te voici maintenant stérile et tu n’enfantes pas. Mais tu deviendras enceinte et tu enfanteras un fils.
13:4	Maintenant, prends garde, s’il te plaît ! Ne bois ni vin ni boisson forte, et ne mange rien d’impur !
13:5	Car voici, tu vas devenir enceinte et tu enfanteras un fils. Le rasoir ne passera pas sur sa tête, car ce garçon sera nazaréen<!--Nazaréen vient du mot « nazir » qui signifie « consacré », « naziréen » ou « séparé ». Voir No. 6.--> d'Elohîm dès le ventre. C'est lui qui commencera à délivrer Israël de la main des Philistins.
13:6	La femme vint et parla à son homme, en disant : Un homme d'Elohîm est venu vers moi, et son aspect était comme l'aspect de l'Ange d'Elohîm, extrêmement redoutable. Je ne lui ai pas demandé d'où il était et il ne m'a pas déclaré son nom.
13:7	Il m'a dit : Voici, tu vas devenir enceinte et tu enfanteras un fils. Maintenant, ne bois ni vin ni boisson forte, et ne mange aucune chose impure, car cet enfant sera nazaréen d'Elohîm dès le ventre jusqu'au jour de sa mort.

### Prière de Manoach exaucée ; rencontre avec l'Ange de YHWH

13:8	Manoach implora YHWH en disant : Excuse-moi, Adonaï ! Que l'homme d'Elohîm que tu as envoyé vienne encore vers nous, s'il te plaît, et qu'il nous enseigne ce que nous devons faire au garçon quand il naîtra !
13:9	Elohîm entendit la voix de Manoach, et l'Ange d'Elohîm vint encore vers la femme pendant qu'elle était assise dans un champ. Mais Manoach, son homme, n'était pas avec elle.
13:10	La femme se hâta de courir informer son homme et lui dit : Voici, l'homme qui était venu vers moi l'autre jour m'est apparu.
13:11	Manoach se leva, suivit sa femme et venant vers l'homme, il lui dit : Es-tu cet homme qui a parlé à cette femme ? Il répondit : C'est moi !
13:12	Manoach dit : Maintenant, quand ta parole arrivera, quel sera le jugement du garçon et son œuvre ?
13:13	L'Ange de YHWH dit à Manoach : La femme se gardera de tout ce que je lui ai dit.
13:14	Elle ne mangera rien qui sorte de la vigne à vin, elle ne boira ni vin ni boisson forte et ne mangera aucune chose impure. Elle prendra garde à tout ce que je lui ai ordonné.
13:15	Manoach dit à l'Ange de YHWH : S'il te plaît, que nous te retenions et que nous préparions un chevreau en ta présence.
13:16	L'Ange de YHWH dit à Manoach : Même si tu me retenais, je ne mangerais pas de ton pain. Mais si tu fais un holocauste, tu le feras monter pour YHWH. Manoach ne savait pas que c'était l'Ange de YHWH.
13:17	Et Manoach dit à l'Ange de YHWH : Quel est ton nom, afin que nous te donnions gloire lorsque ta parole arrivera ?
13:18	L'Ange de YHWH lui dit : Pourquoi demandes-tu mon nom ? Il est merveilleux.
13:19	Manoach prit un jeune chevreau, une offrande de grain et les fit monter sur le rocher pour YHWH. Il se produisit une chose merveilleuse à la vue de Manoach et de sa femme.
13:20	Il arriva que comme la flamme montait de dessus l'autel vers les cieux, l'Ange de YHWH monta aussi avec la flamme de l'autel. Voyant cela, Manoach et sa femme tombèrent sur leurs faces, à terre.
13:21	L'Ange de YHWH n'apparut plus à Manoach ni à sa femme. Alors Manoach sut que c'était l'Ange de YHWH.
13:22	Et Manoach dit à sa femme : Nous mourrons ! Nous mourrons, car nous avons vu Elohîm.
13:23	Mais sa femme lui dit : Si YHWH désirait nous faire mourir, il n'aurait pas pris de nos mains l'holocauste ni l'offrande de grain, et il ne nous aurait pas fait voir tout cela, ni, en un tel temps, fait entendre des choses pareilles.

### Naissance de Shimshôn (Samson)

13:24	La femme enfanta un fils et l'appela du nom de Shimshôn<!--Samson.-->. Le garçon devint grand et YHWH le bénit.
13:25	L'Esprit de YHWH commença à l'agiter à Machané-Dan<!--Qui signifie « camp de Dan ».-->, entre Tsor`ah et Eshthaol.

## Chapitre 14

### Shimshôn (Samson) veut épouser une femme philistine, YHWH au contrôle de la situation

14:1	Shimshôn descendit à Timnah, et il y vit une femme d'entre les filles des Philistins.
14:2	Et lorsqu'il fut monté, il le déclara à son père et à sa mère, en disant : J'ai vu une femme à Timnah d'entre les filles des Philistins. Prenez-la maintenant afin qu'elle soit ma femme.
14:3	Son père et sa mère lui dirent : N'y a-t-il pas de femme parmi les filles de tes frères et parmi tout notre peuple, pour que tu ailles prendre une femme d'entre les Philistins, ces incirconcis ? Et Shimshôn dit à son père : Prenez-la pour moi, car elle est droite à mes yeux.
14:4	Son père et sa mère ne savaient pas que c’était de YHWH car il cherchait une occasion contre les Philistins. Or en ce temps-là, les Philistins dominaient sur Israël.

### Festin des noces ; énigme de Shimshôn (Samson)

14:5	Shimshôn descendit avec son père et sa mère à Timnah. Ils allèrent jusqu'aux vignes de Timnah, et voici qu'un jeune lion rugissait à sa rencontre.
14:6	L'Esprit de YHWH fondit sur lui et, sans avoir rien à la main, il déchira le lion comme on déchire un chevreau. Il ne raconta pas à son père ni à sa mère ce qu'il avait fait<!--1 S. 17:34-35.-->.
14:7	Il descendit et parla à la femme, et elle fut trouvée droite aux yeux de Shimshôn.
14:8	Retournant quelques jours après pour la prendre, il se détourna pour voir la carcasse du lion. Et voici, il y avait dans la carcasse du lion un essaim d'abeilles et du miel.
14:9	Il en prit entre ses paumes, et il s’en alla, marchant et mangeant. Lorsqu'il fut arrivé vers son père et sa mère, il leur en donna et ils mangèrent, mais il ne leur déclara pas qu'il avait pris ce miel dans la carcasse du lion.
14:10	Son père descendit chez la femme et là, Shimshôn fit un festin, car c'est ainsi qu'agissaient les jeunes hommes.
14:11	Il arriva que quand ils le virent, ils prirent 30 compagnons qui furent avec lui.
14:12	Shimshôn leur dit : Laissez-moi vous proposer une énigme, s'il vous plaît. Si vous me l'expliquez, si vous me l'expliquez au cours des 7 jours du festin et si vous la trouvez, je vous donnerai 30 chemises et 30 vêtements de rechange.
14:13	Mais si vous ne pouvez pas me l'expliquer, vous me donnerez 30 chemises et 30 vêtements de rechange. Ils lui répondirent : Propose ton énigme, et nous l'écouterons.
14:14	Il leur dit : De celui qui mange est sorti ce qui se mange, et du fort est sorti le doux. Pendant 3 jours, ils ne purent pas expliquer l'énigme.
14:15	Il arriva, au septième jour, qu'ils dirent à la femme de Shimshôn : Persuade ton mari de nous expliquer l'énigme, sinon, nous te brûlerons au feu, toi et la maison de ton père. C'est pour nous déposséder que vous nous avez appelés ici, n'est-ce pas ?
14:16	La femme de Shimshôn pleurait près de lui et disait : Tu me hais seulement, tu ne m’aimes pas. Tu as proposé une énigme aux fils de mon peuple, et à moi, tu ne me l'as pas déclarée ! Il lui dit : Voici, à mon père et à ma mère je n’ai rien déclaré, et à toi, la déclarerais-je ? 
14:17	Elle pleura auprès de lui pendant les sept jours qu’ils eurent le festin. Et il arriva, au septième jour, qu'il la lui expliqua, car elle le tourmentait. Et elle expliqua l’énigme aux fils de son peuple.
14:18	Les hommes de la ville lui dirent au septième jour, avant le coucher du soleil : Qu'y a-t-il de plus doux que le miel et qu'y a-t-il de plus fort que le lion ? Et il leur dit : Si vous n'aviez pas labouré avec ma génisse vous n'auriez pas trouvé mon énigme.
14:19	L'Esprit de YHWH fondit sur lui et il descendit à Askalon. Il y tua 30 hommes, prit leurs dépouilles, et donna les vêtements de rechange à ceux qui avaient expliqué l'énigme. Sa colère s'enflamma et il monta à la maison de son père.
14:20	La femme de Shimshôn devint celle de son compagnon qu'il s'était choisi pour ami.

## Chapitre 15

### Querelle entre Shimshôn (Samson) et les Philistins

15:1	Il arriva quelques jours après, au jour de la moisson des blés, que Shimshôn alla visiter sa femme avec un jeune chevreau. Il dit : J'entrerai vers ma femme dans sa chambre. Mais le père de sa femme ne lui permit pas d'y entrer.
15:2	Et le père dit : Je me suis dit que tu la haïssais, tu la haïssais, c'est pourquoi je l'ai donnée à ton compagnon. Sa jeune sœur n'est-elle pas meilleure qu'elle ? S’il te plaît, qu’elle devienne tienne à la place de l’autre.
15:3	Shimshôn leur dit : Cette fois je serai innocent à l'égard des Philistins si je leur fais du mal.
15:4	Shimshôn s'en alla. Il captura 300 renards et prit aussi des torches. Il tourna les renards queue contre queue, et mit une torche entre les deux queues, au milieu.
15:5	Allumant le feu aux torches, il les lâcha dans les blés des Philistins et alluma le tas de gerbes, le blé sur pied jusqu'aux plantations d'oliviers.
15:6	Les Philistins dirent : Qui a fait cela ? Et l’on dit : Shimshôn, le gendre du Thimnien, parce qu'il lui a pris sa femme et l'a donnée à son compagnon. Les Philistins montèrent, et ils la brûlèrent au feu, elle avec son père.
15:7	Shimshôn leur dit : Est-ce ainsi que vous agissez ? Je ne cesserai qu'après m'être vengé de vous.
15:8	Il les frappa jambes sur cuisses, d'un grand coup. Et il descendit et demeura dans une caverne du rocher d'Étam.

### Shimshôn (Samson) livré aux Philistins par Yéhouda (Juda) ; 1 000 hommes tués avec une mâchoire d'âne

15:9	Les Philistins montèrent, campèrent en Yéhouda et se déployèrent jusqu'à Léchi.
15:10	Les hommes de Yéhouda dirent : Pourquoi êtes-vous montés contre nous ? Ils dirent : Nous sommes montés pour lier Shimshôn, afin de le traiter comme il nous a traités.
15:11	3 000 hommes de Yéhouda descendirent à la caverne du rocher d'Étam, et dirent à Shimshôn : Ne sais-tu pas que les Philistins dominent sur nous ? Que nous as-tu fait ? Il leur dit : Je les ai traités comme ils m'ont traité.
15:12	Ils lui dirent : Nous sommes descendus pour te lier afin de te livrer entre les mains des Philistins. Shimshôn leur dit : Jurez-moi que vous ne me tuerez pas.
15:13	Ils lui répondirent, en disant : Non, mais nous te lierons, nous te lierons et nous te livrerons entre leurs mains, mais nous ne te tuerons pas. Ils le lièrent avec deux cordes neuves, et le firent monter hors du rocher.
15:14	Il arriva jusqu’à Léchi, et les Philistins poussèrent des cris de joie à sa rencontre. Alors l'Esprit de YHWH fondit sur lui, et les cordes qui étaient sur ses bras devinrent comme du lin brûlé par le feu, et les liens tombèrent de ses mains.
15:15	Il trouva une mâchoire d'âne fraîche, tendit la main pour la prendre et tua 1 000 hommes avec elle.
15:16	Shimshôn dit : Avec une mâchoire d'âne, un tas, deux tas. Avec une mâchoire d'âne, j'ai tué 1 000 hommes.
15:17	Et il arriva, comme il achevait de parler, qu’il jeta de sa main la mâchoire. On appela ce lieu Ramath-Léchi<!--Qui signifie « hauteur de la mâchoire ».-->.
15:18	Il eut extrêmement soif et invoqua YHWH en disant : Tu as accordé par la main de ton serviteur cette grande délivrance. Maintenant mourrais-je de soif et tomberais-je entre les mains des incirconcis<!--1 S. 17:26.--> ?
15:19	Elohîm fendit la cavité de cette mâchoire, et il en sortit de l'eau. Il but, l'esprit lui revint, et il reprit vie. C'est pourquoi on a appelé cette source du nom d'En-Hakkoré<!--Qui signifie « source de celui qui invoque ».-->. Elle existe encore aujourd'hui à Léchi.
15:20	Et il jugea Israël, aux jours des Philistins, pendant 20 ans<!--Jg. 16:31.-->.

## Chapitre 16

### Shimshôn (Samson) séduit par Deliylah

16:1	Or Shimshôn s'en alla à Gaza. Il y vit une femme prostituée et il entra chez elle.
16:2	On dit aux Gazites : Shimshôn est venu ici ! Ils l'entourèrent, et se tinrent en embuscade toute la nuit à la porte de la ville. Ils restèrent tranquilles toute la nuit, en disant : Jusqu’à la lumière du matin, nous le tuerons.
16:3	Shimshôn demeura couché jusqu'au milieu de la nuit. Au milieu de la nuit, il se leva, saisit les battants des portes de la ville et les deux poteaux, les retira avec la barre, les mit sur ses épaules et les porta sur le sommet de la montagne qui est en face d'Hébron.
16:4	Il arriva après cela qu’il aima une femme dans la vallée de Sorek, et son nom était Deliylah.
16:5	Les seigneurs des Philistins montèrent vers elle et lui dirent : Séduis-le, jusqu'à ce que tu saches de lui en quoi consiste sa grande force, et comment pourrions-nous le vaincre. Nous le lierons pour l'humilier, et nous te donnerons chacun 1 100 sicles d'argent.
16:6	Deliylah dit à Shimshôn : Dis-moi, s'il te plaît, en quoi consiste ta grande force, et avec quoi il faudrait te lier pour t'abattre.
16:7	Shimshôn lui dit : Si on me liait avec 7 cordes neuves, qui ne soient pas encore sèches, je deviendrais faible et je serais comme un être humain.
16:8	Les seigneurs des Philistins emmenèrent à Deliylah 7 cordes fraîches, qui n'étaient pas encore sèches. Et elle le lia.
16:9	Or il y avait chez elle, dans une chambre, des gens qui se tenaient en embuscade. Elle lui dit : Les Philistins sont sur toi, Shimshôn ! Alors il rompit les cordes comme se romprait un cordon d'étoupe dès qu'il sent le feu. Et le secret de sa force ne fut pas connu.
16:10	Deliylah dit à Shimshôn : Voici, tu t'es moqué de moi, car tu m'as dit des mensonges. S’il te plaît, déclare-moi maintenant avec quoi il faut te lier.
16:11	Il lui dit : Si on me liait, si on me liait avec des cordes neuves, dont on ne se serait jamais servi pour un quelconque ouvrage, je deviendrais faible et je serais comme un être humain.
16:12	Deliylah prit des cordes neuves avec lesquelles elle le lia. Puis elle lui dit : Les Philistins sont sur toi, Shimshôn ! Or des hommes se tenaient en embuscade dans la chambre. Et il les rompit de dessus ses bras comme un fil.
16:13	Deliylah dit à Shimshôn : Tu t'es moqué de moi, jusqu'ici tu m'as dit des mensonges. Déclare-moi avec quoi il faut te lier. Il lui dit : Tu n’as qu’à tisser les sept tresses de ma tête avec le fil à tisser.
16:14	Lorsqu’elle eut planté le pieu, elle lui dit : Les Philistins sont sur toi, Shimshôn ! Il se réveilla de son sommeil, arracha le pieu, la navette<!--métier qui consiste à tisser.--> et le fil à tisser.

### Shimshôn (Samson) révèle le secret de sa force

16:15	Elle lui dit : Comment peux-tu dire : Je t'aime ! puisque ton cœur n'est pas avec moi ? Tu t'es moqué de moi 3 fois et tu ne m'as pas déclaré en quoi consiste ta grande force.
16:16	Il arriva que comme elle le pressait et l'importunait tous les jours par ses paroles, son âme s'impatienta à en mourir.
16:17	Il lui dévoila tout son cœur et lui dit : Le rasoir n'est jamais passé sur ma tête, car je suis nazaréen d'Elohîm dès le ventre de ma mère. Si j'étais rasé, ma force partirait, je deviendrais faible et je serais comme tout humain.
16:18	Deliylah, voyant qu'il lui avait dévoilé tout son cœur, envoya appeler les seigneurs des Philistins, et leur fit dire : Montez cette fois, car il m'a dévoilé tout son cœur. Les seigneurs des Philistins montèrent vers elle et apportèrent l'argent dans leurs mains.
16:19	Elle l'endormit sur ses genoux. Et ayant appelé un homme, elle rasa les 7 tresses de la tête de Shimshôn et commença à le dompter<!--Le mot hébreu signifie aussi « affliger, opprimer, humilier, être affligé, être accablé ».--> et sa force se retira de lui.
16:20	Elle dit : Les Philistins sont sur toi, Shimshôn ! Il se réveilla de son sommeil et dit : Je m'en sortirai comme les autres fois et je me dégagerai. Mais il ne savait pas que YHWH s'était retiré de lui<!--L'immoralité sexuelle de Shimshôn (Samson) et sa désobéissance à YHWH, dues à son manque de caractère, ont ruiné à jamais son service et compromis l'avenir du peuple d'Israël qu'il devait diriger (Jg. 16). Cet homme avait reçu un appel puissant dès le sein de sa mère, mais il ne vivait pas dans la crainte d'Elohîm. Le manque de discernement de Shimshôn lui coûta ainsi toutes les grâces que le Seigneur lui avait accordées : la sainteté symbolisée par ses 7 tresses, la force ou l'onction, la vision et la liberté (Jg. 16:21).-->.

### Shimshôn (Samson) entre les mains des Philistins ; dernière vengeance sur ses ennemis

16:21	Les Philistins le saisirent et lui crevèrent les yeux. Ils le descendirent à Gaza et le lièrent avec une double chaîne de cuivre. Il tournait la meule dans la maison d'arrêt<!--2 S. 3:34.-->.
16:22	Les cheveux de sa tête commencèrent à pousser dès qu’il a été rasé.
16:23	Les seigneurs des Philistins se rassemblèrent pour sacrifier un grand sacrifice à Dagon, leur elohîm, et pour la joie. Ils disaient : Notre elohîm a livré entre nos mains Shimshôn, notre ennemi.
16:24	En le voyant, le peuple loua son elohîm, car ils disaient : Notre elohîm a livré entre nos mains notre ennemi, celui qui ravageait notre terre et qui multipliait nos morts.
16:25	Il arriva, comme ils avaient le cœur heureux, qu'ils dirent : Appelez Shimshôn, qu'il nous fasse rire ! Ils appelèrent Shimshôn de la maison d'arrêt et il joua devant eux. Ils le firent tenir entre les colonnes.
16:26	Shimshôn dit au garçon qui le tenait par la main : Laisse-moi me reposer et fais-moi toucher les colonnes sur lesquelles repose la maison. Je veux m'appuyer contre elles.
16:27	Or la maison était remplie d'hommes et de femmes. Tous les seigneurs des Philistins y étaient, et il y avait même sur le toit près de 3 000 personnes, hommes et femmes, qui regardaient Shimshôn jouer.
16:28	Shimshôn invoqua YHWH en disant : Adonaï YHWH, souviens-toi de moi, s’il te plaît ! Elohîm, fortifie-moi cette fois seulement, s’il te plaît, afin que, d'une seule vengeance, je me venge des Philistins pour mes deux yeux<!--Hé. 11:32.--> !
16:29	Shimshôn embrassa les deux colonnes du milieu sur lesquelles reposait la maison et il s'appuya contre elles. L'une était à sa droite, et l'autre à sa gauche.
16:30	Il dit : Que mon âme meure avec les Philistins ! Il se pencha de toute sa force, et la maison tomba sur les seigneurs et sur tout le peuple qui y était. Et il fit mourir beaucoup plus de gens à sa mort qu'il n'en avait fait mourir pendant sa vie.
16:31	Ses frères et toute la maison de son père descendirent et le transportèrent. Lorsqu'ils furent montés, ils l'enterrèrent entre Tsor`ah et Eshthaol dans le sépulcre de Manoach, son père. Il avait jugé Israël pendant 20 ans<!--Jg. 13:2.-->.

## Chapitre 17

### L'idole de Miykah (Miykayeh)

17:1	Il y avait un homme de la montagne d'Éphraïm dont le nom était Miykayeh.
17:2	Il dit à sa mère : Les 1 100 sicles d'argent qu'on t'a pris et pour lesquels tu as fait des imprécations même à mes oreilles, voici, j'ai cet argent, c'est moi qui l'avais pris. Sa mère lui dit : Béni soit mon fils par YHWH !
17:3	Il rendit à sa mère les 1 100 sicles d'argent et sa mère dit : Je consacre, je consacre de ma main cet argent à YHWH, afin d'en faire pour mon fils une idole et une image en métal fondu. C'est ainsi que je te le rendrai.
17:4	Et il rendit l'argent à sa mère. Elle prit 200 sicles d'argent et les donna au fondeur, qui en fit une idole et une image en métal fondu. On les plaça dans la maison de Miykayeh.
17:5	Cet homme, Miykah<!--Miykayeh.-->, avait une maison d'Elohîm. Il fit un éphod et des théraphim, et il remplit la main<!--C'est-à-dire : consacrer.--> d’un de ses fils, qui devint son prêtre.
17:6	En ces jours-là il n'y avait pas de roi en Israël. Chaque homme faisait ce qui était droit à ses yeux.
17:7	Or il y avait un jeune homme de Bethléhem de Yéhouda, de la famille de Yéhouda. Il était Lévite et il séjournait là.
17:8	Cet homme partit de la ville de Bethléhem de Yéhouda pour séjourner là où il trouverait un lieu. Chemin faisant, il atteignit la montagne d'Éphraïm et la maison de Miykah.
17:9	Miykah lui dit : D'où viens-tu ? Il lui dit : Je suis Lévite, de Bethléhem de Yéhouda, et je voyage pour séjourner où je trouverai.
17:10	Miykah lui dit : Reste avec moi. Tu deviendras pour moi un père et un prêtre et je te donnerai 10 sicles d'argent par an, les vêtements dont tu auras besoin ainsi que de la nourriture. Et le Lévite y alla<!--Jg. 18:19.-->.
17:11	Le Lévite consentit à demeurer avec cet homme et le jeune homme devint pour lui comme l'un de ses fils.
17:12	Miykah remplit la main du Lévite, et le jeune homme devint son prêtre et il demeura dans la maison de Miykah.
17:13	Miykah dit : Maintenant je sais que YHWH me fera du bien, parce que j'ai un Lévite pour prêtre.

## Chapitre 18

### Dan recherche un territoire

18:1	En ce temps-là, il n'y avait pas de roi en Israël. En ce même temps la tribu des Danites cherchait un héritage afin de pouvoir s'établir, car jusqu'à ce jour, il ne lui était pas échu d'héritage au milieu des tribus d'Israël<!--Jg. 17:6.-->.
18:2	Les fils de Dan envoyèrent cinq hommes de leur famille, des fils talentueux, depuis Tsor`ah et Eshthaol pour explorer et examiner la terre. Ils leur dirent : Allez examiner la terre. Ils entrèrent dans la montagne d'Éphraïm jusqu'à la maison de Miykah et ils y passèrent la nuit.
18:3	Comme ils étaient près de la maison de Miykah, ils reconnurent la voix du jeune lévite et lui dirent : Qui t'a amené ici ? Que fais-tu en ce lieu ? Qu'as-tu ici ?
18:4	Il leur dit : Miykah fait pour moi telle et telle chose, il me donne un salaire et je lui sers de prêtre.
18:5	Ils lui dirent : S’il te plaît, consulte Elohîm, afin que nous sachions si notre chemin par lequel nous allons réussira.
18:6	Et le prêtre leur dit : Allez en paix ! Le chemin où vous marchez est sous le regard de YHWH.
18:7	Ces cinq hommes s'en allèrent et entrèrent à Laïs. Ils virent que le peuple qui s'y trouvait habitait en sécurité, à la manière des Sidoniens, tranquille et confiant. Il n'y avait personne sur la terre qui les humiliait en aucune chose, en prenant possession d'eux par l'oppression. Ils étaient éloignés des Sidoniens et ils n'avaient pas affaire avec les hommes.
18:8	Ils vinrent auprès de leurs frères à Tsor`ah et à Eshthaol, et leurs frères leur dirent : Qu’avez-vous ?
18:9	Ils dirent : Allons ! Montons contre eux, car nous avons vu la terre et nous l'avons trouvée très bonne. Quoi ! Vous restez sans rien faire ? Ne soyez pas paresseux à vous mettre en marche pour entrer prendre possession de la terre.
18:10	Quand vous y entrerez, vous entrerez vers un peuple en sécurité. La terre a les mains larges, car Elohîm l'a livrée entre vos mains. C'est un lieu où rien ne manque de tout ce qui est sur la Terre.
18:11	Et de là, de Tsor`ah et d'Eshthaol, partirent 600 hommes de la famille de Dan, ceints d'armes de guerre.
18:12	Ils montèrent et campèrent à Qiryath-Yéarim en Yéhouda. C'est pourquoi on a appelé ce lieu qui est derrière Qiryath-Yéarim jusqu'à ce jour, Machané-Dan.
18:13	Ils passèrent par la montagne d'Éphraïm et ils arrivèrent à la maison de Miykah.

### Les Danites volent l'idole de Miykah et son prêtre ; ils prennent possession de Laïs

18:14	Les cinq hommes qui étaient allés explorer la terre de Laïs répondirent et dirent à leurs frères : Savez-vous qu'il y a dans ces maisons-là un éphod, des théraphim, une idole et une image en métal fondu ? Voyez maintenant ce que vous avez à faire.
18:15	Ils se détournèrent de ce lieu et entrèrent dans la maison où était le jeune lévite, dans la maison de Miykah. Ils s’enquièrent de la paix<!--Le saluèrent.-->.
18:16	Et les 600 hommes d'entre les fils de Dan, qui étaient munis de leurs armes de guerre, se tenaient à l'entrée de la porte.
18:17	Mais les cinq hommes qui étaient allés explorer la terre montèrent et entrèrent dans la maison. Ils prirent l'idole, l'éphod, les théraphim, et l'image en métal fondu pendant que le prêtre était à l'entrée de la porte avec les 600 hommes munis de leurs armes de guerre.
18:18	Étant entrés dans la maison de Miykah, ils prirent l'idole, l'éphod, les théraphim et l'image en métal fondu. Le prêtre leur dit : Que faites-vous ?
18:19	Ils lui dirent : Tais-toi ! Mets ta main sur ta bouche et viens avec nous. Sois pour nous un père et un prêtre. Vaut-il mieux que tu sois le prêtre de la maison d'un seul homme ou celui d'une tribu et d'une famille en Israël<!--Jg. 17:10.--> ?
18:20	Le prêtre eut de la joie dans son cœur. Il prit l'éphod, les théraphim et l'idole, et vint au milieu du peuple.
18:21	Ils se retournèrent et marchèrent en mettant devant eux les petits enfants, le bétail et les bagages.
18:22	Ils étaient loin de la maison de Miykah, quand les hommes qui étaient dans les maisons voisines de la maison de Miykah furent rassemblés à grands cris et poursuivirent les fils de Dan.
18:23	Comme ils criaient vers les fils de Dan, ceux-ci tournèrent leurs faces et dirent à Miykah : Qu'est-ce que tu as à crier ainsi ?
18:24	Il dit : Mon elohîm, celui que j’ai fait, vous l’avez pris avec le prêtre et vous vous en allez. Qu’ai-je encore ? Comment pouvez-vous me dire : Qu'as-tu<!--Ge. 31:30.--> ?
18:25	Les fils de Dan lui dirent : Ne fais pas entendre ta voix après nous, de peur que des hommes à l'âme amère ne se jettent sur vous et que tu n'y perdes ton âme et l'âme de ta maison.
18:26	Les fils de Dan allèrent leur chemin. Miykah, voyant qu'ils étaient plus forts que lui, s'en retourna et revint dans sa maison.
18:27	Quant à eux, ayant pris les choses que Miykah avait faites et le prêtre qu'il avait, ils entrèrent à Laïs, vers un peuple tranquille et en sécurité. Ils les firent passer au fil de l'épée et ils brûlèrent au feu la ville.
18:28	Il n'y eut personne pour la délivrer, car elle était éloignée de Sidon et n'avait pas affaire avec les hommes : elle était dans la vallée de Beth-Rehob. Ils rebâtirent la ville et y demeurèrent.
18:29	Ils appelèrent la ville Dan, selon le nom de Dan, leur père qui était né d'Israël. Mais la ville s'appelait auparavant Laïs<!--Jos. 19:47.-->.
18:30	Les fils de Dan dressèrent pour eux l'idole. Yonathan, fils de Guershom, fils de Menashè, lui et ses fils, devinrent prêtres pour la tribu des Danites, jusqu’au jour où la terre fut emmenée en exil.
18:31	Ils dressèrent pour eux l'idole que Miykah avait fabriquée, elle resta là tout le temps que la maison d'Elohîm fut à Shiyloh.

## Chapitre 19

### Dégradation morale du peuple

19:1	Il arriva en ces jours-là où il n'y avait pas de roi en Israël, qu'un Lévite qui habitait aux côtés de la montagne d'Éphraïm, prit pour concubine une femme de Bethléhem de Yéhouda<!--Jg. 17:6, 21:25.-->.
19:2	Sa concubine se prostitua contre lui et s’en alla de chez lui à la maison de son père à Bethléhem de Yéhouda, où elle resta des jours, 4 mois.
19:3	Son homme se leva et alla après elle, pour parler à son cœur et la ramener. Il avait avec lui son serviteur et deux ânes. Elle le fit entrer dans la maison de son père. Quand le père de la jeune femme le vit, il s'approcha avec joie.
19:4	Son beau-père, le père de la jeune femme, le retint et il demeura 3 jours chez lui. Ils mangèrent, burent et y passèrent la nuit.
19:5	Il arriva, le quatrième jour, qu’ils se levèrent tôt le matin, et comme il se levait pour s’en aller, le père de la jeune femme dit à son gendre : Fortifie ton cœur avec un morceau de pain, et vous partirez ensuite.
19:6	S’étant assis, ils mangèrent et burent eux deux ensemble. Le père de la jeune femme dit à l'homme : S'il te plaît, consens à passer encore ici cette nuit, et que ton cœur se réjouisse.
19:7	L’homme se levait pour s'en aller, mais son beau-père le pressa tellement qu'il s'en retourna et y passa encore la nuit.
19:8	Le cinquième jour, il se leva tôt le matin pour s'en aller. Alors le père de la jeune femme dit : S’il te plaît, fortifie ton cœur, et attardez-vous jusqu’au déclin du jour. Et ils mangèrent eux deux.
19:9	L’homme se levait pour partir avec sa concubine et son serviteur, quand son beau-père, le père de la jeune femme, lui dit : Voici, s’il vous plaît, le jour baisse, il se fait tard, s’il vous plaît, passez ici la nuit. Voici que le jour décline, passe ici la nuit et que ton cœur se réjouisse ! Demain matin vous vous mettrez en route et tu t'en iras à ta tente.
19:10	Mais l'homme ne voulut pas y passer la nuit, il se leva et s'en alla. Il vint jusque vis-à-vis de Yebous, qui est Yeroushalaim, avec les deux ânes bâtés et sa concubine.
19:11	Et comme ils étaient près de Yebous, et que le jour avait beaucoup baissé, le serviteur dit à son seigneur : Allons, s'il te plaît, détournons-nous vers cette ville des Yebousiens afin que nous y passions la nuit.
19:12	Son seigneur lui dit : Nous ne nous détournerons pas vers une ville d'étrangers, où il n'y a pas de fils d'Israël mais nous passerons par Guibea.
19:13	Il dit à son serviteur : Allons, approchons-nous de l'un de ces lieux, Guibea ou Ramah et passons-y la nuit.
19:14	Ils continuèrent à marcher et le soleil se coucha quand ils furent près de Guibea, qui appartient à Benyamin.
19:15	Ils se détournèrent vers Guibea, et y entrèrent pour passer la nuit. Le Lévite entra et il s'assit sur la place de la ville. Il n'y eut aucun homme qui les reçut dans sa maison afin qu'ils y passent la nuit.
19:16	Et voici qu'un vieil homme rentrait le soir de son travail des champs. Cet homme était de la montagne d'Éphraïm, il séjournait à Guibea et les hommes du lieu étaient Benyamites.
19:17	Et levant ses yeux, il vit le voyageur sur la place de la ville. Le vieil homme lui dit : Où vas-tu et d'où viens-tu ?
19:18	Il lui dit : Nous passons de Bethléhem de Yéhouda jusqu'à l'extrémité de la montagne d'Éphraïm, d'où je suis. J'étais allé jusqu'à Bethléhem de Yéhouda, mais maintenant je m'en vais à la maison de YHWH. Mais il n'y a aucun homme qui me reçoive dans sa maison.
19:19	Nous avons même de la paille, même du fourrage pour nos ânes, et même du pain et du vin pour moi, pour ta servante et pour le garçon qui est avec tes serviteurs. Nous n'avons besoin d'aucune chose.
19:20	Le vieil homme dit : Shalôm à toi ! Que tous tes besoins soient à ma seule charge ! Seulement ne passe pas la nuit sur la place.
19:21	Il les fit entrer dans sa maison, et il donna du fourrage aux ânes. Les voyageurs se lavèrent les pieds, puis ils mangèrent et burent<!--Ge. 43:24.-->.
19:22	Pendant qu'ils réjouissaient leur cœur, voici, les hommes de la ville, des hommes, fils de Bélial<!--Voir commentaire en De. 13:14.-->, environnèrent la maison, frappèrent à la porte, et dirent au vieil homme, maître de la maison : Fais sortir l'homme qui est entré dans ta maison, afin que nous le connaissions<!--Ge. 19:4-5 ; Jg. 20:13 ; Os. 9:9, 10:9.-->.
19:23	Mais cet homme, le maître de la maison, sortit vers eux, et leur dit : Non, mes frères, ne lui faites pas de mal, je vous prie. Puisque cet homme est entré dans ma maison ne faites pas une telle infamie.
19:24	Voici, j'ai une fille vierge, et il a une concubine. Laissez-moi les faire sortir, s’il vous plaît. Vous les humilierez et vous les traiterez comme il semblera bon à vos yeux. Mais ne faites pas cette chose infâme à l'égard de cet homme !

### Viol et meurtre de la concubine du Lévite

19:25	Ces hommes ne voulurent pas l'écouter. Et l'homme saisit sa concubine et la leur amena dehors. Ils la connurent et abusèrent d'elle toute la nuit jusqu'au matin. Puis ils la renvoyèrent au lever de l'aurore.
19:26	Comme le matin approchait, cette femme vint et tomba à la porte de la maison de l'homme où était son seigneur et elle y demeura jusqu'au jour.
19:27	Et le matin, son seigneur se leva et ayant ouvert la porte de la maison, il sortit pour poursuivre son chemin. Mais voici que la femme, sa concubine, était tombée à la porte de la maison, les mains sur le seuil.
19:28	Il lui dit : Lève-toi et allons-nous-en. Mais elle ne répondit pas. Alors l'homme la prit sur son âne, et se leva et s'en alla dans sa demeure.
19:29	En entrant en sa maison, il prit un couteau et saisissant sa concubine, il la coupa avec ses os en 12 morceaux, qu'il envoya dans tout le territoire d'Israël.
19:30	Il arriva que tous ceux qui virent cela dirent : Jamais chose semblable n’est arrivée ni ne s’est vue, depuis le jour où les fils d'Israël sont montés hors de la terre d'Égypte, jusqu'à ce jour. Prenez la chose à cœur, consultez-vous et parlez !

## Chapitre 20

### Réunion des fils d'Israël à Mitspah

20:1	Tous les fils d'Israël sortirent. L'assemblée se réunit comme un seul homme, depuis Dan jusqu'à Beer-Shéba et jusqu'à la terre de Galaad, devant YHWH, à Mitspah.
20:2	Les chefs de tout le peuple, toutes les tribus d'Israël, se présentèrent à l'assemblée du peuple d'Elohîm : 400 000 hommes de pied, tirant l'épée.
20:3	Les fils de Benyamin apprirent que les fils d'Israël étaient montés à Mitspah. Les fils d'Israël dirent : Parlez, comment ce mal est arrivé ?
20:4	L’homme, le Lévite, l’homme dont la femme avait été assassinée, répondit et dit : J'étais venu à Guibea de Benyamin, avec ma concubine, pour y passer la nuit.
20:5	Les seigneurs de Guibea se sont élevés contre moi et ont encerclé de nuit la maison où j'étais. Ils avaient résolu de m'assassiner et ils ont tellement humilié ma concubine qu'elle en est morte.
20:6	C'est pourquoi j'ai saisi ma concubine, je l'ai coupée en morceaux et je les ai envoyés dans tout le territoire de l'héritage d'Israël, car ils ont commis une méchanceté et une infamie en Israël.
20:7	Vous voici tous, fils d'Israël. Tenez conseil ici et donnez votre parole !
20:8	Tout le peuple se leva comme un seul homme, et ils dirent : Aucun homme n'ira dans sa tente, et aucun homme ne se retirera dans sa maison.
20:9	Et maintenant voici ce que nous ferons à Guibea : Nous marcherons contre elle d'après le sort.
20:10	Nous prendrons dans toutes les tribus d'Israël 10 hommes sur 100, 100 sur 1 000, et 1 000 sur 10 000. Ils prendront des provisions pour le peuple, afin qu'en entrant à Guibea de Benyamin, on la traite selon toute l'infamie qu'elle a commise en Israël.
20:11	Ainsi tous les hommes d'Israël se rassemblèrent contre la ville, unis comme un seul homme.

### Benyamin refuse de livrer Guibea : guerre entre Benyamin et le reste d'Israël

20:12	Les tribus d'Israël envoyèrent des hommes vers la maison de Benyamin, pour dire : Quel est ce mal qui est arrivé au milieu de vous ?
20:13	Maintenant livrez-nous ces hommes, fils de Bélial<!--Voir commentaire en De. 13:14.-->, qui sont à Guibea, afin que nous les fassions mourir et que nous ôtions le mal du milieu d'Israël. Mais les fils de Benyamin ne voulurent pas écouter la voix de leurs frères, les fils d'Israël.
20:14	Les fils de Benyamin se rassemblèrent à Guibea pour sortir en guerre contre les fils d'Israël.
20:15	En ce jour-là furent dénombrés les fils de Benyamin qui vinrent de leurs villes : 26 000 hommes, tirant l'épée, sans les habitants de Guibea qui furent dénombrés : 700 hommes sélectionnés.
20:16	De tout ce peuple, il y avait 700 hommes sélectionnés qui étaient empêchés de la main droite. Tous ceux-là lançaient des pierres avec une fronde contre un cheveu et ne le manquaient pas.
20:17	Les hommes d’Israël furent dénombrés, excepté Benyamin : 400 000 hommes tirant l'épée, tous hommes de guerre.
20:18	Les fils d'Israël se levèrent, montèrent vers Elohîm à Béth-El pour le consulter, en disant : Qui d'entre nous montera le premier pour faire la guerre aux fils de Benyamin ? YHWH dit : Yéhouda en premier.
20:19	Les fils d'Israël se levèrent tôt le matin et campèrent près de Guibea.
20:20	Les hommes d'Israël sortirent pour combattre Benyamin. Les hommes d'Israël se rangèrent en bataille près de Guibea.
20:21	Les fils de Benyamin sortirent de Guibea et ils tuèrent ce jour-là 22 000 hommes d'Israël.
20:22	Le peuple, les hommes d'Israël, se fortifièrent et se rangèrent de nouveau en bataille au lieu où ils s'étaient rangés le premier jour.
20:23	Les fils d'Israël montèrent et ils pleurèrent devant YHWH jusqu'au soir. Ils consultèrent YHWH, en disant : M'approcherai-je encore pour combattre contre les fils de Benyamin, mon frère ? YHWH dit : Montez contre lui.
20:24	Le second jour, les fils d'Israël s'approchèrent des fils de Benyamin.
20:25	Benyamin sortit de Guibea à leur rencontre, ce second jour, et ils tuèrent encore 18 000 hommes des fils d'Israël, tous tirant l'épée.
20:26	Tous les fils d'Israël et tout le peuple montèrent et vinrent vers Elohîm à Béth-El. Ils pleurèrent et restèrent là devant YHWH. Ils jeûnèrent ce jour-là jusqu'au soir et ils firent monter des holocaustes et des offrandes de paix<!--Voir commentaire en Lé. 3:1.--> devant YHWH.
20:27	Ensuite les fils d'Israël consultèrent YHWH, c'était là que se trouvait l'arche de l'alliance d'Elohîm,
20:28	et Phinées, fils d'Èl’azar, fils d'Aaron, se tenait debout en face d'elle en ces jours-là - et ils dirent : Continuerai-je encore de sortir pour faire la guerre aux fils de Benyamin, mon frère, ou cesserai-je ? YHWH dit : Montez, car demain je les livrerai entre vos mains.
20:29	Israël mit une embuscade autour de Guibea.
20:30	Le troisième jour, les fils d'Israël montèrent contre les fils de Benyamin, et ils se rangèrent en bataille contre Guibea, comme les autres fois.
20:31	Les fils de Benyamin sortirent à la rencontre du peuple, et ils furent attirés hors de la ville. Ils commencèrent à frapper à mort quelques-uns du peuple comme les autres fois, environ 30 hommes d'Israël, sur les routes dont l'une monte à Béth-El et l'autre à Guibea, par les champs.
20:32	Les fils de Benyamin disaient : Ils tombent battus devant nous, comme la première fois ! Mais les fils d'Israël disaient : Fuyons et attirons-les hors de la ville dans les chemins.
20:33	Tous les hommes d'Israël se levèrent de leur position et se rangèrent à Baal-Thamar. Et les hommes d'Israël placés en embuscade s'élancèrent de la position où ils étaient, de Maaré-Guibea.
20:34	10 000 hommes sélectionnés sur tout Israël vinrent contre Guibea. La bataille fut rude et ils ne surent pas que le mal les atteindrait.

### Défaite écrasante de Benyamin

20:35	YHWH battit Benyamin devant Israël et les fils d'Israël tuèrent ce jour-là 25 100 hommes de Benyamin, tous tirant l'épée.
20:36	Les fils de Benyamin regardaient comme battus les hommes d'Israël, qui cédaient du terrain à Benyamin et se reposaient sur l'embuscade qu'ils avaient mise près de Guibea.
20:37	Ceux qui étaient en embuscade se jetèrent promptement sur Guibea, ils se portèrent en avant et frappèrent toute la ville à bouche d’épée.
20:38	Et le signal convenu entre les hommes d'Israël et l'embuscade était qu'ils fassent monter beaucoup de fumée de la ville.
20:39	Les hommes d'Israël avaient tourné le dos dans la bataille, et Benyamin avait commencé à frapper et à blesser à mort environ 30 hommes de ceux d'Israël, et ils disaient : Certainement ils tombent devant nous comme à la première bataille !
20:40	Mais quand l'épaisse colonne de fumée commençait à monter de la ville, Benyamin se tourna en arrière, et voici, toute la ville montait vers les cieux.
20:41	Les hommes d'Israël se retournèrent et les hommes de Benyamin furent terrifiés, car ils voyaient que le malheur les avait atteints.
20:42	Ils se toumèrent devant les hommes d'Israël par le chemin du désert, mais la bataille s’attacha à eux, et ceux des villes les détruisaient, au milieu d’eux.
20:43	Ils environnèrent Benyamin, le poursuivirent, l'écrasèrent dès qu'il voulut se reposer jusqu'en face de Guibea, du côté du soleil levant.
20:44	Il tomba 18 000 hommes de Benyamin, tous des hommes talentueux.
20:45	Ils se tournèrent et s’enfuirent au désert vers le rocher de Rimmon. On frappa 5 000 hommes sur les grandes routes. On les poursuivit jusqu'à Gid`om et on tua 2 000 hommes.
20:46	Tous ceux de Benyamin qui tombèrent ce jour-là furent 25 000 hommes, tirant l'épée, tous étaient des hommes talentueux.
20:47	Il y eut 600 hommes de ceux qui avaient tourné le dos, qui s'échappèrent vers le désert au rocher de Rimmon, et qui demeurèrent au rocher de Rimmon pendant 4 mois.
20:48	Les hommes d'Israël retournèrent vers les fils de Benyamin et ils les frappèrent à bouche d’épée, depuis les hommes des villes jusqu'aux bêtes, et tout ce qui s'y trouva. Ils brûlèrent toutes les villes qu'ils trouvaient.

## Chapitre 21

### La tribu de Benyamin menacée d'extinction ; regret d'Israël

21:1	Les hommes d'Israël avaient juré à Mitspah, en disant : Aucun homme ne donnera sa fille pour femme à un Benyamin.
21:2	Le peuple vint vers Elohîm à Béth-El, jusqu'au soir. Ils élevèrent leurs voix et pleurèrent grandement,
21:3	et ils dirent : YHWH, Elohîm d'Israël, pourquoi est-il arrivé en Israël qu'une tribu d'Israël ait été aujourd'hui punie ?
21:4	Le lendemain, il arriva que le peuple se leva de bon matin et bâtit là un autel. Ils firent monter des holocaustes et des sacrifices d'offrande de paix.
21:5	Les fils d'Israël dirent : Qui d'entre toutes les tribus d'Israël n'est pas monté à l'assemblée vers YHWH ? Car le grand serment était contre celui qui ne monterait pas vers YHWH à Mitspah, en disant : Il mourra, il mourra.
21:6	Les fils d'Israël se repentaient au sujet de Benyamin leur frère et disaient : Aujourd'hui une tribu a été retranchée d'Israël.
21:7	Que ferons-nous pour ceux qui restent, quant aux femmes ? Nous avons juré par YHWH de ne pas leur donner nos filles pour femmes !
21:8	Ils dirent : Y en a-t-il un parmi toutes les tribus d’Israël, qui ne soit pas monté vers YHWH à Mitspah ? Et voici, aucun homme de Yabesh en Galaad n'était venu au camp, à l'assemblée.
21:9	Le peuple fut dénombré : et voici, il n’y avait là aucun homme des habitants de Yabesh en Galaad.
21:10	L’assemblée envoya là-bas 12 000 hommes, des fils talentueux et leur donna cet ordre en disant : Allez, et frappez à bouche d’épée les habitants de Yabesh en Galaad, tant les femmes que les enfants.
21:11	Voici les choses que vous ferez : Vous détruirez par interdit tout mâle et toute femme qui a connu la couche d'un homme.
21:12	Ils trouvèrent parmi les habitants de Yabesh en Galaad 400 filles vierges, qui n'avaient pas connu d'homme, en s'étendant sur un mâle pour une relation sexuelle, et ils les amenèrent au camp de Shiyloh, qui est sur la terre de Kena'ân.
21:13	Toute l'assemblée envoya parler aux fils de Benyamin qui étaient au rocher de Rimmon, pour leur proclamer la paix.
21:14	Benyamin revint en ce temps-là. On leur donna les femmes qu’on avait laissé vivre d'entre les femmes de Yabesh en Galaad. Mais ils n’en trouvèrent pas assez pour eux.
21:15	Le peuple se repentit de ce qui avait été fait à Benyamin, car YHWH avait fait une brèche dans les tribus d'Israël.
21:16	Les anciens de l'assemblée dirent : Que ferons-nous pour ceux qui restent, quant aux femmes, car les femmes de Benyamin ont été détruites ?
21:17	Ils dirent : Que les rescapés de Benyamin possèdent leur héritage, afin qu'une tribu d'Israël ne soit pas effacée.
21:18	Cependant, nous ne pouvons pas leur donner des femmes d'entre nos filles, car les fils d'Israël ont juré, en disant : Maudit soit celui qui donnera une femme à un Benyamin<!--Jg. 21:1.--> !
21:19	Et ils dirent : Voici, d'année en année il y a une fête de YHWH à Shiyloh, qui est au nord de Béth-El, à l'est qui monte à Béth-El, à Shekem, et au midi de Lebonah.
21:20	Ils donnèrent cet ordre aux fils de Benyamin en disant : Allez, et placez-vous en embuscade dans les vignes.
21:21	Vous verrez, et voici, lorsque les filles de Shiyloh sortiront pour danser en rondes, alors vous sortirez des vignes, vous enlèverez chacun une des filles de Shiyloh pour en faire votre femme, et vous vous en irez en terre de Benyamin.
21:22	S’il arrive que leurs pères ou leurs frères viennent nous quereller, nous leur dirons : Accordez-nous cette faveur, puisque nous n'avons pas pris de femmes pour chaque homme dans cette guerre. Ce n'est pas vous qui les leur avez données. Sinon vous en seriez coupables en ce temps.
21:23	Les fils de Benyamin agirent ainsi. Ils prirent des femmes selon leur nombre parmi les danseuses qu'ils saisirent, puis ils s'en allèrent et retournèrent dans leur héritage. Ils rebâtirent les villes et y habitèrent.
21:24	Les fils d’Israël s’en allèrent de là, en ce temps-là, chaque homme dans sa tribu et dans sa famille. Ils sortirent de là, chaque homme dans son héritage.
21:25	En ce temps-là, il n'y avait pas de roi en Israël. Chaque homme faisait ce qui était droit à ses yeux.
