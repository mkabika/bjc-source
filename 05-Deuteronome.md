# Devarim (Deutéronome) (De.)

Signification : Paroles

Auteur : Probablement Moshé (Moïse)

Thème : Rappel de la torah

Date de rédaction : 1450 – 1410 av. J.-C.

Ce livre est un rappel de la torah de YHWH. Après 40 années d'errance dans le désert, Moshé (Moïse) s'adresse à la nouvelle génération par des discours et des exhortations, depuis les plaines de Moab. Au travers de son serviteur, Elohîm rappelle ainsi la torah donnée sur le Mont Sinaï, les expériences vécues par la génération passée et par conséquent, l'importance de la soumission à Elohîm. De leur obéissance dépendraient les bénédictions ou les malédictions contenues dans ce livre.

## Chapitre 1

### Rappel de l'infidélité d'Israël<!--No. 14.-->

1:1	Voici les paroles que Moshé déclara à tout Israël de l'autre côté du Yarden, dans le désert, dans la région aride, vis-à-vis de Souph, entre Paran, Tophel, Laban, Hatséroth, et Di-Zahab.
1:2	Il y a 11 journées depuis Horeb, par le chemin de la montagne de Séir, jusqu'à Qadesh-Barnéa.
1:3	Or il arriva dans la quarantième année, au onzième mois, le premier du mois, que Moshé parla aux fils d'Israël de tout ce que YHWH lui avait ordonné pour eux,
1:4	après qu'il eut battu Sihon, roi des Amoréens, qui habitait à Hesbon, et Og, roi de Bashân, qui demeurait à Ashtaroth, à Édréi<!--No. 21:23-24.-->.
1:5	De l'autre côté du Yarden, en terre de Moab, Moshé commença à expliquer cette torah, en disant :
1:6	YHWH, notre Elohîm, nous a parlé à Horeb, en disant : Vous avez assez demeuré dans cette montagne.
1:7	Tournez-vous, partez, et allez à la montagne des Amoréens et chez tous leurs voisins, dans la région aride, dans la montagne, dans la vallée, vers le sud, sur le rivage de la mer, en terre des Kena'ânéens et au Liban, jusqu'au grand fleuve, le fleuve d'Euphrate.
1:8	Regardez, j'ai mis devant vous la terre : entrez et prenez possession de la terre que YHWH a juré de donner à vos pères, Abraham, Yitzhak et Yaacov, et à leur postérité après eux.
1:9	Et je vous ai parlé en ce temps-là, et je vous ai dit : Je ne peux pas, à moi seul, vous porter.
1:10	YHWH, votre Elohîm, vous a multipliés, et vous voici aujourd'hui comme les étoiles des cieux par le nombre.
1:11	Que YHWH, l'Elohîm de vos pères, vous fasse croître 1 000 fois au-delà de ce que vous êtes et vous bénisse, comme il vous l'a dit !
1:12	Comment porterais-je moi seul vos chagrins, vos charges, et vos procès ?
1:13	Donnez-vous des hommes sages, intelligents et connus, selon vos tribus, et je les mettrai à votre tête.
1:14	Vous m'avez répondu et dit : La parole que tu as dit d'exécuter est bonne.
1:15	J'ai pris les têtes de vos tribus, des hommes sages et connus, et je les ai mis à votre tête comme chefs de milliers, chefs de centaines, chefs de cinquantaines, chefs de dizaines et commissaires selon vos tribus.
1:16	J'ai donné à vos juges, en ce temps, un ordre en disant : Écoutez vos frères ! Jugez-les avec justice, entre l’homme et son frère ou, son résident étranger<!--Lé. 19:15 ; De. 16:19 ; Pr. 24:23.-->.
1:17	Vous ne reconnaîtrez pas de faces dans le jugement, mais vous écouterez le petit comme le grand. N'ayez peur en face d'aucun homme, car c'est à Elohîm qu'appartient le jugement. L’affaire qui sera trop difficile pour vous, vous me la présenterez et je l’entendrai.
1:18	En ce temps-là, je vous ai ordonné toutes les paroles que vous exécuterez.
1:19	Nous sommes partis d'Horeb, et nous avons marché dans tout ce grand et affreux désert que vous avez vu ; par le chemin de la montagne des Amoréens, ainsi que YHWH notre Elohîm, nous l'avait ordonné, et nous sommes arrivés à Qadesh-Barnéa.
1:20	Je vous ai dit : Vous êtes arrivés jusqu'à la montagne des Amoréens, que YHWH, notre Elohîm, nous donne.
1:21	Regarde, YHWH, ton Elohîm, met la terre devant toi. Monte et prends-en possession, comme YHWH, l'Elohîm de tes pères, te l'a dit. N'aie pas peur, ne te laisse pas effrayer !
1:22	Et vous vous êtes tous approchés de moi en disant : Envoyons devant nous des hommes pour explorer la terre, et qui nous rapportent des nouvelles du chemin par lequel nous monterons, et des villes où nous arriverons<!--No. 13:2.-->.
1:23	Ce discours a paru bon à mes yeux et j'ai pris 12 hommes parmi vous, un homme par tribu.
1:24	Ils se sont tournés pour monter vers la montagne. Ils sont arrivés jusqu'au torrent d'Eshcol et ils l'ont exploré.
1:25	Ils ont pris dans leurs mains des fruits de la terre et nous les ont présentés. Ils nous ont donné des nouvelles en disant : La terre que YHWH, notre Elohîm, nous donne est bonne.
1:26	Mais vous avez refusé d'y monter et vous vous êtes rebellés contre la bouche de YHWH, votre Elohîm.
1:27	Et vous avez murmuré dans vos tentes, en disant : C'est parce que YHWH nous hait qu'il nous a fait sortir de la terre d'Égypte, afin de nous livrer entre les mains des Amoréens pour nous exterminer.
1:28	Où monterions-nous ? Nos frères nous ont fait fondre le cœur, en disant : Le peuple est plus grand que nous et de plus haute taille, les villes sont grandes et closes jusqu'aux cieux. Et même nous y avons vu les fils des Anakim.
1:29	Mais je vous ai dit : Ne tremblez pas et n'ayez pas peur d'eux.
1:30	YHWH, votre Elohîm, qui marche devant vous, lui-même combattra pour vous, selon tout ce que vous avez vu qu'il a fait pour vous en Égypte ;
1:31	et dans le désert, où tu as vu de quelle manière YHWH, ton Elohîm, t'a porté comme un homme porterait son fils, sur tout le chemin où vous avez marché, jusqu'à ce que vous soyez arrivés dans ce lieu-ci.
1:32	Et malgré cette parole, vous n'avez pas cru en YHWH, votre Elohîm,
1:33	qui marchait devant vous sur le chemin afin de vous chercher un lieu pour camper, marchant de nuit dans la colonne de feu pour vous éclairer dans le chemin par lequel vous deviez marcher et de jour dans la nuée.
1:34	YHWH a entendu la voix de vos paroles. Il s'est fâché et a juré en disant :
1:35	Aucun homme, parmi les hommes de cette méchante génération, ne verra cette bonne terre que j'ai juré de donner à vos pères,
1:36	à l'exception de Kaleb, fils de Yephounné. Lui, il la verra et je lui donnerai, à lui et à ses fils, la terre sur laquelle il a marché, parce qu'il a persévéré à suivre YHWH<!--No. 14:22-24.-->.
1:37	Même YHWH s'est mis en colère contre moi à cause de vous, disant : Et toi aussi tu n'y entreras pas.
1:38	Yéhoshoua, fils de Noun, qui se tient en face de toi, lui entrera. Fortifie-le, car c'est lui qui la fera posséder par Israël<!--De. 34:4.-->.
1:39	Et vos petits-enfants, dont vous avez dit : « Ils deviendront un butin ! » et vos fils, qui aujourd'hui ne connaissent pas ce qui est bon ou mauvais, ce sont eux qui y entreront ; c'est à eux que je la donnerai, ce sont eux qui en prendront possession.
1:40	Mais vous, retournez-vous-en en arrière, et allez dans le désert par le chemin de la Mer Rouge.
1:41	Et vous avez répondu en me disant : Nous avons péché contre YHWH, nous monterons et nous combattrons, comme YHWH, notre Elohîm, nous l'a ordonné. Vous avez ceint chacun vos armes et vous avez estimé qu'il vous serait aisé de monter dans la montagne.
1:42	YHWH m'a dit : Dis-leur : Ne montez pas et ne combattez pas, car je ne suis pas au milieu de vous, de peur que vous ne soyez battus devant vos ennemis.
1:43	Je vous ai parlé, mais vous n'avez pas écouté. Vous vous êtes rebellés contre la bouche de YHWH, et vous avez eu l'arrogance de monter sur la montagne.
1:44	Mais les Amoréens, qui habitent cette montagne sont sortis contre vous et vous ont poursuivi comme le font les abeilles. Ils vous ont taillés en pièces depuis Séir jusqu'à Hormah.
1:45	Et à votre retour, vous avez pleuré devant YHWH, mais YHWH n'a pas écouté votre voix et il ne vous a pas prêté l'oreille.
1:46	Vous demeurèrent à Qadesh beaucoup de jours, selon les jours que vous y avez demeuré.

## Chapitre 2

### Périple du peuple dans le désert

2:1	Nous nous sommes tournés et sommes partis pour le désert, par le chemin de la Mer Rouge, comme YHWH me l'avait dit, et pendant de longs jours nous avons tourné autour de la montagne de Séir.
2:2	Et YHWH m'a parlé, en disant :
2:3	Vous avez assez tourné autour de cette montagne. Tournez-vous vers le nord !
2:4	Ordonne au peuple, en disant : Vous allez passer la frontière de vos frères, les fils d'Ésav, qui demeurent en Séir. Ils auront peur de vous, mais soyez bien sur vos gardes.
2:5	Ne les attaquez pas, car je ne vous donnerai rien de leur terre, pas même de quoi poser la plante du pied : J'ai donné à Ésav la montagne de Séir en héritage.
2:6	Vous achèterez d'eux la nourriture à prix d'argent et vous en mangerez, et vous achèterez d'eux l'eau à prix d'argent et vous en boirez.
2:7	Car YHWH, ton Elohîm, t'a béni dans tout le travail de tes mains, il a connu ta marche dans ce grand désert. YHWH, ton Elohîm, a été avec toi pendant ces 40 années, et tu n'as manqué de rien.
2:8	Nous sommes passés loin de nos frères, les fils d'Ésav, qui habitent en Séir, par la route de la région aride, d'Élath et d'Etsyôn-Guéber, puis nous nous sommes tournés vers le nord et nous sommes passés par la route du désert de Moab.
2:9	YHWH m'a dit : N'assiège pas Moab, et ne t'engage pas dans un combat avec lui, car je ne te donnerai rien en héritage dans sa terre : J'ai donné Ar en héritage aux fils de Lot<!--Ge. 19:36-38.-->.
2:10	Les Émim y habitaient auparavant. C'était un peuple grand, nombreux et de haute taille comme les Anakim.
2:11	Ils étaient considérés comme des géants, de même que les Anakim, mais les Moabites les appelaient Émim.
2:12	Séir était habité autrefois par les Horiens, mais les fils d'Ésav les en dépossédèrent, les détruisirent devant eux, et y habitèrent à leur place, comme l'a fait Israël en terre de son héritage que YHWH lui a donnée.
2:13	Maintenant, levez-vous, et passez le torrent de Zéred. Et nous avons passé le torrent de Zéred.
2:14	Les jours où nous avons marché de Qadesh-Barnéa, jusqu'à ce que nous ayons passé le torrent de Zéred, furent de 38 ans, jusqu'à ce que toute la génération des hommes de guerre ait été consumée du milieu du camp, comme YHWH le leur avait juré.
2:15	La main de YHWH fut aussi sur eux pour les détruire du milieu du camp, jusqu'à ce qu'ils aient été consumés.
2:16	Or il est arrivé qu'après que tous les hommes de guerre eurent été consumés par la mort du milieu du peuple,
2:17	YHWH m'a parlé, en disant :
2:18	Tu vas passer aujourd'hui la frontière de Moab, à Ar.
2:19	Tu t'approcheras en face des fils d'Ammon, mais ne les assiège pas, et ne t'engage pas dans un combat avec eux, car je ne te donnerai rien en possession en terre des fils d'Ammon : je l'ai donnée en héritage aux fils de Lot.
2:20	On la considérait, elle aussi, comme une terre de géants. Les géants y habitaient auparavant et les Ammonites les appelaient Zamzoummim :
2:21	c'était un peuple grand, nombreux, et de haute taille, comme les Anakim, YHWH les détruisit devant eux, et ils les dépossédèrent et habitèrent à leur place.
2:22	C'est aussi ce qu'il avait fait pour les fils d'Ésav qui habitent en Séir, quand il avait détruit les Horiens devant eux ; ils les dépossédèrent et habitèrent à leur place jusqu'à ce jour.
2:23	Or quant aux Avviens, qui habitaient dans les villages jusqu'à Gaza, ce sont les Kaphtorim, sortis de Kaphtor, qui les détruisirent et s'installèrent à leur place.

### YHWH livre Sihon, roi de Hesbon, entre les mains d'Israël

2:24	Levez-vous, partez et passez le torrent de l'Arnon. Regarde, j'ai livré entre tes mains Sihon, roi de Hesbon, l'Amoréen, et sa terre. Commence à en prendre possession et fais-lui la guerre !
2:25	En ce jour, je commence à mettre la terreur et la crainte de toi sur les peuples qui sont sous tous les cieux. Quand ils entendront ta rumeur, ils trembleront et seront dans l'angoisse en face de toi.
2:26	J'ai envoyé, du désert de Qedémoth, des messagers à Sihon, roi de Hesbon, avec des paroles de paix, en disant<!--No. 21:21.--> :
2:27	Je passerai par ta terre ! J’irai de chemin en chemin ! Je ne me détournerai ni à droite ni à gauche !
2:28	Tu me vendras de la nourriture à prix d'argent, afin que je mange, et tu me donneras de l'eau à prix d'argent, afin que je boive. Je veux seulement passer à pied.
2:29	C'est ce qu'ont fait les fils d'Ésav qui habitent en Séir, et les Moabites qui habitent à Ar, jusqu'à ce que je passe le Yarden, vers la terre que YHWH, notre Elohîm, nous donne.
2:30	Mais Sihon, roi de Hesbon, n'a pas voulu nous laisser passer chez lui. Car YHWH, ton Elohîm, avait endurci son esprit et affermi son cœur afin de le livrer entre tes mains, comme en ce jour.
2:31	YHWH m'a dit : Regarde, j'ai commencé à livrer en face de toi, Sihon et sa terre. Commence à prendre possession, à prendre possession de sa terre.
2:32	Sihon est sorti nous rencontrer avec tout son peuple pour nous combattre à Yahats.
2:33	Mais YHWH, notre Elohîm, nous l'a livré en face, et nous l'avons battu, lui, ses fils, et tout son peuple.
2:34	En ce temps-là, nous avons pris toutes ses villes et nous avons dévoué par interdit chaque ville, les hommes, les femmes, et les petits enfants, sans laisser de survivants.
2:35	Seulement, nous avons pillé les bêtes pour nous, et le butin des villes que nous avons prises.
2:36	Depuis Aroër, qui est sur le bord du torrent de l'Arnon, et la ville qui est dans la vallée, jusqu'à Galaad, il n'y a pas eu de ville inaccessiblement haute pour nous : YHWH, notre Elohîm, nous a tout livré.
2:37	Seulement, tu ne t'es pas approché de la terre des fils d'Ammon, de toute la portion du torrent de Yabboq, des villes de la montagne, ni de tout ce qu’avait ordonné YHWH, notre Elohîm.

## Chapitre 3

### YHWH livre Og, roi de Bashân, entre les mains d'Israël

3:1	Nous nous sommes tournés pour monter par le chemin de Bashân. Et Og, roi de Bashân, est sorti à notre rencontre, lui et tout son peuple pour nous combattre à Édréi.
3:2	Et YHWH m'a dit : N'aie pas peur de lui, car je le livre entre tes mains ainsi que tout son peuple et sa terre. Tu le traiteras comme tu as traité Sihon, roi des Amoréens, qui habitait à Hesbon.
3:3	YHWH, notre Elohîm, a livré aussi entre nos mains Og, roi de Bashân, avec tout son peuple. Nous l'avons battu et nous ne lui avons laissé aucun survivant.
3:4	En ce même temps, nous avons pris aussi toutes ses villes, il n'y a pas une seule ville que nous n'ayons prise : 60 villes, toute la région d'Argob, le royaume d'Og en Bashân.
3:5	Toutes ces villes-là étaient fortifiées, avec de hautes murailles, des portes et des barres. Il y avait aussi des villes sans murailles en très grand nombre.
3:6	Et nous les avons dévouées par interdit, comme nous l'avions fait pour Sihon, roi de Hesbon. Nous avons dévoué par interdit toutes les villes, les hommes, les femmes et les petits enfants.
3:7	Mais nous avons pillé pour nous toutes les bêtes et le butin des villes.
3:8	Nous avons pris en ce temps-là, la terre de la main des deux rois des Amoréens, qui étaient de l'autre côté du Yarden, depuis le torrent de l'Arnon jusqu'à la montagne de l'Hermon ;
3:9	les Sidoniens appellent l’Hermon Shiryown, et les Amoréens l'appellent Sheniyr.
3:10	Toutes les villes de la plaine, tout Galaad, et tout Bashân jusqu'à Salcah et Édréi, les villes du royaume d'Og en Bashân.
3:11	Og, roi de Bashân, avait survécu seul du reste des géants. Voici, son lit, un lit de fer, n'est-il pas dans Rabbath des fils d'Ammon ? Sa longueur est de 9 coudées, et sa largeur de 4 coudées, en coudées d'homme.

### Premières terres attribuées à Reouben (Ruben), Gad et à la demi-tribu de Menashè (Manassé)

3:12	Cette terre, nous en avons pris possession, en ce temps, depuis Aroër, sur le torrent de l'Arnon, avec la moitié de la montagne de Galaad et ses villes<!--Jos. 13:23-32.-->. Je l'ai donnée aux Reoubénites et aux Gadites.
3:13	J'ai donné à la demi-tribu de Menashè le reste de Galaad et tout le royaume d'Og, en Bashân : toute la région d'Argob avec tout le Bashân, c'est ce qu'on appelait la terre des géants.
3:14	Yaïr, fils de Menashè, a pris toute la région d'Argob jusqu'à la frontière des Guéshouriens et des Maakathiens, qu'il a appelé - ce Bashân - de son nom "Havoth-Jaïr"<!--bourgs de Jaïr.--> jusqu’à ce jour.
3:15	J'ai donné Galaad à Makir.
3:16	Aux Reoubénites et aux Gadites, j'ai donné de Galaad jusqu'au torrent de l'Arnon, dont le milieu du torrent sert de frontière, et jusqu'au torrent de Yabboq, frontière des fils d'Ammon ;
3:17	la région aride, et le Yarden, de la frontière de Kinnéreth jusqu'à la mer de la région aride, la Mer Salée, sous les pentes d'Ashdowth hap-Picgah<!--les montagnes de Pisga, qui incluent le Mont Nebo.--> vers l'est.
3:18	Or en ce temps-là, je vous ai donné cet ordre en disant : YHWH votre Elohîm, vous a donné cette terre, pour que vous en preniez possession. Vous passerez armés devant vos frères, les fils d'Israël, vous tous, fils talentueux.
3:19	Seulement vos femmes, vos petits-enfants, et vos troupeaux, je sais que vous avez beaucoup de troupeaux, resteront dans les villes que je vous ai données,
3:20	jusqu'à ce que YHWH ait accordé du repos à vos frères comme à vous, et qu'ils aient eux-mêmes pris possession de la terre que YHWH, votre Elohîm, leur donne de l'autre côté du Yarden. Retournez, chaque homme à son héritage que je vous ai donné.
3:21	En ce temps-là, je donnai cet ordre à Yéhoshoua en disant : Tes yeux ont vu tout ce que YHWH, votre Elohîm, a fait à ces deux rois : YHWH en fera de même à tous les royaumes vers lesquels tu vas passer.
3:22	N'ayez pas peur d'eux, car YHWH, votre Elohîm, combattra lui-même pour vous.

### Moshé n'entrera pas sur la terre promise

3:23	En ce même temps, j'ai imploré la grâce de YHWH, en disant :
3:24	Adonaï YHWH, tu as commencé à montrer à ton serviteur ta grandeur et ta main puissante. Car quel est le el dans les cieux et sur la Terre qui puisse accomplir tes œuvres et tes actions puissantes ?
3:25	Que je passe, s'il te plaît, et que je voie cette bonne terre de l'autre côté du Yarden, ces bonnes montagnes et le Liban.
3:26	Mais YHWH s'irrita contre moi, à cause de vous, et ne m'écouta pas. YHWH me dit : Assez ! Ne me parle plus de cette affaire !
3:27	Monte au sommet du Pisgah, et lève les yeux vers l'ouest, vers le nord, vers le sud et vers l'est, et regarde de tes yeux, mais tu ne passeras pas ce Yarden.
3:28	Donne des ordres à Yéhoshoua, fortifie-le et affermis-le, car c'est lui qui passera devant ce peuple et qui le mettra en possession de la terre que tu verras.
3:29	Et nous sommes restés dans la vallée, vis-à-vis de Beth-Peor.

## Chapitre 4

### Encouragement à garder la torah de YHWH

4:1	Maintenant, Israël, écoute les lois et les ordonnances que je vous enseigne pour les pratiquer, afin que vous viviez, et que vous entriez pour prendre possession de la terre que vous donne YHWH, l'Elohîm de vos pères.
4:2	Vous n'ajouterez<!--De. 13:1 ; Pr. 30:6 ; Ap. 22:18-19.--> rien à la parole que je vous ordonne, et vous n'en retrancherez rien, afin de garder les commandements de YHWH, votre Elohîm, que je vous ordonne.
4:3	Vos yeux ont vu ce que YHWH a fait à cause de Baal-Peor : YHWH, ton Elohîm, a détruit du milieu de toi tout homme qui était allé après Baal-Peor<!--No. 25:4-9.-->.
4:4	Mais vous, qui vous êtes attachés à YHWH, votre Elohîm, vous êtes tous vivants aujourd'hui.
4:5	Regardez, je vous ai enseigné des lois et des ordonnances, comme YHWH mon Elohîm, me l'a ordonné, afin que vous les mettiez en pratique au sein de la terre où vous entrez pour en prendre possession.
4:6	Vous les garderez et vous les pratiquerez, car c'est là votre sagesse et votre discernement aux yeux de tous les peuples. Lorsqu'ils entendront parler de toutes ces lois, ils diront : Cette grande nation est un peuple sage et intelligent !

### Israël, privilégié parmi tous les peuples

4:7	Car quelle grande nation a des elohîm proches d’elle, comme YHWH, notre Elohîm, toutes les fois que nous l'invoquons ?
4:8	Et quelle est la grande nation qui ait des lois et des ordonnances justes, comme toute cette torah que je mets aujourd'hui devant vous ?
4:9	Seulement, prends garde à toi et garde attentivement ton âme, tous les jours de ta vie, afin que tu n'oublies pas les choses que tes yeux ont vues, et qu'elles ne sortent de ton cœur<!--Pr. 4:23.-->. Enseigne-les à tes fils et aux fils de tes fils.
4:10	Le jour où tu t'es tenu face à YHWH, ton Elohîm, à Horeb, lorsque YHWH m'a dit : Rassemble-moi le peuple ! Je leur ferai entendre mes paroles, afin qu'ils apprennent à me craindre tous les jours qu’ils vivront sur le sol et qu'ils les enseignent à leurs fils.
4:11	Vous vous êtes approchés et vous vous êtes tenus au pied de la montagne. Or la montagne était embrasée par le feu jusqu'au milieu des cieux. Il y avait une profonde obscurité, des nuées et une ténèbre.
4:12	YHWH vous a parlé du milieu du feu : vous avez entendu la voix de ses paroles, mais vous n'avez vu aucune image, il n'y avait que la voix<!--Ex. 19:17-19.-->.
4:13	Et il vous a fait connaître son alliance, qu'il vous a ordonnée d'observer, les dix paroles, et il les a écrites sur deux tablettes de pierre.
4:14	YHWH m'a ordonné aussi, en ce temps-là, de vous enseigner les lois et les ordonnances, afin que vous les mettiez en pratique sur la terre vers laquelle vous passez pour en prendre possession.
4:15	Prenez bien garde à vos âmes, puisque vous n'avez vu aucune image le jour où YHWH vous a parlé du milieu du feu à Horeb,
4:16	de peur que vous ne vous corrompiez et que vous ne vous fassiez une idole, une image de quelque idole, ayant la figure d'un mâle ou d'une femelle,
4:17	ou la figure d'un animal qui soit sur la terre, ou la figure d'un oiseau ailé qui vole dans les cieux,
4:18	ou la figure d'un animal qui rampe sur le sol, ou la figure d'un poisson qui soit dans les eaux au-dessous de la terre.
4:19	De peur que, levant tes yeux vers les cieux, et voyant le soleil, la lune, et les étoiles, toute l'armée des cieux, tu ne sois banni. Ne te prosterne pas devant eux et ne les sers pas, eux que YHWH, ton Elohîm, a donnés en partage à tous les peuples, sous tous les cieux.
4:20	Mais vous, YHWH vous a pris, et vous a fait sortir d'Égypte, du fourneau de fer, afin que vous deveniez le peuple de son héritage, comme vous l'êtes aujourd'hui.

### Conséquences de la désobéissance et de l'idolâtrie

4:21	Or YHWH était en colère contre moi, à cause de vos paroles, et il a juré que je ne passerai pas le Yarden, et que je n'entrerai pas dans cette bonne terre que YHWH, ton Elohîm, te donne en héritage.
4:22	Je mourrai dans cette terre-ci, je ne passerai pas le Yarden, mais vous, vous le passerez et vous prendrez possession de cette bonne terre.
4:23	Gardez-vous d'oublier l'alliance de YHWH, votre Elohîm, qu'il a traitée avec vous, et que vous ne vous fassiez une idole, une image de quoi que ce soit, contrairement à ce que YHWH, ton Elohîm, t'a ordonné.
4:24	Car YHWH, ton Elohîm, est le feu dévorant<!--Hé. 12:29.-->, le El jaloux<!--El Qanna.-->.
4:25	Quand tu auras engendré des fils, et des fils de tes fils, et que vous serez depuis longtemps sur la terre, si vous vous corrompez, et que vous faites des idoles, ou des images de quelque chose que ce soit, si vous faites ce qui est mal aux yeux de YHWH, votre Elohîm, afin de l'irriter,
4:26	j'en prends aujourd'hui à témoin contre vous les cieux et la Terre, vous périrez, vous périrez promptement de dessus la terre dont vous allez prendre possession de l'autre côté du Yarden, vous n'y prolongerez pas vos jours, car vous serez détruits, détruits.
4:27	YHWH vous dispersera parmi les peuples, et vous ne resterez qu'un petit nombre parmi les nations, chez lesquelles YHWH vous emmènera.
4:28	Et là, vous servirez des elohîm, œuvres de main d'être humain, du bois et de la pierre, qui ne peuvent voir, ni entendre, ni manger, ni sentir<!--Es. 44:9, 46:7 ; Ps. 115:4-7.-->.

### YHWH, puissant, miséricordieux et fidèle à son alliance

4:29	Mais de là, tu chercheras YHWH, ton Elohîm, et tu le trouveras, si tu le cherches de tout ton cœur et de toute ton âme.
4:30	Dans ta détresse, lorsque toutes ces paroles t’atteindront dans les derniers jours, tu reviendras à YHWH, ton Elohîm, et tu obéiras à sa voix.
4:31	Parce que YHWH, ton Elohîm, est le El miséricordieux<!--El Rachuwm.-->. Il ne t'abandonnera pas et ne te détruira pas, il n'oubliera pas l'alliance de tes pères qu'il leur a jurée.
4:32	En effet, informe-toi, s'il te plaît, sur les premiers temps qui ont été avant toi, depuis le jour où Elohîm créa l'être humain sur la Terre, et d'une extrémité des cieux à l'autre extrémité des cieux : est-ce qu'il est arrivé une grande chose comme celle-là ou est-ce qu'on a entendu pareille chose ?
4:33	Est-ce qu'un peuple a entendu la voix d'Elohîm parlant du milieu du feu, comme tu l'as entendue, et qui soit resté en vie ?
4:34	Ou Elohîm a-t-il essayé de venir prendre pour lui une nation du milieu d'une nation, par des épreuves, des signes, des miracles, et des batailles, à main forte, à bras étendu et par de grandes terreurs, comme tout ce que YHWH, notre Elohîm, a fait pour vous en Égypte, sous vos yeux ?
4:35	Toi, on t’a fait voir pour que tu saches que YHWH est Elohîm : il n’y en a pas d’autre que lui<!--Es. 46:9.-->.
4:36	Il t'a fait entendre sa voix des cieux pour t'instruire et il t'a montré son grand feu sur la Terre, et tu as entendu ses paroles du milieu du feu.
4:37	Parce qu’il a aimé tes pères, il a choisi sa postérité après eux et il t'a fait sortir d'Égypte en face de lui, par sa grande force,
4:38	pour déposséder devant toi des nations plus grandes et plus puissantes que toi, pour te faire venir et te donner leur terre en possession, comme en ce jour.
4:39	Sache en ce jour et retourne-le en ton cœur que YHWH est Elohîm en haut dans les cieux et sur la Terre en bas : il n’y en a pas d’autre.
4:40	Garde ses lois et ses commandements que je t'ordonne aujourd'hui, afin que tu sois heureux, toi et tes fils après toi, et que tu prolonges tes jours sur le sol que YHWH, ton Elohîm, te donne tous les toujours<!--Ex. 20.-->.

### Trois villes de refuge à l'est du Yarden (Jourdain)

4:41	Alors Moshé sépara trois villes de l'autre côté du Yarden, du côté du soleil levant,
4:42	pour que s’y enfuie le meurtrier qui tue son prochain sans le savoir<!--Sans connaissance.-->, sans l'avoir haï ni d'hier ni d'avant-hier : il s’enfuira dans l'une de ces villes et vivra.
4:43	C'étaient : Betser dans le désert, dans la plaine de la terre, chez les Reoubénites, Ramoth en Galaad, chez les Gadites et Golan en Bashân, chez les Menashites.
4:44	Voici la torah que Moshé plaça face aux fils d'Israël.
4:45	Voici les témoignages, les lois, et les ordonnances que Moshé déclara aux fils d'Israël, après qu'ils furent sortis d'Égypte.
4:46	C'était de l'autre côté du Yarden, dans la vallée, vis-à-vis de Beth-Peor, en terre de Sihon, roi des Amoréens, qui demeurait à Hesbon, et qui fut battu par Moshé et les fils d'Israël après être sortis d'Égypte.
4:47	Ils prirent possession de sa terre avec la terre d'Og, roi de Bashân, deux rois des Amoréens qui étaient de l'autre côté du Yarden, du côté du soleil levant.
4:48	Depuis Aroër, sur le bord du torrent de l'Arnon, jusqu'à la montagne de Sion, qui est l'Hermon,
4:49	et toute la région aride de l'autre côté du Yarden, du côté du soleil levant, jusqu'à la mer de la région aride, au pied du Pisgah.

## Chapitre 5

### L'alliance établie à Horeb rappelée à la nouvelle génération

5:1	Moshé appela tout Israël, et leur dit : Écoute, Israël, les lois et les ordonnances que je prononce aujourd'hui à vos oreilles, apprenez-les, et veillez à les mettre en pratique.
5:2	YHWH, notre Elohîm, a traité avec nous une alliance en Horeb<!--Ex. 19:5.-->.
5:3	YHWH n'a pas traité cette alliance avec nos pères, mais avec nous, qui sommes ici aujourd'hui tous vivants.
5:4	YHWH vous a parlé faces à faces sur la montagne du milieu du feu.
5:5	Je me tenais en ce temps-là entre YHWH et vous pour vous faire connaître la parole de YHWH. Car vous aviez peur face au feu et vous n'êtes pas montés dans la montagne. Il a dit<!--Les dix paroles (Ex. 20).--> :
5:6	Je suis YHWH, ton Elohîm, qui t'ai fait sortir de la terre d'Égypte, de la maison des esclaves.
5:7	Tu n'auras pas d'autres elohîm contre mes faces.
5:8	Tu ne te feras pas d'idole, ni d'image des choses qui sont en haut dans les cieux, ni sur la terre, ni dans les eaux sous la terre.
5:9	Tu ne te prosterneras pas devant elles, et tu ne les serviras pas, car je suis YHWH, ton Elohîm, le El jaloux, qui punit l'iniquité des pères sur les enfants jusqu'à la troisième et à la quatrième génération de ceux qui me haïssent,
5:10	et qui fait miséricorde jusqu'à 1 000 générations à ceux qui m'aiment et qui gardent mes commandements.
5:11	Tu ne prendras pas en vain le Nom de YHWH, ton Elohîm, car YHWH ne tiendra pas pour innocent celui qui aura pris en vain son Nom.
5:12	Garde le jour du shabbat pour le sanctifier, comme YHWH, ton Elohîm, te l'a ordonné.
5:13	Tu travailleras 6 jours, et tu feras tout ton travail,
5:14	mais le septième jour est le shabbat de YHWH, ton Elohîm : tu ne feras aucun travail, ni toi, ni ton fils, ni ta fille, ni ton serviteur, ni ta servante, ni ton bœuf, ni ton âne, ni aucune de tes bêtes, ni l'étranger qui est dans tes portes, afin que ton serviteur et ta servante se reposent comme toi.
5:15	Tu te souviendras que tu as été esclave en terre d'Égypte, et que YHWH, ton Elohîm, t'en a fait sortir à main forte et à bras étendu : c'est pourquoi YHWH, ton Elohîm, t'a ordonné d'observer le jour du shabbat.
5:16	Honore ton père et ta mère, comme YHWH, ton Elohîm, te l'a ordonné, afin que tes jours se prolongent et que tu sois heureux sur le sol que YHWH, ton Elohîm, te donne.
5:17	Tu n'assassineras pas.
5:18	Tu ne commettras pas d'adultère.
5:19	Tu ne voleras pas.
5:20	Tu ne répondras pas contre ton compagnon en témoin vain.
5:21	Tu ne convoiteras pas la femme de ton prochain. Tu ne désireras pas la maison de ton prochain, ni son champ, ni son serviteur, ni sa servante, ni son bœuf, ni son âne, ni aucune chose qui soit à ton prochain.
5:22	YHWH déclara ces paroles à toute votre assemblée sur la montagne, du milieu du feu, des nuées et des ténèbres épaisses, avec une grande voix, sans rien ajouter. Il les a écrites sur deux tablettes de pierre qu'il m'a données.

### Moshé, intermédiaire entre YHWH et le peuple

5:23	Or il arriva qu'aussitôt que vous avez entendu la voix du milieu de la ténèbre, parce que la montagne était embrasée par le feu, vos chefs de tribus et vos anciens se sont tous approchés de moi,
5:24	et vous avez dit : Voici, YHWH, notre Elohîm, nous a fait voir sa gloire et sa grandeur, et nous avons entendu sa voix du milieu du feu. Aujourd'hui, nous avons vu qu'Elohîm a parlé à l'être humain et qu'il est resté en vie.
5:25	Et maintenant pourquoi mourrions-nous ? Car ce grand feu nous dévorera. Si nous entendons encore la voix de YHWH, notre Elohîm, nous mourrons.
5:26	Car qui, de toute chair, a entendu comme nous la voix d'Elohîm le Vivant parlant du milieu du feu, et qui soit resté en vie ?
5:27	Toi, approche pour écouter tout ce que dira YHWH, notre Elohîm. Tu nous parleras, toi, de tout ce dont YHWH, notre Elohîm, t’aura parlé. Nous l'écouterons et nous le ferons.
5:28	YHWH entendit la voix de vos paroles pendant que vous me parliez. Et YHWH me dit : J'ai entendu la voix des paroles de ce peuple, celles qu'ils t'ont dites : tout ce qu'ils ont dit est bien.
5:29	Qui leur donnera un tel cœur pour me craindre et pour garder tous mes commandements, tous les jours, afin qu'ils soient heureux, eux et leurs enfants ?
5:30	Va, dis-leur : Retournez dans vos tentes !
5:31	Mais toi, reste ici avec moi, et je te dirai tous les commandements, les lois, et les ordonnances que tu leur enseigneras, afin qu'ils les pratiquent sur la terre que je leur donne pour qu'ils en prennent possession.
5:32	Vous prendrez garde de faire ce que YHWH, votre Elohîm, vous a ordonné, vous ne vous en détournerez ni à droite ni à gauche.
5:33	Vous marcherez dans toute la voie que YHWH, votre Elohîm, vous a ordonnée, afin que vous viviez et que vous soyez heureux, et que vous prolongiez vos jours sur la terre dont vous prendrez possession.

## Chapitre 6

### Obéissance à la torah, source de bénédictions

6:1	Voici les commandements, les lois et les ordonnances que YHWH, votre Elohîm, m'a ordonné de vous enseigner, afin que vous les mettiez en pratique sur la terre vers laquelle vous passez pour en prendre possession ;
6:2	afin que tu craignes YHWH, ton Elohîm, en gardant durant tous les jours de ta vie, toi, ton fils, et le fils de ton fils, tous ses statuts et ses commandements que je t'ordonne, pour que tes jours soient prolongés.
6:3	Tu les écouteras, Israël, et tu auras soin de les mettre en pratique, afin que tu sois heureux, et que vous vous multipliiez sur la terre où coulent le lait et le miel, comme YHWH, l'Elohîm de tes pères, l'a dit<!--Ex. 3:8.-->.
6:4	Écoute Israël ! YHWH, notre Elohîm, YHWH est un<!--Yaacov fut le premier à faire cette prière qui affirme l'unicité d'Elohîm. Elohîm est un (en hébreu « echad » ou « ehad »). Loin de l'infirmer ou de la contredire, Yéhoshoua (Jésus) a confirmé cette prière et l'enseignement capital qu'elle contient (Mc. 12:29). Elohîm n'est pas trois personnes en une, mais « un ». Cette parole annonce un monothéisme absolu. Elle s'oppose catégoriquement au polythéisme des Kena'ânéens (Cananéens) qui adoraient de multiples dieux, les étoiles, la lune, le soleil, les arbres, les rois, etc. Aussi les Hébreux avaient reçu l'ordre de la part de YHWH de détruire toutes les idoles qu'ils trouveraient dans la terre promise (De. 16:21). Voir également commentaire en Ge. 1:5.-->.
6:5	Tu aimeras Aleph Tav YHWH, ton Elohîm, de tout ton cœur, de toute ton âme, et de toute ta force<!--De. 10:12 ; Mt. 22:37 ; Mc. 12:30 ; Lu. 10:27.-->.

### La torah de YHWH doit être enseignée aux enfants

6:6	Et ces paroles, que je t'ordonne aujourd'hui, seront dans ton cœur.
6:7	Tu les enseigneras d'une manière incisive à tes enfants. Tu en parleras quand tu seras assis dans ta maison, quand tu iras en voyage, quand tu te coucheras et quand tu te lèveras.
6:8	Et tu les lieras comme un signe sur tes mains, et elles seront comme des fronteaux entre tes yeux.
6:9	Tu les écriras aussi sur les poteaux de ta maison et sur tes portes.
6:10	Il arrivera que quand YHWH, ton Elohîm, t’aura fait entrer sur la terre qu'il a juré à tes pères, Abraham, Yitzhak, et Yaacov, de te donner, avec des villes grandes et bonnes que tu n’as pas bâties,
6:11	des maisons pleines de toutes sortes de biens que tu n'as pas remplies, des puits creusés que tu n'as pas creusés, des vignes et des oliviers que tu n'as pas plantés, tu mangeras, et tu te rassasieras,
6:12	prends garde à toi, de peur que tu n'oublies YHWH, qui t'a fait sortir de la terre d'Égypte, de la maison des esclaves.
6:13	Tu craindras YHWH, ton Elohîm, tu le serviras et tu jureras par son Nom.
6:14	Vous n'irez pas après d'autres elohîm, d'entre les elohîm des peuples qui sont autour de vous,
6:15	car YHWH, ton El, qui est au milieu de toi, est l'Elohîm jaloux, de peur que la colère de YHWH, ton Elohîm, ne s'enflamme contre toi, et qu'il ne t'extermine sur les faces du sol.
6:16	Vous ne tenterez pas YHWH, votre Elohîm, comme vous l'avez tenté à Massah.
6:17	Vous garderez, vous garderez les commandements de YHWH, votre Elohîm, ses témoignages et ses lois qu'il vous a ordonnés.
6:18	Tu feras ce qui est droit et bon aux yeux de YHWH, afin que tu sois heureux, que tu entres et que tu prennes possession de la bonne terre que YHWH a jurée à tes pères,
6:19	après qu'il aura chassé tous tes ennemis de devant toi, comme YHWH l'a dit.
6:20	Quand ton enfant t'interrogera à l'avenir, en disant : Que veulent dire ces témoignages, ces lois, et ces ordonnances que YHWH, notre Elohîm, vous a ordonnés ?
6:21	Tu diras à ton enfant : Nous étions esclaves de pharaon en Égypte, et YHWH nous a fait sortir de l'Égypte par sa main puissante.
6:22	YHWH a fait sous nos yeux des signes et des miracles, grands et désastreux contre l'Égypte, contre pharaon et contre toute sa maison,
6:23	et il nous a fait sortir de là pour nous conduire sur la terre qu'il avait juré à nos pères de nous donner.
6:24	YHWH nous a ordonné de pratiquer toutes ces lois, et de craindre YHWH, notre Elohîm, afin que nous soyons toujours heureux, et qu'il préserve notre vie, comme aujourd'hui.
6:25	Et ceci sera notre justice, que nous prenions garde de pratiquer tous ces commandements devant YHWH, notre Elohîm, comme il nous l'a ordonné.

## Chapitre 7

### YHWH interdit les alliances avec les peuples païens

7:1	Quand YHWH ton Elohîm, t'aura fait entrer sur la terre où tu vas entrer pour en prendre possession, et qu'il aura chassé devant toi beaucoup de nations : les Héthiens, les Guirgasiens, les Amoréens, les Kena'ânéens, les Phéréziens, les Héviens et les Yebousiens, sept nations plus grandes et plus puissantes que toi,
7:2	et que YHWH, ton Elohîm, te les aura livrées en face et que tu les auras battues, tu les dévoueras par interdit, tu les dévoueras par interdit. Tu ne traiteras pas d'alliance avec elles et tu n’auras pour elles aucune pitié.
7:3	Tu ne t'allieras pas par mariage avec elles, tu ne donneras pas tes filles à leurs fils, et tu ne prendras pas leurs filles pour tes fils<!--Jos. 23:12-13 ; 1 R. 11:1-4.-->.
7:4	Car elles détourneraient de moi tes fils, et ils serviraient d'autres elohîm<!--1 R. 11:1-4.-->, et la colère de YHWH s'enflammerait contre vous : Il te détruirait promptement.
7:5	Mais vous les traiterez de cette manière : vous renverserez leurs autels, vous briserez leurs monuments, vous abattrez leurs asherah<!--Le terme hébreu « asherah » est cité au moins 40 fois dans le Tanakh. Il fait référence à un objet en bois utilisé dans le culte d'Astarté, l'épouse de Baal. Voir De. 16:21.-->, et vous brûlerez au feu leurs images gravées.
7:6	Car tu es un peuple saint pour YHWH, ton Elohîm. YHWH, ton Elohîm, t'a choisi pour que tu sois son peuple, sa propriété parmi tous les peuples qui sont sur les faces du sol.
7:7	Ce n'est pas parce que vous êtes plus nombreux que tous les peuples que YHWH vous a aimés et qu'il vous a choisis, car vous êtes le plus petit de tous les peuples.
7:8	Mais c'est parce que YHWH vous aime et qu'il garde le serment qu'il a juré à vos pères. YHWH vous a fait sortir par sa main puissante et vous a rachetés de la maison des esclaves, de la main de pharaon, roi d'Égypte.
7:9	Sache que c'est YHWH, ton Elohîm, qui est Elohîm, le El fidèle, qui garde son alliance et sa miséricorde jusqu'à 1 000 générations envers ceux qui l'aiment et qui gardent ses commandements,
7:10	et qui rend la pareille en face à ceux qui le haïssent et les fait périr. Il ne diffère pas envers celui qui le hait, il lui rend la pareille en face.
7:11	Garde les commandements, les lois, et les ordonnances que je t'ordonne aujourd'hui, et mets-les en pratique.

### L'obéissance à YHWH, source de bénédictions et de victoires

7:12	Il arrivera que si vous écoutez ces ordonnances, si vous les gardez et les mettez en pratique, YHWH, ton Elohîm, gardera l'alliance et la bonté qu'il a jurées à tes pères.
7:13	Il t'aimera, te bénira et te multipliera. Il bénira le fruit de ton ventre, le fruit du sol, ton blé, ton vin nouveau, ton huile, la portée de tes bœufs et des troupeaux de tes brebis, sur le sol qu'il a juré de donner à tes pères.
7:14	Tu seras béni plus que tous les peuples. Il n'y aura chez toi et parmi tes bêtes ni mâle ni femelle stérile<!--Ex. 23:26.-->.
7:15	YHWH détournera de toi toute maladie. Il ne t'enverra aucune de ces mauvaises maladies d'Égypte qui te sont connues, mais il les fera venir sur tous ceux qui te haïssent.
7:16	Tu dévoreras tous les peuples que YHWH, ton Elohîm, va te livrer, ton œil n'aura pas de pitié, et tu ne serviras pas leurs elohîm, car cela te serait un piège.
7:17	Si tu dis dans ton cœur : Ces nations sont plus nombreuses que moi, comment pourrai-je les déposséder ?
7:18	N'aie pas peur d'elles. Rappelle-toi, rappelle-toi ce que YHWH, ton Elohîm, a fait à pharaon, et à tous les Égyptiens,
7:19	de ces grandes épreuves que tes yeux ont vues, les signes et les miracles, la main forte et le bras étendu par lesquels YHWH, ton Elohîm, t'a fait sortir. C'est ainsi que YHWH, ton Elohîm, agira envers tous ces peuples que tu crains.
7:20	YHWH, ton Elohîm, enverra même contre eux les frelons, jusqu'à ce que périssent ceux qui resteront, et ceux qui se seront cachés de devant toi.
7:21	Ne t'effraie pas devant eux, car YHWH, ton Elohîm, le El grand et redoutable est au milieu de toi.
7:22	Et YHWH, ton Elohîm, chassera peu à peu ces nations de devant toi. Tu ne pourras pas les exterminer promptement, de peur que les bêtes des champs ne se multiplient contre toi.
7:23	Mais YHWH, ton Elohîm, les livrera devant toi et il les affolera par de grandes confusions jusqu'à ce qu'elles soient détruites.
7:24	Et il livrera leurs rois entre tes mains, et tu feras disparaître leurs noms de dessous les cieux. Aucun homme ne tiendra face à toi, jusqu'à ce que tu les aies détruits.
7:25	Tu brûleras au feu les images gravées de leurs elohîm. Tu ne convoiteras pas et tu ne prendras pas pour toi l'argent et l'or qui seront sur elles, de peur que tu en sois pris au piège, car c'est une abomination pour YHWH, ton Elohîm.
7:26	Et tu n'introduiras pas l'abominable dans ta maison, de peur que tu ne deviennes, comme elle, voué à une entière destruction. Tu l'auras en horreur, tu l'auras en horreur et ce sera pour toi une abomination, une abomination, car c'est une chose vouée à une entière destruction.

## Chapitre 8

### Le désert, lieu de formation, d'humiliation et d'épreuve

8:1	Vous observerez et vous mettrez en pratique tous les commandements que je vous ordonne aujourd'hui, afin que vous viviez, que vous vous multipliiez, que vous entriez et que vous preniez possession de la terre que YHWH a juré de donner à vos pères.
8:2	Et souviens-toi de tout le chemin par lequel YHWH, ton Elohîm, t'a fait marcher pendant ces 40 ans dans ce désert, afin de t'humilier, de t'éprouver, pour connaître ce qui était dans ton cœur, et si tu garderais ses commandements ou non.
8:3	Il t'a humilié, il t'a laissé avoir faim, mais il t'a nourri de la manne, que tu ne connaissais pas et que tes pères n'avaient pas connue, afin de te faire connaître que l'être humain ne vivra pas de pain seulement, mais que l'être humain vivra de tout ce qui sort de la bouche de YHWH<!--Mt. 4:4 ; Lu. 4:4.-->.
8:4	Ton vêtement ne s'est pas usé sur toi, et ton pied ne s'est pas enflé durant ces 40 années<!--Né. 9:21.-->.
8:5	Reconnais dans ton cœur que YHWH, ton Elohîm, te châtie comme un homme châtie son enfant<!--Voir Hé. 12:5-12 et Ap. 3:19.-->.

### Se garder d'oublier YHWH

8:6	Et garde les commandements de YHWH, ton Elohîm, pour marcher dans ses voies, et pour le craindre.
8:7	Car YHWH, ton Elohîm, va te faire venir vers une bonne terre, une terre de torrents d'eaux, de sources et des eaux souterraines qui jaillissent dans les vallées et dans les montagnes,
8:8	une terre de blé, d'orge, de vignes, de figuiers et de grenadiers, une terre d'oliviers donnant de l'huile et du miel,
8:9	une terre où tu ne mangeras pas le pain dans la pauvreté, où tu ne manqueras de rien, une terre dont les pierres sont du fer et où tu pourras extraire le cuivre des montagnes.
8:10	Tu mangeras et tu te rassasieras, tu béniras YHWH, ton Elohîm, pour la bonne terre qu'il t'a donnée.
8:11	Prends garde à toi de peur que tu n'oublies YHWH, ton Elohîm, en ne gardant pas ses commandements, ses ordonnances et ses statuts que je t'ordonne aujourd'hui,
8:12	de peur que quand tu mangeras et que tu seras rassasié, que tu bâtiras et habiteras de belles maisons,
8:13	que ton gros et petit bétail se multiplieront, que ton argent et ton or augmenteront et que tout ce qui est à toi se multipliera,
8:14	que ton cœur ne s'élève pas et que tu n'oublies pas YHWH, ton Elohîm, qui t'a fait sortir de la terre d'Égypte, de la maison des esclaves,
8:15	qui t'a fait marcher dans ce grand et affreux désert de serpents brûlants et de scorpions, sol assoiffé et sans eau, et qui a fait jaillir pour toi de l'eau du rocher le plus dur,
8:16	qui t'a fait manger dans ce désert la manne que tes pères n'avaient pas connue, afin de t'humilier et de t'éprouver, pour te faire ensuite du bien,
8:17	et que tu ne dises dans ton cœur : Ma force et la puissance de ma main m'ont acquis ces richesses.
8:18	Mais tu te souviendras de YHWH, ton Elohîm, car c'est lui qui te donne de la force pour acquérir ces richesses, afin de confirmer son alliance, qu'il a jurée à tes pères, comme en ce jour.
8:19	Et s'il arrive que tu oublies, que tu oublies YHWH, ton Elohîm, et que tu ailles après d'autres elohîm, si tu les sers, et que tu te prosternes devant eux, je vous avertis aujourd'hui que vous périrez, vous périrez.
8:20	Vous périrez comme les nations que YHWH fait périr devant vous, parce que vous n'aurez pas obéi à la voix de YHWH, votre Elohîm.

## Chapitre 9

### YHWH, fidèle à son alliance malgré la rébellion du peuple

9:1	Écoute Israël ! Tu vas passer aujourd'hui le Yarden pour entrer en possession de nations plus grandes et plus puissantes que toi, de villes grandes et fortifiées jusqu'aux cieux,
9:2	un peuple grand et de haute taille, les fils d'Anak, que tu connais, et dont tu as entendu dire : Qui tiendra face aux fils d'Anak ?
9:3	Sache aujourd'hui que YHWH, ton Elohîm, passera devant toi, comme un feu dévorant. C'est lui qui les détruira, qui les humiliera devant toi, et tu les déposséderas, tu les feras périr promptement, comme YHWH te l'a dit.
9:4	Ne parle pas dans ton cœur, quand YHWH, ton Elohîm, les chassera devant toi, en disant : C'est à cause de ma justice que YHWH me fait entrer en possession de cette terre. Mais c'est à cause de la méchanceté de ces nations-là que YHWH les dépossède devant toi.
9:5	Non, ce n'est pas à cause de ta justice et de la droiture de ton cœur que tu entres en possession de leur terre, mais c'est à cause de la méchanceté de ces nations-là que YHWH, ton Elohîm, les dépossède devant toi, et c'est pour confirmer la parole que YHWH a jurée à tes pères, Abraham, Yitzhak, et Yaacov.
9:6	Sache que ce n'est pas à cause de ta justice que YHWH, ton Elohîm, te donne cette bonne terre pour que tu en prennes possession, car tu es un peuple au cou raide.
9:7	Souviens-toi, n'oublie pas que tu as excité la colère de YHWH, ton Elohîm, dans le désert. Depuis le jour où tu es sorti de la terre d'Égypte jusqu'à ce que vous arriviez dans ce lieu, vous avez été rebelles contre YHWH.
9:8	À Horeb, vous avez excité la colère de YHWH et YHWH était en colère contre vous, pour vous détruire.
9:9	Quand je suis monté sur la montagne pour prendre les tablettes de pierre, les tablettes de l'alliance que YHWH a traitée avec vous, je suis resté sur la montagne 40 jours et 40 nuits, sans manger de pain et sans boire d'eau.
9:10	YHWH m'a donné les deux tablettes de pierre écrites du doigt d'Elohîm et sur lesquelles étaient toutes les paroles que YHWH avait déclarées sur la montagne du milieu du feu, le jour de l'assemblée.
9:11	Il est arrivé qu'au bout de 40 jours et 40 nuits, YHWH m'a donné les deux tablettes de pierre, qui sont les tablettes de l'alliance.
9:12	YHWH m'a dit : Lève-toi, descends promptement d'ici. Car ton peuple que tu as fait sortir d'Égypte s'est corrompu. Ils se sont détournés promptement de la voie que je leur avais ordonnée, ils se sont fait une image en métal fondu<!--Le veau d'or (Ex. 32).-->.
9:13	YHWH m'a parlé, en disant : Je vois que ce peuple est un peuple au cou raide.
9:14	Laisse-moi les détruire et effacer leur nom de dessous les cieux, et je te ferai devenir une nation plus puissante et plus grande que celle-ci.
9:15	Je me suis tourné pour descendre de la montagne, en tenant de mes deux mains les deux tablettes de l'alliance. Or la montagne était brûlante de feu.
9:16	J'ai regardé, et voici, vous aviez péché contre YHWH, votre Elohîm, vous vous étiez fait un veau en métal fondu, vous vous étiez détournés promptement de la voie que vous avait ordonnée YHWH.
9:17	J'ai saisi les deux tablettes, je les ai jetées de mes deux mains et je les ai brisées sous vos yeux.

### Moshé intercède pour Israël devant YHWH

9:18	Je suis tombé, face à YHWH, comme auparavant, 40 jours et 40 nuits, sans manger de pain et sans boire d'eau, à cause de tout votre péché, que vous aviez commis en faisant ce qui est mal aux yeux de YHWH, afin de l'irriter.
9:19	Car j'avais peur face à la colère et au courroux dont YHWH était enflammé contre vous, pour vous détruire. Et YHWH m'a écouté encore cette fois.
9:20	YHWH était très en colère contre Aaron, voulant le faire périr, mais j'ai intercédé pour Aaron en ce temps-là.
9:21	J'ai pris le veau<!--Le veau d'or (Ex. 32).--> que vous aviez fait, votre péché, et je l'ai brûlé au feu, je l'ai mis en pièces, je l'ai bien moulu et pulvérisé en poussière, et j'ai jeté cette poussière dans le torrent qui descend de la montagne.
9:22	Vous avez excité la colère de YHWH à Tab`érah, à Massah, et à Qibroth hat-Ta'avah.
9:23	Et quand YHWH vous a envoyés à Qadesh-Barnéa, en disant : Montez, et prenez possession de la terre que je vous donne ! Vous vous êtes rebellés contre la bouche de YHWH, votre Elohîm, vous ne l'avez pas cru, vous n'avez pas obéi à sa voix.
9:24	Vous avez été rebelles à YHWH depuis le jour où je vous ai connus.
9:25	Je suis tombé, face à YHWH, 40 jours et 40 nuits, je suis tombé parce que YHWH avait dit qu'il vous détruirait.
9:26	J'ai prié YHWH et j'ai dit : Adonaï YHWH, ne détruis pas ton peuple, ton héritage que tu as racheté par ta grandeur, et que tu as fait sortir d'Égypte par ta main puissante.
9:27	Souviens-toi de tes serviteurs Abraham, Yitzhak, et Yaacov. Ne te tourne pas vers l'obstination de ce peuple, ni vers sa méchanceté, ni vers son péché,
9:28	de peur que la terre d'où tu nous as fait sortir ne dise : C'est parce que YHWH n'était pas capable de les conduire sur la terre qu'il leur avait promise, et parce qu'il les haïssait, il les a fait sortir pour les faire mourir dans le désert.
9:29	Cependant ils sont ton peuple et ton héritage, que tu as fait sortir par ta grande puissance et par ton bras étendu.

## Chapitre 10

### Rappel du remplacement des tablettes de la torah

10:1	En ce temps-là YHWH m'a dit : Taille deux tablettes de pierre comme les premières, et monte vers moi sur la montagne. Tu feras une arche en bois<!--Ex. 25:10, 34:1-4.-->.
10:2	Et j'écrirai sur ces tablettes les paroles qui étaient sur les premières tablettes que tu as brisées, et tu les mettras dans l'arche.
10:3	J'ai fait une arche en bois d'acacia, j'ai taillé deux tablettes de pierre comme les premières, et je suis monté sur la montagne, les deux tablettes dans ma main<!--Ex. 34:4.-->.
10:4	Il a écrit sur ces tablettes ce qui avait été écrit sur les premières, les dix paroles qu'il vous avait dites sur la montagne, du milieu du feu, le jour de l'assemblée. Puis YHWH me les a données.
10:5	Je me suis tourné et je suis descendu de la montagne. J'ai mis les tablettes dans l'arche que j'avais faite, et elles y sont demeurées, comme YHWH me l'avait ordonné.
10:6	Or les fils d'Israël partirent de Beéroth Bené Ya`aqan pour Moséra. Là mourut Aaron, et il fut enseveli. Èl’azar, son fils, exerça la prêtrise à sa place.
10:7	De là ils partirent pour Goudgoda, et de Goudgoda pour Yotbathah, qui est une terre de torrents d'eau.
10:8	Or en ce temps-là, YHWH sépara la tribu de Lévi afin de porter l'arche de l'alliance de YHWH, de se tenir devant YHWH, de le servir, et de bénir en son Nom, jusqu'à ce jour.
10:9	C'est pourquoi Lévi n'a ni portion ni d'héritage avec ses frères : YHWH est son héritage, comme YHWH, ton Elohîm, lui a dit.
10:10	Je suis resté sur la montagne, comme la première fois, 40 jours et 40 nuits. YHWH m'a écouté encore cette fois : YHWH n'a pas voulu te détruire.
10:11	Mais YHWH m'a dit : Lève-toi, va, marche devant ce peuple. Qu'ils aillent prendre possession de la terre que j'ai juré à leurs pères de leur donner.

### Une alliance basée sur l'amour de YHWH

10:12	Maintenant, Israël, que demande de toi YHWH, ton Elohîm ? N'est-ce pas que tu craignes YHWH, ton Elohîm, afin de marcher dans toutes ses voies, que tu aimes et serves YHWH, ton Elohîm, de tout ton cœur et de toute ton âme,
10:13	que tu gardes les commandements de YHWH et ses statuts que je t'ordonne aujourd'hui, afin que tu sois heureux ?
10:14	Voici, les cieux, et les cieux des cieux appartiennent à YHWH, ton Elohîm, la Terre et tout ce qu'elle renferme.
10:15	YHWH s'est attaché seulement à tes pères pour les aimer. Et après eux, il vous a choisis, vous, leur postérité, entre tous les peuples, comme en ce jour.
10:16	Circoncisez le prépuce de votre cœur, et vous ne raidirez plus votre cou.
10:17	Car YHWH, votre Elohîm, est l'Elohîm des elohîm, Adonaï des seigneurs<!--YHWH, l'Elohîm des elohîm et le Seigneur des seigneurs n'est autre que Yéhoshoua ha Mashiah (Jésus-Christ), notre Seigneur qui s'est révélé à Yohanan (Jean) comme le Seigneur des seigneurs et le Roi des rois (Ap. 17:14, 19:16). Voir aussi 1 Ti. 6:15.-->, le El Gadowl<!--El Gadowl signifie « El Grand ».-->, le Gibbor<!--Gibbor signifie « Puissant » dans Es. 9:5.-->, le Redoutable, qui n'a pas d'égard à l'apparence des personnes et qui n'accepte pas de pot-de-vin,
10:18	qui fait justice à l'orphelin et à la veuve, qui aime l'étranger et lui donne le pain et le vêtement.
10:19	Vous aimerez l'étranger, car vous avez été étrangers en terre d'Égypte.
10:20	Tu craindras YHWH, ton Elohîm, tu le serviras, tu t'attacheras à lui, et tu jureras par son Nom.
10:21	Il est ta louange, il est ton Elohîm, qui a fait pour toi des choses grandes et redoutables que tes yeux ont vues.
10:22	Tes pères descendirent en Égypte au nombre de 70 âmes, et maintenant YHWH, ton Elohîm, t'a fait devenir comme les étoiles des cieux, tant tu es en grand nombre.

## Chapitre 11

### Exhortation à la reconnaissance et à l'obéissance

11:1	Tu aimeras YHWH, ton Elohîm, et tu garderas son dépôt, ses statuts, ses ordonnances et ses commandements tous les jours.
11:2	Vous connaissez aujourd'hui - car ce n'est pas le cas de vos fils, qui n'ont pas connu et qui n'ont pas vu le châtiment de YHWH, votre Elohîm, sa grandeur, sa main puissante, son bras étendu,
11:3	ses signes, et les œuvres qu'il a accomplies au milieu de l'Égypte contre pharaon, roi d'Égypte, et contre toute sa terre.
11:4	Ce qu'il a fait à l'armée d'Égypte, à ses chevaux et à ses chars, quand il a fait déborder sur leurs faces les eaux de la Mer Rouge, dans leur poursuite, derrière vous, YHWH les a détruits jusqu'à ce jour<!--Ex. 14:28.-->.
11:5	Ce qu'il a fait dans le désert, jusqu'à votre arrivée en ce lieu-ci,
11:6	ce qu'il a fait à Dathan et à Abiram, fils d'Éliy'ab, fils de Reouben, comment la terre ouvrit sa bouche et les engloutit, avec leurs maisons et leurs tentes, et tous les êtres qui les suivaient, au milieu de tout Israël<!--No. 16:1-33.-->.
11:7	Car ce sont vos yeux qui ont vu toutes les grandes œuvres que YHWH a faites.

### Les bienfaits de la terre promise sont pour un peuple fidèle

11:8	Vous garderez tous les commandements que je vous ordonne aujourd'hui, afin que vous deveniez forts et que vous entriez prendre possession de la terre où vous allez passer pour en prendre possession,
11:9	afin que vous prolongiez vos jours sur le sol que YHWH a juré à vos pères de leur donner, ainsi qu'à leur postérité, terre où coulent le lait et le miel.
11:10	Car la terre où tu vas entrer pour en prendre possession n'est pas comme la terre d'Égypte, d'où vous êtes sortis, où tu semais ta semence, et l'arrosais avec ton pied, comme un jardin potager.
11:11	Mais la terre où vous allez passer pour en prendre possession est une terre de montagnes et de vallées, qui boit les eaux de la pluie des cieux.
11:12	C'est une terre dont YHWH, ton Elohîm, prend soin, et sur laquelle YHWH, ton Elohîm, a continuellement ses yeux, du commencement de l'année jusqu'à la fin de l'année.
11:13	Il arrivera que, si vous obéissez, si vous obéissez à mes commandements que je vous ordonne aujourd'hui, si vous aimez YHWH, votre Elohîm, et que vous le servez de tout votre cœur et de toute votre âme,
11:14	je donnerai à votre terre la pluie en son temps, la pluie d'automne<!--Ou première pluie.--> et la pluie de printemps<!--Ou dernière pluie. Voir commentaire en Za. 10:1.-->, et tu recueilleras ton blé, ton vin nouveau, et ton huile.
11:15	Je mettrai aussi dans ton champ de l'herbe pour ton bétail, tu mangeras et tu seras rassasié.
11:16	Prenez garde à vous, de peur que votre cœur ne soit trompé, et que vous ne vous détourniez, et ne serviez d'autres elohîm, et ne vous prosterniez devant eux.
11:17	Et que la colère de YHWH ne s'enflamme contre vous, et qu'il ne ferme les cieux, en sorte qu'il n'y ait pas de pluie, et que le sol ne donne plus son fruit, et de peur que vous ne périssiez rapidement de dessus cette bonne terre que YHWH vous donne.
11:18	Mettez dans votre cœur et dans votre âme ces paroles. Liez-les comme un signe sur vos mains, et qu'elles soient comme des fronteaux entre vos yeux.
11:19	Tu les enseigneras à tes fils, en leur en parlant quand tu habiteras dans ta maison et quand tu marcheras par le chemin, quand tu te coucheras et quand tu te lèveras.
11:20	Tu les écriras aussi sur les poteaux de ta maison, et sur tes portes.
11:21	Afin que vos jours et les jours de vos fils, sur le sol que YHWH a juré à vos pères de leur donner, soient aussi nombreux que les jours des cieux sur la Terre.
11:22	Car si vous gardez, si vous gardez tous ces commandements que je vous ordonne d'observer, aimant YHWH, votre Elohîm, marchant dans toutes ses voies, et vous attachant à lui,
11:23	YHWH dépossédera devant vous toutes ces nations, et vous déposséderez des nations plus grandes et plus puissantes que vous.
11:24	Tout lieu que foulera la plante de votre pied sera à vous<!--Jos. 1:3, 14:9.--> : Votre territoire s'étendra du désert au Liban, et du fleuve, le fleuve Euphrate, jusqu'à la mer occidentale.
11:25	Aucun homme ne tiendra face à vous. YHWH, votre Elohîm, mettra, comme il vous l'a dit, la terreur et la crainte de vous sur toute la terre où vous marcherez.

### La malédiction et la bénédiction

11:26	Regardez, je mets aujourd'hui devant vous la bénédiction et la malédiction :
11:27	la bénédiction, si vous obéissez aux commandements de YHWH, votre Elohîm, que je vous ordonne aujourd'hui,
11:28	la malédiction, si vous n'obéissez pas aux commandements de YHWH, votre Elohîm, et si vous vous détournez du chemin que je vous ordonne aujourd'hui, pour aller après d'autres elohîm que vous ne connaissez pas.
11:29	Et il arrivera que quand YHWH, ton Elohîm, t'aura fait venir vers la terre dont tu vas prendre possession, tu mettras la bénédiction sur la montagne de Garizim, et la malédiction sur la montagne d'Ébal.
11:30	Ne sont-elles pas de l'autre côté du Yarden, derrière le chemin du soleil couchant, en terre des Kena'ânéens qui demeurent dans la région aride, vis-à-vis de Guilgal, à côté des grands arbres de Moré ?
11:31	Car vous allez passer le Yarden, pour entrer et prendre possession de la terre que YHWH, votre Elohîm, vous donne. Vous en prendrez possession et y habiterez.
11:32	Vous garderez et pratiquerez toutes les lois et les ordonnances que je mets aujourd'hui devant vous.

## Chapitre 12

### Torah sur les sacrifices offerts au lieu où résidera le Nom de YHWH

12:1	Voici les lois et les ordonnances que vous garderez et pratiquerez sur le sol que YHWH, l'Elohîm de vos pères, te donne pour que tu en prennes possession, tous les jours que vous vivrez sur ce sol.
12:2	Vous détruirez, vous détruirez tous les lieux où les nations que vous allez déposséder servent leurs elohîm, sur les hautes montagnes et sur les collines, et sous tout arbre verdoyant.
12:3	Vous renverserez aussi leurs autels, vous briserez leurs monuments, vous brûlerez au feu leurs asherah, vous mettrez en pièces les images gravées de leurs elohîm, et vous ferez périr leur nom de ce lieu-là.
12:4	Vous n'agirez pas ainsi à l'égard de YHWH, votre Elohîm.
12:5	Mais vous le chercherez seulement dans le lieu que YHWH votre Elohîm aura choisi parmi toutes vos tribus pour y mettre son Nom, pour y demeurer ; c'est là que tu viendras.
12:6	Vous y apporterez vos holocaustes, vos sacrifices, vos dîmes, vos offrandes prélevées de vos mains, vos vœux, vos offrandes volontaires et les premiers-nés de vos bœufs et de vos brebis<!--Lé. 17:3-4.-->.
12:7	Et là, vous mangerez devant YHWH, votre Elohîm, et vous vous réjouirez, vous et vos maisons, de toutes les choses auxquelles vous aurez mis la main, et dans lesquelles YHWH, votre Elohîm, vous aura bénis.
12:8	Vous ne ferez pas selon tout ce que nous faisons ici aujourd’hui, chaque homme ce qui est droit à ses propres yeux,
12:9	car, jusqu’à présent, vous n’êtes pas entrés dans le repos et dans l’héritage que YHWH, votre Elohîm, vous donne.
12:10	Vous passerez le Yarden, et vous habiterez sur la terre que YHWH, votre Elohîm, vous donne en héritage. Il vous donnera du repos de tous vos ennemis qui vous entourent, et vous y habiterez en sécurité.
12:11	Il y aura un lieu que YHWH, votre Elohîm, choisira pour y faire habiter son Nom. C'est là que apporterez tout ce que je vous ordonne, vos holocaustes, vos sacrifices, vos dîmes, vos offrandes prélevées de vos mains et toutes offrandes de choix pour les vœux que vous aurez voués à YHWH.
12:12	Vous vous réjouirez en face de YHWH, votre Elohîm, vous, vos fils et vos filles, vos serviteurs et vos servantes, et le Lévite qui sera dans vos portes, car il n'a ni part ni héritage avec vous.
12:13	Garde-toi de faire monter tes holocaustes dans tous les lieux que tu verras.
12:14	Mais tu feras monter tes holocaustes dans le lieu que YHWH choisira dans l'une de tes tribus, et tu y feras tout ce que je t'ordonne.
12:15	Seulement, toutes les fois que tu en auras le désir en ton âme, tue, et mange de la chair dans toutes tes portes, selon la bénédiction que t'accordera YHWH, ton Elohîm. Celui qui sera impur et celui qui sera pur en mangeront, comme de la gazelle et du cerf.
12:16	Seulement, vous ne mangerez pas de sang. Tu le répandras sur la terre, comme de l'eau.
12:17	Tu ne pourras pas manger dans tes portes la dîme de ton blé, de ton vin nouveau, de ton huile, ni les premiers-nés de tes bœufs et de tes brebis, ni aucune de tes offrandes en accomplissement d'un vœu, ni tes offrandes volontaires, ni les offrandes prélevées de tes mains.
12:18	Mais tu les mangeras en face de YHWH, ton Elohîm, au lieu que YHWH, ton Elohîm, choisira, toi, ton fils, ta fille, ton serviteur, ta servante, et le Lévite qui sera dans tes portes, et tu te réjouiras en face de YHWH, ton Elohîm, de tout ce à quoi tu auras mis la main.
12:19	Garde-toi d’abandonner le Lévite tous les jours, sur ton sol.
12:20	Quand YHWH, ton Elohîm, aura élargi tes frontières, comme il te l'a promis, et que tu diras : Je mangerai de la chair ! Quand ton âme désirera manger de la chair, tu mangeras de la chair selon tous les désirs de ton âme.
12:21	Si le lieu que YHWH, ton Elohîm, aura choisi pour y mettre son Nom est loin de chez toi, tu sacrifieras des bœufs et des brebis que YHWH t’aura donnés, comme je te l'ai ordonné, et tu en mangeras dans tes portes autant que ton âme le désireras.
12:22	Seulement, comme on mange de la gazelle et du cerf, ainsi tu en mangeras. Celui qui est impur et celui qui est pur en mangeront également.
12:23	Seulement, tiens ferme à ne pas manger le sang, car le sang c'est l'âme. Tu ne mangeras pas l'âme avec la chair<!--Lé. 7:26, 17:11-14.-->.
12:24	Tu n'en mangeras pas : tu le répandras sur la terre comme de l'eau.
12:25	Tu n'en mangeras pas, afin que tu sois heureux, toi et tes fils après toi, parce que tu auras fait ce qui est droit aux yeux de YHWH.
12:26	C’est seulement tes choses saintes, qui seront à toi, ainsi que tes vœux que tu porteras en venant au lieu que YHWH choisira.
12:27	Tu feras tes holocaustes, la chair et le sang, sur l'autel de YHWH, ton Elohîm. Mais le sang de tes sacrifices sera versé sur l'autel de YHWH, ton Elohîm, et tu en mangeras la chair.
12:28	Garde et écoute toutes ces paroles que je t'ordonne, afin que tu sois heureux, toi et tes fils après toi, à jamais, en faisant ce qui est bon et droit aux yeux de YHWH, ton Elohîm.

### Mise en garde contre la séduction et les elohîm étrangers

12:29	Quand YHWH, ton Elohîm, aura retranché les nations où tu viens, pour les déposséder en face de toi, quand tu les auras dépossédées et que tu habiteras sur leur terre,
12:30	garde-toi d’être piégé après elles, après qu’elles auront été détruites en face de toi, que tu ne consultes leurs elohîm, en disant : Comment ces nations servaient-elles leurs elohîm ? Ainsi ferai-je, moi aussi.
12:31	Tu n'agiras pas ainsi à l'égard de YHWH, ton Elohîm. Oui, toutes les abominations que YHWH hait, elles les ont faites pour leurs elohîm. Oui, leurs fils et leurs filles aussi, elles les brûlèrent au feu pour leurs elohîm.

## Chapitre 13

13:1	Vous observerez et vous mettrez en pratique tout ce que je vous commande. Vous n'y ajouterez rien, et vous n'en retrancherez rien<!--Ap. 22:19.-->.

### Éprouver les faux prophètes, ôter le méchant du milieu de l'assemblée

13:2	S'il s'élève au milieu de toi un prophète ou un rêveur de rêve, qui te donne un signe ou un miracle,
13:3	et que ce signe ou ce miracle dont il t'a parlé, arrive, s'il te dit : Allons après d'autres elohîm que tu ne connais pas, et servons-les !
13:4	Tu n'écouteras pas les paroles de ce prophète ni de ce rêveur de rêve, car YHWH, votre Elohîm, vous met à l'épreuve pour savoir si vous aimez YHWH, votre Elohîm, de tout votre cœur et de toute votre âme.
13:5	Vous marcherez après YHWH, votre Elohîm, vous le craindrez, vous garderez ses commandements, vous obéirez à sa voix, vous le servirez et vous vous attacherez à lui.
13:6	Ce prophète ou ce rêveur de rêve sera mis à mort, parce qu'il a parlé d'apostasie<!--Le mot « apostasie » utilisé ici, traduit le terme hébreu « carah » et signifie « défection ». L'apostasie est une déviation progressive. C'est l'abandon d'une vérité reçue. Paulos (Paul), l'apôtre, enseigne que deux événements doivent avoir lieu avant le retour du Seigneur sur la terre : l'apostasie et la révélation de l'homme du péché, le fils de perdition, c'est-à-dire l'Anti-Mashiah (2 Th. 2:1-3 ; 2 Ti. 4:1).--> contre YHWH, votre Elohîm, qui vous a fait sortir de la terre d'Égypte et vous a délivrés de la maison des esclaves, pour vous bannir de la voie dans laquelle YHWH, votre Elohîm, vous a ordonné de marcher. Tu ôteras le méchant du milieu de toi.
13:7	Si ton frère, fils de ta mère, ou ton fils, ou ta fille, ou la femme de ton sein, ou ton ami qui est comme ton âme t’incite secrètement en disant : Allons et servons d'autres elohîm ! – des elohîm que ni toi ni tes pères n'avez connus,
13:8	d'entre les elohîm des peuples qui sont autour de vous, près ou loin de toi, d'une extrémité de la Terre à l'autre extrémité de la Terre,
13:9	tu n'accepteras pas, tu ne l'écouteras pas. Ton œil ne le regardera pas avec pitié, tu ne l'épargneras pas, et tu ne le cacheras pas.
13:10	Mais tu le feras mourir, tu le feras mourir<!--Répétition du mot « mourir », voir commentaire en Ge. 2:17.-->. Ta main viendra sur lui la première pour le mettre à mort, et ensuite la main de tout le peuple.
13:11	Tu le lapideras avec des pierres et il mourra, parce qu'il a cherché à te bannir loin de YHWH, ton Elohîm, qui t'a fait sortir de la terre d'Égypte, de la maison des esclaves.
13:12	Tout Israël l’entendra et craindra, et l’on ne recommencera pas à faire une chose aussi mauvaise au milieu de toi.

### Jugement des villes idolâtres

13:13	Si tu entends dire dans l'une des villes que YHWH, ton Elohîm, t'a données pour y habiter :
13:14	des hommes, fils de Bélial<!--L'expression caractérise ceux qui s'adonnent au mal. Elle signifie « indigne », « bon à rien », « sans profit », « méchant ». Les écrits pseudépigraphiques et ceux de la mer morte associent Bélial à Satan. Voir 2 Co. 6:15.-->, sont sortis du milieu de toi, et ont banni les habitants de leur ville, en disant : Allons et servons d'autres elohîm ! – des elohîm que tu ne connais pas,
13:15	tu feras des recherches, tu examineras, tu interrogeras soigneusement. Et voici, c'est la vérité, la chose est établie, cette abomination a été faite au milieu de toi.
13:16	Tu frapperas, tu frapperas<!--Répétition du mot « frapperas ». Dans les écrits hébraïques, la répétition de mots est utilisée afin d'accentuer une action, pour appuyer un fait précis et le renforcer.--> à bouche d’épée les habitants de cette ville, tu la dévoueras par interdit et tout ce qui est en elle, et ses bêtes, à bouche d’épée.
13:17	Tu rassembleras tout son butin au milieu de la place, tu brûleras entièrement au feu cette ville et tout son butin, devant YHWH, ton Elohîm : elle sera pour toujours un monceau de ruines, sans être jamais rebâtie.
13:18	Rien de ce qui sera voué à une entière destruction ne s'attachera à ta main, afin que YHWH revienne de l'ardeur de sa colère, qu'il te fasse miséricorde et grâce et qu'il te multiplie, comme il a juré à tes pères,
13:19	si tu obéis à la voix de YHWH, ton Elohîm, en gardant tous ses commandements que je t'ordonne aujourd'hui, et en faisant ce qui est droit aux yeux de YHWH, ton Elohîm.

## Chapitre 14

### Israël, peuple mis à part

14:1	Vous êtes les fils de YHWH, votre Elohîm. Vous ne vous ferez aucune incision, et vous ne vous ferez pas de place chauve entre les yeux pour aucun mort.
14:2	Car tu es un peuple saint pour YHWH, ton Elohîm. YHWH t'a choisi pour que tu deviennes son peuple, sa propriété parmi tous les peuples qui sont sur les faces du sol.

### Torah sur l'alimentation

14:3	Tu ne mangeras aucune chose abominable.
14:4	Voici les bêtes que vous mangerez : le bœuf, le petit de la brebis et le petit de la chèvre ;
14:5	le cerf, la gazelle et le daim ; le bouquetin, le chevreuil, l'antilope, et le mouflon.
14:6	Toute bête au sabot divisé, au sabot déchiré et fendu, et qui rumine parmi les bêtes, vous la mangerez.
14:7	Mais vous ne mangerez pas de ceux qui ruminent, ou qui ont le sabot divisé et déchiré, comme le chameau, le lièvre et le daman<!--Voir Ps. 104:18.-->, car ils ruminent bien, mais ils n'ont pas le sabot qui est fendu : ils seront impurs pour vous.
14:8	Le porc aussi, car il a le sabot fendu, mais il ne rumine pas : il sera impur pour vous. Vous ne mangerez pas de leur chair, et vous ne toucherez pas à leur cadavre.
14:9	Voici ce que vous mangerez de tout ce qui est dans les eaux : vous mangerez de tout ce qui a des nageoires et des écailles.
14:10	Mais vous ne mangerez pas de ce qui n'a ni nageoires ni écailles : c’est impur pour vous.
14:11	Vous mangerez tout oiseau pur.
14:12	Mais voici ceux dont vous ne mangerez pas : l'aigle, l'orfraie, la buse,
14:13	le milan, le faucon, et le vautour, selon leur espèce,
14:14	tous les corbeaux selon leurs espèces,
14:15	la fille de l'autruche, l’autruche mâle, la mouette, l'épervier, selon son espèce,
14:16	le chat-huant, la chouette et le cygne,
14:17	le pélican, le vautour à charogne, le cormoran,
14:18	la cigogne, le héron, selon leur espèce, la huppe et la chauve-souris.
14:19	Et toute chose grouillante ailée sera impure pour vous. On n'en mangera pas.
14:20	Vous mangerez toute volaille pure.
14:21	Vous ne mangerez aucun cadavre. Tu le donneras à l'étranger qui sera dans tes portes, et il le mangera, ou tu le vendras à un étranger, car tu es un peuple saint pour YHWH, ton Elohîm. Tu ne feras pas cuire le chevreau dans le lait de sa mère.

### Torah sur les dîmes<!--No. 18:21-32.-->

14:22	Tu prendras la dîme<!--Il est question ici de la dîme que les Hébreux consommaient chaque année.-->, tu prendras la dîme<!--Voir commentaire en Ge. 2:16.--> de tout ce que ta semence produira, de ce qui sortira de ton champ d'année en année.
14:23	Tu mangeras devant YHWH, ton Elohîm, au lieu qu'il choisira pour y faire habiter son Nom, la dîme de ton blé, de ton vin nouveau et de ton huile, et les premiers-nés de ton gros et petit bétail, afin que tu apprennes à craindre toujours YHWH, ton Elohîm.
14:24	Quand le chemin sera trop long pour toi, quand tu ne pourras pas les transporter, quand le lieu que YHWH choisira pour y mettre son nom sera loin de toi, quand YHWH ton Elohîm te bénira,
14:25	tu les donneras pour de l’argent, tu serreras cet argent dans ta main, et tu iras au lieu que YHWH, ton Elohîm, aura choisi.
14:26	Là, tu achèteras avec cet argent tout ce que ton âme désirera, des bœufs, des brebis, du vin et des liqueurs fortes, tout ce que ton âme demandera, tu le mangeras devant YHWH, ton Elohîm, et tu te réjouiras toi et ta maison.
14:27	Tu n'abandonneras pas le Lévite qui est dans tes portes, parce qu'il n'a ni portion ni héritage avec toi<!--Ce verset fait référence à la première dîme qui devait être donnée aux Lévites (Voir commentaire en No. 18:21 et Mal. 3:10).-->.
14:28	Au bout de 3 ans, tu sortiras toute la dîme de tes produits de cette année-là, et tu la déposeras dans tes portes.
14:29	Le Lévite viendra, car il n'a ni portion ni héritage avec toi, ainsi que l'étranger, l'orphelin et la veuve qui sont dans tes portes, ils mangeront et se rassasieront, afin que YHWH, ton Elohîm, te bénisse dans toute l'œuvre que tu feras de tes mains.

## Chapitre 15

### Torah sur l'année de la remise : la justice et la bonté de YHWH

15:1	Tous les 7 ans, tu feras la remise<!--Il est question de la remise des dettes. Voir Ex. 21:2 ; Jé. 34:14.-->.
15:2	Voici la parole de cette remise : tout possesseur d’une créance relâchera sa main du prêt qu’il aura fait à son prochain. Il ne pressera pas son prochain ou son frère quand on aura proclamé la remise pour YHWH.
15:3	Tu presseras l'étranger, mais ce qui est entre toi et ton frère, ta main le relâchera,
15:4	afin qu'il n'y ait pas d'indigent chez toi, car YHWH te bénira, il te bénira<!--Voir commentaire en Ge. 2:16.--> sur la terre que YHWH, ton Elohîm, te donne en héritage afin que tu en prennes possession,
15:5	seulement si tu écoutes, si tu écoutes la voix de YHWH, ton Elohîm, en prenant garde de pratiquer tous ces commandements que je t'ordonne aujourd'hui.
15:6	Parce que YHWH, ton Elohîm, te bénira comme il te l'a promis, tu prêteras sur gage à beaucoup de nations, et tu n'emprunteras pas sur gage. Tu domineras sur beaucoup de nations, et elles ne domineront pas sur toi.
15:7	Quand un de tes frères deviendra indigent au milieu de toi, dans l'une de tes portes, sur la terre que YHWH, ton Elohîm, te donne, tu n'endurciras pas ton cœur, et tu ne fermeras pas ta main à ton frère indigent.
15:8	Mais tu ouvriras, tu ouvriras ta main pour lui, et tu lui prêteras sur gage, tu lui prêteras sur gage autant qu'il en aura besoin pour son indigence, dans laquelle il se trouvera.
15:9	Prends garde à toi, de peur que tu n'aies dans ton cœur quelque chose de Bélial, et que tu ne dises : La septième année, l'année de la remise approche ! Et que ton œil soit méchant envers ton frère indigent, afin de ne rien lui donner et qu'il ne crie à YHWH contre toi, et qu'il n'y ait du péché en toi.
15:10	Tu lui donneras, tu lui donneras et que ton cœur lui donne sans regret, car à cause de cela, YHWH, ton Elohîm, te bénira dans toutes tes œuvres, et dans tout ce à quoi tu mettras tes mains.
15:11	Car il y aura toujours des indigents sur la terre. C'est pourquoi je t'ordonne, et je te dis : Tu ouvriras, tu ouvriras ta main à ton frère, à l'affligé, et à l'indigent en ta terre.

### Torah sur les esclaves

15:12	Quand l'un de tes frères hébreux, homme ou femme, te sera vendu, il te servira 6 ans mais la septième année, tu le renverras libre de chez toi.
15:13	Quand tu le renverras libre de chez toi, tu ne le renverras pas à vide.
15:14	Tu le couvriras de présents, tu le couvriras de présents pris sur ton petit bétail, sur ton aire, sur ton pressoir, et tu lui donneras des biens dont YHWH, ton Elohîm, t'aura béni.
15:15	Et tu te souviendras que tu as été esclave en terre d'Égypte et que YHWH, ton Elohîm, t'en a racheté. Voilà pourquoi je t'ordonne cette chose-là aujourd'hui.
15:16	S'il arrive qu'il te dise : Je ne sortirai pas de chez toi, parce qu'il t'aime, toi et ta maison, et qu'il se trouve bien chez toi,
15:17	tu prendras un poinçon<!--Ex. 21:6.--> et tu lui perceras l'oreille contre la porte, et il sera ton serviteur pour toujours. Tu en feras de même à ta servante.
15:18	Ce ne sera pas dur à tes yeux de le renvoyer libre de chez toi, car du double salaire d’un salarié il t’aura servi six ans. YHWH, ton Elohîm, te bénira dans tout ce que tu feras.

### Torah sur les premiers-nés des animaux

15:19	Tout premier-né mâle qui naîtra parmi tes bœufs et tes brebis tu le consacreras à YHWH, ton Elohîm. Tu ne travailleras pas avec le premier-né de ton bœuf, et tu ne tondras pas le premier-né de tes brebis<!--Ex. 13:2.-->.
15:20	Tu le mangeras, toi et ta maison, d'année en année en face de YHWH, ton Elohîm, à l'endroit que YHWH aura choisi.
15:21	S'il a un défaut, s'il est boiteux ou aveugle, ou s'il a quelque autre mauvais défaut, tu ne le sacrifieras pas à YHWH, ton Elohîm.
15:22	Mais tu le mangeras dans tes portes, l'impur et le pur ensemble, comme la gazelle et du cerf.
15:23	Seulement, tu n'en mangeras pas le sang. Tu le répandras sur la terre comme de l'eau.

## Chapitre 16

### La Pâque et la fête des pains sans levain

16:1	Observe le mois d’Abib<!--Nisan (ou épis) = Mars-Avril. Voir Ex. 13:4.--> ! Tu y feras la Pâque pour YHWH, ton Elohîm. Car c'est au mois d’Abib que YHWH, ton Elohîm, t'a fait sortir, de nuit, d'Égypte<!--Ex. 12:2-29.-->.
16:2	Tu sacrifieras la Pâque pour YHWH, ton Elohîm, des brebis et des bœufs, au lieu que YHWH choisira pour y faire habiter son Nom.
16:3	Tu ne mangeras pas de pain levé, mais tu mangeras pendant 7 jours des pains sans levain, du pain d'affliction, parce que tu es sorti précipitamment de la terre d'Égypte, afin que tous les jours de ta vie tu te souviennes du jour où tu es sorti de la terre d'Égypte.
16:4	On ne verra pas de levain chez toi, sur tout le territoire de ta terre pendant 7 jours<!--1 Co. 5:7.--> ; et de la chair que tu sacrifieras le soir du premier jour, rien ne passera la nuit jusqu’au matin.
16:5	Tu ne pourras pas sacrifier la Pâque dans l'une de tes portes que YHWH, ton Elohîm, te donne.
16:6	Mais c'est au lieu que YHWH, ton Elohîm, choisira pour y faire habiter son Nom, que tu sacrifieras la Pâque, le soir, au coucher du soleil, au temps fixé où tu es sorti d'Égypte.
16:7	Tu la cuiras et tu la mangeras dans le lieu que YHWH, ton Elohîm, aura choisi. Et le matin, tu t'en retourneras et tu t'en iras dans tes tentes.
16:8	Pendant 6 jours, tu mangeras des pains sans levain, et le septième jour, il y aura une assemblée solennelle à YHWH, ton Elohîm : tu ne feras aucun travail.

### La fête des semaines

16:9	Tu te compteras 7 semaines. Tu commenceras à compter ces 7 semaines dès que la faucille sera mise dans les blés.
16:10	Tu feras la fête<!--Le mot hébreu signifie « festival », « festin », « rassemblement pour un festival », « pèlerin ». De. 16:13, 14 et 16 ; 31:10.--> des semaines à YHWH, ton Elohîm, en présentant l'offrande volontaire de ta main, que tu donneras, selon que YHWH, ton Elohîm, t'aura béni.
16:11	Tu te réjouiras en face de YHWH, ton Elohîm, toi, ton fils et ta fille, ton serviteur et ta servante, le Lévite qui sera dans tes portes, l'étranger, l'orphelin et la veuve qui seront au milieu de toi, dans le lieu que YHWH, ton Elohîm, aura choisi pour y faire habiter son Nom.
16:12	Tu te souviendras que tu as été esclave en Égypte, tu garderas et pratiqueras ces lois.

### La fête des cabanes (ou des tabernacles)

16:13	Tu feras la fête des cabanes pendant 7 jours, après que tu auras recueilli le produit de ton aire et de ton pressoir.
16:14	Et tu te réjouiras à cette fête, toi, ton fils et ta fille, ton serviteur et ta servante, le Lévite, l'étranger, l'orphelin, et la veuve qui seront dans tes portes.
16:15	Tu célébreras la fête pendant 7 jours à YHWH, ton Elohîm, dans le lieu que YHWH aura choisi, car YHWH, ton Elohîm, te bénira dans toute ta récolte, et dans tout le travail de tes mains, et tu seras dans la joie.

### Offrandes à YHWH selon ses moyens

16:16	Trois fois l'année, tout mâle se présentera en face de YHWH, ton Elohîm, dans le lieu qu'il aura choisi, à la fête des pains sans levain, à la fête des semaines, et à la fête des cabanes. On ne se présentera pas en face de YHWH à vide.
16:17	Chaque homme fera de sa main un don en proportion des bénédictions que YHWH, ton Elohîm, lui aura accordées.

### Des juges établis pour faire respecter la justice de YHWH

16:18	Tu établiras pour toi des juges et des commissaires dans toutes les portes que YHWH, ton Elohîm, te donne, selon tes tribus, et ils jugeront le peuple en rendant de justes jugements.
16:19	Tu ne pervertiras pas le jugement, tu ne reconnaîtra pas les faces et tu ne recevras pas de pot-de-vin, car les pots-de-vin aveuglent les yeux des sages et tordent les paroles des justes.
16:20	Tu suivras la justice, la justice afin que tu vives et que tu prennes possession de la terre que YHWH, ton Elohîm, te donne.

### Prescriptions sur les cultes

16:21	Tu ne planteras pour toi aucun arbre d'Asherah<!--Bois ou arbre d'Asherah : il est question d'un objet en bois, pieu sacré ou arbre utilisé dans le culte d'Astarté, l'épouse de Baal (Ex. 34:13 ; De. 7:5, 12:3 ; Jg. 3:7, 6:25-30 ; 1 R. 14:15,23).-->, près de l'autel de YHWH, ton Elohîm, que tu feras pour toi.
16:22	Tu ne dresseras pas non plus pour toi de monument. C’est quelque chose que YHWH, ton Elohîm, hait.

## Chapitre 17

17:1	Tu ne sacrifieras pas à YHWH ton Elohîm un bœuf ou un agneau ayant un défaut ou quelque chose de mauvais, car c'est une abomination pour YHWH, ton Elohîm.

### Punition de l'idolâtrie

17:2	S'il se trouve au milieu de toi dans l'une des portes que YHWH, ton Elohîm, te donne, un homme ou une femme faisant ce qui est mal aux yeux de YHWH, ton Elohîm, en transgressant son alliance,
17:3	et allant servir d'autres elohîm et se prosterner devant eux, devant le soleil, devant la lune, ou devant toute l'armée des cieux, ce que je n'ai pas ordonné,
17:4	quand tu en seras informé et que tu l'auras entendu, tu feras des recherches avec soin, et voici, c'est la vérité, le fait est établi, cette abomination a été commise en Israël,
17:5	tu feras sortir vers tes portes cet homme ou cette femme qui aura fait cette mauvaise action, l'homme ou la femme, tu les lapideras avec des pierres et ils mourront.
17:6	C'est sur la bouche de deux témoins, ou de trois témoins<!--Mt. 18:15-17.--> que celui qui doit mourir sera mis à mort. Il ne sera pas mis à mort sur la bouche d'un seul témoin.
17:7	La main des témoins sera la première sur lui pour le faire mourir, et ensuite la main de tout le peuple. Et ainsi tu ôteras le mal du milieu de toi.

### Soumission aux autorités

17:8	Quand une affaire te paraîtra trop difficile à juger entre sang et sang, entre cause et cause, entre plaie et plaie, qui sont des affaires de procès dans tes portes, alors tu te lèveras et tu monteras au lieu que YHWH, ton Elohîm, aura choisi.
17:9	Et tu iras vers les prêtres, les Lévites et vers le juge qu'il y aura en ce temps-là. Tu les consulteras et ils te feront connaître la parole du jugement.
17:10	Tu agiras selon la parole qu'ils t'auront déclarée de leur bouche dans le lieu que YHWH aura choisi, et tu prendras garde de faire tout ce qu'ils t'enseigneront.
17:11	Tu agiras selon la torah qu'ils t'auront enseignée de leur bouche et selon le jugement qu'ils t'auront prononcé. Tu ne te détourneras ni à droite ni à gauche de ce qu'ils t'auront déclaré.
17:12	Mais l'homme qui agira avec orgueil et n'obéira pas au prêtre qui se tient là pour servir YHWH, ton Elohîm, ou au juge, cet homme mourra. Tu ôteras le mal d'Israël,
17:13	et tout le peuple l'entendra et craindra, et n'agira plus avec un tel orgueil.

### Instructions sur la royauté

17:14	Lorsque tu seras entré sur la terre que YHWH, ton Elohîm, te donne, lorsque tu en prendras possession, que tu y habiteras et que tu diras : J'établirai un roi sur moi, comme toutes les nations qui sont autour de moi,
17:15	tu établiras, tu établiras sur toi le roi que YHWH, ton Elohîm, aura choisi, tu établiras un roi du milieu de tes frères, tu ne pourras pas désigner un homme étranger qui ne soit pas ton frère<!--Dans sa prescience, YHWH savait que le peuple se détournerait de ses voies et réclamerait un roi, à l'identique des nations d'alentour (1 S. 8). Or depuis leur sortie d'Égypte, seul YHWH était leur Elohîm et leur Roi.-->.
17:16	Seulement, qu'il n'ait pas un grand nombre de chevaux et qu'il ne ramène pas le peuple en Égypte pour augmenter le nombre de chevaux, car YHWH vous a dit : Vous ne retournerez plus par cette voie.
17:17	Qu'il n'ait pas non plus un grand nombre de femmes<!--Voir 1 R. 11:1.-->, afin que son cœur ne se détourne pas, et qu'il n'accumule pas beaucoup d'argent et d'or<!--1 R. 11:1-43.-->.
17:18	Il arrivera, quand il sera assis sur le trône de son royaume, qu'il écrira pour lui, une copie de cette torah dans un livre, en face des prêtres, les Lévites.
17:19	Elle sera avec lui, il la lira tous les jours de sa vie, afin qu’il apprenne à craindre YHWH, son Elohîm, pour garder toutes les paroles de cette torah et ces ordonnances, pour les pratiquer ;
17:20	afin que son cœur ne s'élève pas au-dessus de ses frères, et qu'il ne se détourne pas de ce commandement ni à droite ni à gauche afin qu'il prolonge ses jours dans son royaume, lui et ses fils, au milieu d'Israël.

## Chapitre 18

### Héritage des Lévites et des prêtres

18:1	Les prêtres, les Lévites, toute la tribu de Lévi, n'auront ni part ni héritage avec Israël. Ils mangeront les offrandes consumées par le feu de YHWH et de son héritage.
18:2	Ils n'auront pas d'héritage parmi leurs frères : YHWH sera leur héritage, comme il leur a dit.
18:3	Voici quel sera le droit des prêtres sur le peuple, sur ceux qui offriront un sacrifice, un bœuf ou un agneau : on donnera au prêtre l'épaule, les mâchoires et l'estomac.
18:4	Tu lui donneras la première partie de ton blé, de ton vin nouveau et de ton huile, ainsi que la première partie de la tonte de tes brebis.
18:5	Car YHWH, ton Elohîm, l'a choisi d'entre toutes les tribus, afin qu'il se tienne devant lui, et qu'il fasse le service au Nom de YHWH, lui et ses fils, pour toujours.
18:6	Quand un Lévite viendra de l'une de tes portes, de tout Israël où il séjourne, et qu'il viendra selon tout le désir de son âme, au lieu que YHWH aura choisi,
18:7	il servira au Nom de YHWH, son Elohîm, comme tous ses frères Lévites qui se tiennent là devant YHWH,
18:8	il mangera portion pour portion, outre ce qu'il aura vendu des biens de ses pères.

### Les abominations des nations interdites en Israël

18:9	Quand tu seras entré sur la terre que YHWH, ton Elohîm, te donne, tu n'apprendras pas à faire les abominations de ces nations-là.
18:10	Qu'on ne trouve au milieu de toi personne qui fasse passer par le feu son fils ou sa fille, personne qui pratique la divination, le spiritisme, la sorcellerie, qui dit la bonne aventure,
18:11	ni d'enchanteur qui fait des incantations, personne qui consulte ceux qui évoquent les morts et ceux qui ont un esprit de divination, personne qui consulte les morts<!--YHWH interdit tout contact avec le monde des esprits et des démons. Le croyant qui accepte l'Évangile comprendra sans peine et simplement en obéissant à la Parole que ce domaine est interdit. Voir Ex. 22:17 ; Lé. 19:26,31, 20:6,27 ; Es. 8:19 ; 2 Ch. 33:6 ; Ac. 19:13-20.-->.
18:12	Car quiconque fait ces choses est en abomination à YHWH et c'est à cause de ces abominations que YHWH, ton Elohîm, va déposséder ces nations-là devant toi.
18:13	Tu seras intègre avec YHWH, ton Elohîm.
18:14	Car ces nations que tu vas déposséder écoutent ceux qui pratiquent le spiritisme et la divination, mais à toi, YHWH, ton Elohîm, ne le permet pas.

### Annonce sur la venue du Mashiah

18:15	YHWH, ton Elohîm, te suscitera du milieu de toi, d'entre tes frères, un prophète comme moi<!--Moshé (Moïse) a annoncé la venue d'un prophète comme lui, c'est-à-dire un prophète de la délivrance et de l'exode. Ce prophète n'est autre que Yéhoshoua ha Mashiah (Jésus-Christ) qui nous délivre de l'emprise de Satan et nous sort du monde pour nous emmener dans la nouvelle Yeroushalaim (Jérusalem) (Jn. 14:2 ; Col. 1:13). Notons qu'au moment de la transfiguration, Éliyah (Élie) et Moshé parlaient avec Yéhoshoua (Jésus) de son départ (« exodus » en grec ; Lu. 9:31).--> : vous l'écouterez.
18:16	Selon tout ce que tu as demandé à YHWH, ton Elohîm, à Horeb, le jour de l'assemblée, quand tu disais : Que je n'entende plus la voix de YHWH, mon Elohîm, et que je ne voie plus ce grand feu, de peur de mourir<!--Ex. 20:19.-->.
18:17	YHWH me dit : Ce qu'ils ont dit est bien.
18:18	Je leur susciterai un prophète comme toi du milieu de leurs frères, je mettrai mes paroles dans sa bouche, et il leur dira tout ce que je lui ordonnerai.
18:19	Et il arrivera que si un homme n'écoute pas mes paroles qu'il dira en mon Nom, je lui en demanderai compte.

### Comment éprouver les prophètes ?

18:20	Mais le prophète qui agira de manière orgueilleuse pour dire en mon Nom une parole que je ne lui aurai pas ordonnée de dire, ou qui parlera au nom d'autres elohîm, ce prophète-là mourra.
18:21	Et si tu dis dans ton cœur : Comment connaîtrons-nous la parole que YHWH n'aura pas dite ?
18:22	Quand le prophète parlera au Nom de YHWH, et que ce qu'il aura dit n'arrivera pas, ce sera une parole que YHWH ne lui aura pas dite. C'est par orgueil que le prophète l'a dite : n'aie pas peur de lui.

## Chapitre 19

### Les villes de refuge<!--No. 35:1-34.-->

19:1	Lorsque YHWH, ton Elohîm, aura exterminé les nations dont YHWH ton Elohîm te donne la terre, lorsque tu les auras dépossédées et que tu habiteras dans leurs villes et leurs maisons,
19:2	tu sépareras 3 villes pour toi, au milieu de la terre que YHWH, ton Elohîm, te donne pour que tu en prennes possession.
19:3	Tu établiras pour toi des routes, et tu diviseras en 3 le territoire de ta terre, que YHWH, ton Elohîm, te donnera en héritage. Il en sera ainsi afin que tout meurtrier puisse s'y enfuir.
19:4	Voici le cas du meurtrier qui s'enfuira là, afin qu'il vive. Celui qui aura tué son prochain sans le savoir, ne le haïssant ni d’hier, ni d’avant hier.
19:5	Quiconque va couper du bois dans la forêt avec son prochain, brandissant la hache à la main pour couper du bois, si le fer s’échappe du manche, atteint son prochain, et s'il en meurt, il s'enfuira dans l'une de ces villes et y vivra.
19:6	De peur que le racheteur du sang ne poursuive le meurtrier, parce que son âme est échauffée, et qu'il ne le rattrape, si le chemin est trop long, et ne le frappe à mort, alors qu'il ne mérite pas la mort, parce qu'il ne le haïssait pas d'hier ni d'avant-hier<!--No. 35:1-34.-->.
19:7	C'est pourquoi je t'ordonne, en disant : Mets à part 3 villes.
19:8	Et si YHWH, ton Elohîm, élargit tes frontières comme il l'a juré à tes pères et te donne toute la terre qu'il a promise à tes pères de te donner,
19:9	parce que tu auras gardé et mis en pratique tous ces commandements que je t'ordonne aujourd'hui en aimant YHWH, ton Elohîm, et en marchant toujours dans ses voies, alors tu ajouteras encore 3 villes à ces trois-là,
19:10	afin que le sang innocent ne soit pas versé au milieu de la terre que YHWH, ton Elohîm, te donne en héritage et qu’il n’y ait pas de sang sur toi.
19:11	Mais s'il arrive qu'un homme haïssant son prochain lui dresse une embûche, se lève contre lui et frappe son âme à mort, et qu'il s'enfuie dans l'une de ces villes,
19:12	les anciens de sa ville y enverront quelqu'un pour le saisir, et le livreront entre les mains du racheteur du sang, afin qu'il meure.
19:13	Ton œil ne l'épargnera pas, mais tu feras disparaître d'Israël le sang innocent, et tu seras heureux.
19:14	Tu ne déplaceras pas les bornes de ton prochain, fixées par tes ancêtres, dans l'héritage que tu posséderas, sur la terre que YHWH, ton Elohîm, te donne pour que tu en prennes possession.

### Résoudre des différends

19:15	Un seul témoin ne se lèvera pas contre un homme, pour toute iniquité, pour tout péché, pour tout péché par lequel il a péché. Mais sur la bouche de 2 témoins ou sur la bouche de 3 témoins, la chose sera établie<!--Voir Mt. 18:15-16 ; 2 Co. 13:1.-->.
19:16	Quand un faux témoin se lèvera contre un homme pour l'accuser d'apostasie,
19:17	les deux hommes qui ont la controverse se tiendront en face de YHWH, en face des prêtres et des juges qui seront là en ce temps-là.
19:18	Les juges feront des recherches avec soin et voici que ce témoin est un faux témoin, ayant donné un faux témoignage contre son frère.
19:19	Vous le traiterez comme il avait l'intention de traiter son frère. Tu ôteras ainsi le mal du milieu de toi.
19:20	Et ceux qui restent entendront et craindront, et ils ne feront plus une chose aussi méchante au milieu de toi.
19:21	Ton œil ne l'épargnera pas : âme pour âme, œil pour œil, dent pour dent, main pour main, pied pour pied.

## Chapitre 20

### Instructions diverses pour la guerre

20:1	Quand tu partiras en guerre contre tes ennemis, et que tu verras des chevaux, des chars et un peuple plus grand que toi, tu ne les craindras pas, car YHWH, ton Elohîm, qui t'a fait monter de la terre d'Égypte, est avec toi.
20:2	Il arrivera que quand vous vous approcherez pour le combat, le prêtre s'avancera et parlera au peuple,
20:3	et leur dira : Écoute Israël ! Vous vous approchez aujourd'hui pour combattre vos ennemis. Que votre cœur ne faiblisse pas ! N'ayez pas peur, ne soyez pas effrayés et ne soyez pas terrifiés face à eux.
20:4	Car YHWH, votre Elohîm, marche avec vous, pour combattre vos ennemis, pour vous sauver.
20:5	Les commissaires parleront au peuple, en disant : Qui est l'homme qui a bâti une maison neuve et ne l'a pas inaugurée ? Qu'il s'en aille et retourne dans sa maison, de peur qu'il ne meure dans la bataille et qu'un autre homme ne l'inaugure.
20:6	Quel est l’homme qui a planté une vigne et n’en a pas joui<!--Litt. profaner, souiller, polluer, commencer.--> ? Qu'il s'en aille et retourne dans sa maison, de peur qu'il ne meure dans la bataille et qu'un autre homme n'en jouisse.
20:7	Quel est l’homme qui s’est fiancé à une femme et ne l’a pas prise ? Qu'il s'en aille et retourne dans sa maison, de peur qu'il ne meure dans la bataille et qu'un autre homme ne la prenne.
20:8	Les commissaires continueront à parler au peuple, et diront : Quel est l'homme qui a peur et dont le cœur faiblit, qu'il s'en aille et retourne dans sa maison, de peur que le cœur de ses frères ne se fonde comme son cœur.
20:9	Il arrivera que quand les commissaires auront fini de parler au peuple, ils désigneront les chefs des armées à la tête du peuple.
20:10	Quand tu t'approcheras d'une ville pour lui faire la guerre, tu l'inviteras à la paix.
20:11	S'il arrive qu’elle te fasse une réponse de paix et qu’elle s’ouvre à toi, alors tout le peuple qui s'y trouvera te sera tributaire et te servira. 
20:12	Si elle ne fait pas la paix avec toi, si elle te fait la guerre, tu l’assiégeras.
20:13	YHWH, ton Elohîm, la livrera entre tes mains, et tu frapperas tous les mâles à bouche d’épée.
20:14	Mais les femmes, les enfants, le bétail, tout ce qui sera dans la ville, et tout son butin, tu le prendras pour toi et tu mangeras le butin de tes ennemis, que YHWH, ton Elohîm, t'aura donné.
20:15	C'est ainsi que tu agiras à l'égard de toutes les villes qui sont très éloignées de toi, et qui ne sont pas des villes de ces nations.
20:16	Seulement, dans les villes de ces peuples que YHWH, ton Elohîm, te donne en héritage, ne laisse vivre aucun souffle.
20:17	Car tu les dévoueras par interdit, tu les dévoueras par interdit : les Héthiens, les Amoréens, les Kena'ânéens, les Phéréziens, les Héviens, et les Yebousiens, comme YHWH, ton Elohîm, te l'a ordonné,
20:18	afin qu'ils ne vous enseignent pas à faire toutes les abominations qu'ils font pour leurs elohîm, et que vous ne péchiez pas contre YHWH, votre Elohîm.
20:19	Quand tu assiégeras une ville durant plusieurs jours, en lui faisant la guerre pour la saisir, tu ne détruiras pas les arbres à coups de hache, tu t'en nourriras et tu ne les couperas pas, car l'arbre des champs est-il un humain pour venir en face de toi dans le siège ?
20:20	Tu détruiras et tu couperas seulement les arbres dont tu sauras qu'ils ne sont pas des arbres fruitiers, et tu en construiras des retranchements contre la ville qui te fait la guerre, jusqu'à ce qu'elle tombe.

## Chapitre 21

### Torah sur le meurtre anonyme

21:1	Quand on trouvera sur le sol, que YHWH ton Elohîm te donne pour en prendre possession, un homme blessé mortellement, étendu dans un champ, sans que l'on sache qui l'a frappé,
21:2	tes anciens et tes juges sortiront pour mesurer la distance jusqu’aux villes autour du blessé mortellement.
21:3	Il arrivera, que les anciens de la ville la plus proche du blessé mortellement prendront une génisse du troupeau qui n'a pas travaillé, qui n'a pas tiré au joug.
21:4	Les anciens de cette ville feront descendre cette génisse vers un torrent intarissable où on ne travaille ni ne sème, et là, ils briseront la nuque de la génisse dans le torrent.
21:5	Les prêtres, fils de Lévi, s'approcheront. En effet, YHWH, ton Elohîm, les a choisis pour qu'ils le servent et qu'ils bénissent au Nom de YHWH, et leur bouche décidera de toute contestation et de toute blessure.
21:6	Et tous les anciens de cette ville, qui seront les plus proches de celui qui aura été blessé mortellement, laveront leurs mains sur la génisse à laquelle on aura brisé la nuque dans le torrent.
21:7	Et prenant la parole, ils diront : Nos mains n'ont pas répandu ce sang et nos yeux ne l'ont pas vu.
21:8	Fais la propitiation, YHWH, pour Israël, ton peuple, que tu as racheté. Ne mets pas le sang innocent au milieu de ton peuple d'Israël. Ainsi sera faite pour eux la propitiation du sang.
21:9	Et tu ôteras le sang innocent du milieu de toi, en faisant ce qui est droit aux yeux de YHWH.

### Torah sur le mariage et l'héritage

21:10	Quand tu iras en guerre contre tes ennemis, si YHWH ton Elohîm les livre entre tes mains et que tu en emmènes des captifs,
21:11	si tu vois parmi les captifs une femme belle de figure et que tu désires la prendre pour femme,
21:12	tu la conduiras à l'intérieur de ta maison, et elle rasera sa tête et fera ses ongles,
21:13	elle ôtera les vêtements de sa captivité, elle demeurera dans ta maison, et pleurera son père et sa mère pendant un mois. Après cela, tu iras vers elle, tu l'épouseras et elle deviendra ta femme.
21:14	S'il arrive qu'elle ne te plaise plus, tu la renverras, au gré de son âme, mais tu ne la vendras pas, tu ne la vendras pas pour de l'argent ni la traiteras en esclave, parce que tu l'auras humiliée.
21:15	Si un homme qui a deux femmes aime l'une et hait l'autre, si celle qu'il aime et celle qu'il hait enfantent des fils, et que le fils premier-né est de celle qui est haïe,
21:16	il arrivera qu'au jour où il fera hériter à ses fils ce qui est à lui, il ne pourra pas donner le droit d’aînesse<!--Ou « faire premier-né ».--> au fils de celle qui est aimée, en face du fils de celle qui est haïe, lequel est le premier-né.
21:17	Mais il reconnaîtra pour premier-né le fils de celle qui est haïe, et il lui donnera la double bouche<!--Voir 2 R. 2:9.--> de tout ce qui se trouvera chez lui, car il est le premier de sa vigueur. Le droit d'aînesse lui appartient.

### Le fils désobéissant sous la torah<!--Cp. Lu. 15:11-23.-->

21:18	Si un homme a un fils désobéissant et rebelle, n'écoutant ni la voix de son père, ni la voix de sa mère, et qui, bien qu'ils l'aient châtié, ne les écoute pas,
21:19	le père et la mère le prendront et le mèneront aux anciens de sa ville, à la porte du lieu de sa demeure.
21:20	Et ils diront aux anciens de sa ville : Voici notre fils qui est désobéissant et rebelle, qui n'obéit pas à notre voix, et qui se livre à l'excès et à l'ivrognerie !
21:21	Et tous les hommes de la ville le lapideront avec des pierres, et il mourra. Tu brûleras le mal de ton sein, afin que tout Israël entende et craigne.
21:22	Si un homme a en lui un péché dont le jugement est la mort, il sera tué et pendu à un arbre.
21:23	Son cadavre ne passera pas la nuit sur l'arbre, mais tu l'enterreras, tu l'enterreras le même jour, car celui qui est pendu est une malédiction d'Elohîm<!--Ga. 3:13.-->, et tu ne souilleras pas le sol que YHWH, ton Elohîm, te donne en héritage.

## Chapitre 22

### Torah sur la vie en société

22:1	Si tu vois le bœuf ou la brebis de ton frère s'égarer, tu ne t'en cacheras pas, tu les ramèneras à ton frère, tu les lui ramèneras.
22:2	Si ton frère ne demeure pas près de toi, et que tu ne le connais pas, tu les recueilleras dans ta maison et ils seront chez toi jusqu'à ce que ton frère les cherche, et tu les lui rendras.
22:3	Tu feras de même pour son âne, tu feras de même pour son vêtement, et tu feras de même pour tout ce que ton frère aura perdu et que tu trouveras. Tu ne devras pas t'en détourner.
22:4	Tu ne te cacheras pas, si tu vois l'âne ou le bœuf de ton frère tomber en chemin : tu les relèveras, tu les relèveras avec lui.
22:5	Que l'équipement<!--Vient de l'hébreu « keliy » qui signifie « article », « vaisselle », « outil », « ustensile », « arme », « objet », « instrument », « vases ».--> d'un homme fort ne soit<!--« Être », « devenir », « exister », « arriver », « devenir comme », « institué », « établi ».--> pas sur une femme, et qu’un homme fort ne s'habille pas avec un vêtement de femme, car quiconque fait ces choses est en abomination à YHWH, ton Elohîm<!--Dans ce passage, YHWH condamne le travestisme. Cette pratique était répandue chez les Kena'ânéens. Le travestisme consiste à adopter le comportement, les habitudes sociales et la tenue vestimentaire du sexe opposé dans le but de lui ressembler.-->.
22:6	Si tu rencontres sur le chemin, sur un arbre ou sur la terre, un nid d'oiseaux, ayant des petits ou des œufs, et la mère couchée sur les petits ou les œufs, tu ne prendras pas la mère et les petits,
22:7	mais tu laisseras aller, tu laisseras aller la mère et tu ne prendras que les petits, afin que tu sois heureux et que tu prolonges tes jours.
22:8	Si tu bâtis une maison neuve, tu feras un parapet tout autour de ton toit, afin que tu ne mettes pas de sang sur ta maison, si quelqu'un tombait de là.

### Torah sur les mélanges

22:9	Tu ne sèmeras pas dans ta vigne diverses sortes de grains, de peur que tout le produit ne soit considéré comme consacré : la graine semée et le produit de la vigne.
22:10	Tu ne laboureras pas avec un âne et un bœuf attelés ensemble<!--2 Co. 6:14-16.-->.
22:11	Tu ne t'habilleras pas d'un tissu mélangé de laine et de lin ensemble.
22:12	Tu te feras des franges aux quatre pans du vêtement dont tu te couvriras.

### Torah sur la virginité, l'adultère et la fidélité

22:13	Quand un homme aura pris une femme, et qu'après être venu vers elle, il la haïsse,
22:14	et qu'il lui impute des choses concernant le libertinage et lui fait une mauvaise réputation, en disant : J'ai pris cette femme, et quand je me suis approché d'elle, je n'ai pas trouvé en elle sa virginité<!--Voir Ex. 22:15.-->.
22:15	Le père et la mère de la jeune femme prendront et produiront les signes de la virginité de la jeune femme devant les anciens de la ville, à la porte.
22:16	Et le père de la jeune femme dira aux anciens : J'ai donné ma fille à cet homme pour femme, et il l'a haïe.
22:17	Et voici qu'il lui impute des choses concernant le libertinage en disant : Je n'ai pas trouvé en ta fille de virginité. Cependant, voici les signes de la virginité de ma fille. Et ils étendront le drap devant les anciens de la ville.
22:18	Les anciens de la ville prendront le mari et le châtieront.
22:19	Ils le condamneront à une amende de 100 sicles d'argent, qu'ils donneront au père de la jeune femme, parce qu'il a répandu une mauvaise réputation sur une vierge d'Israël. Elle deviendra sa femme, et il ne pourra pas la répudier, tous ses jours.
22:20	Si cette parole était vraie, si l’on n’a pas trouvé la virginité de la jeune femme,
22:21	ils feront sortir la jeune femme à l'entrée de la maison de son père. Les hommes de sa ville la lapideront de pierres et elle mourra, car elle a commis une infamie en Israël, en se prostituant dans la maison de son père. Tu ôteras le mal du milieu de toi.
22:22	Si l'on trouve un homme couché avec une femme ayant un mari qui l'a épousée, ils mourront tous les deux, l'homme qui a couché avec la femme, et la femme aussi. Tu ôteras ainsi le mal d'Israël.
22:23	Si une jeune fille vierge est fiancée à un homme, et qu'un homme la rencontre dans la ville, et couche avec elle,
22:24	vous les conduirez tous les deux à la porte de la ville. Vous les lapiderez de pierres et ils mourront, la jeune fille, parce qu'elle n'a pas crié étant dans la ville, et l'homme parce qu'il a humilié la femme de son prochain. Tu ôteras le mal du milieu de toi.
22:25	Si cet homme trouve dans les champs une jeune fille fiancée, et que cet homme la force et couche avec elle, alors cet homme qui aura couché avec elle mourra lui seul.
22:26	Mais tu ne feras rien à la jeune fille. La jeune fille n'a pas commis de péché digne de mort, ce cas est comme si un homme s'élevait contre son prochain et assassinait son âme.
22:27	Parce que l'ayant trouvée dans les champs, la jeune fille fiancée a pu crier sans qu'il y ait eu personne pour la délivrer.
22:28	Si un homme trouve une jeune fille vierge non fiancée<!--Voir Ex. 22:15.-->, qu'il la saisisse et couche avec elle, et qu'on les trouve,
22:29	l'homme qui aura couché avec elle donnera au père de la jeune fille 50 sicles d'argent, et elle deviendra sa femme. Et, parce qu'il l'a humiliée, il ne pourra pas la répudier, tous ses jours.

## Chapitre 23

23:1	Un homme ne prendra pas la femme de son père ni ne découvrira le pan de la robe de son père.

### Torah sur l'accès à l'assemblée de YHWH

23:2	Celui qui est mutilé par écrasement des testicules ou ayant le pénis coupé n'entrera pas dans l'assemblée de YHWH.
23:3	Le bâtard<!--Le mot bâtard, « mamzer » en hébreu, désigne l'enfant illégitime, celui issu de l'inceste, celui né d'une population mélangée ou d'un père juif et d'une mère païenne, et inversement.--> n'entrera pas dans l'assemblée de YHWH. Même sa dixième génération n'entrera pas dans l'assemblée de YHWH.
23:4	L'Ammonite et le Moabite n'entreront pas dans l'assemblée de YHWH ; même leur dixième génération n’entrera pas dans l’assemblée de YHWH, à jamais,
23:5	parce qu'ils ne sont pas venus à votre rencontre avec du pain et de l'eau, sur le chemin, lorsque vous sortiez d'Égypte, et parce qu'ils ont engagé contre vous Balaam, fils de Beor, de Pethor en Mésopotamie, pour qu'il vous maudisse.
23:6	Mais YHWH, ton Elohîm, n'a pas voulu écouter Balaam. YHWH, ton Elohîm, a changé la malédiction en bénédiction, parce que YHWH, ton Elohîm, t'aime.
23:7	Tu ne chercheras ni leur paix ni leur prospérité, durant tous tes jours, pour toujours.
23:8	Tu n'auras pas en abomination l'Édomite, car il est ton frère, tu n'auras pas en abomination l'Égyptien, car tu as été étranger en sa terre :
23:9	les fils qui leur naîtront à la troisième génération entreront dans l'assemblée de YHWH.

### La sainteté et la justice dans le camp de YHWH

23:10	Quand le camp sortira contre tes ennemis, garde-toi de toute chose mauvaise.
23:11	S’il y a parmi vous un homme qui n’est pas pur, à la suite d’une pollution arrivée la nuit, il sortira hors du camp, il n'entrera pas dans le camp.
23:12	Il arrivera que sur le soir, il se lavera dans l'eau, et dès que le soleil sera couché, il rentrera dans le camp.
23:13	Il y aura pour toi un espace hors du camp où tu sortiras, dehors.
23:14	Tu auras un pieu parmi tes bagages. Et il arrivera, quand tu t'assiéras dehors, que tu creuseras avec, tu te retourneras et tu couvriras tes excréments.
23:15	Car YHWH, ton Elohîm, marche au milieu de ton camp pour te délivrer et pour livrer tes ennemis devant toi. Que tout ton camp soit saint, afin qu'il ne voie chez toi aucune chose honteuse et qu'il ne se détourne pas de toi.
23:16	Tu ne livreras pas à son maître l'esclave qui se sera sauvé chez toi d'auprès de son maître.
23:17	Il demeurera avec toi, au milieu de toi, dans le lieu qu'il choisira, dans l'une de tes portes, là où bon lui semblera : tu ne l'opprimeras pas.
23:18	Il n'y aura aucune prostituée parmi les filles d'Israël, il n'y aura aucun homme prostitué qui se prostitue parmi les fils d'Israël.
23:19	Tu n'apporteras pas dans la maison de YHWH, ton Elohîm, le salaire d'une prostituée, ni le prix d'un chien, pour quelque vœu que ce soit, car tous les deux sont en abomination devant YHWH, ton Elohîm.
23:20	Tu n'exigeras aucun intérêt à ton frère, ni intérêt pour de l'argent, ni intérêt pour des vivres, ni intérêt pour quelque chose que ce soit que l'on prête avec intérêt.
23:21	Tu prêteras avec intérêt à l'étranger, mais tu ne prêteras pas avec intérêt à ton frère, afin que YHWH, ton Elohîm, te bénisse dans tout ce que ta main entreprendra sur la terre où tu entres pour en prendre possession.

### Vœux faits à YHWH

23:22	Si tu fais un vœu à YHWH, ton Elohîm, tu ne tarderas pas à l'accomplir, car YHWH, ton Elohîm, te le réclamerait, te le réclamerait, cela deviendrait chez toi un péché.
23:23	Mais si tu t'abstiens de faire un vœu, cela ne deviendra pas chez toi un péché.
23:24	Mais tu prendras garde de faire ce qui sortira de tes lèvres, l'offrande volontaire que tu auras vouée à YHWH, ton Elohîm, et que ta bouche aura prononcée.

### Diverses torahs

23:25	Si tu entres dans la vigne de ton prochain, tu pourras manger des raisins pour rassasier ton âme, mais tu n'en mettras pas dans ton vase.
23:26	Si tu entres dans les blés de ton prochain, tu pourras arracher des épis avec ta main, mais tu n'agiteras pas la faucille sur les blés de ton prochain.

## Chapitre 24

### Torah sur le divorce

24:1	Quand un homme aura pris et épousé une femme, s'il arrive qu'elle ne trouve pas grâce à ses yeux, parce qu'il aura trouvé en elle quelque chose de honteux, il lui écrira une lettre de divorce, et après la lui avoir remise en main, il la renverra de sa maison.
24:2	Elle sortira de sa maison, s'en ira, et elle pourra devenir la femme d'un autre homme.
24:3	Si ce dernier homme la hait, écrit une lettre de divorce, la lui donne dans sa main, et la renvoie de sa maison, ou que ce dernier homme qui l'a prise pour femme, meure,
24:4	son premier mari qui l'avait renvoyée ne pourra pas retourner la prendre pour femme après qu'elle s'est ainsi rendue impure, car c'est une abomination devant YHWH, et tu ne feras pas pécher la terre que YHWH, ton Elohîm, te donne en héritage.

### Diverses torahs sur l'organisation de la société

24:5	Quand un homme aura nouvellement pris une femme, il n'ira pas à la guerre et il ne sera chargé d’aucune affaire. Il en sera exempté pour sa maison pendant un an et il réjouira la femme qu'il a prise.
24:6	On ne prendra pas pour gage les deux meules, pas même la meule de dessus, parce qu'on prendrait pour gage l'âme.
24:7	Si l'on trouve un homme qui ait enlevé une âme d’entre ses frères, d’entre les fils d’Israël, et qui l’ait traité en esclave et l’ait vendu, ce voleur mourra. Tu ôteras le mal du milieu de toi.
24:8	Prends garde à la plaie de la lèpre, afin de bien observer et de faire tout ce que les prêtres, les Lévites, vous enseigneront. Vous prendrez garde de faire selon ce que je leur ai ordonné.
24:9	Souviens-toi de ce que YHWH, ton Elohîm, fit à Myriam, en chemin, après votre sortie d'Égypte.

### Torah en faveur des nécessiteux

24:10	Lorsque tu feras à ton prochain un prêt quelconque, tu n'entreras pas dans sa maison pour prendre son gage.
24:11	Tu te tiendras dehors, et l'homme à qui tu feras le prêt t'apportera le gage dehors.
24:12	Si cet homme est pauvre, tu ne te coucheras pas ayant encore son gage.
24:13	Tu lui rendras le gage, tu le lui rendras dès que le soleil sera couché, afin qu'il se couche dans son vêtement et qu'il te bénisse. Et cela te sera imputé à justice devant YHWH, ton Elohîm.
24:14	Tu ne presseras pas le mercenaire, le pauvre et l'indigent, d'entre tes frères, ou d'entre les étrangers qui demeurent en ta terre, dans tes portes.
24:15	Tu lui donneras son salaire le jour même avant que le soleil se couche, car il est pauvre et son âme l'attend. Autrement il crierait à YHWH contre toi, et tu serais chargé d'un péché.
24:16	On ne fera pas mourir les pères pour les fils, et on ne fera pas mourir les fils pour les pères. Mais on fera mourir chacun pour son péché.
24:17	Tu ne feras pas d'injustice à l'étranger ni à l'orphelin, et tu ne prendras pas en gage le vêtement de la veuve.
24:18	Et tu te souviendras que tu as été esclave en Égypte, et que YHWH, ton Elohîm, t'a racheté de là. C'est pourquoi je t'ordonne de faire ces choses.
24:19	Quand tu moissonneras ta moisson dans ton champ, et que tu auras oublié une gerbe dans ton champ, tu ne retourneras pas la prendre : elle sera pour l'étranger, pour l'orphelin et pour la veuve, afin que YHWH, ton Elohîm, te bénisse dans toute l'œuvre de tes mains.
24:20	Quand tu secoueras tes oliviers, tu n'y retourneras pas pour cueillir ce qui reste aux branches : ce sera pour l'étranger, pour l'orphelin et pour la veuve.
24:21	Quand tu vendangeras ta vigne, tu ne grappilleras pas après : ce sera pour l'étranger, pour l'orphelin et pour la veuve.
24:22	Et tu te souviendras que tu as été esclave en terre d'Égypte. C'est pourquoi je t'ordonne de faire ces choses.

## Chapitre 25

### Le juste justifié et le méchant condamné

25:1	Quand il y aura un différend entre des hommes et qu'ils viendront en jugement afin qu'on les juge, on justifiera le juste, et on condamnera le méchant.
25:2	Et s’il arrive que le méchant mérite d'être battu, le juge le fera jeter par terre et on le battra devant lui d'un nombre de coups proportionnel à sa culpabilité.
25:3	Il le fera battre de 40 coups<!--2 Co. 11:24.-->, pas plus, de peur que si l'on continuait à le frapper avec plus de coups, ton frère ne soit méprisé à tes yeux.
25:4	Tu ne muselleras pas ton bœuf lorsqu'il foule le grain<!--1 Co. 9:9.-->.

### Torah sur la continuité de la postérité<!--Le lévirat.-->

25:5	Quand des frères demeureront ensemble, et que l'un d'entre eux mourra sans fils, la femme du mort ne sera pas, dehors, à un homme étranger. C'est son beau-frère qui ira vers elle, la prendra pour femme et il exercera envers elle son droit de beau-frère<!--Lé. 25:25,48. Voir commentaire en Ru. 2:20.-->.
25:6	Il arrivera que le premier-né qu’elle enfantera s’établira sur le nom du frère mort, afin que le nom de celui-ci ne soit pas effacé d’Israël.
25:7	Si cet homme ne désire pas prendre sa belle-sœur, sa belle-sœur montera à la porte vers les anciens<!--Ru. 4:1-10.-->, et dira : Mon beau-frère refuse de relever le nom de son frère en Israël, il ne veut pas exercer envers moi son droit de beau-frère.
25:8	Les anciens de la ville l'appelleront et lui parleront. S'il demeure ferme, en disant : Je ne désire pas la prendre,
25:9	sa belle-sœur s'approchera de lui sous les yeux des anciens et lui enlèvera la sandale du pied, lui crachera à la face et, répondant, elle dira : Voilà ce qu'on fait à l'homme qui refuse de bâtir la maison de son frère.
25:10	Et son nom sera appelé en Israël la maison de celui à qui l'on a enlevé la sandale.

### L'abomination sévèrement et justement punie

25:11	Quand des hommes se querelleront ensemble, un homme contre son frère, si la femme de l'un s'approche pour délivrer son homme de la main de celui qui le frappe, et qu'étendant sa main elle saisisse ses parties intimes,
25:12	tu lui couperas la main, et ton œil ne l'épargnera pas.
25:13	Tu n'auras pas dans ton sac deux poids différents, un grand et un petit.
25:14	Il n'y aura pas dans ta maison un épha grand et un épha petit<!--Lé. 19:35-37.-->.
25:15	Mais tu auras un poids exact et juste, tu auras un épha exact et juste, afin que tes jours se prolongent sur le sol que YHWH, ton Elohîm, te donne.
25:16	Car celui qui fait ces choses, celui qui commet une injustice, est en abomination à YHWH, ton Elohîm.

### YHWH confirme le sort d'Amalek

25:17	Souviens-toi de ce que t'a fait Amalek sur le chemin de votre sortie d'Égypte<!--Ex. 17:8.-->,
25:18	comment il est venu te rencontrer sur le chemin, et, sans aucune crainte d'Elohîm, attaqua par derrière ceux qui étaient fatigués derrière toi, quand toi-même tu étais épuisé et fatigué.
25:19	Il arrivera que quand YHWH, ton Elohîm, t'aura accordé du repos de tous tes ennemis qui t'entourent, sur la terre que YHWH, ton Elohîm, te donne en héritage pour en prendre possession, alors tu effaceras la mémoire d'Amalek de dessous les cieux : ne l'oublie pas.

## Chapitre 26

### Torah sur les prémices ou les premiers fruits<!--Cp. Ex. 23:16-19.-->

26:1	Il arrivera que quand tu seras entré sur la terre que YHWH, ton Elohîm, te donne en héritage, que tu en auras pris possession et y seras établi,
26:2	tu prendras les premiers de tous les fruits du sol, que tu auras tirés de la terre que YHWH, ton Elohîm, te donne, tu les mettras dans une corbeille, et tu iras au lieu que YHWH, ton Elohîm, choisira pour y faire habiter son Nom<!--Ex. 23:16-19.-->.
26:3	Tu viendras vers le prêtre qui sera en ce temps-là, et tu lui diras : Je déclare aujourd'hui à YHWH, ton Elohîm, que je suis entré sur la terre que YHWH a juré à nos pères de nous donner.
26:4	Le prêtre prendra la corbeille de ta main, il la posera devant l'autel de YHWH, ton Elohîm.
26:5	Tu prendras la parole et tu diras devant YHWH ton Elohîm : Mon père était un Syrien qui périssait. Il est descendu en Égypte avec un petit nombre de gens et il y a habité. Là, il est devenu une nation grande, puissante et nombreuse.
26:6	Les Égyptiens nous ont maltraités et humiliés, et ils nous ont imposé une dure servitude.
26:7	Nous avons crié à YHWH, l'Elohîm de nos pères. YHWH a entendu notre voix et il a vu notre affliction, notre travail et notre oppression.
26:8	YHWH nous a fait sortir d'Égypte, à main forte et à bras étendu, avec une grande frayeur, avec des signes et des miracles.
26:9	Il nous a fait venir dans ce lieu et il nous a donné cette terre, terre où coulent le lait et le miel.
26:10	Maintenant voici, j'apporte les premiers des fruits du sol que tu m'as donné, YHWH ! Tu les poseras devant YHWH, ton Elohîm, et tu te prosterneras devant YHWH, ton Elohîm.
26:11	Tu te réjouiras de tout le bien que YHWH, ton Elohîm, t'aura donné, et à ta maison, toi et le Lévite, et l'étranger qui sera au milieu de toi.
26:12	Quand tu auras achevé de lever toute la dîme de ta récolte, la troisième année, l'année de la dîme, tu la donneras au Lévite, à l'étranger, à l'orphelin, et à la veuve ; ils en mangeront dans tes portes, et ils en seront rassasiés.
26:13	Tu diras en face de YHWH, ton Elohîm : J’ai consumé le sacré de la maison. Je l'ai donné au Lévite, à l'étranger, à l'orphelin, et à la veuve, selon tous tes commandements que tu m'as ordonnés. Je n'ai transgressé ni oublié aucun de tes commandements.
26:14	Je n’en ai pas mangé durant mon deuil, je n’en ai pas brûlé étant impur et je n’en ai pas donné pour un mort. J'ai obéi à la voix de YHWH, mon Elohîm, j'ai agi selon tout ce que tu m'avais ordonné.
26:15	Regarde de ta sainte demeure, des cieux, et bénis ton peuple d'Israël et le sol que tu nous as donné, comme tu l'avais juré à nos pères, terre où coulent le lait et le miel.
26:16	Aujourd'hui, YHWH, ton Elohîm, t'ordonne de mettre en pratique ces lois et ces ordonnances. Prends garde de les faire de tout ton cœur et de toute ton âme.
26:17	Tu as fait promettre aujourd'hui à YHWH qu'il sera ton Elohîm, pour que tu marches dans ses voies, que tu observes ses lois, ses commandements et ses ordonnances, et que tu obéisses à sa voix.
26:18	Aujourd'hui, YHWH t'a fait promettre que tu seras son peuple, sa propriété comme il te l'a dit, et que tu observeras tous ses commandements,
26:19	pour qu'il te donne sur toutes les nations qu'il a créées la supériorité en louange, en renom et en beauté, et pour que tu sois un peuple saint pour YHWH, ton Elohîm, comme il te l'a dit.

## Chapitre 27

### La torah gravée sur des pierres au Mont Ébal

27:1	Moshé et les anciens d'Israël donnèrent cet ordre au peuple en disant : Gardez tous les commandements que je vous ordonne aujourd'hui.
27:2	Il arrivera au jour où vous passerez le Yarden, pour entrer sur la terre que YHWH, ton Elohîm, te donne, tu dresseras de grandes pierres, et tu les enduiras de chaux.
27:3	Tu écriras sur elles toutes les paroles de cette torah, quand tu passeras pour entrer sur la terre que YHWH, ton Elohîm, te donne, terre où coulent le lait et le miel, comme te l'a dit YHWH, l'Elohîm de tes pères.
27:4	Il arrivera, quand vous aurez passé le Yarden, que vous dresserez ces pierres sur le Mont Ébal, comme je vous l’ordonne aujourd’hui, et tu les enduiras de chaux.
27:5	Tu bâtiras aussi là un autel à YHWH, ton Elohîm ; un autel de pierres, sur lesquelles tu ne lèveras pas le fer.
27:6	Tu bâtiras l'autel de YHWH, ton Elohîm, de pierres entières. Tu y feras monter des holocaustes à YHWH, ton Elohîm.
27:7	Tu y offriras aussi des offrandes de paix<!--Voir commentaire en Lé. 3:1.-->, et tu mangeras là et te réjouiras devant YHWH, ton Elohîm.
27:8	Et tu écriras sur ces pierres toutes les paroles de cette torah, en les gravant bien distinctement.

### Les malédictions prononcées sur le Mont Ébal

27:9	Moshé et les prêtres, les Lévites, parlèrent à tout Israël, en disant : Écoute et garde le silence, Israël ! Aujourd'hui, tu es devenu le peuple de YHWH, ton Elohîm.
27:10	Tu écouteras la voix de YHWH, ton Elohîm, et tu observeras ces commandements et ces lois que je te prescris aujourd'hui.
27:11	Moshé donna cet ordre au peuple ce jour-là, en disant :
27:12	Ceux-ci se tiendront sur le Mont Garizim pour bénir le peuple, quand vous passerez le Yarden : Shim’ôn, Lévi, Yéhouda, Yissakar, Yossef, et Benyamin ;
27:13	Et ceux-ci se tiendront sur le Mont Ébal, pour maudire : Reouben, Gad, Asher, Zebouloun, Dan et Nephthali.
27:14	Les Lévites répondront et diront à haute voix à tous les hommes d'Israël :
27:15	Maudit soit l'homme qui fait une idole ou une image en métal fondu, car c'est une abomination à YHWH, œuvre des mains d'un artisan, et qui la met dans un lieu secret ! Et tout le peuple répondra, et dira : Amen !
27:16	Maudit soit celui qui méprise son père et sa mère ! Et tout le peuple dira : Amen !
27:17	Maudit soit celui qui déplace les bornes de son prochain ! Et tout le peuple dira : Amen !
27:18	Maudit soit celui qui égare un aveugle dans le chemin ! Et tout le peuple dira : Amen !
27:19	Maudit soit celui qui fait injustice à l'étranger, à l'orphelin, et à la veuve ! Et tout le peuple dira : Amen !
27:20	Maudit soit celui qui couche avec la femme de son père, car il découvre le pan de la robe de son père ! Et tout le peuple dira : Amen !
27:21	Maudit soit celui qui couche avec une bête ! Et tout le peuple dira : Amen !
27:22	Maudit soit celui qui couche avec sa sœur, fille de son père, ou fille de sa mère ! Et tout le peuple dira : Amen !
27:23	Maudit soit celui qui couche avec sa belle-mère ! Et tout le peuple dira : Amen !
27:24	Maudit soit celui qui frappe son prochain en secret ! Et tout le peuple dira : Amen !
27:25	Maudit soit celui qui reçoit un pot-de-vin pour frapper une âme, alors que c’est du sang innocent ! Et tout le peuple dira : Amen !
27:26	Maudit soit celui qui n'accomplit pas les paroles de cette torah et ne les met pas en pratique ! Et tout le peuple dira : Amen !

## Chapitre 28

### Les bénédictions accompagnent l'obéissance

28:1	Or il arrivera que si tu écoutes, si tu écoutes la voix de YHWH, ton Elohîm, et que tu prennes garde de pratiquer tous ses commandements que je t'ordonne aujourd'hui, YHWH, ton Elohîm, te donnera la supériorité sur toutes les nations de la Terre.
28:2	Voici toutes les bénédictions qui viendront sur toi, et qui t'atteindront, quand tu obéiras à la voix de YHWH, ton Elohîm :
28:3	tu seras béni dans la ville, et tu seras aussi béni dans les champs.
28:4	Le fruit de tes entrailles, le fruit du sol, le fruit de tes troupeaux, les portées de ton gros et de ton petit bétail seront bénis.
28:5	Ta corbeille et ta cuve de pétrissage seront bénies.
28:6	Tu seras béni en entrant, et tu seras béni en sortant.
28:7	YHWH te donnera tes ennemis qui se lèveront contre toi. Ils seront battus en face de toi. Ils sortiront contre toi par un chemin, et s’enfuiront en face de toi par sept chemins.
28:8	YHWH ordonnera à la bénédiction d'être avec toi dans tes greniers et dans tout ce à quoi tu mettras ta main ; il te bénira sur la terre que YHWH, ton Elohîm, te donne.
28:9	YHWH t'établira pour être son peuple saint, comme il te l'a juré, quand tu garderas les commandements de YHWH, ton Elohîm, et que tu marcheras dans ses voies.
28:10	Tous les peuples de la Terre verront que tu es appelé du Nom de YHWH, et ils te craindront.
28:11	YHWH te fera abonder de biens dans le fruit de tes entrailles, le fruit de tes troupeaux, et le fruit de ton sol, sur le sol que YHWH a juré à tes pères de te donner.
28:12	YHWH t'ouvrira son bon trésor, les cieux, pour donner à ta terre la pluie en son temps et pour bénir tout le travail de tes mains. Tu prêteras à beaucoup de nations et tu n'emprunteras pas.
28:13	YHWH te mettra à la tête et non à la queue, tu seras toujours en haut et tu ne seras jamais en bas, quand tu obéiras aux commandements de YHWH, ton Elohîm, que je t'ordonne aujourd'hui, d’observer et de pratiquer,
28:14	et que tu ne te détourneras, ni à droite ni à gauche, d’aucune des paroles que je vous ordonne aujourd’hui, pour aller après d’autres elohîm, pour les servir.

### Les malédictions accompagnent la désobéissance

28:15	Mais s’il arrive que tu n'écoutes pas la voix de YHWH, ton Elohîm, pour prendre garde de pratiquer tous ses commandements et ses statuts que je t'ordonne aujourd'hui, voici toutes les malédictions qui viendront sur toi, et qui t'atteindront :
28:16	tu seras maudit dans la ville, et tu seras maudit dans les champs.
28:17	Ta corbeille et ta cuve de pétrissage seront maudites.
28:18	Le fruit de tes entrailles, le fruit de ton sol, les portées de ton gros et de ton petit bétail seront maudits.
28:19	Tu seras maudit à ton entrée, et tu seras maudit à ta sortie.
28:20	YHWH enverra sur toi la malédiction, la confusion, et la ruine dans tout ce à quoi tu mettras ta main et que tu feras, jusqu'à ce que tu sois détruit, et que tu périsses promptement, à cause de la méchanceté de tes pratiques, par lesquelles tu m'auras abandonné.
28:21	YHWH te fera attraper la peste jusqu'à ce qu'elle te consume du sol où tu entres pour en prendre possession.
28:22	YHWH te frappera de maladie infectieuse<!--Une maladie des poumons.-->, de fièvre, d'inflammation, de chaleur extrême, de l'épée, de flétrissure et de rouille, qui te poursuivront jusqu'à ce que tu périsses.
28:23	Les cieux sur ta tête seront de cuivre, et la terre sous toi sera de fer.
28:24	YHWH te donnera pour pluie à ta terre de la poussière et de la poudre. Elles descendront des cieux sur toi jusqu'à ce que tu sois détruit.
28:25	YHWH te donnera à tes ennemis. Tu seras battu en face d'eux. Tu sortiras par un chemin contre eux, et tu t'enfuiras en face d'eux par sept chemins. Tu deviendras un objet de terreur pour tous les royaumes de la Terre.
28:26	Ton cadavre deviendra la nourriture de tous les oiseaux des cieux et des bêtes de la terre, et il n'y aura personne pour les effrayer.
28:27	YHWH te frappera de l'ulcère d'Égypte, d'hémorroïdes, de gale et de démangeaison, dont tu ne pourras guérir.
28:28	YHWH te frappera de folie, d'aveuglement et d'égarement d'esprit<!--1 R. 22:21-24 ; Ro. 1:24-28 ; 2 Th. 2:11.-->.
28:29	Il arrivera que tu tâtonneras en plein midi comme tâtonne un aveugle dans l'obscurité, tu ne prospéreras pas dans tes voies, et tu seras opprimé et dépouillé tous les jours, et il n'y aura personne pour venir te sauver.
28:30	Tu fianceras une femme, mais un autre homme couchera avec elle et la violera ; tu bâtiras une maison, mais tu ne l'habiteras pas. Tu planteras une vigne, mais tu n'en jouiras pas.
28:31	Ton bœuf sera tué sous tes yeux, et tu n'en mangeras pas. Ton âne sera enlevé devant toi, et on ne te le rendra pas. Tes brebis seront livrées à tes ennemis, et il n'y aura personne pour te sauver.
28:32	Tes fils et tes filles seront livrés à un autre peuple, tes yeux le verront, et languiront tout le jour après eux, et tu n'auras aucun pouvoir en ta main.
28:33	Un peuple que tu n'auras pas connu mangera le fruit de ton sol et tout ton travail, et tu seras opprimé et écrasé tous les jours.
28:34	Tu deviendras fou à cause de ce que tu verras de tes yeux.
28:35	YHWH te frappera d'un ulcère malin sur les genoux et sur les cuisses dont tu ne pourras guérir, depuis la plante du pied jusqu'au sommet de ta tête.
28:36	YHWH te fera marcher, toi et ton roi que tu auras établi sur toi, vers une nation que tu n'auras pas connue, ni toi ni tes pères. Et là, tu serviras d'autres elohîm, du bois et de la pierre.
28:37	Tu deviendras une horreur, une parabole, une raillerie, parmi tous les peuples vers lesquels YHWH t'aura emmené.
28:38	Tu jetteras beaucoup de semence dans ton champ, et tu recueilleras peu, car les sauterelles la consumeront.
28:39	Tu planteras des vignes, tu les cultiveras, mais tu n'en boiras pas le vin et tu n'en recueilleras rien, car les vers la mangeront.
28:40	Tu auras des oliviers sur tout le territoire, mais tu ne t'oindras pas d'huile, car tes olives tomberont.
28:41	Tu engendreras des fils et des filles, mais ils ne seront pas à toi, car ils iront en captivité.
28:42	Les insectes prendront possession de tous tes arbres et du fruit de ton sol.
28:43	L'étranger qui sera au milieu de toi montera toujours plus au-dessus de toi, et toi, tu descendras toujours plus bas.
28:44	Il te prêtera, et tu ne lui prêteras pas. Il sera à la tête, et tu seras à la queue.
28:45	Toutes ces malédictions viendront sur toi, elles te poursuivront et t'atteindront jusqu'à ce que tu sois détruit, parce que tu n'auras pas obéi à la voix de YHWH, ton Elohîm, pour garder ses commandements et ses statuts qu'il t'a ordonnés.
28:46	Ces choses deviendront pour toi et pour ta postérité des signes et des prodiges, à perpétuité.
28:47	Et parce que tu n'auras pas servi YHWH, ton Elohîm, avec joie, et de bon cœur, malgré l'abondance de toutes choses,
28:48	tu serviras, dans la faim, dans la soif, dans la nudité, et dans la disette de toutes choses, ton ennemi que YHWH enverra contre toi. Il mettra un joug de fer sur ton cou, jusqu'à ce qu'il t'ait détruit.

### Prophétie sur l'invasion babylonienne et la dispersion d'Israël<!--2 R. 24-25.-->

28:49	YHWH fera lever de loin, des extrémités de la Terre, une nation qui volera comme l'aigle, une nation dont tu ne comprendras pas la langue,
28:50	une nation aux faces féroces, qui ne portera pas les faces du vieux et n'aura pas pitié pour le jeune homme<!--Cette prophétie s'est accomplie en 587 av. J.-C. Voir 2 R. 24-25.-->.
28:51	Elle mangera le fruit de tes troupeaux et les fruits de ton sol jusqu'à ce que tu sois détruit. Elle n'épargnera ni blé, ni vin nouveau, ni huile, ni portée de ton gros et de ton petit bétail jusqu'à ce qu'elle t'ait fait périr.
28:52	Et elle t'assiégera<!--2 R. 24:10, 25:1-3. Le siège de Yeroushalaim s'est déroulé en 587 et 586 av. J.-C.--> dans toutes tes portes, jusqu'à ce que tombent ces hautes et fortes murailles dans lesquelles tu auras mis ta confiance dans toute ta terre. Elle t'assiégera dans toutes tes portes, sur toute la terre que YHWH, ton Elohîm, te donne.
28:53	Tu mangeras le fruit de tes entrailles, la chair de tes fils et de tes filles<!--2 R. 6:29 ; La. 4:10.--> que YHWH, ton Elohîm, t'aura donnés, durant le siège et la détresse dont ton ennemi te serrera.
28:54	L'homme le plus tendre et le plus délicat d'entre vous regardera d'un mauvais œil son frère, sa femme bien-aimée, et le reste de ses fils qu'il a épargnés,
28:55	pour ne donner à aucun d'eux de la chair de ses fils, qu'il mangera, parce qu'il ne lui restera rien du tout, à cause du siège et de la détresse dont ton ennemi te serrera dans toutes tes portes.
28:56	La femme la plus tendre et la plus délicate d'entre vous, qui n'a pas osé mettre la plante de son pied sur la terre, par délicatesse et par mollesse, regardera d'un mauvais œil son mari bien-aimé, son fils et sa fille,
28:57	et le placenta qui sortira d'entre ses pieds, et les fils qu'elle enfantera<!--La. 2:20, 4:10.-->. Car manquant de tout, elle les mangera secrètement, à cause du siège et de la détresse, dont ton ennemi te serrera dans toutes les villes.
28:58	Si tu ne prends pas garde d'observer toutes les paroles de cette torah, qui sont écrites dans ce livre, en craignant le Nom glorieux et redoutable de YHWH, ton Elohîm,
28:59	YHWH rendra extraordinaires tes plaies et les plaies de ta postérité, des plaies grandes et persistantes, des maladies mauvaises et persistantes.
28:60	Et il fera retourner sur toi toutes les maladies d'Égypte, devant lesquelles tu avais peur et elles s'attacheront à toi.
28:61	Même YHWH fera venir sur toi toutes maladies et toutes plaies, qui ne sont pas écrites dans le livre de cette torah, jusqu'à ce que tu sois détruit.
28:62	Et vous resterez en petit nombre, après avoir été aussi nombreux que les étoiles des cieux, parce que tu n'auras pas obéi à la voix de YHWH, ton Elohîm.
28:63	Et il arrivera que comme YHWH se réjouissait à votre égard, en vous faisant du bien et en vous multipliant, de même YHWH se réjouira à votre égard en vous faisant périr et en vous détruisant. Vous serez arrachés<!--Lé. 26:33.--> du sol dont vous allez entrer en possession.
28:64	YHWH te dispersera<!--Lé. 26:33 ; 2 R. 24 et 25.--> parmi tous les peuples, d’une extrémité de la Terre à l’autre extrémité de la Terre. Là, tu serviras d'autres elohîm que ni toi ni tes pères n'avez connus, le bois et la pierre.
28:65	Tu n'auras aucun repos parmi ces nations<!--Ps. 137.-->, même la plante de ton pied n'aura aucun repos. Car YHWH te donnera un cœur tremblant, des yeux languissants, et une âme souffrante.
28:66	Et ta vie sera en suspens devant toi, tu trembleras la nuit et le jour, et tu ne seras pas sûr de ta vie.
28:67	Tu diras le matin : Qui me fera voir le soir ? Et le soir tu diras : Qui me fera voir le matin ? À cause de la terreur dont tremblera ton cœur et à cause des choses que tu verras de tes yeux.
28:68	Et YHWH te fera retourner en Égypte sur des navires, pour faire le chemin dont je t'ai dit : Tu ne le verras plus ! Et là, vous vous vendrez à vos ennemis, comme esclaves et servantes et il n'y aura personne pour vous acheter.
28:69	Voici les paroles de l'alliance que YHWH ordonna à Moshé de traiter avec les fils d'Israël en terre de Moab, outre l'alliance qu'il avait traitée avec eux à Horeb.

## Chapitre 29

### YHWH rappelle sa fidélité à Israël

29:1	Moshé appela tout Israël, et leur dit : Vous avez vu tout ce que YHWH a fait sous vos yeux, en terre d'Égypte, à pharaon, à tous ses serviteurs, et à toute sa terre,
29:2	les grandes épreuves que tes yeux ont vues, ces signes et ces grands miracles.
29:3	Mais, jusqu'à ce jour, YHWH ne vous a pas donné un cœur pour connaître, ni des yeux pour voir, ni des oreilles pour entendre.
29:4	Je t'ai conduit pendant 40 ans par le désert. Tes vêtements ne se sont pas usés et ta sandale ne s'est pas usée à ton pied.
29:5	Vous n'avez pas mangé de pain, ni bu de vin ni de liqueur forte, afin que vous sachiez que je suis YHWH, votre Elohîm.
29:6	Et vous êtes venus en ce lieu. Sihon, roi de Hesbon, et Og, roi de Bashân, sont sortis à notre rencontre, pour nous combattre, et nous les avons battus.
29:7	Nous avons pris leur terre et nous l'avons donnée en héritage aux Reoubénites, aux Gadites et à la demi-tribu des Menashites.

### Béni est celui qui reste fidèle à l'alliance

29:8	Vous garderez les paroles de cette alliance, et vous les pratiquerez, afin de prospérer dans tout ce que vous ferez.
29:9	Vous vous tiendrez aujourd'hui devant YHWH, votre Elohîm, vos chefs de tribus, vos anciens, vos commissaires, tous les hommes d'Israël,
29:10	vos enfants, vos femmes, et l'étranger qui est au milieu de ton camp, depuis celui qui coupe ton bois jusqu'à celui qui puise ton eau ;
29:11	afin que tu entres dans l'alliance de YHWH, ton Elohîm, dans ce serment, que YHWH, ton Elohîm, traite aujourd'hui avec toi,
29:12	afin qu'il t'établisse aujourd'hui pour son peuple et qu'il soit ton Elohîm, comme il te l'a dit, et comme il l'a juré à tes pères, Abraham, Yitzhak et Yaacov.
29:13	Et ce n'est pas seulement avec vous que je traite cette alliance, ce serment.
29:14	Mais c'est avec ceux qui sont ici, avec nous aujourd'hui devant YHWH, notre Elohîm, et avec ceux qui ne sont pas ici, avec nous aujourd'hui.

### Mise en garde contre celui qui abandonne l'alliance

29:15	Car vous savez comment nous avons habité sur la terre d'Égypte, et comment nous sommes passés au milieu des nations, que vous avez traversées.
29:16	Et vous avez vu leurs abominations et leurs idoles, le bois et la pierre, l'argent et l'or qui sont parmi eux.
29:17	Qu'il n'y ait parmi vous ni homme, ni femme, ni famille, ni tribu qui détourne son cœur aujourd'hui de YHWH, notre Elohîm, pour aller servir les elohîm de ces nations. Qu'il n'y ait pas parmi vous de racine qui produise du poison et de l'absinthe<!--Hé. 12:15.-->.
29:18	Qu'il n'arrive que quelqu'un, en entendant les paroles de cette malédiction, ne se bénisse dans son cœur, en disant : Il y aura paix pour moi, même si je marche dans la dureté de mon cœur, en balayant celui qui est arrosé avec celui qui a soif.
29:19	YHWH ne voudra pas lui pardonner. Mais alors la colère de YHWH et la jalousie s'enflammeront contre cet homme, et toutes les malédictions écrites dans ce livre reposeront sur lui, et YHWH effacera son nom de dessous les cieux.
29:20	Et YHWH le séparera de toutes les tribus d'Israël, pour son malheur, selon toutes les malédictions de l'alliance écrite dans ce livre de la torah.
29:21	Elle dira, la génération dernière, vos fils qui se lèveront après vous, et l'étranger qui viendra d'une terre lointaine, et qui verront les plaies de cette terre et les maladies dont YHWH l'aura frappé :
29:22	« Toute cette terre n'est que soufre, sel et feu. On ne peut rien y semer, rien n'y germe, aucune herbe n'y pousse, comme après la destruction de Sodome, et de Gomorrhe, et d'Admah, et de Tseboïm, que YHWH renversa dans sa colère et dans sa fureur.»
29:23	Toutes les nations diront : « Pourquoi YHWH a-t-il traité ainsi cette terre ? D'où vient l'ardeur de cette grande colère ? »
29:24	On dira : « C'est parce qu'ils ont abandonné l'alliance de YHWH, l'Elohîm de leurs pères, qu'il a traitée avec eux quand il les fit sortir de la terre d'Égypte.
29:25	Ils sont allés servir d'autres elohîm et se sont prosternés devant eux, des elohîm qu'ils ne connaissaient pas et qu'il ne leur avait pas donnés en partage.
29:26	À cause de cela, la colère de YHWH s'est enflammée contre cette terre, et il a fait venir sur elle toutes les malédictions écrites dans ce livre. 
29:27	YHWH les a arrachés de leur sol avec colère, avec courroux, avec une grande indignation<!--Ce mot signifie aussi « colère ».-->, et il les a chassés sur une autre terre, comme en ce jour. »
29:28	Les choses cachées sont à YHWH, notre Elohîm, et les choses révélées sont à nous et à nos fils, à jamais, afin que nous pratiquions toutes les paroles de cette torah.

## Chapitre 30

### YHWH bénira et restaurera le peuple repentant

30:1	Il arrivera que lorsque toutes ces choses seront venues sur toi, la bénédiction et la malédiction, que je mets devant toi, si tu les prends de nouveau à cœur, au milieu de toutes les nations vers lesquelles YHWH, ton Elohîm, t'aura banni,
30:2	si tu reviens à YHWH, ton Elohîm, et si tu obéis à sa voix de tout ton cœur, de toute ton âme, toi et tes fils, selon tout ce que je t'ordonne aujourd'hui,
30:3	YHWH, ton Elohîm, ramènera tes captifs, il aura compassion de toi, il te ramènera et te rassemblera de tous les peuples là où YHWH ton Elohîm, t'aura dispersé.
30:4	Quand tu serais banni à l'extrémité des cieux, YHWH, ton Elohîm, te rassemblera de là, et de là, il te prendra.
30:5	YHWH, ton Elohîm te ramènera sur la terre dont tes pères avaient pris possession, et tu en prendras possession. Il te fera du bien, et te rendra plus nombreux que tes pères.
30:6	YHWH, ton Elohîm, circoncira ton cœur, et le cœur de ta postérité, pour que tu aimes YHWH, ton Elohîm, de tout ton cœur, et de toute ton âme, afin que tu vives<!--Ro. 2:29.-->.
30:7	Et YHWH, ton Elohîm, mettra toutes ces malédictions sur tes ennemis, et sur ceux qui te haïront et te persécuteront.
30:8	Tu retourneras à YHWH, tu obéiras à sa voix, et tu pratiqueras tous ses commandements que je t'ordonne aujourd'hui.

### Faire connaître la torah aux futures générations

30:9	Et YHWH, ton Elohîm, te fera abonder en bien dans toute l'œuvre de ta main, dans le fruit de tes entrailles, dans le fruit de tes troupeaux et dans le fruit de ton sol, car YHWH se réjouira de nouveau de ton bonheur, comme il s'est réjoui de celui de tes pères,
30:10	quand tu obéiras à la voix de YHWH, ton Elohîm, en gardant ses commandements et ses statuts écrits dans ce livre de la torah, quand tu reviendras à YHWH, ton Elohîm, de tout ton cœur et de toute ton âme.

### Le peuple devant un choix

30:11	Car ce commandement que je t'ordonne aujourd'hui n'est pas trop difficile pour toi, ni loin de toi.
30:12	Il n’est pas dans les cieux, pour qu’on dise : Qui montera pour nous dans les cieux ? Qui le prendra pour nous et nous le fera entendre pour que nous le pratiquions ?
30:13	Il n’est pas au-delà de la mer, pour qu'on dise : Qui passera pour nous au-delà de la mer, le prendra pour nous, nous le fera entendre pour que nous le pratiquions ?
30:14	Car cette parole est très près de toi, dans ta bouche et dans ton cœur, afin que tu la pratiques<!--Ro. 10:8.-->.
30:15	Regarde, je mets aujourd'hui devant toi la vie et le bonheur, la mort et le malheur.
30:16	Car je t'ordonne aujourd'hui d'aimer YHWH, ton Elohîm, de marcher dans ses voies, de garder ses commandements, ses statuts et ses ordonnances, afin que tu vives, que tu multiplies, et que YHWH, ton Elohîm, te bénisse sur la terre où tu entres pour en prendre possession.
30:17	Mais si ton cœur se détourne, si tu n'obéis pas, si tu es banni, si tu te prosternes devant d’autres elohîm et que tu les serves,
30:18	je vous déclare aujourd'hui que vous périrez, vous périrez ! Vous ne prolongerez pas vos jours sur le sol où tu entres pour en prendre possession en passant le Yarden.
30:19	J'en prends aujourd'hui à témoin les cieux et la Terre contre vous : j'ai mis devant toi la vie et la mort, la bénédiction et la malédiction. Choisis la vie<!--Cp. Mt. 7:13-14.-->, afin que tu vives, toi et ta postérité,
30:20	en aimant YHWH, ton Elohîm, en obéissant à sa voix, et en t'attachant à lui. Car c'est lui qui est ta vie et la longueur de tes jours, afin que tu demeures sur le sol que YHWH a juré à tes pères, à Abraham, à Yitzhak et à Yaacov de leur donner.

## Chapitre 31

### Moshé encourage et affermit le peuple

31:1	Moshé s'en alla, et dit ces paroles à tout Israël :
31:2	Aujourd'hui, leur dit-il, je suis fils de 120 ans, je ne pourrai plus sortir ni entrer, et YHWH m'a dit : Tu ne passeras pas ce Yarden !
31:3	YHWH ton Elohîm passera lui-même en face de toi, il détruira ces nations en face de toi et tu les déposséderas. Yéhoshoua passera aussi en face de toi, comme YHWH l'a dit.
31:4	Et YHWH les traitera comme il a traité Sihon et Og, rois des Amoréens, qu'il a détruits avec leurs terres.
31:5	Et YHWH les livrera en face de vous, et vous agirez à leur égard selon tout le commandement que je vous ai donné.
31:6	Fortifiez-vous et prenez courage ! N'ayez pas peur et ne soyez pas effrayés en face d’eux, car YHWH, ton Elohîm, marchera avec toi, il ne te délaissera<!--Hé. 13:5.--> pas et ne t'abandonnera pas.
31:7	Et Moshé appela Yéhoshoua, et lui dit en face de tout Israël : Fortifie-toi et prends courage, car tu entreras avec ce peuple sur la terre que YHWH a juré à leurs pères de leur donner, et c'est toi qui les en mettras en possession.
31:8	YHWH lui-même marchera en face de toi, il sera lui-même avec toi, il ne te délaissera pas, il ne t'abandonnera pas. N'aie pas peur et ne t'effraie pas.
31:9	Or Moshé écrivit cette torah, et il la donna aux prêtres, fils de Lévi, qui portaient l'arche de l'alliance de YHWH, et à tous les anciens d'Israël.
31:10	Moshé leur ordonna, en disant : Tous les 7 ans, au temps fixé de l'année de la remise, à la fête des cabanes,
31:11	quand tout Israël viendra voir les faces de YHWH, ton Elohîm, dans le lieu qu'il aura choisi, tu liras cette torah en face de tout Israël, à leurs oreilles.
31:12	Tu rassembleras le peuple, les hommes, les femmes, les enfants et l'étranger qui sera dans tes portes, pour qu'ils t'entendent, et qu'ils apprennent à craindre YHWH, votre Elohîm, et qu'ils prennent garde à pratiquer toutes les paroles de cette torah.
31:13	Et leurs fils qui ne la connaîtront pas l'entendront, et ils apprendront à craindre YHWH, votre Elohîm, tous les jours que vous vivrez sur ce sol dont vous allez prendre possession après avoir passé le Yarden.

### YHWH annonce les événements à venir

31:14	YHWH dit à Moshé : Voici, le jour où tu vas mourir est proche. Appelle Yéhoshoua, et tenez-vous dans la tente de réunion. Je lui donnerai mes ordres. Moshé et Yéhoshoua allèrent et se présentèrent dans la tente de réunion.
31:15	YHWH apparut dans la tente, dans une colonne de nuée. La colonne de nuée s'arrêta à l'entrée de la tente.
31:16	YHWH dit à Moshé : Voici, tu vas te coucher avec tes pères. Et ce peuple se lèvera et se prostituera après les elohîm étrangers de la terre au milieu de laquelle il va entrer. Il m'abandonnera et rompra mon alliance que j'ai traitée avec lui.
31:17	En ce jour-là ma colère s'enflammera contre lui. Je les abandonnerai, et je leur cacherai mes faces. Il sera dévoré, une multitude de maux et d'angoisses l'atteindront, et il dira en ce jour-là : N'est-ce pas parce que mon Elohîm n'est pas au milieu de moi, que ces maux m'ont atteint ?
31:18	En ce jour-là je cacherai, je cacherai mes faces, à cause de tout le mal qu'il aura fait, parce qu'il se sera tourné vers d'autres elohîm.
31:19	Maintenant, écrivez ce chant. Enseigne-le aux fils d'Israël, mets-le dans leur bouche, afin que ce chant me serve de témoignage contre les fils d'Israël.
31:20	Car je le conduirai sur le sol que j'ai juré à ses pères, où coulent le lait et le miel. Il mangera, se rassasiera, et s'engraissera ; puis il se tournera vers d'autres elohîm, et il les servira, il m'irritera par mépris et rompra mon alliance.
31:21	Et il arrivera qu'il sera atteint par une multitude de maux et d'angoisses, ce chant, qui ne sera pas oublié et qui sera dans la bouche de sa postérité, répondra comme témoin contre eux. Je connais ses desseins, qu'il a déjà préparés aujourd'hui, avant même que je l'aie fait entrer sur la terre que j'ai jurée.
31:22	Ainsi Moshé écrivit ce chant en ce jour-là, et l'enseigna aux fils d'Israël.
31:23	Et YHWH donna ses ordres à Yéhoshoua, fils de Noun, en disant : Fortifie-toi et prends courage, car c'est toi qui feras entrer les fils d'Israël sur la terre que je leur ai jurée. Je serai avec toi.
31:24	Et il arriva que quand Moshé eut achevé d'écrire dans un livre les paroles de cette torah jusqu'à ce qu'elle soit complète,
31:25	Moshé ordonna aux Lévites qui portaient l'arche de l'alliance de YHWH, en disant :
31:26	Prenez ce livre de la torah, et mettez-le à côté de l'arche de l'alliance de YHWH, votre Elohîm, et il sera là comme témoin contre toi.
31:27	Car je connais ta rébellion et ton cou raide. Voici, déjà aujourd'hui étant en vie avec vous, vous avez été rebelles contre YHWH, combien plus le serez-vous après ma mort ?
31:28	Rassemblez auprès de moi tous les anciens de vos tribus et vos commissaires. Je dirai ces paroles à leurs oreilles, et j'appellerai à témoin contre eux les cieux et la Terre.
31:29	Car je sais qu'après ma mort vous vous corromprez, vous vous corromprez et que vous vous détournerez de la voie que je vous ai prescrite. Mais à la fin, le malheur vous atteindra, parce que vous aurez fait ce qui déplaît aux yeux de YHWH, en l'irritant par les œuvres de vos mains.
31:30	Moshé déclara aux oreilles de toute l’assemblée d’Israël les paroles de ce chant, jusqu’à la fin.

## Chapitre 32

### Cantique de Moshé

32:1	Cieux ! Prêtez l'oreille et je parlerai. Terre ! Écoute les paroles de ma bouche.
32:2	Que mon enseignement tombe comme la pluie, que ma parole se répande comme la rosée, comme des ondées sur la végétation et comme de grosses averses sur l'herbe !
32:3	Car j'invoquerai le Nom de YHWH. Attribuez la grandeur à notre Elohîm.
32:4	L'œuvre du Rocher<!--Voir commentaire en Es. 8:13-17.--> est parfaite, car toutes ses voies sont justes. C'est le El fidèle et sans injustice, il est juste et droit.
32:5	Ils se sont corrompus à son égard, leur tache n’est pas celle de ses fils. C'est une génération tortueuse et perverse.

### Israël, le choix de YHWH

32:6	Est-ce ainsi que tu récompenses YHWH, peuple insensé et sans sagesse ? N'est-il pas ton père, qui t’a acheté<!--1 Co. 7:23.-->, celui qui t’a fait et qui t’a établi ?
32:7	Souviens-toi des anciens jours, considère les années, d'âge en âge, interroge ton père, et il te l'apprendra, et tes anciens, et ils te le diront.
32:8	Quand Élyon partageait leur héritage aux nations, quand il séparait les fils des humains, il fixa les limites des peuples selon le nombre des fils d'Israël.
32:9	Car la portion de YHWH, c'est son peuple, Yaacov est le lot de son héritage.
32:10	Il l'a trouvé sur une terre déserte, dans la désolation des hurlements d'un tohu, il l'a entouré, il l'a dirigé, il l'a gardé comme la prunelle de son œil,
32:11	comme l'aigle réveille son nid, couve ses petits, étend ses ailes, les prend, les porte sur ses ailes.
32:12	YHWH seul l'a conduit, et il n'y a pas eu avec lui de el étranger.
32:13	Il l'a fait monter à cheval sur les hauteurs de la terre, et il a mangé les fruits des champs. Il lui a donné à sucer le miel du rocher, l'huile du rocher le plus dur,
32:14	la crème des troupeaux et le lait des brebis, avec la graisse des agneaux, des béliers, fils de Bashân et des boucs, avec la graisse des rognons du blé. Tu as bu le vin, le sang du raisin.

### Condamnation de l'apostasie d'Israël

32:15	Yeshouroun<!--« Celui qui est droit ». Nom symbolique donné à Israël pour décrire son caractère idéal.--> s'est engraissé et a regimbé. Tu es devenu gras, gros et épais ! Il a abandonné Éloah qui l'a fait et il a méprisé le Rocher de son salut.
32:16	Ils ont excité sa jalousie par des elohîm étrangers, ils l'ont irrité par des abominations.
32:17	Ils ont sacrifié à des démons<!--Ps. 106:37 ; 1 Co. 10:20-21.--> qui ne sont pas Éloah, à des elohîm qu'ils ne connaissaient pas, des nouveaux, venus depuis peu, et que vos pères n'avaient pas redoutés.
32:18	Tu as oublié le Rocher qui t'a engendré, et tu as oublié le El qui t'a fait naître.
32:19	YHWH l'a vu, et a été irrité, parce que ses fils et ses filles l'ont provoqué à la colère.
32:20	Et il a dit : Je leur cacherai ma face, je verrai quelle sera leur fin, car ils sont une génération perverse, des fils infidèles.
32:21	Ils ont excité ma jalousie par ce qui n'est pas El, ils m'ont irrité par leurs vanités ; moi aussi j’exciterai leur jalousie par un non-peuple, et je les provoquerai à la colère par une nation insensée<!--Ro. 10:19.-->.
32:22	Car le feu de ma colère s'est allumé et brûlera jusqu'aux parties inférieures du shéol. Il dévorera la terre et son fruit, et embrasera les fondements des montagnes.
32:23	Je rassemblerai sur eux des maux, et je détruirai toutes mes flèches sur eux.
32:24	Ils seront consumés par la famine, rongés par des charbons ardents, et par une destruction amère. J'enverrai contre eux la dent des bêtes et le venin des serpents qui rampent sur la poussière.
32:25	Au-dehors l’épée les privera, et au-dedans, la terreur, du jeune homme comme de la vierge, de l'enfant à la mamelle comme de l'homme aux cheveux gris.

### À YHWH la vengeance et la rétribution

32:26	J'ai dit : Je les détruirai, et je ferai disparaître leur mémoire du milieu des mortels !
32:27	Si je ne craignais la colère de l'ennemi, de peur que leurs adversaires n'interprètent mal et ne disent : Notre main est élevée et ce n'est pas YHWH qui a fait tout cela.
32:28	Car c'est une nation qui se perd par les conseils, et il n'y a en eux aucune intelligence.
32:29	S'ils étaient sages, ils comprendraient ceci, et ils considéreraient leur fin.
32:30	Comment un seul en poursuivrait-il 1 000, et deux en mettraient-ils 10 000 en fuite, si leur Rocher ne les avait pas vendus, et si YHWH ne les avait pas enfermés ?
32:31	Car leur rocher n'est pas comme notre Rocher, nos ennemis en sont juges.
32:32	Car leur vigne est du plant de Sodome et des champs de Gomorrhe. Leurs raisins sont des raisins empoisonnés, leurs grappes sont amères.
32:33	Leur vin est un venin de dragon et du poison cruel de cobras.
32:34	Cela n'est-il pas caché près de moi, scellé dans mes trésors ?
32:35	À moi la vengeance et la rétribution<!--Ro. 12:19.-->, le temps où leur pied glissera ! Car le jour de leur calamité est près, et ce qui leur est préparé se hâte.
32:36	Car YHWH jugera son peuple et il se repentira en faveur de ses serviteurs, quand il verra que leur force a disparu et qu'il n'y a personne de retenu ni d'abandonné.
32:37	Et il dira : Où sont leurs elohîm, le rocher en qui ils se confiaient,
32:38	qui mangeaient la graisse de leurs sacrifices, qui buvaient le vin de leurs libations ? Qu'ils se lèvent, qu'ils vous aident, et qu'ils vous servent de refuge !
32:39	Voyez maintenant que c’est moi, moi, lui-même, et il n'y a pas d'elohîm avec moi<!--Ce verset confirme qu'Elohîm est un, puisqu'il n'y a pas d'autres elohîm à ses côtés.-->. C'est moi qui fais mourir et qui fais vivre, c'est moi qui blesse et qui guéris ! Et il n'y a personne qui délivre de ma main !
32:40	Oui, je lève ma main vers les cieux et je dis : Moi, le Vivant éternel,
32:41	si j'aiguise l'éclair de mon épée, et si ma main saisit la justice, je rendrai la vengeance à mes adversaires et je rétribuerai ceux qui me haïssent.
32:42	J'enivrerai mes flèches de sang et mon épée dévorera la chair, j'enivrerai mes flèches du sang des blessés mortellement et des captifs, de la tête des chefs de l'ennemi.
32:43	Nations, réjouissez-vous avec son peuple ! Car il venge le sang de ses serviteurs, il tire vengeance de ses ennemis, et fait la propitiation pour son sol, pour son peuple.

### Fin du cantique, invitation à demeurer fidèle

32:44	Moshé vint et déclara toutes les paroles de ce chant aux oreilles du peuple, lui et Hoshea fils de Noun.
32:45	Moshé ayant achevé de déclarer toutes ces paroles à tout Israël,
32:46	il leur dit : Mettez votre cœur à toutes les paroles au sujet desquelles je rends témoignage parmi vous en ce jour, pour les commander à vos fils, afin qu'ils prennent garde d'observer toutes les paroles de cette torah.
32:47	Car ce n'est pas une parole vaine pour vous, mais c'est votre vie. Par cette parole vous prolongerez vos jours sur le sol dont vous allez prendre possession après avoir passé le Yarden.

### Moshé, invité à monter sur le Mont Nebo

32:48	En ce même jour-là, YHWH parla à Moshé, en disant :
32:49	Monte sur cette montagne d'Abarim, sur le Mont Nebo, en terre de Moab, vis-à-vis de Yeriycho, et regarde la terre de Kena'ân que je donne en propriété aux fils d'Israël.
32:50	Tu mourras sur la montagne où tu vas monter, et tu seras recueilli vers tes peuples, comme Aaron, ton frère, est mort sur la montagne d'Hor, et a été recueilli vers ses peuples,
32:51	parce que vous avez commis un délit contre moi au milieu des fils d'Israël, aux eaux de Meriybah, à Qadesh, dans le désert de Tsin, car vous ne m'avez pas sanctifié au milieu des fils d'Israël.
32:52	Oui, tu verras la terre en face de toi, mais tu ne viendras pas là, sur la terre que je donne aux fils d'Israël.

## Chapitre 33

### Moshé bénit les tribus d'Israël

33:1	Et voici la bénédiction par laquelle Moshé, homme d'Elohîm, bénit les fils d'Israël, face à sa mort.
33:2	Il dit : YHWH est venu de Sinaï, il s'est levé sur eux de Séir, il a resplendi de la montagne de Paran, et il est venu avec des myriades de saints, et de sa droite sortait pour eux le feu du décret.
33:3	En effet, il aime les peuples. Tous ses saints sont dans ta main, ils se sont assemblés à tes pieds pour porter tes paroles.
33:4	Moshé nous a donné la torah, héritage de l'assemblée de Yaacov.
33:5	Il est devenu roi en Yeshouroun<!--Voir commentaire en De. 32:15.-->, quand les chefs du peuple se sont rassemblés ensemble, avec les tribus d'Israël.
33:6	Que Reouben vive et qu'il ne meure pas, et que ses hommes deviennent innombrables !
33:7	Voici ce qu'il dit sur Yéhouda : YHWH ! Écoute la voix de Yéhouda, et ramène-le vers son peuple. Que ses mains soient fortes, deviens son secours contre ses ennemis.
33:8	Sur Lévi il dit : Tes thoummim et tes ourim sont à l'homme fidèle que tu as éprouvé à Massah, avec qui tu as contesté aux eaux de Meriybah.
33:9	Il dit de son père et de sa mère : Je ne les ai pas vus ! Il ne reconnaît pas ses frères et ne connaît pas ses fils. Car ils gardent tes paroles et ils gardent ton alliance.
33:10	Ils enseignent tes ordonnances à Yaacov, et ta torah à Israël. Ils mettent l'encens sous tes narines, et l'holocauste sur ton autel.
33:11	YHWH, bénis sa force ! Agrée l'œuvre de ses mains ! Brise les reins de ceux qui s'élèvent contre lui, et que ceux qui le haïssent ne se relèvent plus !
33:12	Sur Benyamin il dit : Le bien-aimé de YHWH habitera en sécurité avec lui. Il le protégera toujours, et demeurera entre ses épaules.
33:13	Sur Yossef il dit : Sa terre est bénie par YHWH, de ce qu'il y a d'excellent aux cieux, de la rosée, et de l'abîme qui est en bas,
33:14	et d'excellents produits du soleil, d'excellents produits de la lune,
33:15	et du sommet des montagnes antiques, et de ce qu'il y a d'excellent des collines éternelles,
33:16	et de ce qu'il y a d'excellent de la terre et de sa plénitude. Que la grâce de celui qui demeura dans le buisson vienne sur la tête de Yossef, sur le sommet de la tête du Nazaréen d'entre ses frères !
33:17	Sa majesté est comme le premier-né de son bœuf. Ses cornes sont les cornes du taureau sauvage. Avec elles, il heurtera des peuples tous ensemble jusqu’aux extrémités de la Terre : Ce sont les myriades d'Éphraïm, ce sont les milliers de Menashè.
33:18	Sur Zebouloun il dit : Réjouis-toi, Zebouloun, dans ta sortie, et toi, Yissakar, dans tes tentes.
33:19	Ils appelleront les peuples sur la montagne. Là, ils sacrifieront des sacrifices de justice, car ils suceront l'abondance des mers, et les trésors cachés dans le sable.
33:20	Sur Gad il dit : Béni soit celui qui élargit Gad ! Il habite comme une lionne, et il déchire le bras et la tête.
33:21	Il a vu la première part, parce que c'était là qu'était cachée la portion du législateur. Il est venu en tête du peuple, il a exécuté la justice de YHWH et ses jugements envers Israël.
33:22	Sur Dan il dit : Dan est un jeune lion, il s'élance de Bashân.
33:23	Sur Nephthali il dit : Nephthali, rassasié de faveur et rempli de la bénédiction de YHWH, prends possession de l’ouest et du sud !
33:24	Sur Asher il dit : Asher sera béni entre les fils. Il sera agréable à ses frères, il trempera son pied dans l'huile.
33:25	Tes verrous seront de fer et de cuivre, et ta force durera autant que tes jours.
33:26	Nul n'est comme le El de Yeshouroun<!--Voir commentaire en De. 32:15.-->, porté sur les cieux pour te venir en aide, et sur les nuées dans sa majesté.
33:27	L'Elohîm des temps anciens<!--Vient de l'hébreu « qedem » qui signifie « est », « orient », « antiquité », « devant », « ce qui est devant », « le temps passé ».--> est ta demeure, et au-dessous de toi sont ses bras éternels. Car il a chassé de devant toi tes ennemis, et il a dit : Extermine !
33:28	Israël habitera en sécurité, la source de Yaacov est à part sur une terre de blé et de vin nouveau, et ses cieux distilleront la rosée.
33:29	Heureux es-tu, Israël ! Qui est comme toi, peuple sauvé par YHWH, le bouclier de ton secours et l'épée de ta majesté ? Tes ennemis seront trouvés menteurs devant toi, et tu fouleras de tes pieds leurs lieux élevés.

## Chapitre 34

### Moshé voit la terre de Kena'ân (Canaan) mais n'y entre pas

34:1	Moshé monta des régions arides de Moab sur le Mont Nebo, au sommet du Pisgah, vis-à-vis de Yeriycho. YHWH lui fit voir toute la terre de Galaad jusqu'à Dan,
34:2	tout Nephthali, la terre d'Éphraïm et de Menashè, toute la terre de Yéhouda jusqu'à la mer occidentale,
34:3	le sud, les environs du Yarden, la plaine de Yeriycho, la ville des palmiers, jusqu'à Tsoar.
34:4	YHWH lui dit : C'est ici la terre au sujet de laquelle j’ai juré à Abraham, à Yitzhak, et à Yaacov, en disant : Je la donnerai à ta postérité<!--Ge. 15.-->. Je te l'ai fait voir de tes yeux, mais tu n’y passeras pas.

### Mort de Moshé

34:5	Moshé, serviteur de YHWH, mourut là, sur la terre de Moab, selon la parole de YHWH.
34:6	Il l’enterra dans la vallée, en terre de Moab, vis-à-vis de Beth-Peor. Aucun homme n'a connu son sépulcre jusqu'à ce jour<!--Jud. 1:9.-->.
34:7	Or Moshé était fils de 120 ans quand il mourut. Son œil ne s’était pas affaibli et sa vigueur<!--« Humidité », « fraîcheur », « force naturelle ».--> ne s’était pas enfuie.
34:8	Les fils d'Israël pleurèrent Moshé pendant 30 jours dans les régions arides de Moab. Puis ces jours de pleurs et de deuil sur Moshé arrivèrent à leur terme.

### Yéhoshoua, successeur de Moshé

34:9	Et Yéhoshoua, fils de Noun, fut rempli de l'Esprit de sagesse, parce que Moshé lui avait imposé les mains<!--Jos. 1:9.-->. Les fils d'Israël lui obéirent et firent ce que YHWH avait ordonné à Moshé.
34:10	Et il ne s'est plus levé en Israël de prophète comme Moshé, que YHWH connaissait faces à faces,
34:11	selon tous les signes et les miracles que YHWH l'envoya faire en terre d'Égypte, devant pharaon, et tous ses serviteurs, et toute sa terre,
34:12	et selon toute cette main forte, et tous ces terribles prodiges, que Moshé fit sous les yeux de tout Israël.
