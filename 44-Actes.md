# Actes (Ac.)

Auteur : Loukas (Luc)

Thème : Les missions du 1er siècle

Date de rédaction : Env. 60 ap. J.-C.

D'origine grecque, Loukas fut l'auteur du livre communément appelé « Actes des apôtres » et de l'évangile éponyme, tous deux adressés à Theophilos (Théophile). Ce livre retrace la genèse de l'Assemblée, de l'ascension de Yéhoshoua à la pentecôte, de la prédication vivante et fructueuse de Petros (Pierre) à la conversion de Paulos (Paul), jusqu'au voyage de celui-ci à Rome en tant que prisonnier. On y découvre des apôtres déterminés, des ouvriers du Mashiah (Christ) qui acceptèrent de subir l'humiliation et la persécution par amour de la vérité. Sont également présentés des hommes et des femmes qui – touchés par la simplicité de l'Évangile du Royaume – se convertirent, puis se firent baptiser.

Bien plus qu'un recueil relatant de banales manifestations, ce livre est avant tout celui des actes du Saint-Esprit. Il témoigne de la résurrection et de la puissance de Yéhoshoua ha Mashiah (Jésus-Christ) manifestée au travers de son corps. Il retrace l'origine et le développement du premier réveil après Yéhoshoua ha Mashiah, qui fut un véritable bouleversement au sein de l'Empire romain en proie à l'impiété et à l'idolâtrie.

## Chapitre 1

### Introduction : Le Mashiah (Christ) ressuscité parle, pendant 40 jours, des choses qui concernent le Royaume d'Elohîm

1:1	En effet, j'ai fait le premier discours, ô Theophilos, concernant toutes les choses que Yéhoshoua commença de faire et d'enseigner,
1:2	jusqu'au jour où il fut enlevé, après avoir donné à travers l'Esprit Saint ses ordres aux apôtres qu'il avait élus.
1:3	Auxquels aussi, après avoir souffert, il se présenta lui-même vivant, avec beaucoup de preuves, leur apparaissant pendant 40 jours, et leur parlant des choses qui concernent le Royaume d'Elohîm.
1:4	Et étant assemblé avec eux, il leur donna l'ordre de ne pas partir de Yeroushalaim, mais d'attendre la promesse du Père, ce que vous avez entendu de moi.
1:5	Parce que Yohanan a en effet baptisé d'eau, mais vous, vous serez baptisés dans l'Esprit Saint, pas après de nombreux jours.
1:6	Eux donc, s'étant réunis en effet, l'interrogèrent en disant : Seigneur, est-ce en ce temps-ci<!--Chronos. Voir Ac. 1:7.--> que tu restaures à l'état initial le royaume d'Israël ?
1:7	Mais il leur dit : Ce n'est pas à vous de connaître les temps<!--Chronos : divinité du temps dans la mythologie grecque. Il est question du temps mécanique.--> et les mesures de temps<!--Kairos : le temps favorable et opportun.--> que le Père a fixés de sa propre autorité.

### La puissance du Saint-Esprit pour évangéliser les nations<!--Mt. 28:18-20 ; Mc. 16:15-18 ; Lu. 24:47-48 ; Jn. 20:21-22.-->

1:8	Mais vous recevrez la puissance du Saint-Esprit qui viendra sur vous, et vous serez mes témoins<!--Pr. 14:25.--> non seulement à Yeroushalaim, mais aussi dans toute la Judée et la Samarie, et jusqu'aux extrémités de la terre.
1:9	Et après qu'il eut dit ces choses, pendant qu'ils regardaient, il fut élevé, et une nuée le prit et l'emporta de devant leurs yeux.

### Promesse du retour de Yéhoshoua (Jésus)

1:10	Et comme ils avaient les yeux fixés vers le ciel pendant qu'il s'en allait, voici, deux hommes en vêtement blanc se présentèrent devant eux
1:11	et ils dirent : Hommes galiléens, pourquoi vous tenez-vous là regardant vers le ciel ? Ce Yéhoshoua qui a été enlevé au ciel du milieu de vous, viendra de la même manière que vous l'avez vu allant au ciel<!--Yéhoshoua ha Mashiah (Jésus-Christ) est monté au ciel depuis la Montagne des Oliviers. Lors de son retour, ses pieds se poseront sur cette même montagne. Voir Za. 14:4.-->.
1:12	Alors ils retournèrent à Yeroushalaim, de la montagne appelée la Montagne des Oliviers qui est près de Yeroushalaim, à la distance du chemin d'un jour de shabbat<!--Un chemin de shabbat est la distance qu'il est permis à un Juif de parcourir le jour du shabbat (Ex. 16:29). Elle correspond à 2 000 coudées ou 1 100 mètres. Voir le tableau « Mesures de longueurs ».-->.
1:13	Et quand ils furent entrés, ils montèrent dans une chambre haute où demeuraient Petros et Yaacov, et Yohanan et Andreas, Philippos et Thomas, Bar-Talmaï et Mattithyah, Yaacov, fils d'Alphaios et Shim’ôn le zélote, et Yéhouda, frère de Yaacov.
1:14	Tous ceux-ci, d'un commun accord, persévéraient dans la prière et dans la supplication avec les femmes, et Myriam, mère de Yéhoshoua, et avec ses frères.

### Matyah (Matthias) remplace Yéhouda (Juda) Iscariot dans le service

1:15	Et en ces jours-là, Petros se leva au milieu des disciples, qui étaient rassemblés au nombre d'environ 120 personnes, et il leur dit :
1:16	Hommes frères, il fallait que fût accomplie cette Écriture que le Saint-Esprit a prononcée d'avance par la bouche de David au sujet de Yéhouda, devenu le guide de ceux qui ont saisi Yéhoshoua.
1:17	Car il était compté avec nous et avait obtenu par tirage au sort le lot de ce service.
1:18	En effet, après avoir acquis une terre avec le salaire de l'injustice qui lui avait été donné, il est tombé la tête la première, s'est rompu par le milieu, et toutes ses entrailles ont été répandues.
1:19	Et la chose a été si connue de tous les habitants de Yeroushalaim, que cette terre a été appelée dans leur propre langue, Hakeldama, c'est-à-dire la terre du sang.
1:20	Car il est écrit dans le livre des Psaumes : Que sa demeure devienne déserte, et qu'il n'y ait personne qui habite en elle<!--Ps. 69:26.--> et qu'un autre prenne sa fonction de surveillant<!--Du grec « episkope », il s'agit de la fonction d'un ancien. Ps. 109:8.-->.
1:21	Il faut donc parmi les hommes qui nous ont accompagnés tout le temps que le Seigneur Yéhoshoua allait et venait avec nous,
1:22	en commençant depuis le baptême de Yohanan jusqu'au jour où il a été enlevé du milieu de nous, qu'il y en ait un qui soit témoin avec nous de sa résurrection.
1:23	Et ils en présentèrent deux : Yossef, appelé Barsabbas, surnommé Juste, et Matyah<!--Matthias.-->.
1:24	Et en priant, ils dirent : Toi, Seigneur, qui connais les cœurs de tous, désigne lequel de ces deux tu as choisi,
1:25	pour prendre le lot de ce service et de cet apostolat que Yéhouda a abandonné pour aller en son lieu.
1:26	Et ils jetèrent le sort sur eux, et le sort tomba sur Matyah, et il fut désigné par vote pour être avec les onze apôtres.

## Chapitre 2

### DÉBUT DE LA PÉRIODE DE LA NOUVELLE ALLIANCE


### Effusion de l'Esprit et naissance de l'Assemblée<!--Joë. 3:5.-->

2:1	Et comme le jour de la pentecôte s'accomplissait, ils étaient tous d'un commun accord dans un même lieu.
2:2	Et soudainement, il vint du ciel un bruit comme celui d'un vent qui souffle avec violence, et il remplit toute la maison où ils étaient assis.
2:3	Alors leur apparurent des langues divisées comme du feu et il s’en posa une sur chacun d’eux.
2:4	Et ils furent tous remplis du Saint-Esprit et commencèrent à parler en d'autres langues, selon que l'Esprit leur donnait de s'exprimer.
2:5	Or il y avait, demeurant à Yeroushalaim, des Juifs, hommes pieux de toutes les nations qui sont sous le ciel.
2:6	Et après que ce son eut lieu, la multitude vint ensemble et fut confondue de ce que chacun les entendait parler dans sa propre langue.
2:7	Ils étaient tous surpris et dans l'admiration, se disant les uns aux autres : Voici, tous ces gens qui parlent ne sont-ils pas galiléens ?
2:8	Et comment les entendons-nous, chacun dans la propre langue dans laquelle nous sommes nés ?
2:9	Parthes et Mèdes et Élamites, et ceux qui habitent la Mésopotamie, la Judée et la Cappadoce, le Pont et l'Asie,
2:10	la Phrygie et la Pamphylie, l'Égypte et le territoire de la Libye qui est près de Cyrène, et ceux qui sont venus de Rome, aussi bien Juifs que prosélytes,
2:11	Crétois et Arabes, nous les entendons parler en nos langues des merveilles d'Elohîm !
2:12	Or tous étaient étonnés et en perplexité, se disant l'un à l'autre : Qu'est-ce que cela veut dire ?
2:13	Mais d'autres, se moquant, disaient : Ils sont pleins de vin doux !

### Prédication de Petros (Pierre)

2:14	Mais Petros se présentant avec les onze, éleva sa voix et leur dit : Hommes juifs et vous tous qui habitez à Yeroushalaim, que ceci vous soit connu et que vos oreilles reçoivent mes paroles !
2:15	Car ceux-ci ne sont pas ivres comme vous le supposez, car c'est la troisième heure<!--9 heures du matin.--> du jour.
2:16	Mais c'est ici ce qui a été dit par le moyen du prophète Yoel :
2:17	Et il arrivera dans les derniers jours<!--Yaacov est le premier homme à avoir utilisé l'expression « derniers jours ». Ge. 49:1-2 ; Da. 2:28 ; Ja. 5:3 ; 2 Ti. 3:1 ; 1 P. 1:5 et 20 ; 2 P. 3:3 ; Hé. 1:2 ; Jud. 1:18 ; 1 Jn. 2:18. Voir la résurrection des saints. Jn. 6:39-40, 44 et 54 ; Jn. 11:24. Le dernier correspond aussi au jugement dernier Jn. 12:48.-->, dit Elohîm, que je répandrai de mon Esprit sur toute chair, et vos fils et vos filles prophétiseront, et vos jeunes gens auront des visions, et vos vieillards auront des rêves.
2:18	Et même en ces jours-là, je répandrai de mon Esprit sur mes esclaves, hommes et femmes, et ils prophétiseront.
2:19	Et je donnerai des prodiges en haut dans le ciel et des signes en bas sur la Terre : du sang et du feu et une vapeur de fumée.
2:20	Le soleil se changera en ténèbre et la lune en sang, avant que le grand et notable jour du Seigneur vienne.
2:21	Et il arrivera que quiconque invoquera le Nom du Seigneur sera sauvé<!--Joë. 3.-->.

### Proclamation de la résurrection du Mashiah

2:22	Hommes israélites, écoutez ces paroles ! Yéhoshoua, le Nazaréen, homme désigné d'Elohîm parmi vous, par les miracles, les prodiges et les signes, qu'Elohîm a produits par lui au milieu de vous, comme vous-mêmes vous le savez :
2:23	Celui-ci, livré selon le conseil déterminé et selon la prescience d'Elohîm, vous l'avez pris, et vous l'avez fait mourir, l'ayant attaché par le moyen des mains des violeurs de la torah<!--Voir Mc. 15:28.-->.
2:24	Elohîm l'a relevé, ayant rompu les douleurs d'enfantement de la mort, parce qu'il n'était pas possible qu'il soit retenu par elle.
2:25	Car David dit de lui : Je voyais d'avance le Seigneur devant moi continuellement, parce qu'il est à ma droite, afin que je ne sois pas ébranlé<!--Ps. 16:8-11.-->.
2:26	C'est pourquoi, mon cœur s'est réjoui et ma langue a exulté. Et même encore, ma chair demeurera sur l'espérance.
2:27	Car tu n'abandonneras pas mon âme dans le Hadès<!--Voir commentaire en Mt. 16:18.--> et tu ne permettras pas que ton Saint voie la corruption.
2:28	Tu m'as fait connaître le chemin de la vie, tu me rempliras de joie dans ta présence<!--Ps. 16:11.-->.
2:29	Hommes frères, qu'il soit légal pour moi de vous parler avec liberté au sujet du patriarche David, et qu'il a fini, et qu'il a été enseveli, et que son sépulcre est au milieu de nous jusqu'à ce jour.
2:30	Étant donc prophète, et sachant qu'Elohîm lui avait promis avec serment que, du fruit de ses reins, il suscitera selon la chair le Mashiah, pour le faire asseoir sur son trône,
2:31	prévoyant cela, il a dit, concernant la résurrection du Mashiah, que son âme n'a pas été abandonnée dans le Hadès et que sa chair n'a pas vu la corruption.
2:32	Elohîm a relevé ce Yéhoshoua, nous en sommes tous témoins.
2:33	Élevé donc à la droite d'Elohîm et ayant reçu du Père la promesse du Saint-Esprit, il a répandu ce que vous voyez et entendez maintenant.
2:34	Car David n'est pas monté dans les cieux, mais lui-même dit : Le Seigneur a dit à mon Seigneur : Assieds-toi à ma droite,
2:35	jusqu'à ce que j'aie mis tes ennemis pour le marchepied de tes pieds<!--Ps. 110:1.-->.
2:36	Que toute la maison d'Israël sache donc avec certitude qu'Elohîm l'a fait et Seigneur et Mashiah, ce Yéhoshoua que vous avez crucifié.

### Exhortation à la repentance

2:37	Or, ayant entendu ces choses, ils eurent le cœur transpercé et ils dirent à Petros et aux autres apôtres : Hommes frères, que ferons-nous ?
2:38	Et Petros leur dit : Repentez-vous et que chacun de vous soit baptisé au Nom de Yéhoshoua Mashiah pour le pardon des péchés, et vous recevrez le don du Saint-Esprit.
2:39	Car la promesse est pour vous, et pour vos enfants et pour tous ceux qui sont au loin, en aussi grand nombre que le Seigneur notre Elohîm les appellera.
2:40	Et par beaucoup d'autres paroles, il rendait témoignage et les exhortait en disant : Sauvez-vous de cette génération tordue !

### Conversion et baptême de 3 000 personnes

2:41	Ceux donc qui acceptèrent<!--Le verbe « accepter » vient du grec « apodéchomai » qui signifie « accepter de, recevoir » ou « accepter ce qui est offert ».--> sa parole avec joie furent en effet baptisés. Et environ 3 000 âmes furent ajoutées ce jour-là.
2:42	Et ils persévéraient tous dans la doctrine des apôtres, dans la communion fraternelle, dans la fraction du pain et dans les prières.
2:43	Et toute âme avait de la crainte, et beaucoup de prodiges et de signes se produisaient par le moyen des apôtres.
2:44	Mais tous ceux qui croyaient étaient ensemble dans le même lieu, et ils avaient toutes choses communes.
2:45	Et ils vendaient leurs propriétés et leurs possessions, et les distribuaient à tous selon que chacun en avait besoin.
2:46	Chaque jour, persévérant d'un commun accord dans le temple et rompant le pain dans chaque maison, ils prenaient leur repas avec allégresse<!--Joie extrême. Aux fêtes, le peuple était oint avec « l'huile d'allégresse ». Dans Hébreux 1:9, l'auteur de cette lettre fait allusion à cette cérémonie, et l'utilise comme emblème de la puissance divine et de la majesté à laquelle Yéhoshoua a été élevé.--> et simplicité de cœur.
2:47	Louant Elohîm et trouvant grâce auprès de tout le peuple. Et le Seigneur ajoutait chaque jour à l'Assemblée ceux qui sont sauvés.

## Chapitre 3

### Guérison d'un homme boiteux de naissance

3:1	Or Petros et Yohanan montaient ensemble au temple, à l'heure de la prière, la neuvième heure.
3:2	Et l'on portait un certain homme qui était boiteux dès le ventre de sa mère, et on le plaçait chaque jour vers la porte du temple, appelée la Belle, pour demander l'aumône à ceux qui entraient dans le temple.
3:3	Lui, voyant Petros et Yohanan qui étaient sur le point d'entrer dans le temple, les supplia pour recevoir l'aumône.
3:4	Mais Petros, de même que Yohanan, fixa les yeux sur lui et lui dit : Regarde-nous !
3:5	Et il les regardait attentivement, s'attendant à recevoir d'eux quelque chose.
3:6	Mais Petros lui dit : Je n'ai ni argent, ni or, mais ce que j'ai, je te le donne : Au Nom de Yéhoshoua Mashiah, le Nazaréen, lève-toi et marche !
3:7	Et le saisissant par la main droite, il le fit lever. Et immédiatement, les plantes et les chevilles de ses pieds devinrent fermes.
3:8	Et sautant, il se tint debout et il marchait. Et il entra avec eux dans le temple, marchant, sautant et louant Elohîm.
3:9	Et tout le peuple le vit marchant et louant Elohîm.
3:10	Et reconnaissant que c'était celui-là même qui était assis vers la Belle porte du temple pour l'aumône, ils furent remplis d'admiration et d'étonnement de ce qui lui était arrivé.
3:11	Et comme le boiteux qui avait été guéri tenait par la main Petros et Yohanan, tout le peuple étonné, se précipita vers eux au portique qu'on appelle de Shelomoh.

### Yéhoshoua, le Mashiah annoncé par les prophètes

3:12	Mais Petros voyant cela, dit au peuple : Hommes israélites, pourquoi vous étonnez-vous de cela ? Ou pourquoi avez-vous les yeux fixés sur nous, comme si c'était par notre propre puissance ou notre piété que nous l'avons fait marcher ?
3:13	L'Elohîm d'Abraham, et de Yitzhak et de Yaacov, l'Elohîm de nos pères, a glorifié son Fils Yéhoshoua, que vous avez livré et renié devant Pilate, quoiqu'il jugeât qu'il devait être relâché.
3:14	Mais vous, vous avez renié le Saint<!--Yéhoshoua est le Saint dont Iyov (Job) n'avait pas transgressé la loi (Job 6:10). Voir Mc. 1:24 ; Lu. 1:35, 4:34 ; Es. 1:4, 5:16-24, 10:20, 12:6, 17:7, 29:19-23, 30:11-15, 31:1, 37:23, 40:25, 41:14-20, 43:3,14-15, 45:11, 47:4, 48:17, 49:7, 54:5, 57:15 ; Jé. 50:29, 51:5 ; Ez. 39:7 ; Os. 11:9 ; Ha. 3:3 ; Ps. 78:41, 89:19.--> et le Juste, et vous avez demandé qu'on vous accorde la grâce d'un homme qui était un meurtrier.
3:15	Mais vous avez fait mourir le Prince de la vie qu'Elohîm a réveillé d'entre les morts, ce dont nous sommes témoins.
3:16	Et c'est par la foi en son Nom, que son Nom a raffermi celui que vous voyez et connaissez. Et la foi qui est par lui a donné à celui-ci cette parfaite santé en présence de vous tous.
3:17	Et maintenant, frères, je sais que vous avez agi par ignorance, de même que vos chefs.
3:18	Mais Elohîm a ainsi accompli ce qu’il avait d’avance annoncé par la bouche de tous ses prophètes, que le Mashiah devait souffrir<!--Es. 53.-->.
3:19	Repentez-vous donc et convertissez-vous pour que vos péchés soient effacés ! Afin que des temps de rafraîchissement viennent par la présence du Seigneur,
3:20	et qu'il envoie celui qui vous a été annoncé d'avance, Yéhoshoua Mashiah,
3:21	que le ciel doit en effet recevoir jusqu'au temps du rétablissement de toutes choses, dont Elohîm a parlé par la bouche de tous ses saints prophètes de tout âge<!--Du grec « aion » qui signifie « période », « temps », etc.-->.
3:22	Car Moshé a en effet dit aux pères : Le Seigneur, votre Elohîm, vous suscitera d'entre vos frères un Prophète comme moi : c'est lui que vous écouterez dans tout ce qu'il vous dira,
3:23	et il arrivera que toute âme qui n'écoutera pas ce Prophète, sera exterminée du milieu du peuple<!--De. 18:15-19.-->.
3:24	Et même tous les prophètes qui ont successivement parlé depuis Shemouél ont aussi annoncé d'avance ces jours-là.
3:25	Vous, vous êtes les fils des prophètes et de l'alliance, le testament<!--Hé. 8:10, 9:16-17.--> qu'Elohîm a fait avec nos pères, en disant à Abraham : Toutes les familles de la Terre seront bénies en ta postérité<!--Ge. 12:2.-->.
3:26	C'est à vous premièrement qu'Elohîm, ayant suscité son Fils Yéhoshoua, l'a envoyé pour vous bénir, en détournant chacun de vous de ses méchancetés.

## Chapitre 4

### Première persécution de l'Assemblée : Petros (Pierre) et Yohanan (Jean) emprisonnés

4:1	Mais comme ils parlaient au peuple, survinrent les prêtres, le strategos du temple et les sadducéens,
4:2	étant offensés de ce qu'ils enseignaient le peuple et qu'ils annonçaient la résurrection des morts en Yéhoshoua.
4:3	Et ils jetèrent les mains sur eux et les mirent en prison jusqu'au lendemain, car c'était déjà le soir.
4:4	Et beaucoup de ceux qui avaient entendu la parole crurent, et le nombre des hommes arriva à environ 5 000.

### Petros et Yohanan comparaissent devant le sanhédrin

4:5	Or, il arriva que le lendemain, leurs chefs, et leurs anciens et leurs scribes se rassemblèrent à Yeroushalaim,
4:6	avec Chananyah, le grand-prêtre, Kaïaphas, Yohanan, Alexandros, et tous ceux qui étaient de la race des principaux prêtres.
4:7	Et les ayant placés au milieu d’eux, ils leur demandèrent : Par quelle puissance ou par quel nom avez-vous fait cela ?
4:8	Alors Petros étant rempli du Saint-Esprit, leur dit : Chefs du peuple et anciens d'Israël,
4:9	si nous sommes jugés aujourd'hui sur un bienfait accordé à un homme infirme et sur la manière dont il a été sauvé,
4:10	qu'il soit connu de vous tous et de tout le peuple d'Israël, que c'est par le Nom de Yéhoshoua Mashiah le Nazaréen, que vous avez crucifié et qu'Elohîm a réveillé des morts, c'est par lui que celui-ci se présente en bonne santé devant vous.
4:11	C'est lui, la pierre méprisée par vous qui bâtissez, qui est devenue la tête de l'angle<!--Ps. 118:22.-->.
4:12	Et il n'y a de salut<!--Es. 12:2.--> en aucun autre, car il n'y a sous le ciel aucun autre Nom, donné aux humains, par lequel il nous faille être sauvés.

### Le sanhédrin interdit aux apôtres de prêcher au Nom de Yéhoshoua (Jésus)

4:13	Mais, voyant l'assurance de Petros et de Yohanan, et ayant compris que c'étaient des hommes illettrés et ignorants, ils étaient dans l'étonnement et ils les reconnaissaient pour avoir été avec Yéhoshoua.
4:14	Et voyant là, debout avec eux, l'homme qui avait été guéri, ils n'avaient rien à opposer.
4:15	Mais ils leur ordonnèrent de sortir hors du sanhédrin et ils délibérèrent entre eux, 
4:16	en disant : Que ferons-nous à ces hommes ? Car il est manifeste pour tous les habitants de Yeroushalaim, qu'un signe a en effet été accompli par leur moyen, et cela est si évident que nous ne pouvons le nier.
4:17	Mais afin que cela ne se répande pas davantage parmi le peuple, défendons-leur avec de grandes menaces de parler encore en ce nom-là, à qui que ce soit parmi les humains.
4:18	Et les ayant donc appelés, ils leur ordonnèrent de ne plus parler ni d'enseigner en aucune manière au nom de Yéhoshoua.
4:19	Mais Petros et Yohanan leur répondirent et dirent : Jugez s'il est juste devant Elohîm de vous obéir plutôt qu'à Elohîm.
4:20	Car nous ne pouvons pas ne pas parler de ce que nous avons vu et entendu.
4:21	Mais eux, en y joignant des menaces, les relâchèrent, ne trouvant pas de quelle manière les punir, à cause du peuple, parce que tous glorifiaient Elohîm de ce qui était arrivé,
4:22	car l'homme en qui cette guérison, ce signe, avait été accompli, avait plus de 40 ans.

### L'Assemblée demande l'assistance d'Elohîm

4:23	Et après avoir été relâchés, ils allèrent vers les leurs, et racontèrent tout ce que les principaux prêtres et les anciens leur avaient dit.
4:24	Et lorsqu'ils eurent entendu cela, ils élevèrent d'un commun accord leur voix vers Elohîm et dirent : Maître<!--Yéhoshoua est le seul Maître. Voir Jud. 1:4 ainsi que Lu. 2:29 ; 2 P. 2:1 et Ap. 6:10.-->, tu es l'Elohîm qui as fait le ciel et la Terre, la mer et toutes les choses qui y sont,
4:25	et qui as dit par la bouche de David, ton serviteur : Pourquoi les nations ont-elles henni et les peuples ont-ils médité<!--Chez les Grecs : réflexion méditative associée à la pratique des orateurs et spécialistes de la rhétorique.--> des choses vaines ?
4:26	Les rois de la Terre se sont présentés et les princes se sont assemblés contre le Seigneur et contre son Mashiah<!--Ps. 2:1-2.-->.
4:27	Car, en vérité, contre ton Saint Fils Yéhoshoua, que tu as oint, se sont assemblés Hérode et Ponce Pilate, avec les nations et le peuple d'Israël,
4:28	pour faire toutes les choses que ta main et ton conseil avaient d'avance déterminé devoir arriver.
4:29	Et maintenant, Seigneur, regarde à leurs menaces et donne à tes esclaves d'annoncer ta parole avec toute assurance,
4:30	en étendant ta main pour la guérison et pour que des signes et des prodiges surviennent par le Nom de ton Saint Fils Yéhoshoua.
4:31	Et quand ils eurent prié, le lieu où ils étaient assemblés trembla. Et ils furent tous remplis du Saint-Esprit et ils annonçaient la parole d'Elohîm avec assurance.

### La multitude des croyants unie comme un seul corps<!--Ac. 2:42-47.-->

4:32	Or, la multitude de ceux qui avaient cru était un seul cœur et une seule âme. Et pas un d'eux ne disait que ses biens lui appartenaient en propre, mais toutes choses étaient communes entre eux.
4:33	Et les apôtres rendaient témoignage avec une grande force de la résurrection du Seigneur Yéhoshoua, et une grande grâce était sur eux tous.
4:34	Car il n'y avait parmi eux aucun indigent, car tous ceux qui étaient possesseurs de terres ou de maisons les vendaient et ils apportaient le prix des choses vendues,
4:35	et le mettaient aux pieds des apôtres, et on le distribuait à chacun selon qu'il en avait besoin.
4:36	Or Yossef, surnommé par les apôtres Barnabas, ce qui, interprété, est : fils de consolation, Lévite, de nationalité chypriote,
4:37	ayant un champs, le vendit, en apporta le prix et le mit aux pieds des apôtres.

## Chapitre 5

### Mensonge de Chananyah (Ananias) et Saphira

5:1	Mais un homme du nom de Chananyah, avec Saphira sa femme, vendit une propriété,
5:2	et, du consentement de sa femme, il détourna quelque chose du prix et, en ayant apporté une partie, il la mit aux pieds des apôtres.
5:3	Mais Petros lui dit : Chananyah, en-raison-de quoi Satan a-t-il rempli ton cœur pour mentir au Saint-Esprit et détourner quelque chose du prix de la terre ?
5:4	Si tu l’avais gardée, ne te restait-elle pas ? Et, vendue, n'était-elle pas en ton pouvoir ? Comment as-tu pu mettre en ton cœur un pareil dessein ? Ce n'est pas à des humains que tu as menti, mais à Elohîm !
5:5	Et en entendant ces paroles, Chananyah tomba et expira. Et il y eut une grande crainte sur tous ceux qui entendirent ces choses.
5:6	Et les jeunes gens s'étant levés, l'enveloppèrent de bandages et, l'ayant emporté dehors, ils l'ensevelirent.
5:7	Or il arriva, à un intervalle d'environ trois heures, que sa femme, ne sachant pas ce qui était arrivé, entra.
5:8	Et Petros lui répondit : Dis-moi, la terre que vous avez vendue, c’était tant ? Elle dit : Oui, tant.
5:9	Mais Petros lui dit : Comment vous êtes-vous accordés ensemble pour tenter l'Esprit du Seigneur ? Voici que les pieds de ceux qui ont enseveli ton mari sont à la porte et ils t'emporteront.
5:10	Et au même instant, elle tomba à ses pieds et expira. Et quand les jeunes hommes furent entrés, ils la trouvèrent morte, ils l'emportèrent dehors et l'ensevelirent auprès de son mari.
5:11	Et une grande crainte vint sur toute l'assemblée et sur tous ceux qui entendirent ces choses.

### Miracles et conversions à Yeroushalaim (Jérusalem)

5:12	Et beaucoup de signes et de prodiges se produisaient dans le peuple par les mains des apôtres. Et ils étaient tous d'un commun accord dans le portique de Shelomoh.
5:13	Mais aucun des autres n'osait se joindre à eux, mais le peuple les magnifiait.
5:14	Et de plus en plus de croyants se joignaient au Seigneur, des multitudes, tant d'hommes que de femmes.
5:15	Et on apportait les malades dans les rues, et on les mettait sur des lits de camp et sur des couchettes, pour que Petros en venant, au moins son ombre<!--Vient du grec « episkiazo » qui signifie : « jeter une ombre sur », « envelopper d'une ombre », « ombrager », « obscurcir ». Depuis une nuée vaporeuse qui enveloppe d'une ombre, la parole est transférée en une nuée brillante qui entoure et enveloppe les personnes de brillance. Mot utilisé pour le Saint-Esprit exerçant son énergie créatrice au sein de Myriam et l'en imprégnant. L'usage de ce mot semble provenir de l'idée familière de l'ancienne alliance où une nuée symbolisait la présence et la puissance d'Elohîm. Voir Mt. 17:5 ; Mc. 9:7 ; Lu. 1:35 et 9:34.--> passe sur quelqu'un d'eux.
5:16	Et la multitude accourait aussi des villes voisines à Yeroushalaim, amenant des malades et ceux qui étaient tourmentés par des esprits impurs, et tous étaient guéris.

### Deuxième persécution de l'Assemblée : Les apôtres en prison puis devant le sanhédrin

5:17	Mais le grand-prêtre s'étant levé, ainsi que tous ceux qui étaient avec lui, c'est-à-dire la secte des sadducéens, furent remplis de jalousie,
5:18	et ils jetèrent les mains sur les apôtres et les mirent dans la prison publique.
5:19	Mais un ange du Seigneur ouvrit pendant la nuit les portes de la prison, les conduisit dehors et leur dit :
5:20	Allez et présentez-vous dans le temple, annoncez au peuple toutes les paroles de cette vie.
5:21	Et lorsqu'ils entendirent cela, ils entrèrent dans le temple tôt le matin, et ils enseignaient. Mais le grand-prêtre et ceux qui étaient avec lui étant arrivés, ils convoquèrent le sanhédrin et tout le conseil des anciens des fils d'Israël, et ils envoyèrent les chercher à la prison.
5:22	Mais les huissiers, à leur arrivée, ne les trouvèrent pas dans la prison. Ils retournèrent et firent leur rapport,
5:23	en disant : Nous avons en effet trouvé la prison fermée avec toute sûreté, et à l'extérieur, les gardes se tenant debout devant les portes, mais, ayant ouvert, nous n'avons trouvé personne dedans.
5:24	Mais le grand-prêtre, le strategos du temple et les principaux prêtres, ayant entendu ces paroles, furent en perplexité à leur sujet, ne sachant ce que cela deviendrait.
5:25	Mais survint quelqu'un qui leur annonça, disant : Voici que les hommes que vous avez mis en prison sont dans le temple. Ils se tiennent là et enseignent le peuple !
5:26	Alors le strategos du temple partit avec les huissiers et il les conduisit sans violence, car ils avaient peur d'être lapidés par le peuple.
5:27	Mais après qu'ils les eurent amenés, ils les présentèrent au sanhédrin. Et le grand-prêtre les interrogea,
5:28	en disant : Ne vous avions-nous pas ordonné expressément de ne plus enseigner en ce nom-là ? Et voici, vous avez rempli Yeroushalaim de votre doctrine et vous voulez faire venir sur nous le sang de cet homme !
5:29	Mais Petros et les autres apôtres répondant, dirent : Il faut obéir à Elohîm plutôt qu'aux humains.
5:30	L'Elohîm de nos pères a réveillé Yéhoshoua, que vous avez tué de vos mains en le suspendant au bois.
5:31	Elohîm l'a élevé par sa puissance pour être Prince et Sauveur, afin de donner à Israël la repentance et le pardon des péchés.
5:32	Et nous sommes nous-mêmes les témoins de ce que nous vous disons, de même que le Saint-Esprit qu'Elohîm a donné à ceux qui lui obéissent.
5:33	Mais ayant entendu cela, ils étaient sciés en deux et délibéraient de les tuer.

### Parole de sagesse de Gamaliel

5:34	Mais un pharisien du nom de Gamaliel<!--« Récompense de El ». Voir No. 1:10, 2:20.-->, docteur de la torah, honoré de tout le peuple, se leva dans le sanhédrin et ordonna de faire sortir un instant les apôtres.
5:35	Et il leur dit : Hommes israélites, prenez garde à vous-mêmes, sur ce que vous êtes sur le point de faire à l'égard de ces hommes-là.
5:36	Car avant ces jours-ci s'éleva Theudas, se disant être quelqu'un, et auquel se joignit étroitement un nombre d'environ 400 hommes. Il fut tué, et tous ceux qui s'étaient laissés persuader par lui ont été éparpillés et réduits à rien.
5:37	Après lui se leva Yéhouda le Galiléen, aux jours du recensement, et il entraîna un assez grand peuple à la révolte derrière lui. Il périt lui aussi, et tous ceux qui s'étaient laissés persuader par lui ont été dispersés.
5:38	Et maintenant je vous dis : Ne continuez plus vos poursuites contre ces hommes et laissez-les. Car si ce dessein ou cette œuvre vient des humains, elle sera détruite.
5:39	Mais si elle vient d'Elohîm, vous ne pourrez pas la détruire, de peur que vous ne soyez même trouvés en guerre contre Elohîm !
5:40	Et ils furent de son avis. Ayant appelé les apôtres, ils les battirent et leur ordonnèrent de ne plus parler au Nom de Yéhoshoua et ils les relâchèrent.

### Persécutés, les apôtres continuent à annoncer l'Évangile

5:41	Eux donc se retirèrent en effet de devant le sanhédrin, se réjouissant d'avoir été jugés dignes d'être déshonorés en faveur de son Nom.
5:42	Et chaque jour, ils ne cessaient d'enseigner et d'annoncer dans le temple et à la maison Yéhoshoua, le Mashiah.

## Chapitre 6

### Sept hommes élus pour le service

6:1	Or, en ces jours-là, les disciples se multipliant, il s'éleva un murmure des Hellénistes<!--Les Hellénistes étaient des Juifs issus de la diaspora ayant adopté la culture et la langue grecque.--> contre les Hébreux, parce que leurs veuves étaient négligées dans le service quotidien.
6:2	Mais les douze, ayant convoqué la multitude des disciples, leur dirent : Il n'est pas convenable que nous laissions la parole d'Elohîm pour servir aux tables.
6:3	Regardez donc, frères, pour choisir sept hommes parmi vous, de qui l'on rende un témoignage honorable, remplis de l’Esprit-Saint et de sagesse, auxquels nous confierons ce devoir.
6:4	Mais nous, nous persévérerons dans la prière et dans le service de la parole.
6:5	Et ce discours plut à toute la multitude qui était là présente. Et ils élurent Stephanos<!--Étienne.-->, homme plein de foi et du Saint-Esprit, et Philippos, et Prochoros, et Nicanor, et Timon, et Parménas, et Nikolaos, prosélyte<!--« Un nouveau venu », « un étranger », « celui qui vient d'une autre religion au Judaïsme ». Les rabbins considéraient deux catégories de prosélytes : a) les prosélytes de la justice, qui adhéraient à la circoncision, au baptême, offraient des sacrifices, pratiquaient le Judaïsme intégral b) les prosélytes de la porte, moins avancés, suivaient les sept préceptes de Noah, mais ne se soumettaient ni à la circoncision, ni à l'ensemble des ordonnances juives : idolâtrie, blasphème contre Elohîm, homicide, impudicité, vol ou pillage, rébellion contre les gouvernants, et usage de la viande avec le sang.--> d'Antioche<!--Capitale de la Syrie, située sur le fleuve Oronte, fondée en 300 av. J.-C. et ainsi nommée en l'honneur de son fondateur Antiochos. De nombreux Juifs-Grecs y vivaient.-->,
6:6	qu'ils placèrent devant les apôtres et, après avoir prié, ils leur imposèrent les mains.
6:7	Et la parole d'Elohîm croissait et le nombre des disciples se multipliait beaucoup à Yeroushalaim, et une grande foule de prêtres obéissaient à la foi.
6:8	Or, Stephanos, plein de foi et de puissance, produisait de grands prodiges et de grands signes parmi le peuple.

### Troisième persécution de l'Assemblée : Stephanos (Étienne) comparaît devant le sanhédrin

6:9	Mais quelques-uns de la synagogue dite des Affranchis<!--Affranchis : du grec « libertinos », c'est-à-dire « libertins » : hommes libres. Fraction de la communauté juive qui avait sa propre synagogue à Yeroushalaim (Jérusalem). Probablement des Juifs qui avaient été faits prisonniers par Pompée (106 – 48 av. J.-C.) et d'autres généraux romains, et avaient été déportés à Rome, puis libérés.-->, et des Cyrénéens et des Alexandrins, et avec ceux de Cilicie et d'Asie, s'élevèrent, disputant contre Stephanos.
6:10	Mais ils ne pouvaient pas résister à la sagesse et à l'Esprit par lequel il parlait.
6:11	Alors ils soudoyèrent des hommes qui dirent : Nous l'avons entendu prononcer des paroles blasphématoires contre Moshé et contre Elohîm.
6:12	Et ils soulevèrent le peuple, et les anciens et les scribes, et se jetant sur lui, ils le saisirent par la force et l'emmenèrent au sanhédrin.
6:13	Et ils présentèrent de faux témoins qui dirent : Cet homme ne cesse de prononcer des paroles blasphématoires contre ce saint lieu et contre la torah.
6:14	Car nous l'avons entendu dire que Yéhoshoua, ce Nazaréen, détruira ce lieu-ci et changera les coutumes que Moshé nous a données.
6:15	Et tous ceux qui étaient assis dans le sanhédrin ayant fixé les yeux sur lui, virent son visage comme le visage d’un ange.

## Chapitre 7

### Discours de Stephanos (Étienne) devant le sanhédrin

7:1	Et le grand-prêtre dit : Ces choses sont-elles ainsi ?
7:2	Mais il répondit : Hommes frères et pères, écoutez ! L'Elohîm de gloire apparut à notre père Abraham, lorsqu'il était en Mésopotamie, avant qu'il s'établisse à Charan, 
7:3	et il lui dit : Sors de ta terre et de ta famille, et viens vers la terre que je te montrerai.
7:4	Alors, étant sorti de la terre des Chaldéens, il habita à Charan. De là, après la mort de son père, il le fit passer vers cette terre où vous habitez maintenant.
7:5	Et il ne lui donna là aucun héritage, pas même l'espace couvert par un pied, et il lui a promis de le lui donner en possession et à sa postérité après lui, alors qu’il n’avait pas encore d’enfant.
7:6	Mais Elohîm parla ainsi : Sa postérité sera étrangère sur une terre étrangère, et on la réduira en esclavage et on la maltraitera pendant 400 ans.
7:7	Mais la nation dont ils auront été les esclaves, c'est moi qui la jugerai, dit Elohîm. Et après cela, ils sortiront et me serviront en ce lieu-ci<!--Ge. 15:13-14.-->.
7:8	Il lui donna aussi l'alliance de la circoncision et c’est ainsi qu’ayant engendré Yitzhak, il le circoncit le huitième jour, et Yitzhak, Yaacov, et Yaacov les douze patriarches.
7:9	Et les patriarches, émus d'envie, vendirent Yossef pour l'Égypte. Mais Elohîm était avec lui,
7:10	et il l'arracha à toutes ses tribulations et lui donna grâce et sagesse devant pharaon, roi d'Égypte. Il l'établit gouverneur sur l'Égypte et sur toute sa maison.
7:11	Or, il survint dans toute la terre d'Égypte et dans celle de Kena'ân, une famine et une grande tribulation, et nos pères ne trouvaient pas de vivres.
7:12	Mais Yaacov apprit qu'il y avait du blé en Égypte, il y envoya une première fois nos pères.
7:13	Et la seconde fois, Yossef fut reconnu par ses frères, et la famille de Yossef fut déclarée à pharaon.
7:14	Et Yossef envoya chercher Yaacov, son père, et toute sa famille avec 75 âmes.
7:15	Et Yaacov descendit en Égypte et il y finit ses jours ainsi que nos pères.
7:16	Et ils furent transportés à Shekem<!--Généralement traduit par Sichem.--> et mis dans le sépulcre qu'Abraham avait acheté à prix d'argent des fils de Hamor, à Shekem.
7:17	Mais comme approchait le temps de la promesse qu'Elohîm avait jurée à Abraham, le peuple s'accrut et se multiplia en Égypte,
7:18	jusqu'à ce qu’il se leva un autre roi qui n'avait pas connu Yossef.
7:19	Celui-ci, usant d'artifice contre notre race, maltraita nos pères, jusqu'à leur faire exposer leurs bébés pour qu'ils ne soient pas préservés vivants.
7:20	En ce temps-là, naquit Moshé. Et il était beau pour Elohîm. Et il fut nourri trois mois dans la maison de son père.
7:21	Mais ayant été exposé, la fille de pharaon le prit pour elle-même et l'éleva comme son fils.
7:22	Et Moshé fut instruit dans toute la sagesse des Égyptiens, et il était puissant en paroles et en œuvres.
7:23	Mais quand s’accomplissait le temps de sa quarantième année, il lui monta au cœur de visiter ses frères, les fils d'Israël.
7:24	Et en ayant vu un à qui l'on faisait du mal, il prit sa défense et vengea celui qui était opprimé, en frappant l'Égyptien.
7:25	Or il pensait que ses frères comprendraient qu'Elohîm leur donnait le salut par son entremise, mais ils ne le comprirent pas.
7:26	Le jour suivant, il parut au milieu d'eux alors qu'ils se battaient et il les exhorta à la paix, en disant : Hommes, vous êtes frères ! Pourquoi vous faites-vous du mal l'un à l'autre ?
7:27	Mais celui qui faisait du mal à son prochain le repoussa, en disant : Qui t'a établi prince et juge sur nous ?
7:28	Veux-tu me tuer, comme tu as tué hier l'Égyptien ?
7:29	Et à cette parole, Moshé s’est enfui et est devenu un étranger en terre de Madian, où il engendra deux fils.
7:30	Et 40 ans s'étant écoulés, un ange du Seigneur lui apparut dans le désert de la montagne de Sinaï, dans la flamme d'un buisson en feu.
7:31	Et Moshé, voyant cela, s’étonnait de cette vision et, comme il s'approchait pour observer, advint une voix du Seigneur :
7:32	C'est moi l'Elohîm de tes pères, l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov. Et Moshé, devenu tout tremblant, n'osait pas observer.
7:33	Et le Seigneur lui dit : Ôte la sandale de tes pieds, car le lieu sur lequel tu te tiens est une terre sainte.
7:34	J'ai vu, j'ai vu l'affliction de mon peuple qui est en Égypte, et j'ai entendu leur gémissement, et je suis descendu pour l'arracher. Maintenant, vas-y, je t'enverrai en Égypte.
7:35	Ce Moshé qu'ils avaient rejeté, en disant : Qui t'a établi prince et juge ? C'est lui qu'Elohîm envoya comme prince et comme rédempteur par le moyen de l'Ange qui lui était apparu dans le buisson.
7:36	C'est lui qui les a conduits dehors en produisant des prodiges et des signes en terre d'Égypte, à la Mer Rouge et au désert pendant 40 ans.
7:37	C'est ce Moshé qui a dit aux fils d'Israël : Le Seigneur votre Elohîm vous suscitera d'entre vos frères un Prophète comme moi. Écoutez-le<!--De. 18:15.--> !
7:38	C'est lui, qui, lors de l'assemblée dans le désert, étant avec l'Ange qui lui parlait sur la montagne de Sinaï et avec nos pères, reçut les paroles de vie pour nous les donner.
7:39	À qui nos pères ne voulurent pas obéir, mais ils le rejetèrent et ils tournèrent leur cœur vers l'Égypte,
7:40	en disant à Aaron : Fais-nous des elohîm qui marchent devant nous ! Car nous ne savons pas ce qui est arrivé à ce Moshé qui nous a conduits hors de la terre d'Égypte.
7:41	Et, en ces jours-là, ils firent un veau, et amenèrent un sacrifice à l'idole et se réjouirent dans les œuvres de leurs mains.
7:42	Mais Elohîm aussi se détourna d'eux et les livra au service de l'armée du ciel, selon qu’il est écrit dans le livre des prophètes<!--Am. 5:25-27.--> : Maison d'Israël, m'avez-vous offert des sacrifices et des victimes pendant 40 ans dans le désert ?
7:43	Mais vous avez porté le tabernacle de Moloc<!--Lé. 18:21.--> et l'étoile<!--Am. 5:26-27.--> de votre elohîm Remphan, les figures que vous avez faites pour les adorer. C'est pourquoi je vous transporterai au-delà de Babel.
7:44	Le tabernacle du témoignage fut au milieu de nos pères dans le désert, comme celui qui parlait à Moshé avait ordonné de le faire selon le modèle qu'il avait vu.
7:45	Et, l'ayant reçu, nos pères le firent entrer avec Yéhoshoua<!--Josué.--> dans la possession des nations qu'Elohîm chassa de devant la face de nos pères, jusqu'aux jours de David,
7:46	qui trouva grâce devant Elohîm et demanda de trouver une tente pour l'Elohîm de Yaacov.
7:47	Et ce fut Shelomoh qui lui bâtit une maison.
7:48	Mais le Très-Haut n'habite pas dans des temples faits de mains d’hommes, comme le dit le prophète :
7:49	Le ciel est mon trône et la Terre est le marchepied de mes pieds : quelle maison me bâtirez-vous, dit le Seigneur, ou quel pourrait être le lieu de mon repos<!--Es. 66:1.--> ?
7:50	Ma main n'a-t-elle pas fait toutes ces choses ?
7:51	Têtes dures et incirconcis de cœur et d'oreilles ! Vous vous opposez toujours au Saint-Esprit. Vous êtes tels que vos pères !
7:52	Lequel des prophètes vos pères n'ont-ils pas persécuté ? Ils ont même tué ceux qui annonçaient d'avance la venue du Juste dont vous êtes maintenant devenus les traîtres et les meurtriers,
7:53	vous qui avez reçu la torah sur les ordres des anges et qui ne l'avez pas gardée !

### Stephanos (Étienne) : Premier martyr

7:54	Mais en entendant ces choses, leurs cœurs étaient sciés en deux et ils grinçaient des dents contre lui.
7:55	Mais lui, étant rempli du Saint-Esprit et ayant les yeux fixés vers le ciel, vit la gloire d'Elohîm et Yéhoshoua se tenant debout<!--Ps. 109:31.--> à la droite d'Elohîm.
7:56	Et il dit : Voici, je vois les cieux ouverts et le Fils d'humain se tenant debout à la droite d'Elohîm.
7:57	Mais criant à grande voix, ils se bouchèrent les oreilles et se précipitèrent sur lui d'un commun accord.
7:58	Et l'ayant tiré hors de la ville, ils le lapidèrent. Et les témoins déposèrent leurs vêtements aux pieds d'un jeune homme nommé Shaoul<!--Saul.-->.
7:59	Et ils lapidaient Stephanos qui priait et disait : Seigneur Yéhoshoua, reçois mon esprit<!--Ec. 12:7.--> !
7:60	Et s'étant mis à genoux, il cria d'une grande voix : Seigneur, ne leur impute pas ce péché ! Et quand il eut dit cela, il s'endormit.

## Chapitre 8

### Quatrième persécution de l'Assemblée : Shaoul (Saul) opprime les saints

8:1	Or Shaoul était consentant à son assassinat. Et en ce jour-là survint une grande persécution contre l'Assemblée à Yeroushalaim. Et tous, excepté les apôtres, se dispersèrent dans les contrées de la Judée et de la Samarie.
8:2	Et des hommes pieux emportèrent Stephanos pour l'ensevelir et firent sur lui une grande lamentation en se frappant la poitrine en signe de douleur.
8:3	Mais Shaoul ravageait l'Assemblée : entrant dans les maisons et traînant hommes et femmes, il les livrait en prison.

### La dispersion des chrétiens<!--Ac. 11:19-21.-->

8:4	Alors ceux qui avaient été dispersés, allaient en effet de lieu en lieu, annonçant la parole.

### Philippos (Philippe) en Samarie ; Shim’ôn (Simon), le magicien

8:5	Et Philippos, étant descendu dans la ville de Samarie, leur prêcha le Mashiah.
8:6	Et les foules d'un commun accord, étaient attentives à ce que Philippos disait, en l’entendant et en voyant les signes qu'il produisait.
8:7	Car les esprits impurs de beaucoup de personnes qui en avaient, sortaient en criant à grande voix, et beaucoup de paralytiques et de boiteux furent guéris.
8:8	Et il y eut une grande joie dans cette ville-là.
8:9	Or il y avait auparavant dans la ville un certain homme du nom de Shim’ôn, qui pratiquait des actes de magie et étonnait la nation de la Samarie, disant de lui-même être quelqu'un de grand,
8:10	auquel tous s’attachaient, depuis le plus petit jusqu'au plus grand, disant : Celui-ci est la grande puissance d'Elohîm.
8:11	Or, ils s’attachaient à lui parce que depuis assez longtemps il les avait étonnés par ses sorcelleries.
8:12	Mais quand ils eurent cru Philippos annonçant l'Évangile des choses concernant le Royaume d'Elohîm et le Nom de Yéhoshoua Mashiah, tant les hommes que les femmes furent baptisés.
8:13	Et Shim’ôn lui-même crut aussi et, après avoir été baptisé, il était dévoué<!--« Adhérer à quelqu'un », « être son adhérent », « être dévoué ou constant à une cause ».--> à Philippos et il était dans l'étonnement, en voyant les signes et les grands miracles qui se produisaient.
8:14	Mais quand les apôtres qui étaient à Yeroushalaim eurent entendu que la Samarie avait reçu la parole d'Elohîm, ils leur envoyèrent Petros et Yohanan,
8:15	qui, y étant descendus, prièrent pour eux afin qu'ils reçoivent l'Esprit Saint.
8:16	Car il n’était encore descendu sur aucun d’eux, mais ils avaient été seulement baptisés dans le Nom du Seigneur Yéhoshoua.
8:17	Alors ils leur imposèrent les mains et ils reçurent l'Esprit Saint.
8:18	Mais Shim’ôn voyant que le Saint-Esprit était donné par le moyen de l'imposition des mains des apôtres, leur présenta de l'argent,
8:19	en disant : Donnez-moi aussi ce pouvoir, afin que tous ceux à qui j'imposerai les mains, reçoivent l'Esprit Saint.
8:20	Mais Petros lui dit : Que ton argent aille en perdition avec toi, puisque tu as pensé acquérir le don d'Elohîm avec de l'argent !
8:21	Il n'y a pour toi ni part ni lot dans cette affaire, car ton cœur n'est pas droit devant Elohîm.
8:22	Repens-toi donc de cette malice et prie Elohîm, afin que, s'il est possible, la pensée de ton cœur te soit pardonnée.
8:23	Car je vois que tu es dans un fiel amer et dans un lien d'injustice.
8:24	Mais Shim’ôn répondit et dit : Priez vous-mêmes le Seigneur en ma faveur, afin que ne vienne sur moi aucune des choses que vous avez dites.
8:25	Eux donc, après avoir en effet rendu témoignage et prêché la parole du Seigneur, ils retournèrent à Yeroushalaim en évangélisant beaucoup de villages des Samaritains.

### Conversion et baptême de l'eunuque éthiopien

8:26	Or un ange du Seigneur parla à Philippos, en disant : Lève-toi et va vers le midi, sur le chemin qui descend de Yeroushalaim à Gaza, celui qui est désert.
8:27	Et s’étant levé, il s’en alla. Et voici, un homme éthiopien, un eunuque, un grand ministre de Candace<!--Candace : « qui possède », « qui se repent ». Le nom n'est pas celui d'une personne particulière, mais celui d'une dynastie de reines éthiopiennes.-->, la reine des Éthiopiens, et responsable de tous ses trésors, était venu à Yeroushalaim pour adorer,
8:28	s'en retournait, assis dans son char et lisait le prophète Yesha`yah<!--Ésaïe.-->.
8:29	Mais l'Esprit dit à Philippos : Avance et joins-toi à ce char !
8:30	Et Philippos y étant accouru, il l’entendit lisant le prophète Yesha`yah. Et il lui dit : Comprends-tu vraiment ce que tu lis ?
8:31	Mais il lui dit : Comment donc le pourrais-je, si personne ne me guide ? Et il pria Philippos de monter et de s'asseoir avec lui.
8:32	Et le passage de l'Écriture qu'il lisait était celui-ci : Il a été mené comme une brebis à la boucherie et comme un agneau muet devant celui qui le tond, ainsi il n'ouvre pas sa bouche.
8:33	Dans son humiliation, son jugement a été ôté. Mais sa génération, qui la racontera ? Car sa vie est ôtée de la Terre<!--Es. 53:7-8.-->.
8:34	Et l'eunuque prenant la parole, dit à Philippos : Je te prie, de qui est-ce que le prophète dit cela ? Est-ce de lui-même, ou de quelqu'un d'autre ?
8:35	Et Philippos, ouvrant sa bouche, et commençant par cette Écriture, lui annonça l'Évangile de Yéhoshoua.
8:36	Mais comme ils allaient par le chemin, ils arrivèrent à une eau, et l'eunuque dit : Voici de l'eau. Qu'est-ce qui empêche que je ne sois baptisé ?
8:37	Et Philippos dit : Si tu crois de tout ton cœur, c'est légal. Et répondant, il dit : Je crois que Yéhoshoua Mashiah est le Fils d'Elohîm.
8:38	Et il ordonna d'arrêter le char. Ils descendirent tous les deux dans l’eau, Philippos et l'eunuque, et il le baptisa.
8:39	Mais, quand ils furent remontés de l’eau, l’Esprit du Seigneur enleva Philippos, et l’eunuque ne le vit plus, car il poursuivait sa route en se réjouissant.
8:40	Mais Philippos se trouva dans Azot, et en passant, il évangélisait toutes les villes jusqu’à ce qu’il vînt à Césarée.

## Chapitre 9

### Yéhoshoua (Jésus) se révèle à Shaoul (Saul)<!--Ac. 22:1-16, 26:9-18.-->

9:1	Or Shaoul, respirant encore la menace et le carnage contre les disciples du Seigneur, alla vers le grand-prêtre
9:2	et lui demanda des lettres pour les synagogues de Damas afin que, s'il y trouvait quelques-uns qui étaient de la Voie<!--Vient du grec « hodos » qui signifie au sens propre « une voie, une route, un chemin ou un sentier ». Il signifie également « une manière de se conduire, de penser, de décider » ou « une doctrine (la doctrine du Seigneur) ». Voir Ac. 19:9, 22:4, 24:14,22.-->, hommes ou femmes, il les amenât liés à Yeroushalaim.
9:3	Et comme il était en marche, il arriva près de Damas. Et soudainement, une lumière venant du ciel brilla comme un éclair autour de lui.
9:4	Et étant tombé par terre, il entendit une voix qui lui disait : Shaoul, Shaoul, pourquoi me persécutes-tu ?
9:5	Il dit : Mais qui es-tu Seigneur ? Et le Seigneur lui dit : Moi, je suis Yéhoshoua que tu persécutes. Il t'est dur de regimber contre les aiguillons<!--Une piqûre comme celle des abeilles, scorpions... Ces animaux blessant et causant même la mort, dans 1 Corinthiens 15:54-56, Paulos (Paul) compare la mort à une piqûre, une arme mortelle. Aiguillon : pièce métallique pour diriger les bœufs et autres bêtes de trait d'où le proverbe, « ruer contre les aiguillons », c'est-à-dire offrir une vaine, périlleuse et ruineuse résistance.-->.
9:6	Alors, tremblant et effrayé, il dit : Seigneur, que veux-tu que je fasse ? Et le Seigneur lui dit : Lève-toi, entre dans la ville et on te dira ce que tu dois faire.
9:7	Mais les hommes qui l'accompagnaient s'arrêtèrent, incapables de parler par terreur, entendant en effet la voix, mais ne voyant personne.
9:8	Et Shaoul se leva de terre et, malgré ses yeux ouverts, il ne voyait rien. Et le conduisant par la main, ils le menèrent à Damas,
9:9	et il fut trois jours, sans voir, et sans manger ni boire.
9:10	Or il y avait à Damas, un disciple du nom de Chananyah. Et le Seigneur lui dit en vision : Chananyah ! Et il répondit : Me voici Seigneur !
9:11	Et le Seigneur lui dit : Lève-toi, va dans la rue appelée la droite et cherche dans la maison de Yéhouda un nommé Shaoul de Tarse, car voici qu'il prie.
9:12	Et il a vu en vision, un homme du nom de Chananyah, entrant et lui imposant les mains, afin qu'il recouvre la vue. 
9:13	Mais Chananyah répondit : Seigneur, j’ai entendu dire par beaucoup concernant cet homme combien de choses mauvaises il a faites à tes saints dans Yeroushalaim.
9:14	Il a même ici le pouvoir, de la part des principaux prêtres, de lier tous ceux qui invoquent ton Nom.
9:15	Mais le Seigneur lui dit : Va, parce que cet homme est un vase<!--Le mot « vase » vient du grec « skeuos ». « Vase » était une métaphore grecque commune pour « le corps » car les Grecs pensaient que l'âme vivait temporairement dans les corps. 2 Co. 4:7 ; Ro. 9:21-23 ; 2 Ti. 2:20-21.--> que j'ai choisi<!--Élection. Ro. 9:11.--> pour porter<!--Vient du grec « bastazo » qui signifie : « prendre », « soulever avec les mains », « prendre dans le but de transporter », « poser sur soi une chose à transporter », « porter », « transporter », « soutenir », « supporter ».--> mon Nom devant les nations, et les rois, et les fils d'Israël.
9:16	Car je lui montrerai combien il faut qu'il souffre en faveur de mon Nom.

### Shaoul (Saul) rempli du Saint-Esprit

9:17	Et Chananyah s'en alla et entra dans la maison. Et ayant posé les mains sur lui, il dit : Shaoul, frère, le Seigneur Yéhoshoua, qui t'est apparu sur le chemin par lequel tu venais, m'a envoyé, afin que tu recouvres la vue et que tu sois rempli du Saint-Esprit.
9:18	Et immédiatement il tomba de ses yeux comme des écailles, et à l'instant, il recouvra la vue. Et il se leva et fut baptisé.
9:19	Et ayant pris de la nourriture, il fut fortifié. Et Shaoul fut quelques jours avec les disciples qui étaient à Damas.
9:20	Et immédiatement il prêcha dans les synagogues le Mashiah : que celui-ci est le Fils d'Elohîm.
9:21	Et tous ceux qui l'entendaient s'étonnaient et disaient : N'est-ce pas celui-là qui a détruit à Yeroushalaim ceux qui invoquent ce Nom, et qui est venu ici pour cela même, pour les amener liés aux principaux prêtres ?
9:22	Mais Shaoul se fortifiait de plus en plus et confondait les Juifs qui habitaient à Damas, démontrant que celui-ci<!--Yéhoshoua.--> est le Mashiah.

### Les Juifs complotent contre Shaoul (Saul)

9:23	Mais lorsqu'un assez grand nombre de jours furent accomplis, les Juifs tinrent conseil ensemble pour le tuer.
9:24	Et leur complot parvint à la connaissance de Shaoul. Or, ils gardaient les portes jour et nuit pour le tuer.
9:25	Mais pendant une nuit, les disciples le prirent et le descendirent à travers la muraille, en le descendant dans une corbeille.

### Shaoul (Saul) rencontre Barnabas et les apôtres à Yeroushalaim (Jérusalem)

9:26	Mais étant arrivé à Yeroushalaim, Shaoul tâchait de se joindre aux disciples, mais tous le craignaient, ne croyant pas qu'il soit un disciple.
9:27	Mais Barnabas l'ayant pris avec lui, le conduisit vers les apôtres et leur raconta comment sur le chemin, il avait vu le Seigneur, qui lui avait parlé, et comment à Damas, il parlait avec assurance au Nom de Yéhoshoua.
9:28	Et il était avec eux à Yeroushalaim, allant et venant,
9:29	et discourant avec assurance au Nom du Seigneur Yéhoshoua, il parlait et discutait<!--Vient du grec « suzeteo » qui signifie « chercher, examiner ensemble, discuter, disputer, se demander » ou « s'interroger ».--> avec les Hellénistes, mais ceux-ci tentaient de le tuer.
9:30	Mais les frères, l'ayant su, l'emmenèrent à Césarée et le firent partir pour Tarse.
9:31	Alors les assemblées étaient en effet en paix dans toute la Judée, la Galilée et la Samarie, s'édifiant et marchant dans la crainte du Seigneur, et elles s'accroissaient par le rafraîchissement du Saint-Esprit.

### Guérison d'Aineas, le paralytique

9:32	Or il arriva que, comme Petros les visitait tous, il descendit aussi vers les saints qui demeuraient à Lydde.
9:33	Et il y trouva un certain homme du nom d'Aineas, qui était paralytique, étendu depuis huit ans sur un lit de camp.
9:34	Et Petros lui dit : Aineas ! Yéhoshoua ha Mashiah te guérit. Lève-toi et fais toi-même ton lit ! Et immédiatement il se leva.
9:35	Tous ceux qui habitaient à Lydde et à Saron le virent, et ils se convertirent au Seigneur.

### Résurrection de Tabitha

9:36	Mais il y avait à Joppé une certaine disciple<!--Le mot grec « mathetria » (« femme disciple » ; « femme chrétienne ») est au féminin.--> du nom de Tabitha, ce qui, traduit, signifie Dorkas. Elle était pleine de bonnes œuvres et faisait des aumônes.
9:37	Mais il arriva en ces jours-là, qu'étant tombée malade, elle mourut. Et après l'avoir lavée, on la déposa dans une chambre haute.
9:38	Mais comme Lydde était près de Joppé, les disciples ayant appris que Petros y était, envoyèrent vers lui deux hommes, pour le prier de venir chez eux sans tarder.
9:39	Et Petros se leva et partit avec ces hommes. Lorsqu'il fut arrivé, on le conduisit dans la chambre haute. Toutes les veuves l'entourèrent en pleurant, et lui montrèrent les tuniques et les vêtements que faisait Dorkas, quand elle était avec elles.
9:40	Mais Petros fit sortir tout le monde, se mit à genoux et pria et, se tournant vers le corps, il dit : Tabitha, lève-toi ! Et elle ouvrit ses yeux et, voyant Petros, elle s'assit.
9:41	Et il lui donna la main et la fit lever. Et ayant appelé les saints et les veuves, il la leur présenta vivante.
9:42	Et cela fut connu dans tout Joppé et beaucoup crurent au Seigneur.
9:43	Et il arriva qu'il demeura plusieurs jours à Joppé, chez un certain Shim’ôn, un corroyeur.

## Chapitre 10

### Un ange d'Elohîm apparaît à Kornelios (Corneille)

10:1	Mais il y avait à Césarée un certain homme du nom de Kornelios, officier de l'armée romaine d'une cohorte appelée Italienne.
10:2	Il était pieux et craignant Elohîm<!--Cette expression désigne une catégorie de personnes tout à fait particulières. Il s'agit de païens gagnés au judaïsme mais non encore circoncis.-->, avec toute sa famille. Il faisait aussi beaucoup d'aumônes au peuple, et priait Elohîm continuellement.
10:3	Vers la neuvième heure du jour, il vit clairement dans une vision, un ange d'Elohîm qui entra chez lui et qui lui dit : Kornelios !
10:4	Et celui-ci ayant les yeux fixés sur lui et tout effrayé, dit : Qu'y a-t-il Seigneur ? Et il lui dit : Tes prières et tes aumônes sont montées devant Elohîm, et il s'en est souvenu.
10:5	Et maintenant, envoie des hommes à Joppé et fais venir Shim’ôn, qui est surnommé Petros.
10:6	Celui-ci loge chez un certain Shim’ôn, corroyeur, dont la maison est près de la mer. C'est lui qui te dira ce qu'il faut que tu fasses.
10:7	Et dès que l'ange qui lui parlait fut parti, Kornelios appela deux de ses domestiques et un soldat craignant Elohîm, d'entre ceux qui se tenaient près de lui.
10:8	Et après leur avoir tout raconté, il les envoya à Joppé.

### Vision de Petros (Pierre) : Une nappe descend du ciel

10:9	Et le lendemain, comme ils marchaient et qu'ils approchaient de la ville, Petros monta sur le toit, vers la sixième heure, pour prier.
10:10	Mais il eut faim et voulut prendre de la nourriture. Mais pendant qu’on la lui préparait, il tomba en extase.
10:11	Et il vit le ciel ouvert, et un certain vase descendant sur lui, semblable à une grande toile de lin, attachée par les quatre coins, qui descendait vers la Terre,
10:12	et dans lequel il y avait tous les quadrupèdes de la Terre, et les bêtes sauvages, et les reptiles, et les oiseaux du ciel.
10:13	Et une voix vint vers lui : Petros, lève-toi, tue et mange !
10:14	Mais Petros dit : En aucune façon, Seigneur, car je n'ai jamais rien mangé d'impur ni de souillé<!--Lé. 17:15 ; De. 14:3 ; Ez. 4:14.-->.
10:15	Et la voix lui dit encore pour la seconde fois : Ce qu'Elohîm a rendu pur, toi, ne le tiens pas pour impur !
10:16	Et cela arriva jusqu'à trois fois, et le vase fut enlevé de nouveau dans le ciel.
10:17	Mais comme Petros était en perplexité en lui-même sur ce que pouvait signifier la vision qu’il avait vue, voici que les hommes envoyés par Kornelios, s’étant informés de la maison de Shim’ôn, se tenaient à la porte.
10:18	Et ayant appelé, ils demandèrent si Shim’ôn, surnommé Petros, logeait là.
10:19	Mais comme Petros réfléchissait sur la vision, l'Esprit lui dit : Voici trois hommes qui te cherchent.
10:20	Mais lève-toi, descends et pars avec eux sans hésiter, parce que c'est moi qui les ai envoyés.
10:21	Et Petros descendit vers les hommes qui lui avaient été envoyés de la part de Kornelios et dit : Voici, je suis celui que vous cherchez. Quelle est la raison pour laquelle vous êtes ici ?
10:22	Et ils dirent : Kornelios, officier de l'armée romaine, homme juste et craignant Elohîm, et à qui toute la nation des Juifs rend un bon témoignage, a été averti divinement par un saint ange de te faire venir dans sa maison et d'entendre tes paroles.

### Petros (Pierre) chez Kornelios (Corneille)

10:23	Alors il les fit entrer et les logea. Le lendemain, Petros s'en alla avec eux, et quelques-uns des frères de Joppé l'accompagnèrent.
10:24	Et ils arrivèrent à Césarée le jour suivant. Kornelios les attendait et avait invité ses parents et amis intimes.
10:25	Mais il arriva que comme Petros entrait, Kornelios vint à sa rencontre, tomba à ses pieds et l'adora.
10:26	Mais Petros le releva, en lui disant : Lève-toi ! Moi aussi je suis un être humain.
10:27	Et s'entretenant avec lui, il entra et trouva beaucoup de personnes assemblées.
10:28	Et il leur dit : Vous savez qu'il est criminel<!--Vient du grec « athemitos » qui signifie « contraire à la loi et la justice », « prohibé », « illicite ».--> pour un homme juif de se lier avec un étranger, ou d'aller chez lui, mais Elohîm m'a montré que je ne devais appeler aucun être humain souillé<!--« Commun », « ordinaire », « appartenant à la généralité ».--> ou impur.
10:29	C'est pourquoi, ayant été appelé, je suis venu sans contradiction. Je m'informe donc pour quel motif vous m'avez fait appeler.
10:30	Et Kornelios lui dit : Il y a quatre jours, jusqu’à cette heure-ci j’étais en jeûne, et à la neuvième heure je priais dans ma maison, et tout à coup, un homme vêtu d'un habit resplendissant se présenta devant moi,
10:31	et me dit : Kornelios, ta prière est exaucée, et tes aumônes ont été rappelées en mémoire devant<!--Même construction en Ap. 16:19.--> Elohîm.
10:32	Envoie donc quelqu'un à Joppé et convoque Shim’ôn, surnommé Petros, il loge dans la maison de Shim’ôn, le corroyeur, près de la mer. Quand il sera venu, il te parlera.
10:33	À l'instant donc j'ai envoyé quelqu'un vers toi, et tu as bien fait de venir. Maintenant donc, nous sommes tous présents devant Elohîm, pour entendre toutes les choses qui t'ont été ordonnées par Elohîm.

### Petros (Pierre) évangélise les nations<!--Ac. 2:14-41.-->

10:34	Mais Petros ouvrant la bouche, dit : En vérité, je comprends<!--Saisir avec l'esprit.--> qu'Elohîm n'a pas égard à l'apparence des personnes,
10:35	mais qu'en toute nation, celui qui le craint et qui pratique la justice, est accepté par lui.
10:36	Telle est la parole qu’il a envoyée aux fils d’Israël, leur prêchant l'Évangile de la paix par Yéhoshoua Mashiah, qui est le Seigneur de tous.
10:37	Vous avez vu l'arrivée de la parole dans toute la Judée, en commençant depuis la Galilée après le baptême que Yohanan a prêché,
10:38	Yéhoshoua qui était de Nazareth, comment Elohîm l'a oint d'Esprit Saint et de force, lui qui allait de lieu en lieu, faisant du bien et guérissant tous ceux qui étaient sous l'empire du diable, parce qu'Elohîm était avec lui.
10:39	Et nous, nous sommes témoins de toutes les choses qu'il a faites, tant au pays des Juifs qu'à Yeroushalaim. Lui qu’ils ont tué en le suspendant au bois.
10:40	Elohîm l'a réveillé le troisième jour et lui a donné de se manifester en apparaissant,
10:41	non à tout le peuple, mais aux témoins choisis d'avance par Elohîm, à nous qui avons mangé et bu avec lui, après qu'il fut relevé des morts.
10:42	Et il nous a ordonné de prêcher au peuple et de témoigner que c'est lui qui a été désigné par Elohîm, juge des vivants et des morts.
10:43	Tous les prophètes rendent de lui le témoignage, que quiconque croit en lui, reçoit le pardon de ses péchés par son Nom.

### Le Saint-Esprit descend sur les nations

10:44	Comme Petros prononçait encore ce discours<!--Vient du grec « rhema » qui signifie « ce qui est ou a été émis par une voix humaine, la chose dite, la parole ».-->, le Saint-Esprit tomba sur tous ceux qui écoutaient la parole.
10:45	Et tous les fidèles de la circoncision qui étaient venus avec Petros, furent étonnés de ce que le don du Saint-Esprit était aussi répandu sur les nations.
10:46	Car ils les entendaient parler en langues et magnifier Elohîm.
10:47	Alors Petros prenant la parole, dit : Quelqu'un peut-il refuser l’eau pour baptiser ceux-ci, qui ont reçu le Saint-Esprit aussi bien que nous ?
10:48	Et il ordonna qu'ils soient baptisés dans le Nom du Seigneur. Après cela, ils le prièrent de rester quelques jours.

## Chapitre 11

### Face à la désapprobation, Petros (Pierre) relate aux disciples sa rencontre avec Kornelios (Corneille)

11:1	Or les apôtres et les frères qui étaient en Judée, apprirent que les nations aussi avaient reçu la parole d'Elohîm.
11:2	Et quand Petros fut monté à Yeroushalaim, ceux de la circoncision s'opposaient à lui,
11:3	en disant : Tu es entré chez des hommes incirconcis<!--Ou des hommes qui ont l'incirconcision ou le prépuce.--> et tu as mangé avec eux.
11:4	Mais Petros se mit à leur exposer les choses par ordre, disant :
11:5	J'étais dans la ville de Joppé et, en priant, je vis en extase une vision : un certain vase semblable à une grande toile de lin, descendant du ciel par les quatre coins et elle vint jusqu’à moi.
11:6	Fixant les yeux sur elle, je l'examinais et j'ai vu les quadrupèdes, et les bêtes sauvages, et les reptiles et les oiseaux du ciel.
11:7	Et j'ai entendu une voix qui me disait : Petros, lève-toi, tue et mange !
11:8	Et j'ai dit : En aucune façon, Seigneur, car jamais rien de souillé ni d'impur n'est entré dans ma bouche.
11:9	Mais la voix m'a répondu du ciel une seconde fois : Ce qu'Elohîm a rendu pur, toi, ne le tiens pas pour impur !
11:10	Or cela est arrivé trois fois, et tout a été retiré de nouveau dans le ciel.
11:11	Et voilà qu'au même instant trois hommes envoyés de Césarée vers moi se sont présentés à la maison où j'étais.
11:12	Et l'Esprit m'a dit de partir avec eux sans hésiter. Et les six frères que voici, m'ont accompagné et nous sommes entrés dans la maison de cet homme.
11:13	Il nous a raconté comment il avait vu dans sa maison, un ange qui s'était présenté à lui et lui avait dit : Envoie des gens à Joppé et fais venir Shim’ôn, surnommé Petros,
11:14	qui te dira des paroles par lesquelles tu seras sauvé, toi et toute ta maison.
11:15	Mais lorsque je me suis mis à parler, le Saint-Esprit est tombé sur eux comme il était tombé sur nous au commencement.
11:16	Et je me suis souvenu de la parole du Seigneur, comme il disait<!--Ac. 1:8.--> : En effet, Yohanan a baptisé d'eau, mais vous, vous serez baptisés dans le Saint-Esprit.
11:17	Si donc Elohîm leur a donné le même don qu'à nous qui avons cru au Seigneur Yéhoshoua Mashiah, mais qui étais-je, moi, pour pouvoir empêcher l'Elohîm ?
11:18	Et ayant entendu ces choses, ils se calmèrent et ils glorifièrent Elohîm, en disant : Elohîm a donc aussi donné la repentance aux nations pour la vie !

### Les disciples appelés « chrétiens » pour la première fois à Antioche

11:19	Ceux qui avaient été dispersés par la tribulation survenue à cause de Stephanos, allèrent en effet jusqu'en Phénicie, dans l'île de Chypre et à Antioche<!--Capitale de la Syrie, située sur le fleuve Oronte, fondée en 300 av. J.-C. et ainsi nommée en l'honneur de son fondateur Antiochos. De nombreux Juifs-Grecs y vivaient. C'est là que les disciples du Mashiah (Christ) furent appelés « chrétiens » pour la première fois.-->, n'annonçant la parole à personne, si ce n'est à des Juifs seulement.
11:20	Mais il y eut parmi eux, quelques hommes chypriotes et cyrénéens, qui, étant venus à Antioche, parlèrent aussi aux Grecs et leur annoncèrent l'Évangile du Seigneur Yéhoshoua.
11:21	Et la main du Seigneur était avec eux, et un grand nombre de personnes crurent et se convertirent au Seigneur.
11:22	Or le bruit en parvint aux oreilles de l'Assemblée de Yeroushalaim et ils envoyèrent Barnabas pour aller jusqu'à Antioche.
11:23	Lorsqu'il fut arrivé et qu'il eut vu la grâce d'Elohîm, il s'en réjouit, et il les exhortait tous à demeurer attachés au Seigneur de tout leur cœur.
11:24	Car c'était un homme bon, plein du Saint-Esprit et de foi. Et un grand nombre de personnes se joignirent au Seigneur.
11:25	Mais Barnabas s'en alla à Tarse pour chercher Shaoul.
11:26	Et l'ayant trouvé, il le conduisit à Antioche. Et il arriva qu'ils se réunirent pendant toute une année avec l'Assemblée et qu'ils enseignèrent une foule assez nombreuse. Et c'est à Antioche que, pour la première fois, les disciples reçurent le nom de chrétiens.

### Prophétie d'Agabos

11:27	Or en ces jours-là, des prophètes descendirent de Yeroushalaim à Antioche.
11:28	Et l'un d'eux, du nom d'Agabos, se leva et déclara par l'Esprit, qu'une grande famine était sur le point d'arriver sur toute la terre habitée, laquelle eut lieu en effet sous Claude César.
11:29	Or les disciples, chacun selon ses moyens, déterminèrent d'envoyer quelque chose pour le service des frères qui demeuraient en Judée.
11:30	Ce qu’ils firent aussi, l’envoyant aux anciens par les mains de Barnabas et de Shaoul.

## Chapitre 12

### Cinquième persécution de l'Assemblée : Meurtre de Yaacov (Jacques) ; Petros (Pierre) emprisonné

12:1	Or en ce même temps, le roi Hérode mit la main sur quelques-uns de l'Assemblée pour les maltraiter,
12:2	et il fit mourir par l'épée Yaacov, le frère de Yohanan.
12:3	Et voyant que la chose était agréable aux Juifs, il ajouta à cela d'arrêter aussi Petros. Or c'étaient les jours des pains sans levain.
12:4	Et l’ayant saisi, il le mit en prison, le livrant à la garde de quatre escouades<!--Une escouade est un groupe de 4 soldats. 4 escouades (16 soldats) furent préposés à la garde de Petros (Pierre), emprisonné à Yeroushalaim : chaque escouade montait la faction pendant une veille de trois heures. Durant les veilles nocturnes, 2 soldats partageaient le cachot de l'apôtre, tandis que 2 autres se tenaient devant la porte.--> de 4 soldats chacune, voulant après la Pâque l'amener devant le peuple.

### Un ange du Seigneur délivre Petros (Pierre)

12:5	En effet, Petros était donc gardé dans la prison, mais une prière constante se faisait à Elohîm par l'Assemblée en sa faveur.
12:6	Or, quand Hérode était sur le point de le faire comparaître, cette même nuit, Petros était endormi entre deux soldats, lié de deux chaînes. Et des gardes devant la porte, gardaient la prison.
12:7	Et voici qu'un ange du Seigneur survint et une lumière resplendit dans l'habitation. Et frappant Petros au côté, il le réveilla, en disant : Lève-toi promptement ! Et les chaînes tombèrent de ses mains.
12:8	Et l'ange lui dit : Mets ta ceinture et chausse tes sandales<!--Voir Mc. 6:9.--> ! Et il fit ainsi. Et il lui dit : Enveloppe-toi de ton manteau et suis-moi !
12:9	Et étant sorti, il le suivait, et il ne savait pas que ce qui se faisait par le moyen de l'ange était vrai, mais il pensait voir une vision.
12:10	Et lorsqu'ils eurent passé la première et la seconde garde, ils arrivèrent à la porte de fer qui mène à la ville. Elle s'ouvrit à eux d'elle-même et, étant sortis, ils s'avancèrent dans une rue. Et immédiatement, l'ange le quitta.
12:11	Et revenu à lui-même, Petros dit : Maintenant, je vois que le Seigneur a vraiment envoyé son ange et qu'il m'a arraché de la main d'Hérode et de toute l'attente du peuple juif.
12:12	Quand il eut compris<!--Voir (avoir vu) ensemble avec les autres ; voir (avoir vu) dans son esprit, en soi-même : comprendre, percevoir ; connaître avec un autre ; connaître dans son esprit, avoir conscience de.-->, il alla à la maison de Myriam, mère de Yohanan, surnommé Markos, où un assez grand nombre étaient réunis et priaient.
12:13	Mais comme Petros frappait à la porte du vestibule, une servante du nom de Rhode vint pour écouter.
12:14	Reconnaissant la voix de Petros et, dans sa joie, elle n'ouvrit pas le vestibule, mais elle courut annoncer que Petros était devant la porte.
12:15	Mais ils lui dirent : Tu es folle. Mais elle affirmait qu'il en était ainsi. Et ils disaient : C'est son ange.
12:16	Mais Petros continuait à frapper. Et quand ils eurent ouvert, ils le virent et furent étonnés.
12:17	Mais leur ayant fait signe de la main de se taire, il leur raconta comment le Seigneur l'avait conduit hors de la prison, et il dit : Annoncez ces choses à Yaacov et aux frères. Et étant sorti, il s'en alla dans un autre lieu.
12:18	Or, le jour étant venu, l'agitation ne fut pas petite parmi les soldats : Qu’était donc devenu Petros ?
12:19	Mais Hérode, l'ayant cherché sérieusement et ne l'ayant pas trouvé, fit juger les gardes et donna l'ordre de les mener au châtiment. Et il descendit de Judée à Césarée, où il séjourna.

### Mort d'Hérode

12:20	Or Hérode amenait à la guerre avec une grande animosité les Tyriens et les Sidoniens. Mais ils vinrent le trouver d'un commun accord et, ayant gagné Blaste, son chambellan, ils demandèrent la paix, parce que leur pays était alimenté par celui du roi.
12:21	Or, au jour fixé, Hérode, revêtu de ses habits royaux, s'assit sur son trône et adressa un discours au peuple.
12:22	Or le peuple criait : Voix d'un elohîm et non d'un être humain !
12:23	Et immédiatement, un ange du Seigneur le frappa, parce qu'il n'avait pas donné gloire à Elohîm. Et il fut mangé par les vers et expira.
12:24	Mais la parole d'Elohîm croissait et se multipliait.
12:25	Mais Barnabas et Shaoul revinrent de Yeroushalaim, après avoir accompli le service, ayant aussi pris avec eux, Yohanan surnommé Markos.

## Chapitre 13

### Shaoul (Saul) et Barnabas mis à part par le Saint-Esprit

13:1	Or il y avait dans l'assemblée qui est à Antioche quelques prophètes et docteurs : Barnabas, Shim’ôn appelé le Noir, Loukios le Cyrénien, Manahen qui avait été élevé avec Hérode le tétrarque, et Shaoul.
13:2	Or, en servant le Seigneur et en jeûnant, le Saint-Esprit dit : Séparez-moi maintenant Barnabas et Shaoul pour l'œuvre à laquelle je les ai appelés.
13:3	Alors, ayant jeûné et prié, et leur ayant imposé les mains, ils les laissèrent aller<!--Voir annexe « Les voyages missionnaires de Paulos (Paul) ».-->.

### Shaoul (Saul), Barnabas et Yohanan sur l'île de Chypre

13:4	Eux donc, envoyés en effet par le Saint-Esprit, descendirent à Séleucie, et de là, ils s'embarquèrent pour l'île de Chypre.
13:5	Et lorsqu'ils furent à Salamine, ils annoncèrent la parole d'Elohîm dans les synagogues des Juifs. Ils avaient Yohanan avec eux pour serviteur<!--De rang inférieur, d'un rang de subordonné. Dans le Testament de Yéhoshoua les huissiers et aides des magistrats, l'officier qui fait exécuter les peines. Voir Jn. 7:32-45. Celui qui en aide un autre dans un travail. Voir Lu. 4:20.-->.

### Bar-Yéhoshoua (Bar-Jésus) aveuglé et conversion du proconsul Sergios Paulos

13:6	Et ayant traversé l’île jusqu'à Paphos, ils trouvèrent là, un certain magicien, faux prophète juif, du nom de Bar-Yéhoshoua,
13:7	qui était avec le proconsul<!--L'empereur Auguste avait divisé les provinces romaines en sénatoriales et impériales. Les premières étaient dirigées par des proconsuls, les autres par des légats de l'empereur, qui étaient appelés « propraetor », propréteurs.--> Sergios Paulos, homme intelligent. Celui-ci fit appeler Barnabas et Shaoul, désirant entendre la parole d'Elohîm.
13:8	Mais Élymas, le magicien, (car c'est ainsi que son nom s'interprète), leur résistait, cherchant à détourner de la foi le proconsul.
13:9	Mais Shaoul, qui est aussi Paulos, rempli du Saint-Esprit, fixa les yeux sur lui, 
13:10	et dit : Ô toi, qui es plein de toute sorte de tromperie et de ruse, fils du diable, ennemi de toute justice, ne cesseras-tu pas de pervertir les voies droites du Seigneur ?
13:11	Et maintenant, voici sur toi la main du Seigneur, et tu seras aveugle, ne voyant pas le soleil, jusqu’à un certain temps. Immédiatement l'obscurité et la ténèbre tombèrent sur lui, et il se tournait de tous côtés, cherchant des personnes qui le conduisissent par la main.
13:12	Alors le proconsul, voyant ce qui était arrivé, crut, étant choqué par la doctrine du Seigneur.
13:13	Mais Paulos et ceux qui étaient avec lui s’étant embarqués à Paphos, vinrent à Perge en Pamphylie. Mais Yohanan se sépara d'eux et retourna à Yeroushalaim.

### Paulos (Paul) à Antioche de Pisidie

13:14	Quant à eux, de Perge passant plus loin, ils vinrent à Antioche de Pisidie<!--Antioche de Pisidie : ville de Pisidie (en Turquie), à la frontière de Phrygie, fondée par Seleucus Nicanor. Elle devint une colonie romaine et fut aussi appelée Césarée.--> et, étant entrés dans la synagogue le jour du shabbat, ils s'assirent.
13:15	Et après la lecture de la torah et des prophètes, les chefs de la synagogue leur envoyèrent dire : Hommes frères, si vous avez quelque parole d'exhortation pour le peuple, parlez !
13:16	Mais Paulos, s'étant levé et, ayant fait signe de la main, dit : Hommes israélites et vous qui craignez Elohîm, écoutez !
13:17	L'Elohîm de ce peuple d'Israël a choisi nos pères. Et il a élevé le peuple pendant son séjour en terre d'Égypte, et il les en a fait sortir à bras levé.
13:18	Et pendant l'espace d'environ 40 ans, il a supporté leur caractère<!--Le verbe supporter vient du grec « tropophoreo » qui signifie « supporter les manières, endurer le caractère de quelqu'un ».--> dans le désert.
13:19	Et ayant renversé sept nations en terre de Kena'ân, il leur a distribué la terre par lot.
13:20	Et après ces choses, pendant environ 450 ans, il leur a donné des juges, jusqu'à Shemouél le prophète.
13:21	Ensuite ils ont demandé un roi. Et Elohîm leur a donné Shaoul<!--Saül.-->, fils de Kis, homme de la tribu de Benyamin : 40 ans.
13:22	Et l'ayant destitué de cette fonction, il leur a suscité pour roi David, auquel il a rendu ce témoignage : J'ai trouvé David<!--Ps.89:20.-->, fils d'Isaï, homme selon mon cœur, qui fera toutes mes volontés.
13:23	C'est de sa postérité qu'Elohîm, selon la promesse, a suscité à Israël le Sauveur, Yéhoshoua.
13:24	Avant, face à son entrée, Yohanan avait annoncé d'avance le baptême de repentance à tout le peuple d'Israël.
13:25	Mais comme Yohanan accomplissait<!--« Remplir » c'est-à-dire, remplir jusqu'au bord. Ac. 20:24 : 2 Ti. 4:7.--> sa course, il disait : Ce que vous supposez que je suis, moi, je ne le suis pas. Mais voici qu'après moi vient celui dont je ne suis pas digne de délier la sandale de ses pieds<!--Mt. 3:11.-->.
13:26	Hommes frères, fils de la race d'Abraham, et vous qui craignez Elohîm, c'est à vous que la parole de ce salut a été envoyée.
13:27	Car ceux qui habitent à Yeroushalaim et leurs chefs ne l'ayant pas connu et, en le condamnant, ils ont accompli les paroles des prophètes qui se lisent chaque shabbat.
13:28	Et, ne trouvant aucune cause de mort, ils demandèrent à Pilate de le faire mourir.
13:29	Et, après avoir accompli toutes les choses qui sont écrites à son sujet, ils le descendirent du bois et le déposèrent dans un sépulcre.
13:30	Mais Elohîm l'a réveillé d'entre les morts.
13:31	Il est apparu pendant de nombreux jours à ceux qui étaient montés avec lui de Galilée à Yeroushalaim, et qui sont ses témoins devant le peuple.
13:32	Et nous, nous vous annonçons la bonne nouvelle concernant la promesse faite aux pères,
13:33	qu'Elohîm l'a accomplie envers nous, leurs enfants, en ressuscitant Yéhoshoua, comme aussi il est écrit dans le deuxième psaume : Tu es mon Fils, je t'ai aujourd'hui engendré<!--Ps. 2:7.-->.
13:34	Mais qu’il l’ait relevé des morts, pour ne plus être sur le point de retourner dans la corruption, il a parlé ainsi : Je vous donnerai les choses saintes<!--« Non souillé par le péché », « libre de méchanceté », « observant religieusement toute obligation morale », « pur », « saint », « pieux ».--> de David qui sont fidèles<!--Voir Es. 55:3.-->.
13:35	C'est pourquoi il a dit aussi dans un autre endroit : Tu ne permettras pas que ton Saint voie la corruption<!--Ps. 16:10.-->.
13:36	Car David, après avoir en effet servi<!--« Agir comme un serviteur », « comme un homme du rang », « administrer », « rendre des services ».--> en sa propre génération au dessein d'Elohîm, s’est endormi, a rejoint ses pères et a vu la corruption.
13:37	Mais celui qu'Elohîm a réveillé n'a pas vu la corruption.
13:38	Qu'il vous soit donc connu, hommes frères, que c'est par son moyen que le pardon des péchés vous est annoncé,
13:39	et que quiconque croit, est justifié par lui, de toutes les choses dont vous n'avez pas pu être justifiés par la torah de Moshé.
13:40	Prenez donc garde que ce qui a été dit dans les prophètes ne vienne sur vous :
13:41	Voyez, vous qui me méprisez<!--« Contempteur », « celui qui méprise ».-->, soyez étonnés et disparaissez ! Car je fais une œuvre en vos jours, une œuvre que vous ne croiriez jamais si quelqu'un vous la racontait<!--Ha. 1:5.-->.
13:42	Et lorsqu'ils sortirent de la synagogue des Juifs, les nations les prièrent de parler le shabbat suivant sur les mêmes choses.
13:43	Et la synagogue s'étant dissoute<!--« Délier des liens », « relâcher », « rendre libre », « annuler », « renverser ».-->, beaucoup de Juifs et de prosélytes adorateurs, suivirent Paulos et Barnabas, qui les exhortèrent à persévérer dans la grâce d'Elohîm.

### Les Juifs d'Antioche rejettent la Parole ; l'Évangile annoncé aux nations<!--Ac. 18:6, 28:25-28.-->

13:44	Mais le shabbat suivant, presque toute la ville se rassembla pour entendre la parole d'Elohîm.
13:45	Mais voyant les foules, les Juifs furent remplis de jalousie et ils contredisaient ce qui était dit par Paulos, contredisant et blasphémant.
13:46	Mais Paulos et Barnabas parlant avec assurance, dirent : C'est à vous premièrement qu'il fallait annoncer la parole d'Elohîm, mais puisque vous la rejetez et que vous vous jugez vous-mêmes indignes de la vie éternelle, voici, nous nous tournons vers les nations.
13:47	Car ainsi nous l'a ordonné le Seigneur : Je t’ai établi pour la lumière des nations, afin que tu sois en salut jusqu’aux extrémités de la Terre.
13:48	Mais les nations se réjouissaient en entendant cela, et elles glorifiaient la parole du Seigneur, et tous ceux qui étaient désignés<!--« Mettre en ordre », « placer », « poster », « placer dans un certain ordre », « arranger », « assigner une place », « désigner », « nommer », « ordonner », « instituer ».--> pour la vie éternelle, crurent.
13:49	Et la parole du Seigneur continuait à être portée à travers tout le pays.
13:50	Mais les Juifs excitèrent les femmes adoratrices et honorables et les principaux de la ville, et ils provoquèrent une persécution contre Paulos et Barnabas et les chassèrent de leur territoire.
13:51	Mais ceux-ci ayant secoué contre eux la poussière de leurs pieds, allèrent à Icone.
13:52	Mais les disciples étaient remplis de joie et du Saint-Esprit.

## Chapitre 14

### Paulos (Paul) et Barnabas à Icone

14:1	Or il arriva qu'à Icone ils entrèrent ensemble dans la synagogue des Juifs, et ils parlèrent d'une telle manière qu'une grande multitude de Juifs et de Grecs crut.
14:2	Mais les Juifs rebelles excitèrent et envenimèrent les âmes des nations contre les frères.
14:3	C'est pourquoi ils séjournèrent là assez longtemps en effet, parlant avec assurance sur le Seigneur qui rendait témoignage à la parole de sa grâce en donnant des signes et des prodiges par leurs mains.
14:4	Mais la multitude de la ville fut divisée. Et les uns étaient en effet avec les Juifs, mais les autres avec les apôtres.
14:5	Et comme il se faisait une émeute des nations et des Juifs, avec leurs principaux chefs, pour les outrager et les lapider,
14:6	eux, s’en étant aperçus, se réfugièrent dans les villes de Lycaonie : Lystre et Derbe, et dans la région environnante.
14:7	Et ils y annoncèrent l'Évangile.

### Guérison d'un boiteux de naissance à Lystre

14:8	Et à Lystre, se tenait assis un certain homme impotent des pieds. Il était boiteux dès le ventre de sa mère et jamais il n'avait marché.
14:9	Celui-ci entendait parler Paulos, qui, fixant ses yeux sur lui et voyant qu'il avait la foi pour être sauvé,
14:10	dit d'une grande voix : Lève-toi droit sur tes pieds ! Et il sautait et marchait.

### Confrontés à l'idolâtrie, Paulos (Paul) et Barnabas s'humilient ; Paulos lapidé

14:11	Et les foules ayant vu ce que Paulos avait fait, élevèrent la voix et dirent en langue lycaonienne : Les elohîm s'étant faits semblables à des humains sont descendus vers nous.
14:12	Et ils appelaient Barnabas Zeus, et Paulos Hermès, parce que c'était lui en effet qui portait<!--« Mener », « conduire », « aller devant », « être un chef », « diriger », « commander », « considéré ». Voir Ac. 15:22.--> la parole.
14:13	Or le prêtre de “Zeus qui est devant leur ville”, ayant amené des taureaux et des guirlandes<!--Un filet, une guirlande, posée sur les victimes.--> aux portes, voulait offrir un sacrifice avec les foules.
14:14	Mais les apôtres Barnabas et Paulos, ayant appris cela, déchirèrent leurs vêtements et se précipitèrent dans la foule en criant
14:15	et disant : Hommes, pourquoi faites-vous ces choses ? Nous aussi, nous sommes des humains ayant les mêmes sentiments que vous, et vous annonçant l'Évangile, pour que vous vous détourniez de ces choses vaines vers l'Elohîm, le Vivant<!--Ap. 1:18.-->, qui a fait le ciel et la Terre, la mer et toutes les choses qui y sont :
14:16	Lequel, dans les générations passées, a permis à toutes les nations de suivre leurs propres voies.
14:17	Et cependant il ne s'est pas laissé sans témoignage, en faisant du bien, en nous donnant du ciel les pluies et les saisons fertiles, remplissant nos cœurs de nourriture et de joie.
14:18	Et bien qu'en disant ces choses, c'est avec difficulté qu'ils empêchèrent les foules de leur sacrifier.
14:19	Mais, survinrent quelques Juifs d'Antioche et d'Icone, qui gagnèrent la foule, et qui, après avoir lapidé Paulos, le traînèrent hors de la ville, pensant qu'il était mort.
14:20	Mais les disciples l'ayant entouré, il se leva et entra dans la ville. Et le lendemain, il s'en alla avec Barnabas à Derbe.
14:21	Après avoir évangélisé cette ville et fait un assez grand nombre de disciples, ils retournèrent à Lystre, à Icone et à Antioche,
14:22	fortifiant davantage les âmes des disciples et les exhortant à persévérer dans la foi, disant que c'est par beaucoup de tribulations qu'il nous faut entrer dans le Royaume d'Elohîm.
14:23	Et leur ayant désigné des anciens par vote<!--Voter en élevant la main.--> dans chaque assemblée et, après avoir prié et jeûné, ils les confièrent au Seigneur en qui ils avaient cru.
14:24	Et traversant ensuite la Pisidie, ils allèrent en Pamphylie,
14:25	annoncèrent la parole à Perge, et descendirent à Attalie.
14:26	De là, ils s'embarquèrent pour Antioche, d'où ils avaient été recommandés à la grâce d'Elohîm pour l'œuvre qu'ils avaient accomplie.
14:27	Mais étant arrivés, et ayant rassemblé l'assemblée, ils racontèrent toutes les choses qu'Elohîm avait faites par eux, et comment il avait ouvert aux nations la porte de la foi.
14:28	Et ils séjournèrent là avec les disciples, un temps qui ne fut pas court.

## Chapitre 15

### Paulos (Paul) et Barnabas débattent avec les judaïsants

15:1	Et quelques-uns, qui étaient descendus de Judée, enseignaient aux frères : Si vous n'êtes pas circoncis selon la coutume de Moshé, vous ne pouvez pas être sauvés.
15:2	Une sédition a donc eu lieu et une discussion non minime, entre Paulos et Barnabas et eux, il fut décidé que Paulos et Barnabas, avec quelques-uns des leurs, monteraient à Yeroushalaim vers les apôtres et les anciens, au sujet de cette question.
15:3	Eux donc, après avoir été accompagnés en effet par l’assemblée, traversèrent la Phénicie et la Samarie, racontant la conversion des nations et ils causèrent une grande joie à tous les frères.
15:4	Et arrivés à Yeroushalaim, ils furent reçus par l'assemblée, les apôtres et les anciens, et ils rapportèrent toutes les choses qu'Elohîm avait faites avec eux.
15:5	Mais certains de la secte des pharisiens qui avaient cru, s'élevèrent disant qu'il faut les circoncire et leur ordonner de garder la torah de Moshé.

### Réunion des apôtres et des anciens

15:6	Mais les apôtres et les anciens s'assemblèrent pour examiner cette affaire.
15:7	Et après une grande discussion, Petros se leva et leur dit : Hommes frères, vous savez que depuis longtemps, Elohîm m'a choisi parmi nous, afin que par ma bouche, les nations entendent la parole de l'Évangile et qu'elles croient.
15:8	Et Elohîm, qui connaît les cœurs, leur a rendu témoignage, en leur donnant le Saint-Esprit, de même qu'à nous.
15:9	Et il n'a fait aucune différence entre nous et elles, ayant purifié leurs cœurs par la foi.
15:10	Maintenant donc, pourquoi tentez-vous Elohîm en voulant imposer aux disciples un joug que, ni nos pères ni nous, n'avons pu porter ?
15:11	Mais c'est par le moyen de la grâce du Seigneur Yéhoshoua Mashiah que nous croyons être sauvés, de la même manière qu'eux aussi.
15:12	Mais toute la multitude garda le silence, et ils écoutaient Barnabas et Paulos, racontant combien de signes et de prodiges Elohîm avait produits par leur intermédiaire, au milieu des nations.
15:13	Et lorsqu'ils eurent cessé de parler, Yaacov répondit, disant : Hommes frères, écoutez-moi !
15:14	Shim’ôn a raconté comment Elohîm pour la première fois a visité les nations afin d'en prendre un peuple à son Nom.
15:15	Et les paroles des prophètes s'y accordent, selon qu'il est écrit :
15:16	Après ces choses, je reviendrai et je reconstruirai le tabernacle<!--Le mot « tabernacle » vient du grec « skene » qui signifie aussi « tente ». Le Seigneur Yéhoshoua est venu dresser sa tente parmi les humains. Voir Jean 1:14. Il reviendra restaurer Israël. Voir Am. 9:11.--> de David qui est tombé, j'en reconstruirai les ruines et je le relèverai<!--Am. 9:11.-->,
15:17	afin que le reste des humains cherche le Seigneur, ainsi que toutes les nations sur lesquelles mon Nom est invoqué, dit le Seigneur qui fait toutes ces choses.
15:18	Toutes les œuvres d'Elohîm lui sont connues de toute éternité.
15:19	C'est pourquoi moi, je juge qu'il ne faut pas contrarier ceux d'entre les nations qui se tournent vers Elohîm,
15:20	mais leur écrire qu'ils s'abstiennent des souillures des idoles et de relation sexuelle illicite, et de ce qui est étouffé et du sang.
15:21	Car, depuis les générations anciennes, Moshé a dans chaque ville, des gens qui le prêchent, en le lisant chaque jour de shabbat dans les synagogues.
15:22	Alors il parut bon aux apôtres et aux anciens, avec toute l'assemblée, de choisir parmi eux et d'envoyer à Antioche, avec Paulos et Barnabas, Yéhouda appelé Barsabas et Silas, hommes considérés<!--« Mener », « conduire », « aller devant », « être un chef », « diriger », « commander ». Voir Ac. 14:12.--> entre les frères.

### Lettre des apôtres et des anciens aux frères d'entre les nations

15:23	Ils écrivirent par le moyen de leur main ces choses : Les apôtres, les anciens et les frères, aux frères d'entre les nations qui sont à Antioche, en Syrie et en Cilicie, salut !
15:24	Puisque nous avons appris que quelques-uns sortis de chez nous, auxquels nous n’avions donné aucun ordre, vous ont troublés par leurs discours et ont ébranlé vos âmes, en vous disant qu'il faut être circoncis et garder la torah,
15:25	il nous a paru bon, étant tombés d’accord, d'envoyer vers vous, avec nos bien-aimés Barnabas et Paulos, des hommes que nous avons choisis,
15:26	des hommes qui ont livré leurs âmes en faveur du Nom de notre Seigneur Yéhoshoua Mashiah.
15:27	Nous avons donc envoyé Yéhouda et Silas, qui vous rapporteront les mêmes choses de vive voix.
15:28	Car il a paru bon au Saint-Esprit et à nous, de ne vous imposer d'autre fardeau que ce qui est nécessaire :
15:29	vous abstenir des viandes sacrifiées aux idoles, du sang, de ce qui est étouffé et de relation sexuelle illicite. Choses contre lesquelles vous vous trouverez bien de vous tenir en garde. Adieu !
15:30	Ayant donc été congédiés, ils allèrent à Antioche, où ils rassemblèrent la multitude et remirent en effet la lettre.
15:31	Et l’ayant lue, ils se réjouirent de cette exhortation.
15:32	Et Yéhouda et Silas, qui étaient eux-mêmes prophètes, exhortèrent les frères par beaucoup de discours et les fortifièrent davantage.
15:33	Et quand ils eurent passé là quelque temps, ils furent renvoyés en paix par les frères vers les apôtres.
15:34	Mais il sembla bon à Silas de demeurer là.
15:35	Et Paulos et Barnabas séjournèrent aussi à Antioche, enseignant et annonçant, avec beaucoup d'autres, la parole du Seigneur.

### Paulos et Barnabas se séparent

15:36	Mais quelques jours après, Paulos dit à Barnabas : Retournons maintenant visiter nos frères dans chaque ville où nous avons annoncé la parole du Seigneur, pour voir dans quel état ils se trouvent<!--Voir annexe « Les voyages missionnaires de Paulos (Paul) ».-->.
15:37	Or Barnabas décida d'emmener avec eux Yohanan, appelé Markos,
15:38	mais Paulos jugea plus convenable de ne pas emmener avec eux celui qui les avait quittés depuis la Pamphylie et qui ne les avait pas accompagnés dans l'œuvre.
15:39	Il y eut donc entre eux de l'irritation, en sorte qu'ils se séparèrent l'un de l'autre. Alors Barnabas prit Markos avec lui et s'embarqua pour l'île de Chypre.
15:40	Mais Paulos, ayant choisi Silas, partit après avoir été recommandé à la grâce d'Elohîm par les frères.
15:41	Et il traversa la Syrie et la Cilicie, fortifiant davantage les assemblées.

## Chapitre 16

### Paulos (Paul) rencontre Timotheos (Timothée)

16:1	Et il arriva à Derbe et à Lystre. Et voici qu'il y avait là un disciple du nom de Timotheos, fils d'une femme juive fidèle, mais d'un père grec.
16:2	Lequel avait un témoignage honorable de la part des frères qui étaient à Lystre et à Icone.
16:3	Paulos voulait qu’il vienne avec lui. Et l'ayant pris, il le circoncit, à cause des Juifs qui étaient dans ces lieux-là, car tous savaient que son père était Grec.
16:4	Mais en passant par les villes, ils leur recommandaient d'observer les dogmes décrétés par les apôtres et les anciens de Yeroushalaim.
16:5	En effet, les assemblées se fortifiaient donc dans la foi et abondaient en nombre chaque jour.

### Vision de Paulos

16:6	Mais lorsqu'ils eurent traversé la Phrygie et le pays de Galatie, ayant été empêchés par le Saint-Esprit d'annoncer la parole en Asie,
16:7	arrivés près de la Mysie, ils essayaient d'aller en Bithynie, mais l'Esprit de Yéhoshoua<!--Notons que le Saint-Esprit est appelé « l'Esprit de Yéhoshoua (Jésus) ». Ainsi, de la même manière qu'on ne peut dissocier un homme de son esprit pour en faire deux entités distinctes, on ne peut dissocier Yéhoshoua de son Esprit. Elohîm est un.--> ne le leur permit pas.
16:8	Mais ayant passé la Mysie, ils descendirent à Troas.
16:9	Et une vision apparut à Paulos pendant la nuit. C'était un homme macédonien qui se tenait là, le suppliant et disant : Passe en Macédoine et secours-nous !
16:10	Et dès qu'il a vu la vision, nous avons immédiatement cherché à partir pour la Macédoine, concluant<!--« Faire fusionner », « joindre ensemble », « unir une personne avec une autre dans une conclusion ou l'amener à une même opinion ».--> que le Seigneur nous appelait à les évangéliser.

### Paulos à Philippes

16:11	Embarqués donc à Troas, nous avons pris un trajet direct vers Samothrace, et le lendemain vers Néapolis.
16:12	Et de là à Philippes, qui est la première ville de cette portion de la Macédoine, une colonie<!--La cité de Philippes est ainsi appelée car Octavius y avait implanté une colonie romaine.-->. Et nous avons séjourné quelques jours dans cette ville.

### Conversion de Ludia (Lydie)

16:13	Le jour du shabbat, nous sommes sortis hors de la ville, près d'une rivière où l'on avait coutume de faire la prière. Nous nous sommes assis pour parler aux femmes qui s'y étaient assemblées.
16:14	Et une certaine femme, du nom de Ludia, marchande de pourpre, de la ville de Thyatire, était une femme adoratrice d'Elohîm, et elle nous écoutait. Et le Seigneur a ouvert son cœur afin qu'elle soit attentive aux choses dont parlait Paulos.
16:15	Mais après avoir été baptisée avec sa maison, elle pria en disant : Si vous me jugez fidèle au Seigneur, entrez dans ma maison et demeurez-y. Et elle nous y força.

### Paulos et Silas battus de verges et emprisonnés

16:16	Or il arriva que, comme nous allions à la prière, une jeune esclave qui avait un esprit de Python<!--Vient de Putho (nom de la région où Delphe, le siège du fameux oracle, était situé). Dans la mythologie grecque, Python était le nom du serpent ou dragon qui demeurait au pied du Mont Parnasse, qui, dit-on, gardait l'oracle de Delphe, et fut tué par Apollon.-->, et qui, en devinant, procurait beaucoup de profit à ses maîtres, est venue à notre rencontre.
16:17	Et elle nous suivait, Paulos et nous, en criant et disant : Ces hommes sont les esclaves de l'Elohîm Très-Haut et ils vous annoncent la voie du salut !
16:18	Et elle faisait cela depuis plusieurs jours. Mais Paulos, fatigué, se retourna et dit à l'esprit : Je t'ordonne, au Nom de Yéhoshoua Mashiah, de sortir d'elle. Et il sortit à l'heure même.
16:19	Mais ses maîtres voyant que l'espérance de leur profit s'en était allée, se saisirent de Paulos et de Silas et les traînèrent à l'Agora<!--Voir commentaire en Ac. 17:17.-->, devant les magistrats.
16:20	Et les ayant amenés aux strategos, ils dirent : Ces hommes, qui sont juifs, troublent notre ville,
16:21	et ils annoncent des coutumes qu'il ne nous est pas permis de recevoir, ni de pratiquer, à nous qui sommes romains.
16:22	Et la foule se souleva aussi contre eux, et les strategos, ayant fait déchirer leurs vêtements, ordonnèrent qu'ils soient battus de verges.
16:23	Et après qu’ils les eurent frappés de nombreux coups, ils les jetèrent en prison, en recommandant au geôlier de les garder sûrement.
16:24	Celui-ci ayant reçu cet ordre, les jeta dans la prison intérieure et leur serra les pieds dans le bois.

### Libération miraculeuse de Paulos et Silas ; conversion du geôlier et de sa famille

16:25	Mais vers minuit, Paulos et Silas étant en prière, chantaient les hymnes pascals<!--Chant d'hymnes pascals, les Psaumes 113 à 118 et 136, que les Juifs appelaient le « grand Hallel ».--> à Elohîm, et les prisonniers les entendaient.
16:26	Et soudainement, il se fit un grand tremblement de terre, en sorte que les fondements de la prison furent ébranlés. Toutes les portes se sont immédiatement ouvertes et les liens de tous furent desserrés.
16:27	Mais le geôlier, étant sorti du sommeil et voyant les portes de la prison ouvertes, tira son épée et était sur le point de se tuer, pensant que les prisonniers s'étaient enfuis.
16:28	Mais Paulos cria d'une grande voix, disant : Ne te fais pas de mal, nous sommes tous ici.
16:29	Ayant alors demandé de la lumière, il entra précipitamment et tomba tout tremblant aux pieds de Paulos et de Silas.
16:30	Il les fit sortir et dit : Seigneurs, que faut-il que je fasse pour être sauvé ?
16:31	Et ils dirent : Crois au Seigneur Yéhoshoua Mashiah et tu seras sauvé, toi et ta maison.
16:32	Et ils lui annoncèrent la parole du Seigneur, et à tous ceux qui étaient dans sa maison.
16:33	Et les ayant pris à cette heure même de la nuit, il lava leurs plaies, et il fut baptisé, lui et tous les siens, immédiatement.
16:34	Et les ayant amenés dans sa maison, il dressa une table et il se réjouit fortement avec toute sa famille d’avoir cru en Elohîm.

### Paulos et Silas relâchés

16:35	Or le jour étant venu, les strategos envoyèrent des licteurs<!--« Quelqu'un qui porte les verges c'est-à-dire les faisceaux », « un licteur, officier public, qui porte les faisceaux bâton, et autres insignes de fonction devant un magistrat ».--> pour dire : Relâche ces hommes.
16:36	Et le geôlier rapporta ces paroles à Paulos, disant : Les strategos ont envoyé des hommes pour qu’on vous relâche. Sortez donc maintenant et partez dans la paix !
16:37	Mais Paulos leur dit : Après nous avoir battus<!--« Écorcher », « enlever la peau ».--> de verges publiquement et sans jugement, nous, des hommes, des citoyens romains, ils nous ont jetés en prison et maintenant ils nous jettent dehors secrètement ! Non en effet, mais qu'ils viennent eux-mêmes et nous conduisent dehors !
16:38	Et les licteurs rapportèrent ces paroles aux strategos, qui furent effrayés en apprenant qu'ils sont romains.
16:39	Et étant venus, ils les exhortèrent et, les ayant conduits dehors, ils les supplièrent de quitter la ville.
16:40	Mais étant sortis de la prison, ils entrèrent chez Ludia et, après avoir vu et consolé les frères, ils partirent.

## Chapitre 17

### Paulos (Paul) et Silas à Thessalonique

17:1	Et ayant traversé Amphipolis et Apollonie, ils arrivèrent à Thessalonique où était la synagogue des Juifs.
17:2	Et Paulos y entra, selon sa coutume. Pendant trois shabbats, il discuta avec eux, d'après les Écritures,
17:3	ouvrant complètement ce qui a été fermé et expliquant qu'il fallait que le Mashiah souffre, et qu'il ressuscite des morts et que ce Mashiah est Yéhoshoua que je vous annonce.
17:4	Et quelques-uns d'entre eux crurent et se joignirent à Paulos et à Silas, ainsi qu'une grande multitude de Grecs adorateurs et des femmes du premier rang, non en petit nombre.

### Émeute à Thessalonique

17:5	Mais les Juifs rebelles, émus d'envie, prirent avec eux quelques hommes méchants qui traînaient sur la place du marché et, ayant rassemblé une foule, ils firent du bruit dans la ville. Et s'étant précipités vers la maison de Iason, ils les cherchèrent pour les amener vers le peuple.
17:6	Mais ne les ayant pas trouvés, ils traînèrent Iason et quelques frères devant les magistrats de la ville, en criant : Ces gens, qui ont agité la terre habitée, sont aussi venus ici, 
17:7	et Iason les a reçus chez lui. Et ceux-ci agissent tous contre les ordonnances de César, disant qu'il y a un autre Roi, Yéhoshoua.
17:8	Et ils troublèrent la foule et les magistrats de la ville, qui, entendant ces choses,
17:9	et après avoir reçu satisfaction de Iason et des autres, ils relâchèrent.

### Paulos et Silas fuient à Bérée

17:10	Et les frères envoyèrent immédiatement à Bérée, pendant la nuit, Paulos et Silas, qui, étant arrivés, entrèrent dans la synagogue des Juifs.
17:11	Or ceux-ci avaient l'esprit plus noble que ceux de Thessalonique. Ils reçurent la parole avec tout empressement, examinant tous les jours les Écritures pour voir s'il en était bien ainsi.
17:12	En effet, beaucoup d'entre eux crurent donc, ainsi que des femmes grecques de distinction et des hommes, non en petit nombre.
17:13	Mais quand les Juifs de Thessalonique surent qu'à Bérée aussi la parole d'Elohîm était annoncée par Paulos, ils y allèrent, agitant les foules.
17:14	Mais immédiatement, les frères envoyèrent donc Paulos en avant, vers la mer, mais Silas et Timotheos restèrent là.

### Paulos à Athènes

17:15	Et ceux qui s'étaient chargés de Paulos, le conduisirent jusqu'à Athènes et, après avoir reçu pour Silas et pour Timotheos l'ordre de venir très rapidement vers lui, ils s’en allèrent.
17:16	Mais pendant que Paulos les attendait à Athènes, son esprit s'irritait en lui-même, en voyant que cette ville était pleine d'idoles.
17:17	En effet, il discutait donc dans la synagogue avec les Juifs et les hommes adorateurs, et chaque jour sur l'Agora<!--Agora qui signifie « assemblée » était le lieu de rassemblement des citoyens pour les débats publics, les élections et les procès. Dans la Grèce antique, l'Agora était le marché le plus important et le plus fréquenté de la cité. Elle était une composante essentielle du concept de la ville grecque, à tel point qu'Aristote qualifiera les barbares de non civilisés, car ils n'avaient pas d'agora. L'Agora est l'équivalent du forum romain. À Athènes, l'Agora était le centre de la vie sociale où il fallait absolument se rendre pour philosopher entre amis. C'était la place publique la plus importante de la cité où l'on pouvait faire toutes sortes de rencontres. Voir Mt. 11:16, 20:3, 23:7 ; Mc. 6:56, 7:4, 12:38 ; Lu. 7:32, 11:43, 20:46 ; Ac. 17:17.--> avec ceux qui s'y rencontraient.
17:18	Mais quelques-uns des philosophes épicuriens<!--L'épicurisme a été fondé par Épicure (341 – 270 av. J.-C.). Cette philosophie est axée sur la recherche du bonheur par l'évitement de la souffrance et des inquiétudes (ataraxie).--> et stoïciens<!--Les stoïciens étaient disciples de Zénon (336 – 264 av. J.-C.). Leur philosophie se fondait sur la conception d'un homme se suffisant à lui-même, sur une discipline rigoureuse et sur la solidarité du genre humain.--> conversèrent avec lui. Et les uns disaient : Que veut dire ce picoreur de semences<!--Vient du grec « spermologos » qui signifie « qui picore la semence ». Ce terme fait allusion aux oiseaux, en particulier au corbeau ou au choucas qui mangent les graines dans les champs. Au sens figuré, il est question de ceux qui fréquentaient les places de marchés (Agora) pour récupérer tout ce qui peut tomber, avec un peu de chance, d'un chargement de marchandise.--> ? Les autres : Il semble être un prédicateur de divinités étrangères. C'est parce qu'il leur prêchait Yéhoshoua et la résurrection.
17:19	Alors ils le prirent avec eux et le conduisirent à l'Aréopage<!--À l'origine, l'Aréopage désignait le tribunal d'Athènes qui siégeait sur la colline d'Arès. Le sens figuré est le suivant : « assemblée de juges, de savants, d'hommes de lettres très compétents ».-->, en disant : Pourrions-nous savoir quelle est cette nouvelle doctrine dont tu parles ?
17:20	Car tu nous remplis les oreilles de certaines choses étranges. Nous voudrions donc savoir ce que veulent dire ces choses.
17:21	Or tous les Athéniens et les étrangers qui demeuraient à Athènes, ne passaient leur temps qu'à dire ou à écouter quelque chose de nouveau.

### Prédication de Paulos à l'Aréopage

17:22	Mais Paulos, debout au milieu de l'Aréopage, leur dit : Hommes athéniens, je vous vois en toutes choses comme plus superstitieux.
17:23	Car, en passant et en regardant attentivement les objets<!--Il est question des temples, des autels, des statues et des images idolâtres.--> de votre culte, j'ai même trouvé un autel sur lequel était écrit : À un elohîm inconnu<!--Oublié.--> ! Celui que vous révérez sans le connaître, c'est celui que je vous annonce.
17:24	L'Elohîm qui a fait le monde et toutes les choses qui y sont, lui qui est le Seigneur du ciel et de la Terre, n'habite pas dans des temples faits de mains d’hommes.
17:25	Et il n'est pas servi par les mains des humains, comme s'il avait besoin de quelque chose, lui qui donne à tous la vie et la respiration et toutes choses.
17:26	Et il a fait habiter, sur toute la face de la Terre, toute nation d'humains<!--Voir Ge. 1:26.--> sortis d'un seul sang, ayant déterminé les temps définis d'avance et les bornes de leur habitation,
17:27	pour chercher le Seigneur, pour voir si en le cherchant à tâtons, ils le trouveraient, quoiqu’il ne soit pas loin de chacun de nous.
17:28	Car c'est par lui que nous vivons, que nous nous mouvons et que nous sommes. C'est ce qu'ont dit quelques-uns même de vos poètes : Car nous sommes aussi sa race.
17:29	Étant donc la race d'Elohîm, nous ne devons pas penser que le Divin soit semblable à de l'or, à de l'argent ou à de la pierre, sculptés par l'art et la pensée des humains.
17:30	En effet, sans tenir compte des temps d'ignorance, Elohîm ordonne maintenant à tous les humains, en tous lieux, de se repentir,
17:31	parce qu'il a arrêté un jour dans lequel il va juger la terre habitée avec justice, par l'homme qu'il a désigné, ayant fourni à tous la foi en le ressuscitant des morts.
17:32	Mais lorsqu'ils entendirent parler de la résurrection des morts, en effet les uns se moquèrent, et les autres dirent : Nous t'entendrons là-dessus une autre fois.
17:33	Et c'est ainsi que Paulos sortit du milieu d'eux.
17:34	Mais quelques hommes se joignirent à lui et crurent. Parmi eux Dyonisos<!--L'elohîm du vin.-->, l'Aréopagite<!--Membre de l'aréopage d'Athènes faisant partie des anciens, dont l'expérience politique et l'autorité morale étaient réputées.-->, une femme du nom de Damaris et d'autres avec eux.

## Chapitre 18

### Paulos (Paul) enseigne à Corinthe pendant un an et demi

18:1	Mais après cela, Paulos partit<!--Vient du grec « chorizo » qui signifie aussi « séparer », « diviser », « partager », « mettre en morceaux », « se séparer de », « laisser un mari ou une épouse », « divorce ».--> d'Athènes et se rendit à Corinthe.
18:2	Il y trouva un Juif du nom d'Akulas, originaire du Pont, récemment arrivé d'Italie avec Priscilla sa femme parce que Claude avait ordonné à tous les Juifs de sortir de Rome. Il s'approcha d'eux,
18:3	et parce qu'il était de même métier, il demeura chez eux et il travaillait. Car ils étaient faiseurs de tentes de leur métier.
18:4	Or il discourait dans la synagogue chaque shabbat et il persuadait des Juifs et des Grecs.
18:5	Mais quand Silas et Timotheos furent descendus de Macédoine, Paulos était pressé de chaque côté par l'Esprit, rendant témoignage aux Juifs du Mashiah Yéhoshoua.
18:6	Mais comme ils résistaient et qu'ils blasphémaient, ayant secoué ses vêtements, il leur dit : Votre sang est sur votre tête ! Moi, j'en suis pur ! Dès maintenant, j'irai vers les nations.
18:7	Et sortant de là, il entra dans la maison d’un nommé Juste, adorateur d'Elohîm, et dont la maison était contiguë à la synagogue.
18:8	Mais Crispos, le chef de la synagogue, crut au Seigneur avec toute sa maison, et beaucoup de Corinthiens l'entendant, croyaient et étaient baptisés.
18:9	Et le Seigneur dit à Paulos par le moyen d'une vision pendant la nuit : N'aie pas peur, mais parle et ne te tais pas,
18:10	parce que moi, je suis avec toi, et nul ne t'attaquera pour te maltraiter, parce que j'ai un peuple nombreux dans cette ville.
18:11	Il s'établit là un an et six mois, enseignant parmi eux la parole d'Elohîm.

### Soulèvement des Juifs contre Paulos

18:12	Mais pendant que Gallion était proconsul<!--Député.--> de l'Achaïe, les Juifs se soulevèrent d'un commun accord contre Paulos et le menèrent devant le tribunal,
18:13	en disant : Celui-ci agite par persuasion les humains à adorer Elohîm d'une manière contraire à la torah.
18:14	Mais comme Paulos était sur le point d'ouvrir la bouche, Gallion dit aux Juifs : Ô Juifs ! S'il était vraiment question de quelque iniquité ou de quelque méchante action, je vous soutiendrais autant qu'il serait raisonnable.
18:15	Mais s'il est question de discours et de noms, et d'une torah qui vous soit propre, voyez vous-mêmes, car je ne veux pas être juge de ces choses.
18:16	Et il les renvoya du tribunal.
18:17	Mais tous les Grecs se saisirent de Sosthènes, le chef de la synagogue, le battirent devant le tribunal, et Gallion ne prit soin d'aucune de ces choses.

### Paulos fait un vœu<!--Ga. 3:23-28 ; 2 Co. 3:7-14 ; Ro. 6:14.-->

18:18	Mais Paulos étant encore resté un assez grand nombre de jours, prit congé des frères et s'embarqua pour la Syrie, ayant avec lui Priscilla et Akulas, après s'être fait raser la tête à Cenchrées, car il avait fait un vœu.
18:19	Et il arriva à Éphèse, et les y laissa. Mais pour lui, étant entré dans la synagogue, il s'entretint avec les Juifs.
18:20	Et ceux-ci le priant de rester encore plus longtemps avec eux, il n'y consentit pas.
18:21	Mais il prit congé d'eux, disant : Il faut absolument que je fasse la fête prochaine à Yeroushalaim. Mais je reviendrai vers vous si Elohîm le veut. Et d’Éphèse il prit la mer.
18:22	Et étant débarqué à Césarée, il monta saluer l’assemblée et descendit à Antioche.
18:23	Et ayant séjourné là quelque temps, il s’en alla et traversa successivement le pays de Galatie et la Phrygie, fortifiant davantage tous les disciples<!--Voir annexe « Les voyages missionnaires de Paulos (Paul) ».-->.

### Apollos annonce l'Évangile à Éphèse et à Corinthe

18:24	Mais un certain juif du nom d'Apollos, originaire d'Alexandrie, homme éloquent et puissant dans les Écritures, vint à Éphèse.
18:25	Il avait été instruit dans la voie du Seigneur et, bouillant de chaleur de l'esprit, il annonçait et enseignait avec précision ce qui concerne Yéhoshoua, bien qu'il ne connaisse que le baptême de Yohanan.
18:26	Il commença donc à parler avec assurance dans la synagogue. Et quand Akulas et Priscilla l'eurent entendu, ils le prirent avec eux et lui exposèrent plus exactement la Voie d'Elohîm.
18:27	Et comme il voulait passer en Achaïe, les frères écrivirent aux disciples et les exhortèrent à le recevoir. Quand il fut arrivé, il aida beaucoup ceux qui avaient cru par le moyen de la grâce.
18:28	Car il réfutait avec véhémence les Juifs en public, démontrant par les Écritures que le Mashiah, c'est Yéhoshoua.

## Chapitre 19

### Paulos (Paul) enseigne à Éphèse<!--V. 9-10 ; Ac. 20:31.-->

19:1	Or il arriva, pendant qu'Apollos était à Corinthe, que Paulos, après avoir traversé les contrées supérieures, arriva à Éphèse. Et ayant trouvé quelques disciples, 
19:2	il leur dit : Avez-vous reçu le Saint-Esprit après avoir cru ? Et ils lui dirent : Mais nous n'avons même pas entendu dire qu'il y ait un Esprit Saint.
19:3	Et il leur dit : Dans quoi donc avez-vous été baptisés ? Et ils dirent : Dans le baptême de Yohanan.
19:4	Mais Paulos dit : En effet, Yohanan a baptisé du baptême de repentance, en disant au peuple de croire en celui qui venait après lui, c'est-à-dire, en Yéhoshoua Mashiah.
19:5	Mais après avoir entendu cela, ils furent baptisés dans le Nom du Seigneur Yéhoshoua.
19:6	Et lorsque Paulos leur eut imposé les mains, le Saint-Esprit vint sur eux et, non seulement ils parlaient en langues, mais ils prophétisaient même.
19:7	Et ils étaient en tout environ 12 hommes.
19:8	Mais, étant entré dans la synagogue, il parla avec assurance pendant trois mois, discourant et les persuadant en ce qui concerne le Royaume d'Elohîm.
19:9	Mais comme quelques-uns s'endurcissaient et étaient rebelles, parlant mal de la Voie<!--Voir commentaire en Actes 9:2.--> devant la multitude, il s'éloigna d'eux, sépara les disciples, discourant chaque jour dans l'école d'un certain Turannos.
19:10	Et cela se fit durant 2 ans, de sorte que tous ceux qui habitaient l'Asie, Juifs et Grecs, entendirent la parole du Seigneur Yéhoshoua.

### Elohîm fait des prodiges à Éphèse

19:11	Et Elohîm faisait des miracles extraordinaires par les mains de Paulos,
19:12	au point qu'on appliquait même sur les malades des mouchoirs ou des tissus qui avaient touché son corps, et ils étaient guéris de leurs maladies, et les esprits mauvais sortaient d'eux.

### Les sept fils de Scéva

19:13	Mais quelques-uns d'entre les exorcistes ambulants, juifs, entreprirent de prononcer le Nom du Seigneur Yéhoshoua sur ceux qui étaient possédés d'esprits mauvais, en disant : Je vous conjure par ce Yéhoshoua que Paulos prêche !
19:14	Or c'étaient 7 fils de Scéva, un principal prêtre juif, qui faisaient cela.
19:15	Mais l'esprit mauvais répondant leur dit : Je connais Yéhoshoua et je sais qui est Paulos. Mais vous, qui êtes-vous ?
19:16	Et l'homme en qui était l'esprit mauvais sautant sur eux, les maîtrisa l’un après l’autre avec une telle force qu'ils s'enfuirent de cette maison nus et blessés.
19:17	Et cela fut connu de tous ceux qui habitaient Éphèse, tant juifs que grecs, et une terreur tomba sur eux tous, et le Nom du Seigneur Yéhoshoua était magnifié<!--« Exalter », « recevoir gloire et louange ». Voir Lu. 1:46 ; Ac. 10:46.-->.
19:18	Et beaucoup de ceux qui avaient cru, venaient, confessant et déclarant leurs mauvaises actions.
19:19	Et un assez grand nombre de ceux qui avaient pratiqué les arts magiques apportèrent leurs livres et les brûlèrent devant tous. On en estima la valeur et on en trouva pour 50 000 pièces d'argent.
19:20	Ainsi, par la force souveraine, la parole du Seigneur croissait et se fortifiait.
19:21	Or comme ces choses furent accomplies, Paulos forma le projet par l'Esprit, d'aller à Yeroushalaim, en traversant la Macédoine et l'Achaïe. Quand j'y serai allé, se disait-il, il faut aussi que je voie Rome.
19:22	Mais il envoya en Macédoine deux de ceux qui le servaient, Timotheos et Erastos, et il resta lui-même quelque temps en Asie.

### Démétrios suscite une émeute

19:23	Or en ce temps-là, il y eut une agitation qui ne fut pas peu de chose concernant la Voie<!--Ou « doctrine ».-->.
19:24	Car, un certain homme nommé Démétrios, un orfèvre, en fabriquant des temples d'Artémis<!--La déesse Artémis est aussi appelée Diane.--> en argent, procurait aux artisans un profit non-négligeable.
19:25	Il les réunit ainsi que les travailleurs des métiers similaires et dit : Hommes, vous savez que ce profit est notre fortune,
19:26	et vous voyez et entendez, que non seulement à Éphèse, mais dans presque toute l'Asie, ce Paulos a persuadé et détourné une grande foule en disant que ce ne sont pas des elohîm, ceux qui sont faits par des mains.
19:27	Et non seulement il y a du danger pour nous que cette partie ne tombe en discrédit, mais aussi que le temple de la grande déesse Artémis ne soit compté pour rien, et celle que toute l'Asie et toute la terre habitée adorent, est sur le point d'être dépouillée de sa grandeur !
19:28	Mais ayant entendu cela, et remplis de colère, ils criaient disant : Grande est l'Artémis des Éphésiens !
19:29	Et toute la ville fut remplie de confusion. Et ils se précipitèrent d'un commun accord dans le théâtre, et enlevèrent Gaïos et Aristarchos, Macédoniens, compagnons de voyage de Paulos.
19:30	Et comme Paulos voulait entrer vers le peuple, les disciples ne le lui permirent pas.
19:31	Et même quelques Asiarques<!--Chef dans la province romaine d'Asie. Chacune des villes de l'Asie pro-consulaire assemblait ses plus honorables citoyens, lors de l'équinoxe d'automne, pour présider aux jeux de l'année donnés en l'honneur des dieux et de l'Empereur Romain. Une assemblée générale se tenait à Éphèse, ou Smyrne, ou Sardes, pour élire dix membres, puis le proconsul en choisissait un qui présidait.--> qui étaient ses amis envoyèrent quelqu'un vers lui pour le prier de ne pas se présenter au théâtre.
19:32	Les uns en effet criaient d'une manière, les autres d'une autre, car l'assemblée était confuse, et la plupart ne savaient pas pourquoi ils s'étaient rassemblés.
19:33	Et Alexandros fut traîné hors de la foule, les Juifs le poussant en avant, et Alexandros, faisant signe de la main, voulait se défendre devant le peuple.
19:34	Mais quand ils reconnurent qu'il est juif, tous d'une seule voix crièrent pendant deux heures : Grande est l'Artémis des Éphésiens !
19:35	Mais le scribe ayant apaisé la foule dit : Hommes éphésiens, quel est donc l'humain qui ne sache pas que la ville des Éphésiens est la gardienne<!--Celle qui balaye et nettoie un temple, celui qui a charge d'un temple, le garde et le décore, un sacristain, c'était un titre honoraire dans certaines cités (gardien du temple) spécialement en Asie Mineure. Dans ces cités, on adorait une divinité. Et certaines règles d'adoration avaient été établies par les hommes.--> de la grande déesse Artémis et de son image tombée de Zeus<!--Ou « tombée de Jupiter », c'est-à-dire du ciel.--> ?
19:36	Ces choses étant donc incontestables, il faut que vous vous apaisiez et que vous ne fassiez rien avec précipitation.
19:37	Car vous avez amené ces hommes qui ne sont ni sacrilèges, ni blasphémateurs de votre déesse.
19:38	Si donc Démétrios et les artisans qui sont avec lui ont en effet affaire à quelqu'un, il y a des tribunaux<!--Vient du grec « agoraios » qui signifie : sur ou aux environs de la place publique, la place de marché, ceux qui fréquentent cette place, trafiquants insignifiants, marchands au détail, désœuvrés, flâneurs, le peuple commun, la populace, généralement, propre à l'assemblée, procès judiciaire, transactions. Agoraios vient de « Agora » (le lieu de l'assemblée).-->, il y a des proconsuls : qu'ils s'accusent les uns les autres !
19:39	Et si vous réclamez quelque chose sur d'autres sujets, on en décidera dans l'assemblée légale.
19:40	Et nous sommes en danger d'être accusés de sédition pour celle d’aujourd’hui, car il n'existe aucune cause qui nous permette de rendre compte de cette affluence de personnes désordonnées<!--Une combinaison secrète, une coalition, une conspiration, un complot, une affluence de personnes désordonnées, une émeute. Voir Ac. 23:12.-->. 
19:41	Et ayant dit cela, il congédia l'assemblée<!--Ou église.-->.

## Chapitre 20

### Paulos (Paul) en Macédoine, en Grèce et à Troas

20:1	Or, après que le tumulte eut cessé, Paulos ayant appelé les disciples et les ayant salués, sortit pour aller en Macédoine.
20:2	Mais ayant traversé les régions, et les ayant exhortés par beaucoup de discours, il vint en Grèce,
20:3	où il passa trois mois. Les Juifs ayant formé un complot contre lui, alors qu'il était sur le point de s'embarquer pour la Syrie, il fut d'avis de s'en retourner par la Macédoine.
20:4	Et il avait pour l'accompagner jusqu'en Asie : Sopatros, un habitant de Bérée, Aristarchos et Second de Thessalonique, Gaïos de Derbe, Timotheos, ainsi que Tuchikos et Trophimos, originaires d'Asie.
20:5	Ceux-ci prirent les devants et nous attendirent à Troas.
20:6	Mais pour nous, après les jours des pains sans levain, nous avons embarqué à Philippes et, au bout de 5 jours, nous les avons rejoints à Troas où nous avons séjourné 7 jours.

### Résurrection d'Eutychos

20:7	Mais l'un des shabbats<!--« Sabbaton » en grec : un shabbat, un jour de shabbat. C'est le septième jour de chaque semaine qui était une fête sacrée, pour lequel les Israélites devaient s'abstenir de tout travail. Les disciples rompaient le pain le jour du shabbat.-->, les disciples étant rassemblés pour rompre le pain, Paulos, qui était sur le point de partir le lendemain, discourait avec eux et il prolongea son discours jusqu’à minuit.
20:8	Or, il y avait beaucoup de lampes dans la chambre haute où ils étaient rassemblés.
20:9	Or, un certain jeune homme du nom d'Eutychos était assis sur une fenêtre. Il s'endormit profondément pendant le long discours de Paulos et, entraîné par le sommeil, il tomba du troisième étage en bas. On le releva, mais il était mort.
20:10	Mais Paulos étant descendu, se coucha sur lui et, l'ayant embrassé, dit : Ne faites pas tout ce bruit<!--Mc. 5:39.-->, car son âme est en lui.
20:11	Mais quand il fut monté, il rompit le pain, mangea et parla assez longtemps, jusqu'au point du jour, et partit ainsi.
20:12	Et ils amenèrent le jeune homme vivant, et ils ne furent pas modérément consolés.
20:13	Mais pour nous, ayant pris les devants, nous nous sommes embarqués sur un bateau pour Assos, où nous étions sur le point de prendre Paulos : c’est ainsi qu’il l’avait ordonné, car il était le point de faire lui-même le voyage à pied.
20:14	Mais lorsqu'il nous a rejoints à Assos, nous l'avons pris à bord et sommes allés à Mytilène.
20:15	Nous avons embarqué le lendemain et, de là, nous sommes arrivés en face de Chios. Et le jour suivant, nous nous sommes rendus vers Samos, et après être restés à Trogyllion, nous sommes arrivés à Milet.
20:16	Car Paulos avait jugé bon de passer au large d'Éphèse, afin qu'il ne lui arrivât pas de perdre du temps en Asie, car il se hâtait pour être, s'il lui était possible, à Yeroushalaim le jour de la pentecôte.

### Paulos exhorte les anciens d'Éphèse

20:17	Mais de Milet<!--Ville importante de la Ionie, à environ 60 km au sud d'Éphèse, sur les confins de la Carie ; elle était sur la rive méridionale de la baie de Latmus, où se jetait le Méandre. Ville natale de Thalès, Démocrite, et Anaximandre. La ville possédait un temple célèbre dédié à Apollon. Milétos est le fils d'Apollon et le fondateur de la cité de Milet en Asie mineure.-->, il envoya appeler à Éphèse les anciens de l'assemblée.
20:18	Et lorsqu'ils furent arrivés vers lui, il leur dit : Vous savez de quelle manière je me suis toujours conduit avec vous, dès le premier jour où j'ai mis le pied en Asie.
20:19	Servant le Seigneur en toute humilité, et avec beaucoup de larmes et de tentations qui me sont arrivées par les complots des Juifs.
20:20	Comment je ne vous ai rien caché des choses qui étaient utiles, ne manquant pas de vous les annoncer et de vous les enseigner publiquement et dans les maisons,
20:21	rendant témoignage, tant aux Juifs qu’aux Grecs, de la repentance envers Elohîm et de la foi en Yéhoshoua Mashiah, notre Seigneur.
20:22	Et maintenant voici que, lié par l'Esprit, je vais à Yeroushalaim ne sachant pas les choses qui m'y arriveront.
20:23	Sauf que, de ville en ville, l’Esprit Saint rend témoignage et dit que des liens et des tribulations m'attendent.
20:24	Mais je ne fais aucun cas de mon âme comme m'étant précieuse, pourvu que j'accomplisse ma course avec joie et le service que j'ai reçu du Seigneur Yéhoshoua : rendre témoignage à l'Évangile de la grâce d'Elohîm.
20:25	Et maintenant voici, je sais que vous ne verrez plus mon visage, vous tous au milieu desquels je suis passé en prêchant le Royaume d'Elohîm.
20:26	C'est pourquoi je vous témoigne aujourd'hui que je suis pur du sang de tous,
20:27	car je vous ai annoncé tout le conseil d'Elohîm, sans en rien cacher.
20:28	Prenez donc garde à vous-mêmes et à tout le troupeau parmi lequel le Saint-Esprit vous a établis surveillants<!--« Episcopos » en grec : « surveillant, gardien », généralement traduit par « évêque ». Ce terme désigne la fonction des anciens. Dans la nouvelle alliance, les surveillants (ou anciens) sont des personnes dont la mission est de veiller au bon fonctionnement des assemblées locales. Yéhoshoua ha Mashiah (Jésus-Christ) notre Elohîm, est le Surveillant par excellence (1 Pi. 2:25).-->, pour paître l'Assemblée d'Elohîm, qu'il a acquise<!--Ps. 74:2.--> au moyen de son propre sang.
20:29	Car je sais ceci : qu'après mon départ il entrera parmi vous des loups impitoyables qui n'épargneront pas le troupeau,
20:30	et que, d'entre vous-mêmes, s'élèveront des hommes qui diront des choses perverties pour entraîner les disciples après eux.
20:31	C'est pourquoi, veillez, vous souvenant que durant l'espace de trois ans, je n'ai cessé nuit et jour d'avertir chacun de vous avec larmes.
20:32	Et maintenant, frères, je vous remets à Elohîm et à la parole de sa grâce, à celui qui est puissant pour achever de vous édifier et pour vous donner l'héritage avec tous les sanctifiés.
20:33	Je n'ai désiré ni l'argent, ni l'or, ni l'habit de personne.
20:34	Mais vous savez vous-mêmes que, pour mes besoins et pour ceux qui étaient avec moi, ces mains y ont servi.
20:35	Je vous ai montré en toutes choses que c'est ainsi qu'il faut aider les faibles en travaillant dur, et se rappeler les paroles du Seigneur Yéhoshoua, parce que lui-même a dit : Il y a plus de bénédiction à donner qu'à recevoir<!--Lu. 14:12-14.-->.
20:36	Et quand il eut dit ces choses, il se mit à genoux et pria avec eux tous.
20:37	Et il y eut bien des pleurs de la part de tous et, se jetant au cou de Paulos, ils l'embrassèrent tendrement,
20:38	étant principalement affligés à cause de la parole qu'il avait dite, qu'ils étaient sur le point de ne plus voir son visage. Et ils l'accompagnèrent jusqu'au navire.

## Chapitre 21

### Paulos (Paul) et ses compagnons à Tyr

21:1	Mais il arriva qu'après nous être arrachés à eux, et nous être embarqués, nous avons filé tout droit et sommes arrivés à Cos, et le jour suivant à Rhodes, et de là à Patara.
21:2	Et ayant trouvé un navire qui faisait la traversée vers la Phénicie, nous avons embarqué et nous avons pris la mer.
21:3	Mais arrivés en vue de Chypre, nous l'avons laissée à gauche et nous avons navigué vers la Syrie, pour débarquer à Tyr, car le navire devait y décharger sa cargaison.
21:4	Et nous avons trouvé des disciples et sommes restés là 7 jours. Ils disaient à Paulos, par l'Esprit, de ne pas monter à Yeroushalaim.
21:5	Mais quand est venu pour nous le terme de ces jours, nous sommes sortis et nous avons poursuivi le voyage, eux tous nous accompagnant avec femmes et enfants jusque hors de la ville et, nous nous sommes agenouillés sur le rivage et avons prié.
21:6	Et après nous être salués<!--Utilisé pour ceux qui accostent quelqu'un d'autre, qui vont faire une courte visite ; montrer son respect par une visite. Les salutations ne consistaient pas dans un simple geste amical et quelques mots, mais dans de grandes embrassades. Un voyage était souvent retardé par les manifestations d'adieux.--> les uns les autres, nous sommes montés sur le navire, et ils sont retournés chez eux.

### Escales à Ptolémaïs puis à Césarée

21:7	Mais nous, achevant le voyage sur mer, nous sommes allés de Tyr à Ptolémaïs et, ayant salué les frères, nous sommes restés un jour avec eux.
21:8	Mais le lendemain, ceux qui étaient avec Paulos étant partis, nous sommes arrivés à Césarée. Étant entrés dans la maison de Philippos, l'évangéliste, qui était l'un des sept, nous sommes restés chez lui.
21:9	Et il avait quatre filles vierges qui prophétisaient.

### Prophétie d'Agabos

21:10	Mais comme nous restions là plusieurs jours, un certain prophète du nom d'Agabos, arriva de Judée
21:11	qui, étant venu vers nous et ayant pris la ceinture de Paulos et, s'étant lié les mains et les pieds, dit : Ainsi dit le Saint-Esprit : L'homme à qui appartient cette ceinture, les Juifs le lieront de la même manière à Yeroushalaim et le livreront entre les mains des nations.
21:12	Mais quand nous avons entendu ces choses, nous l'avons supplié, nous et ceux qui habitaient dans ce lieu, de ne pas monter à Yeroushalaim.
21:13	Mais Paulos répondit : Que faites-vous en pleurant et en affligeant mon cœur ? Car pour moi je suis prêt, non seulement à être lié, mais aussi à mourir à Yeroushalaim en faveur du Nom du Seigneur Yéhoshoua.
21:14	Mais comme il ne se laissait pas persuader, nous nous sommes tenus tranquilles, disant : Que la volonté du Seigneur soit faite !

### Arrivée à Yeroushalaim (Jérusalem)

21:15	Et après ces jours-là, ayant préparé nos biens pour les déménager, nous sommes montés à Yeroushalaim<!--Voir annexe « Les voyages missionnaires de Paulos (Paul) ».-->.
21:16	Et des disciples aussi sont venus avec nous de Césarée, nous conduisant chez un certain Mnason, Chypriote, un ancien disciple, chez qui nous devions loger.
21:17	Et lorsque nous sommes arrivés à Yeroushalaim, les frères nous ont accueillis avec joie.
21:18	Et le jour suivant, Paulos s'est rendu avec nous chez Yaacov, et tous les anciens étaient présents.
21:19	Et après les avoir salués, il raconta une à une toutes les choses qu'Elohîm avait faites au milieu des nations au moyen de son service.
21:20	Et après l'avoir entendu, ils glorifiaient le Seigneur. Et ils lui dirent : Tu vois, frère, combien il y a de myriades de Juifs qui ont cru, et ils sont tous zélateurs de la torah.
21:21	Or ils ont été informés à ton sujet que tu enseignes à tous les Juifs qui sont parmi les nations, l'apostasie à l'égard de Moshé, en disant qu'ils ne doivent pas circoncire leurs enfants, ni marcher selon les coutumes.
21:22	Qu’en est-il donc ? Il faut sûrement assembler la multitude, car ils apprendront que tu es venu.
21:23	C'est pourquoi fais ce que nous te disons : Nous avons quatre hommes ayant sur eux un vœu.
21:24	Prends-les et purifie-toi avec eux, et paye leur dépense, afin qu'ils se rasent la tête. Et tous sauront que ce qu’ils ont entendu dire de toi n’est rien, mais que toi aussi, tu marches en gardant la torah.
21:25	Mais concernant ceux des nations qui ont cru, nous leur avons écrit, jugeant qu'ils ne devaient rien observer de semblable, mais seulement qu'ils s'abstiennent de ce qui est sacrifié aux idoles, du sang, de ce qui est étouffé et de relation sexuelle illicite.
21:26	Alors Paulos prit ces hommes, se purifia et entra le lendemain dans le temple avec eux, déclarant l'accomplissement des jours de la purification pendant que l'offrande fut présentée en faveur de chacun d'eux.

### Paulos chassé du temple et brutalisé par les Juifs

21:27	Mais comme les 7 jours étaient sur le point de s’accomplir, les Juifs d'Asie l'ayant vu dans le temple, poussèrent à la révolte toute la foule et mirent les mains sur lui,
21:28	en criant : Hommes israélites, à l'aide ! C’est ici l’homme qui enseigne partout, à tout le monde, et contre la torah et contre ce lieu. Il a même introduit des Grecs dans le temple et a profané ce saint lieu.
21:29	Car ils avaient vu auparavant Trophimos l’Éphésien avec lui dans la ville, et ils pensaient que Paulos l'avait fait entrer dans le temple.
21:30	Toute la ville fut en mouvement et le peuple arriva en foule hostile. Et ayant saisi Paulos, ils le traînèrent hors du temple, et aussitôt les portes furent fermées.
21:31	Mais comme ils cherchaient à le tuer, le bruit vint au tribun de la cohorte que tout Yeroushalaim était en confusion.
21:32	Celui-ci, prenant tout de suite avec lui des soldats et des officiers de l'armée romaine et courut vers eux. Voyant le tribun et les soldats, ils cessèrent de frapper Paulos.
21:33	Alors le tribun s'étant approché, se saisit de lui et le fit lier de deux chaînes. Et il demanda qui il était et ce qu'il avait fait.
21:34	Mais les uns criaient d'une manière, et les autres d'une autre dans la foule. Ne pouvant donc rien apprendre de certain à cause du tumulte, il ordonna de le mener dans la forteresse.
21:35	Et lorsqu'il fut sur les marches, il arriva qu'il fut porté par les soldats à cause de la violence de la foule,
21:36	car la multitude du peuple le suivait, en criant : Enlève-le !
21:37	Comme on était sur le point de faire entrer Paulos dans la forteresse, il dit au tribun : Est-il légal pour moi de te dire quelque chose ? Et il répondit : Tu sais le grec ?
21:38	N'es-tu pas l'Égyptien qui, ces jours passés, a excité une sédition et conduit dans le désert les 4 000 hommes assassins ?
21:39	Mais Paulos dit : Je suis vraiment un homme juif de Tarse, citoyen de la ville renommée de la Cilicie. Permets-moi, je te prie, de parler au peuple.
21:40	Et quand il le lui permit, Paulos se tenant sur les marches fit signe de la main au peuple. Et s'étant fait un grand silence, il leur parla en langue hébraïque, en disant :

## Chapitre 22

### Paulos (Paul) raconte sa conversion<!--Ac. 9:1-18, 26:9-18.-->

22:1	Hommes frères et pères, écoutez maintenant ma plaidoirie auprès de vous !
22:2	Et lorsqu'ils entendirent qu'il leur parlait en langue hébraïque, ils firent encore plus silence. Et il dit :
22:3	Je suis vraiment un homme juif, né à Tarse en Cilicie, mais j'ai été élevé dans cette ville-ci aux pieds de Gamaliel, et instruit dans la connaissance exacte de la torah de nos pères, étant zélateur d'Elohîm, comme vous l'êtes tous aujourd'hui.
22:4	J'ai persécuté jusqu'à la mort la Voie<!--« Une manière de se conduire », « de penser », « de décider », « une doctrine ».-->, liant et mettant en prison hommes et femmes.
22:5	Comme aussi le grand-prêtre m'en est témoin, ainsi que tout le corps des anciens. Ayant même reçu d'eux des lettres pour les frères, j'allais à Damas pour amener aussi liés à Yeroushalaim ceux qui étaient là afin qu’ils soient punis.
22:6	Mais il arriva que comme j'allais et que j'approchais de Damas, soudainement, vers midi, une assez grande lumière resplendit du ciel telle qu'un éclair autour de moi.
22:7	Et je suis tombé par terre et j'ai entendu une voix qui me disait : Shaoul, Shaoul, pourquoi me persécutes-tu ?
22:8	Mais j'ai répondu : Qui es-tu Seigneur ? Il m'a dit : Moi, je suis Yéhoshoua, le Nazaréen, que tu persécutes.
22:9	Mais ceux qui étaient avec moi ont vraiment vu la lumière et ont été effrayés, mais ils n'ont pas compris la voix de celui qui me parlait. 
22:10	Mais j'ai dit : Que ferai-je Seigneur ? Et le Seigneur m'a dit : Lève-toi, va à Damas, et là on te parlera de toutes les choses qu'il t'est ordonné de faire.
22:11	Mais comme je ne voyais rien à cause de la gloire de cette lumière, j'arrivai à Damas, ceux qui étaient avec moi me conduisant par la main.
22:12	Or, un certain Chananyah, homme pieux selon la torah, et de qui tous les Juifs qui habitaient là rendaient un bon témoignage,
22:13	étant venu vers moi et se tenant là, me dit : Shaoul, frère, recouvre la vue ! Et à cette heure même je l'ai vu.
22:14	Et il dit : L'Elohîm de nos pères t'a désigné pour connaître sa volonté et pour voir le Juste et pour entendre une voix de sa bouche,
22:15	parce que tu seras pour lui, témoin devant tous les humains, des choses que tu as vues et entendues.
22:16	Et maintenant, pourquoi tardes-tu ? Lève-toi, sois baptisé et purifié de tes péchés en invoquant le Nom du Seigneur.
22:17	Et il m'arriva qu'étant de retour à Yeroushalaim, et en prière dans le temple, je fus ravi en extase
22:18	et je le vis qui me disait : Hâte-toi et sors promptement de Yeroushalaim, parce qu'ils ne recevront pas le témoignage que tu me rends.
22:19	Et j'ai dit : Seigneur, ils savent eux-mêmes que c'est vraiment moi qui, dans les synagogues, emprisonnais et battais de verges ceux qui croient en toi.
22:20	Et lorsqu'on a répandu le sang de Stephanos, ton témoin, j'étais moi-même présent, consentant à sa mort et gardant les vêtements de ceux qui le tuaient.
22:21	Et il m'a dit : Va, parce que moi je t'enverrai au loin vers les nations.

### Les Juifs réclament la mort de Paulos

22:22	Et ils l'écoutèrent jusqu'à cette parole, mais alors ils élevèrent leur voix, en disant : Ôte de la Terre un tel homme ! Car il n'est pas concevable qu'il vive.
22:23	Mais eux, criant avec force et jetant leurs vêtements et lançant de la poussière en l'air,
22:24	le tribun ordonna de le faire entrer dans la forteresse, disant de l'interroger sous la torture, par le fouet, afin de savoir pour quel sujet ils criaient ainsi contre lui.

### Paulos revendique sa citoyenneté romaine

22:25	Mais comme on l'étendait avec des courroies de cuir, Paulos dit à l'officier de l'armée romaine qui était près de lui : Est-il légal pour vous de châtier avec un fouet un homme, un Romain, et qui n'est même pas condamné ?
22:26	Et l'officier de l'armée romaine ayant entendu cela, alla le rapporter au tribun en disant : Prends garde à ce que tu es sur le point de faire, car cet homme est romain.
22:27	Et le tribun s'étant approché, lui dit : Dis-moi si tu es romain ? Et il dit : Oui.
22:28	Et le tribun répondit : Moi, j'ai acquis cette citoyenneté pour une grande somme d'argent. Et Paulos dit : Mais moi, je suis né tel.
22:29	Immédiatement, ceux qui étaient sur le point de l'interroger sous la torture s'écartèrent de lui et le tribun lui-même eut peur, sachant qu'il est romain et qu'il l'avait fait lier.

### Paulos comparaît devant le sanhédrin

22:30	Et le lendemain, voulant savoir avec certitude de quoi il est accusé par les Juifs, le tribun lui fit ôter ses liens, et donna l'ordre aux principaux prêtres et à tout le sanhédrin de se réunir et, ayant fait descendre Paulos, il le plaça devant eux.

## Chapitre 23

23:1	Et ayant fixé les yeux sur le sanhédrin, Paulos dit : Hommes frères ! Je me suis conduit comme un citoyen<!--Phil. 1:27.--> en toute bonne conscience devant Elohîm jusqu'à ce jour.
23:2	Et le grand-prêtre Chananyah ordonna à ceux qui étaient près de lui de le frapper sur la bouche.
23:3	Alors Paulos lui dit : Elohîm est sur le point de te frapper, mur blanchi ! Et tu es assis pour me juger selon la torah, et tu violes la torah en ordonnant qu'on me frappe !
23:4	Mais ceux qui étaient présents lui dirent : Tu insultes le grand-prêtre d'Elohîm ?
23:5	Et Paulos dit : Je ne savais pas, frères, que c'est le grand-prêtre, car il est écrit : Tu ne parleras pas mal du chef de ton peuple<!--Ex. 22:27.-->.

### Dissensions entre pharisiens et sadducéens

23:6	Mais Paulos, sachant qu'une partie étaient des sadducéens et l'autre des pharisiens, s'écria dans le sanhédrin : Hommes frères ! Moi je suis pharisien, fils de pharisien. C'est à cause de l'espérance et de la résurrection des morts que je suis jugé.
23:7	Mais quand il eut dit cela, il s'éleva une dissension entre les pharisiens et les sadducéens, et la multitude se divisa.
23:8	Car les sadducéens disent en effet qu'il n'y a pas de résurrection, ni d'ange, ni d'esprit, mais les pharisiens confessent l'un et l'autre.
23:9	Et il y eut une grande clameur. Alors, les scribes du parti des pharisiens se levèrent et se mirent à contester avec violence, en disant : Nous ne trouvons aucun mal en cet homme, et si un esprit ou un ange lui avait parlé ? Ne combattons pas contre Elohîm.
23:10	Et comme la dissension devenait grande, le tribun craignant que Paulos ne soit mis en pièces par eux, ordonna que la bande de soldats descende pour l'enlever de force du milieu d'eux et le conduire dans la forteresse.

### Le Seigneur fortifie Paulos (Paul)

23:11	Mais la nuit suivante, le Seigneur se présentant à lui, lui dit : Prends courage, Paulos, car comme tu as rendu témoignage dans Yeroushalaim me concernant, il faut de même que tu rendes témoignage aussi dans Rome.

### Les Juifs complotent contre Paulos

23:12	Mais quand le jour fut venu, certains des Juifs formèrent un complot<!--Une combinaison secrète, une coalition, une conspiration. Voir Ac. 19:40.-->, s'engagèrent, sous peine d'anathème et dirent qu’ils ne mangeraient ni ne boiraient jusqu’à ce qu’ils aient tué Paulos.
23:13	Et ceux qui formèrent cette conspiration étaient plus de 40,
23:14	et ils approchèrent des principaux prêtres et des anciens, et leur dirent : Nous nous sommes engagés, sous peine d'anathème, à ne rien manger avant que nous n’ayons tué Paulos.
23:15	Vous donc maintenant, comparaissez avec le sanhédrin devant le tribun, pour qu'il le fasse descendre vers vous demain, comme si vous étiez sur le point d'examiner plus exactement ce qui le concerne. Et nous, avant qu'il approche, nous sommes prêts à le tuer.
23:16	Mais le fils de la sœur de Paulos, ayant eu connaissance de ce guet-apens, vint, et étant entré dans la forteresse, le rapporta à Paulos.
23:17	Et Paulos appela l'un des officiers de l'armée romaine, et dit : Conduis ce jeune homme au tribun, car il a quelque chose à lui rapporter.
23:18	Il le prit donc et l'amena en effet au tribun, et il dit : Le prisonnier Paulos m'a appelé et m'a prié de t'amener ce jeune homme, qui a quelque chose à te dire.
23:19	Et le tribun le prenant par la main, se retira à part et demanda : Qu'est-ce que tu as à me rapporter ?
23:20	Et il dit : Les Juifs ont conspiré de te prier que demain tu envoies Paulos au sanhédrin, comme s’ils étaient sur le point de s’informer plus exactement à son sujet.
23:21	Toi donc ne leur cède pas, car plus de 40 hommes parmi eux lui tendent un piège, ils se sont engagés, sous peine d'anathème, à ne pas manger ni boire, avant qu’ils l’aient tué ; et maintenant ils sont prêts et n'attendent que ta promesse.
23:22	Le tribun donc renvoya le jeune homme, après lui avoir donné cet ordre : Ne dis en effet à personne que tu m'as révélé ces choses.

### Paulos conduit à Césarée

23:23	Et ayant appelé à lui deux des officiers de l'armée romaine, il dit : Préparez 200 soldats, 70 cavaliers et 200 archers, pour qu'ils aillent jusqu'à Césarée dès la troisième heure de la nuit.
23:24	Apportez aussi des bêtes, afin qu'ils fassent monter Paulos dessus et le transportent préservé du danger chez Félix, le gouverneur<!--Marcus Antonius Félix était procurateur de la province romaine de la Judée de 52 à 60 ap. J.-C.-->.
23:25	Il écrivit une lettre ayant cette forme :
23:26	Claude Lysias<!--Libérateur. Un tribun militaire ou chiliarque (chef de 1 000 soldats) d'une cohorte romaine, commandant la garnison de Yeroushalaim.--> au très excellent gouverneur Félix, salut !
23:27	Cet homme, les Juifs l'avaient saisi et il était sur le point d'être tué par eux, lorsque je suis survenu avec une bande de soldats et je l'ai enlevé, ayant appris qu'il est romain.
23:28	Et voulant connaître le motif pour lequel ils l'accusaient, je l'ai fait descendre dans leur sanhédrin.
23:29	J'ai trouvé qu'il est accusé au sujet de questions relatives à leur torah, mais qu'il n'est chargé d'aucune accusation qui mérite la mort ou les liens.
23:30	Mais comme on m'a révélé un complot que les Juifs étaient sur le point d'organiser contre lui, je te l'ai aussitôt envoyé, en ordonnant à ses accusateurs de te dire eux-mêmes ce qu'ils ont contre lui. Adieu !
23:31	Les soldats donc, selon l'ordre qu'ils avaient reçu, prirent Paulos et le conduisirent en effet pendant la nuit jusqu'à Antipatris.
23:32	Mais le lendemain, laissant les cavaliers poursuivre la route avec lui, ils retournèrent à la forteresse.
23:33	Arrivés à Césarée, les cavaliers remirent la lettre au gouverneur et lui présentèrent aussi Paulos.
23:34	Et le gouverneur l’ayant lue et ayant demandé de quelle province il était, lorsqu'il sut qu'il est de Cilicie,
23:35	il dit : Je t'entendrai quand tes accusateurs seront venus. Et il ordonna qu'il soit gardé dans le prétoire d'Hérode.

## Chapitre 24

### Paulos (Paul) comparaît devant le gouverneur Félix

24:1	Or cinq jours après, Chananyah le grand-prêtre, descendit avec les anciens, et un certain Tertullos, orateur. Ils portèrent plainte au gouverneur contre Paulos.
24:2	Mais l’ayant appelé, Tertullos fut le premier à l'accuser, disant : Nous jouissons par ton moyen d’une grande paix et des mesures publiques saines sont prises pour cette nation par ton soin providentiel.
24:3	Partout et de toute manière, Très excellent Félix, nous les accueillons avec une entière reconnaissance.
24:4	Mais afin de ne pas t'arrêter davantage, je te prie de nous entendre, selon ta bonté, dans ce que nous allons te dire en peu de paroles.
24:5	Car nous avons trouvé cet homme, une peste, qui excite la sédition parmi tous les Juifs de toute la terre habitée, et qui aussi est un chef<!--Quelqu'un qui se tient au premier rang.--> de la secte des Nazaréens.
24:6	Il a même tenté de profaner le temple. Nous l'avons saisi et nous avons voulu le juger selon notre torah,
24:7	mais le tribun Lysias étant survenu, il nous l'a arraché de nos mains avec une grande violence,
24:8	ordonnant à ses accusateurs de venir devant toi. Tu pourras toi-même, en l'examinant, savoir de lui tout ce dont nous l'accusons.
24:9	Mais les Juifs aussi consentirent à cela, en affirmant que les choses étaient ainsi.
24:10	Et après que le gouverneur lui eut fait signe de parler, Paulos répondit : Sachant qu'il y a déjà plusieurs années que tu es le juge de cette nation, je réponds pour moi avec plus de courage :
24:11	Tu peux savoir qu'il n'y a pas plus de 12 jours que je suis monté à Yeroushalaim pour adorer.
24:12	Mais ils ne m'ont pas trouvé dans le temple disputant avec personne, ni faisant un amas de peuple, soit dans les synagogues, soit dans la ville.
24:13	Et ils ne peuvent prouver les choses dont ils m'accusent maintenant.
24:14	Or, je te confesse bien ceci, que selon la Voie<!--Voir Ac. 22:4.--> qu'ils appellent secte, ainsi je sers l'Elohîm de mes pères, croyant toutes les choses qui sont écrites dans la torah et dans les prophètes,
24:15	ayant en Elohîm l'espérance qu'eux-mêmes admettent aussi, qu'il y aura une résurrection, tant des justes que des injustes<!--Voir Da. 12:2-3 ; Jn. 5:29 ; Ap. 20:11-15.-->.
24:16	Or en ceci que moi-même, je m'exerce à avoir constamment une conscience sans achoppement devant Elohîm et devant les humains.
24:17	Or après plusieurs années, je suis venu pour faire des aumônes et des offrandes dans ma nation.
24:18	En ces circonstances, ils m'ont trouvé purifié dans le temple, mais il n'y avait ni foule avec moi, ni tumulte, mais certains Juifs d'Asie.
24:19	Mais c’est eux qui auraient dû se présenter devant toi et m’accuser, s'ils avaient quelque chose contre moi.
24:20	Ou bien, que ceux-ci eux-mêmes, disent, s'ils ont trouvé en moi quelque iniquité, quand j'ai été présenté au sanhédrin.
24:21	Si ce n'est au sujet de cette seule voix que j'ai criée, debout au milieu d'eux : C'est au sujet de la résurrection des morts que je suis jugé aujourd’hui par vous.
24:22	Mais après avoir entendu ces choses, Félix qui savait plus exactement ce qui concernait la Voie<!--Voir Ac. 22:4.-->, les ajourna en disant : Quand le tribun Lysias sera venu, j'examinerai votre affaire.
24:23	Et il donna l'ordre à l'officier de l'armée romaine de garder Paulos, en lui laissant une certaine liberté, et n'empêchant aucun des siens de le servir ou de venir vers lui.

### Paulos annonce l'Évangile à Félix, le gouverneur, et à sa femme

24:24	Et quelques jours après, Félix vint avec Drusilla, sa femme, qui était juive, et il envoya chercher Paulos. Il l'entendit sur la foi en Mashiah.
24:25	Mais en discourant sur la justice, le contrôle de soi et le jugement qui est imminent, Félix fut effrayé et répondit : Pour le moment retire-toi. Mais quand j'aurai trouvé le temps convenable, je te convoquerai.
24:26	Mais il espérait en même temps que Paulos lui donnerait de l'argent afin de le délivrer, c'est pourquoi il l'envoyait chercher souvent et s'entretenait avec lui.
24:27	Or après deux ans accomplis, Félix eut pour successeur Porcius Festus<!--Porcius Festus était procurateur de Judée d'environ 60 à 62, succédant à Antonius Félix.-->, qui, voulant faire plaisir aux Juifs, laissa Paulos lié.

## Chapitre 25

### Paulos (Paul) comparaît devant le gouverneur Festus

25:1	Festus donc étant arrivé dans la province, monta trois jours après, de Césarée à Yeroushalaim.
25:2	Et le grand-prêtre et les principaux d'entre les Juifs portèrent plainte contre Paulos devant lui, et ils le suppliaient, 
25:3	demandant contre celui-ci comme une grâce de le faire venir à Yeroushalaim. Or ils avaient dressé une embuscade pour le tuer en chemin.
25:4	Alors Festus leur répondit que Paulos était en effet gardé à Césarée et que lui-même était sur le point de partir promptement.
25:5	Que ceux d’entre vous qui le peuvent descendent donc avec moi. S'il y a quelque chose contre cet homme, qu'ils l'accusent !
25:6	Et n'ayant séjourné parmi eux que dix jours, il descendit à Césarée. Le lendemain, siégeant au tribunal, il ordonna que Paulos soit amené.
25:7	Et quand il fut présent, les Juifs qui étaient descendus de Yeroushalaim, l'entourèrent et portèrent contre lui de nombreuses et graves accusations, qu'ils ne pouvaient pas prouver.
25:8	Tandis que celui-ci se défendait : Je n'ai péché en rien ni contre la torah des Juifs, ni contre le temple, ni contre César.
25:9	Mais Festus voulant faire plaisir aux Juifs, répondit à Paulos et dit : Veux-tu monter à Yeroushalaim et y être jugé sur ces choses devant moi ?

### Paulos en appelle à César

25:10	Mais Paulos dit : Je me tiens devant le tribunal de César, et c'est là que je dois être jugé. Je n'ai fait aucun tort aux Juifs, comme tu le sais très bien.
25:11	Si donc j'ai vraiment commis une injustice ou quelque crime digne de mort, je ne refuse pas de mourir. Mais s'il n'est rien des choses dont ils m'accusent, personne ne peut me livrer à eux par faveur. J'en appelle à César !
25:12	Alors Festus, après avoir parlé avec l'assemblée des conseillers, répondit : Tu en as appelé à César, tu iras devant César !

### Le roi Agrippa informé du cas de Paulos

25:13	Or quelques jours s'étant écoulés, le roi Agrippa<!--Agrippa II (27-28 – 93-101 ap. J.-C.) était le fils d'Agrippa I (10 av. J.-C. – 44 ap. J.-C.), qui était lui-même le petit-fils d'Hérode le Grand (73 – 4 av. J.-C.).--> et Bernike<!--Bernike (ou Bérénice) (née vers 28 ap. J.-C.) était la fille d'Agrippa I et donc la sœur d'Agrippa II. Pendant tout le règne de son frère, elle fut présentée comme reine à ses côtés, raison pour laquelle on soupçonna une liaison incestueuse entre eux.--> arrivèrent à Césarée pour saluer Festus.
25:14	Et comme ils passèrent là plusieurs jours, Festus exposa au roi l'affaire de Paulos, en disant : Félix a laissé prisonnier un homme
25:15	au sujet duquel, comme j'étais à Yeroushalaim, les principaux prêtres et les anciens des Juifs ont porté plainte, en demandant contre lui sentence de condamnation.
25:16	Mais je leur ai répondu que ce n'est pas la coutume des Romains de livrer par faveur un homme à la mort, avant que l’accusé se trouve en face de ses accusateurs, et qu’il ait l’occasion de parler pour sa plaidoirie au sujet du crime dont on l’accuse.
25:17	Ils sont donc venus ici et, sans tarder, j'ai siégé le lendemain au tribunal et j'ai donné l'ordre qu'on amène cet homme.
25:18	Les accusateurs s'étant présentés, ne lui imputèrent aucune des accusations que je supposais.
25:19	Mais ils avaient avec lui des discussions au sujet de leur propre religion et d’un certain Yéhoshoua qui est mort, que Paulos affirmait être vivant.
25:20	Mais moi, perplexe au sujet d’un débat de cette sorte, je lui ai demandé s'il voulait aller à Yeroushalaim et y être jugé sur ces choses.
25:21	Mais Paulos en ayant appelé pour être gardé pour la décision d'Auguste, j'ai ordonné qu'on le garde jusqu'à ce que je l'envoie à César.
25:22	Alors Agrippa dit à Festus : Je voudrais moi aussi entendre cet homme. Demain, dit-il, tu l'entendras.
25:23	Le lendemain donc, Agrippa et Bernike arrivèrent en grande pompe et entrèrent dans la salle d'audience avec les tribuns et les hommes éminents de la ville. Paulos fut amené sur l'ordre de Festus.
25:24	Et Festus dit : Roi Agrippa, et vous tous, hommes qui êtes ici avec nous, vous voyez celui au sujet duquel toute la multitude des Juifs s'est adressée à moi, tant à Yeroushalaim qu'ici, en criant qu'il ne devait plus vivre.
25:25	Mais moi, ayant compris qu'il n'avait rien fait qui mérite la mort, mais lui-même en ayant appelé à Auguste, j'ai résolu de l'envoyer.
25:26	Comme je n'ai rien de certain à écrire au seigneur<!--Il est question d'Auguste, empereur romain. Voir 1 Co. 8:5-6.--> à son sujet, je vous l'ai présenté, et spécialement à toi, roi Agrippa, afin qu'après en avoir fait l'examen, j'aie de quoi écrire.
25:27	Car il me semble qu'il n'est pas raisonnable d'envoyer un prisonnier sans indiquer aussi les accusations portées contre lui.

## Chapitre 26

### Discours de Paulos (Paul) devant Agrippa<!--Ac. 9:1-18, 22:1-16.-->

26:1	Et Agrippa dit à Paulos : Il t'est permis de parler en ta propre faveur. Alors Paulos, étendant la main, se défendit :
26:2	Concernant toutes les choses dont je suis accusé par les Juifs, roi Agrippa, je m'estime béni d'être sur le point de me défendre aujourd'hui devant toi,
26:3	surtout parce que tu es un connaisseur de toutes les coutumes et de toutes les questions qui existent parmi les Juifs. C'est pourquoi je te supplie de m'écouter avec longanimité.
26:4	Ma manière donc de vivre et d'agir, telle que je l’ai passée dès le commencement au milieu de ma nation dans Yeroushalaim, tous les Juifs la connaissent en effet.
26:5	Ils me connaissent depuis le début, s'ils veulent en rendre témoignage, que j'ai vécu en pharisien, selon la secte la plus stricte<!--« Exact », « soigneux. » Dans une secte stricte : l'interprétation la plus précise et la plus rigoureuse de la loi mosaïque, l'observation des moindres préceptes de la loi et de la tradition.--> de notre religion.
26:6	Et maintenant, je suis jugé à cause de l'espérance en la promesse qu'Elohîm a faite à nos pères,
26:7	et à laquelle nos douze tribus, en servant Elohîm sérieusement nuit et jour, espèrent parvenir. Et c'est pour cette espérance, roi Agrippa, que je suis accusé par les Juifs.
26:8	Quoi ! Juge-t-on incroyable qu'Elohîm ressuscite les morts ?
26:9	Pour moi, j'avais donc vraiment cru que je devais beaucoup m'opposer au Nom de Yéhoshoua, le Nazaréen.
26:10	C'est ce que j'ai fait à Yeroushalaim. J'ai enfermé dans des prisons beaucoup de saints, après en avoir reçu le pouvoir des principaux prêtres, et quand on les faisait mourir, j’apportais<!--« Jeter un caillou (ou calcul) dans une urne », « donner son vote », « approuver ».--> mon caillou.
26:11	Et souvent, dans toutes les synagogues, en les punissant, je les forçais à blasphémer. Et étant extrêmement en rage contre eux, je les persécutais même jusque dans les villes étrangères.
26:12	Dans ces dispositions, et comme j'allais à Damas, avec pouvoir et permission des principaux prêtres,
26:13	au milieu du jour, dans le chemin, roi, j'ai vu une lumière venant du ciel, plus éclatante que le soleil, laquelle brillait autour de moi et de ceux qui étaient en chemin avec moi.
26:14	Et nous sommes tous tombés par terre et j'ai entendu une voix me parlant en langue hébraïque, disant : Shaoul, Shaoul, pourquoi me persécutes-tu ? Il t'est dur de regimber contre les aiguillons.
26:15	Mais j'ai dit : Qui es-tu Seigneur ? Et il a dit : Moi, je suis Yéhoshoua que tu persécutes.
26:16	Mais lève-toi et tiens-toi sur tes pieds, car je te suis apparu pour te désigner pour serviteur et témoin des choses que tu as vues et de celles pour lesquelles je t'apparaîtrai,
26:17	en t'arrachant du milieu de ce peuple et des nations, vers qui je t'envoie maintenant,
26:18	pour ouvrir leurs yeux, afin qu'ils se tournent de la ténèbre vers la lumière et de l'autorité de Satan vers Elohîm, afin qu'ils reçoivent le pardon des péchés et un lot parmi ceux qui ont été sanctifiés par la foi en moi.
26:19	C'est pourquoi, roi Agrippa, je n'ai pas été rebelle à l'apparition céleste.
26:20	Mais j'ai annoncé premièrement à ceux de Damas, et à Yeroushalaim, dans tout le pays de Judée et parmi les nations, de se repentir et de se convertir à Elohîm, avec la pratique d'œuvres dignes de la repentance.
26:21	C'est pour cela que les Juifs se sont saisis de moi dans le temple et ont tenté de me tuer de leurs mains.
26:22	Mais ayant obtenu<!--Le mot grec signifie aussi avoir la chance.--> le secours qui vient d'Elohîm, me voici debout jusqu'à ce jour, rendant témoignage aux petits et aux grands, sans rien dire en dehors des choses dont les Prophètes ainsi que Moshé ont déclaré qu’elles étaient sur le point d'arriver :
26:23	que le Mashiah souffrirait, et que, le premier de la résurrection d’entre les morts, il devait annoncer la lumière au peuple et aux nations.
26:24	Et comme il disait ces choses pour sa défense, Festus dit à grande voix : Tu es fou Paulos ! Ton grand savoir dans les lettres te fait tourner à la folie !
26:25	Et Paulos dit : Je ne suis pas fou, très excellent Festus, mais je dis des paroles de vérité et de bon sens.
26:26	Car il sait bien ces choses, le roi à qui aussi je parle avec assurance, car je suis persuadé qu'il n'ignore rien de ces choses, car ceci n'a pas été fait en cachette !
26:27	Crois-tu aux prophètes, roi Agrippa ? Je sais que tu y crois.
26:28	Et Agrippa dit à Paulos : Tu me persuades un peu de devenir chrétien !
26:29	Et Paulos dit : Que ce soit pour un peu ou pour beaucoup, je prie Elohîm que non seulement toi, mais aussi tous ceux qui m'écoutent aujourd'hui, vous deveniez comme moi, à l'exception de ces liens !
26:30	Et lorsqu'il eut dit cela, le roi se leva, avec le gouverneur et Bernike, et ceux qui étaient assis avec eux.
26:31	Et s'étant retirés, ils parlaient les uns avec les autres en disant : Cet homme n'a rien fait qui mérite la mort ou les liens.
26:32	Et Agrippa dit à Festus : Cet homme aurait pu être relâché s'il n'en avait pas appelé à César.

## Chapitre 27

### Paulos (Paul) embarque pour Rome

27:1	Mais quand il a été jugé que nous embarquerions pour l'Italie, on a livré Paulos avec quelques autres prisonniers à un officier de l'armée romaine, du nom de Ioulios, de la cohorte d'Auguste.
27:2	Et nous sommes montés sur un navire d'Adramytte qui était sur le point de faire voile le long des côtes de l'Asie et nous sommes partis. Aristarchos, Macédonien de la ville de Thessalonique, étant avec nous.
27:3	Le jour suivant, nous avons abordé à Sidon. Et Ioulios, qui traitait Paulos avec courtoisie, lui a permis d'aller vers ses amis afin de recevoir leurs soins.
27:4	Et étant partis de là, nous avons longé l'île de Chypre, parce que les vents étaient contraires.
27:5	Après avoir traversé la Mer de Cilicie et de Pamphylie, nous avons débarqué à Myra en Lycie.
27:6	Et là, l'officier de l'armée romaine a trouvé un navire d'Alexandrie qui allait en Italie, et il nous y a fait monter.
27:7	Et pendant plusieurs jours nous avons navigué lentement, et ce n'est pas sans difficulté que nous sommes arrivés à la hauteur de Cnide, et comme le vent ne nous permettait pas d'aborder, nous avons longé la Crète, vers Salmoné.
27:8	Nous avons longé l'île avec peine et sommes arrivés à un lieu appelé Beaux-Ports, près duquel était la ville de Lasée.
27:9	Et comme un temps assez long s'était écoulé et que le voyage par mer était déjà dangereux, parce que même le jeûne était déjà passé<!--Ce jeûne correspondait au jour de l'expiation célébré le dixième jour du septième mois. Lé. 23:27.-->, Paulos les avertissait,
27:10	en leur disant : Hommes, je vois que le voyage par mer se fera avec dommage infligé par la violence d'une tempête et une grande perte, non seulement pour la cargaison et pour le navire, mais aussi pour nos propres vies.
27:11	Mais l'officier de l'armée romaine croyait plus le maître de navigation et le propriétaire du navire que ce que Paulos disait.
27:12	Et comme le port n'était pas bon pour y passer l'hiver, la plupart furent d'avis de partir de là, pour tâcher de gagner Phénix, un port de Crète, tourné vers le vent du sud-ouest et le vent du nord-ouest, afin d'y passer l'hiver.
27:13	Et le vent du midi commença à souffler doucement et se croyant maîtres de leur dessein, ils levèrent l'ancre et côtoyèrent de près l'île de Crète.

### Une tempête de plusieurs jours

27:14	Mais peu après, un vent impétueux, du nord-est, qu'on appelle Euraquilon<!--Euraquilon : vagues et vent d'est.-->, se leva du côté de l'île.
27:15	Et le navire fut emporté par la violence de la tempête. Et ne pouvant résister, nous nous sommes laissés emporter à la dérive.
27:16	Et ayant passé au-dessous d'une petite île appelée Clauda, nous avons eu beaucoup de peine à nous rendre maîtres de la chaloupe.
27:17	Après l'avoir hissée, ils se sont servis des moyens de secours pour ceindre<!--Ceindre au dessous, lier un navire latéralement : c'est-à-dire avec des cordes, pour le rendre capable de résister à la force des vagues et de la tempête.--> le navire et, de crainte de tomber sur la Syrte<!--Il s'agit de la Grande Syrte et de la Petite Syrte : deux bancs de sables mouvants très redoutés.-->, ils abaissèrent les voiles. C'est ainsi qu'on se laissa emporter par le vent.
27:18	Mais comme nous étions extrêmement secoués par la tempête, le jour suivant ils se sont mis à alléger le navire<!--Jeter hors de, délester, allégement en jetant par-dessus bord ce qui peut rendre le bateau plus manœuvrable dans une tempête et le préserver du naufrage.-->.
27:19	Et le troisième jour, nous avons jeté de nos propres mains les agrès du navire.
27:20	Et comme ni le soleil ni les étoiles n'apparurent pendant plusieurs jours, et que ce n’était pas une petite tempête qui se trouvait sur nous, finalement, nous avions perdu toute espérance de nous sauver.

### Paulos rassure les membres du navire

27:21	Et comme on avait été longtemps sans manger, Paulos se tenant alors debout au milieu d'eux dit : Ô hommes, il fallait vraiment m'écouter et ne pas partir de Crète, afin d'éviter ce dommage infligé par la violence de cette tempête et cette perte.
27:22	Maintenant même, je vous exhorte à prendre courage car il n'y aura perte d'aucune âme parmi vous, mais du navire seulement.
27:23	Car un ange d'Elohîm à qui je suis et que je sers s'est présenté à moi cette nuit,
27:24	disant : Paulos, n'aie pas peur ! Il faut que tu comparaisses devant César, et voici qu'Elohîm t'a donné gracieusement tous ceux qui naviguent avec toi.
27:25	C'est pourquoi, hommes, prenez courage, car je crois Elohîm : la chose sera comme elle m'a été dite.
27:26	Mais il nous faut échouer sur une île quelconque.
27:27	Mais quand la quatorzième nuit fut venue, vers le milieu de la nuit, tandis que nous étions ballottés sur l'Adriatique, les matelots soupçonnèrent qu'on approchait de quelque terre.
27:28	Et ayant jeté la sonde, ils trouvèrent 20 brasses. Et étant passés un peu plus loin, et ayant encore jeté la sonde, ils trouvèrent 15 brasses.
27:29	Mais craignant d'échouer sur quelque écueil, ils jetèrent quatre ancres de la poupe et prièrent pour que le jour vînt.
27:30	Mais comme les matelots cherchaient à s'échapper du navire et mettaient la chaloupe à la mer, sous prétexte d'être sur le point de jeter les ancres de la proue,
27:31	Paulos dit à l'officier de l'armée romaine et aux soldats : Si ceux-ci ne restent pas dans le navire, vous ne pouvez pas être sauvés.
27:32	Alors les soldats coupèrent les cordes de la chaloupe et la laissèrent tomber.
27:33	Et jusqu'à ce que le jour soit sur le point de venir, Paulos les exhortait tous à prendre de la nourriture, en leur disant : C’est aujourd’hui le quatorzième jour que vous passez à jeun, dans l’attente, sans avoir rien pris.
27:34	C'est pourquoi je vous exhorte à prendre de la nourriture, car il le faut pour votre salut, car il ne tombera pas un cheveu de la tête d'aucun de vous.
27:35	Mais ayant dit ces choses et prenant un pain, il rendit grâce à Elohîm en présence de tous et, l'ayant rompu, il se mit à manger.
27:36	Et tous, reprenant courage, mangèrent aussi.
27:37	Et nous étions dans le navire 276 âmes en tout.
27:38	Et quand ils eurent mangé jusqu'à être rassasiés, ils allégèrent le navire en jetant le blé dans la mer.

### Naufrage du navire

27:39	Et quand le jour fut venu, ils ne reconnurent pas la terre, mais ils aperçurent une certaine baie ayant une plage, sur laquelle ils résolurent de pousser le navire, s'ils le pouvaient.
27:40	Ils délièrent alors les ancres et les laissèrent aller dans la mer, ils relâchèrent en même temps les attaches des gouvernails et, hissant au vent la voile d'artimon, ils se dirigeaient vers le rivage.
27:41	Mais étant tombés dans un lieu qui avait la mer des deux côtés, ils y firent échouer le navire. La partie avant du navire fortement engagée restait, en effet, immobile, tandis que la poupe se brisait par la violence des grosses vagues.
27:42	Et la décision des soldats fut de tuer les prisonniers, de peur qu’il ne s’en échappât quelqu’un à la nage.
27:43	Mais l'officier de l'armée romaine, voulant préserver du danger Paulos, s’opposa à leur conseil. Il ordonna à ceux qui pouvaient nager de se jeter les premiers à l'eau pour gagner la terre,
27:44	et le reste, les uns sur des planches, et les autres sur quelques débris du navire. Et ainsi il arriva que tous parvinrent en effet à terre, préservés du danger.

## Chapitre 28

### Paulos (Paul), mordu par une vipère sur l'île de Malte

28:1	Et ayant été préservés du danger, ils reconnurent alors que l'île s'appelait Malte.
28:2	Et les barbares nous traitèrent avec beaucoup d'humanité, car ayant allumé un feu, ils nous reçurent tous à cause de la pluie qui était survenue et à cause du froid.
28:3	Mais Paulos ayant ramassé un tas de broussailles et l'ayant mis au feu, une vipère en sortit à cause de la chaleur et s'attacha à sa main.
28:4	Et quand les barbares virent la bête suspendue à sa main, ils se dirent les uns les autres : Sûrement, cet homme est un meurtrier ! Bien qu’il ait été préservé du danger hors de la mer, Diké<!--Du grec « dike ». « Diké » ou « Dicé » était la déesse grecque personnifiant la justice et la vengeance.--> ne permet pas qu'il vive.
28:5	Lui donc, ayant secoué la bête sauvage dans le feu, ne ressentit en effet aucun mal.
28:6	Mais eux s'attendaient à le voir enfler ou tomber mort soudainement. Mais après avoir longtemps attendu, voyant qu'il ne lui arrivait aucun mal, ils changèrent d'opinion et dirent que c'était un elohîm.

### Guérison du père de Poplios

28:7	Or il y avait près de cet endroit, des terres qui appartenaient au premier de l'île, du nom de Poplios, qui nous reçut et nous logea pendant trois jours d'une manière amicale.
28:8	Et il arriva que le père de Poplios était couché, malade de la fièvre et de la dysenterie. Paulos, étant entré vers lui et ayant prié en lui imposant les mains, le guérit.
28:9	Cela étant donc arrivé, les autres habitants de l'île qui avaient des maladies vinrent aussi et furent guéris.
28:10	Ils nous rendirent aussi de grands honneurs et, au moment où nous embarquions, ils nous fournirent ce qui était nécessaire.

### Paulos arrive à Rome

28:11	Or au bout de 3 mois, nous avons embarqué sur un navire d'Alexandrie qui avait passé l'hiver dans l'île, et qui était marqué par l'image des Dioskouroi<!--Dioskouroi (ou Dioscures) = “les fils de Zeus” : Castor et Pollux. Ces derniers étaient deux frères jumeaux, fils de Zeus (Jupiter) et Leda, et considérés comme les divinités protégeant les marins.-->.
28:12	Et ayant abordé à Syracuse, nous y sommes restés 3 jours.
28:13	De là nous avons fait un circuit et sommes arrivés à Reggio. Et un jour après, un vent du midi s'est levé et nous sommes parvenus le deuxième jour à Pouzzoles,
28:14	où nous avons trouvé des frères qui nous ont prié de rester 7 jours avec eux. C'est ainsi que nous sommes arrivés à Rome.
28:15	Et de là, les frères qui avaient entendu parler de nous, sont venus à notre rencontre jusqu'au Forum d'Appius et aux Trois-Tavernes. En les voyant, Paulos a rendu grâce à Elohîm et il a pris courage.

### Paulos évangélise les Juifs de Rome

28:16	Mais lorsque nous sommes arrivés à Rome, l'officier de l'armée romaine livra les prisonniers entre les mains du préfet du prétoire. Mais on a permis à Paulos de demeurer à part avec le soldat qui le gardait.
28:17	Or, il arriva 3 jours après, que Paulos convoqua les principaux des Juifs. Et quand ils furent réunis, il leur dit : Hommes frères ! Sans avoir rien fait contre le peuple ni contre les coutumes des pères, j'ai été mis en prison à Yeroushalaim et livré entre les mains des Romains,
28:18	qui après m'avoir jugé, voulaient me relâcher parce qu'il n'y avait en moi aucun crime qui mérite la mort.
28:19	Mais les Juifs s'y opposèrent, j'ai été contraint d'en appeler à César, n'ayant du reste aucun dessein d'accuser ma nation.
28:20	C'est pour ce sujet que je vous ai appelés, afin de vous voir et vous parler. Car c'est pour l'espérance d'Israël que je suis entouré de cette chaîne.
28:21	Mais ils lui dirent : Nous n'avons reçu de Judée aucune lettre à ton sujet, et il n'est venu aucun frère qui ait rapporté ou dit du mal de toi.
28:22	Mais nous jugeons digne d’entendre de toi ce que tu penses, car pour ce qui est de cette secte, nous savons en effet qu'on la contredit partout.
28:23	Et lui ayant assigné un jour, ils vinrent auprès de lui en plus grand nombre dans son logement et, du matin au soir, il leur exposait le Royaume d'Elohîm, en rendant témoignage, et en les persuadant de ce qui concerne Yéhoshoua, tant par la torah de Moshé que par les prophètes.

### Paulos se tourne vers les nations<!--Ap. 13:14, 18:6.-->

28:24	Et les uns furent en effet persuadés par les choses qui étaient dites, mais les autres n'y crurent pas.
28:25	Mais n'étant pas d'accord entre eux, ils se retirèrent après que Paulos leur eut dit ces paroles : Le Saint-Esprit a bien parlé à nos pères par le prophète Yesha`yah, 
28:26	en disant : Va vers ce peuple et dis-lui : En entendant, vous entendrez, mais vous ne comprendrez jamais et, en regardant, vous regarderez, mais vous ne verrez jamais.
28:27	Car le cœur de ce peuple s'est engraissé, et de leurs oreilles ils ont entendu avec difficulté et ils ont fermé leurs yeux, de peur qu'ils ne voient des yeux, et qu'ils n'entendent des oreilles, et qu'ils ne comprennent de leur cœur, et qu'ils ne se convertissent et que je ne les guérisse<!--Es. 6:9-10.-->.
28:28	Qu'il vous soit donc connu que le salut d'Elohîm a été envoyé aux nations et que même elles l'écouteront.
28:29	Et quand il eut dit ces choses, les Juifs s'en allèrent, ayant entre eux une grande discussion.
28:30	Et Paulos demeura deux ans entiers dans une maison qu'il avait louée. Et il recevait tous ceux qui venaient le voir,
28:31	prêchant le Royaume d'Elohîm et enseignant les choses qui concernent le Seigneur Yéhoshoua Mashiah, en toute liberté dans les paroles et sans aucun empêchement.
