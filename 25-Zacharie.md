# Zekaryah (Zacharie) (Za.)

Signification : Yah se souvient

Auteur : Zekaryah (Zacharie)

Thème : Les deux parousies du Mashiah

Date de rédaction : 6ème siècle av. J.-C.

Zekaryah, contemporain de Chaggay (Aggée), exerça son service en Yéhouda (Juda) au retour des exilés de Babel (Babylone), où il était né. Il annonça la venue du Mashiah et raconta de manière très précise différents épisodes de sa vie que l'on retrouve également dans les évangiles. Il dévoila aussi quelques-uns des attributs du Sauveur ; qui serait premièrement rejeté, mais finalement accepté par le peuple juif pendant le millénium. Le Mashiah est ici présenté comme le Grand-Prêtre, le Germe, le Roi de paix, le Fils de David...

## Chapitre 1

### YHWH avertit son peuple

1:1	Le huitième mois de la deuxième année de Darius, la parole de YHWH apparut à Zekaryah, le prophète, fils de Berekyah, fils d'Iddo, en disant :
1:2	YHWH a été irrité, il a été irrité contre vos pères.
1:3	Tu leur diras : Ainsi parle YHWH Tsevaot : Revenez à moi, – déclaration de YHWH Tsevaot –, et je reviendrai à vous, dit YHWH Tsevaot<!--Es. 31:6 ; Jé. 3:12 ; Joë. 2:12.-->.
1:4	Ne devenez pas comme vos pères, eux que les premiers prophètes ont appelés, en disant : Ainsi parle YHWH Tsevaot : Revenez, s’il vous plaît, de vos mauvaises voies et de vos mauvaises actions ! Mais ils n'ont pas écouté et n'ont pas été attentifs à ce que je leur disais, – déclaration de YHWH<!--La. 5:7 ; Esd. 9:7 ; Né. 9:16 ; 2 Ch. 29:6.-->.
1:5	Vos pères, où sont-ils ? Et les prophètes, ont-ils vécu éternellement ? 
1:6	Cependant mes paroles et mes ordonnances que j'avais données aux prophètes, mes serviteurs, n'ont-elles pas atteint vos pères ? Ils sont revenus et ont dit : De la manière dont YHWH Tsevaot avait projeté de nous traiter, selon nos voies et selon nos actions, ainsi nous a-t-il traités.

### Le cavalier sur un cheval rouge

1:7	Le vingt-quatrième jour du onzième mois, qui est le mois de Shebat, dans la deuxième année de Darius, la parole de YHWH apparut à Zekaryah, le prophète, fils de Berekyah, fils d'Iddo, en disant :
1:8	Je vis pendant la nuit, et voici, un homme était monté sur un cheval rouge et il se tenait parmi les myrtes qui étaient dans un lieu creux. Derrière lui, des chevaux rouges, fauves et blancs<!--Ap. 6:2-4.-->.
1:9	Je dis : Qui sont ceux-ci, mon seigneur ? L'ange qui parlait avec moi me dit : Je te ferai voir qui sont ceux-ci.
1:10	L'homme qui se tenait parmi les myrtes répondit et dit : Ce sont ceux que YHWH a envoyés pour parcourir la terre.
1:11	Et ils répondirent à l'Ange de YHWH qui se tenait parmi les myrtes, et dirent : Nous avons parcouru la Terre et voici que toute la Terre est en repos et tranquille.

### Compassion de YHWH pour Yeroushalaim (Jérusalem)

1:12	L'Ange de YHWH répondit et dit : YHWH Tsevaot, jusqu'à quand n'auras-tu pas compassion de Yeroushalaim et des villes de Yéhouda, contre lesquelles tu es irrité depuis 70 ans<!--Yirmeyah (Jérémie) prophétisa que la captivité babylonienne durerait 70 ans (Jé. 25:11-12, 29:10). Les 70 ans commencèrent à la déportation de la famille royale à Babel (Babylone) en 605 av. J.-C. (2 R. 24 ; Da. 1) et se terminèrent avec la première vague de retours conduite par Zerubbabel (Zorobabel) (Esd. 1). Les Israélites furent emmenés en captivité en plusieurs vagues. Le livre d'Esdras (Ezra) raconte les deux premières. En 538 av. J.-C., Zerubbabel mena la première vague et fut nommé gouverneur (Ag. 1:1). Le grand-prêtre Yéhoshoua (Esd. 3:2) et les prophètes Chaggay (Aggée) et Zekaryah (Esd. 5:1-2) le secondaient. Leur plus grand défi fut de rebâtir le temple. Puisque la seule tribu à être retournée en masse fut celle de Yéhouda, le reste du peuple fut appelé « les Juifs » (Esd. 4:23).--> ?
1:13	YHWH répondit à l'ange qui parlait avec moi, par de bonnes paroles, par des paroles de consolation.
1:14	L'ange qui parlait avec moi me dit : Crie, en disant : Ainsi parle YHWH Tsevaot : Je suis jaloux pour Yeroushalaim et pour Sion d'une grande jalousie,
1:15	je suis dans une grande colère et fâché contre les nations qui sont à leur aise, car moi, j’étais un peu fâché, mais elles ont aidé au mal.
1:16	C'est pourquoi ainsi parle YHWH : Je suis revenu à Yeroushalaim avec compassion. Ma maison y sera bâtie, – déclaration de YHWH Tsevaot –, et le cordeau sera étendu sur Yeroushalaim.
1:17	Crie encore en disant : Ainsi parle YHWH Tsevaot : Mes villes déborderont encore de biens, et YHWH consolera encore Sion, et il choisira encore Yeroushalaim.

## Chapitre 2

### Vision des quatre cornes et des quatre forgerons

2:1	Levant les yeux, je regardai, et voici quatre cornes<!--Da. 7:7-11, 8:22 ; Ap. 13:1-11.-->.
2:2	Je dis à l’ange qui parlait avec moi : Que sont celles-ci ? Il me dit : Celles-ci sont les cornes qui ont dispersé Yéhouda, Israël et Yeroushalaim.
2:3	YHWH me montra quatre forgerons.
2:4	Je dis : Qu’est-ce que ceux-ci viennent faire ? Il répondit et dit : Celles-là sont les cornes qui ont dispersé Yéhouda au point que personne ne lève la tête. Mais ceux-ci sont venus pour les effrayer, et pour abattre les cornes des nations qui ont levé la corne contre la terre de Yéhouda pour la disperser.

### Yeroushalaim (Jérusalem) mesurée au cordeau

2:5	Levant les yeux, je regardai, et voici un homme qui avait à la main un cordeau pour mesurer,
2:6	auquel je dis : Où vas-tu ? Et il me dit : Je vais mesurer Yeroushalaim pour voir quelle est sa largeur et quelle est sa longueur.
2:7	Et voici, l'ange qui parlait avec moi sortit, et un autre ange sortit à sa rencontre.

### YHWH, la gloire de Yeroushalaim (Jérusalem)

2:8	Et il lui dit : Cours, et parle à ce jeune homme-là en disant : Yeroushalaim sera habitée comme les villes ouvertes à cause de la multitude d'êtres humains et de bêtes qui seront au milieu d'elle<!--Né. 1:3, 2:13.-->.
2:9	Mais je deviendrai pour elle, – déclaration de YHWH –, une muraille de feu tout autour, et je deviendrai sa gloire au milieu d'elle<!--Es. 60:19.-->.
2:10	Ha ! Ha ! Fuyez, – déclaration de YHWH –, fuyez hors de la terre du nord ! Car je vous ai dispersés vers les quatre vents des cieux, – déclaration de YHWH.
2:11	Ha ! Sion qui demeure avec la fille de Babel<!--Es. 48:20, 52:11 ; Jé. 50:8, 51:6.-->, sauve-toi !
2:12	Car ainsi parle YHWH Tsevaot, lequel après la gloire, m'a envoyé vers les nations qui vous ont pillées. Car celui qui vous touche, touche à la prunelle de son œil<!--De. 32:10 ; Ps. 17:8.-->.
2:13	Oui, voici, je vais remuer çà et là ma main contre elles, et elles deviendront la proie de leurs serviteurs, et vous saurez que YHWH Tsevaot m'a envoyé.
2:14	Pousse des cris de joie et réjouis-toi, fille de Sion ! Car voici, je viens<!--Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH qui vient ! C'est la seconde venue de Yéhoshoua ha Mashiah qui est évoquée ici (Es. 40:10-11 ; Za. 12:10-14, 14:1-10 ; Ac. 1:1-11 ; Ap. 1:7-8, 22:12-17).--> et j'habiterai au milieu de toi, – déclaration de YHWH.
2:15	Beaucoup de nations se joindront à YHWH en ce jour-là et deviendront mon peuple. J'habiterai au milieu de toi, et tu sauras que YHWH Tsevaot m'a envoyé vers toi.
2:16	Et YHWH possédera Yéhouda comme sa part sur le sol saint, et il choisira encore Yeroushalaim.
2:17	Que toute chair se taise face à YHWH ! Car il s'est réveillé de sa demeure sainte.

## Chapitre 3

### Yéhoshoua, le grand-prêtre justifié ; promesse de la venue du Germe de YHWH

3:1	Il me fit voir Yéhoshoua le grand-prêtre qui se tenait debout face à l'Ange de YHWH, et Satan qui se tenait debout à sa droite pour s'opposer à lui<!--Dans le but de l'accuser.-->.
3:2	YHWH dit à Satan : Que YHWH te réprimande, Satan ! Que YHWH, qui a choisi Yeroushalaim, te réprimande ! N'est-ce pas là un tison sauvé du feu<!--Am. 4:11 ; Jud. 1:9.--> ?
3:3	Or Yéhoshoua était vêtu de vêtements sales et il se tenait debout face à l'Ange.
3:4	Celui-ci répondit et parla à ceux qui se tenaient debout face à lui, disant : Ôtez-lui ces vêtements sales ! Puis il lui dit : Regarde, je passe sur ton iniquité et je te revêts de robes de fête.
3:5	Je dis : Qu'on mette sur sa tête un turban pur ! Et ils mirent un turban pur sur sa tête, puis ils lui mirent des vêtements<!--Ap. 19:8.-->. L'Ange de YHWH se tenait debout.
3:6	L'Ange de YHWH apporta ce témoignage à Yéhoshoua, en disant :
3:7	Ainsi parle YHWH Tsevaot : Si tu marches dans mes voies et si tu gardes mes injonctions, tu jugeras ma maison, tu garderas mes parvis et je te donnerai libre accès parmi ceux-ci, qui se tiennent debout.
3:8	S'il te plaît, écoute, Yéhoshoua, grand-prêtre, toi et tes compagnons qui sont assis en face de toi, car ce sont des hommes qui serviront de signes. Car voici, je ferai venir mon serviteur, le Germe<!--Le Germe est un autre nom de Yéhoshoua ha Mashiah, notre Seigneur (Es. 4:2).-->.
3:9	Car voici la pierre<!--Yéhoshoua ha Mashiah est le Rocher des âges (Es. 8:13-17 ; Ap. 5:1-7).--> que j'ai mise en face de Yéhoshoua. Sur cette pierre qui n'est qu'une<!--Cette pierre est une (« e'had »), c'est-à-dire indivisible (De. 6:4).-->, il y a sept yeux. Voici, je graverai moi-même ce qui doit y être gravé, – déclaration de YHWH Tsevaot. J'ôterai en un jour l'iniquité de cette terre.
3:10	En ce jour-là, – déclaration de YHWH Tsevaot –, vous appellerez, chaque homme son ami sous la vigne et sous le figuier.

## Chapitre 4

### Le chandelier d'or et ses sept lampes au milieu des deux oliviers

4:1	L’ange qui parlait avec moi revint et me réveilla comme un homme que l'on réveille de son sommeil.
4:2	Il me dit : Que vois-tu ? Je dis : Je regarde, et voici, il y a un chandelier tout en or, surmonté d'un vase et portant ses sept lampes, avec sept conduits pour les sept lampes qui sont au sommet du chandelier<!--Ap. 1:12-13.--> ;
4:3	sur lui, il y a deux oliviers, l'un à la droite du vase et l'autre à sa gauche.
4:4	Je répondis et dis à l’ange qui parlait avec moi, en disant : Mon Seigneur, que signifient ces choses ?
4:5	L'ange qui parlait avec moi répondit et me dit : Ne sais-tu pas qui sont ceux-là ? Je dis : Non, mon Seigneur !
4:6	Il répondit et me parla, disant : C’est ici la parole de YHWH à Zerubbabel<!--Zorobabel.-->, disant : Ce n'est ni par la puissance ni par la force, mais par mon Esprit, dit YHWH Tsevaot.
4:7	Qui es-tu, grande montagne, face à Zerubbabel ? Tu seras aplanie. Il fera sortir la pierre du sommet aux cris de : Grâce, grâce pour elle !

### Zerubbabel (Zorobabel) encouragé à achever l'œuvre

4:8	La parole de YHWH m'est apparue en disant :
4:9	Les mains de Zerubbabel ont fondé cette maison, et ses mains l'achèveront. Et tu sauras que YHWH Tsevaot m'a envoyé vers vous.
4:10	Car qui est-ce qui a méprisé le jour des faibles commencements ? Ils se réjouiront en voyant la pierre d'étain dans la main de Zerubbabel. Ces sept<!--Les sept yeux de YHWH sont aussi les sept yeux de l'Agneau (Ap. 5:6). Ces yeux représentent l'omniscience et l'omniprésence de Yéhoshoua ha Mashiah (Jésus-Christ) (Za. 3:9, 4:10, 14:7 ; Jn. 16:30 ; Ac. 1:24 ; Ap. 21:17).--> sont les yeux de YHWH qui parcourent toute la Terre.
4:11	Je répondis et lui dis : Que sont ces deux oliviers, à la droite et à la gauche du chandelier ?
4:12	Je répondis pour la seconde fois et lui dis : Que sont les deux branches d'olivier qui sont près des deux conduits d'or, d'où l'or découle ?
4:13	Il me répondit et dit : Ne sais-tu pas ce qu’elles sont ? Et je dis : Non, mon Seigneur.
4:14	Et il dit : Ce sont les deux fils<!--L'identité de ces deux individus est inconnue.--> de l'huile pure qui se tiennent devant le Seigneur de toute la Terre.

## Chapitre 5

### La malédiction répandue sur Israël

5:1	Je me retournai et levai les yeux pour regarder, et voici un rouleau qui volait.
5:2	Il me dit : Que vois-tu ? Je dis : Je vois un rouleau qui vole dont la longueur est de 20 coudées et la largeur de 10 coudées.
5:3	Il me dit : C'est la malédiction qui sort sur les faces de toute la terre. Car tout voleur de ceci, comme elle, sera nettoyé. Et tout parjure en ceci sera nettoyé comme elle.
5:4	Je la fais sortir – déclaration de YHWH Tsevaot, et elle entrera dans la maison du voleur et dans la maison de celui qui jure faussement en mon Nom, et elle logera au milieu de leur maison et la consumera avec son bois et ses pierres.

### L'épha s'en va en terre de Shinear

5:5	L'ange qui parlait avec moi sortit et me dit : Lève les yeux, s’il te plaît, et regarde : Qu’est-ce qui sort là ? 
5:6	Je dis : Qu'est-ce que c'est ? Il dit : C'est l'épha<!--L'épha était une unité de mesure utilisée dans le commerce des céréales, souvent à des fins frauduleuses (De. 25:14 ; Am. 8:5 ; Mi. 6:10.).--> qui sort dehors. Puis il dit : C'est leur œil sur toute la terre.
5:7	Et voici, on portait un disque de plomb et une femme était assise au milieu de l'épha<!--Cette femme représente la grande prostituée décrite en Ap. 17, avec sa coupe d'or pleine d'abominations et des impuretés de sa relation sexuelle illicite (v. 4). Cette femme est la figure du « mystère de la violation de la torah qui est déjà à l'œuvre » (2 Th. 2:7).-->.
5:8	Il dit : C'est là la méchanceté<!--Ou l'iniquité.-->. Puis il la jeta au milieu de l'épha, et il jeta la pierre de plomb sur l'ouverture.
5:9	Je levai les yeux et je regardai, et voici, deux femmes sortirent. Le vent soufflait dans leurs ailes : elles avaient des ailes comme les ailes de la cigogne<!--Les deux femmes sont décrites avec « des ailes de cigogne » (oiseau impur sous Moshé : Lé. 11:19) et portées par le vent (dans les Écritures, le vent est constamment lié au jugement : Es. 7:2, 26:6, 41:16 ; Job 27:20-22, 30:22).-->. Et elles enlevèrent l'épha entre la terre et les cieux.
5:10	Je dis à l'ange qui parlait avec moi : Où emportent-elles l'épha ?
5:11	Il me dit : C'est pour lui bâtir une maison en terre de Shinear<!--Ou Babylonie (Chaldée) (Ge. 10:6-12).-->. Et quand elle sera fermement établie, il sera déposé là sur sa base.

## Chapitre 6

### Les quatre vents des cieux

6:1	M’étant retourné, je levai les yeux, je regardai, et voici, quatre chars<!--Dans les Écritures, les chars et les chevaux représentent souvent la puissance d'Elohîm exerçant un jugement sur la Terre (Jé. 46:9-10 ; Joë. 2:3-11). Ce jugement concerne le monde entier (Ap. 6:1-8).--> sortaient d'entre deux montagnes. Et ces montagnes étaient des montagnes de cuivre.
6:2	Au premier char, il y avait des chevaux rouges, au deuxième char, des chevaux noirs,
6:3	au troisième char, des chevaux blancs, et au quatrième char, des chevaux tachetés et pommelés<!--Vient d'un mot hébreu qui signifie « fort, pommelé, couleur d'une pie ».-->.
6:4	Je répondis et dis à l’ange qui parlait avec moi : Ceux-là, que sont-ils, mon seigneur ? 
6:5	L'ange répondit et me dit : Ce sont les quatre vents des cieux, qui sortent du lieu où ils se tenaient devant le Seigneur de toute la Terre.
6:6	Celui auquel sont les chevaux noirs sort vers la terre du nord, et les blancs sortent après eux, et les tachetés sortent vers la terre du Théman.
6:7	Les pommelés sortirent et cherchèrent à aller parcourir la Terre. L'ange leur dit : Allez et parcourez la Terre ! Et ils parcoururent la Terre.
6:8	Il cria vers moi et me parla, en disant : Voici, ceux qui sortent vers la terre du nord ont apaisé mon Esprit en terre du nord.

### Le règne du Germe de YHWH

6:9	La parole de YHWH m'est apparue, en disant :
6:10	Prends de la main des exilés : de Heldaï, de Tobiyah et de Yekda`yah, et tu iras toi-même ce jour-là, tu iras dans la maison de Yoshiyah, fils de Tsephanyah, où ils se sont rendus en arrivant de Babel.
6:11	Prends de l'argent et de l'or et fais-en des couronnes, et mets-les sur la tête de Yéhoshoua, fils de Yehotsadaq, le grand-prêtre.
6:12	Et parle-lui, en disant : Ainsi a parlé YHWH Tsevaot, disant : Voici un homme dont le nom est Germe<!--Es. 4:2.-->, qui germera de son lieu et qui bâtira le temple de YHWH<!--C'est YHWH, c'est-à-dire Yéhoshoua ha Mashiah (Jésus-Christ) lui-même, qui bâtit son temple (Ps. 127:1-2 ; Mt. 16:18).-->.
6:13	C'est lui qui bâtira le temple de YHWH et y portera la majesté. Il s'assiéra et dominera sur son trône, il sera prêtre<!--Yéhoshoua ha Mashiah (Jésus-Christ) est notre Grand-Prêtre (Hé. 6:20, 7:1-28).--> sur son trône, et entre eux deux il y aura un conseil de paix.
6:14	Les couronnes seront pour Hélem, Tobiyah et Yekda`yah, et pour Hen, fils de Tsephanyah, un souvenir dans le temple de YHWH.
6:15	Et ceux qui sont éloignés viendront et bâtiront le temple de YHWH. Vous saurez que YHWH Tsevaot m'a envoyé vers vous. Et ceci arrivera si vous écoutez, si vous écoutez la voix de YHWH votre Elohîm.

## Chapitre 7

### Le jeûne formaliste

7:1	Il arriva, dans la quatrième année du roi Darius, que la parole de YHWH apparut à Zekaryah, le quatrième jour du neuvième mois, qui est le mois de Kisleu.
7:2	On avait envoyé à Béth-El Sharetser et Réguem-Mélek avec ses hommes pour supplier les faces de YHWH,
7:3	pour parler aux prêtres de la maison de YHWH Tsevaot et aux prophètes, en disant : Dois-je pleurer au cinquième mois, me tenant séparé comme je l'ai déjà fait pendant plusieurs années ?
7:4	La parole de YHWH Tsevaot vint à moi en disant :
7:5	Parle à tout le peuple de la terre et aux prêtres, en disant : Quand vous avez jeûné et pleuré au cinquième mois et au septième, et cela depuis 70 ans, avez-vous célébré ce jeûne par amour pour moi ?
7:6	Et quand vous mangez, quand vous buvez, n’est-ce pas vous qui mangez, vous qui buvez<!--Es. 58:3-4.--> ?
7:7	Ne connaissez-vous pas les paroles qu'a proclamées YHWH par la main des premiers prophètes, lorsque Yeroushalaim était habitée et paisible avec ses villes alentours et que le midi et la plaine étaient habités ?

### YHWH n'exauce pas les pécheurs

7:8	La parole de YHWH apparut à Zekaryah en disant :
7:9	Ainsi parlait YHWH Tsevaot, en disant : Rendez véritablement la justice et exercez la miséricorde et la compassion chaque homme envers son frère.
7:10	N'opprimez pas la veuve et l'orphelin, l'étranger et le pauvre, et ne méditez pas le mal dans vos cœurs, l'homme contre son frère<!--Ex. 22:20 ; Es. 1:23 ; Jé. 5:28 ; Pr. 22:22-23.-->.
7:11	Mais ils n'ont pas voulu écouter, ils eurent l'épaule rebelle, ils ont appesanti leurs oreilles pour ne pas entendre.
7:12	Ils firent de leur cœur un diamant pour ne pas écouter la torah, ni les paroles que YHWH Tsevaot leur envoyait par son Esprit, par la main des premiers prophètes. C'est pourquoi il y a eu une grande colère de la part de YHWH Tsevaot.
7:13	Il arriva que, comme il appelait et qu'ils n'écoutaient pas, de même ils ont appelé et je n'ai pas écouté dit YHWH Tsevaot<!--Es. 1:15 ; Jé. 11:11 ; Pr. 1:28.-->.
7:14	Je les ai dispersés par une tempête dans toutes les nations qu'ils ne connaissaient pas. La terre a été dévastée derrière eux, il n'y a plus eu ni allants ni venants. Et d'une terre de délices ils ont fait un désert.

## Chapitre 8

### Rétablissement du royaume d'Israël

8:1	La parole de YHWH Tsevaot vint à moi en disant :
8:2	Ainsi parle YHWH Tsevaot : Je suis jaloux pour Sion d'une grande jalousie, et je suis jaloux pour elle d'un grand courroux.
8:3	Ainsi parle YHWH : Je retourne à Sion, et j'habiterai au milieu de Yeroushalaim. Et Yeroushalaim sera appelée « ville fidèle », et la montagne de YHWH Tsevaot, « montagne sainte »<!--Es. 1:26.-->.
8:4	Ainsi parle YHWH Tsevaot : Il y aura encore des vieillards et des femmes âgées, assis dans les rues de Yeroushalaim, et chacun aura son bâton à la main, à cause du grand nombre de leurs jours.
8:5	Les places de la ville seront remplies de fils et de filles, jouant dans ses places.
8:6	Ainsi parle YHWH Tsevaot : Si cela paraît étonnant aux yeux du reste de ce peuple en ces jours-là, à mes yeux aussi cela sera-t-il étonnant ? – déclaration de YHWH Tsevaot.
8:7	Ainsi parle YHWH Tsevaot : Voici, je délivre mon peuple de la terre de l'orient et de la terre du soleil couchant.
8:8	Et je les ferai venir et ils habiteront dans Yeroushalaim, et ils seront mon peuple, et je serai leur Elohîm dans la vérité et la justice.

### Promesse de délivrance

8:9	Ainsi parle YHWH Tsevaot : Que vos mains se fortifient, vous qui entendez en ces jours ces paroles de la bouche des prophètes, le jour où la maison de YHWH a été fondée et son temple rebâti<!--Ag. 2:4.-->.
8:10	Car avant ces jours-là, il n'y avait pas de salaire pour l'être humain ni de salaire pour la bête. Il n'y avait pas de paix pour ceux qui entraient et sortaient, à cause de la détresse, et je lâchais tous les humains chacun contre son compagnon.
8:11	Mais maintenant je ne serai pas pour le reste de ce peuple comme aux premiers jours, – déclaration de YHWH Tsevaot.
8:12	Car les semailles prospéreront, la semence de paix sera là : la vigne rendra son fruit, la terre donnera ses produits, et les cieux donneront leur rosée. Je ferai hériter toutes ces choses au reste de ce peuple.
8:13	Il arrivera que comme vous avez été une malédiction parmi les nations, maison de Yéhouda et maison d'Israël, ainsi je vous sauverai et vous serez une bénédiction. N'ayez pas peur et fortifiez vos mains<!--Ge. 1:11 ; Ap. 22:2.--> !
8:14	Car ainsi parle YHWH Tsevaot : Comme j'ai eu la pensée de vous affliger, lorsque vos pères ont provoqué ma colère, dit YHWH Tsevaot, et que je ne m'en suis pas repenti,
8:15	ainsi je reviens en arrière et j'ai résolu en ces jours de faire du bien à Yeroushalaim et à la maison de Yéhouda. N'ayez pas peur !

### Ordonnances pour le peuple

8:16	Voici les choses que vous devez faire : Que chaque homme dise la vérité à son prochain, jugez selon la vérité et prononcez un jugement en vue de la paix dans vos portes<!--Ex. 20:16 ; Mt. 19:18 ; Lu. 18:20 ; Ep. 4:25.-->,
8:17	ne méditez pas dans vos cœurs le mal, l'homme contre son prochain, et n'aimez pas le faux serment, car ce sont là des choses que je hais, – déclaration de YHWH<!--Ps. 5:5, 11:5 ; Pr. 6:16-19.-->.
8:18	La parole de YHWH Tsevaot vint à moi en disant :
8:19	Ainsi parle YHWH Tsevaot : Le jeûne du quatrième mois, le jeûne du cinquième, le jeûne du septième et le jeûne du dixième seront changés pour la maison de Yéhouda en joie et en allégresse, et en fêtes solennelles de réjouissance. Aimez la vérité et la paix<!--Ep. 4:15.-->.

### Les nations adoreront YHWH, le seul Elohîm

8:20	Ainsi parle YHWH Tsevaot : Il viendra encore des peuples et des habitants de beaucoup de villes.
8:21	Les habitants de l'une iront à l'autre, en disant : Allons, allons implorer les faces de YHWH et chercher YHWH Tsevaot ! Nous irons aussi !
8:22	Beaucoup de peuples et de puissantes nations viendront chercher YHWH Tsevaot à Yeroushalaim<!--Jérusalem est appelée à devenir le centre d'adoration de la terre et la capitale du monde à cause de la présence d'Elohîm (Es. 66:23 ; Za. 14:16-21).-->, et implorer les faces de YHWH.
8:23	Ainsi parle YHWH Tsevaot : Il arrivera en ces jours-là, que dix hommes de toutes les langues des nations prendront et tiendront le pan de la robe d'un homme juif, et diront : Nous irons avec vous, car nous avons entendu qu'Elohîm est avec vous.

## Chapitre 9

### YHWH juge les villes aux alentours d'Israël

9:1	Fardeau, parole de YHWH sur la terre de Hadrac. Elle s'arrête sur Damas, car YHWH a l'œil sur les humains et sur toutes les tribus d'Israël.
9:2	Elle s'arrête aussi sur Hamath, à la frontière de Damas, sur Tyr et Sidon, bien qu’elle soit très sage.
9:3	Tyr s'est bâti une forteresse. Elle a amassé l'argent comme la poussière et l'or fin comme la boue des rues<!--Ez. 28:3-17.-->.
9:4	Voici qu'Adonaï en prendra possession, il frappera sur mer sa puissance, et elle-même sera dévorée par le feu<!--Ez. 26:3-4.-->.
9:5	Askalon le verra, et elle sera dans la crainte, Gaza aussi le verra, et un violent tremblement la saisira, ainsi qu'Ékron, car l'objet de son espoir sera confondu. Et il n'y aura plus de roi à Gaza, et Askalon ne sera plus habitée<!--So. 2:4.-->.
9:6	Un bâtard habitera dans Asdod, et j'abattrai l'orgueil des Philistins.
9:7	J'ôterai son sang de sa bouche et ses abominations d’entre ses dents. Lui aussi restera pour notre Elohîm, il sera comme un chef en Yéhouda, et Ékron sera comme le Yebousien.
9:8	Je camperai comme une garde autour de ma maison contre une armée, contre ceux qui passent et ceux qui reviennent, et aucun oppresseur ne passera plus chez eux, car maintenant je la vois de mes yeux.

### Prophétie sur la première venue du Mashiah

9:9	Réjouis-toi grandement, fille de Sion ! Crie de joie, fille de Yeroushalaim ! Voici, ton roi vient à toi, juste, sauveur, humble, monté sur un âne, sur un âne, le fils d'une ânesse<!--Cette prophétie s'est accomplie 500 ans après. Effectivement, Yéhoshoua (Jésus) est entré à Yeroushalaim (Jérusalem) monté sur un âne (Mt. 21:1-11 ; Lu. 19:28-40 ; Jn. 12:12-19).-->.

### YHWH délivrera son peuple

9:10	Je retrancherai d'Éphraïm les chars et de Yeroushalaim les chevaux, et les arcs de guerre seront aussi retranchés. Il parlera de paix aux nations, et sa domination ira de la mer à la mer, du fleuve aux extrémités de la Terre<!--Es. 57:19 ; Ps. 2:8, 72:8.-->.
9:11	Quant à toi, à cause de ton alliance scellée par le sang, je retirerai tes captifs de la fosse où il n'y a pas d'eau.
9:12	Retournez à la forteresse, captifs pleins d'espérance ! Aujourd'hui même je le déclare, je te rendrai le double.
9:13	Car j'ai tendu pour moi Yéhouda, d'Éphraïm j’ai rempli mon arc, et je réveillerai tes fils, Sion, contre tes fils, Yavan ! Je te rendrai telle que l’épée d'un puissant homme.
9:14	YHWH apparaîtra au-dessus d’eux, et sa flèche partira comme l'éclair. Adonaï YHWH sonnera du shofar<!--1 Th. 4:16.-->, il marchera dans le tourbillon du Théman.
9:15	YHWH Tsevaot les protégera, ils dévoreront après avoir foulé aux pieds les pierres de fronde. Ils boiront et seront bruyants comme des hommes ivres, ils se rempliront de vin comme une cuvette, comme les coins de l'autel.
9:16	YHWH, leur Elohîm, les sauvera en ce jour-là, comme le troupeau de son peuple, car ils sont les pierres d'une couronne qui brilleront sur son sol.
9:17	Car combien est grande sa bonté ! Quelle beauté ! Le blé fera prospérer les jeunes hommes, et le vin nouveau les vierges.

## Chapitre 10

### YHWH rassemblera son peuple

10:1	Demandez à YHWH la pluie, au temps de la pluie du printemps<!--La pluie de la première saison correspond aux fortes précipitations qui tombent en Israël pendant quelques jours durant les mois de novembre et décembre. Très attendues par les fermiers, ces pluies rendent le sol, habituellement très dur, labourable et propice à être ensemencé. La pluie de l'arrière-saison, semblable à la première, tombe quant à elle juste avant la moisson.--> ! YHWH produira des éclairs et il vous donnera une averse, une pluie, il donnera à chaque homme de l'herbe dans son champ.
10:2	Car les théraphim ont des paroles vaines, et les devins prophétisent le mensonge, ils profèrent des rêves vains et consolent par la vanité. C'est pourquoi ils sont errants comme des brebis, ils sont malheureux parce qu'il n'y a pas de berger<!--Jé. 23:21-30 ; Ez. 34:2 ; Mt. 9:36.-->.
10:3	Ma colère s'est enflammée contre ces bergers et je châtierai les boucs. Car YHWH Tsevaot visite son troupeau, la maison de Yéhouda, il en fait son cheval majestueux dans la bataille.
10:4	De lui sortira l'Angle<!--L'Angle ou la Pierre angulaire (Yéhoshoua ha Mashiah) : Es. 8:13-17 ; 1 Pi. 2:7.-->, de lui sortira le clou, de lui sortira l'arc de bataille, et de lui sortiront tous les chefs ensemble.
10:5	Ils seront comme des vaillants hommes foulant la boue des rues dans la bataille, et ils combattront, parce que YHWH sera avec eux, et ceux qui sont montés sur des chevaux seront couverts de honte.
10:6	Je fortifierai la maison de Yéhouda et je sauverai la maison de Yossef. Oui, je leur donnerai une demeure. Oui, j'aurai compassion d'eux, et ils seront comme si je ne les avais pas rejetés. Oui, moi, YHWH, leur Elohîm, je leur répondrai.
10:7	Éphraïm deviendra comme un homme vaillant, et leur cœur se réjouira comme par le vin. Leurs enfants le verront et se réjouiront, leur cœur se réjouira en YHWH.
10:8	Je sifflerai pour les rassembler, car je les rachète, et ils se multiplieront comme ils se multipliaient.
10:9	Je les sèmerai parmi les peuples, mais dans les contrées lointaines, ils se souviendront de moi, ils vivront avec leurs fils et ils reviendront.
10:10	Je les ramènerai de la terre d'Égypte, je les rassemblerai de l'Assyrie, je les ferai venir en terre de Galaad et au Liban, mais il ne s’en trouvera pas assez pour eux.
10:11	Il passera la mer de détresse, et il frappera les flots de la mer, et toutes les profondeurs du fleuve seront desséchées. L'orgueil de l'Assyrie sera abattu et le sceptre d'Égypte sera ôté.
10:12	Je les fortifierai en YHWH, et ils marcheront en son Nom, – déclaration de YHWH.

## Chapitre 11

### Les houlettes du vrai berger

11:1	Liban, ouvre tes portes et que le feu dévore tes cèdres !
11:2	Cyprès, gémis, car le cèdre est tombé, parce que les choses magnifiques ont été ravagées ! Chênes de Bashân, gémissez, car la forêt inaccessible est coupée !
11:3	Les bergers poussent des cris de lamentations, parce que leur magnificence est ravagée. On entend le cri de rugissement des lionceaux, parce que l'orgueil du Yarden<!--Jourdain.--> est abattu.
11:4	Ainsi parle YHWH, mon Elohîm : Fais paître les brebis de la tuerie !
11:5	Leurs possesseurs les tuent, sans qu'on les tienne pour coupables, et celui qui les vend dit : Béni soit YHWH, car je m'enrichis ! Et leurs bergers ne les épargnent pas.
11:6	Car je n'ai plus de pitié pour les habitants de la terre, – déclaration de YHWH ! Voici, moi-même je livre les humains, chaque homme aux mains de son compagnon et aux mains de son roi. Ils ravageront la terre et je ne délivrerai pas de leurs mains.
11:7	Je fis paître les brebis de la tuerie, qui sont véritablement les plus misérables du troupeau. Je pris pour moi deux verges, j'appelai l'une Grâce et l'autre Cordon et je fis paître les brebis.
11:8	Et j’exterminai les trois bergers en un mois, car mon âme était impatiente à leur sujet, et leur âme aussi éprouva du dégoût pour moi.
11:9	Je dis : Je ne vous ferai plus paître ! Que celle qui meurt, meure ! Que celle qui va périr, périsse ! Quant à celles qui restent, qu’elles dévorent chaque femme la chair de sa voisine !
11:10	Je pris ma verge Grâce et je la brisai pour rompre mon alliance que j'avais traitée avec tous ces peuples.
11:11	Elle fut rompue en ce jour-là, et les plus malheureuses brebis<!--Les plus malheureuses brebis sont le reste d'Israël.--> qui prirent garde à moi, reconnurent ainsi que c'était la parole de YHWH.
11:12	Je leur dis : Si cela est bon à vos yeux, donnez mon salaire ! Sinon, cessez ! Et ils pesèrent<!--Mt. 26:15, 27:9-10.--> pour mon salaire 30 pièces d'argent<!--Selon la torah de Moshé (loi de Moïse), pour racheter un mâle de 20 à 60 ans, ayant fait un vœu, il fallait payer 50 sicles d'argent (Lé. 27:3). Pour dédommager un préjudice causé par un bœuf ayant frappé un esclave, on devait donner 30 sicles d'argent au maître de l'esclave et lapider le bœuf (Ex. 21:32). Or le prix du Seigneur a été estimé à 30 sicles d'argent, comme pour les esclaves.--> !
11:13	YHWH me dit : Jette-le au potier, ce prix honorable auquel ils m'ont estimé ! Je pris les 30 pièces d'argent et les jetai dans la maison de YHWH pour le potier.
11:14	Je brisai ma seconde verge, « Liens », pour rompre la fraternité entre Yéhouda et Israël.

### Les faux bergers (pasteurs)

11:15	YHWH me dit : Prends encore pour toi l’équipement d'un berger insensé !
11:16	Car voici, je susciterai sur la terre un berger qui ne prendra pas soin de celles qui sont détruites. Il ne cherchera pas celles qui sont dispersées, il ne guérira pas celles qui sont brisées, il ne soutiendra pas celles qui sont debout, mais il dévorera la chair des plus grasses et il déchirera jusqu'aux cornes de leurs pieds.
11:17	Malheur au berger bon à rien qui abandonne les brebis ! Que l'épée fonde sur son bras et sur son œil droit ! Que son bras se dessèche, qu'il se dessèche et que son œil droit s'éteigne, qu'il s'éteigne !

## Chapitre 12

### Yeroushalaim (Jérusalem), une coupe d'étourdissement pour les nations

12:1	Fardeau, parole de YHWH, sur Israël. Déclaration de YHWH, qui a étendu les cieux et fondé la Terre, et qui a formé l'esprit de l'être humain au-dedans de lui :
12:2	Voici, je ferai de Yeroushalaim une coupe d'étourdissement pour tous les peuples d'alentour. Il en sera même ainsi pour Yéhouda lors du siège de Yeroushalaim<!--Ap. 16:12-16.-->.
12:3	Il arrivera en ce jour-là que je ferai de Yeroushalaim une pierre pesante pour tous les peuples, et tous ceux qui la soulèveront seront meurtris, meurtris. Toutes les nations de la Terre se rassembleront contre elle.
12:4	En ce jour-là, – déclaration de YHWH –, je frapperai d'égarement tous les chevaux, et de folie ceux qui les monteront. Mais j'aurai les yeux ouverts sur la maison de Yéhouda, et je frapperai d'aveuglement tous les chevaux des peuples.
12:5	Les chefs de Yéhouda diront en leur cœur : Les habitants de Yeroushalaim sont notre force, par YHWH Tsevaot, leur Elohîm.
12:6	En ce jour-là, je ferai des chefs de Yéhouda un foyer de feu parmi du bois, une torche de feu parmi des gerbes. Ils dévoreront à droite et à gauche tous les peuples d'alentour, et Yeroushalaim demeurera encore à sa place, à Yeroushalaim.
12:7	YHWH sauvera premièrement les tentes de Yéhouda afin que la gloire de la maison de David, la gloire des habitants de Yeroushalaim ne s'élève pas au-dessus de Yéhouda.
12:8	En ce jour-là, YHWH protégera les habitants de Yeroushalaim et le plus faible parmi eux sera, en ce jour-là, comme David. La maison de David sera comme Elohîm, comme l'Ange de YHWH en face d’eux.
12:9	Il arrivera qu'en ce jour-là, je chercherai à détruire toutes les nations qui viendront contre Yeroushalaim.

### Repentance et délivrance d'Israël

12:10	Je répandrai sur la maison de David et sur les habitants de Yeroushalaim l'Esprit de grâce et de supplications<!--Joë. 3:1-2.-->, et ils regarderont vers Aleph Tav<!--Voir Ap. 1:8, 21:6 et 22:13. Yéhoshoua est l'Aleph et le Tav qu'on a transpercé.-->, lequel ils ont percé<!--Il est question ici du Seigneur Yéhoshoua ha Mashiah. Jn. 19:37 ; Ap. 1:7.-->. Il y aura des gémissements<!--Au retour du Mashiah, il y aura une repentance et une conversion nationale d'Israël (Ro. 11:26).--> sur lui comme pour les gémissements d'un unique, ils seront amers à cause de lui comme quand on est amer à cause d'un premier-né.
12:11	En ce jour-là, il y aura un grand gémissement à Yeroushalaim, comme le gémissement d'Hadadrimmon dans la vallée de Meguiddon.
12:12	La terre sera dans le deuil, chaque famille à part : la famille de la maison de David à part et les femmes de cette maison-là à part, la famille de la maison de Nathan à part et les femmes de cette maison-là à part,
12:13	la famille de la maison de Lévi à part et les femmes de cette maison-là à part, la famille de Shimeï à part et ses femmes à part.
12:14	Toutes les autres familles, chaque famille à part et leurs femmes à part.

## Chapitre 13

### YHWH ôte les faux prophètes

13:1	En ce jour-là, il y aura une source ouverte en faveur de la maison de David et des habitants de Yeroushalaim, pour le péché et pour la souillure.
13:2	Il arrivera en ce jour-là, – déclaration de YHWH Tsevaot –, que je retrancherai de la terre les noms des faux elohîm, et on n'en fera plus mention. Je ferai passer hors de la terre les prophètes et l'esprit d'impureté.
13:3	Il arrivera que si un homme prophétise encore, son père et sa mère qui l'ont engendré, lui diront : Tu ne vivras plus car tu as prononcé des mensonges au Nom de YHWH ! Son père et sa mère qui l'ont engendré le transperceront quand il prophétisera.
13:4	Il arrivera, en ce jour-là, que les prophètes auront honte, chaque homme de sa vision, quand ils prophétiseront. Ils ne s'habilleront plus du manteau de poil pour tromper.
13:5	Il dira : Je ne suis pas prophète, je suis un homme qui travaille le sol, car un être humain m'a acquis dès ma jeunesse<!--La Bible version Martin a traduit par : « On m'a appris à gouverner du bétail ».-->.
13:6	Mais on lui dira : Pourquoi ces plaies sur tes mains ? Il dira : J'ai été frappé dans la maison de ceux qui m'aiment.

### Le Mashiah, le Bon Berger

13:7	Épée, réveille-toi contre mon Berger<!--Mon Berger : il est question de Yéhoshoua ha Mashiah (Jésus-Christ), le Bon Berger (Ps. 23 ; Jn. 10:1-17).--> et sur l'homme fort qui est mon compagnon ! – déclaration de YHWH Tsevaot. Frappe le Berger<!--Frappe le Berger : cette prophétie fait référence à la crucifixion du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) (Ge. 3:15 ; Mt. 26:31 ; Mc. 14:27, 14:50, 15:19).--> et les brebis seront dispersées, et je tournerai ma main vers les faibles.

### Le reste du peuple éprouvé et purifié

13:8	Et il arrivera sur toute la terre, – déclaration de YHWH –, que les deux tiers seront retranchés et périront, mais l'autre tiers y restera.
13:9	Je ferai entrer ce tiers dans le feu et je le purifierai comme on purifie l'argent, je l'éprouverai comme on éprouve l'or. Il invoquera mon Nom et je lui répondrai. Je dirai : C'est mon peuple ! Et il dira : YHWH est mon Elohîm<!--Ps. 50:15, 91:15, 144:15 ; 1 Pi. 1:6-7.--> !

## Chapitre 14

### Le jour de YHWH

14:1	Voici, le jour de YHWH<!--Les expressions « jour de YHWH » ou « jour du Seigneur » sont récurrentes dans la Bible (19 fois dans le Tanakh et 42 fois dans les Écrits de la nouvelle alliance). Elles désignent un temps arrêté par le Seigneur durant lequel il interviendra de manière directe et personnelle dans les affaires humaines. En ce jour-là, Elohîm exprimera sa colère et exercera sa justice sur les impies d'Israël (Es. 22 ; Jé. 30:1-17 ; Joë. 1-3 ; Am. 5 ; So. 1) et ceux du monde entier (Ez. 38-39 ; Za. 14).--> vient, et ton butin sera partagé au milieu de toi.
14:2	Je rassemblerai toutes les nations à Yeroushalaim pour qu'elles lui fassent la guerre<!--Joë. 3 ; Ap. 16:12-16.--> : la ville sera prise, les maisons pillées et les femmes violées. La moitié de la ville ira en captivité, mais le reste du peuple ne sera pas retranché de la ville.
14:3	YHWH sortira et il combattra contre ces nations, comme il a combattu au jour de la bataille<!--Es. 31:4.-->.

### Retour visible et glorieux du Seigneur

14:4	Ses pieds se tiendront debout en ce jour sur la Montagne des Oliviers<!--Ce sont les pieds de Yéhoshoua ha Mashiah (Jésus-Christ) (Ac. 1:10-11).-->, qui est en face de Yeroushalaim, à l'orient. La Montagne des Oliviers se fendra par le milieu, d'est en ouest, en une très grande vallée : une moitié de la montagne reculera vers le nord, et l'autre moitié vers le midi.
14:5	Vous fuirez dans la vallée de mes montagnes, car la vallée des montagnes atteindra jusqu’à Atzel. Vous fuirez comme vous avez fui devant le tremblement de terre, au temps d'Ouzyah, roi de Yéhouda. YHWH, mon Elohîm viendra, tous les saints avec lui<!--Ce passage confirme clairement que Yéhoshoua ha Mashiah est YHWH (Es. 34:5, 40:10-11, 62:11-12 ; 1 Th. 3:13 ; Jud. 14-15.).-->.
14:6	Il arrivera, en ce jour-là, qu'il n'y aura pas de lumière précieuse, mais du froid et du gel.
14:7	Ce sera le jour un, il est connu de YHWH, il n’y aura ni jour ni nuit, mais il arrivera qu’au temps du soir apparaîtra la lumière.
14:8	Il arrivera qu'en ce jour-là, des eaux vives<!--Ez. 47:1-12 ; Ap. 22:1-2.--> sortiront de Yeroushalaim, la moitié d'elles coulera vers la mer orientale, et l'autre moitié vers la mer occidentale. Il en sera ainsi été et hiver.

### Le Royaume du Mashiah

14:9	YHWH deviendra Roi sur toute la Terre. En ce jour-là, YHWH sera un, et son Nom un<!--Littéralement « e'had ». Voir commentaire en Ge. 1:5. Voir également De. 6:4 ; Ja. 2:19 ; Ga. 3:20 ; 1 Ti. 2:5.-->.
14:10	Toute la terre deviendra comme la région aride, depuis Guéba jusqu'à Rimmon, au midi de Yeroushalaim. Et Yeroushalaim sera exaltée et restera à sa place, depuis la porte de Benyamin jusqu'à l'endroit de la première porte, jusqu'à la porte des angles, et depuis la tour de Hananeel jusqu'aux pressoirs du roi.
14:11	On y habitera, et elle ne sera plus vouée à une entière destruction. Yeroushalaim sera habitée en sécurité.
14:12	Voici la plaie dont YHWH frappera tous les peuples qui auront fait la guerre contre Yeroushalaim : il fera pourrir leur chair tandis qu'ils seront sur leurs pieds, leurs yeux pourriront dans leurs orbites et leur langue pourrira dans leur bouche.
14:13	Et il arrivera en ce jour-là, que YHWH produira un grand trouble parmi eux. Car chaque homme saisira la main de son compagnon, et sa main s’élèvera contre la main de son compagnon.
14:14	Yéhouda combattra aussi dans Yeroushalaim, et les richesses de toutes les nations d'alentour y seront amassées : l'or, l'argent et des vêtements en très grand nombre.
14:15	Et la même plaie sera sur les chevaux, les mulets, les chameaux, les ânes et sur toutes les bêtes qui seront dans ces camps : une plaie semblable à celle-là.

### Adoration de YHWH Tsevaot dans le Royaume (fête des cabanes ou des tabernacles)

14:16	Il arrivera que tous ceux qui resteront de toutes les nations venues contre Yeroushalaim, monteront en foule d'année en année pour adorer le Roi, YHWH Tsevaot, et pour célébrer la fête des cabanes.
14:17	Il arrivera, quant à ceux d’entre les familles de la Terre qui ne monteront pas à Yeroushalaim pour adorer le Roi, YHWH Tsevaot, que la pluie ne tombera pas sur eux.
14:18	Si la famille d'Égypte ne monte pas, si elle ne vient pas, sur eux non plus il n’y en aura pas. Le fléau surviendra, celui dont YHWH frappera les nations qui ne monteront pas pour célébrer la fête des cabanes.
14:19	Tel sera le péché de l'Égypte, et le péché de toutes les nations qui ne monteront pas pour célébrer la fête des cabanes.
14:20	En ce jour-là, il y aura sur les clochettes des chevaux : « Sainteté à YHWH ! » Les chaudières dans la maison de YHWH seront comme les cuvettes devant l'autel.
14:21	Toute chaudière qui sera à Yeroushalaim et dans Yéhouda, sera consacrée à YHWH Tsevaot. Tous ceux qui sacrifieront viendront en prendre pour y cuire des viandes. Il n'y aura plus de Kena'ânéens<!--« Un négociant », « commerçant », « marchand ».--> dans la maison de YHWH Tsevaot, en ce jour-là.
