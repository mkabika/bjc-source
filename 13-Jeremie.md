# Yirmeyah (Jérémie) (Jé.)

Signification : Celui que Yah a désigné

Auteur : Yirmeyah (Jérémie)

Thème : Avertissements et jugements

Date de rédaction : 7ème siècle av. J.-C.

Issu d'une famille de prêtres, Yirmeyah fut appelé dès son plus jeune âge au service de YHWH et exerça un service prophétique avant et pendant les premières années de déportation. Outre son message à Israël et aux nations, le livre de Yirmeyah révèle sa personnalité. On découvre alors que l'opposition de ses pairs fut l'une de ses expériences les plus douloureuses. En effet, ce récit raconte ses combats contre les faux prophètes et met en évidence les signes accompagnant les prophètes authentiques, à savoir la souffrance, la solitude, l'incompréhension et le rejet.

Son message annonçait le jugement imminent d'Elohîm et invitait le peuple à la repentance pour éviter le châtiment de YHWH. Après la chute de Yeroushalaim (Jérusalem), alors que Neboukadnetsar lui avait laissé le choix, Yirmeyah décida de rester avec les plus pauvres plutôt que de partir pour Babel (Babylone). Cependant, des Israélites décidèrent de s'expatrier en Égypte et l'entraînèrent avec eux de force. En terre étrangère, Yirmeyah continua de porter le fardeau de son peuple, l'exhortant à réformer ses voies.

Parmi les prophéties de Yirmeyah, figure le retour du peuple d'Israël sur la terre promise avant la seconde venue du Mashiah.

## Chapitre 1

### YHWH appelle Yirmeyah à son service

1:1	Paroles de Yirmeyah, fils de Chilqiyah<!--Grand-prêtre d'Anathoth. 2 R. 22 ; 2 Ch. 35-9.-->, d'entre les prêtres qui étaient à Anathoth, en terre de Benyamin,
1:2	auquel vint la parole de YHWH aux jours de Yoshiyah, fils d'Amon, roi de Yéhouda, la treizième année de son règne,
1:3	elle vint aux jours de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda, jusqu'à la fin de la onzième année de Tsidqiyah, fils de Yoshiyah, roi de Yéhouda - jusqu’à l’exil de Yeroushalaim, au cinquième mois.
1:4	La parole de YHWH m'est apparue, en disant :
1:5	Avant que je t'aie formé dans le ventre, je te connaissais, et avant que tu sois sorti de la matrice, je t'avais consacré, je t'avais établi<!--Vient de l'hébreu « nathan » qui signifie « donner », « poser », « mettre ». Je t'avais donné en tant que prophète pour les nations.--> prophète pour les nations<!--Es. 49:5 ; Ga. 1:15.-->.
1:6	Je dis : Ah ! Adonaï YHWH ! Voici, je ne sais pas parler, car je suis un jeune garçon<!--Ex. 4:10-11.--> !
1:7	Et YHWH me dit : Ne dis pas : Je suis un jeune garçon. Car tu iras partout où je t'enverrai, et tu diras tout ce que je t'ordonnerai.
1:8	N'aie pas peur de leurs faces, car je suis avec toi pour te délivrer, – déclaration de YHWH.
1:9	YHWH étendit sa main et toucha ma bouche, et YHWH me dit : Voici, je mets mes paroles dans ta bouche.
1:10	Regarde, je t’ai préposé<!--« Désigner », « nommer à une charge ».--> aujourd'hui sur les nations et sur les royaumes, pour arracher et pour abattre, pour faire périr et pour détruire, pour bâtir et pour planter<!--Yirmeyah (Jérémie) devait d'abord arracher, abattre, ruiner et détruire avant de bâtir et de planter. Il y avait dans le temple de Yeroushalaim (Jérusalem) les autels de Baal et le pieu d'Asherah (2 R. 21). De même, avant de planter la parole d'Elohîm qui est une semence plantée dans les cœurs (Mc. 4:3-17), il est nécessaire au préalable d'arracher et de renverser les fausses doctrines et le péché en les dénonçant.-->.

### YHWH confirme la mission de Yirmeyah et l'établit sur Yéhouda

1:11	La parole de YHWH m'est apparue, en disant : Que vois-tu, Yirmeyah ? Je dis : Je vois une branche d'amandier<!--Vient de l'hébreu « shaqad » qui signifie « s'éveiller », « veiller », « veiller à », « éveiller », « être alerté », « être vigilant sur ». L'amandier a la particularité rare de fleurir bien avant l'apparition de ses feuilles. L'amandier est le premier arbre à annoncer le printemps tandis que les autres arbres sont totalement endormis dans la torpeur de l'hiver.-->.
1:12	YHWH me dit : Tu as bien vu, car je veille sur ma parole pour l'exécuter.
1:13	La parole de YHWH m'est apparue pour la seconde fois, en disant : Que vois-tu ? Je dis : Je vois un pot bouillant. Ses faces sont face au nord.
1:14	Et YHWH me dit : Le mal se découvrira du côté du nord sur tous les habitants de la terre.
1:15	Car voici, je vais appeler toutes les familles des royaumes du nord, – déclaration de YHWH. Elles viendront, et chacune mettra son trône à l'entrée des portes de Yeroushalaim, devant toutes ses murailles à l'entour, et devant toutes les villes de Yéhouda.
1:16	Je prononcerai mes jugements contre eux, à cause de toute leur méchanceté, parce qu'ils m'ont abandonné, qu'ils ont brûlé de l’encens à d’autres elohîm et qu'ils se sont prosternés devant les œuvres de leurs mains.
1:17	Toi, ceins tes reins, lève-toi, et dis-leur tout ce que je t'ordonnerai. N'aie pas peur en face d’eux sinon je te ferai peur en face d’eux.
1:18	Et moi, voici que je t'établis aujourd'hui sur toute la terre comme une ville forte, une colonne de fer et un mur de cuivre, contre les rois de Yéhouda, contre les chefs de la terre, contre ses prêtres et contre le peuple de la terre.
1:19	Ils te feront la guerre, mais ils ne seront pas plus forts que toi, car je suis avec toi, – déclaration de YHWH –, pour te délivrer.

## Chapitre 2

### YHWH dénonce l'attitude d'Israël et l'avertit

2:1	La parole de YHWH m'est apparue en disant :
2:2	Va et crie aux oreilles de Yeroushalaim, et dis : Ainsi parle YHWH : Je me souviens de la fidélité de ta jeunesse, de l'amour de tes fiançailles, quand tu venais après moi dans le désert, sur une terre qu'on n'ensemence pas.
2:3	Israël était consacré à YHWH, il était le premier de son revenu<!--Lé. 23:20 ; Pr. 3:9 ; Né. 10:36.-->. Tous ceux qui le dévoraient étaient coupables, et le malheur les atteignait, – déclaration de YHWH.
2:4	Écoutez la parole de YHWH, maison de Yaacov, et vous toutes les familles de la maison d'Israël !
2:5	Ainsi parle YHWH : Quelle injustice vos pères ont-ils trouvée en moi, pour qu'ils se soient éloignés de moi, et qu'ils aient marché après la vanité et soient devenus vains ?
2:6	Et ils n'ont pas dit : Où est YHWH qui nous a fait monter de la terre d'Égypte, qui nous a fait marcher dans le désert, sur une terre aride et pleine de fosses, sur une terre de sécheresse et d'ombre de mort, terre où pas un homme n'était passé, où pas un humain n'avait habité ?
2:7	Je vous ai fait venir sur une terre de verger, pour que vous en mangiez les fruits et les biens. Mais vous êtes venus et vous avez rendu ma terre impure, et vous avez rendu abominable mon héritage.
2:8	Les prêtres n'ont pas dit : Où est YHWH ? Ceux qui manient la torah ne m'ont pas connu, les bergers se sont révoltés contre moi, les prophètes ont prophétisé par Baal<!--Baal. Voir Jg. 2:13.-->, et sont allés après ce qui n'est d'aucun profit.
2:9	C'est pourquoi je contesterai encore avec vous, – déclaration de YHWH –, je contesterai avec les fils de vos fils.
2:10	Passez par les îles de Kittim et voyez ! Envoyez quelqu'un à Qedar, observez bien et voyez s'il n'y a rien de semblable !
2:11	Y a-t-il une nation qui change ses elohîm, quoiqu'ils ne soient pas des elohîm ? Mais mon peuple a changé sa gloire contre ce qui n'est d'aucun profit<!--Ro. 1:23.--> !
2:12	Cieux, soyez étonnés de cela, frissonnez et soyez extrêmement asséchés ! – déclaration de YHWH.
2:13	Car mon peuple a commis doublement le mal : Ils m'ont abandonné, moi, la source d'eaux vives<!--YHWH est la Source d'eaux vives. Yéhoshoua ha Mashiah (Jésus-Christ) se présente aussi comme la Source d'eau vive (Jn. 4:13-14 ; Ap. 21:6).-->, pour se creuser des citernes, des citernes brisées qui ne retiennent pas l'eau.
2:14	Israël est-il un esclave, ou un esclave né dans la maison ? Pourquoi est-il mis au pillage ?
2:15	Les lionceaux rugissent, poussent leurs cris contre lui, et ils mettent sa terre en ruine. Ses villes sont brûlées, de sorte que personne n'y habite.
2:16	Même les fils de Noph et de Tachpanès te casseront le sommet de la tête.
2:17	Cela ne t'arrive-t-il pas parce que tu as abandonné YHWH, ton Elohîm, à l'époque où il te conduisait par le chemin ?
2:18	Et maintenant, pourquoi prendrais-tu le chemin de l'Égypte pour boire l'eau du Shiychor<!--Shiychor, qui signifie « sombre, noir, boueux », était l'un des affluents du Nil.--> ? Pourquoi prendrais-tu le chemin de l'Assyrie pour boire l'eau du fleuve ?
2:19	Ta méchanceté te châtiera et ton apostasie te jugera. Tu sauras et tu verras que c'est une chose mauvaise et amère d'abandonner YHWH, ton Elohîm, et de n'avoir de moi aucune crainte, – déclaration d'Adonaï YHWH Tsevaot.
2:20	Parce que depuis longtemps j'ai brisé ton joug, et rompu tes liens, tu as dit : Je ne serai plus dans la servitude. C'est pourquoi tu as erré en te prostituant sur toute haute colline, et sous tout arbre vert.
2:21	Or je t'avais moi-même plantée, vigne de choix, dont toute la semence était vraie. Comment t'es-tu changée à mon égard en sarments d'une vigne étrangère ?
2:22	Même si tu te lavais avec du nitre et que tu multipliais le savon, ton iniquité serait une tache devant moi, – déclaration d'Adonaï YHWH.
2:23	Comment peux-tu dire : Je ne me suis pas rendue impure, je ne suis pas allée après les Baalim ! Regarde tes pas dans la vallée, reconnais ce que tu as fait, dromadaire à la course légère dont les routes se tortillent !
2:24	Anesse sauvage, accoutumée au désert, humant le vent dans le désir de son âme, qui l'arrêtera dans son ardeur ? Tous ceux qui la cherchent n'ont pas à se fatiguer : ils la trouvent pendant son mois.
2:25	Empêche ton pied d’être pieds nus, et ton gosier de se dessécher ! Mais tu dis : Non, c'est sans espoir ! Car j'aime les étrangers, et j'irai après eux.
2:26	Comme un voleur est confus quand il est surpris, ainsi seront confus ceux de la maison d'Israël, eux, leurs rois, leurs chefs, leurs prêtres et leurs prophètes.
2:27	Ils disent au bois : Tu es mon père ! Et à la pierre : Tu m'as engendré ! Car ils me tournent le dos, et non les faces. Et ils disent dans le temps de leur malheur : Lève-toi, et sauve-nous !
2:28	Où sont tes elohîm que tu t'es faits ? Qu'ils se lèvent, s'ils peuvent te sauver au temps de ton malheur ! Car tu as autant d'elohîm que de villes, Yéhouda !
2:29	Pourquoi contesteriez-vous avec moi ? Vous vous êtes tous rebellés contre moi, – déclaration de YHWH.
2:30	En vain ai-je frappé vos fils : ils n'ont pas reçu d'instruction. Votre épée a dévoré vos prophètes comme un lion destructeur.
2:31	Hommes de cette génération, considérez la parole de YHWH ! Ai-je été un désert pour Israël, ou une terre de ténèbres ? Pourquoi mon peuple dit-il : Nous sommes les maîtres, nous ne viendrons plus à toi ?
2:32	La vierge oublie-t-elle ses ornements, l'épouse sa ceinture ? Mais mon peuple m'a oublié depuis des jours sans nombre.
2:33	Pourquoi disposes-tu bien tes voies pour chercher l'amour ? Aussi, même dans le mal tu enseignes tes voies !
2:34	Même sur les pans de tes vêtements on trouve le sang des âmes pauvres et innocentes que tu n'as pas trouvés en effraction.
2:35	Et tu dis : Oui, je suis innocente ! En effet, sa colère s'est détournée de moi. Voici, je vais contester avec toi parce que tu as dit : Je n'ai pas péché.
2:36	Pourquoi cours-tu tant ici et là pour changer tes voies ? Tu auras aussi honte de l'Égypte comme tu as eu honte de l'Assyrie.
2:37	Tu sortiras même d'ici, ayant tes mains sur la tête, car YHWH rejette les fondements de ta confiance, et tu n'auras aucune prospérité par eux.

## Chapitre 3

### Israël comparé à une prostituée

3:1	On dit : Si un homme renvoie sa femme et qu'elle le quitte pour devenir la femme d'un autre, cet homme retournera-t-il encore vers elle<!--Lé. 21:7 ; De. 24:2.--> ? La terre même n'en serait-elle pas souillée, souillée ? Or toi, tu t'es prostituée à de nombreux compagnons, toutefois reviens à moi, – déclaration de YHWH.
3:2	Lève tes yeux vers les lieux élevés, et regarde ! Où n'as-tu pas été violée ? Tu t’es assise sur les chemins pour eux, comme un Arabe dans le désert, et tu as souillé la terre par tes prostitutions et par ta méchanceté.
3:3	C'est pourquoi les grosses averses ont été retenues, et il n'y a pas eu de pluie de printemps. Mais tu as eu le front d'une femme prostituée, tu n'as pas voulu avoir honte.
3:4	Maintenant, ne crieras-tu pas vers moi : Mon Père ! Tu as été l'ami de ma jeunesse !
3:5	Gardera-t-il pour toujours sa colère ? La tiendra-t-il à perpétuité<!--Es. 57:16 ; Ps. 103:9.--> ? Voici, tu as parlé et tu as fais les mauvaises choses autant que tu as pu.

### YHWH appelle Israël à la repentance

3:6	YHWH me dit aux jours du roi Yoshiyah : As-tu vu ce qu'a fait Israël, l'apostate ? Elle est allée sur toute haute colline et sous tout arbre vert, et là elle s'est prostituée.
3:7	Je me disais : Après avoir fait toutes ces choses, elle reviendra à moi. Mais elle n'est pas revenue. Et sa sœur Yéhouda, la traître, l'a vu.
3:8	J'ai vu que, quoique j'aie renvoyé Israël, l'apostate, à cause de tous ses adultères, en lui donnant sa lettre de divorce, sa sœur, Yéhouda, l'infidèle, n'a pas eu de crainte, mais elle s'en est allée et s'est aussi prostituée.
3:9	Et il est arrivé que par la légèreté de sa fornication, elle a souillé la terre, elle a commis un adultère avec la pierre et le bois.
3:10	Malgré tout cela, sa sœur Yéhouda, la traître, n'est pas revenue à moi de tout son cœur. C'est avec fausseté qu'elle l'a fait, – déclaration de YHWH.
3:11	Et YHWH me dit : Israël, l'apostate, a montré que son âme est plus juste que Yéhouda, l'infidèle.
3:12	Va, crie ces paroles vers le nord, et dis : Reviens, Israël ! l'apostate, – déclaration de YHWH. Je ne ferai pas tomber ma colère sur vous, car je suis fidèle, – déclaration de YHWH –, je ne garde pas ma colère pour toujours.
3:13	Mais reconnais ton iniquité ! Oui, tu t'es rebellée contre YHWH, ton Elohîm. Tu as dispersé tes chemins vers les étrangers sous tout arbre vert, et tu n'as pas écouté ma voix, – déclaration de YHWH.
3:14	Revenez, fils apostats, – déclaration de YHWH – car je vous ai épousés. Je vous prendrai, un d'une ville, deux d'une famille, et je vous ferai entrer dans Sion.
3:15	Je vous donnerai des bergers selon mon cœur. Ils vous feront paître avec intelligence et avec sagesse<!--Jé. 23:5.-->.
3:16	Il arrivera que quand vous serez devenus nombreux et que vous aurez porté du fruit sur la terre, en ces jours-là, – déclaration de YHWH –, on ne parlera plus de l'arche de l'alliance de YHWH. Elle ne viendra plus à la pensée, on ne s'en souviendra plus, on ne s'apercevra plus de son absence et l'on n'en fera pas une autre.
3:17	En ce temps-là, on appellera Yeroushalaim le trône de YHWH. Toutes les nations s'assembleront à Yeroushalaim, au Nom de YHWH, et elles ne marcheront plus suivant la dureté de leur mauvais cœur.
3:18	En ces jours-là, la maison de Yéhouda marchera avec la maison d'Israël. Elles viendront ensemble de la terre du nord à la terre que j'ai donnée en héritage à vos pères.
3:19	Je me disais : Comment te mettrai-je parmi les fils et te donnerai-je une terre désirable, l'héritage de la gazelle des armées des nations ? Je me disais : Tu m'appelleras : Mon Père ! Et tu ne te détourneras pas de moi.
3:20	Mais comme une femme est infidèle à son compagnon, ainsi vous m'avez été infidèles, maison d'Israël, – déclaration de YHWH.
3:21	Une voix se fait entendre sur les hauteurs dénudées : ce sont les pleurs, les supplications des fils d'Israël. Car ils ont perverti leur voie, ils ont oublié YHWH, leur Elohîm.
3:22	Revenez, fils apostats, je guérirai votre apostasie. Nous voici, nous venons à toi, car tu es YHWH, notre Elohîm.
3:23	En vérité, les collines et la multitude des montagnes n’étaient que mensonge. En vérité, c'est en YHWH, notre Elohîm, qu'est la délivrance d'Israël.
3:24	La honte a dévoré dès notre jeunesse le travail de nos pères, leurs brebis et leurs bœufs, leurs fils et leurs filles.
3:25	Nous nous couchons dans notre honte et notre ignominie nous couvre, parce que nous avons péché contre YHWH, notre Elohîm, nous et nos pères, dès notre jeunesse jusqu'à ce jour, et nous n'avons pas obéi à la voix de YHWH, notre Elohîm.

## Chapitre 4

### Prophétie sur l'invasion de la terre

4:1	Israël, si tu reviens, – déclaration de YHWH – si tu reviens à moi, si tu ôtes tes abominations de devant moi, tu ne seras plus errant çà et là.
4:2	Tu jureras : YHWH est vivant ! Avec vérité, avec droiture et avec justice. Les nations seront bénies en lui et se vanteront en lui.
4:3	Car ainsi parle YHWH aux hommes de Yéhouda et de Yeroushalaim : Labourez pour vous un sol labourable, et ne semez pas parmi les épines<!--Mt. 13:7,22 ; Mc. 4:7,18 ; Lu. 8:14.-->.
4:4	Hommes de Yéhouda, et vous habitants de Yeroushalaim, circoncisez-vous pour YHWH, ôtez les prépuces de vos cœurs<!--Ro. 2:29.-->, de peur que mon courroux ne sorte comme un feu, et qu'il ne brûle sans qu'on puisse l'éteindre, à cause de la méchanceté de vos actions.
4:5	Annoncez en Yéhouda, publiez dans Yeroushalaim, et dites : Sonnez du shofar sur la terre ! Criez à pleine voix et dites : Rassemblez-vous, et entrons dans les villes fortes !
4:6	Élevez une bannière vers Sion, mettez-vous à l’abri, ne vous arrêtez pas ! Car je fais venir du nord le malheur et une grande calamité.
4:7	Le lion<!--Lion est ici une allusion à Neboukadnetsar, roi de Babel (Babylone). Voir 2 R. 24-25 ; Da. 7:4.--> est sorti de la caverne, le destructeur des nations est en marche, il est sorti de son lieu, pour mettre ta terre en ruine. Tes villes tomberont en ruine, elles n'auront plus d'habitants.
4:8	C'est pourquoi ceignez-vous de sacs, lamentez-vous et gémissez, car l'ardeur de la colère de YHWH ne se détourne pas de nous.
4:9	Et il arrivera ce jour-là, – déclaration de YHWH –, que le cœur du roi et le cœur des chefs périront, que les prêtres seront frappés de stupeur, et que les prophètes seront stupéfaits.
4:10	C'est pourquoi je dis : Ah ! Adonaï YHWH ! Tu as vraiment trompé, trompé ce peuple et Yeroushalaim, en disant : Vous aurez la paix ! Et cependant l'épée atteint jusqu'à l'âme.
4:11	En ce temps-là on dira à ce peuple et à Yeroushalaim : Un vent éclaircissant vient des lieux élevés du désert sur le chemin de la fille de mon peuple, non pas pour vanner ni pour purifier.
4:12	C'est un vent impétueux qui vient de là jusqu'à moi, et je leur ferai maintenant leur procès.
4:13	Voici, il monte comme des nuées ! Ses chars sont comme un vent d'orage, ses chevaux sont plus légers que les aigles. Malheur à nous, car nous sommes détruits !
4:14	Yeroushalaim, lave ton cœur du mal afin que tu sois sauvée ! Jusques à quand demeureront-elles dans ton cœur tes pensées de méchanceté ?
4:15	Car une voix apporte des nouvelles de Dan, elle publie depuis la montagne d'Éphraïm le tourment.
4:16	Rappelez-le aux nations, faites-le entendre à Yeroushalaim : Des observateurs viennent d'une terre éloignée, ils poussent des cris contre les villes de Yéhouda.
4:17	Ils se sont mis tout autour d'elle comme ceux qui gardent un champ, parce qu'elle s'est rebellée contre moi, – déclaration de YHWH.
4:18	Ta conduite et tes actions t'ont produit ces choses, telle a été ta méchanceté, parce que cela a été une chose amère, certainement elle t'atteindra jusqu'au cœur.
4:19	Mes entrailles ! Mes entrailles ! Je me tords de douleur ! Les murs de mon cœur ! Mon cœur s'agite en moi ! Je ne puis me taire. Car tu entends, mon âme le son du shofar, l'alarme de la guerre.
4:20	On annonce brèche sur brèche, car toute la terre est dévastée, mes tentes sont détruites tout à coup, mes pavillons en un instant.
4:21	Jusqu'à quand verrai-je la bannière et entendrai-je le son du shofar ?
4:22	Car mon peuple est fou, il ne me connaît pas. Ce sont des enfants insensés, qui n'ont pas d'intelligence. Ils sont habiles pour faire le mal, et ils ne savent pas faire le bien.
4:23	Je regarde la Terre, et voici qu’elle est tohu et bohu<!--Voir Ge. 1:2.-->, les cieux et leur lumière ne sont plus.
4:24	Je regarde les montagnes, et voici, elles sont ébranlées, et toutes les collines sont renversées.
4:25	Je regarde, et voici, il n'y a pas d'humain et tous les oiseaux des cieux se sont enfuis.
4:26	Je regarde, et voici, le verger est un désert, et toutes ses villes sont abattues devant YHWH, devant l'ardeur de sa colère.
4:27	Car ainsi parle YHWH : Toute la terre sera dévastée, mais je ne ferai pas une entière destruction.
4:28	C'est pourquoi la terre mènera deuil et les cieux en haut seront obscurcis, parce que je l'ai dit, je l'ai résolu, et je ne m'en repentirai pas et je ne le révoquerai pas.
4:29	Toute la ville s'enfuit à cause du bruit des cavaliers et des tireurs à l'arc. Ils entrent dans les bois fourrés, et montent sur les rochers. Toute la ville est abandonnée, et aucun homme n'y habite.
4:30	Et toi, dévastée, que fais-tu ? Quoique tu te revêtes de pourpre, que tu te pares d'ornements d'or, et que tu déchires tes yeux avec du fard, tu te fais belle en vain : tes amants t'ont méprisée, c'est ton âme qu'ils cherchent.
4:31	Oui, j’entends une voix comme celle d’une femme en travail, l’angoisse comme celle d’une femme enfantant son premier-né. C'est la voix de la fille de Sion ; elle soupire, elle étend ses paumes : Malheur maintenant à moi ! Car mon âme est faible devant les meurtriers !

## Chapitre 5

### Raisons du jugement de YHWH

5:1	Parcourez les rues de Yeroushalaim, regardez, s'il vous plaît, sachez et cherchez sur les places, si vous y trouvez un homme, s’il existe quelqu’un qui pratique la justice, qui cherche la vérité, et je pardonnerai<!--Es. 59:15 ; Mi. 7:2 ; Pr. 20:6.-->.
5:2	Même quand ils disent : YHWH est vivant ! C'est faussement qu'ils jurent.
5:3	YHWH, tes yeux ne sont-ils pas sur la fidélité ? Tu les frappes, mais ils ne tremblent pas. Tu les consumes, et ils refusent de recevoir l'instruction. Ils endurcissent leurs faces plus qu'un rocher, ils refusent de se convertir.
5:4	Je me disais : Certainement ce ne sont que les pauvres, ils agissent d'une manière insensée parce qu'ils ne connaissent pas la voie de YHWH, la justice de leur Elohîm.
5:5	J’irai pour moi vers les grands et je leur parlerai, car eux, ils connaissent la voie de YHWH, la justice de leur Elohîm. Mais, eux aussi, ils ont tous ensemble brisé le joug et rompu les liens.
5:6	C'est pourquoi le lion de la forêt les tue, le loup de la région aride les détruit. Le léopard est aux aguets contre leurs villes, quiconque en sortira sera déchiré. Car leurs transgressions sont nombreuses, et leurs apostasies se sont multipliées.
5:7	Comment te pardonnerais-je en cela ? Tes fils m'ont abandonné, et ils jurent par ceux qui ne sont pas elohîm. Je les ai rassasiés, mais ils commettent l'adultère et ils se pressent en foule dans la maison de la prostituée.
5:8	Ils sont comme des chevaux bien nourris, quand ils se lèvent le matin, chacun hennit après la femme de son prochain.
5:9	Ne punirais-je pas ces choses-là ? – déclaration de YHWH. Mon âme ne se vengerait-elle pas d'une telle nation ?
5:10	Montez sur ses murailles et détruisez-les, mais ne les achevez pas entièrement ! Ôtez ses rameaux car ils ne sont pas à YHWH<!--Jn. 15:5.--> !
5:11	Car la maison d'Israël et la maison de Yéhouda m'ont été infidèles, – déclaration de YHWH.
5:12	Ils renient YHWH, ils disent : Cela n'arrivera pas, et le malheur ne viendra pas sur nous, nous ne verrons ni l'épée ni la famine.
5:13	Et les prophètes sont légers comme le vent, et la parole n'est pas en eux. Qu'il leur soit fait ainsi !
5:14	C'est pourquoi ainsi parle YHWH, Elohîm Tsevaot : Parce que vous avez prononcé cette parole-là, voici, je vais mettre mes paroles dans ta bouche pour y être comme un feu, et ce peuple sera comme le bois, et ce feu les consumera.
5:15	Maison d'Israël, voici, je fais venir contre vous une nation d'une terre éloignée<!--Il s'agit de Babel (Babylone). Voir 2 R. 24-25.-->, – déclaration de YHWH –, une nation puissante, une nation ancienne, une nation dont tu ne connais pas la langue, et dont tu ne comprendras pas ce qu'elle dira.
5:16	Son carquois est comme un sépulcre ouvert, et ils sont tous des hommes vaillants.
5:17	Elle dévorera ta moisson et ton pain, elle dévorera tes fils et tes filles, elle dévorera tes brebis et tes bœufs, elle dévorera ta vigne et de ton figuier, elle abattra par l'épée tes villes fortifiées dans lesquelles tu te confies.
5:18	Toutefois en ces jours-là, – déclaration de YHWH –, je ne vous anéantirai pas complètement.
5:19	Et il arrivera que vous direz : Pourquoi YHWH, notre Elohîm, nous a-t-il fait toutes ces choses ? Tu leur diras ainsi : Comme vous m'avez abandonné et que vous avez servi les elohîm étrangers sur votre terre, ainsi vous servirez des étrangers sur une terre qui n'est pas la vôtre.
5:20	Annoncez ceci dans la maison de Yaacov, et publiez-le dans Yéhouda, en disant :
5:21	Écoutez maintenant ceci, peuple insensé et sans cœur ! vous avez des yeux mais vous ne voyez pas, vous avez des oreilles, mais vous n'entendez pas<!--Ez. 12:2 ; Jn. 12:40.--> !
5:22	Ne me craindrez-vous pas ? – déclaration de YHWH. Ne tremblerez-vous pas en face de moi ? C'est moi qui ai mis le sable pour limite à la mer par un décret perpétuel, elle ne va pas au-delà. Ses vagues s'agitent, mais elles sont impuissantes. Elles grondent, mais elles ne vont pas au-delà<!--Pr. 8:29 ; Job 38:8.-->.
5:23	Mais ce peuple a un cœur désobéissant et rebelle. Ils se détournent et s'en vont.
5:24	Et ils ne disent pas dans leur cœur : Craignons maintenant YHWH, notre Elohîm, qui donne la pluie en son temps, la pluie d'automne<!--Voir Za. 10:1.--> et la pluie de printemps, qui nous garde les semaines ordonnées pour la moisson.
5:25	Vos iniquités ont détourné ces choses, vos péchés ont retenu ces bonnes choses loin de vous.
5:26	Car il se trouve parmi mon peuple des méchants : ils épient comme l'oiseleur qui dresse des pièges, ils tendent des filets et prennent des hommes<!--Ps. 91:3, 124:7.-->.
5:27	Comme la cage est remplie d'oiseaux, ainsi leurs maisons sont remplies de fraude. C'est par ce moyen qu'ils deviennent grands et riches.
5:28	Ils sont devenus gras et luisants. Ils débordent de choses mauvaises. Ils ne jugent pas la cause, la cause de l'orphelin, et ils prospèrent ; ils ne font pas droit aux pauvres.
5:29	Ne punirais-je pas ces choses-là ? – déclaration de YHWH. Mon âme ne se vengerait-elle pas d'une telle nation ?
5:30	Il est arrivé sur la terre une chose étonnante et horrible :
5:31	les prophètes prophétisent le mensonge, et les prêtres dominent par leur main, et mon peuple prend plaisir à cela. Que ferez-vous quand elle prendra fin ?


## Chapitre 6

### Yeroushalaim (Jérusalem) dans la confusion

6:1	Fils de Benyamin, mettez-vous à l’abri, hors du sein de Yeroushalaim, sonnez du shofar à Tekoa, et élevez un signal de feu à Beth-Hakkérem ! Car on voit venir du nord un malheur et une grande ruine.
6:2	La belle et délicate fille de Sion, je la détruis !
6:3	Les bergers avec leurs troupeaux viennent contre elle. Ils plantent leurs tentes autour d'elle, et chaque homme y fera paître ceux qui seront sous sa main.
6:4	Préparez le combat contre elle ! Levez-vous et montons en plein midi !... Malheur à nous, car le jour décline, car les ombres du soir s’inclinent.
6:5	Levez-vous ! Montons de nuit, et ruinons ses palais !
6:6	Car ainsi parle YHWH Tsevaot : Coupez des arbres, élevez des tertres contre Yeroushalaim ! C'est la ville qui doit être visitée. Tout est oppression au milieu d'elle.
6:7	Comme un puits fait jaillir ses eaux, ainsi elle fait jaillir sa méchanceté. Devant moi, on n'entend continuellement en elle que violence et ruine, avec des maladies et des plaies.
6:8	Yeroushalaim, reçois l'instruction, de peur que mon âme ne se détache<!--Vient du mot hébreu qui signifie aussi « se disloquer ».--> de toi, de peur que je ne fasse de toi un désert, une terre inhabitée !
6:9	Ainsi parle YHWH Tsevaot : On grappillera, on grappillera comme une vigne les restes d'Israël. Remets ta main dans les paniers, comme un vendangeur.
6:10	À qui parlerai-je, qui prendrai-je à témoin, pour qu'ils écoutent ? Voici, leur oreille est incirconcise, et ils ne peuvent entendre. Voici, la parole de YHWH est devenue pour eux une insulte, ils n'y prennent pas de plaisir.
6:11	Je suis plein de courroux de YHWH, je me lasse de le contenir. Répands-le sur l'enfant dans la rue, sur les assemblées des jeunes hommes ! Oui, même l’homme, même la femme, seront pris, le vieillard avec celui qui est plein de jours.
6:12	Leurs maisons retourneront à d'autres, les champs et les femmes aussi, quand j'étendrai ma main sur les habitants de la terre, – déclaration de YHWH.
6:13	Oui, du petit jusqu’au grand d’entre eux, tous sont avides de profit, du prophète jusqu’au prêtre, tous pratiquent le mensonge.
6:14	Ils guérissent à la légère la fracture de la fille de mon peuple, en disant : Paix ! Paix ! Mais il n'y a pas de paix<!--1 Th. 5:3.-->.
6:15	Sont-ils confus d'avoir commis des abominations ? Ils n'en ont même aucune honte, et ils ne savent pas ce que c'est que de rougir. C'est pourquoi ils tomberont parmi ceux qui tombent, ils seront renversés au temps où je les visiterai, dit YHWH.
6:16	Ainsi parle YHWH : Tenez-vous sur les chemins et voyez ! Demandez aux sentiers éternels : « Où est le bon chemin ? » Marchez-y, et vous trouverez le repos de vos âmes ! Mais ils disent : Nous n'y marcherons pas !
6:17	J'ai aussi établi sur vous des sentinelles<!--Es. 21:6 ; Ez. 33:1-19.--> : Soyez attentifs au son du shofar ! Mais ils disent : Nous n'y serons pas attentifs.
6:18	Vous, nations, écoutez, et toi assemblée, sache ce qui se passe chez eux !
6:19	Écoute, Terre ! Voici, je fais venir un mal sur ce peuple, c'est le fruit de leurs pensées. Car ils n'ont pas été attentifs à mes paroles, et qu'ils ont rejeté ma torah.
6:20	Que me fait cet encens venu de Séba, le précieux roseau aromatique d’une terre lointaine ? Vos holocaustes ne me plaisent pas, et vos sacrifices ne me sont pas agréables.
6:21	C'est pourquoi ainsi parle YHWH : Voici, je mettrai devant ce peuple des pierres d'achoppement, auxquelles les pères et les fils, le voisin et son compagnon, se heurteront ensemble, et ils périront.
6:22	Ainsi parle YHWH : Voici, un peuple vient de la terre du nord, une grande nation se réveille des extrémités de la Terre.
6:23	Ils prennent l'arc et le javelot, ils sont cruels et n'ont pas de pitié. Leur voix gronde comme la mer, ils sont montés sur des chevaux, ils sont rangés comme un seul homme en bataille contre toi, fille de Sion !
6:24	Nous en entendons le bruit, nos mains en deviennent lâches, l'angoisse nous saisit, une douleur comme celle d'une femme qui enfante.
6:25	Ne sortez pas dans les champs, n'allez pas par les chemins, car l'épée de l'ennemi et la terreur sont partout.
6:26	Fille de mon peuple, ceins-toi d'un sac et roule-toi dans la cendre ! Fais-toi un deuil pour un fils unique, un gémissement d'amertume ! Car le dévastateur vient subitement sur nous.
6:27	Je fais de toi un essayeur<!--Un inspecteur qui évalue les métaux.--> parmi mon peuple, une forteresse, pour que tu connaisses et éprouves leur voie.
6:28	Ce sont tous des rebelles qui se sont détournés du chemin. Ils marchent dans la calomnie, ils sont du cuivre et du fer, ils sont tous corrompus.
6:29	Le soufflet brûle, le plomb est consumé par le feu. On épure, on épure en vain : les méchants ne se détachent pas.
6:30	On les appelle de l'argent réprouvé, car YHWH les a réprouvés.

## Chapitre 7

### Hypocrisie de Yéhouda

7:1	La parole apparut à Yirmeyah de la part de YHWH en disant :
7:2	Tiens-toi debout à la porte de la maison de YHWH, et là, crie cette parole, et dis : Écoutez la parole de YHWH, vous tous, hommes de Yéhouda, qui entrez par ces portes, pour vous prosterner devant YHWH !
7:3	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Rendez bonnes vos voies et vos actions, et je vous ferai habiter en ce lieu.
7:4	Ne vous confiez pas en des paroles trompeuses, en disant : C'est ici le temple de YHWH, le temple de YHWH, le temple de YHWH !
7:5	Mais si vous rendez bonnes, si vous rendez bonnes vos voies et vos actions, si vous pratiquez, si vous pratiquez la justice envers un homme qui plaide contre son prochain,
7:6	si vous n'opprimez pas l'étranger, l'orphelin et la veuve, si vous ne versez pas en ce lieu le sang innocent, si vous n'allez pas après d'autres elohîm pour votre malheur,
7:7	alors je vous laisserai demeurer en ce lieu, sur la terre que j'ai donnée à vos pères depuis toujours et pour toujours.
7:8	Voici, vous mettez votre confiance dans des paroles mensongères qui ne servent à rien.
7:9	Vous volez, vous assassinez, vous commettez l'adultère, vous jurez faussement, vous offrez de l'encens à Baal, vous allez après les elohîm étrangers que vous ne connaissez pas,
7:10	ensuite vous venez vous tenir debout en face de moi, dans cette maison sur laquelle mon Nom est invoqué, et vous dites : Nous sommes délivrés ! Et c'est pour accomplir toutes ces abominations !
7:11	N'est-elle plus à vos yeux qu'une caverne de voleurs<!--Mt. 21:13 ; Mc. 11:17 ; Lu. 19:46.-->, cette maison sur laquelle mon Nom est invoqué ? Et voici, moi-même je le vois, – déclaration de YHWH.
7:12	Mais allez maintenant à mon lieu qui était à Shiyloh, où j'avais fait demeurer mon Nom au commencement. Et regardez ce que je lui ai fait, à cause de la méchanceté de mon peuple d'Israël.
7:13	Maintenant, puisque vous avez fait toutes ces actions, – déclaration de YHWH –, puisque je vous ai parlé, me levant de bonne heure et parlant, et que vous n'avez pas écouté, puisque je vous ai appelés et que vous n'avez pas répondu,
7:14	je traiterai cette maison sur laquelle mon Nom est invoqué et sur laquelle vous vous confiez, ce lieu que j'ai donné à vous et à vos pères, comme j'ai traité Shiyloh :
7:15	et je vous chasserai loin de mes faces, comme j'ai chassé tous vos frères, avec toute la postérité d'Éphraïm.
7:16	Toi ne prie pas pour ce peuple, n'élève pour eux ni cri ni prière. Ne fais pas une intercession auprès de moi<!--Ez. 3:26-27.-->, car je ne t'écouterai pas.
7:17	Ne vois-tu pas ce qu'ils font dans les villes de Yéhouda et dans les rues de Yeroushalaim ?
7:18	Les fils ramassent le bois, et les pères allument le feu, et les femmes pétrissent la pâte pour faire des gâteaux à la reine des cieux<!--La reine des cieux est une déesse qui change de nom en fonction des pays. Asherah, Astarté, Isis, Junon, Cybèle, Artémis (Diane) ou encore la vierge Marie (Myriam), proclamée mère d'Elohîm en 431 au concile d'Éphèse. Voir De. 16:2-3.-->, et pour faire des libations aux elohîm étrangers, afin de m'irriter.
7:19	Est-ce moi qu'ils irritent ? – déclaration de YHWH. N'est-ce pas eux-mêmes, à la confusion de leurs propres faces ?
7:20	C'est pourquoi ainsi parle Adonaï YHWH : Voici, ma colère et mon courroux se répandent sur ce lieu, sur les hommes et sur les bêtes, sur les arbres des champs et sur le fruit du sol. Ma colère brûlera et ne s'éteindra pas.
7:21	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Ajoutez vos holocaustes à vos sacrifices, et mangez-en la chair !
7:22	Car je n'ai pas parlé avec vos pères et je ne leur ai pas donné d'ordre au sujet des holocaustes et des sacrifices, le jour où je les ai fait sortir de la terre d'Égypte.
7:23	Mais voici la parole que je leur ai ordonnée, en disant : Écoutez ma voix et je serai votre Elohîm, et vous serez mon peuple, marchez dans toutes les voies que je vous ordonne, afin que vous soyez heureux<!--Ex. 15:26.--> !
7:24	Mais ils n'ont pas écouté, et n'ont pas prêté l'oreille. Mais ils ont marché dans les conseils et la dureté de leur cœur mauvais. Ils se sont éloignés et ne sont pas revenus à moi.
7:25	Depuis le jour où vos pères sont sortis de la terre d'Égypte, jusqu'à ce jour, je vous ai envoyé tous mes serviteurs les prophètes. Je les ai envoyés chaque jour, de bonne heure.
7:26	Mais ils ne m'ont pas écouté, et ils n'ont pas prêté l'oreille. Mais ils ont raidi leur cou, ils ont fait le mal plus que leurs pères.
7:27	Tu leur diras toutes ces paroles, mais ils ne t'écouteront pas. Tu les appelleras, mais ils ne te répondront pas.
7:28	C'est pourquoi tu leur diras : Voici la nation qui n'écoute pas la voix de YHWH, son Elohîm, et qui n'accepte pas la correction. La vérité a disparu<!--Es. 59:15.-->, elle a été retranchée de leur bouche.
7:29	Coupe ta chevelure de nazir et jette-la et, sur les hauteurs dénudées, élève un chant funèbre ! Car YHWH rejette et abandonne la génération qui a provoqué sa fureur.
7:30	Car les fils de Yéhouda ont fait ce qui est mal à mes yeux, – déclaration de YHWH. Ils ont mis leurs abominations dans cette maison sur laquelle mon Nom est invoqué, afin de la souiller.
7:31	Ils ont bâti des hauts lieux de Topheth, dans la vallée du fils de Hinnom<!--Voir commentaire en Ap. 16:16.-->, pour brûler au feu leurs fils et leurs filles<!--Lé. 18:21. Voir commentaire en Lé. 20:2.-->, ce que je n'avais pas ordonné, et à quoi je n'ai jamais pensé.
7:32	C'est pourquoi voici, les jours viennent, – déclaration de YHWH –, qu'elle ne sera plus appelée Topheth, ni la vallée de Ben-Hinnom, mais la vallée de la tuerie. On enterrera alors à Topheth, faute de place.
7:33	Et les cadavres de ce peuple deviendront une nourriture pour les oiseaux des cieux et pour les bêtes de la Terre, sans qu'il n'y ait personne qui les effraye.
7:34	Je ferai aussi cesser dans les villes de Yéhouda et dans les rues de Yeroushalaim les cris de joie et les cris d'allégresse, la voix de l'époux et la voix de l'épouse, car la terre ne deviendra qu’une désolation.

## Chapitre 8

### Yéhouda dans l'égarement

8:1	En ce temps-là, – déclaration de YHWH –, on sortira les os des rois de Yéhouda, et les os de ses chefs, les os des prêtres, et les os des prophètes, et les os des habitants de Yeroushalaim, hors de leurs sépulcres.
8:2	Et on les étendra devant le soleil, devant la lune et devant toute l'armée des cieux, qu'ils ont aimés, qu'ils ont servis et après lesquels ils ont marché, qu'ils ont cherchés et devant lesquels ils se sont prosternés. Ils ne seront pas recueillis ni enterrés, ils seront comme du fumier sur les faces du sol.
8:3	La mort sera choisie plutôt que la vie pour tout le résidu de ceux qui resteront de cette race méchante, dans tous les lieux où j'aurai bannis ceux qui resteront, – déclaration de YHWH Tsevaot.
8:4	Dis-leur : Ainsi parle YHWH : Si l'on tombe, ne se relève-t-on pas ? Celui qui se détourne, ne revient-il pas ?
8:5	Pourquoi ce peuple de Yeroushalaim s'abandonne-t-il à de perpétuelles apostasies ? Ils se fortifient dans la tromperie, et ils refusent de revenir.
8:6	Je suis attentif et j'écoute, mais personne ne parle selon la justice. Il n'y a personne qui se repente de sa méchanceté, disant : Qu'ai-je fait ? Ils retournent tous vers les objets qui les entraînent, comme un cheval qui se précipite à la bataille.
8:7	Même la cigogne connaît dans les cieux sa saison. La tourterelle, l'hirondelle et la grue observent le temps où elles doivent venir. Mais mon peuple ne connaît pas le jugement<!--Ou « temps du jugement ».--> de YHWH.
8:8	Comment dites-vous : Nous sommes sages et la torah de YHWH est avec nous ! Voici, la plume de mensonge des scribes s'est mise à l'œuvre pour le mensonge.
8:9	Les sages sont dans la honte, ils sont épouvantés et capturés. Car ils ont rejeté la parole de YHWH, et quelle sagesse ont-ils ?
8:10	C'est pourquoi je donnerai leurs femmes à d'autres, leurs champs à ceux qui les déposséderont. Oui, du petit jusqu'au grand, tous sont avides de profit, du prophète jusqu’au prêtre, tous pratiquent le mensonge.
8:11	Ils guérissent la plaie de la fille de mon peuple avec légèreté, en disant : Paix ! Paix ! Et il n'y a pas de paix.
8:12	Ont-ils eu honte d'avoir commis des abominations ? Ils n'en ont même aucune honte, et ils ne savent pas ce que c'est que de rougir. C'est pourquoi ils tomberont parmi ceux qui tombent, ils seront renversés au temps où je les visiterai, dit YHWH.
8:13	Je les ramasserai, j'en finirai avec eux, – déclaration de YHWH. Il n'y aura plus de raisins à la vigne, et il n'y aura plus de figues au figuier, les feuilles se flétriront. Ce que je leur avais donné sera transporté avec eux.
8:14	Pourquoi restons-nous assis ? Rassemblez-vous et entrons dans les villes fortes, et nous serons là en repos ! Car YHWH, notre Elohîm, nous réduit au silence, et il nous fait boire des eaux empoisonnées, parce que nous avons péché contre YHWH.
8:15	Nous espérions la paix : rien de bon ! le temps de la guérison : voici la terreur !
8:16	Le hennissement de ses chevaux se fait entendre de Dan, et toute la terre tremble au bruit des hennissements de ses puissants chevaux. Ils viennent et dévorent la terre et ce qu'elle contient, la ville et ceux qui l'habitent.
8:17	Car voici, j'envoie contre vous des serpents, des vipères, contre lesquels il n'existe pas de charme, et ils vous mordront, – déclaration de YHWH.
8:18	Je voudrais prendre des forces pour soutenir la douleur, mais mon cœur est languissant au dedans de moi.
8:19	Voici la voix de la fille de mon peuple, qui crie d'une terre éloignée : YHWH n'est-il plus à Sion ? Son Roi n'est-il plus au milieu d'elle ? Pourquoi m'ont-ils irrité par leurs images gravées, par les vanités<!--Idoles qu'Elohîm appelle vanité, vapeur ou souffle.--> étrangères ?
8:20	La moisson est passée, l'été est fini, et nous ne sommes pas sauvés !
8:21	Je suis brisé à cause de la ruine de la fille de mon peuple, je suis sombre<!--Pleurer, être sombre.-->, l'épouvante m'a saisi.
8:22	N'y a-t-il pas de baume en Galaad ? N'y a-t-il pas là de médecin ? Pourquoi la guérison de la fille de mon peuple ne s'opère-t-elle pas ?
8:23	Qui donnera de l'eau à ma tête, et à mes yeux une fontaine de larmes ? Je pleurerai jour et nuit les blessés mortellement de la fille de mon peuple.

## Chapitre 9

### Yirmeyah pleure sur son peuple

9:1	Qui me donnera au désert un abri de voyageurs ? J'abandonnerais mon peuple, je m'en irais loin de lui ! Car ils sont tous des adultères, et une assemblée de traîtres.
9:2	Ils tendent leur langue, leur arc, dans le mensonge<!--Ps. 64:3-4.--> ! Ce n'est pas par la vérité qu'ils sont puissants sur la terre. Car ils vont de mal en mal et ils ne me connaissent pas, – déclaration de YHWH.
9:3	Que chaque homme se garde de son ami ! Ne faites pas confiance à vos frères<!--Mi. 7:5.--> ! Car tout frère supplante, il supplante et tout ami marche dans la calomnie.
9:4	Chaque homme se moque de son ami, et on ne parle pas selon la vérité. Ils exercent leur langue à proférer le mensonge, ils se fatiguent pour faire le mal.
9:5	Ta demeure est au milieu de la tromperie. C'est par tromperie qu'ils refusent de me connaître, – déclaration de YHWH.
9:6	C'est pourquoi, ainsi parle YHWH Tsevaot : Voici, je vais les fondre, je les éprouverai<!--Mal. 3:3.-->. Car comment agir autrement à l'égard de la fille de mon peuple ?
9:7	Leur langue est une flèche qui tue, elle profère des tromperies. Chacun de sa bouche parle de la paix avec son ami, mais au-dedans il lui dresse des embûches<!--Ps. 12:3, 28:3.-->.
9:8	Ne les punirais-je pas pour ces choses-là ? – déclaration de YHWH. Mon âme ne se vengerait-elle pas d'une telle nation ?
9:9	Sur les montagnes j'élèverai des pleurs et des chants de deuil, et sur les pâturages du désert des chants funèbres, parce qu'ils sont brûlés, plus personne n'y passe, on n'y entend plus la voix des troupeaux. Les oiseaux des cieux et le bétail se sont enfuis, ils s'en sont allés.
9:10	Je ferai de Yeroushalaim des monceaux de ruines, elle sera un repaire de dragons, et je ferai des villes de Yéhouda un désert sans habitants.
9:11	Qui est l'homme sage qui comprend ces choses ? Qui est celui à qui la bouche de YHWH a parlé ? Qu'il le déclare ! Pourquoi la terre est-elle détruite, brûlée comme un désert, sans que personne y passe ?
9:12	YHWH dit : C'est parce qu'ils ont abandonné ma torah que j'avais mise devant eux, parce qu'ils n'ont pas écouté ma voix et qu'ils n'ont pas marché selon elle,
9:13	et qu'ils ont marché après la dureté de leur cœur et après les Baalim, comme leurs pères le leur ont enseigné.
9:14	C'est pourquoi, ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, je vais nourrir ce peuple d'absinthe, et je leur ferai boire des eaux empoisonnées.
9:15	Je les disperserai parmi les nations que n'ont connues ni eux ni leurs pères, et j'enverrai après eux l'épée, jusqu'à ce que je les aie exterminés.
9:16	Ainsi parle YHWH Tsevaot : Discernez ! Appelez les pleureuses<!--Vient de l'hébreu « koon » qui signifie « chanter un chant funèbre », « chanter », « gémir », « se lamenter ».--> ! Qu'elles viennent ! Envoyez chercher les femmes sages ! Qu'elles viennent !
9:17	Qu'elles se hâtent ! Qu'elles élèvent une lamentation sur nous ! Que les larmes tombent de nos yeux ! Que l'eau coule de nos paupières !
9:18	Car une voix de lamentation se fait entendre de Sion : Eh quoi ! Nous sommes dévastés ! Nous sommes couverts de honte parce que nous avons abandonné la terre et que nos tabernacles nous ont jetés dehors !
9:19	C'est pourquoi, vous femmes, écoutez la parole de YHWH ! Que votre oreille reçoive la parole de sa bouche ! Enseignez à vos filles les chants de deuil, la femme à sa voisine les chants funèbres !
9:20	Car la mort est montée par nos fenêtres, elle est entrée dans nos palais, pour exterminer les enfants dans les rues, et les jeunes hommes sur les places.
9:21	Parle ! Voici la déclaration de YHWH : Les cadavres des humains tomberont comme du fumier sur les faces des champs, comme une gerbe derrière le moissonneur que personne ne ramasse !
9:22	Ainsi parle YHWH : Que le sage ne se glorifie pas de sa sagesse ! Que l’homme fort ne se glorifie pas de sa force ! Que le riche ne se glorifie pas de sa richesse !
9:23	Mais que celui qui se glorifie se glorifie en ceci<!--Jé. 4:2 ; 1 Ch. 16:10.--> : d'avoir de l'intelligence et de me connaître, car je suis YHWH qui exerce la miséricorde, le jugement et la justice sur la Terre, car c'est en ces choses que je prends plaisir, – déclaration de YHWH<!--Ps. 62:10 ; 1 Co. 1:31 ; 2 Co. 10:17 ; 1 Ti. 6:17.-->.
9:24	Voici, les jours viennent, – déclaration de YHWH –, où je punirai tout circoncis ayant le prépuce,
9:25	l'Égypte, Yéhouda, Édom, les fils d'Ammon, Moab, et tous ceux qui se coupent les coins de leur barbe, et qui habitent dans le désert, car toutes les nations sont incirconcises, et toute la maison d'Israël a le cœur incirconcis.

## Chapitre 10

### Dénonciation de l'idolâtrie en Israël

10:1	Écoutez la parole que YHWH vous déclare, maison d'Israël !
10:2	Ainsi parle YHWH : N'apprenez pas les habitudes des nations<!--Le mot hébreu signifie aussi « voie », « chemin », « manière ». Lé. 18:3 ; De. 12:30.-->, et ne craignez pas les signes des cieux, parce que les nations les craignent.
10:3	Car les ordonnances des peuples sont une vanité. On coupe le bois dans la forêt : la main de l'ouvrier le travaille avec la hache<!--Es. 40:20, 44:12-18.-->,
10:4	on l'embellit avec de l'argent et de l'or, on le fait tenir avec des clous et à coups de marteau, afin qu'il ne vacille pas.
10:5	Ils sont comme un palmier, un ouvrage martelé : ils ne parlent pas. On les porte, on les porte, car ils ne marchent pas ! N'ayez pas peur d'eux, car ils ne sauraient faire aucun mal, et aussi ils sont incapables de faire du bien.
10:6	Personne n'est semblable à toi, YHWH ! Tu es grand, et ton Nom est grand par ta puissance.
10:7	Qui ne te craindrait, Roi des nations ? C'est bien cela qui te convient ! Car parmi tous les sages des nations et dans tous leurs royaumes, personne n'est semblable à toi<!--Ap. 15:4.-->.
10:8	Et ils sont tous ensemble stupides et insensés. Leur discipline n'est que vanité, c'est du bois<!--Ha. 2:18.--> !
10:9	On fait venir de Tarsis de l'argent battu, et de l'or d'Ouphaz. L'artisan et la main de l'orfèvre les travaillent. Les étoffes teintes en violet et en pourpre rouge sont leurs vêtements. Toutes ces choses sont l'ouvrage de sages.
10:10	Mais YHWH est l'Elohîm de vérité. C'est Elohîm le Vivant et le Roi éternel. La Terre tremble devant sa colère, et les nations ne supportent pas sa fureur.
10:11	Vous leur parlerez ainsi : Les élahhs qui n'ont pas fait les cieux et la Terre périront de la Terre et de dessous les cieux.
10:12	Il est celui qui a fait la Terre par sa puissance, celui qui a fondé le monde par sa sagesse, et celui qui, par son intelligence, a étendu les cieux.
10:13	De la voix, il donne le tumulte des eaux dans les cieux. Il fait monter les vapeurs des extrémités de la Terre, il fait les éclairs et la pluie, et il fait sortir le vent de ses réservoirs.
10:14	Tout homme devient stupide par sa connaissance. Tout orfèvre est honteux de ses idoles, car les idoles en métal fondu ne sont que mensonge, il n'y a pas de souffle en elles.
10:15	Elles ne sont que vanité, une œuvre de tromperie. Elles périront au temps de leur châtiment.
10:16	Celui qui est la portion de Yaacov n'est pas comme ces choses-là, car c'est lui qui a tout formé, et Israël est la tribu de son héritage. Son Nom est YHWH Tsevaot.
10:17	Toi qui es assise dans la détresse, emporte de la terre tes paquets !
10:18	Car ainsi parle YHWH : Voici, cette fois je vais lancer comme avec une fronde les habitants de la terre. Je vais les mettre à l'étroit afin qu'on les atteigne.
10:19	Malheur à moi, à cause de ma ruine ! Ma plaie est douloureuse ! Et je me disais : Oui, c’est là ma maladie, et je la supporterai.
10:20	Ma tente est dévastée, tous mes cordages sont rompus. Mes fils m'ont quittée, ils ne sont plus là. Il n'y a plus personne qui dresse ma tente, qui relève mes pavillons.
10:21	Car les bergers ont été stupides : ils n'ont pas cherché YHWH. C'est pour cela qu'ils n'ont pas réussi, et que tous leurs troupeaux s'éparpillent.
10:22	Un bruit, une rumeur ! Voici qu'elle vient, c'est un grand tremblement qui vient de la terre du nord pour faire des villes de Yéhouda un désert, un repaire de dragons.
10:23	YHWH ! Je sais que la voie de l'être humain ne dépend pas de lui<!--Pr. 16:1.-->, et qu'il n'est pas au pouvoir de l'homme qui marche de diriger ses pas.
10:24	YHWH ! Châtie-moi, mais avec mesure, non dans ta colère, de peur que tu ne me réduises à rien<!--Es. 27:8 ; Ps. 38:2.-->.
10:25	Répands ton courroux sur les nations qui ne te connaissent pas, et sur les familles qui n'invoquent pas ton Nom ! Car ils dévorent Yaacov, ils le dévorent et le consument, et ils mettent en désolation son agréable demeure.

## Chapitre 11

### YHWH dénonce la prostitution de Yéhouda

11:1	La parole apparut à Yirmeyah de la part de YHWH en disant :
11:2	Écoutez les paroles de cette alliance, et parlez aux hommes de Yéhouda et aux habitants de Yeroushalaim !
11:3	Dis-leur : Ainsi parle YHWH, l'Elohîm d'Israël : Maudit soit l'homme qui n'écoute pas les paroles de cette alliance<!--De. 27:26 ; Ga. 3:10.-->,
11:4	que j'ai ordonnée à vos pères, le jour où je les ai fait sortir de la terre d'Égypte, de la fournaise de fer, en disant : Écoutez ma voix et faites toutes les choses que je vous ordonnerai, alors vous serez mon peuple et je serai votre Elohîm<!--Lé. 26:12 ; De. 4:20.-->,
11:5	afin que j'accomplisse le serment que j'ai juré à vos pères, de leur donner une terre où coulent le lait et le miel, comme en ce jour. Et je répondis et dis : Amen ! YHWH !
11:6	YHWH me dit : Crie toutes ces paroles dans les villes de Yéhouda et dans les rues de Yeroushalaim, en disant : Écoutez les paroles de cette alliance et observez-les !
11:7	Car j'ai averti, j'ai averti vos pères, depuis le jour où je les ai fait monter de la terre d'Égypte jusqu'à ce jour, je les ai avertis dès le matin, en disant : Écoutez ma voix !
11:8	Mais ils n'ont pas écouté, ils n'ont pas prêté l'oreille, ils ont marché chacun suivant la dureté de leur mauvais cœur. C'est pourquoi j'ai fait venir sur eux toutes les paroles de cette alliance, que je leur avais ordonné d'observer et qu'ils n'ont pas observée.
11:9	YHWH me dit : Il y a une conspiration entre les hommes de Yéhouda et entre les habitants de Yeroushalaim.
11:10	Ils sont retournés aux iniquités de leurs premiers pères, qui ont refusé d'écouter mes paroles, et ils sont allés après d'autres elohîm pour les servir. La maison d'Israël et la maison de Yéhouda ont rompu mon alliance, que j'avais faite avec leurs pères.
11:11	C'est pourquoi ainsi parle YHWH : Voici, je fais venir sur eux un mal dont ils ne pourront sortir. Ils crieront vers moi, et je ne les écouterai pas<!--Es. 1:15 ; Ez. 8:18 ; Mi. 3:4 ; Pr. 1:28.-->.
11:12	Et les villes de Yéhouda et les habitants de Yeroushalaim s'en iront et crieront vers les elohîm auxquels ils brûlent de l'encens, mais ces elohîm-là ne les sauveront pas, ils ne les sauveront pas au temps de leur malheur.
11:13	Car tes elohîm sont devenus aussi nombreux que tes villes, Yéhouda ! Et les autels que vous avez dressés aux choses honteuses, des autels pour brûler de l'encens à Baal<!--Ez. 16:24-31 ; Ac. 17:23.--> sont aussi nombreux que tes rues, Yeroushalaim !
11:14	Toi, n'intercède pas pour ce peuple. N'élève pour eux ni cri ni prière, car je ne les écouterai pas au temps où ils crieront vers moi dans leur malheur.
11:15	Qu’est-ce que ma bien-aimée a à faire dans ma maison ? Elle prépare beaucoup de complots ! La chair sainte sera transportée loin de toi, car c'est quand tu fais le mal que tu triomphes !
11:16	YHWH avait appelé ton nom Olivier verdoyant, beau par la forme de ton fruit. À la voix d'un grand rugissement, il y met le feu et ses rameaux sont brisés.
11:17	YHWH Tsevaot, qui t'a plantée, prononce le mal contre toi, à cause de la méchanceté de la maison d'Israël et de la maison de Yéhouda, qui ont agi pour m'irriter, en brûlant de l'encens à Baal.

### Jugement des ennemis de Yirmeyah

11:18	Et YHWH me l'a fait savoir, et je l'ai su. Alors tu m'as fait voir leurs actions.
11:19	Moi, j’étais comme un agneau, un animal apprivoisé, mené à la boucherie et je ne savais pas qu'ils projetaient de mauvais desseins contre moi : Détruisons l'arbre avec son fruit ! Exterminons-le de la Terre des vivants, et qu'on ne se souvienne plus de son nom !
11:20	Mais toi, YHWH Tsevaot, qui juges avec justice, et qui éprouves les reins et le cœur, fais que je voie ta vengeance s'exercer contre eux, car je t'ai révélé ma cause<!--1 S. 16:7 ; Ps. 26:2 ; 1 Ch. 28:9 ; Ap. 2:23.-->.
11:21	C'est pourquoi ainsi parle YHWH contre les hommes d'Anathoth, qui cherchent ton âme et qui disent : Ne prophétise plus au Nom de YHWH, et tu ne mourras pas par nos mains<!--Es. 30:10 ; Mi. 2:6.--> !
11:22	C'est pourquoi ainsi parle YHWH Tsevaot : Voici, je vais les punir. Les jeunes hommes mourront par l'épée, leurs fils et leurs filles mourront par la famine.
11:23	Et il ne restera rien d'eux, car je ferai venir le mal sur les hommes d'Anathoth, l'année de leur châtiment.

## Chapitre 12

### Prière de Yirmeyah et réponse de YHWH

12:1	Toi, YHWH, tu es juste quand je conteste avec toi ! Cependant je parlerai de jugements avec toi : Pourquoi la voie des méchants est-elle prospère ? Pourquoi tous les perfides sont-ils tranquilles<!--Job 21:7-9 ; Ro. 3:4.--> ?
12:2	Tu les as plantés, ils ont pris racine. Ils avancent, ils portent du fruit. Tu es près de leurs bouches, mais loin de leurs cœurs<!--Es. 29:13 ; Job 21:7-8.-->.
12:3	Mais toi, YHWH, tu me connais, tu me vois, tu éprouves mon cœur qui est avec toi. Traîne-les comme des brebis pour l’abattage, consacre-les pour le jour de la tuerie !
12:4	Jusqu'à quand la terre sera-t-elle en deuil, et l'herbe de tous les champs séchera-t-elle à cause de la méchanceté de ceux qui y habitent ? Les bêtes et les oiseaux sont consumés. Ils disent : On ne verra pas notre fin.
12:5	Si tu cours avec des piétons et qu'ils te fatiguent, comment rivaliseras-tu avec des chevaux ? Et s'il te faut une terre de paix pour avoir confiance, que feras-tu devant l'orgueil du Yarden ?
12:6	Car tes frères eux-mêmes aussi et la maison de ton père te trahissent, ils crient eux-mêmes après toi à plein gosier. Ne les crois pas quand ils te disent de bonnes choses<!--Pr. 26:25.-->.
12:7	J'ai abandonné ma maison, j'ai quitté mon héritage, j'ai livré l'objet de l'amour de mon âme aux paumes de ses ennemis.
12:8	Mon héritage est devenu pour moi comme un lion dans la forêt, contre moi il a donné de la voix, c'est pourquoi je l’ai haï.
12:9	Mon héritage a-t-il été pour moi un oiseau de proie tacheté ? Les oiseaux de proie ne seront-ils pas autour de lui ? Venez, assemblez-vous, vous tous les animaux des champs, venez pour le dévorer<!--Es. 56:9.--> !
12:10	Beaucoup de bergers ravagent ma vigne, ils foulent mon champ. Ils réduisent le champ de mes délices en un désert, en une désolation.
12:11	Ils le réduisent en un désert. Il est en deuil, il est désolé devant moi. Toute la terre est ravagée, parce que pas un homme ne prend cela à cœur.
12:12	Les dévastateurs arrivent sur tous les lieux élevés du désert, car l'épée de YHWH dévore d'un bout de la terre jusqu'à l'autre bout de la terre. Il n'y a de paix pour aucune chair.
12:13	Ils ont semé du froment, et ils moissonnent des épines, ils se sont fatigués sans profit. Soyez honteux de vos récoltes, à cause de la colère des narines de YHWH<!--Lé. 26:16.-->.
12:14	Ainsi parle YHWH contre tous mes mauvais voisins, qui touchent à l'héritage que j'ai fait hériter à mon peuple d'Israël : Voici, je les arracherai de leur sol, et j'arracherai la maison de Yéhouda du milieu d'eux.
12:15	Il arrivera qu'après les avoir arrachés, j'aurai encore compassion d'eux et je les ferai retourner chaque homme à son héritage, chaque homme à sa terre<!--De. 30:3.-->.
12:16	Il arrivera que s'ils apprennent, s'ils apprennent les voies de mon peuple, pour jurer par mon Nom, en disant : YHWH est vivant ! Comme ils ont enseigné à mon peuple à jurer par Baal, ils seront édifiés au milieu de mon peuple.
12:17	Mais s'ils n'écoutent pas, j'arracherai, j'arracherai une telle nation, et je la ferai périr, – déclaration de YHWH<!--Es. 60:12.-->.

## Chapitre 13

### La ceinture abîmée, illustration du jugement

13:1	Ainsi m'a parlé YHWH : Va, et achète-toi une ceinture de lin et mets-la sur tes reins, mais ne la mets pas dans l'eau.
13:2	J'achetai une ceinture selon la parole de YHWH, et je la mis sur mes reins.
13:3	La parole de YHWH m'est apparue pour la seconde fois, en disant :
13:4	Prends la ceinture que tu as achetée et qui est sur tes reins, lève-toi, va-t'en vers l'Euphrate, et là, cache-la dans la fente d'un rocher.
13:5	J'allai et je la cachai près de l'Euphrate, comme YHWH me l'avait ordonné.
13:6	Et il arriva, à la fin d'un grand nombre de jours, que YHWH me dit : Lève-toi, va vers l'Euphrate et prends la ceinture que je t'avais ordonné d'y cacher.
13:7	J'allai vers l'Euphrate, je creusai et je pris la ceinture dans le lieu où je l'avais cachée, mais voici que la ceinture était pourrie, n’étant plus bonne à rien.
13:8	La parole de YHWH m'est apparue en disant :
13:9	Ainsi parle YHWH : C'est ainsi que je détruirai l'orgueil de Yéhouda et le grand orgueil de Yeroushalaim.
13:10	Ce méchant peuple qui refuse d'écouter mes paroles, qui marche selon la dureté de son cœur, et qui va après d'autres elohîm, pour les servir et pour se prosterner devant eux, qu'il devienne comme cette ceinture qui n’est bonne à rien !
13:11	Car comme une ceinture est attachée aux reins d'un homme, ainsi je m'étais attaché toute la maison d'Israël et toute la maison de Yéhouda, – déclaration de YHWH –, afin qu'elles soient mon peuple, mon Nom, ma louange, et ma gloire. Mais ils ne m'ont pas écouté.
13:12	Tu leur diras cette parole-ci : Ainsi parle YHWH, l'Elohîm d'Israël : Toute outre sera remplie de vin. Et ils te diront : Ne savons-nous pas que toute outre sera remplie de vin ?
13:13	Tu leur diras : Ainsi parle YHWH : Voici, je vais remplir d'ivresse tous les habitants de cette terre, les rois qui sont assis sur le trône de David, les prêtres, les prophètes, et tous les habitants de Yeroushalaim.
13:14	Et je les briserai, l'homme contre son frère, les pères avec les fils ensemble, – déclaration de YHWH<!--Es. 51:17-20 ; Ps. 60:5.-->. Je n'aurai pas de compassion, je n'épargnerai pas, et je n'aurai pas de miséricorde, rien ne m'empêchera de les détruire.
13:15	Écoutez et prêtez l'oreille ! Ne vous élevez pas ! Car YHWH parle.
13:16	Donnez gloire à YHWH, votre Elohîm, avant qu'il fasse venir les ténèbres, avant que vos pieds se heurtent contre les montagnes du crépuscule. Vous attendrez la lumière, mais il la changera en ombre de la mort, il la réduira en profonde obscurité<!--Es. 59:9 ; Jn. 12:35.-->.
13:17	Si vous n'écoutez pas ceci, mon âme pleurera en secret, face à l'orgueil. Mes yeux vont pleurer, pleurer, fondre en pleurs, parce que le troupeau de YHWH sera emmené captif<!--La. 1:2-16.-->.
13:18	Dis au roi et à la reine : Humiliez-vous, asseyez-vous ! Car elle est tombée de vos têtes, la couronne de votre gloire.
13:19	Les villes du midi sont fermées, il n'y a personne qui les ouvre. Tout Yéhouda est emmené captif, il est emmené captif complètement.
13:20	Levez vos yeux et voyez ceux qui viennent du nord. Où est le troupeau qui t'avait été donné, le troupeau qui faisait ta gloire ?
13:21	Que diras-tu quand il te punira ? Car tu leur appris à être princes et chefs sur toi. Les douleurs ne te saisiront-elles pas, comme elles saisissent une femme qui enfante ?
13:22	Si tu dis en ton cœur : Pourquoi cela m'arrive-t-il ? C'est à cause de la multitude de tes iniquités que les pans de ta robe sont découverts, tes talons brutalisés<!--Es. 47:2-3.-->.
13:23	Un Éthiopien changera-t-il sa peau, un léopard ses taches ? Et vous, pourriez-vous faire le bien, vous qui êtes accoutumés à faire le mal ?
13:24	Je les disperserai comme le chaume qu’ emporte le vent du désert.
13:25	Voilà ton sort, la portion que je te mesure, – déclaration de YHWH –, parce que tu m'as oublié, et que tu as mis ta confiance dans le mensonge.
13:26	Moi aussi, je relèverai les pans de ta robe sur tes faces, et ta honte se verra.
13:27	Tes adultères et tes hennissements, la méchanceté de tes prostitutions sur les collines et dans les champs, tes abominations, je les ai vues. Malheur à toi, Yeroushalaim ! Ne seras-tu pas purifiée ? Jusqu'à quand cela durera-t-il ?

## Chapitre 14

### La terre frappée par la sécheresse

14:1	La parole de YHWH qui apparut à Yirmeyah au sujet de la sécheresse.
14:2	Yéhouda est dans le deuil, ses portes languissent. Elles sont assombries à terre, les cris de Yeroushalaim montent aux cieux.
14:3	Les majestueux envoient les insignifiants chercher de l'eau. Ils vont aux citernes, mais ils ne trouvent pas d'eau, ils reviennent avec leurs vases vides. Ils sont honteux et confus, ils couvrent leur tête.
14:4	À cause du sol qui s’est brisé parce qu'il n'y a pas eu de pluie sur la terre, les laboureurs sont honteux, ils se couvrent la tête.
14:5	Car même la biche dans le champ met bas et abandonne sa portée, car il n'y a pas d'herbe.
14:6	Les ânes sauvages se tiennent sur les lieux élevés, aspirant l'air comme des dragons. Leurs yeux se consument parce qu'il n'y a pas d'herbe.
14:7	Si nos iniquités témoignent contre nous, agis à cause de ton Nom, YHWH<!--Es. 59:12.--> ! Car nos apostasies sont nombreuses, c'est contre toi que nous avons péché.
14:8	Toi qui es l'espérance d'Israël, son Sauveur au temps de la détresse, pourquoi serais-tu sur la terre comme un étranger, comme un voyageur qui se détourne pour passer la nuit ?
14:9	Pourquoi deviens-tu comme un homme stupéfait, comme un homme vaillant qui n'est pas capable de sauver ? Tu es au milieu de nous, YHWH, et ton Nom est invoqué sur nous : Ne nous abandonne pas !
14:10	Voici ce que YHWH dit de ce peuple : Parce qu'ils aiment à errer ainsi çà et là, et qu'ils ne savent retenir leurs pieds, YHWH ne prend pas plaisir en eux, il se souvient maintenant de leurs iniquités, et il punit leurs péchés<!--Os. 8:13.-->.
14:11	YHWH me dit : N'intercède pas pour le bonheur de ce peuple.
14:12	Quand ils jeûneront, je n'écouterai pas leurs cris. Quand ils feront monter des holocaustes et des offrandes, je n'y prendrai pas plaisir. Mais je les consumerai par l'épée, par la famine et par la peste.
14:13	Je dis : Ah ! Adonaï YHWH ! Voici, les prophètes leur disent : Vous ne verrez pas l'épée, et vous n'aurez pas de famine, mais je vous donnerai en ce lieu-ci une paix véritable.
14:14	YHWH me dit : C'est le mensonge que ces prophètes prophétisent en mon Nom. Je ne les ai pas envoyés, je ne leur ai pas donné d'ordre, je ne leur ai pas parlé. Ils vous prophétisent des visions de mensonge, des divinations de néant et des tromperies de leur cœur<!--De. 18:20-22 ; Ez. 13:2-3.-->.
14:15	C'est pourquoi ainsi parle YHWH sur les prophètes qui prophétisent en mon Nom, sans que je les ai envoyés, et qui disent : Il n'y aura ni épée ni la famine sur cette terre. Ces prophètes-là seront consumés par l'épée et par la famine.
14:16	Et le peuple à qui ils prophétisent sera jeté dans les rues de Yeroushalaim à cause de la famine et de l'épée. Il n'y aura personne pour les enterrer, ni eux, ni leurs femmes, ni leurs fils, ni leurs filles. Je répandrai sur eux leur méchanceté.
14:17	Dis-leur cette parole : Les larmes tombent de mes yeux nuit et jour, sans cesse,<!--La. 1:16.-->, car la vierge, fille de mon peuple, a été frappée d'un grand coup, d'une plaie très douloureuse.
14:18	Si je sors dans les champs, voici des gens blessés mortellement par l'épée ! Si j'entre dans la ville, voilà les maladies de la famine ! Même le prophète et le prêtre parcourent la terre, sans savoir où ils vont.
14:19	As-tu rejeté, rejeté Yéhouda ? Ton âme a-t-elle rejeté Sion avec dégoût ? Pourquoi nous frappes-tu sans qu'il y ait pour nous de guérison ? On attendait la paix, mais il n'y a rien de bon, un temps de guérison, et voici la terreur !
14:20	YHWH, nous reconnaissons notre méchanceté, l'iniquité de nos pères. Car nous avons péché contre toi<!--Ps. 106:6 ; Da. 9:8.-->.
14:21	Ne nous rejette pas, à cause de ton Nom, et ne déshonore pas le trône de ta gloire ! Souviens-toi de ton alliance avec nous, ne la romps pas !
14:22	Parmi les vanités<!--Ce terme veut aussi dire « idole ».--> des nations, y en a-t-il qui fassent pleuvoir ? Les cieux donnent-ils les grosses averses<!--Es. 30:23 ; Ac. 14:17.--> ? N'est-ce pas toi, YHWH, notre Elohîm ? C'est pourquoi nous nous attendons à toi, car c'est toi qui as fait toutes ces choses.

## Chapitre 15

### YHWH fermement décidé à juger son peuple

15:1	YHWH me dit : Quand Moshé et Shemouél se tiendraient devant moi, mon âme ne reviendrait pas vers ce peuple. Chasse-le loin de mes faces ! Qu’ils sortent !
15:2	Il arrivera que, s’ils te disent : Où sortirons-nous ? Tu leur diras : Ainsi parle YHWH : À la mort ceux qui sont pour la mort, à l'épée ceux qui sont pour l'épée, à la famine ceux qui sont pour la famine, à la captivité ceux qui sont pour la captivité<!--Za. 11:9.--> !
15:3	J'enverrai sur eux quatre familles, – déclaration de YHWH : l'épée pour tuer, les chiens pour traîner, les oiseaux des cieux et les bêtes de la Terre pour dévorer et pour détruire.
15:4	Je ferai d'eux un objet de terreur pour tous les royaumes de la Terre, à cause de Menashè, fils de Yehizqiyah, roi de Yéhouda, pour les choses qu'il a faites dans Yeroushalaim.
15:5	Car qui aura compassion de toi, Yeroushalaim, ou qui te plaindra ? Ou qui se détournera pour s'informer de ta paix ?
15:6	Tu m'as abandonné, – déclaration de YHWH –, tu es retournée en arrière. C'est pourquoi j'étends ma main sur toi, et je te détruis, je suis las d'avoir compassion.
15:7	Je les vanne avec un van aux portes de la terre<!--Mt. 3:12.-->, je prive d'enfants, je fais périr mon peuple, et ils ne se sont pas détournés de leurs voies.
15:8	Je multiplie ses veuves plus que le sable de la mer. Je fais venir sur eux, sur la mère du jeune homme, le dévastateur en plein midi. Je fais tomber soudain sur elle une fin soudaine et l'angoisse.
15:9	Celle qui en avait enfanté sept languit, elle rend l'âme. Son soleil se couche pendant qu'il est encore jour<!--Am. 8:9.-->, elle est confondue et couverte de honte. Ceux qui restent, je les livre à l'épée devant leurs ennemis, – déclaration de YHWH.
15:10	Malheur à moi, ma mère, parce que tu m'as enfanté<!--Job 3:1-2.--> homme de controverse et homme de dispute pour toute la terre ! Je n’ai pas prêté, on ne m’a pas prêté, mais tous me maudissent.
15:11	YHWH dit : Si je ne te libère pour le bonheur ! Si je ne fais venir au-devant de toi l’ennemi, au temps du malheur et au temps de la détresse.
15:12	Le fer brisera-t-il le fer du nord et le cuivre ?
15:13	Je livre au pillage, sans en faire le prix, tes richesses et tes trésors, et cela à cause de tous tes péchés, sur tout ton territoire.
15:14	Je te fais passer avec tes ennemis sur une terre que tu ne connais pas, car le feu de ma colère s'est allumé, il brûle sur vous<!--De. 32:22.-->.

### La mise à part de Yirmeyah

15:15	Toi, tu sais, YHWH ! Souviens-toi de moi, visite-moi, venge-moi de mes persécuteurs<!--Ps. 106:4.--> ! Dans la lenteur de ta colère ne m'enlève pas ! Sache que je supporte l'insulte à cause de toi.
15:16	J'ai trouvé tes paroles, je les ai dévorées<!--Ez. 3:3 ; Ap. 10:9.-->. Tes paroles ont fait la joie et l'allégresse de mon cœur, car ton Nom est invoqué sur moi, YHWH, Elohîm Tsevaot !
15:17	Je ne m’assieds pas dans l'assemblée des moqueurs pour me réjouir. En face de ta main je m’assieds solitaire, car tu me remplis d'indignation.
15:18	Pourquoi ma douleur est-elle devenue éternelle ? Pourquoi ma plaie est-elle incurable et refuse-t-elle d'être guérie ? Es-tu devenu, es-tu devenu pour moi comme une source trompeuse, comme des eaux non fidèles ?
15:19	C'est pourquoi ainsi parle YHWH : Si tu reviens, je te ferai revenir et tu te tiendras debout en face de moi. Si tu fais sortir le précieux du vil, tu deviendras comme ma bouche. Eux reviendront vers toi, mais toi, tu n'as pas à revenir vers eux ! 
15:20	Je ferai de toi une muraille de cuivre fortifiée pour ce peuple. Ils combattront contre toi, mais ils n'auront pas le dessus sur toi, car je suis avec toi pour te sauver et te délivrer, – déclaration de YHWH.
15:21	Je te délivrerai de la main des méchants, je te rachèterai de la paume des terrifiants.

## Chapitre 16

### Célibat de Yirmeyah, illustration du jugement sur Yéhouda

16:1	La parole de YHWH m'est apparue en disant :
16:2	Tu ne prendras pas de femme et tu n'auras pas de fils ni de filles en ce lieu.
16:3	Car ainsi parle YHWH sur les fils et les filles qui naîtront en ce lieu, sur leurs mères qui les auront enfantés, et sur leurs pères qui les auront engendrés sur cette terre :
16:4	Ils mourront de maladie mortelle, sans être pleurés ni enterrés. Ils deviendront du fumier sur les faces du sol. Ils seront consumés par l'épée et par la famine, et leurs cadavres deviendront une nourriture pour les oiseaux des cieux et pour les bêtes de la Terre.
16:5	Car ainsi parle YHWH : N'entre pas dans une maison de deuil, ne va pas te lamenter ni te plaindre avec eux, car j'ai retiré à ce peuple ma paix, – déclaration de YHWH –, ma miséricorde et mes compassions.
16:6	Grands et petits mourront sur cette terre. Ils ne seront pas enterrés, on ne les pleurera pas, on ne se fera pas d'incision, et on ne se rasera pas pour eux<!--Lé. 19:28 ; De. 14:1 ; Ez. 7:11.-->.
16:7	Nul ne partagera avec eux le deuil pour les consoler au sujet d'un mort. Nul ne leur donnera à boire de la coupe de consolation pour leur père ou pour leur mère.
16:8	N'entre pas non plus dans une maison de festin pour t'asseoir avec eux, pour manger et pour boire.
16:9	Car ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, je vais faire cesser dans ce lieu-ci, devant vos yeux et en vos jours, les cris de joie et les cris d'allégresse, la voix de l'époux et la voix de l'épouse.
16:10	Il arrivera que quand tu annonceras à ce peuple toutes ces paroles, ils te diront : Pourquoi YHWH parle-t-il de tout ce grand mal contre nous ? Quelle est notre iniquité ? Quel est notre péché par lequel nous avons péché contre YHWH, notre Elohîm ?
16:11	Tu leur diras : Parce que vos pères m'ont abandonné, – déclaration de YHWH. Ils sont allés après d'autres elohîm, les ont servis et se sont prosternés devant eux. Ils m'ont abandonné et ils n'ont pas gardé ma torah.
16:12	Et vous, vous avez fait le mal plus encore que vos pères. Car voici, chacun de vous marche selon la dureté de son mauvais cœur pour ne pas m'écouter.
16:13	À cause de cela, je vous transporterai de cette terre vers une terre que vous n'avez pas connue, ni vous ni vos pères. Là, vous servirez jour et nuit les autres elohîm, car je ne vous accorderai plus aucune faveur<!--De. 28:64-65.-->.
16:14	C'est pourquoi, voici, des jours viennent, – déclaration de YHWH –, où l'on ne dira plus : YHWH est vivant, lui qui a fait monter les fils d'Israël de la terre d'Égypte,
16:15	mais plutôt : Vivant est YHWH, lui qui a fait monter les fils d'Israël de la terre du nord et de toutes les terres où il les avait bannis. Car je les ferai revenir sur leur sol, celui que j'ai donné à leurs pères.
16:16	Voici, j'envoie des pêcheurs en grand nombre, – déclaration de YHWH –, et ils les pêcheront. Ensuite, j'enverrai des chasseurs en grand nombre, et ils les chasseront de toutes les montagnes et de toutes les collines, et des fentes des rochers.
16:17	Car mes yeux sont sur toutes leurs voies, elles ne sont pas cachées en face de moi, et leur iniquité n'est pas couverte devant mes yeux<!--Pr. 5:21 ; Job 34:21.-->.
16:18	Je leur paierai premièrement le double de leur iniquité et de leur péché, parce qu'ils ont souillé ma terre par les cadavres de leurs idoles, et parce qu'ils ont rempli mon héritage de leurs abominations.
16:19	YHWH, ma force, ma forteresse et mon refuge au jour de la détresse ! Les nations viendront à toi des extrémités de la Terre et diront : En effet, nos pères ont hérité le mensonge et la vanité, choses où il n’y a aucun profit.
16:20	L'être humain se fabrique-t-il des elohîm, qui ne sont pas des elohîm ?
16:21	C'est pourquoi voici, je leur fais connaître, cette fois, je leur fais connaître ma main et ma puissance. Ils sauront que mon Nom est YHWH.

## Chapitre 17

### Le caractère sinueux du cœur

17:1	Le péché de Yéhouda est écrit avec un burin de fer, avec une pointe de diamant. Il est gravé sur la tablette de leur cœur et sur les cornes de leurs autels.
17:2	Leurs fils se souviendront de leurs autels et de leurs asherah, près des arbres verts, sur les hautes collines.
17:3	Montagne dans les champs, ta richesse et tous tes trésors, je les livre au pillage à cause du péché de tes hauts lieux sur tout ton territoire.
17:4	Et, de toi-même, tu te détacheras de l'héritage que je t'avais donné. Je te ferai servir tes ennemis sur une terre que tu ne connais pas, car vous avez allumé le feu de ma colère et, jusqu'à l'éternité, il brûlera.
17:5	Ainsi parle YHWH : Maudit soit l'homme fort qui se confie dans un être humain, qui fait de la chair sa force, et dont le cœur se détourne de YHWH !
17:6	Il deviendra un dénudé<!--Le mot hébreu signifie : « dénudé », « dépouillé » ou « destitué ». Voir Ps. 102:18.--> dans la région aride : il ne verra pas venir le bonheur, mais il habitera dans des lieux brûlés du désert, une terre salée où nul n’habite.
17:7	Béni soit l'homme fort qui se confie en YHWH, et dont YHWH est devenu l'espérance<!--1 Ti. 1:1.--> !
17:8	Il deviendra un arbre planté près des eaux<!--Ps. 23.-->, qui étend ses racines vers le ruisseau et, quand la chaleur viendra, il ne s'en apercevra pas, et sa feuille restera verte. Une année de sécheresse ne l'inquiètera pas, et il ne se retirera pas sans produire du fruit.
17:9	Le cœur est trompeur plus que tout, et il est incurable. Qui peut le connaître<!--Ps. 64:7.--> ?
17:10	Moi, YHWH, je sonde le cœur, j'éprouve les reins<!--Ap. 2:23.--> pour rendre à chacun selon sa voie, selon le fruit de ses actions.
17:11	Comme une perdrix qui couve ce qu'elle n'a pas pondu, celui qui se fait des richesses sans justice les laissera au milieu de ses jours, et sa fin sera celle d’un insensé<!--Ec. 4:8.-->.
17:12	Le lieu de notre sanctuaire est un trône de gloire, un lieu haut élevé dès le commencement.
17:13	YHWH, tu es l'espérance d'Israël ! Tous ceux qui t'abandonnent seront honteux. Ceux qui se détournent de moi seront inscrits sur la Terre, car ils abandonnent la source des eaux vives, dit YHWH<!--Es. 1:28 ; Ps. 73:28.-->.
17:14	YHWH, guéris-moi et je serai guéri ! Sauve-moi et je serai sauvé, car tu es ma louange.
17:15	Voici, ils me disent : Où est la parole de YHWH ? Qu'elle vienne maintenant<!--Es. 5:19 ; Ez. 12:23 ; 2 Pi. 3:3-4.--> !
17:16	Quant à moi, je ne me suis pas hâté d'être un berger derrière toi, je n'ai pas non plus désiré le jour des douleurs, tu le sais. Ce qui est sorti de mes lèvres est présent devant toi.
17:17	Ne deviens pas pour moi une terreur, toi, mon refuge au jour du malheur !
17:18	Que ceux qui me persécutent soient honteux, mais que je ne sois pas honteux. Qu'ils soient brisés, mais que je ne sois pas brisé ! Fais venir sur eux le jour du malheur, frappe-les d'une double plaie !

### Message à propos du shabbat

17:19	Ainsi m'a parlé YHWH : Va, et tiens-toi debout à la porte des fils du peuple, par laquelle les rois de Yéhouda entrent et par laquelle ils sortent, et à toutes les portes de Yeroushalaim.
17:20	Tu leur diras : Écoutez la parole de YHWH, vous, rois de Yéhouda, tout Yéhouda, et vous tous, habitants de Yeroushalaim qui entrez par ces portes !
17:21	Ainsi parle YHWH : Prenez garde à vos âmes ! Ne portez aucun fardeau le jour du shabbat, et ne les faites pas passer par les portes de Yeroushalaim<!--Né. 13:19.-->.
17:22	Ne faites sortir de vos maisons aucun fardeau le jour du shabbat, et ne faites aucune œuvre, mais sanctifiez le jour du shabbat, comme je l'ai ordonné à vos pères<!--Ex. 20:8, 23:12.-->.
17:23	Mais ils n'ont pas écouté, ils n'ont pas prêté l'oreille. Ils ont raidi leur cou pour ne pas écouter et ne pas recevoir la correction.
17:24	Il arrivera que si vous m'écoutez, si vous m'écoutez, – déclaration de YHWH –, pour ne pas faire passer de fardeau par les portes de cette ville le jour du shabbat, et si vous sanctifiez le jour du shabbat, en ne faisant aucune œuvre ce jour-là,
17:25	des rois et des princes entreront par la porte de cette ville, assis sur le trône de David, montés sur des chars et sur des chevaux, eux et les princes d'entre eux, les hommes de Yéhouda et les habitants de Yeroushalaim, et cette ville sera habitée pour toujours.
17:26	On viendra aussi des villes de Yéhouda et des environs de Yeroushalaim, de la terre de Benyamin, des basses terres, des montagnes et du midi, pour apporter des holocaustes, des sacrifices, des offrandes de grain et de l'encens, pour apporter aussi des louanges à la maison de YHWH.
17:27	Si vous ne m'écoutez pas pour sanctifier le jour du shabbat, pour ne pas porter de fardeau en entrant par les portes de Yeroushalaim le jour du shabbat, j’allumerai un feu dans ses portes, il dévorera les palais de Yeroushalaim et ne s'éteindra pas<!--2 R. 25:9.-->.

## Chapitre 18

### La maison du potier ; appel à la repentance et avertissement

18:1	La parole qui apparut à Yirmeyah de la part de YHWH, disant :
18:2	Lève-toi et descends dans la maison du potier. Là, je te ferai entendre mes paroles.
18:3	Je descendis dans la maison du potier, et voici, il faisait son ouvrage sur les roues du potier.
18:4	Le vase qu'il faisait avec l'argile fut détruit dans la main du potier. Il revint et fit un autre vase, comme il paraissait juste aux yeux du potier de le faire.
18:5	La parole de YHWH m'est apparue en disant :
18:6	Ne suis-je pas capable d'agir envers vous comme ce potier, maison d'Israël ? – déclaration de YHWH. Voici, comme l'argile est dans la main d'un potier, ainsi vous êtes dans ma main, maison d'Israël !
18:7	Tantôt je parle sur une nation, sur un royaume, pour arracher, pour abattre et pour détruire.
18:8	Mais cette nation, contre laquelle j'ai parlé, revient-elle de sa méchanceté, je me repens du mal que j'avais pensé lui faire<!--Jon. 3:6-10.-->.
18:9	Tantôt je parle sur une nation, sur un royaume, pour bâtir et pour planter.
18:10	Mais, si elle fait ce qui est mal à mes yeux et qu'elle n’écoute pas ma voix, je me repens du bien que j'avais dit de lui faire.
18:11	Maintenant, s’il te plaît, parle aux hommes de Yéhouda et aux habitants de Yeroushalaim, en disant : Ainsi parle YHWH : Voici, je façonne contre vous un malheur, et je projette contre vous des projets. Revenez, s’il vous plaît, chaque homme de sa mauvaise voie, rendez bonnes vos voies et vos actions !
18:12	Mais ils disent : C’est désespéré ! Oui, nous irons derrière nos pensées, chacun de nous agira selon la dureté de son mauvais cœur.
18:13	C'est pourquoi ainsi parle YHWH : S’il vous plaît, demandez aux nations ! Qui a entendu de telles choses ? La vierge d'Israël a fait une chose très horrible<!--1 Co. 5:1.-->.
18:14	La neige du Liban abandonne-t-elle le rocher du champ ? Les eaux étrangères, fraîches et ruisselantes, tarissent-elles ?
18:15	Oui, mon peuple m'a oublié. Il brûle de l'encens à la vanité. On l'a fait trébucher dans ses voies, dans les anciens sentiers, pour marcher sur les sentiers d'une voie non frayée,
18:16	afin de faire de leur terre un objet d'horreur, de sifflement perpétuel. Tous les passants seront stupéfaits et secoueront la tête.
18:17	Comme le vent d’orient, je les disperserai face à l'ennemi. C’est de dos et non de face que je les verrai au jour de leur  calamité<!--Es. 27:8.-->.
18:18	Et ils ont dit : Venez, inventons des projets contre Yirmeyah ! Car la torah ne périra pas chez le prêtre, ni le conseil chez le sage, ni la parole chez le prophète. Venez ! Tuons-le avec la langue et ne soyons plus attentifs à toutes ses paroles !
18:19	YHWH ! Prête attention à moi, et entends la voix de mes adversaires !
18:20	Le mal sera-t-il rendu pour le bien<!--Ps. 35:12, 109:5.--> ? Car ils ont creusé une fosse pour mon âme. Souviens-toi que je me suis tenu en face de toi, afin de parler pour leur bien, afin de détourner d'eux ton courroux.
18:21	C'est pourquoi livre leurs fils à la famine, fais-les couler par le pouvoir de l'épée ! Que leurs femmes soient privées d'enfants et deviennent veuves, que leurs maris soient tués par la mort, que leurs jeunes hommes soient frappés par l'épée dans la bataille<!--Ps. 109:9-13.--> !
18:22	Qu'on entende le cri de leurs maisons, quand tu feras venir subitement des troupes contre eux ! Car ils ont creusé une fosse pour me prendre, et ils ont caché des pièges pour mes pieds.
18:23	Et toi, YHWH, tu connais tous leurs conseils pour me faire mourir. Ne couvre pas leur iniquité et n'efface pas leur péché en face de toi ! Mais qu’ils trébuchent en face de toi ! Au temps de ta colère, agis contre eux !

## Chapitre 19

### Le vase brisé : Image de Yéhouda

19:1	Ainsi a parlé YHWH : Va ! Avec des anciens du peuple, des anciens parmi les prêtres, achète un vase de terre d'un potier.
19:2	Et sors à la vallée de Ben-Hinnom, qui est près de l'entrée de la porte orientale. Et là, tu proclameras les paroles que je te dirai.
19:3	Tu diras : Rois de Yéhouda, et vous, habitants de Yeroushalaim, écoutez la parole de YHWH ! Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, je vais faire venir sur ce lieu-ci un mal, tel que quiconque l'entendra, les oreilles lui tinteront<!--1 S. 3:11 ; 2 R. 21:12.-->.
19:4	Ils m'ont abandonné, ils ont profané ce lieu, ils y ont brûlé de l'encens pour d'autres elohîm, que ne connaissaient ni eux, ni leurs pères, ni les rois de Yéhouda, et ils ont rempli ce lieu du sang des innocents.
19:5	Ils ont bâti des hauts lieux du Baal pour brûler leurs fils dans le feu en holocaustes au Baal, chose que je n'avais pas ordonnée, dont je n'avais pas parlé et qui ne m'était pas montée au cœur.
19:6	À cause de cela, voici les jours viennent, – déclaration de YHWH –, où ce lieu ne sera plus appelé Topheth, ni la vallée de Ben-Hinnom, mais la vallée de la tuerie.
19:7	J'anéantirai en ce lieu le conseil de Yéhouda et de Yeroushalaim. Je les ferai tomber par l'épée devant leurs ennemis et par la main de ceux qui cherchent leur âme. Je livrerai leurs cadavres en nourriture aux oiseaux des cieux et aux bêtes de la Terre.
19:8	Je ferai de cette ville une désolation et une moquerie. Quiconque passera près d'elle sera étonné et sifflera à cause de toutes ses plaies.
19:9	Je leur ferai manger la chair de leurs fils et la chair de leurs filles. Chaque homme mangera la chair de son compagnon durant le siège, et dans la détresse où les réduiront leurs ennemis et ceux qui cherchent leur âme<!--Lé. 26:29 ; De. 28:53 ; La. 2:20.-->.
19:10	Tu briseras le vase, sous les yeux des hommes qui seront allés avec toi.
19:11	Tu leur diras : Ainsi parle YHWH Tsevaot : Je briserai ce peuple et cette ville, de même qu'on brise un vase de potier, qui ne peut être réparé. Et ils seront enterrés à Topheth parce qu'il n'y aura plus d'autre lieu pour les enterrer.
19:12	C'est ainsi que je traiterai ce lieu, – déclaration de YHWH –, et ses habitants, et je rendrai cette ville semblable à Topheth.
19:13	Les maisons de Yeroushalaim et les maisons des rois de Yéhouda deviendront impures comme le lieu de Topheth, à cause de toutes les maisons sur les toits desquelles ils brûlaient de l'encens à toute l'armée des cieux et faisaient des libations à d'autres elohîm.
19:14	Yirmeyah revint de Topheth, où YHWH l'avait envoyé prophétiser. Il se tint debout dans le parvis de la maison de YHWH et dit à tout le peuple :
19:15	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, je vais faire venir sur cette ville et sur toutes ses villes tout le mal que j'ai prononcé contre elle, parce qu'ils ont raidi leur cou pour ne pas écouter mes paroles.

## Chapitre 20

### Pashhour outrage Yirmeyah

20:1	Pashhour, fils d'Immer, prêtre et commissaire en chef dans la maison de YHWH, entendit Yirmeyah qui prophétisait ces paroles.
20:2	Et Pashhour frappa le prophète Yirmeyah et le mit dans la prison qui était à la porte supérieure de Benyamin, dans la maison de YHWH.
20:3	Il arriva que dès le lendemain, Pashhour fit sortir Yirmeyah de prison. Et Yirmeyah lui dit : YHWH ne t'appelle plus du nom de Pashhour, mais du nom de Magor-Missabib<!--« Magor-Missabib » signifie « terreur de chaque côté ».-->.
20:4	Car ainsi parle YHWH : Voici, je vais te livrer à la terreur, toi et tous tes amis qui tomberont par l'épée de leurs ennemis, et tes yeux le verront. Je livrerai tous ceux de Yéhouda entre les mains du roi de Babel, qui les transportera à Babel et les frappera de l'épée.
20:5	Je livrerai toutes les richesses de cette ville, tout son travail, et tout ce qu'elle a de précieux, je livrerai tous les trésors des rois de Yéhouda entre les mains de leurs ennemis. Ils les pilleront, les enlèveront et les conduiront à Babel.
20:6	Et toi, Pashhour, et tous ceux qui demeurent dans ta maison, vous irez en captivité. Tu iras à Babel, tu y mourras et y seras enterré, toi et tous tes amis auxquels tu as prophétisé le mensonge.

### Yirmeyah gémit auprès de YHWH

20:7	Tu m'as persuadé, YHWH, et je me suis laissé persuader ! Tu as été plus fort que moi et tu m'as vaincu. Je suis devenu un objet de dérision tout le jour, tout le monde se moque de moi.
20:8	Car depuis que je parle, je n'ai fait que pousser des cris, que proclamer : violence et dévastation ! Car la parole de YHWH est devenue pour moi une insulte et une dérision tout le jour<!--Es. 57:4.-->.
20:9	Je dis : Je ne ferai plus mention de lui, je ne parlerai plus en son Nom. Mais il y a dans mon cœur un feu ardent, renfermé dans mes os. Je suis fatigué de le porter, et je n'en peux plus.
20:10	Car j’entends les diffamations de beaucoup : Terreur de tous côtés ! Exposez-le ! Nous l'exposerons ! Tous les mortels qui étaient en paix avec moi observent si je boite : Peut-être se laissera-t-il séduire, et nous le vaincrons et nous tirerons vengeance de lui !
20:11	Mais YHWH est avec moi comme un homme vaillant et terrifiant. C'est pourquoi ceux qui me persécutent seront renversés, ils ne me vaincront pas. Ils seront honteux, car ils ne réussiront pas : Ce sera une honte éternelle qui ne s'oubliera jamais.
20:12	YHWH Tsevaot, qui éprouves les justes, qui vois les reins et les cœurs, je verrai ta vengeance sur eux, car c’est à toi que j’ai révélé ma cause.
20:13	Chantez à YHWH, louez YHWH ! Car il délivre l'âme des pauvres de la main des méchants.
20:14	Maudit soit le jour où je suis né ! Que le jour où ma mère m'a enfanté ne soit pas béni !
20:15	Maudit soit l'homme qui porta cette nouvelle à mon père, en lui disant : Un fils, un mâle t'est né, et qui l’a réjoui, l’a réjoui !
20:16	Que cet homme devienne comme les villes que YHWH a renversées sans s'en repentir ! Qu'il entende la clameur le matin, et l'alarme de guerre au temps du midi<!--Ge. 19:24-25 ; So. 2:4.--> !
20:17	Que ne m'a-t-on fait mourir dans le sein de ma mère ! Pourquoi ma mère ne m'a-t-elle pas servi de sépulcre ? Et pourquoi n'est-elle pas restée éternellement enceinte ?
20:18	Pourquoi suis-je sorti de son sein pour ne voir que peine et douleur, et pour consumer mes jours dans la honte ?

## Chapitre 21

### Prophétie sur les rois de Yéhouda : Tsidqiyah (Sédécias)

21:1	La parole qui apparut à Yirmeyah de la part de YHWH, lorsque le roi Tsidqiyah envoya vers lui Pashhour, fils de Malkiyah, et Tsephanyah, fils de Ma`aseyah, le prêtre, pour lui dire :
21:2	S’il te plaît, consulte YHWH pour nous, car Neboukadnetsar, roi de Babel, nous fait la guerre. Peut-être YHWH fera-t-il en notre faveur une de ses merveilles afin qu'il se retire de nous.
21:3	Et Yirmeyah leur dit : Vous direz ainsi à Tsidqiyah :
21:4	Ainsi parle YHWH, l'Elohîm d'Israël : Voici, je vais détourner les armes de guerre qui sont dans vos mains, et avec lesquelles vous combattez en dehors des murailles contre le roi de Babel et contre les Chaldéens qui vous assiègent, et je les rassemblerai au milieu de cette ville.
21:5	Je combattrai contre vous, à main étendue et à bras puissant, avec colère, courroux et grande indignation.
21:6	Je frapperai les habitants de cette ville, les hommes et les bêtes. Ils mourront d'une grande peste.
21:7	Après cela, – déclaration de YHWH –, je livrerai Tsidqiyah, roi de Yéhouda, ses serviteurs, le peuple et ceux qui dans cette ville survivront à la peste, à l'épée et à la famine, entre les mains de Neboukadnetsar<!--2 R. 24-25.-->, roi de Babel, entre les mains de leurs ennemis et entre les mains de ceux qui cherchent leur âme. Il les frappera à bouche d’épée, il ne les épargnera pas, il n'aura aucune pitié, aucune compassion.
21:8	Tu diras aussi à ce peuple : Ainsi parle YHWH : Voici, je mets devant vous le chemin de la vie et le chemin de la mort<!--De. 30:19.-->.
21:9	Quiconque restera dans cette ville mourra par l'épée, par la famine ou par la peste, mais celui qui en sortira pour se rendre aux Chaldéens qui vous assiègent vivra, il aura son âme comme butin.
21:10	Car j'ai tourné mes faces contre cette ville pour lui faire du mal et non du bien, – déclaration de YHWH. Elle sera livrée entre les mains du roi de Babel, et il la brûlera par le feu.
21:11	Quant à la maison du roi de Yéhouda : Écoutez la parole de YHWH !
21:12	Maison de David ! Ainsi parle YHWH : Rendez la justice dès le matin et délivrez de la main de l'oppresseur celui qui est pillé, de peur que mon courroux ne sorte comme un feu, et qu'il ne brûle sans qu'on puisse l'éteindre, à cause de la méchanceté de vos actions.
21:13	Voici que je suis contre toi qui habites dans la vallée, sur le rocher de la plaine, – déclaration de YHWH. À vous qui dites : Qui descendra contre nous, et qui entrera dans nos demeures ?
21:14	Je vous punirai selon le fruit de vos actions, – déclaration de YHWH. Je mettrai le feu dans sa forêt et il dévorera tout ce qui est autour d'elle<!--Ez. 21:2-3.-->.

## Chapitre 22

### Tsidqiyah (Sédécias) averti de la destruction de Yeroushalaim (Jérusalem)

22:1	Ainsi parle YHWH : Descends dans la maison du roi de Yéhouda, et là prononce cette parole.
22:2	Tu diras : Écoute la parole de YHWH, roi de Yéhouda qui es assis sur le trône de David, toi et tes serviteurs, et ton peuple, qui entrez par ces portes !
22:3	Ainsi parle YHWH : Observez le jugement et la justice, délivrez de la main de l'oppresseur celui qui est pillé, ne maltraitez pas l'orphelin, ni l'étranger, ni la veuve, n'agissez pas avec violence et ne répandez pas le sang innocent en ce lieu !
22:4	Car si vous agissez, si vous agissez selon cette parole, alors entreront par les portes de cette maison des rois assis sur le trône de David, montés sur des chars et sur des chevaux, eux, leurs serviteurs et leur peuple.
22:5	Si vous n'écoutez pas ces paroles, je le jure par moi-même<!--Es. 45:23 ; Hé. 6:13.-->, – déclaration de YHWH –, que cette maison deviendra une désolation.
22:6	Car ainsi parle YHWH sur la maison du roi de Yéhouda : Tu es pour moi un Galaad, un sommet du Liban, mais je ferai de toi un désert, une ville sans habitants.
22:7	Je prépare contre toi des destructeurs, chacun avec ses armes, qui couperont tes cèdres de choix, et les jetteront au feu.
22:8	Beaucoup de nations passeront près de cette ville, et chaque homme dira à son compagnon : Pourquoi YHWH a-t-il traité de la sorte cette grande ville<!--De. 29:23-27 ; 1 R. 9:8.--> ?
22:9	Et l’on dira : C'est parce qu'ils ont abandonné l'alliance de YHWH, leur Elohîm, et qu'ils se sont prosternés devant d'autres elohîm et les ont servis.

### Prophétie sur les rois de Yéhouda : Yehoachaz (Shalloum)

22:10	Ne pleurez pas celui qui est mort, ne vous lamentez pas sur lui ! Mais pleurez, pleurez celui qui s'en va, car il ne reviendra plus, il ne verra plus la terre de sa naissance.
22:11	Car ainsi parle YHWH sur Shalloum, fils de Yoshiyah, roi de Yéhouda, qui régnait à la place de Yoshiyah, son père, et qui est sorti de ce lieu : Il n'y reviendra plus,
22:12	mais il mourra dans le lieu où on l'a transporté, et ne verra plus cette terre.

### Prophétie sur les rois de Yéhouda : Yehoyaqiym

22:13	Malheur à celui qui bâtit sa maison par l'injustice, et ses chambres hautes sans droiture, qui fait travailler son prochain pour rien, sans lui donner le salaire de son travail<!--Lé. 19:13 ; De. 24:14-15 ; Ha. 2:9.-->,
22:14	qui dit : Je me bâtirai une grande maison et des chambres hautes spacieuses, et qui s'y fait percer des fenêtres, la recouvre de cèdre et la peint en rouge !
22:15	Régneras-tu, parce que tu rivalises avec le cèdre ? Ton père mangeait et buvait, mais il pratiquait le jugement et la justice, et il fut heureux.
22:16	Il jugeait la cause du pauvre et de l'indigent, alors il fut heureux. N'est-ce pas là me connaître ? – déclaration de YHWH.
22:17	Mais tu n'as des yeux et un cœur que pour ton profit, pour répandre le sang innocent, pour exercer l'oppression et la violence.
22:18	C'est pourquoi ainsi parle YHWH sur Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda : On ne se lamentera pas pour lui : Hélas, mon frère ! hélas, ma sœur ! On ne se lamentera pas pour lui : Hélas, seigneur ! hélas, sa majesté !
22:19	Il sera enterré comme on enterre un âne, il sera traîné et jeté hors des portes de Yeroushalaim.

### Prophétie sur les rois de Yéhouda : Konyah (Yekonyah)

22:20	Monte sur le Liban, et crie ! Donne de la voix sur le Bashân ! Crie du haut d'Abarim ! Car tous ceux qui t'aimaient sont brisés.
22:21	Je t’ai parlé dans ta prospérité. Tu as dit : Je n'écouterai pas. Telle est ta voie depuis ta jeunesse, car tu n'as pas écouté ma voix.
22:22	Tous tes bergers, un vent les fera paître, et tes amoureux iront en captivité. Certainement tu seras alors honteuse et confuse à cause de toute ta méchanceté.
22:23	Toi qui habites sur le Liban, et qui fais ton nid dans les cèdres, combien tu seras un objet de pitié quand les douleurs t'atteindront, les douleurs comme celles d'une femme qui enfante !
22:24	Moi, le Vivant – déclaration de YHWH, même si Konyah<!--Yekonyah.-->, fils de Yehoyaqiym, roi de Yéhouda, était une bague à ma main droite, je t'arracherais de là.
22:25	Je te livrerai entre les mains de ceux qui cherchent ton âme, et entre les mains de ceux devant qui tu es craintif, et entre les mains de Neboukadnetsar, roi de Babel, et entre les mains des Chaldéens<!--2 R. 24:14 ; Ez. 17:12 ; 2 Ch. 36:10.-->.
22:26	Et je te jetterai, toi et ta mère qui t'a enfanté, sur une autre terre où vous n'êtes pas nés, et vous y mourrez.
22:27	Et quant à la terre où leur âme aspire à retourner, ils n'y retourneront pas.
22:28	Cet homme, Konyah, est-il un vase méprisé et brisé ? Est-il un objet qui ne fait plus plaisir ? Pourquoi sont-ils jetés là, lui et sa postérité, lancés sur une terre qu'ils ne connaissent pas<!--Os. 8:8.--> ?
22:29	Terre ! Terre ! Terre ! Écoute la parole de YHWH !
22:30	Ainsi parle YHWH : Écrivez au sujet de cet homme fort qu'il sera privé d'enfants, qu'il ne prospérera pas pendant ses jours. Car aucun homme de sa postérité ne réussira plus à s'asseoir sur le trône de David et à régner sur Yéhouda<!--2 R. 24:8-16.-->.

## Chapitre 23

### La maison d'Israël sera rassemblée par le Mashiah (Christ)

23:1	Malheur aux bergers qui détruisent et dispersent le troupeau de mon pâturage ! – déclaration de YHWH.
23:2	C'est pourquoi ainsi parle YHWH, l'Elohîm d'Israël, sur les bergers qui font paître mon peuple : Vous avez dispersé mes brebis, vous les avez bannies, vous ne les avez pas visitées. Voici, je vous punirai à cause de la méchanceté de vos actions, – déclaration de YHWH.
23:3	Je rassemblerai le reste de mes brebis de toutes les terres où je les ai bannies. Je les ramènerai à leur pâturage, et elles porteront du fruit et se multiplieront.
23:4	Je susciterai aussi sur elles des bergers qui les paîtront. Elles n'auront plus de peur, elles s'épouvanteront plus et il n'en manquera aucune, – déclaration de YHWH.
23:5	Voici, les jours viennent, – déclaration de YHWH –, où je susciterai à David un Germe juste. Il régnera en roi, il prospérera et exercera le droit et la justice sur la terre<!--Es. 4:2 ; Za. 6:12-13 ; Ps. 96:13 ; Lu. 1:32-33.-->.
23:6	Dans ses jours, Yéhouda sera sauvé et Israël demeurera en sécurité. Voici le nom dont on l'appellera : YHWH-Tsidkenou<!--YHWH notre justice.-->.
23:7	C'est pourquoi, voici, les jours viennent, – déclaration de YHWH –, où l'on ne dira plus : Vivant est YHWH, lui qui a fait monter les fils d'Israël de la terre d'Égypte !
23:8	Mais : Vivant est YHWH, lui qui a fait monter et qui a ramené la postérité de la maison d'Israël, de la terre du nord et de toutes les terres où je les avais bannis, et ils habiteront sur leur sol.

### Jugement sur les faux prophètes

23:9	À cause des prophètes mon cœur est brisé au-dedans de moi, tous mes os se relâchent. Je suis comme un homme ivre, et comme un homme fort imprégné de vin, face à YHWH, et face aux paroles de sa sainteté.
23:10	Car la terre est remplie d'adultère. La terre est en deuil à cause de la malédiction : Les pâturages du désert sont desséchés, leur course ne va qu'au mal, et leur force à ce qui n'est pas droit.
23:11	Car le prophète et le prêtre sont corrompus. J'ai même trouvé dans ma maison leur méchanceté, – déclaration de YHWH.
23:12	C'est pourquoi leur chemin sera comme des lieux glissants dans l'obscurité. Ils y seront poussés et y tomberont<!--Ps. 35:6 ; Pr. 4:19.-->. Oui, je ferai venir le malheur sur eux, l'année de leur châtiment, – déclaration de YHWH.
23:13	Or j'ai vu de la folie dans les prophètes de Samarie, car ils prophétisaient par Baal et ils égaraient Israël, mon peuple.
23:14	Mais j'ai vu des choses horribles chez les prophètes de Yeroushalaim : ils commettent des adultères et ils marchent dans le mensonge. Ils fortifient les mains de ceux qui font le mal, afin qu'aucun ne se détourne de sa méchanceté. Eux tous sont devenus pour moi comme Sodome et ses habitants comme Gomorrhe<!--Es. 1:9.-->.
23:15	C'est pourquoi, ainsi parle YHWH Tsevaot sur les prophètes : Voici, je vais leur faire manger de l'absinthe, et leur ferai boire des eaux empoisonnées, car c'est par les prophètes que la profanation est venue sur toute la terre.
23:16	Ainsi parle YHWH Tsevaot : N'écoutez pas les paroles des prophètes qui vous prophétisent ! Ils vous font devenir vains<!--Ils vous remplissent des vains espoirs.-->, ils parlent des visions de leur cœur, et non pas de la bouche de YHWH.
23:17	Ils disent, ils disent à ceux qui me méprisent : YHWH a dit : Vous aurez la paix, et ils disent à tous ceux qui marchent suivant la dureté de leur cœur : Il ne vous arrivera aucun mal<!--Ez. 13:10.-->.
23:18	Car qui s'est trouvé au conseil secret de YHWH ? Et qui a aperçu et entendu sa parole<!--Es. 40:13 ; Job 15:8 ; 1 Co. 2:16.--> ? Qui a été attentif à sa parole, et l'a entendue ?
23:19	Voici la tempête de YHWH, son courroux va se montrer, et le tourbillon prêt à fondre tombera sur la tête des méchants.
23:20	La colère de YHWH ne se détournera pas jusqu'à ce qu'il ait accompli, exécuté les desseins de son cœur. Vous le comprendrez avec discernement dans les derniers jours<!--Ge. 49:1-2.-->.
23:21	Je n'ai pas envoyé ces prophètes-là, et ils ont couru. Je ne leur ai pas parlé, et ils ont prophétisé.
23:22	S'ils s'étaient trouvés dans mon conseil secret, ils auraient aussi fait entendre mes paroles à mon peuple, et ils les auraient ramenés de leur mauvaise voie, de la méchanceté de leurs actions.
23:23	Ne suis-je Elohîm que de près ? – déclaration de YHWH. Ne suis-je pas aussi Elohîm de loin ?
23:24	Si un homme se cache dans des lieux cachés, moi ne le verrai-je pas ? – déclaration de YHWH. Ne remplis-je pas, moi, les cieux et la Terre ? – déclaration de YHWH<!--Ps. 139:7-8 ; Am. 9:2-3.-->.
23:25	J'ai entendu ce que disent les prophètes qui prophétisent le mensonge en mon Nom en disant : J'ai eu un rêve ! J'ai eu un rêve !
23:26	Jusqu’à quand cela existera-t-il dans le cœur des prophètes qui prophétisent le mensonge, qui sont des prophètes de la tromperie de leur cœur ?
23:27	Ils pensent faire oublier mon Nom à mon peuple, par leurs rêves qu’ils se racontent, chaque homme à son ami, comme leurs pères ont oublié mon Nom pour Baal<!--Jg. 2:13.-->.
23:28	Que le prophète qui a eu un rêve raconte ce rêve et que celui qui a ma parole proclame ma parole en vérité ! Qu'est-ce que la paille a à faire avec le blé ? – déclaration de YHWH.
23:29	Ma parole n'est-elle pas comme un feu, – déclaration de YHWH –, comme un marteau qui brise le roc ?
23:30	C'est pourquoi, me voici contre les prophètes – déclaration de YHWH – qui se volent mes paroles, chaque homme à son ami.
23:31	Me voici contre les prophètes – déclaration de YHWH – qui prennent leur langue et qui disent<!--« Prophétiser », « prononcer une prophétie », « parler comme prophète ».--> : Déclaration !
23:32	Me voici contre ceux qui prophétisent des rêves mensongers – déclaration de YHWH –, qui les racontent et qui égarent mon peuple par leurs mensonges et leur témérité, alors que, moi, je ne les ai pas envoyés, je ne leur ai pas donné d'ordre, et ils ne sont d'aucune utilité à ce peuple – déclaration de YHWH<!--So. 3:4.-->.
23:33	Si ce peuple t'interroge, ou qu'il interroge le prophète, ou le prêtre, en disant : Quel est le fardeau de YHWH ? Tu leur diras : Quel est ce fardeau ? Je vous abandonnerai, – déclaration de YHWH.
23:34	Quant au prophète, au prêtre, ou au peuple qui dira : Fardeau de YHWH, je punirai cet homme-là et sa maison.
23:35	Vous direz ainsi, chaque homme à son ami, chaque homme à son frère : Qu'a répondu YHWH ? Qu'a déclaré YHWH ?
23:36	Vous ne ferez plus mention du fardeau de YHWH. Car la parole de chaque homme deviendra pour lui un fardeau, parce que vous changez<!--« tourner », « renverser », « contourner », « transformer ». 2 Pi. 3:15-16.--> les paroles d'Elohîm le Vivant, de YHWH Tsevaot, notre Elohîm.
23:37	Tu diras au prophète : Que t'a répondu YHWH et que t'a dit YHWH ?
23:38	Et si vous dites : « Fardeau de YHWH ! » - alors, ainsi parle YHWH : Parce que vous dites cette parole : « Fardeau de YHWH ! », alors que j'ai envoyé vers vous pour dire : Ne dites plus : « Fardeau de YHWH ! »
23:39	À cause de cela, voici, je vous oublierai<!--Vient de l'hébreu « nasha » qui signifie aussi « tromper », « séduire », « décevoir ».-->, je vous décevrai, je vous abandonnerai avec la ville que je vous avais donnée, à vous et à vos pères, loin de mes faces.
23:40	Et je mettrai sur vous une insulte éternelle et une honte éternelle, qui ne s'oublieront pas.

## Chapitre 24

### Bonnes figues : Futur retour en Yéhouda des captifs de Babel ; mauvaises figues : Jugement sur Yeroushalaim (Jérusalem)

24:1	YHWH me fit voir deux paniers de figues posés devant le temple de YHWH, après que Neboukadnetsar, roi de Babel, eut transporté de Yeroushalaim, Yekonyah, fils de Yehoyaqiym, roi de Yéhouda, les chefs de Yéhouda, avec les artisans et les serruriers, et les eut conduits à Babel.
24:2	L'un des paniers avait de très bonnes figues, comme les figues de la première récolte, et l'autre panier avait de très mauvaises figues, qu'on ne pouvait manger à cause de leur mauvaise qualité.
24:3	YHWH me dit : Que vois-tu, Yirmeyah ? Je dis : Des figues. Les bonnes figues sont très bonnes, et les mauvaises sont très mauvaises et ne peuvent être mangées à cause de leur mauvaise qualité.
24:4	La parole de YHWH m'est apparue en disant :
24:5	Ainsi parle YHWH, l'Elohîm d'Israël : Comme tu distingues ces bonnes figues, ainsi je distinguerai, pour leur bonheur, les captifs de Yéhouda, que j'ai envoyés hors de ce lieu en terre des Chaldéens.
24:6	Je mettrai mes yeux sur eux pour leur bonheur et je les ramènerai sur cette terre, je les y rétablirai et je ne les détruirai plus, je les planterai et je ne les arracherai pas.
24:7	Je leur donnerai un cœur pour qu'ils sachent que je suis YHWH. Ils seront mon peuple et je serai leur Elohîm, car ils reviendront à moi de tout leur cœur<!--De. 30:6 ; Ez. 11:19.-->.
24:8	Et comme de mauvaises figues qu'on ne peut manger tant elles sont mauvaises, ainsi parle YHWH : C'est ainsi que je traiterai Tsidqiyah, roi de Yéhouda, ses chefs, et le reste de Yeroushalaim, ceux qui sont restés sur cette terre, et ceux qui habitent en terre d'Égypte.
24:9	Je ferai d'eux un objet de terreur, de malheur pour tous les royaumes de la Terre, d'insulte, de proverbe, de raillerie et de malédiction, dans tous les lieux où je les aurai bannis<!--De. 28:37.-->.
24:10	J'enverrai sur eux l'épée, la famine et la peste, jusqu'à ce qu'ils soient exterminés du sol que j'avais donné à eux et à leurs pères.

## Chapitre 25

### Prophétie sur les 70 ans de captivité babylonienne<!--Da. 9:2.-->

25:1	La parole qui apparut à Yirmeyah concernant tout le peuple de Yéhouda, la quatrième année de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda, qui est la première année de Neboukadnetsar, roi de Babel,
25:2	parole que Yirmeyah, le prophète, prononça à tout le peuple de Yéhouda, et à tous les habitants de Yeroushalaim, en disant :
25:3	Depuis la treizième année de Yoshiyah, fils d'Amon, roi de Yéhouda, jusqu'à ce jour, il y a 23 ans que la parole de YHWH est venue à moi. Je vous ai parlé, me levant dès le matin et parlant, et vous n'avez pas écouté.
25:4	Et YHWH vous a envoyé tous ses serviteurs, les prophètes, se levant dès le matin et les envoyant, et vous ne les avez pas écoutés, vous n'avez pas prêté l'oreille pour écouter.
25:5	Lorsqu'ils disaient : Revenez, s’il vous plaît, chaque homme de sa mauvaise voie, de la méchanceté de vos actions et vous habiterez d'éternité en éternité sur le sol que YHWH a donné à vous et à vos pères<!--Jon. 3:8 ; 2 R. 17:13.-->.
25:6	N'allez pas après d'autres elohîm pour les servir et pour vous prosterner devant eux, ne m'irritez pas par les œuvres de vos mains, et je ne vous ferai aucun mal.
25:7	Mais vous ne m’avez pas écouté, – déclaration de YHWH –, pour m'irriter par les œuvres de vos mains, pour votre malheur.
25:8	C'est pourquoi ainsi parle YHWH Tsevaot : Parce que vous n'avez pas écouté mes paroles,
25:9	voici, j'enverrai et je prendrai toutes les familles du nord, – déclaration de YHWH –, avec Neboukadnetsar, roi de Babel, mon serviteur, et je les ferai venir contre cette terre, contre ses habitants et contre toutes ces nations d'alentour. Je les dévouerai par interdit, je les mettrai en ruine, en opprobre et en désolation éternelle<!--De. 28:37 ; Es. 10:6.-->.
25:10	Je ferai cesser parmi eux les cris de joie et les cris d'allégresse, la voix de l'époux et la voix de l'épouse, le bruit des meules et la lumière des lampes<!--Es. 24:7 ; Ez. 26:13.-->.
25:11	Et toute cette terre deviendra une désolation, des ruines, et ces nations seront asservies au roi de Babel pendant 70 ans<!--Voir Jé. 29:10. Les 70 ans se rapportent également au temps de la domination mondiale babylonienne. Le peuple avait une dette envers YHWH de 70 ans de shabbats (Lé. 25:1-7, 26:34-43 ; 2 Ch. 36:21).-->.

### Jugement sur Babel et les nations impies

25:12	Il arrivera que quand ces 70 ans seront accomplis, je punirai le roi de Babel et cette nation, – déclaration de YHWH –, à cause de leurs iniquités, je punirai la terre des Chaldéens, j'en ferai une désolation éternelle<!--Da. 9:2.-->.
25:13	Je ferai venir sur cette terre toutes mes paroles que j'ai prononcées contre elle, toutes les choses qui sont écrites dans ce livre, ce que Yirmeyah a prophétisé contre toutes les nations.
25:14	Car elles aussi seront asservies à beaucoup de nations et à de grands rois, je leur rendrai selon leurs actions et selon l'œuvre de leurs mains.
25:15	Car ainsi m'a parlé YHWH, l'Elohîm d'Israël : Prends cette coupe du vin de courroux de ma main et fais-la boire à toutes les nations vers lesquelles je t’envoie<!--Ab. 16.-->.
25:16	Elles boiront, elles chancelleront et seront prises de folie face à l'épée que j'enverrai au milieu d'elles.
25:17	Je pris la coupe de la main de YHWH, et je la fis boire à toutes les nations vers lesquelles YHWH m'envoyait :
25:18	À Yeroushalaim et aux villes de Yéhouda, à ses rois et à ses chefs, pour en faire une désolation, des ruines, une moquerie et une malédiction, comme aujourd'hui ;
25:19	à pharaon, roi d'Égypte, à ses serviteurs, à ses chefs, et à tout son peuple ;
25:20	à tout le peuple mélangé<!--Vient de l'hébreu « ereb » qui signifie : « mélangé », « entrelacé », « matériel tricoté », « mélange », « personnes mixtes », « société mixte ».-->, à tous les rois de la terre d'Outs, à tous les rois de la terre des Philistins, à Askalon, à Gaza, à Ékron, et au reste d'Asdod ;
25:21	à Édom, à Moab, et aux fils d'Ammon ;
25:22	à tous les rois de Tyr, à tous les rois de Sidon, et aux rois des îles qui sont au-delà de la mer ;
25:23	à Dedan, à Théma, à Bouz, et à tous ceux qui se coupent les coins de la barbe ;
25:24	à tous les rois des Arabes, et à tous les rois du peuple mélangé qui habitent dans le désert ;
25:25	à tous les rois de Zimri, à tous les rois d'Éylam, et à tous les rois de Médie ;
25:26	à tous les rois du nord, tant proches qu'éloignés, l'homme à son frère, et à tous les royaumes du monde qui sont sur les faces du sol. Et le roi de Sheshak boira après eux.
25:27	Et tu leur diras : Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Buvez et soyez enivrés, vomissez et tombez sans vous relever, face à l'épée que j'enverrai parmi vous !
25:28	Il arrivera que, s'ils refusent de prendre la coupe de ta main pour boire, tu leur diras : Ainsi parle YHWH Tsevaot : Vous en boirez ! Vous en boirez !
25:29	Car voici, j'envoie le malheur en commençant par la ville sur laquelle mon Nom est invoqué et vous, seriez-vous exempts du châtiment ? Exempts du châtiment ? Vous ne resterez pas exempts du châtiment, car je m'en vais appeler l'épée sur tous les habitants de la Terre, – déclaration de YHWH Tsevaot<!--1 Pi. 4:17-18.-->.
25:30	Tu prophétiseras contre eux toutes ces paroles-là, et tu leur diras : YHWH rugira d'en haut, il fera entendre sa voix de la demeure de sa sainteté ! Il rugira, il rugira contre son agréable demeure, il répondra à tous les habitants de la Terre en criant comme ceux qui foulent au pressoir<!--Joë. 4:16 ; Am. 1:2.-->.
25:31	Le vacarme en est arrivé jusqu'à l'extrémité de la Terre, car YHWH est en procès avec les nations. Il juge toute chair, il livre les méchants à l'épée, – déclaration de YHWH.
25:32	Ainsi parle YHWH Tsevaot : Voici, le malheur sort de nation en nation, et une grande tempête se réveillera des parties extrêmes de la Terre.
25:33	En ce jour-là, ceux qui auront été blessés mortellement par YHWH seront depuis un bout de la Terre jusqu’à l’autre bout de la Terre. Ils ne seront ni pleurés, ni recueillis, ni enterrés, mais ils seront comme du fumier sur les faces du sol.
25:34	Bergers, hurlez et criez ! Et vous, les majestueux du troupeau, roulez-vous dans la cendre ! Car le jour du massacre et de la dispersion est accompli pour vous. Vous tomberez comme un vase précieux.
25:35	Le refuge des bergers sera détruit, les majestueux du troupeau n'auront plus aucun moyen d'échapper.
25:36	Clameur et cri de détresse des bergers, gémissement des majestueux de troupeau ! Car YHWH dévaste leur pâturage.
25:37	Les pâturages de la paix seront abattus à cause de l'ardeur de la colère de YHWH.
25:38	Il a abandonné sa tanière comme un jeune lion. Car leur terre deviendra un lieu dévasté à cause de l'ardeur de l'oppresseur, à cause de l'ardeur de sa colère.

## Chapitre 26

### Avertissement dans le parvis du temple

26:1	Au commencement du règne de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda, cette parole vint à Yirmeyah par YHWH, en disant :
26:2	Ainsi parle YHWH : Tiens-toi debout dans le parvis de la maison de YHWH, et prononce à toutes les villes de Yéhouda qui viennent pour se prosterner dans la maison de YHWH toutes les paroles que je t'ordonne de leur dire. Ne retranche pas une parole.
26:3	Peut-être qu'ils écouteront et qu'ils se détourneront chacun de sa mauvaise voie. Alors je me repentirai du mal que j'avais pensé leur faire à cause de la méchanceté de leurs actions.
26:4	Tu leur diras : Ainsi parle YHWH : Si vous ne m'écoutez pas pour marcher selon ma torah que je vous ai proposée,
26:5	pour obéir aux paroles des prophètes, mes serviteurs, que je vous envoie, que je vous ai envoyés de bonne heure et que vous n'avez pas écoutés,
26:6	je mettrai cette maison dans le même état que Shiyloh, et je livrerai cette ville à la malédiction, à toutes les nations de la Terre.
26:7	Or les prêtres, les prophètes, et tout le peuple, entendirent Yirmeyah déclarer ces paroles dans la maison de YHWH.

### Yirmeyah menacé de mort par les prêtres et les prophètes

26:8	Il arriva qu'aussitôt que Yirmeyah eut achevé de déclarer tout ce que YHWH lui avait ordonné de dire à tout le peuple, les prêtres, les prophètes, et tout le peuple, le saisirent en disant : Tu mourras, tu mourras<!--Ge. 2:17.--> !
26:9	Pourquoi as-tu prophétisé au Nom de YHWH, en disant : Cette maison sera comme Shiyloh, et cette ville sera tellement déserte que personne n'y habitera ? Et tout le peuple se rassembla autour de Yirmeyah dans la maison de YHWH.
26:10	Les chefs de Yéhouda ayant entendu toutes ces choses, montèrent de la maison du roi à la maison de YHWH, et s'assirent à l'entrée de la porte Neuve de la maison de YHWH.
26:11	Les prêtres et les prophètes parlèrent aux chefs et à tout le peuple, en disant : Cet homme mérite d'être condamné à mort, car il a prophétisé contre cette ville, comme vous l'avez entendu de vos oreilles.
26:12	Yirmeyah parla à tous les chefs et à tout le peuple, en disant : YHWH m'a envoyé pour prophétiser contre cette maison et contre cette ville toutes les paroles que vous avez entendues.
26:13	Maintenant, rendez bonnes vos voies et vos actions, écoutez la voix de YHWH, votre Elohîm, et YHWH se repentira du mal qu'il a prononcé contre vous.
26:14	Pour moi, me voici entre vos mains : traitez-moi comme il semblera bon et juste à vos yeux.
26:15	Mais sachez-le, sachez-le : Oui, si vous me faites mourir, oui, vous mettrez du sang innocent sur vous, sur cette ville et sur ses habitants. Oui, en vérité YHWH m'a envoyé vers vous pour prononcer à vos oreilles toutes ces paroles.
26:16	Les chefs et tout le peuple dirent aux prêtres et aux prophètes : Cet homme ne mérite pas d'être condamné à mort, Oui, il nous a parlé au Nom de YHWH, notre Elohîm.
26:17	Quelques-uns des anciens de la terre se levèrent et parlèrent à toute l'assemblée du peuple, en disant :
26:18	Miykayah, de Morésheth, prophétisait aux jours d'Hizqiyah, roi de Yéhouda, et il parlait à tout le peuple de Yéhouda, en disant : Ainsi parle YHWH Tsevaot : Sion sera labourée comme un champ, Yeroushalaim sera réduite en un monceau de pierres, et la montagne du temple en des hauts lieux d'une forêt<!--Mi. 1:1, 3:12.-->.
26:19	Hizqiyah, roi de Yéhouda et tous ceux de Yéhouda l'ont-ils fait mourir, fait mourir ? Hizqiyah ne craignit-il pas YHWH ? N'implora-t-il pas YHWH ? Et YHWH se repentit du mal qu'il avait prononcé contre eux. Et nous, nous ferions un grand mal contre nos âmes<!--2 Ch. 32:26.--> !
26:20	Il y eut aussi un homme qui prophétisait au Nom de YHWH : Ouriyah, fils de Shema’yah, de Qiryath-Yéarim. Il prophétisa contre cette même ville et contre cette même terre, de la même manière que Yirmeyah.
26:21	Et le roi Yehoyaqiym, tous ses vaillants hommes, et tous ses chefs entendirent ses paroles, et le roi chercha à le faire mourir. Ouriyah, qui en fut informé, eut peur, prit la fuite, et se retira en Égypte.
26:22	Et le roi Yehoyaqiym envoya des hommes en Égypte : Elnathan, fils d'Acbor, et quelques hommes avec lui, qui allèrent en Égypte.
26:23	Et ils firent sortir d'Égypte Ouriyah et l'amenèrent au roi Yehoyaqiym qui le frappa avec l'épée et jeta son cadavre sur les sépulcres des fils du peuple.
26:24	Toutefois la main d'Achikam, fils de Shaphan, fut avec Yirmeyah, afin de ne pas le livrer entre les mains du peuple pour le faire mourir.

## Chapitre 27

### Prophétie : Les nations seront asservies à Neboukadnetsar

27:1	Au commencement du règne de Yehoyaqiym<!--Il est probable que ce soit une erreur de copiste, car bien que l'hébreu dise « Yehoyaqiym », le contexte se rapporte à Tsidqiyah (Sédécias). Voir Jé. 27:3, 27:12, 27:20, 28:1.-->, fils de Yoshiyah, roi de Yéhouda, cette parole apparut à Yirmeyah de la part de YHWH, en disant :
27:2	Ainsi m'a parlé YHWH : Fais-toi des liens et des jougs, et mets-les sur ton cou<!--Ez. 7:23.-->.
27:3	Envoie-les au roi d'Édom, au roi de Moab, au roi des fils d'Ammon, au roi de Tyr et au roi de Sidon, par les mains des messagers qui sont venus à Yeroushalaim vers Tsidqiyah, roi de Yéhouda.
27:4	Tu leur donneras mes ordres pour leurs seigneurs, en disant : Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Vous parlerez ainsi à vos seigneurs :
27:5	J'ai fait la Terre, les humains et les bêtes qui sont sur la Terre, par ma grande force et par mon bras étendu, et je les donne à qui il est juste à mes yeux<!--De. 32:8.-->.
27:6	Maintenant j'ai livré toutes ces terres entre les mains de Neboukadnetsar, roi de Babel, mon serviteur. Je lui ai même donné les bêtes des champs pour qu'elles le servent<!--Da. 2:38.-->.
27:7	Et toutes les nations lui seront asservies, à lui, à son fils, et au fils de son fils, jusqu'à ce que le temps de sa terre vienne aussi et que de nombreuses nations et de grands rois l'asservissent.
27:8	Et il arrivera que la nation ou le royaume qui ne le servira pas, lui, Neboukadnetsar, roi de Babel, et qui ne mettra pas son cou sous le joug du roi de Babel, je punirai cette nation par l'épée, par la famine et par la peste, – déclaration de YHWH –, jusqu'à ce que je les aie consumés par sa main.
27:9	Vous, n'écoutez pas vos prophètes, ni vos devins, ni vos rêves, ni vos enchanteurs, ni vos sorciers, qui vous parlent, en disant : Vous ne serez pas asservis au roi de Babel.
27:10	Car ils vous prophétisent le mensonge pour vous faire aller loin de votre sol, afin que je vous bannisse et que vous périssiez.
27:11	Mais la nation qui fera venir son cou sous le joug du roi de Babel et qui le servira, je la laisserai sur son sol, – déclaration de YHWH –, pour qu'elle le cultive et qu'elle y demeure.
27:12	Puis j'ai parlé à Tsidqiyah, roi de Yéhouda, selon toutes ces paroles, en disant : Faites venir vos cous sous le joug du roi de Babel et servez-le, lui et son peuple, et vous vivrez.
27:13	Pourquoi mourriez-vous, toi et ton peuple, par l'épée, par la famine et par la peste, selon ce que YHWH a dit à la nation qui ne servira pas le roi de Babel ?
27:14	N'écoutez pas les paroles des prophètes qui vous parlent en disant : Vous ne serez pas asservis au roi de Babel ! Car ils vous prophétisent le mensonge.
27:15	Car je ne les ai pas envoyés, – déclaration de YHWH –, et ils vous prophétisent faussement en mon Nom, afin que je vous bannisse et que vous périssiez, vous et les prophètes qui vous prophétisent.
27:16	J'ai parlé aussi aux prêtres et à tout ce peuple, en disant : Ainsi parle YHWH : N'écoutez pas les paroles de vos prophètes qui vous prophétisent, en disant : Voici, les ustensiles de la maison de YHWH seront bientôt rapportés de Babel ! Car ils vous prophétisent le mensonge.
27:17	Ne les écoutez pas, rendez-vous sujets au roi de Babel, et vous vivrez. Pourquoi cette ville deviendrait-elle une désolation ?
27:18	S'ils sont prophètes et si la parole de YHWH est en eux, s’il vous plaît, qu'ils intercèdent auprès de YHWH Tsevaot, afin que les ustensiles qui restent dans la maison de YHWH, dans la maison du roi de Yéhouda, et dans Yeroushalaim, n'aillent pas à Babel.
27:19	Car ainsi parle YHWH Tsevaot au sujet des colonnes, de la mer, des bases, et des autres ustensiles qui sont restés dans cette ville,
27:20	que Neboukadnetsar, roi de Babel, n'a pas emportés, quand il a transporté de Yeroushalaim à Babel, Yekonyah, fils de Yehoyaqiym, roi de Yéhouda, et tous les nobles de Yéhouda et de Yeroushalaim.
27:21	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël, au sujet des ustensiles qui restent dans la maison de YHWH, dans la maison du roi de Yéhouda et dans Yeroushalaim :
27:22	Ils seront emportés à Babel, et ils y demeureront jusqu'au jour où je les visiterai, – déclaration de YHWH –, et où je les ferai monter et revenir dans ce lieu<!--2 R. 24:14-15 ; Esd. 1:7-11 ; 2 Ch. 25:13-16, 36:18.-->.

## Chapitre 28

### Chananyah meurt suite à sa prophétie mensongère

28:1	Il arriva, en cette même année, au commencement du règne de Tsidqiyah, roi de Yéhouda, au cinquième mois de la quatrième année, que Chananyah, fils d'Azzour, prophète de Gabaon, me parla dans la maison de YHWH, aux yeux des prêtres et de tout le peuple, en disant :
28:2	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Je romps le joug du roi de Babel !
28:3	Dans deux ans accomplis, je ferai revenir en ce lieu tous les ustensiles de la maison de YHWH que Neboukadnetsar, roi de Babel, a pris de ce lieu pour les faire venir à Babel.
28:4	Et je ferai revenir en ce lieu, – déclaration de YHWH –, Yekonyah, fils de Yehoyaqiym, roi de Yéhouda, et tous les captifs de Yéhouda qui sont allés à Babel, car je romprai le joug du roi de Babel.
28:5	Yirmeyah, le prophète, parla à Chananyah, le prophète, aux yeux des prêtres, et aux yeux de tout le peuple qui se tenait dans la maison de YHWH.
28:6	Yirmeyah, le prophète, dit : Amen ! Que YHWH fasse ainsi ! Que YHWH accomplisse les paroles que tu as prophétisées, et qu'il fasse revenir de Babel dans ce lieu-ci les ustensiles de la maison de YHWH, et tous les captifs de Babel !
28:7	Mais écoute, s’il te plaît, cette parole que je prononce à tes oreilles et aux oreilles de tout le peuple :
28:8	Les prophètes qui ont été avant moi et avant toi, dès les temps anciens, ont prophétisé contre beaucoup de terres et de grands royaumes, la guerre, le malheur et la peste.
28:9	Si un prophète prophétise la paix, c'est quand arrivera la parole de ce prophète qu'il sera reconnu comme véritablement envoyé par YHWH.
28:10	Chananyah, le prophète, prit le joug de dessus le cou de Yirmeyah, le prophète, et le rompit.
28:11	Chananyah parla aux yeux de tout le peuple, en disant : Ainsi parle YHWH : C'est ainsi que dans deux années, je romprai le joug de Neboukadnetsar, roi de Babel, de dessus le cou de toutes les nations. Et Yirmeyah, le prophète, s'en alla son chemin.
28:12	La parole de YHWH apparut à Yirmeyah, après que Chananyah, le prophète, eut rompu le joug de dessus le cou de Yirmeyah, le prophète, en disant :
28:13	Va, et parle à Chananyah, en disant : Ainsi parle YHWH : Tu as rompu les jougs de bois, et tu auras à la place des jougs de fer.
28:14	Car ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Je mets un joug de fer sur le cou de toutes ces nations, afin qu'elles servent Neboukadnetsar, roi de Babel. Elles le serviront et je lui ai livré même les bêtes des champs<!--De. 28:48.-->.
28:15	Yirmeyah, le prophète, dit à Chananyah, le prophète : Écoute, s’il te plaît, Chananyah ! YHWH ne t'a pas envoyé et tu as fait que ce peuple se confie au mensonge<!--Ez. 13:3-9.-->.
28:16	C'est pourquoi ainsi parle YHWH : Voici, je te chasse des faces du sol. Tu mourras cette année, car tu as parlé d'apostasie contre YHWH.
28:17	Chananyah le prophète mourut cette année-là, au septième mois.

## Chapitre 29

### Message à l'attention des Juifs captifs à Babel

29:1	Voici les paroles de la lettre que Yirmeyah, le prophète, envoya de Yeroushalaim au reste des anciens en captivité, aux prêtres et aux prophètes, et à tout le peuple, que Neboukadnetsar avait emmenés en exil de Yeroushalaim à Babel,
29:2	après que le roi Yekonyah fut sorti de Yeroushalaim, avec la reine, les eunuques, les chefs de Yéhouda et de Yeroushalaim, les artisans et les serruriers<!--2 R. 24:12.-->.
29:3	C'est par la main d'El`asah, fils de Shaphan, et Gemaryah, fils de Chilqiyah, que Tsidqiyah, roi de Yéhouda, l'envoya à Babel vers Neboukadnetsar, roi de Babel. Elle disait :
29:4	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël, à tous les exilés que j'ai emmenés en exil de Yeroushalaim à Babel.
29:5	Bâtissez des maisons et habitez-les. Plantez des jardins et mangez-en les fruits.
29:6	Prenez des femmes et engendrez des fils et des filles. Prenez aussi des femmes pour vos fils et donnez vos filles à des hommes, afin qu'elles enfantent des fils et des filles ! Multipliez-vous là où vous êtes et ne diminuez pas !
29:7	Cherchez la paix de la ville où je vous ai emmenés en exil et priez YHWH pour elle, parce que dans sa paix vous aurez la paix.
29:8	Car ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Que vos prophètes qui sont au milieu de vous, et vos devins, ne vous séduisent pas, et n'écoutez pas vos rêves que vous rêvez<!--Tous les rêves ne viennent pas toujours du Seigneur. Les visions et les rêves doivent être en accord avec la parole d'Elohîm.-->.
29:9	Parce qu'ils vous prophétisent faussement en mon Nom. Je ne les ai pas envoyés, – déclaration de YHWH.
29:10	Car ainsi parle YHWH : Lorsque les 70 ans seront accomplis pour Babel, je vous visiterai, et j'accomplirai ma bonne parole à votre égard, pour vous faire revenir dans ce lieu.
29:11	Car je sais, moi-même, les pensées que moi-même je pense pour vous, – déclaration de YHWH –, pensées de paix et non de malheur, pour vous donner une fin telle que vous espérez<!--Jos. 1:8.-->.
29:12	Vous m'appellerez et vous partirez, vous me prierez et je vous écouterai<!--Os. 5:15.-->.
29:13	Vous me chercherez et vous me trouverez, car vous me consulterez de tout votre cœur<!--Mt. 7:7.-->.
29:14	Je serai trouvé par vous, – déclaration de YHWH –, je ramènerai vos captifs. Je vous rassemblerai de toutes les nations et de tous les lieux où je vous ai bannis, – déclaration de YHWH –, et je vous ramènerai dans le lieu d'où je vous ai exilés.
29:15	Car vous dites : YHWH nous a suscité des prophètes à Babel !
29:16	Oui, ainsi parle YHWH sur le roi qui est assis sur le trône de David, sur tout le peuple qui habite dans cette ville, sur vos frères qui ne sont pas partis avec vous en exil,
29:17	ainsi parle YHWH Tsevaot : Voici que j’envoie contre eux l'épée, la famine et la peste, et je les rendrai pareils aux figues affreuses qui ne peuvent être mangées à cause de leur mauvaise qualité.
29:18	Je les poursuivrai par l'épée, par la famine et par la peste, je les rendrai un objet de terreur pour tous les royaumes de la Terre, une malédiction, une horreur, une moquerie et une insulte parmi toutes les nations où je les bannirai<!--De. 28:25-37.-->,
29:19	parce qu'ils n'ont pas entendu mes paroles, – déclaration de YHWH –, eux à qui j'ai envoyé mes serviteurs, les prophètes, à qui je les ai envoyés de bonne heure, mais ils n'ont pas entendu, – déclaration de YHWH.
29:20	Vous tous, écoutez la parole de YHWH, vous les exilés que j'ai envoyés de Yeroushalaim à Babel !
29:21	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël sur Achab, fils de Qolayah, et sur Tsidqiyah, fils de Ma`aseyah, qui vous prophétisent faussement en mon Nom : Voici, je vais les livrer entre les mains de Neboukadnetsar, roi de Babel, et il les frappera sous vos yeux.
29:22	On prendra d'eux une malédiction parmi tous les captifs de Yéhouda qui sont à Babel, en disant : Que YHWH te traite comme Tsidqiyah et comme Achab, que le roi de Babel a fait rôtir au feu !
29:23	Parce qu'ils ont commis une infamie en Israël, en commettant l’adultère avec les femmes de leurs prochains et en prononçant en mon Nom des paroles de mensonge, ce que je ne leur avais pas ordonné. C’est moi-même qui le sais, et j’en suis le témoin, – déclaration de YHWH.
29:24	À Shema’yah, le Néchélamite, tu parleras en disant :
29:25	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Tu as envoyé en ton nom une lettre à tout le peuple de Yeroushalaim, à Tsephanyah, fils de Ma`aseyah, le prêtre, et à tous les prêtres, en disant :
29:26	YHWH t'a établi prêtre à la place de Yehoyada, le prêtre, afin qu'il y ait dans la maison de YHWH des commissaires pour surveiller tout homme qui est fou et se donne pour prophète, et afin que tu le mettes en prison et dans les fers.
29:27	Et maintenant, pourquoi n'as-tu pas réprimandé Yirmeyah d'Anathoth, qui prophétise parmi vous ?
29:28	Car à cause de cela il nous a envoyé un message à Babel en disant : Ce sera long ! Bâtissez des maisons, et habitez-les, plantez des jardins, et mangez-en les fruits !
29:29	Or Tsephanyah, le prêtre, lut cette lettre aux oreilles de Yirmeyah, le prophète.
29:30	La parole de YHWH apparut à Yirmeyah, en disant :
29:31	Envoie dire à tous les exilés : Ainsi parle YHWH sur Shema’yah, le Néchélamite : Parce que Shema’yah vous a prophétisé, et que moi je ne l’ai pas envoyé, et qu’il vous a fait vous confier au mensonge,
29:32	c’est pourquoi ainsi : Voici que je punirai Shema’yah, Néchélamite et sa postérité. Il n’aura pas d’homme qui habitera au milieu de ce peuple, et il ne verra pas le bien que je ferai à mon peuple, – déclaration de YHWH, - car il a parlé d'apostasie contre YHWH.

## Chapitre 30

### Le jour de YHWH

30:1	La parole qui apparut à Yirmeyah de la part de YHWH, en disant :
30:2	Ainsi parle YHWH, l'Elohîm d'Israël : Écris pour toi dans un livre toutes les paroles que je t'ai déclarées.
30:3	Car voici, les jours viennent, – déclaration de YHWH –, où je ramènerai les captifs de mon peuple d'Israël et de Yéhouda, dit YHWH. Je les ramènerai sur la terre que j'ai donnée à leurs pères, et ils en prendront possession.
30:4	Et voici les paroles que YHWH a déclarées sur Israël et Yéhouda.
30:5	Ainsi parle YHWH : Nous entendons des cris d'effroi et de terreur, il n'y a pas de paix.
30:6	Informez-vous, s’il vous plaît, et voyez si un mâle enfante ! Pourquoi vois-je les hommes forts les mains sur leurs reins, comme une femme qui enfante ? Toutes les faces sont devenues pâles !
30:7	Malheur ! Oui, il sera grand, ce jour ! Il n’y en aura pas eu de semblable. Un temps de détresse pour Yaacov, mais il en sera sauvé<!--Joë. 2:11 ; So. 1:15 ; Da. 12:1 ; Mt. 24:21.-->.
30:8	Il arrivera en ce jour-là, – déclaration de YHWH Tsevaot –, que je briserai son joug de dessus ton cou, je romprai tes liens, et les étrangers ne t'asserviront plus.
30:9	Ils serviront YHWH, leur Elohîm, et David, leur roi, que j’établirai<!--Ez. 34:23-24.--> pour eux.
30:10	Toi, mon serviteur Yaacov, n'aie pas peur, – déclaration de YHWH. Ne t'épouvante pas, Israël ! Car, voici, je te délivrerai de la terre éloignée, et ta postérité de la terre de leur captivité. Yaacov reviendra, il sera en repos et sera en paix, et il n'y aura personne qui lui fasse peur<!--Es. 41:13.-->.
30:11	Car je suis avec toi pour te sauver, – déclaration de YHWH. Car j'ordonnerai l'anéantissement de toutes les nations parmi lesquelles je t'ai dispersé, mais quant à toi, je n'ordonnerai pas ton anéantissement. Je te châtierai avec équité, je ne t'innocenterai pas, je ne t'innocenterai pas<!--Es. 27:7-8.-->.
30:12	Ainsi parle YHWH : Ta fracture est incurable, ta plaie est très douloureuse<!--Mi. 1:9 ; 2 Ch. 36:16.-->.
30:13	Il n'y a personne qui défende ta cause, pour ta blessure. Pour toi, il n'y a ni médicament ni guérison<!--De la nouvelle chair et de la peau qui se reforment sur la blessure.-->.
30:14	Tous tes amoureux t'oublient, ils ne te cherchent plus, car je t’avais frappé, comme un ennemi, d’un châtiment cruel, à cause de la multitude de tes iniquités. Tes péchés sont devenus nombreux<!--La. 1:2.-->.
30:15	Pourquoi cries-tu à cause de ta plaie ? Ta fracture est incurable, je t'ai fait ces choses à cause de la grandeur de tes iniquités. Tes péchés sont devenus nombreux.
30:16	C’est pourquoi tous ceux qui te dévorent seront dévorés, et tous ceux qui te mettent dans la détresse, iront en captivité. Ceux qui te dépouillent seront dépouillés, et je livrerai au pillage tous ceux qui te pillent<!--Es. 41:11 ; Ab. 15.-->.
30:17	Oui, je ferai monter la guérison pour toi et je te guérirai de tes blessures, – déclaration de YHWH. Oui, ils t'appellent « la bannie », cette « Sion que personne ne recherche ».

### Israël délivré par YHWH

30:18	Ainsi parle YHWH : Voici, je ramène les captifs des tentes de Yaacov. J'ai compassion de ses tabernacles : la ville sera rebâtie sur le monceau de ses ruines et le palais sera habité selon la coutume<!--Le mot hébreu signifie aussi « jugement », « ordonnance », « lieu ».-->.
30:19	Il en sortira des louanges et la voix des joueurs. Je les multiplierai, et ils ne diminueront pas. Je les honorerai, et ils ne seront plus insignifiants.
30:20	Ses fils seront comme autrefois, son assemblée sera affermie devant moi, et je punirai tous ceux qui l'oppriment.
30:21	Sa majesté sera issue de lui, son dominateur sortira du milieu de lui. Je le ferai approcher, et il viendra vers moi. Qui mettrait en gage<!--Vient d'un mot hébreu qui signifie aussi « échanger », « hypothèque », « engagement », « occuper », « entreprendre », « donner des garanties », « être en sécurité ».--> son cœur pour s'approcher de moi ? – déclaration de YHWH.
30:22	Vous deviendrez mon peuple, et moi je deviendrai votre Elohîm.
30:23	Voici la tempête de YHWH, le courroux est sorti. C'est une tempête qui s'entasse, elle tournoie sur la tête des méchants.
30:24	La colère de la narine de YHWH ne se détournera pas, jusqu'à ce qu'il ait exécuté, accompli les desseins de son cœur. Vous le comprendrez dans les derniers jours.

## Chapitre 31

### Communion retrouvée : La paix et la joie

31:1	En ce temps-là, – déclaration de YHWH –, je deviendrai l'Elohîm de toutes les familles d'Israël, et ils deviendront mon peuple.
31:2	Ainsi parle YHWH : Le peuple survivant à l'épée a trouvé grâce dans le désert. Israël marche vers son lieu de repos.
31:3	De loin YHWH s’est fait voir à moi : Je t'aime d'un amour éternel, c'est pourquoi j'ai prolongé ma bonté envers toi.
31:4	Je te bâtirai encore, et tu seras bâtie, vierge d'Israël ! Tu te pareras encore de tes tambours et tu sortiras avec des danses des joueurs.
31:5	Tu planteras encore des vignes sur les montagnes de Samarie. Les planteurs planteront et commenceront à en profiter<!--Es. 65:21.-->.
31:6	Car il y a un jour où les gardes crieront sur la montagne d'Éphraïm : Levez-vous, et montons à Sion, vers YHWH, notre Elohîm !
31:7	Car ainsi parle YHWH : Poussez des cris de joie, avec gaieté pour Yaacov, hennissez en tête des nations ! Faites-le entendre, chantez des louanges, et dites : YHWH, sauve ton peuple, le reste d'Israël !
31:8	Voici, je vais les faire venir de la terre du nord, je les rassemblerai des extrémités de la Terre. L'aveugle et le boiteux, la femme enceinte et celle qui enfante seront ensemble parmi eux. Une grande assemblée qui reviendra ici.
31:9	Ils étaient partis en pleurant, mais je les ferai retourner avec des supplications. Je les conduirai aux torrents d'eaux, et par un droit chemin, où ils ne broncheront pas. Car je deviens un Père pour Israël, et Éphraïm est mon premier-né<!--Ex. 4:22.-->.
31:10	Nations, écoutez la parole de YHWH, et annoncez-la aux îles éloignées ! Dites : Celui qui a dispersé Israël le rassemblera, et il le gardera comme un berger son troupeau.
31:11	Car YHWH paie la rançon pour Yaacov, il le rachète de la main d’un plus fort que lui.
31:12	Ils viendront et pousseront des cris de joie sur les hauteurs de Sion. Ils afflueront vers les biens de YHWH : le blé, le vin nouveau, l'huile, et vers les fils des brebis et des bœufs. Leur âme deviendra comme un jardin arrosé, et ils ne continueront plus de languir<!--Es. 61:11.-->.
31:13	Alors la vierge se réjouira à la danse, les jeunes hommes et les anciens ensemble. Je changerai leur deuil en joie, et je les consolerai, je les réjouirai après leur douleur.
31:14	Je rassasierai aussi de graisse l'âme des prêtres, et mon peuple sera rassasié de mes biens, – déclaration de YHWH.
31:15	Ainsi parle YHWH : On entend des cris à Ramah, des lamentations, des larmes amères : Rachel pleure sur ses fils. Elle refuse d’être consolée pour ses fils, parce qu’ils ne sont plus<!--Mt. 2:17-18.-->.
31:16	Ainsi parle YHWH : Retiens ta voix de pleurer et tes yeux de verser des larmes, car ton œuvre aura son salaire, – déclaration de YHWH. Ils reviendront des terres de l'ennemi.
31:17	Il y a de l'espérance pour tes derniers jours, – déclaration de YHWH –, et tes fils reviendront dans leur territoire.
31:18	J’entends, j’entends Éphraïm qui se lamente : Tu m'as châtié, et j'ai été châtié comme un veau qui n'est pas dressé. Fais-moi revenir et je reviendrai, car toi, YHWH, tu es mon Elohîm<!--Ps. 119:67-71.-->.
31:19	Car après m'être détourné, je me repens. Après l’avoir su, je frappe sur ma cuisse. Je suis honteux et confus, car je porte l'insulte de ma jeunesse<!--Ez. 21:17.-->.
31:20	Éphraïm est pour moi un fils très précieux, un fils qui fait mes délices. Car plus je parle de lui, plus je me souviens, je me souviens encore de lui. C'est pourquoi mes entrailles sont agitées à cause de lui : J'aurai pitié de lui, j'aurai pitié de lui – déclaration de YHWH<!--Es. 5:7.-->.
31:21	Dresse-toi des signes sur les chemins, place des poteaux, prends garde à la route, au chemin par lequel tu es venue... Reviens, vierge d'Israël, reviens dans tes villes !
31:22	Jusqu'à quand seras-tu errante, fille apostate ? Car YHWH crée une chose nouvelle sur la Terre : La femme tournera autour de l'homme fort.
31:23	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : On dira encore cette parole sur la terre de Yéhouda et dans ses villes, quand j'aurai ramené leurs captifs : Que YHWH te bénisse, agréable demeure de la justice, montagne de sainteté !
31:24	Yéhouda et toutes ses villes ensemble, les laboureurs et ceux qui conduisent les troupeaux, y habiteront.
31:25	Car j'abreuverai l'âme épuisée par le travail et je remplirai toute âme languissante.
31:26	C'est pourquoi je me suis réveillé et j'ai regardé. Mon sommeil m'avait été agréable.

### Promesse d'une nouvelle alliance

31:27	Voici, les jours viennent, – déclaration de YHWH –, où j'ensemencerai la maison d'Israël et la maison de Yéhouda d'une semence d'hommes et d'une semence de bêtes.
31:28	Il arrivera que comme j'ai veillé sur eux pour arracher et abattre, pour détruire, pour faire périr et pour faire du mal, ainsi je veillerai sur eux pour bâtir et pour planter, – déclaration de YHWH.
31:29	En ces jours-là, on ne dira plus : Les pères ont mangé des raisins verts, et les dents des fils en ont été agacées<!--Ez. 18:2-3.-->.
31:30	Car chaque homme mourra pour son iniquité. Tout humain qui mangera des raisins verts, ses dents en seront agacées.
31:31	Voici, les jours viennent, – déclaration de YHWH –, où je traiterai une nouvelle alliance<!--Il s'agit de l'alliance du sang que Yéhoshoua (Jésus), notre Mashiah (Messie), est venu inaugurer en prenant sur lui tous nos péchés et en mourant sur la croix à notre place (Mt. 26:27-29 ; Hé. 8:7-13).--> avec la maison d'Israël et avec la maison de Yéhouda,
31:32	non comme l'alliance que je traitai avec leurs pères, le jour où je les pris par la main, pour les faire sortir de la terre d'Égypte. Eux, ils ont rompu mon alliance, et moi, j'étais leur époux<!--Es. 54:5.-->, – déclaration de YHWH.
31:33	Car voici l'alliance que je traiterai avec la maison d'Israël après ces jours-là – déclaration de YHWH : je mettrai ma torah dans leurs entrailles, je l'écrirai dans leur cœur, je deviendrai leur Elohîm et ils deviendront mon peuple.
31:34	Un homme n'enseignera plus son prochain, ni un homme son frère, en disant : Connaissez YHWH ! Car tous me connaîtront, depuis le plus petit jusqu'au plus grand, – déclaration de YHWH. En effet, je pardonnerai leur iniquité et je ne me souviendrai plus de leur péché<!--Es. 54:13 ; Ha. 2:14 ; Jn. 6:45.-->.
31:35	Ainsi parle YHWH, qui a donné le soleil pour la lumière du jour, les ordonnances<!--Vient de l'hébreu « chuqqah » qui signifie : « statut », « quelque chose de prescrit ». Voir Je. 33:25.--> de la lune et des étoiles pour la lumière de la nuit, qui remue la mer, et qui fait gronder ses flots, lui dont le Nom est YHWH Tsevaot<!--Ge. 1:16 ; Es. 51:15.--> :
31:36	Si ces lois<!--Vient de l'hébreu « choq » qui signifie : « statut », « ordonnance », « décret », « lois ».--> se retiraient face à moi, – déclaration de YHWH –, la postérité d'Israël aussi cessera d'être à jamais une nation en face de moi.
31:37	Ainsi parle YHWH : Si les cieux en haut peuvent être mesurés, si les fondements de la Terre en bas peuvent être sondés, alors je rejetterai toute la postérité d'Israël, à cause de toutes les choses qu'ils ont faites, – déclaration de YHWH.
31:38	Voici, les jours viennent, – déclaration de YHWH –, où la ville sera rebâtie pour YHWH, depuis la tour de Hananeel, jusqu'à la porte de l'angle<!--Za. 14:10 ; Né. 3:1 ; 2 Ch. 26:9.-->.
31:39	Le cordeau à mesurer sortira encore vis-à-vis d'elle jusqu'à la colline de Gareb et fera le tour jusqu'à Goath.
31:40	Et toute la vallée des cadavres et des cendres, et tous les champs jusqu'au torrent de Cédron, jusqu'à l'angle de la porte des chevaux à l'est, seront consacrés à YHWH, et ne seront plus jamais arrachés ni détruits.

## Chapitre 32

### Le champ de Hanameel : La pérennité d'Israël

32:1	La parole qui apparut à Yirmeyah de la part de YHWH, la dixième année de Tsidqiyah, roi de Yéhouda. C'était la dix-huitième année de Neboukadnetsar.
32:2	L'armée du roi de Babel assiégeait alors Yeroushalaim, et Yirmeyah le prophète était enfermé dans la cour de la prison qui était dans la maison du roi de Yéhouda.
32:3	En effet, Tsidqiyah, roi de Yéhouda, l'avait fait enfermer en lui disant : Pourquoi prophétises-tu, en disant : Ainsi parle YHWH : Voici, je vais livrer cette ville entre les mains du roi de Babel, et il la prendra.
32:4	Et Tsidqiyah, roi de Yéhouda, n'échappera pas aux mains des Chaldéens, mais il sera livré, livré entre les mains du roi de Babel, il lui parlera bouche à bouche et ses yeux verront les yeux de ce roi.
32:5	Il conduira Tsidqiyah à Babel, où il demeurera jusqu'à ce que je le visite, – déclaration de YHWH. Si vous combattez contre les Chaldéens, vous ne prospérerez pas.
32:6	Yirmeyah dit : La parole de YHWH m'est apparue, en disant :
32:7	Voici Hanameel, fils de Shalloum, ton oncle, qui vient vers toi pour te dire : Achète pour toi mon champ qui est à Anathoth, car tu as le droit de rachat pour l’acheter<!--Lé. 25:48 ; Ru. 3:12.-->.
32:8	Hanameel, fils de mon oncle, vint à moi, selon la parole de YHWH, dans la cour de la prison et me dit : S’il te plaît, achète mon champ qui est à Anathoth, en terre de Benyamin, car tu as le droit d'héritage, à toi le rachat, achète-le pour toi ! Je sus que c'était la parole de YHWH.
32:9	J'achetai de Hanameel, fils de mon oncle, le champ d'Anathoth et je lui pesai l'argent : 17 sicles d'argent.
32:10	J'écrivis la lettre et je la scellai, je fis témoigner des témoins, et je pesai l'argent dans une balance.
32:11	Je pris la lettre d'achat, celle qui était scellée, selon les ordonnances et les statuts, et celle qui était ouverte.
32:12	Je donnai la lettre d'achat à Baroukh, fils de Neriyah, fils de Machseyah, sous les yeux de Hanameel, fils de mon oncle, des témoins qui avaient signé la lettre d'achat, et sous les yeux de tous les Juifs qui étaient assis dans la cour de la prison.
32:13	Je donnai sous leurs yeux cet ordre à Baroukh, en disant :
32:14	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Prends ces lettres, cette lettre d'achat, celle qui est scellée et cette lettre ouverte, et mets-les dans un vase de terre, afin qu'elles se conservent beaucoup de jours.
32:15	Car ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : On achètera encore des maisons, des champs et des vignes, dans cette terre.

### Promesse du retour des Juifs en Israël

32:16	Et après avoir donné à Baroukh, fils de Neriyah, la lettre d'achat, je priai YHWH, en disant :
32:17	Ah ! Adonaï YHWH, voici, tu as fait les cieux et la Terre par ta grande puissance et par ton bras étendu : Aucune chose n'est étonnante de ta part.
32:18	Tu fais miséricorde jusqu'à la millième génération, et tu punis l'iniquité des pères dans le sein de leurs fils après eux<!--Ex. 34:7 ; Es. 65:7 ; Ps. 79:12 ; La. 5:7.-->. Tu es le El Gadowl<!--Le El Grand.-->, le Gibbor<!--Le Puissant. Voir Es. 9:5.-->, dont le Nom est YHWH Tsevaot.
32:19	Tu es grand en conseil et puissant en actions, tes yeux sont ouverts sur toutes les voies des fils d'humains, pour rendre à l'homme selon ses voies, et selon le fruit de ses œuvres.
32:20	Tu as fait en terre d'Égypte des miracles et des prodiges jusqu'à ce jour, ainsi qu'en Israël et parmi les humains, et tu t'es fait un Nom tel qu'il est aujourd'hui.
32:21	Tu as fait sortir de la terre d'Égypte ton peuple d'Israël par des miracles, des prodiges, avec une main forte, un bras étendu et une grande terreur.
32:22	Tu leur as donné cette terre, que tu avais juré à leurs pères de leur donner, terre où coulent le lait et le miel.
32:23	Ils sont venus et ils en ont pris possession. Mais ils n'ont pas obéi à ta voix, ils n'ont pas marché selon ta torah et n'ont pas fait tout ce que tu leur avais ordonné de faire. C'est pourquoi tu as fait arriver sur eux tout ce mal-ci !
32:24	Voici que des tertres atteignent la ville pour la prendre et, face à l'épée, à la famine, et à la peste, la ville est livrée entre la main des Chaldéens qui combattent contre elle. Ce que tu as dit est arrivé, et voici, tu le vois.
32:25	Et toi, Adonaï YHWH, tu m'as dit : Achète pour toi ce champ à prix d'argent, et fais témoigner des témoins, quoique la ville soit livrée entre les mains des Chaldéens.
32:26	La parole de YHWH apparut à Yirmeyah, en disant :
32:27	Voici, je suis YHWH, l'Elohîm de toute chair. Y a-t-il quelque chose d'étonnant de ma part ?
32:28	C'est pourquoi ainsi parle YHWH : Voici, je vais livrer cette ville entre les mains des Chaldéens, entre les mains de Neboukadnetsar, roi de Babel, qui la prendra.
32:29	Les Chaldéens qui combattent contre cette ville y entreront. Ils mettront le feu à cette ville et la brûleront avec les maisons où, sur les toits, on a brûlé de l'encens pour Baal et répandu des libations pour d'autres elohîm pour m'irriter.
32:30	Car les fils d'Israël et les fils de Yéhouda n'ont fait, dès leur jeunesse, que ce qui est mal à mes yeux. Car les fils d'Israël n'ont fait que m'irriter par les œuvres de leurs mains, – déclaration de YHWH.
32:31	Car cette ville devient pour moi une cause de colère et de courroux, depuis le jour où ils l'ont bâtie jusqu'à ce jour, au point que je la détourne de mes faces,
32:32	à cause de tout le mal que les fils d'Israël et les fils de Yéhouda ont fait pour m'irriter, eux, leurs rois, leurs chefs, leurs prêtres et leurs prophètes, les hommes de Yéhouda et les habitants de Yeroushalaim.
32:33	Ils m'ont tourné le dos et non les faces. Je les ai enseignés, je les ai enseignés dès le matin, mais ils n'ont pas écouté pour recevoir l'instruction.
32:34	Ils ont mis leurs abominations dans la maison sur laquelle mon Nom est invoqué, pour la souiller.
32:35	Ils ont bâti les hauts lieux de Baal, qui sont dans la vallée de Ben-Hinnom, pour faire passer par le feu leurs fils et leurs filles à Moloc<!--Voir commentaire en Lé. 18:21.-->. Ce que je ne leur avais pas ordonné, et il ne m'était pas monté à la pensée qu'ils feraient cette abomination pour faire pécher Yéhouda.
32:36	Et maintenant, à cause de cela YHWH, l'Elohîm d'Israël, parle ainsi sur cette ville dont vous dites qu'elle est livrée entre les mains du roi de Babel, par l'épée, par la famine, et par la peste :
32:37	Voici, je vais les rassembler de toutes les terres où je les ai bannis, dans ma colère, dans ma fureur et dans mon grand courroux. Je les ramènerai en ce lieu-ci et je les y ferai habiter en sécurité.
32:38	Ils deviendront mon peuple, et je deviendrai leur Elohîm.
32:39	Je leur donnerai un même cœur et une même voie, afin qu'ils me craignent pour toujours, pour leur bien et celui de leurs fils après eux.
32:40	Je traiterai avec eux une alliance éternelle, je ne me retournerai pas de derrière eux, pour leur faire du bien. Je mettrai ma crainte dans leur cœur, afin qu'ils ne se détournent pas de moi<!--Es. 54:10.-->.
32:41	Je me réjouirai sur eux en leur faisant du bien et je les planterai vraiment dans cette terre, de tout mon cœur et de toute mon âme.
32:42	Car ainsi parle YHWH : Comme j'ai fait venir tout ce grand mal sur ce peuple, ainsi je ferai venir sur eux tout le bien que je prononce en leur faveur.
32:43	On achètera des champs sur cette terre dont vous dites : C'est une désolation, sans humains ni bêtes, elle est livrée entre les mains des Chaldéens.
32:44	On achètera des champs à prix d'argent, on écrira des actes de vente, scellés au témoignage de témoins, en terre de Benyamin, aux environs de Yeroushalaim, dans les villes de Yéhouda, tant dans les villes des montagnes, que dans les villes de la plaine et dans les villes du midi, car je ramènerai leurs captifs, – déclaration de YHWH.

## Chapitre 33

### Yéhoshoua (Jésus), le Germe appelé à régner<!--Voir 2 S. 7:8-16.-->

33:1	La parole de YHWH apparut une seconde fois à Yirmeyah, quand il était encore enfermé dans la cour de la prison, en disant :
33:2	Ainsi parle YHWH, qui fait ces choses, YHWH qui les forme et les établit, lui dont le Nom est YHWH :
33:3	Appelle-moi<!--YHWH, qui demandait qu'on l'invoque, n'est autre que Yéhoshoua ha Mashiah (Jésus-Christ), notre Seigneur (Joë. 3:5 ; 1 Co. 1:2 ; Ro. 10:13).--> et je te répondrai. Je te raconterai des choses grandes et cachées que tu ne connais pas.
33:4	Car ainsi parle YHWH, l'Elohîm d'Israël, au sujet des maisons de cette ville et des maisons des rois de Yéhouda, qui seront abattues par les tertres et par l'épée :
33:5	Quand ils sortiront pour combattre les Chaldéens, ils les rempliront des cadavres des humains que j'aurai frappés en raison de ma colère et de mon courroux, car c'est à cause de toute leur méchanceté que je cache mes faces à cette ville.
33:6	Voici, je vais lui donner la santé et la guérison. Je les guérirai et je leur ferai découvrir une abondance de paix et de vérité<!--Ap. 22:1-2.-->.
33:7	Je ramènerai les captifs de Yéhouda, et les captifs d'Israël, et je les rétablirai comme autrefois.
33:8	Je les purifierai de toutes leurs iniquités par lesquelles ils ont péché contre moi. Je pardonnerai toutes leurs iniquités par lesquelles ils ont péché contre moi et par lesquelles ils se sont révoltés contre moi<!--Ez. 37:23.-->.
33:9	Elle deviendra pour moi un nom, une allégresse, une louange et une gloire, parmi toutes les nations de la Terre qui entendront parler de tout le bien que je leur ferai. Elles seront dans la crainte et trembleront, à cause de tout le bien et de toute la paix que je vais lui donner.
33:10	Ainsi parle YHWH : En ce lieu dont vous dites : Il est désert, il n'y a plus d'humains, plus de bêtes dans les villes de Yéhouda, et dans les rues de Yeroushalaim, qui sont désolées, privées d'humains, d'habitants, de bêtes ;
33:11	on y entendra encore les cris de joie et les cris d'allégresse, la voix de l'époux et la voix de l'épouse, et la voix de ceux qui disent : Louez YHWH Tsevaot, car YHWH est bon, car sa bonté est éternelle ! Ils viendront avec la louange à la maison de YHWH. Car je ramènerai les captifs de cette terre comme autrefois, dit YHWH.
33:12	Ainsi parle YHWH Tsevaot : Dans ce lieu désert, où il n'y a ni humains ni bêtes, et dans toutes ses villes, il y aura encore des demeures de bergers qui y feront reposer leurs troupeaux.
33:13	Dans les villes des montagnes, dans les villes de la plaine, dans les villes du midi, en terre de Benyamin et aux environs de Yeroushalaim, et dans les villes de Yéhouda ; les brebis passeront encore sous les mains de celui qui les compte, dit YHWH.
33:14	Voici, les jours viennent, – déclaration de YHWH –, où j'accomplirai la bonne parole que j'ai déclarée sur la maison d'Israël et la maison de Yéhouda.
33:15	En ces jours et en ce temps-là, je ferai germer à David le Germe de justice, qui exercera le jugement et la justice sur la terre.
33:16	En ces jours-là, Yéhouda sera sauvé, Yeroushalaim habitera en sécurité. Voici comment on l'appellera : YHWH-Tsidkenou<!--YHWH notre justice.-->.
33:17	Car ainsi parle YHWH : Il ne sera pas retranché, en ce qui concerne David, un homme pour siéger sur le trône de la maison d'Israël.
33:18	En ce qui concerne les prêtres et les Lévites, il ne sera pas retranché un homme de devant moi faisant monter des holocaustes, brûlant de l'encens avec les offrandes et faisant des sacrifices continuellement.
33:19	La parole de YHWH apparut à Yirmeyah, en disant :
33:20	Ainsi parle YHWH : Si vous pouvez rompre mon alliance avec le jour et mon alliance avec la nuit, de sorte que le jour et la nuit ne soient plus en leur temps,
33:21	mon alliance aussi avec David, mon serviteur sera rompue, de sorte qu'il n'aura plus de fils qui règne sur son trône, ainsi qu'avec les Lévites, les prêtres qui sont à mon service.
33:22	De même qu’on ne peut compter l'armée des cieux, ni mesurer le sable de la mer, de même je multiplierai la postérité de David, mon serviteur, et les Lévites qui font mon service<!--Ge. 2:1, 15:5.-->.
33:23	La parole de YHWH apparut à Yirmeyah, en disant :
33:24	Ne vois-tu pas de quoi parle ce peuple ? Ils disent : YHWH a rejeté les deux familles qu'il avait élues. Ainsi ils méprisent mon peuple, au point que devant eux, il n'est plus une nation.
33:25	Ainsi parle YHWH : Si je n’avais pas établi mon alliance avec le jour et la nuit, les ordonnances<!--Je. 31:35.--> des cieux et de la Terre,
33:26	je rejetterais aussi la postérité de Yaacov et celle de David mon serviteur, pour ne plus prendre dans sa postérité ceux qui gouverneront la postérité d'Abraham, de Yitzhak et de Yaacov. Car je ramènerai, je ramènerai leurs captifs et j'aurai compassion d'eux.

## Chapitre 34

### Désobéissance du peuple : Yeroushalaim (Jérusalem) dévastée

34:1	La parole qui apparut à Yirmeyah de la part de YHWH, lorsque Neboukadnetsar, roi de Babel, et toute son armée, et tous les royaumes de la Terre, et tous les peuples qui étaient sous l'autorité de sa main, combattaient contre Yeroushalaim, et contre toutes ses villes, en disant<!--2 R. 25:1-2.--> :
34:2	Ainsi parle YHWH, l'Elohîm d'Israël : Va et parle à Tsidqiyah, roi de Yéhouda, et dis-lui : Ainsi parle YHWH : Voici, je vais livrer cette ville entre les mains du roi de Babel, et il la brûlera par le feu.
34:3	Et toi, tu n'échapperas pas de sa main, car tu seras pris, tu seras pris et livré entre ses mains. Tes yeux verront les yeux du roi de Babel, il te parlera bouche à bouche et tu iras à Babel.
34:4	Toutefois écoute la parole de YHWH, Tsidqiyah, roi de Yéhouda ! Ainsi parle YHWH sur toi : Tu ne mourras pas par l'épée,
34:5	mais tu mourras en paix. On brûlera pour toi des parfums aromatiques, comme on en a brûlé pour tes pères, les rois précédents qui ont été avant toi, et on fera sur toi la lamentation : Hélas, Seigneur ! Car j'ai prononcé cette parole, – déclaration de YHWH<!--2 Ch. 16:14.-->.
34:6	Yirmeyah, le prophète, dit toutes ces paroles à Tsidqiyah, roi de Yéhouda, à Yeroushalaim.
34:7	L'armée du roi de Babel combattait contre Yeroushalaim, contre toutes les villes de Yéhouda qui restaient, contre Lakis et contre Azéqah, car c'étaient les villes fortifiées qui restaient parmi les villes de Yéhouda<!--2 R. 18:13.-->.

### Yeroushalaim (Jérusalem) deviendra une désolation à cause de la désobéissance

34:8	La parole apparut à Yirmeyah de la part de YHWH, après que le roi Tsidqiyah eut traité une alliance avec tout le peuple de Yeroushalaim, pour proclamer la liberté,
34:9	afin que chaque homme renvoie libre son esclave, chaque homme sa servante, l'Hébreu et la femme Hébreu, et que nul homme n'asservisse un Juif, son frère.
34:10	Tous les chefs et tout le peuple, qui étaient entrés dans cette alliance, écoutèrent pour renvoyer libres chaque homme son esclave, chaque homme sa servante, pour ne plus les asservir encore. Ils écoutèrent et les renvoyèrent.
34:11	Mais après cela, ils revinrent en arrière, et firent revenir leurs esclaves et leurs servantes, qu'ils avaient renvoyés libres et ils les forcèrent à redevenir leurs esclaves et leurs servantes.
34:12	La parole de YHWH apparut à Yirmeyah, en disant :
34:13	Ainsi parle YHWH, l'Elohîm d'Israël : J'ai traité une alliance avec vos pères, le jour où je les ai fait sortir de la terre d'Égypte, de la maison des esclaves, en disant :
34:14	À la fin de la septième année, chaque homme renverra libre son frère hébreu qui lui aura été vendu ; il te servira six années, puis tu le renverras libre de chez toi. Mais vos pères ne m'ont pas écouté, ils n'ont pas prêté l'oreille<!--Ex. 21:2 ; Lé. 25:10-15 ; De. 15:12.-->.
34:15	Vous, vous étiez revenus aujourd'hui, vous aviez fait ce qui est droit à mes yeux en proclamant la liberté chaque homme à son compagnon. Vous aviez traité alliance en ma présence, dans la maison sur laquelle mon Nom est invoqué.
34:16	Mais vous êtes revenus en arrière et vous avez profané mon Nom. Vous avez fait revenir chaque homme ses esclaves, chaque homme ses servantes que vous aviez renvoyés libres et rendus à leurs âmes, et vous les avez forcés à être pour vous des esclaves et des servantes.
34:17	C'est pourquoi ainsi parle YHWH : Vous ne m'avez pas écouté en proclamant la liberté chaque homme à son frère, chaque homme à son prochain. Voici, je proclame contre vous, – déclaration de YHWH –, la liberté de l'épée, de la peste et de la famine, et je ferai de vous un objet de terreur pour tous les royaumes de la Terre.
34:18	Je livrerai les hommes qui ont transgressé mon alliance, et qui n'ont pas observé les paroles de l'alliance qu'ils avaient traitée devant moi, lorsqu'ils sont passés entre les morceaux du veau qu'ils ont coupé en deux.
34:19	Les chefs de Yéhouda, les chefs de Yeroushalaim, les eunuques, les prêtres et tout le peuple de la terre, qui sont passés au travers des morceaux du veau<!--Ge. 15:9-12.-->.
34:20	Je les livrerai entre les mains de leurs ennemis, entre les mains de ceux qui cherchent leur âme. Leurs cadavres deviendront une nourriture pour les oiseaux des cieux et pour les bêtes de la Terre.
34:21	Je livrerai aussi Tsidqiyah, roi de Yéhouda, et les chefs de sa cour entre les mains de leurs ennemis, entre les mains de ceux qui cherchent leur âme, entre les mains de l'armée du roi de Babel qui s'est éloignée de vous.
34:22	Voici, je vais leur donner mes ordres, – déclaration de YHWH –, et je les ramènerai contre cette ville. Ils combattront contre elle, la prendront et la brûleront par le feu. Je ferai des villes de Yéhouda un désert sans habitants.

## Chapitre 35

### L'obéissance des Rékabites

35:1	La parole qui apparut à Yirmeyah de la part de YHWH aux jours de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda, en disant :
35:2	Va à la maison des Rékabites et parle-leur. Fais-les venir à la maison de YHWH dans l'une des chambres et présente-leur du vin à boire<!--2 S. 4:2 ; 1 Ch. 2:55.-->.
35:3	Je pris Ya`azanyah, fils de Yirmeyah, fils de Chabatstsanyah, ses frères, tous ses fils et toute la maison des Rékabites,
35:4	et je les fis venir dans la maison de YHWH, dans la chambre des fils de Hanan, fils de Yigdalyah, homme d'Elohîm, qui était près de la chambre des chefs, au-dessus de la chambre de Ma`aseyah, fils de Shalloum, garde du seuil.
35:5	Je mis devant les fils de la maison des Rékabites des coupes pleines de vin, et des calices, et je leur dis : Buvez du vin !
35:6	Mais ils dirent : Nous ne buvons pas de vin, car Yonadab, fils de Rékab, notre père, nous a donné cet ordre, en disant : Vous ne boirez jamais de vin, ni vous ni vos fils<!--Lé. 10:9 ; No. 6:2-4.-->.
35:7	Vous ne bâtirez aucune maison, vous ne sèmerez aucune semence, vous ne planterez aucune vigne, et vous n'en aurez pas, mais vous habiterez sous des tentes tous les jours de votre vie, afin que vous viviez beaucoup de jours sur les faces du sol où vous êtes étrangers.
35:8	Nous avons obéi à la voix de Yehonadab, fils de Rékab, notre père dans tout ce qu’il nous a ordonné, de sorte que nous n'avons pas bu de vin pendant tous nos jours, ni nous, ni nos femmes, ni nos fils, ni nos filles.
35:9	Nous n'avons bâti aucune maison pour notre demeure, et nous n'avons eu ni vigne, ni champ, ni semence.
35:10	Mais nous avons habité sous des tentes, nous obéissons et nous agissons selon tout ce que Yonadab, notre père, nous a ordonné.
35:11	Il est arrivé que quand Neboukadnetsar, roi de Babel, est monté contre cette terre, nous nous sommes dit : Venez ! Entrons dans Yeroushalaim face à l'armée des Chaldéens et face à l'armée de Syrie. C'est ainsi que nous habitons à Yeroushalaim.
35:12	La parole de YHWH apparut à Yirmeyah, en disant :
35:13	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Va, et dis aux hommes de Yéhouda, et aux habitants de Yeroushalaim : Ne recevrez-vous pas d'instruction pour obéir à mes paroles ? – déclaration de YHWH.
35:14	Toutes les paroles de Yehonadab, fils de Rékab, qui a ordonné à ses fils de ne pas boire de vin, ont été observées, et ils n'en ont pas bu jusqu'à ce jour, mais ils ont obéi au commandement de leur père. Mais moi, je vous ai parlé, je vous ai parlé dès le matin, et vous ne m'avez pas écouté.
35:15	Je vous ai envoyé tous les prophètes, mes serviteurs, je les ai envoyés dès le matin, pour vous dire : Revenez, s’il vous plaît, chaque homme de sa mauvaise voie, rendez bonnes vos actions, n'allez pas après d'autres elohîm pour les servir, et vous resterez sur le sol que j'ai donné à vous et à vos pères. Mais vous n'avez pas prêté l'oreille, et vous ne m'avez pas écouté.
35:16	Parce que les fils de Yehonadab, fils de Rékab, ont observé le commandement que leur avait donné leur père, et que ce peuple ne m'écoute pas,
35:17	à cause de cela, YHWH Elohîm Tsevaot, l'Elohîm d'Israël, parle ainsi : Voici, je vais faire venir sur Yéhouda et sur tous les habitants de Yeroushalaim, tout le mal que j'ai prononcé contre eux, parce que je leur ai parlé et qu'ils n'ont pas écouté, parce que je les ai appelés et qu'ils n'ont pas répondu.
35:18	Et Yirmeyah dit à la maison des Rékabites : Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Parce que vous avez obéi au commandement de Yehonadab, votre père, que vous avez gardé tous ses commandements et que vous avez agi selon tout ce qu'il vous a ordonné<!--Les Rékabites furent bénis parce qu'ils obéirent aux commandements de leur père (Ep. 6:1-3).-->,
35:19	à cause de cela, ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Yonadab, fils de Rékab, ne manquera jamais de descendants qui se tiennent debout devant moi.

## Chapitre 36

### Le roi Yehoyaqiym brûle le manuscrit de Yirmeyah

36:1	Il arriva, dans la quatrième année de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda, que cette parole vint à Yirmeyah de la part de YHWH, en disant :
36:2	Prends pour toi un rouleau de livre et écris-y toutes les paroles que je t'ai dites contre Israël, contre Yéhouda et contre toutes les nations, depuis le jour où je t'ai parlé, depuis les jours de Yoshiyah, jusqu'à ce jour.
36:3	Peut-être que la maison de Yéhouda entendra-t-elle tout le mal que je pense leur faire, afin que chaque homme revienne de sa mauvaise voie, alors je pardonnerai leur iniquité et leur péché.
36:4	Yirmeyah appela Baroukh, fils de Neriyah, et Baroukh écrivit, de la bouche de Yirmeyah, dans le rouleau de livre, toutes les paroles que YHWH lui avait dites.
36:5	Yirmeyah donna cet ordre à Baroukh, en disant : Je suis retenu, je ne peux pas entrer dans la maison de YHWH.
36:6	Tu y entreras et tu liras dans le rouleau que tu as écrit de ma bouche, toutes les paroles de YHWH, aux oreilles du peuple dans la maison de YHWH le jour du jeûne. Tu les liras aussi aux oreilles de tout Yéhouda, de ceux qui seront venus de leurs villes.
36:7	Peut-être leur demande de faveur tombera-t-elle devant YHWH et reviendront-ils, chaque homme de sa mauvaise voie. Car grande est la colère, le courroux que YHWH a déclaré contre ce peuple.
36:8	Baroukh, fils de Neriyah, fit tout ce que lui avait ordonné Yirmeyah, le prophète, lisant dans le rouleau les paroles de YHWH, dans la maison de YHWH.
36:9	Or il arriva dans la cinquième année de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda, le neuvième mois, qu'on proclama le jeûne devant YHWH à tout le peuple de Yeroushalaim et à tout le peuple venu des villes de Yéhouda à Yeroushalaim.
36:10	Et Baroukh lut dans le livre, les paroles de Yirmeyah, aux oreilles de tout le peuple, dans la maison de YHWH, dans la chambre de Gemaryah, fils de Shaphan, le scribe, dans le parvis supérieur, à l'entrée de la porte Neuve de la maison de YHWH.
36:11	Quand Miykayeh, fils de Gemaryah, fils de Shaphan, eut entendu d'après le livre toutes les paroles de YHWH,
36:12	il descendit dans la maison du roi, vers la chambre du scribe, et voici tous les chefs y étaient assis : Éliyshama, le scribe, Delayah, fils de Shema’yah, Elnathan, fils d'Acbor, et Gemaryah, fils de Shaphan, et Tsidqiyah, fils de Chananyah, et tous les chefs.
36:13	Et Miykayeh leur rapporta toutes les paroles qu'il avait entendues, quand Baroukh lisait dans le livre, aux oreilles du peuple.
36:14	Tous les chefs envoyèrent vers Baroukh, Yehoudi, fils de Nethanyah, fils de Shelemyah, fils de Koushi, pour lui dire : Prends dans ta main le rouleau que tu as lu aux oreilles du peuple, et viens ici ! Baroukh, fils de Neriyah, prit le rouleau dans sa main et vint vers eux.
36:15	Ils lui dirent : Assieds-toi, s’il te plaît, et lis-le à nos oreilles ! Et Baroukh le lut à leurs oreilles.
36:16	Il arriva qu'aussitôt qu'ils entendirent toutes ces paroles, ils tremblèrent, l’homme et son compagnon. Ils dirent à Baroukh : Nous rapporterons, nous rapporterons au roi toutes ces paroles.
36:17	Ils interrogèrent Baroukh, en disant : Dis-nous, s’il te plaît, comment tu as écrit toutes ces paroles de sa bouche.
36:18	Baroukh leur dit : Il me dictait de sa bouche toutes ces paroles, et je les écrivais avec de l'encre dans le livre.
36:19	Les chefs dirent à Baroukh : Va, et cache-toi, ainsi que Yirmeyah, et que personne ne sache où vous serez.
36:20	Ils s'en allèrent vers le roi dans la cour, mais ils prirent soin de laisser le rouleau dans la chambre d'Éliyshama, le scribe et ils racontèrent toutes ces paroles aux oreilles du roi.
36:21	Et le roi envoya Yehoudi pour prendre le rouleau. Yehoudi le prit dans la chambre d'Éliyshama, le scribe, et il le lut aux oreilles du roi et de tous les chefs qui étaient autour de lui.
36:22	Or le roi était assis dans la maison d'hiver, au neuvième mois, et un brasier était allumé devant lui.
36:23	Il arriva qu'aussitôt que Yehoudi eut lu trois ou quatre feuilles, le roi déchira le rouleau avec le rasoir du scribe, et le jeta au feu du brasier, jusqu'à ce que tout le rouleau soit entièrement consumé par le feu du brasier.
36:24	Le roi et tous ses serviteurs qui entendirent toutes ces paroles ne tremblèrent pas et ne déchirèrent pas leurs vêtements.
36:25	Toutefois, Elnathan, Delayah et Gemaryah intercédèrent auprès du roi afin qu'il ne brûle pas le rouleau, mais il ne les écouta pas.
36:26	Le roi ordonna à Yerachme'el, fils du roi, à Serayah, fils d'Azriel, et à Shelemyah, fils d'Abdeel, de saisir Baroukh, le scribe, et Yirmeyah, le prophète, mais YHWH les cacha.

### Remplacement du manuscrit brûlé ; jugement sur Yehoyaqiym

36:27	La parole de YHWH apparut à Yirmeyah, après que le roi eut brûlé le rouleau et les paroles que Baroukh avait écrites de la bouche de Yirmeyah, en disant :
36:28	Retourne, prends pour toi un autre rouleau, écris dessus toutes les paroles, les premières, qui étaient sur le premier rouleau que Yehoyaqiym, roi de Yéhouda, a brûlé.
36:29	Et contre Yehoyaqiym, roi de Yéhouda, tu diras : Ainsi parle YHWH : Tu as brûlé ce rouleau, et tu as dit : Pourquoi as-tu écrit dessus, en disant : Le roi de Babel viendra, il viendra et détruira cette terre, il exterminera les humains et les bêtes ?
36:30	C'est pourquoi ainsi parle YHWH contre Yehoyaqiym, roi de Yéhouda : Aucun des siens ne sera assis sur le trône de David, et son cadavre sera jeté de jour à la chaleur et de nuit à la gelée.
36:31	Je le punirai, lui, sa postérité et ses serviteurs à cause de leur iniquité, et je ferai venir sur eux, sur les habitants de Yeroushalaim et sur les hommes de Yéhouda tout le mal dont je les ai menacés puisqu'ils n'ont pas écouté.
36:32	Yirmeyah prit un autre rouleau et le donna à Baroukh, fils de Neriyah, le scribe, qui écrivit dessus, de la bouche de Yirmeyah toutes les paroles du rouleau que Yehoyaqiym, roi de Yéhouda, avait brûlé au feu. Beaucoup de paroles semblables y furent encore ajoutées.

## Chapitre 37

### Tsidqiyah (Sédécias) sollicite l'intercession de Yirmeyah (Jérémie)

37:1	Le roi Tsidqiyah, fils de Yoshiyah, régna à la place de Konyah, fils de Yehoyaqiym, et il fut établi roi en terre de Yéhouda par Neboukadnetsar, roi de Babel.
37:2	Ni lui, ni ses serviteurs, ni le peuple de la terre, n'obéirent aux paroles que YHWH déclara par la main de Yirmeyah, le prophète.
37:3	Le roi Tsidqiyah envoya Yehoukal, fils de Shelemyah, et Tsephanyah, fils de Ma`aseyah, le prêtre, vers Yirmeyah, le prophète, pour lui dire : S’il te plaît, intercède pour nous auprès de YHWH, notre Elohîm.
37:4	Yirmeyah entrait et sortait parmi le peuple, parce qu'on ne l'avait pas encore mis dans la maison de détention.
37:5	L'armée de pharaon sortit d'Égypte, et quand les Chaldéens qui assiégeaient Yeroushalaim en entendirent la rumeur à leur sujet, ils se retirèrent de Yeroushalaim.
37:6	La parole de YHWH apparut à Yirmeyah, le prophète, en disant :
37:7	Ainsi parle YHWH, l'Elohîm d'Israël : Vous direz ainsi au roi de Yéhouda, qui vous a envoyés me consulter : Voici, l'armée de pharaon, qui était sortie à votre secours, retourne vers sa terre, en Égypte.
37:8	Les Chaldéens reviendront, ils combattront contre cette ville, la prendront et la brûleront par le feu.
37:9	Ainsi parle YHWH : Ne trompez pas vos âmes, en disant : Les Chaldéens s'en iront loin de nous ! Car ils ne s'en iront pas.
37:10	Car si vous aviez battu toute l'armée des Chaldéens qui combattent contre vous, et qu’il n’en restât que des hommes transpercés, chaque homme hors de sa tente, ils se lèveraient et brûleraient cette ville par le feu.

### Yirmeyah calomnié et emprisonné

37:11	Il arriva, quand l'armée des Chaldéens monta loin de Yeroushalaim, face à l'armée du pharaon,
37:12	que Yirmeyah sortit de Yeroushalaim pour s'en aller en terre de Benyamin en se glissant de là au milieu du peuple.
37:13	Arrivé à la porte de Benyamin, le commandant de la garde, du nom de Yir'iyayh, fils de Shelemyah, fils de Chananyah, se trouvait là. Il saisit Yirmeyah le prophète en disant : Tu tombes<!--Vient de l'hébreux « naphal » qui signifie : « Tomber », « être couché », « être jeté à bas », « échouer ».--> chez les Chaldéens, toi !
37:14	Yirmeyah dit : C'est un mensonge ! Je ne tombe pas chez les Chaldéens ! Mais il ne l'écouta pas, et Yir'iyayh prit Yirmeyah et l'amena vers les chefs.
37:15	Les chefs furent fâchés contre Yirmeyah, le frappèrent et le mirent en maison de liens<!--Ou « prison ».--> dans la maison de Yehonathan, le scribe, car ils en avaient fait une maison d'arrêt.
37:16	Oui, Yirmeyah entra dans la maison de la fosse, dans une cellule. Yirmeyah y resta beaucoup de jours.
37:17	Le roi Tsidqiyah envoya quelqu'un le prendre pour l'interroger en secret dans sa maison. Il lui dit : Y a-t-il une parole de la part de YHWH ? Yirmeyah dit : Il y en a ! et il dit : Tu seras livré entre les mains du roi de Babel.
37:18	Yirmeyah dit au roi Tsidqiyah : Quel péché ai-je commis contre toi, contre tes serviteurs et contre ce peuple, pour que vous m'ayez mis en maison d'arrêt ?
37:19	Où sont vos prophètes qui vous prophétisaient, en disant : Le roi de Babel ne viendra pas contre vous, ni contre cette terre ?
37:20	Et maintenant écoute, s’il te plaît, roi, mon seigneur ! Que ma supplication, s’il te plaît, tombe devant toi, et ne me renvoie pas dans la maison de Yehonathan, le scribe, de peur que je n'y meure !
37:21	Le roi Tsidqiyah ordonna qu'on garde Yirmeyah dans la cour de la prison, et qu'on lui donne chaque jour un pain rond de la rue des boulangers, jusqu'à ce que tout le pain de la ville soit épuisé. Ainsi Yirmeyah demeura dans la cour de la prison.

## Chapitre 38

### Yirmeyah jeté dans la fosse, puis délivré par Ébed-Mélek, l'Éthiopien

38:1	Shephatyah, fils de Matthan, Gedalyah, fils de Pashhour, Youkal<!--Yehoukal. Voir v. 37:3.-->, fils de Shelemyah et Pashhour, fils de Malkiyah, entendirent les paroles que Yirmeyah déclarait à tout le peuple, en disant :
38:2	Ainsi parle YHWH : Celui qui restera dans cette ville mourra par l'épée, par la famine, ou par la peste, mais celui qui sortira vers les Chaldéens vivra, et son âme sera son butin, et il vivra.
38:3	Ainsi parle YHWH : Cette ville sera livrée, livrée aux mains de l'armée du roi de Babel, qui la prendra.
38:4	Les chefs dirent au roi : S’il te plaît, que cet homme soit mis à mort, car il décourage les mains des hommes de guerre qui restent dans cette ville, ainsi que les mains de tout le peuple, en leur tenant de tels discours, car cet homme ne cherche nullement la paix pour ce peuple, mais son malheur.
38:5	Le roi Tsidqiyah dit : Voici, il est entre vos mains, car le roi ne peut rien contre vous.
38:6	Ils prirent Yirmeyah et le jetèrent dans la fosse de Malkiyah, fils du roi, laquelle se trouvait dans la cour de la prison, et ils descendirent Yirmeyah avec des cordes dans cette fosse où il n'y avait pas d'eau mais de la boue, et ainsi Yirmeyah s'enfonça dans la boue.
38:7	Ébed-Mélek, l'Éthiopien, un homme, un eunuque de la maison du roi, entendit qu'ils avaient mis Yirmeyah dans cette fosse. Le roi était assis à la porte de Benyamin.
38:8	Ébed-Mélek sortit de la maison du roi, et parla au roi, en disant :
38:9	Roi, mon seigneur, c'est méchant, tout ce que ces hommes ont fait contre Yirmeyah, le prophète, en le jetant dans la fosse. Il mourra là-dessous, face à la famine, car il n’y a plus de pain dans la ville.
38:10	Le roi donna cet ordre à Ébed-Mélek, l'Éthiopien, en disant : Prends ici 30 hommes sous ta main, et fais monter hors de la fosse Yirmeyah, le prophète, avant qu'il ne meure.
38:11	Ébed-Mélek prit des hommes sous sa main, et entra dans la maison du roi, dans un lieu au-dessous du trésor, d'où il prit de vieux lambeaux et de vieux chiffons, et les descendit avec des cordes à Yirmeyah dans la fosse.
38:12	Ébed-Mélek, l'Éthiopien dit à Yirmeyah : S’il te plaît, mets ces vieux lambeaux et ces chiffons sous les aisselles de tes mains, au-dessous des cordes. Et Yirmeyah fit ainsi.
38:13	Ils tirèrent Yirmeyah avec les cordes et le firent monter hors de la fosse. Yirmeyah resta dans la cour de la prison.

### Yirmeyah appelle Tsidqiyah (Sédécias) à la repentance

38:14	Le roi Tsidqiyah envoya prendre chez lui Yirmeyah, le prophète, à la troisième entrée, qui était dans la maison de YHWH. Et le roi dit à Yirmeyah : Je vais te demander une chose, ne me cache rien.
38:15	Yirmeyah dit à Tsidqiyah : Si je te le déclare, ne me feras-tu pas mourir, mourir ? Et si je te donne un conseil, tu ne m'écouteras pas !
38:16	Le roi Tsidqiyah jura secrètement à Yirmeyah, en disant : Vivant est YHWH, qui a fait notre âme. Je ne te ferai pas mourir, et je ne te livrerai pas entre les mains de ces hommes qui cherchent ton âme.
38:17	Yirmeyah dit à Tsidqiyah : Ainsi parle YHWH, Elohîm Tsevaot, l'Elohîm d'Israël : Si tu sors, si tu sors vers les chefs du roi de Babel, ton âme vivra et cette ville ne sera pas brûlée par le feu. Tu vivras, toi et ta maison.
38:18	Mais si tu ne sors pas vers les chefs du roi de Babel, cette ville sera livrée entre les mains des Chaldéens, qui la brûleront par le feu et tu n'échapperas pas à leurs mains.
38:19	Le roi Tsidqiyah dit à Yirmeyah : Moi, j’ai peur des Juifs qui sont tombés chez les Chaldéens : on me livrera entre leurs mains et me traiteront avec sévérité.
38:20	Yirmeyah lui dit : On ne te livrera pas à eux. S'il te plaît, écoute la voix de YHWH dans ce que je te dis ! Tout ira bien pour toi et ton âme vivra.
38:21	Mais si tu refuses de sortir, voici ce que YHWH m'a fait voir :
38:22	Toutes les femmes qui restent dans la maison du roi de Yéhouda seront amenées aux chefs du roi de Babel, et elles diront : Tu as été séduit, vaincu, par les hommes qui te prédisaient la paix. Quand tes pieds se sont enfoncés dans la boue, ils se sont retirés en arrière !
38:23	Toutes tes femmes et tes fils seront amenés dehors aux Chaldéens et toi, tu n'échapperas pas à leurs mains : mais tu seras saisi par la main du roi de Babel, et à cause de toi, cette ville sera brûlée par le feu.
38:24	Tsidqiyah dit à Yirmeyah : Que personne ne sache rien de ces paroles, et tu ne mourras pas.
38:25	Si les chefs apprennent que je t'ai parlé, et qu'ils viennent vers toi et te disent : Déclare-nous, s’il te plaît, ce que tu as dit au roi, et ce que le roi t'a dit, ne nous cache rien, et nous ne te ferons pas mourir,
38:26	tu leur diras : Je faisais tomber ma demande de faveur devant le roi, afin qu'il ne me renvoie pas à la maison de Yehonathan, pour y mourir.
38:27	Tous les chefs, vinrent vers Yirmeyah et l'interrogèrent. Mais il leur répondit selon toutes les paroles que le roi lui avait ordonnées de dire. Ils gardèrent le silence, car l'affaire n'avait pas été entendue.
38:28	Yirmeyah demeura dans la cour de la prison, jusqu'au jour où Yeroushalaim fut prise, et il y était lorsque Yeroushalaim fut prise.

## Chapitre 39

### Prise de Yeroushalaim (Jérusalem) ; Tsidqiyah (Sédécias) déporté à Babel<!--2 R. 25:1-7 ; Jé. 52:4-17 ; 2 Ch. 36:17-21.-->

39:1	La neuvième année de Tsidqiyah, roi de Yéhouda, au dixième mois, Neboukadnetsar, roi de Babel, vint avec toute son armée contre Yeroushalaim, et ils l'assiégèrent.
39:2	La onzième année de Tsidqiyah, le neuvième jour du quatrième mois, une brèche fut faite à la ville.
39:3	Tous les chefs du roi de Babel y entrèrent et s'assirent à la porte du milieu : Nergal-Sharetser, Samgar-Nebou, Sarsekim, chef des eunuques, Nergal-Sharetser, chef des devins et tous les autres chefs du roi de Babel.
39:4	Il arriva qu'aussitôt que Tsidqiyah, roi de Yéhouda et tous les hommes de guerre les eurent vus, ils s'enfuirent et sortirent de nuit hors de la ville, par le chemin du jardin du roi, par la porte entre les deux murailles, et ils s'en allèrent par le chemin de la région aride.
39:5	Mais l'armée des Chaldéens les poursuivit et rattrapa Tsidqiyah dans les régions arides de Yeriycho. Ils le prirent et le firent monter vers Neboukadnetsar, roi de Babel, à Riblah, en terre de Hamath, où il prononça un jugement contre lui.
39:6	Le roi de Babel tua à Riblah les fils de Tsidqiyah sous ses yeux. Le roi de Babel tua aussi tous les nobles de Yéhouda.
39:7	Il creva les yeux de Tsidqiyah et le lia avec une double chaîne de cuivre pour le conduire à Babel.
39:8	Les Chaldéens brûlèrent par le feu la maison royale et les maisons du peuple, et ils abattirent<!--Ici débute le « temps des nations » (587 – 586 av. J.-C.), Yeroushalaim est foulée aux pieds par les nations. Voir aussi 2 R. 25:8-24 ; 2 Ch. 36:17-21.--> les murailles de Yeroushalaim.
39:9	Et le reste du peuple, ceux qui restaient dans la ville, et les tombés, ceux qui étaient tombés vers eux, le reste du peuple, ceux qui restaient, Nebouzaradân, le grand bourreau, les emmena en exil à Babel.
39:10	Mais Nebouzaradân, le grand bourreau, laissa en terre de Yéhouda les plus pauvres du peuple qui n'avaient rien, et il leur donna ce jour-là des vignes et des champs.

### Yirmeyah libéré de prison

39:11	Or Neboukadnetsar, roi de Babel, avait donné cet ordre au sujet de Yirmeyah, par la main de Nebouzaradân, le grand bourreau, en disant :
39:12	Prends cet homme et veille sur lui. Ne lui fais aucun mal, mais fais pour lui tout ce qu'il te dira.
39:13	Nebouzaradân, le grand bourreau, Neboushazbân, chef des eunuques, Nergal-Sharetser, chef des devins, et tous les grands du roi de Babel
39:14	envoyèrent prendre Yirmeyah dans la cour de la prison, et le remirent à Gedalyah, fils d'Achikam, fils de Shaphan, pour qu'il le conduise dans sa maison. Et il demeura au milieu du peuple.

### YHWH épargne Ébed-Mélek

39:15	La parole de YHWH apparut à Yirmeyah pendant qu'il était enfermé dans la cour de la prison, en disant :
39:16	Va, et parle à Ébed-Mélek, l'Éthiopien, et dis-lui : Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, je vais faire venir mes paroles sur cette ville pour son malheur et non pas pour son bonheur, et elles s'accompliront en ce jour-là devant toi.
39:17	Je te délivrerai en ce jour-là, – déclaration de YHWH –, et tu ne seras pas livré entre les mains des hommes que tu crains.
39:18	Car je te ferai échapper, je te ferai échapper et tu ne tomberas pas sous l'épée. Ton âme deviendra ton butin parce que tu as eu confiance en moi, – déclaration de YHWH.

## Chapitre 40

### Le roi de Babel établit Gedalyah sur la terre

40:1	La parole qui apparut à Yirmeyah de la part de YHWH, quand Nebouzaradân, le grand bourreau, l'eut renvoyé de Ramah. Quand il le fit prendre, Yirmeyah était lié de chaînes au milieu de tous les exilés de Yeroushalaim et de Yéhouda, ceux qu'on exilait à Babel.
40:2	Le grand bourreau prit Yirmeyah et lui dit : YHWH, ton Elohîm, a prononcé ce malheur contre ce lieu.
40:3	YHWH l'a fait venir, il a agi conformément à ce qu'il avait déclaré. Ces choses vous sont arrivées parce que vous avez péché contre YHWH et que vous n'avez pas écouté sa voix.
40:4	Maintenant, voici, je t’ai ouvert aujourd’hui des chaînes qui étaient sur ta main. S'il est bon à tes yeux de venir avec moi à Babel, viens ! J'aurai les yeux sur toi. Mais si c’est mauvais à tes yeux de venir avec moi à Babel, abstiens-toi. Regarde ! Toute la terre est en face de toi. Là où il est bon et juste à tes yeux d’aller, vas-y.
40:5	Et comme il ne s’en retournait pas encore, il dit : Retourne, vers Gedalyah, fils d'Achikam, fils de Shaphan, que le roi de Babel a établi sur les villes de Yéhouda, et demeure avec lui parmi le peuple, ou bien, va partout où il conviendra à tes yeux d'aller. Le grand bourreau lui donna des vivres et quelques présents et le renvoya.
40:6	Yirmeyah alla vers Gedalyah, fils d'Achikam, à Mitspah, et demeura avec lui parmi le peuple qui était resté sur la terre.
40:7	Tous les chefs des armées qui étaient dans les champs, eux et leurs hommes, apprirent que le roi de Babel avait établi Gedalyah, fils d'Achikam, sur la terre, et qu'il lui avait confié hommes, femmes et enfants, les pauvres de la terre qu'on n'avait pas emmenés en exil à Babel.
40:8	Ils allèrent vers Gedalyah à Mitspah. Il y avait Yishmael, fils de Nethanyah, Yohanan et Yonathan, fils de Karéach, Serayah, fils de Tanhoumeth, les fils d'Éphaï de Netophah, Yezanyah, fils du Maakatite, eux et leurs hommes.
40:9	Gedalyah, fils d'Achikam, fils de Shaphan, leur jura, à eux et à leurs hommes, en disant : N'ayez pas peur de servir les Chaldéens ! Demeurez sur la terre, servez le roi de Babel, et vous serez heureux.
40:10	Quant à moi, voici que j'habite à Mitspah, pour me tenir devant les Chaldéens qui viendront vers nous. Quant à vous, recueillez le vin, les fruits d'été et l'huile, mettez-les dans vos vases, et demeurez dans vos villes que vous avez prises.
40:11	Tous les Juifs qui étaient en Moab, chez les fils d'Ammon, en Édom et dans toutes les terres, apprirent aussi que le roi de Babel avait laissé quelque reste à Yéhouda, et qu'il avait établi sur eux Gedalyah, fils d'Achikam, fils de Shaphan.
40:12	Tous ces Juifs-là retournèrent de tous les lieux où ils avaient été bannis et vinrent en terre de Yéhouda vers Gedalyah à Mitspah. Ils recueillirent du vin et des fruits d'été en très grande quantité.
40:13	Mais Yohanan, fils de Karéach, et tous les chefs des armées qui étaient dans les champs, vinrent vers Gedalyah à Mitspah,
40:14	et lui dirent : Ne sais-tu pas, ne sais-tu pas que Baalis, roi des Ammonites, a envoyé Yishmael, le fils de Nethanyah, pour tuer ton âme ? Mais Gedalyah, fils d'Achikam, ne les crut pas.
40:15	Yohanan, fils de Karéach, parla en secret à Gedalyah à Mitspah, en disant : Que j’aille, s'il te plaît, et je tuerai Yishmael, fils de Nethanyah, et personne ne le saura. Pourquoi tuerait-il ton âme ? Pourquoi tous les Juifs qui sont rassemblés auprès de toi seraient-ils dispersés, et le reste de Yéhouda serait-il exterminé ?
40:16	Mais Gedalyah, fils d'Achikam, dit à Yohanan, fils de Karéach : Ne fais pas cela car tu parles faussement de Yishmael.

## Chapitre 41

### Assassinat de Gedalyah

41:1	Il arriva, au septième mois, que Yishmael, fils de Nethanyah, fils d'Éliyshama, de la postérité royale, et l'un des grands du roi et dix hommes avec lui, vinrent vers Gedalyah, fils d'Achikam, à Mitspah. Et là, à Mitspah<!--2 R. 25:25.-->, ils mangèrent du pain ensemble.
41:2	Et Yishmael, fils de Nethanyah, se leva, ainsi que les dix hommes qui étaient avec lui, et ils frappèrent avec l'épée Gedalyah, fils d'Achikam, fils de Shaphan, et on le fit mourir, lui que le roi de Babel avait établi sur la terre.
41:3	Yishmael frappa aussi tous les Juifs qui étaient avec Gedalyah à Mitspah, et les Chaldéens, hommes de guerre, qui se trouvaient là.
41:4	Il arriva, le second jour après qu’on eut fait mourir Gedalyah, et personne ne le savait,
41:5	que des hommes vinrent de Shekem, de Shiyloh et de Samarie, 80 hommes, la barbe rasée et les vêtements déchirés, et qui s’étaient fait des incisions, et avaient dans leur main des offrandes et de l'encens, pour les faire venir à la maison de YHWH.
41:6	Yishmael, fils de Nethanyah, sortit de Mitspah au-devant d'eux. Il marchait, il marchait en pleurant. Et quand il les rencontra, il leur dit : Venez vers Gedalyah, fils d'Achikam.
41:7	Il arriva que, quand ils furent entrés jusqu'au milieu de la ville, Yishmael, fils de Nethanyah les tua au milieu de la fosse, lui et ses hommes avec lui.
41:8	Mais il se trouva parmi eux dix hommes, qui dirent à Yishmael : Ne nous fais pas mourir, car nous avons des provisions cachées dans les champs, du blé, de l'orge, de l'huile et du miel. Il s’abstint et ne les fit pas mourir au milieu de leurs frères.
41:9	Or la fosse où Yishmael jeta tous les cadavres des hommes qu’il tua par la main de Gedalyah, est celle que le roi Asa avait faite par crainte de Baesha, roi d'Israël. Yishmael, fils de Nethanyah, la remplit de gens qu'il avait blessés mortellement<!--1 R. 15:22.-->.
41:10	Yishmael emmena captif tout le reste du peuple qui était à Mitspah, les filles du roi et tous ceux du peuple qui demeuraient à Mitspah, que Nebouzaradân, le grand bourreaus, avait confiés à Gedalyah, fils d'Achikam. Yishmael, fils de Nethanyah, les emmena captifs et s'en alla pour passer chez les fils d’Ammon.

### Yohanan délivre le peuple ; fuite de Yishmael

41:11	Mais Yohanan, fils de Karéach, et tous les chefs des armées qui étaient avec lui, apprirent tout le mal que Yishmael, fils de Nethanyah, avait fait.
41:12	Ils prirent tous les hommes et s'en allèrent pour combattre contre Yishmael, fils de Nethanyah. Ils le trouvèrent près des grandes eaux qui sont à Gabaon.
41:13	Il arriva qu'aussitôt que tout le peuple qui était avec Yishmael vit Yohanan, fils de Karéach, et tous les chefs des armées qui étaient avec lui, il s'en réjouit.
41:14	Tout le peuple que Yishmael avait emmené captif de Mitspah se tourna, il revint et s’en alla vers Yohanan, fils de Karéach.
41:15	Mais Yishmael, fils de Nethanyah s'échappa avec huit hommes devant Yohanan et s'en alla vers les fils d'Ammon.
41:16	Yohanan, fils de Karéach, et tous les chefs des troupes qui étaient avec lui, prirent tout le reste du peuple, et le délivrèrent des mains de Yishmael, fils de Nethanyah, lorsqu'il l'emmenait de Mitspah, après avoir tué Gedalyah, fils d'Achikam : les hommes de guerre, les femmes, les enfants et les eunuques, il les ramena de Gabaon.
41:17	Ils s'en allèrent et demeurèrent à l'hôtellerie de Kimham, près de Bethléhem, pour partir et s’en aller en Égypte,
41:18	face aux Chaldéens, car ils avaient peur en face d’eux, parce que Yishmael, fils de Nethanyah, avait tué Gedalyah, fils d'Achikam, que le roi de Babel avait établi sur la terre.

## Chapitre 42

### YHWH défend au reste du peuple de se réfugier en Égypte

42:1	Tous les chefs des armées, Yohanan, fils de Karéach, Yezanyah, fils d'Hosha`yah et tout le peuple, depuis le plus petit jusqu'au plus grand, s'approchèrent,
42:2	et dirent à Yirmeyah, le prophète : Que notre supplication, s’il te plaît, tombe devant toi ! Intercède auprès de YHWH ton Elohîm en notre faveur et en faveur de tout ce reste ! Car nous étions beaucoup et nous ne sommes plus qu'un petit nombre, comme tes yeux nous voient.
42:3	Que YHWH, ton Elohîm, nous fasse connaître le chemin où nous devons marcher et la chose que nous devons faire !
42:4	Et Yirmeyah, le prophète, leur dit : Entendu ! Voici, je vais prier YHWH, votre Elohîm, selon vos paroles ; il arrivera que toute parole que YHWH vous donnera en réponse, je vous la ferai connaître. Je ne vous refuserai pas une parole.
42:5	Ils dirent à Yirmeyah : Que YHWH soit entre nous un témoin véritable et fidèle, si nous ne faisons pas selon toutes les paroles que YHWH, ton Elohîm, t'enverra nous dire !
42:6	Que ce soit bon ou mauvais, nous obéirons à la voix de YHWH, notre Elohîm, vers qui nous t'envoyons, ainsi nous serons heureux pour avoir obéi à la voix de YHWH, notre Elohîm.
42:7	Il arriva, au bout de dix jours, que la parole de YHWH apparut à Yirmeyah.
42:8	Et il appela Yohanan, fils de Karéach, tous les chefs des armées qui étaient avec lui, et tout le peuple, depuis le plus petit jusqu'au plus grand.
42:9	Il leur dit : Ainsi parle YHWH, l'Elohîm d'Israël, vers qui vous m'avez envoyé, pour présenter votre supplication devant lui :
42:10	Si vous retournez et habitez sur cette terre, je vous rétablirai et je ne vous détruirai pas. Je vous y planterai et je ne vous arracherai pas, car je me repens du mal que je vous ai fait.
42:11	N'ayez pas peur face au roi de Babel, en face de qui vous avez peur ! N'ayez pas peur de lui, – déclaration de YHWH –, car je suis avec vous pour vous sauver et pour vous délivrer de sa main.
42:12	Je vous donnerai mes miséricordes et j’aurai compassion de vous. Je vous ferai revenir sur votre sol.
42:13	Mais si vous dites : Nous ne resterons pas sur cette terre, refusant ainsi d'obéir à la voix de YHWH, notre Elohîm,
42:14	si vous dites : Non ! mais nous irons en terre d'Égypte, où nous ne verrons pas de guerre, où nous n'entendrons pas le son du shofar, et où nous n'aurons pas faim de pain, et c'est là que nous habiterons,
42:15	et maintenant, à cause de cela, écoutez la parole de YHWH, vous, le reste de Yéhouda ! Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Si vous mettez, si vous mettez vos faces en direction de l'Égypte et si vous y allez demeurer,
42:16	il arrivera que l'épée dont vous avez peur vous atteindra là-bas, en terre d'Égypte, et la famine que vous craignez vous suivra de près, là, en Égypte et là vous mourrez<!--Ez. 30:9-11.-->.
42:17	Et il arrivera que tous les hommes qui tourneront leurs faces pour aller en Égypte afin d'y demeurer, mourront par l'épée, par la famine et par la peste. Il n'y aura pour eux ni survivant, ni fugitif devant le malheur que je ferai venir sur eux.
42:18	Car ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : De même que ma colère et ma fureur se sont déversées sur les habitants de Yeroushalaim, de même mon courroux se déversera sur vous quand vous serez entrés en Égypte. Vous deviendrez une exécration, une horreur, une malédiction et une insulte, et vous ne verrez plus ce lieu.
42:19	Vous, les restes de Yéhouda, YHWH a parlé contre vous : N'allez pas en Égypte ! Sachez-le, sachez-le, je vous ai avertis aujourd'hui.
42:20	Car vous avez usé de tromperie contre vos âmes, quand vous m'avez envoyé vers YHWH, votre Elohîm, en me disant : Intercède en notre faveur auprès de YHWH, notre Elohîm, et fais-nous connaître exactement tout ce que YHWH notre Elohîm dira, et nous le ferons.
42:21	Je vous l'ai fait connaître aujourd'hui, mais vous n'écoutez pas la voix de YHWH, votre Elohîm, ni rien de tout ce pour quoi il m'a envoyé vers vous.
42:22	Maintenant sachez-le, sachez-le que vous mourrez par l'épée, par la famine et par la peste, dans le lieu où vous avez désiré d'aller pour y demeurer.

## Chapitre 43

### Désobéissance des Hébreux ; jugement sur l'Égypte

43:1	Il arriva qu'aussitôt que Yirmeyah eut fini de déclarer à tout le peuple toutes les paroles de YHWH, leur Elohîm, toutes ces paroles avec lesquelles YHWH, leur Elohîm, l'avait envoyé vers eux,
43:2	Azaryah, fils d'Hosha`yah, Yohanan, fils de Karéach, et tous ces hommes orgueilleux, parlèrent à Yirmeyah en disant : Tu dis des mensonges ! YHWH, notre Elohîm, ne t'a pas envoyé nous dire : N'allez pas en Égypte pour y demeurer.
43:3	Mais Baroukh, fils de Neriyah, t'incite contre nous, afin de nous livrer entre les mains des Chaldéens, pour nous faire mourir, et pour nous faire transporter à Babel.
43:4	Yohanan, fils de Karéach, et tous les chefs des armées, et tout le peuple n'obéirent pas à la voix de YHWH pour demeurer en terre de Yéhouda.
43:5	Yohanan, fils de Karéach, et tous les chefs des armées, prirent tous les restes de Yéhouda qui étaient revenus de toutes les nations, parmi lesquelles ils avaient été bannis, pour demeurer en terre de Yéhouda,
43:6	les hommes forts, les femmes, les enfants, les filles du roi et toutes les âmes que Nebouzaradân, le grand bourreau, avait laissées avec Gedalyah, fils d'Achikam, fils de Shaphan, ainsi que Yirmeyah, le prophète et Baroukh, fils de Neriyah,
43:7	ils allèrent en terre d'Égypte, car ils n'obéirent pas à la voix de YHWH et ils allèrent jusqu'à Tachpanès.
43:8	La parole de YHWH apparut à Yirmeyah, à Tachpanès, en disant :
43:9	Prends dans ta main de grandes pierres, et cache-les dans l'argile, dans le four à briques qui est à l'entrée de la maison de pharaon à Tachpanès, sous les yeux des hommes, des Juifs,
43:10	et dis-leur : Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, j'enverrai chercher Neboukadnetsar, roi de Babel, mon serviteur, et je mettrai son trône sur ces pierres que j'ai cachées. Il étendra son pavillon royal sur elles.
43:11	Il viendra et frappera la terre d'Égypte : à la mort ceux qui sont pour la mort, à la captivité ceux qui sont pour la captivité, à l'épée ceux qui sont pour l'épée<!--Ez. 29:9.--> !
43:12	J'allumerai le feu dans les maisons des elohîm d'Égypte. Il les brûlera et les emmènera captifs. Il s’enveloppera de la terre d'Égypte comme le berger s'enveloppe de son vêtement, et il sortira de là en paix<!--Es. 19:1 ; Ez. 30:13.-->.
43:13	Il brisera aussi les monuments de Beth-Shémesh<!--« Maison du soleil » ou « temple du soleil ».-->, qui est en terre d'Égypte, et il brûlera par le feu les maisons des elohîm d'Égypte.

## Chapitre 44

### YHWH avertit les Juifs d'Égypte<!--Jé. 43:8-13.-->

44:1	La parole qui apparut à Yirmeyah sur tous les Juifs qui demeuraient en terre d'Égypte, qui habitaient à Migdol, à Tachpanès, à Noph, et en terre de Pathros, en disant :
44:2	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Vous avez vu tous les malheurs que j'ai fait venir sur Yeroushalaim et sur toutes les villes de Yéhouda : Voici, elles ne sont plus aujourd'hui qu'une désolation, et personne n'y habite,
44:3	face au mal qu'ils ont fait pour m'irriter, en allant brûler de l'encens et servir d'autres elohîm, qu'ils n'ont pas connus, ni eux, ni vous, ni vos pères.
44:4	Et je vous ai envoyé tous mes serviteurs, les prophètes, me levant dès le matin, et les envoyant, pour vous dire : S’il vous plaît, ne faites pas cette chose abominable que je hais.
44:5	Mais ils n'ont pas écouté, ils n'ont pas prêté l'oreille pour se détourner de leur méchanceté, afin de ne pas faire brûler de l'encens à d'autres elohîm.
44:6	C'est pourquoi mon courroux et ma colère ont été versés, ils ont consumé les villes de Yéhouda et les rues de Yeroushalaim, qui sont devenues une désolation et une dévastation comme en ce jour.
44:7	Maintenant, ainsi parle YHWH, Elohîm Tsevaot, l'Elohîm d'Israël : Pourquoi faites-vous ce grand mal contre vos âmes, pour vous faire exterminer du milieu de Yéhouda, hommes et femmes, petits enfants et ceux qui tètent, afin qu'on ne vous laisse aucun reste ?
44:8	En m'irritant par les œuvres de vos mains, en brûlant de l'encens à d'autres elohîm en terre d'Égypte, où vous êtes venus pour y demeurer, afin de vous faire exterminer et d'être une malédiction et une insulte parmi toutes les nations de la Terre ?
44:9	Avez-vous oublié le mal que vos pères ont fait, le mal que les rois de Yéhouda ont fait, le mal fait par leurs femmes, le mal que vous avez vous-mêmes fait et le mal que vos femmes ont fait en terre de Yéhouda et dans les rues de Yeroushalaim ?
44:10	Jusqu'à ce jour, ils ne se sont pas humiliés, ils n'ont pas eu de crainte, ils n'ont pas marché dans ma torah et mes statuts que j'ai mis devant vous et devant vos pères.
44:11	C'est pourquoi ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, je mets mes faces contre vous pour le malheur, pour retrancher tout Yéhouda<!--Am. 9:4.-->.
44:12	Et je prendrai les restes de ceux de Yéhouda qui ont tourné les faces pour aller en terre d'Égypte afin d'y demeurer. Ils seront tous consumés. Ils tomberont en terre d'Égypte. Ils seront consumés par l'épée, par la famine, depuis le plus petit jusqu'au plus grand. Ils mourront par l'épée et par la famine. Ils deviendront une exécration, une dévastation, une malédiction et une insulte.
44:13	Je punirai ceux qui demeurent en terre d'Égypte, comme j'ai puni Yeroushalaim, par l'épée, par la famine et par la peste.
44:14	Il n'y aura personne du reste de Yéhouda qui, venu en terre d'Égypte pour y séjourner, n'échappera ou ne restera, pour retourner en terre de Yéhouda, où ils ont le désir de retourner s'établir. Car ils n'y retourneront pas, sinon ceux qui se seront échappés.
44:15	Mais tous les hommes qui savaient que leurs femmes brûlaient de l'encens à d'autres elohîm, toutes les femmes qui se tenaient là en une grande assemblée et tout le peuple qui demeurait en terre d'Égypte, à Pathros, répondirent à Yirmeyah, en disant :
44:16	Quant à la parole que tu nous as dite au Nom de YHWH, nous ne t'écouterons pas.
44:17	Mais nous agirons, nous agirons selon toute la parole qui est sortie de notre bouche, en brûlant de l'encens à la reine des cieux<!--Voir commentaire en Jé. 7:18.-->, et en lui faisant des libations, comme nous l'avons fait, nous et nos pères, nos rois et nos chefs, dans les villes de Yéhouda et dans les rues de Yeroushalaim. Nous étions rassasiés de pain, nous étions heureux et nous ne voyions pas le malheur<!--Ez. 16:24, 20:32.-->.
44:18	Mais depuis le temps où nous avons cessé de brûler de l'encens à la reine des cieux et de lui faire des libations, nous avons manqué de tout, et nous avons été consumés par l'épée et par la famine...
44:19	Et quand nous brûlions de l'encens à la reine des cieux et que nous lui faisions des libations, est-ce sans nos hommes que nous lui avons fait des gâteaux sur lesquels elle est façonnée et que nous lui avons fait des libations ?
44:20	Yirmeyah dit à tout le peuple, aux hommes forts, aux femmes et à tout le peuple qui lui avait répondu par ce discours, il leur dit :
44:21	YHWH ne s'est-il pas souvenu, ne lui est-il pas monté à cœur l'encens que vous avez brûlé dans les villes de Yéhouda et dans les rues de Yeroushalaim, vous et vos pères, vos rois, vos chefs et le peuple de la terre ?
44:22	YHWH ne pourra plus le supporter, face à la méchanceté de vos actions, face aux abominations que vous avez faites, et votre terre est devenue une désolation, des ruines et une malédiction, sans habitant comme aujourd’hui.
44:23	C'est parce que vous avez brûlé de l'encens et que vous avez péché contre YHWH, n'ayant pas écouté la voix de YHWH ni marché dans sa torah, ses statuts et ses témoignages. C’est pour cela que ce malheur vous est arrivé, comme en ce jour.
44:24	Yirmeyah dit à tout le peuple et à toutes les femmes : Vous, tout Yéhouda, qui êtes en terre d'Égypte, écoutez la parole de YHWH !
44:25	Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël, en disant : Vous et vos femmes, de votre bouche vous parlez et de vos mains vous l'accomplissez en disant : Nous ferons, nous ferons nos vœux que nous avons voués, en brûlant de l'encens à la reine des cieux et en lui versant des libations. Vous avez accompli, vous avez accompli vos vœux et vous avez fait, vous avez fait vos vœux.
44:26	C'est pourquoi, écoutez la parole de YHWH, vous, tout Yéhouda, qui demeurez en terre d'Égypte ! Voici, je le jure par mon grand Nom, dit YHWH, mon Nom ne sera plus invoqué par la bouche d'aucun homme de Yéhouda, et sur toute la terre d'Égypte aucun ne dira : Adonaï YHWH est vivant !
44:27	Voici, je veille sur eux pour le malheur et non pour le bonheur. Tous les hommes de Yéhouda qui sont en terre d'Égypte seront détruits par l'épée et par la famine, jusqu'à ce qu'ils soient consumés<!--Da. 9:14.-->.
44:28	Quelques hommes, peu nombreux, échappant à l'épée, reviendront d'Égypte en terre de Yéhouda, et tout le reste de Yéhouda venu en terre d'Égypte pour y séjourner saura quelle est la parole qui se sera accomplie : la mienne ou la leur !
44:29	Et ceci sera pour vous le signe, – déclaration de YHWH –, que je vous punirai dans ce lieu, afin que vous sachiez que mes paroles s'accompliront, elles s'accompliront pour votre malheur.
44:30	Ainsi parle YHWH : Voici, je livrerai pharaon Hophra, roi d'Égypte, entre les mains de ses ennemis, entre les mains de ceux qui cherchent son âme, comme j'ai livré Tsidqiyah, roi de Yéhouda, entre les mains de Neboukadnetsar, roi de Babel, son ennemi, et qui cherchait son âme.

## Chapitre 45

### YHWH explique son dessein à Baroukh

45:1	La parole que Yirmeyah le prophète déclara à Baroukh, fils de Neriyah, lorsqu’il écrivait dans un livre ces paroles, de la bouche de Yirmeyah, la quatrième année de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda. Il dit :
45:2	Ainsi parle YHWH, l'Elohîm d'Israël, sur toi, Baroukh :
45:3	Tu dis : Malheur à moi maintenant ! Car YHWH ajoute la tristesse à ma douleur. Je me suis lassé dans mon gémissement, et je ne trouve pas de repos.
45:4	Tu lui diras : Ainsi parle YHWH : Voici, je détruis ce que j'avais bâti, et j'arrache ce que j'avais planté, toute cette terre.
45:5	Et toi, chercherais-tu de grandes choses ? Ne les cherche pas ! Car voici, je vais faire le malheur sur toute chair, – déclaration de YHWH. Mais je te donnerai ton âme pour butin, dans tous les lieux où tu iras.

## Chapitre 46

### Prophétie contre l'Égypte

46:1	La parole de YHWH qui apparut à Yirmeyah, le prophète, sur les nations.
46:2	Sur l'Égypte. Contre l'armée de pharaon Neco, roi d'Égypte, qui était près du fleuve Euphrate, à Karkemiysh, et qui fut battue par Neboukadnetsar, roi de Babel, la quatrième année de Yehoyaqiym, fils de Yoshiyah, roi de Yéhouda<!--2 R. 24:7.-->.
46:3	Préparez le petit bouclier et le grand bouclier, approchez-vous pour la bataille !
46:4	Attelez les chevaux, montez, cavaliers ! Présentez-vous avec vos casques, polissez vos lances, revêtez l'armure !
46:5	Pourquoi les vois-je effrayés ? Ils reviennent en arrière ! Leurs hommes vaillants sont battus, ils s'enfuient, ils fuient sans regarder derrière eux... La terreur les environne, – déclaration de YHWH.
46:6	Que celui qui est rapide ne s'enfuie pas, et que le fort ne se sauve pas<!--Am. 2:14-16.--> ! Ils sont renversés et tombés vers le nord, sur la main du fleuve Euphrate.
46:7	Qui est celui qui s'élève comme le Nil et dont les eaux sont agitées comme les fleuves ?
46:8	C'est l'Égypte. Elle s'élève comme le Nil et les eaux agitées comme les fleuves. Elle dit : Je m'élèverai et je couvrirai la terre, je détruirai la ville et ceux qui y habitent.
46:9	Montez, chevaux ! Agissez en insensés, chars ! Que les hommes vaillants sortent, ceux d'Éthiopie et de Pouth qui manient le bouclier, et ceux de Loud qui manient et tendent l'arc<!--Ez. 30:5-9 ; Na. 3:9-10.--> !
46:10	C'est le jour d'Adonaï YHWH Tsevaot. C'est un jour de vengeance où il se venge de ses ennemis. L'épée dévore, elle se rassasie, elle s'enivre de leur sang. Car il y a des sacrifices pour Adonaï, YHWH Tsevaot, en terre du nord, sur le fleuve Euphrate<!--Es. 34:5-6 ; Ez. 39:17 ; So. 1:7.-->.
46:11	Monte en Galaad, prends du baume, vierge, fille de l'Égypte ! En vain tu multiplies les médicaments, il n'y a pas de guérison pour toi<!--Ez. 30:21-25 ; Na. 3:19.-->.
46:12	Les nations apprennent ta honte, et tes cris remplissent la terre, car l'homme vaillant trébuche sur l'homme vaillant, et ils tombent tous les deux ensemble.
46:13	La parole que YHWH déclara à Yirmeyah, le prophète, sur la venue de Neboukadnetsar, roi de Babel, pour frapper la terre d'Égypte :
46:14	Faites-le savoir en Égypte, faites-le entendre à Migdol, faites-le entendre à Noph et à Tachpanès ! Dites : Présente-toi, tiens-toi prêt car l'épée dévore ce qui est autour de toi !
46:15	Pourquoi tes vaillants hommes sont-ils emportés ? Aucun ne tient debout, parce que YHWH les a expulsés.
46:16	Il a multiplié ceux qui trébuchent, et même un homme tombe sur son compagnon et ils disent : Levons-nous, retournons vers notre peuple, vers la terre de notre naissance, loin de l'épée de l'oppresseur !
46:17	Là, ils s'écrient : Pharaon, roi d'Égypte, n'est qu'un vacarme ! Il a laissé passer le temps fixé.
46:18	Moi, le Vivant, – déclaration du Roi, dont le Nom est YHWH Tsevaot. Comme le Thabor entre les montagnes, comme le Carmel qui s'avance dans la mer, ainsi viendra-t-il.
46:19	Fille, habitante de l'Égypte, fais tes bagages pour la captivité ! Car Noph deviendra un désert, elle sera brûlée, elle n'aura plus d'habitants.
46:20	L'Égypte est une très belle génisse... La destruction vient, elle vient du nord.
46:21	Même les mercenaires sont au milieu d'elle comme des veaux engraissés. Et eux aussi tournent le dos, ils fuient tous sans résister. Car le jour de leur calamité, le temps de leur châtiment est venu sur eux.
46:22	Elle sifflera comme un serpent, car ils marcheront avec une armée, ils viendront contre elle avec des haches, comme des bûcherons.
46:23	Ils couperont sa forêt, – déclaration de YHWH –, parce qu’elle est impénétrable, car ils sont plus nombreux que les sauterelles, on ne saurait la compter.
46:24	La fille de l'Égypte est confuse, elle est livrée entre les mains du peuple du nord.
46:25	YHWH Tsevaot, l'Elohîm d'Israël, dit : Voici, je vais punir Amon<!--elohîm égyptien, à l'origine elohîm local de Thèbes, plus tard à la tête du panthéon égyptien.--> de No<!--L'ancienne capitale de l'Égypte. Voir Na. 3:8-->, pharaon, l'Égypte, ses elohîm et ses rois, pharaon et ceux qui se confient en lui.
46:26	Et je les livrerai entre les mains de ceux qui cherchent leur âme, entre les mains de Neboukadnetsar, roi de Babel, et entre les mains de ses serviteurs ; mais après cela, l'Égypte sera habitée comme aux temps passés, – déclaration de YHWH.
46:27	Et toi, Yaacov, mon serviteur, n'aie pas peur ! Ne t'épouvante pas Israël ! Car voici, je te sauverai de la terre lointaine, je sauverai ta postérité de la terre de leur captivité. Yaacov reviendra, il sera en repos et en paix, et il n'y aura personne qui lui fasse peur.
46:28	Toi, Yaacov, mon serviteur, n'aie pas peur ! – déclaration de YHWH. Oui, je suis avec toi, oui, je ferai une extermination de toutes les nations où je t’ai banni, mais en ce qui te concerne je ne ferai pas d’extermination, mais je te châtierai avec justice, je ne t'innocenterai pas, je ne t'innocenterai pas.

## Chapitre 47

### Prophétie contre la Philistie et la Phénicie

47:1	La parole de YHWH qui apparut à Yirmeyah, le prophète, contre les Philistins, avant que pharaon ne frappe Gaza.
47:2	Ainsi parle YHWH : Voici des eaux montent du nord, elles deviennent comme un torrent qui submerge. Elles submergeront la terre et ce qui la remplit, les villes et leurs habitants. Les hommes poussent des cris, et tous les habitants de la terre se lamentent,
47:3	au bruit du battement de pieds, du sabot de ses puissants, au tremblement de ses chars et au tumulte de ses roues. Les pères ne se tournent pas vers leurs fils, à cause de la diminution de leurs mains,
47:4	à cause du jour qui vient détruire tous les Philistins, retrancher de Tyr et de Sidon tout survivant qui venait en aide. Oui, YHWH va détruire les Philistins, les restes de l'île de Kaphtor.
47:5	La calvitie vient vers Gaza, Askalon est perdue, le reste de leur plaine aussi. Jusqu'à quand te feras-tu des incisions ?
47:6	Ah ! Épée de YHWH, quand te reposeras-tu ? Rentre dans ton fourreau, repose-toi, et sois tranquille !
47:7	Comment te reposerais-tu ? Car YHWH lui donne ses ordres, il l'a assignée contre Askalon et contre le rivage de la mer.

## Chapitre 48

### Prophétie sur Moab

48:1	Sur Moab. Ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Malheur à Nebo, car elle est dévastée ! Qiryathayim est honteuse, elle est prise. Misgab est honteuse et brisée.
48:2	Non, la louange de Moab n’est plus. À Hesbon, on a projeté du mal contre elle : Allons, exterminons-la, qu'elle ne soit plus une nation ! Toi aussi, Madmen, tu seras détruite. L'épée marche derrière toi.
48:3	Bruit, cri de détresse, de Choronaïm : Un ravage, une grande ruine !
48:4	Moab est brisée ! On entend les cris des plus jeunes.
48:5	Pleurs sur pleurs s'élèveront à la montée de Louhith, car on entendra à la descente de Choronaïm<!--Es. 15:5.--> ceux qui crieront de détresse à cause des plaies que les ennemis leur auront faites.
48:6	Fuyez, sauvez vos âmes ! Devenez pareils au genévrier dans le désert ! 
48:7	En effet, parce que tu t'es confié dans tes ouvrages et dans tes trésors, tu seras pris, et Kemosh sortira pour être transporté avec ses prêtres et ses chefs<!--Es. 46:1-7.-->.
48:8	Le dévastateur entrera dans toutes les villes et aucune ville n'échappera. La vallée périra et la plaine sera détruite comme YHWH l'a dit.
48:9	Donnez des ailes à Moab, et qu'elle parte en volant ! Ses villes seront réduites en désert, elles n'auront plus d'habitants.
48:10	Maudit soit celui qui fait l'œuvre de YHWH avec paresse, maudit soit celui qui garde son épée du sang !
48:11	Moab était tranquille depuis sa jeunesse, elle reposait sur sa lie, elle n'était pas vidée de vase en vase, et elle n'allait pas en captivité. C'est pourquoi sa saveur lui est restée, et son odeur ne s'est pas changée.
48:12	Mais voici, les jours viennent, – déclaration de YHWH –, où je lui enverrai des transvaseurs qui la transvaseront, ils videront ses vases et briseront ses outres.
48:13	Moab aura honte de Kemosh, comme la maison d'Israël a eu honte de Béth-El, sa confiance.
48:14	Comment dites-vous : Nous sommes de vaillants hommes, des soldats prêts à combattre ?
48:15	Moab est dévastée, et chacune de ses villes monte en fumée, l'élite de sa jeunesse est descendue pour le massacre, – déclaration du Roi, dont le Nom est YHWH Tsevaot.
48:16	La calamité de Moab est proche, son malheur avance à grands pas.
48:17	Vous tous qui êtes autour d'elle, ayez de la compassion pour elle, et vous tous qui connaissez son nom, dites : Comment a été brisé le bâton de force la verge d'honneur ?
48:18	Toi qui te tiens chez la fille de Dibon, descends de ta gloire, et assieds-toi dans un lieu desséché ! Car le dévastateur de Moab monte contre toi, il détruit tes forteresses.
48:19	Habitante d'Aroër, tiens-toi sur le chemin, et regarde ! Interroge celle qui s'enfuit, celle qui s'échappe. Dis : Qu'est-il arrivé ?
48:20	Moab est confuse, car elle est brisée. Poussez des gémissements et des cris<!--Es. 15:5, 16:7.--> ! Rapportez dans Arnon que Moab est dévastée.
48:21	Le jugement est venu sur la terre de la plaine, sur Holon, sur Yahats, sur Méphaath,
48:22	sur Dibon, sur Nebo, sur Beth-Diblathayim,
48:23	sur Qiryathayim, sur Beth-Gamoul, sur Beth-Meon,
48:24	sur Qeriyoth, sur Botsrah, sur toutes les villes de la terre de Moab, éloignées et proches.
48:25	La force de Moab est abattue, et son bras est brisé, – déclaration de YHWH.
48:26	Enivrez-la car elle s'est élevée contre YHWH ! Moab se jettera sur son vomissement et deviendra aussi un objet de dérision !
48:27	Israël n’est-il pas devenu pour toi un objet de dérision ? Avait-il été trouvé parmi les voleurs ? Car tu t’agitais chaque fois que tu parlais de lui. 
48:28	Habitants de Moab, quittez les villes et demeurez dans les rochers ! Devenez comme les colombes qui font leur nid aux côtés de l'entrée des cavernes !
48:29	Nous avons appris l'orgueil de Moab, il est très orgueilleux, sa hauteur, son orgueil, son arrogance et son cœur hautain<!--Es. 16:6 ; So. 2:9-10.-->.
48:30	Je connais son arrogance, – déclaration de YHWH. Ses discours et ses œuvres sont vains !
48:31	C’est pourquoi sur Moab je hurlerai, sur Moab tout entier je crierai. On gémira sur les hommes de Kir-Hérès.
48:32	Vignoble de Sibma, je pleure sur toi du pleur de Ya`azeyr. Tes rameaux allaient au-delà de la mer, ils atteignaient la Mer de Ya`azeyr. Le dévastateur s'est jeté sur tes fruits d'été et sur ta vendange.
48:33	L'allégresse et la joie ont été retirées du verger et de la terre de Moab. J'ai fait cesser le vin des cuves : on n'y foulera plus en chantant. Il y a des cris de guerre, et non des cris de joie<!--Es. 16:10.-->.
48:34	Les cris de Hesbon parviennent jusqu'à Élealé, et ils font entendre leurs cris jusqu'à Yahats, même depuis Tsoar jusqu'à Choronaïm, jusqu'à Églath-Sheliyshiyah. Car les eaux de Nimrim deviendront une horreur.
48:35	Je ferai cesser en Moab, – déclaration de YHWH –, celui qui offre sur les hauts lieux et qui brûle de l'encens à ses elohîm.
48:36	C'est pourquoi mon cœur murmurera sur Moab, comme des flûtes. Mon cœur murmurera comme des flûtes sur les hommes de Kir-Hérès. C'est pourquoi tous les biens qu'ils ont acquis ont péri.
48:37	Car toutes les têtes sont chauves, toutes les barbes sont coupées. Il y a des incisions sur toutes les mains, et sur les reins des sacs.
48:38	Il y aura des gémissements sur tous les toits de Moab et dans ses places, parce que j'aurai brisé Moab comme un vase auquel on ne prend aucun plaisir, – déclaration de YHWH.
48:39	Ils hurleront : Comment a-t-il été brisé ! Comment Moab a-t-il tourné le dos, tout honteux ! Car Moab deviendra un objet de dérision et de frayeur pour tous ceux qui sont autour de lui.
48:40	Car ainsi parle YHWH : Voici, il vole comme un aigle, et il étend ses ailes sur Moab.
48:41	Qeriyoth est prise, les forteresses sont saisies, et le cœur des hommes vaillants de Moab est en ce jour comme le cœur d'une femme qui est en travail.
48:42	Et Moab sera exterminée, elle ne sera plus un peuple, parce qu'elle s'est élevée contre YHWH.
48:43	Habitant de Moab, la terreur, la fosse, et le filet sont sur toi ! – déclaration de YHWH.
48:44	Le fuyard, face à la terreur, tombera dans la fosse et celui qui monte de la fosse sera pris au filet, car je fais venir sur lui, sur Moab, l'année de son châtiment, – déclaration de YHWH<!--Es. 24:18.-->.
48:45	Ils se sont arrêtés à l'ombre de Hesbon à bout de forces, mais le feu sort de Hesbon, une flamme du milieu de Sihon. Elle dévore les flancs de Moab et le sommet de la tête des fils du vacarme<!--No. 21:28.-->.
48:46	Malheur à toi, Moab ! Le peuple de Kemosh est perdu ! Car tes fils sont enlevés et emmenés captifs, et tes filles ont été emmenées captives.
48:47	Je ramènerai les captifs de Moab dans les derniers jours<!--Ge. 49:1-2.-->, – déclaration de YHWH. Jusqu’ici le jugement de Moab.

## Chapitre 49

### Prophétie sur Ammon

49:1	Sur les fils d'Ammon. Ainsi parle YHWH : Israël n'a-t-il pas de fils ? N'a-t-il pas d'héritier ? Pourquoi Malcom hérite-t-il de Gad, et pourquoi son peuple demeure-t-il dans ses villes ?
49:2	C'est pourquoi voici, les jours viennent, dit YHWH, où je ferai entendre l'alarme de guerre contre Rabbath des fils d'Ammon. Elle sera réduite en un monceau de ruines, et ses filles seront brûlées par le feu. Israël dépossédera ceux qui l'avaient dépossédé, – déclaration de YHWH.
49:3	Hurle, Hesbon, car Aï est dévastée ! Poussez des cris, filles de Rabba, ceignez-vous de sacs, lamentez-vous, courez çà et là le long des murailles ! Car Malcom s'en va en captivité avec ses prêtres et ses chefs.
49:4	Pourquoi te vantes-tu de tes vallées ? Ta vallée se fond, fille apostate, qui te confiais dans tes trésors : Qui viendra contre moi ?
49:5	Voici, je fais venir sur toi la terreur de tous les alentours, – déclaration d'Adonaï YHWH Tsevaot. Vous serez bannis chacun çà et là, et il n'y aura personne qui rassemblera les fuyards.
49:6	Mais après cela, je ramènerai les captifs des fils d'Ammon, – déclaration de YHWH.

### Prophétie sur Édom

49:7	Sur Édom. Ainsi parle YHWH Tsevaot : N'y a-t-il plus de sagesse dans Théman ? Le conseil a-t-il manqué aux hommes intelligents ? Leur sagesse s'est-elle évanouie<!--Ab. 1:8.--> ?
49:8	Fuyez ! Détournez-vous ! Habitez dans les lieux profonds, habitants de Dedan ! Car je fais venir sa calamité sur Ésav, le temps où je le visiterai.
49:9	Si des vendangeurs venaient chez toi, ne laisseraient-ils pas des grappillages ? Si c’étaient des voleurs de nuit, ils ne détruiraient que ce qui leur suffirait.
49:10	Mais c’est moi qui mettrai à nu Ésav, je découvrirai ses lieux secrets, il ne pourra se cacher. Sa postérité, ses frères et ses voisins périront, et il ne sera plus.
49:11	Abandonne tes orphelins, c'est moi qui les ferai vivre, et que tes veuves se confient en moi !
49:12	Car ainsi parle YHWH : Voici, ceux que le jugement ne condamnait pas à boire la coupe, la boiront, la boiront, et toi, tu resterais impuni, impuni ? Tu ne resteras pas impuni, mais tu la boiras, tu la boiras.
49:13	Car j’ai juré par moi-même, – déclaration de YHWH –, que Botsrah deviendra des ruines, une insulte, une sécheresse et une malédiction, et que toutes ses villes deviendront une désolation éternelle.
49:14	J'ai entendu de YHWH une nouvelle, et un messager a été envoyé parmi les nations : Rassemblez-vous et venez contre elle ! Levez-vous pour la guerre !
49:15	Car voici, je te rendrai petit parmi les nations, méprisé parmi les humains.
49:16	Ton frissonnement t’a trompé, ainsi que l'orgueil de ton cœur, toi qui habites dans le creux des rochers, toi qui occupes le sommet des collines. Quand tu élèverais ton nid comme l'aigle, je t'en ferai descendre, – déclaration de YHWH.
49:17	Édom deviendra une désolation. Quiconque passera près d'elle sera étonné, et sifflera à cause de toutes ses plaies.
49:18	Comme dans la destruction de Sodome<!--Ge. 19:25 ; Am. 4:11.--> et de Gomorrhe, et des villes voisines, a dit YHWH, pas un homme n'habitera là, il n'y résidera pas un fils d'humain.
49:19	Voici, comme un lion il monte de l'orgueil du Yarden contre la demeure durable. Oui, en un instant, je le ferai courir loin d'elle, et j'y établirai celui que j'ai choisi. Oui, qui est comme moi ? Qui m'assignerait ? Quel est ce berger qui se tiendrait debout en face de moi<!--Job 41:2.--> ?
49:20	C'est pourquoi, écoutez le conseil de YHWH, le conseil qu'il a donné contre Édom, et les desseins qu'il a projetés contre les habitants de Théman ! Si les petits du troupeau ne les traînent ! S’il ne réduit en désolation leur demeure sur eux.
49:21	La terre tremble au bruit de leur chute. Le bruit de leur cri de détresse se fait entendre jusqu'à la Mer Rouge...
49:22	Voici, il montera comme un aigle, il volera, et il étendra ses ailes sur Botsrah, et le cœur des hommes vaillants d'Édom sera en ce jour-là comme le cœur d'une femme qui est en travail.

### Prophétie sur Damas

49:23	Sur Damas. Hamath et Arpad sont honteuses parce qu'elles ont entendu de très mauvaises nouvelles, elles tremblent. Il y a une tourmente dans la mer qui ne peut se calmer.
49:24	Damas est défaillante, elle se tourne pour fuir, et la panique la saisit. L'angoisse et les douleurs la saisissent comme une femme qui enfante.
49:25	Comment n'est-elle pas abandonnée, la ville de louange, la cité de ma joie ?
49:26	C’est pourquoi ses jeunes hommes tomberont dans les places, et tous ses hommes de guerre périront en ce jour-là, – déclaration de YHWH Tsevaot.
49:27	Je mettrai le feu à la muraille de Damas, qui dévorera les palais de Ben-Hadad.

### Prophétie sur Qedar (les Arabes) et Hatsor

49:28	Sur Qedar et les royaumes de Hatsor, que Neboukadnetsar, roi de Babel, frappera. Ainsi parle YHWH : Levez-vous, montez vers Qedar et détruisez les fils d'orient !
49:29	On prendra leurs tentes et leurs troupeaux, et on prendra leurs tapis, tous leurs bagages et leurs chameaux, et l'on jettera de toutes parts contre eux des cris de terreur.
49:30	Fuyez, agitez-vous extrêmement, habitez dans les lieux profonds, vous, habitants de Hatsor ! – déclaration de YHWH –, car Neboukadnetsar, roi de Babel, a formé un projet contre vous, il a préparé un plan contre vous.
49:31	Levez-vous, montez vers la nation tranquille qui habite en sécurité, – déclaration de YHWH. Elle n'a ni portes ni barres, elle habite seule<!--Ez. 38:11.-->.
49:32	Leurs chameaux deviendront un butin, et la multitude de leur bétail, une proie. Je les disperserai à tout vent, vers ceux qui se coupent le coin de la barbe, et je ferai venir de tous les côtés leur calamité, – déclaration de YHWH.
49:33	Hatsor deviendra le repaire des dragons et un désert pour toujours. Personne n'y habitera, et aucun fils d'humain n'y séjournera.

### Prophétie sur Éylam

49:34	La parole de YHWH qui apparut à Yirmeyah, le prophète, contre Éylam, au commencement du règne de Tsidqiyah, roi de Yéhouda, en disant :
49:35	Ainsi parle YHWH Tsevaot : Voici, je vais briser l'arc d'Éylam, qui est leur principale force<!--Ez. 32:24-27.-->.
49:36	Et je ferai venir contre Éylam les quatre vents des quatre extrémités des cieux, et je les disperserai par tous ces vents. Il n'y aura pas une nation où ne viennent ceux qui seront bannis d'Éylam éternellement.
49:37	Et je ferai trembler les habitants d'Éylam devant leurs ennemis, et devant ceux qui cherchent leur âme. Je ferai venir du mal sur eux, l'ardeur de ma colère, – déclaration de YHWH. J'enverrai l'épée après eux, jusqu'à ce que je les aie consumés.
49:38	Je mettrai mon trône dans Éylam, et j'en détruirai les rois et les chefs, – déclaration de YHWH.
49:39	Il arrivera qu'aux derniers jours<!--Ge. 49:1-2.-->, je ferai revenir les captifs d'Éylam, – déclaration de YHWH.

## Chapitre 50

### Prophétie sur Babel

50:1	La parole que YHWH déclara contre Babel, contre la terre des Chaldéens, par la main de Yirmeyah, le prophète :
50:2	Annoncez-le parmi les nations, faites-le entendre, levez une bannière ! Faites-le entendre, ne le cachez pas ! Dites : Babel est prise ! Bel est couvert de honte, Merodak est brisé ! Ses idoles sont couvertes de honte, ses idoles sont brisées<!--Es. 46:1.--> !
50:3	Car une nation monte contre elle du nord. Elle mettra sa terre en désolation, et il n'y aura plus personne qui y habite. Les hommes et les bêtes fuient, ils s'en vont.
50:4	En ces jours-là, en ce temps-là, – déclaration de YHWH –, les fils d'Israël viendront, eux et les fils de Yéhouda ensemble, ils marcheront allant et pleurant, et cherchant YHWH, leur Elohîm.
50:5	Ils demanderont le chemin de Sion et tourneront vers elle leurs faces : Venez et joignons-nous à YHWH par une alliance éternelle qui ne sera jamais oubliée.
50:6	Mon peuple était devenu un troupeau de brebis perdues. Leurs bergers les égaraient, ils les faisaient tourner en rond dans les montagnes ; elles allaient de montagne en colline, oubliant leur lieu de repos<!--Ez. 34:5-6 ; Za. 10:2 ; Mt. 9:36.-->.
50:7	Tous ceux qui les trouvaient les dévoraient, et leurs ennemis disaient : Nous ne sommes coupables d'aucun mal, parce qu'ils ont péché contre YHWH, contre la demeure de la justice, contre YHWH, l'espérance de leurs pères.
50:8	Fuyez du milieu de Babel et sortez de la terre des Chaldéens ! Soyez comme des boucs qui vont devant le troupeau<!--Es. 48:20 ; 2 Co. 6:17 ; Ap. 18:4.--> !
50:9	Car voici, je vais susciter et faire monter contre Babel une multitude de grandes nations de la terre du nord, qui se rangeront en bataille contre elle, de sorte qu'elle sera prise. Leurs flèches seront comme celles d'un homme puissant, qui ne fait que détruire, et qui ne retourne pas à vide<!--Es. 13:18.-->.
50:10	La Chaldée deviendra un butin. Tous ceux qui la pilleront seront rassasiés, – déclaration de YHWH.
50:11	Parce que vous vous êtes réjouis, parce que vous vous êtes égayés, en ravageant mon héritage, parce que vous avez bondi comme une génisse qui foule l'herbe, et que vous avez henni comme de puissants chevaux.
50:12	Votre mère est extrêmement honteuse, celle qui vous a enfantés a rougi. Voici, elle sera la dernière entre les nations, elle sera un désert, une sécheresse, une région aride.
50:13	Elle ne sera plus habitée à cause de la colère de YHWH, elle ne sera plus qu'une désolation. Quiconque passera près de Babel sera étonné et sifflera à cause de toutes ses plaies.
50:14	Rangez-vous en bataille contre Babel, mettez-vous tout autour, vous tous qui tendez l'arc ! Tirez contre elle et n'épargnez pas les flèches ! Car elle a péché contre YHWH.
50:15	Poussez des cris de guerre contre elle tout autour ! Elle tend les mains. Ses fondements tombent, ses murs sont renversés. Car c'est la vengeance de YHWH. Vengez-vous sur elle ! Faites-lui comme elle a fait<!--Ab. 1:15 ; Ps. 137:8 ; Lu. 6:38.--> !
50:16	Retranchez de Babel le semeur, et celui qui manie la faucille au temps de la moisson ! Face à l’épée de l'oppresseur, que chaque homme se tourne vers son peuple, que chaque homme s'enfuie vers sa terre !
50:17	Israël est une brebis égarée que les lions ont bannie. Le roi d'Assyrie l'a dévorée le premier, mais ce dernier, Neboukadnetsar, roi de Babel, lui a brisé les os.
50:18	C'est pourquoi ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : Voici, je vais punir le roi de Babel et sa terre, comme j'ai puni le roi d'Assyrie<!--2 R. 19:35 ; Es. 37:36.-->.
50:19	Je ferai revenir Israël dans sa demeure. Il paîtra au Carmel et au Bashân, son âme se rassasiera sur la montagne d'Éphraïm et de Galaad.
50:20	En ces jours-là, en ce temps-là, – déclaration de YHWH –, on cherchera l'iniquité d'Israël, mais il n'y en aura pas, et les péchés de Yéhouda, mais ils ne seront pas trouvés, car je pardonnerai au reste que j'aurai laissé.
50:21	Monte contre la terre de « la double rébellion »<!--« Merathayim » qui signifie : « double rébellion ». C'est un autre nom de Babel. Ap. 18:6.-->, contre elle et contre les habitants destinés à la visitation ! Dévaste et détruis entièrement après eux, – déclaration de YHWH –, et fais selon tout ce que je t'ai ordonné !
50:22	Cris de guerre sur terre, grande ruine !
50:23	Comment est-il mis en pièces et rompu, le marteau de toute la terre ! Comment Babel est-elle réduite en sujet d'étonnement parmi les nations !
50:24	Je t'ai tendu un piège et tu as été prise, Babel, sans que tu le saches ! Tu as été trouvée et même attrapée, parce que tu as lutté contre YHWH.
50:25	YHWH a ouvert son arsenal et en a sorti les armes de sa colère, car c'est l'œuvre d'Adonaï, de YHWH Tsevaot, en terre des Chaldéens.
50:26	Venez contre elle des confins, ouvrez ses entrepôts, entassez-la comme des gerbes, dévouez-la par interdit, et qu'il n'en reste rien !
50:27	Attaquez tous ses taureaux et qu'ils descendent au massacre ! Malheur à eux ! Car le jour est venu, le temps de leur visitation.
50:28	Cris de ceux qui s'enfuient, de ceux qui se sont échappés de la terre de Babel pour annoncer dans Sion la vengeance de YHWH, notre Elohîm, la vengeance de son temple !
50:29	Convoquez les archers contre Babel ! Vous tous qui bandez l'arc, campez contre elle tout autour ! Que personne n'échappe ! Rendez-lui selon ses œuvres, faites-lui selon tout ce qu’elle a fait ! Car elle a été arrogante envers YHWH, envers le Saint d'Israël<!--Es. 13:11 ; Joë. 4:4-9 ; La. 1:22. Voir Ac. 3:14.-->.
50:30	C'est pourquoi, ses jeunes hommes tomberont dans les places, et tous ses hommes de guerre périront en ce jour-là, – déclaration de YHWH.
50:31	Me voici contre toi, orgueilleuse ! déclaration d'Adonaï YHWH Tsevaot, car ton jour est venu, le temps où je te visiterai.
50:32	L'orgueilleuse chancellera et tombera, et il n'y aura personne pour la relever. Je mettrai le feu à ses villes, et il dévorera tous ses environs.
50:33	Ainsi parle YHWH Tsevaot : Les fils d'Israël et les fils de Yéhouda sont ensemble opprimés. Tous ceux qui les ont emmenés captifs les retiennent, et refusent de les laisser aller.
50:34	Leur Racheteur est fort, son Nom est YHWH Tsevaot. Il plaidera, il plaidera leur cause, afin de donner du repos à la terre et de faire trembler les habitants de Babel.
50:35	L'épée est sur les Chaldéens, – déclaration de YHWH –, sur les habitants de Babel, sur ses chefs et sur ses sages !
50:36	L'épée est tirée contre ses devins, ils deviendront insensés ! L'épée est sur ses hommes vaillants et ils seront épouvantés !
50:37	L'épée est sur ses chevaux ! Sur ses chars et sur tout le peuple mélangé<!--« entrelacé », « matériel tricoté », « mélange », « personnes mixtes », « société mixte ».--> qui est au milieu d'elle ! Ils deviendront des femmes ! L'épée est sur ses trésors et ils seront pillés !
50:38	La sécheresse contre ses eaux ! Qu'elles soient mises à sec ! Car c'est une terre d'images gravées ! Ils agissent comme des fous devant leurs terreurs<!--Es. 2:8.--> !
50:39	C'est pourquoi les bêtes sauvages des déserts y habiteront avec les chacals, et les filles de l'autruche y habiteront aussi. Elle ne sera plus jamais habitée, et d’âge en âge on n’y demeurera plus.
50:40	Comme dans la destruction par Elohîm de Sodome et de Gomorrhe, et de ses villes voisines, – déclaration de YHWH –, elle ne sera plus habitée par des hommes et aucun fils d'humain n'y séjournera.
50:41	Voici, un peuple vient du nord, une grande nation et beaucoup de rois se réveillent des extrémités de la Terre.
50:42	Ils saisissent l'arc et le javelot. Ils sont cruels, et ils n'ont pas de compassion. Leur voix mugit comme la mer, et ils sont montés sur des chevaux, chacun d'eux est rangé en bataille comme un seul homme, contre toi, fille de Babel !
50:43	Le roi de Babel a entendu leur rumeur, ses mains se sont relâchées, l'angoisse l'a saisi, des douleurs comme celles d'une femme qui accouche...
50:44	Voici, comme un lion il monte de l'orgueil du Yarden contre la demeure durable. Oui, en un instant, je les ferai courir loin d’elle. Qui est l'homme choisi que j'établirai sur elle ? Oui, qui est comme moi ? Qui m’assignerait ? Quel est ce berger qui se tiendrait debout en face de moi ?
50:45	C'est pourquoi, écoutez le conseil de YHWH, qu’il a conseillé contre Babel, et les projets qu'il a projetés contre la terre des Chaldéens ! Si les petits du troupeau ne les traînent ! S’il ne réduit en désolation leur demeure sur eux !
50:46	Au bruit de la prise de Babel la terre est ébranlée, et il y a un cri, entendu parmi les nations.

## Chapitre 51

### Le jugement de Babel par YHWH

51:1	Ainsi parle YHWH : Voici, je vais faire lever un vent de destruction contre Babel, et contre ceux qui habitent au milieu de ceux qui s'élèvent contre moi.
51:2	J'enverrai contre Babel des vanneurs qui la vanneront, et qui videront sa terre, car de tous côtés ils seront contre elle, au jour du malheur.
51:3	Qu'on bande l'arc contre celui qui bande son arc et contre celui qui s'élève dans son armure ! N'épargnez pas ses jeunes hommes ! Exterminez à la façon de l'interdit toute son armée !
51:4	Les blessés mortellement tomberont en terre des Chaldéens, et les transpercés dans ses rues.
51:5	Car Israël et Yéhouda ne sont pas abandonnés de leur Elohîm, de YHWH Tsevaot. Oui, leur terre est pleine de culpabilité contre le Saint<!--Voir commentaire en Ac. 3:14.--> d'Israël.
51:6	Fuyez hors de Babel<!--Voir Es. 48:20 et Ap. 18:4.--> et que chacun sauve son âme ! Ne soyez pas exterminés dans son iniquité ! Car c'est le temps de la vengeance de YHWH, il lui rend selon les produits de ses mains.
51:7	Babel était une coupe d'or dans la main de YHWH, enivrant toute la Terre<!--Voir Ap. 17:1-18.-->. Les nations ont bu de son vin, c'est pourquoi les nations sont devenues insensées.
51:8	Babel est tombée<!--Ap. 18.--> en un instant, elle est brisée ! Hurlez sur elle, prenez du baume pour sa douleur : peut-être guérira-t-elle !
51:9	Nous avons pansé Babel, mais elle n'est pas guérie. Laissez-la et allons-nous-en chacun vers sa terre, car son jugement atteint les cieux et s'élève jusqu'aux nuages.
51:10	YHWH a fait sortir notre justice. Venez et racontons dans Sion l'œuvre de YHWH, notre Elohîm.
51:11	Polissez les flèches, remplissez les boucliers ! YHWH a réveillé l'esprit des rois de Médie, car son dessein est contre Babel pour la détruire, car c'est la vengeance de YHWH, la vengeance de son temple.
51:12	Élevez une bannière sur les murs de Babel, renforcez la garnison, établissez les gardes, préparez les embuscades, car YHWH a formé un dessein, même il a fait ce qu'il a dit contre les habitants de Babel.
51:13	Toi qui habites sur beaucoup d'eaux, abondante en trésors, ta fin est venue et ton profit est à son terme.
51:14	YHWH Tsevaot a juré en son âme, en disant : Certainement je te remplirai d'hommes comme de sauterelles, et ils pousseront un cri contre toi.
51:15	Il a fait la Terre par sa puissance, fondé le monde par sa sagesse et étendu les cieux par son intelligence<!--Ge. 1:1 ; Es. 40:22 ; Ps. 104:2 ; Job 9:8.-->.
51:16	Au son de sa voix, il se fait un tumulte d'eaux dans les cieux. Il fait monter les vapeurs des extrémités de la Terre, il produit les éclairs et la pluie, et il fait sortir le vent de ses réservoirs.
51:17	Tout humain est abruti par sa connaissance, tout fondeur est honteux par les idoles. En effet, ses idoles en métal fondu ne sont que mensonge, il n'y a pas de souffle en elles.
51:18	Elles ne sont que vanité et une œuvre de tromperie. Elles périront au temps de leur visitation.
51:19	Celui qui est la portion de Yaacov n'est pas comme ces choses-là, car c'est celui qui a tout formé, et Israël est la tribu de son héritage. Son Nom est YHWH Tsevaot.
51:20	Tu as été pour moi un marteau, une arme de guerre. Par toi j’ai brisé les nations, par toi j'ai détruit les royaumes.
51:21	Par toi j’ai brisé le cheval et son cavalier. Par toi j’ai brisé le char et celui qui était monté dessus.
51:22	Par toi j’ai brisé l'homme et la femme. Par toi j’ai brisé le vieillard et le garçon. Par toi j'ai brisé le jeune homme et la vierge.
51:23	Par toi j'ai mis en pièces le berger et son troupeau. Par toi j’ai brisé le laboureur et ses bœufs accouplés. Par toi j’ai brisé les gouverneurs et les magistrats.
51:24	Mais je rendrai à Babel, et à tous les habitants de la Chaldée, tout le mal qu'ils ont fait à Sion sous vos yeux, – déclaration de YHWH<!--La. 1:21.-->.
51:25	Me voici contre toi, montagne de destruction, – déclaration de YHWH –, qui détruis toute la Terre. J'étendrai ma main sur toi et je te roulerai du haut des rochers, et je ferai de toi une montagne embrasée.
51:26	On ne prendra plus de toi ni pierre angulaire ni pierre pour fondements, car tu seras réduite en désolations perpétuelles, – déclaration de YHWH<!--Es. 13:19-20.-->...
51:27	Élevez une bannière sur la terre, sonnez du shofar parmi les nations ! Préparez les nations contre elle ! Appelez contre elle les royaumes d'Ararat, de Minni et d'Ashkenaz ! Établissez contre elle des scribes ! Faites monter ses chevaux comme des sauterelles hérissées !
51:28	Préparez contre elle les nations, les rois de Médie, ses gouverneurs et tous ses magistrats, et toute la terre sous leur domination<!--Es. 13:17.--> !
51:29	La terre tremble, elle est en travail, car les desseins de YHWH s'accomplissent contre Babel, pour faire de la terre de Babel des ruines sans habitants<!--Es. 13:14 ; Joë. 4:16.-->.
51:30	Les hommes vaillants de Babel ont cessé de combattre, ils sont restés dans les forteresses, leur force est épuisée, ils sont devenus des femmes. On a mis le feu à leurs tabernacles et leurs barres sont brisées.
51:31	Le courrier viendra à la rencontre du courrier, et le messager viendra à la rencontre du messager, pour annoncer au roi de Babel que sa ville est prise de tous côtés,
51:32	que ses gués sont saisis, que ses marais sont brûlés par le feu et que les hommes de guerre sont terrifiés.
51:33	Car ainsi parle YHWH Tsevaot, l'Elohîm d'Israël : La fille de Babel est comme une aire. Il est temps qu'elle soit foulée. Encore un peu, et le temps de sa moisson viendra.
51:34	Neboukadnetsar, roi de Babel, m'a dévorée et m'a écrasée. Il m'a mise dans le même état qu'un vase vide. Il m'a engloutie comme un dragon, il a rempli son ventre de mes délices. Il m'a chassée au loin.
51:35	Que la violence envers moi et ma chair déchirée retombe sur Babel ! dit l'habitante de Sion. Que mon sang retombe sur les habitants de la Chaldée ! dit Yeroushalaim.
51:36	C'est pourquoi ainsi parle YHWH : Voici, je vais plaider ta cause, et je ferai la vengeance pour toi ; je dessécherai sa mer, et je ferai tarir sa source.
51:37	Et Babel sera réduite en monceau de ruines, en un repaire de dragons<!--Ap. 18:1-2.-->, elle sera un sujet d'étonnement et de moquerie. Il n'y aura plus d'habitants.
51:38	Ils rugiront ensemble comme des lions et pousseront des cris comme des lionceaux.
51:39	Je les ferai échauffer dans leurs festins et les enivrerai afin qu'ils se réjouissent, qu'ils dorment d'un sommeil éternel et qu'ils ne se réveillent plus, – déclaration de YHWH.
51:40	Je les ferai descendre comme des agneaux à la boucherie, comme les moutons avec les boucs.
51:41	Comment ! Sheshak est prise, elle est saisie, la louange de toute la terre ! Comment ! Babel est devenue une désolation parmi les nations !
51:42	La mer est montée sur Babel, elle a été couverte de la multitude de ses flots<!--Es. 8:8 ; Ez. 26:3-19 ; Lu. 21:25.-->.
51:43	Ses villes sont devenues des ruines, une terre sèche et une région aride, une terre où personne ne demeure, et où il ne passe pas un fils d'humain.
51:44	Je punirai aussi Bel à Babel, je ferai sortir de sa bouche ce qu'il a englouti, et les nations n’afflueront plus vers lui. Le mur même de Babel est tombé !
51:45	Mon peuple, sortez du milieu d'elle<!--Voir 2 Co. 6:17 et Ap. 18:4.-->, et que chaque homme sauve son âme de l'ardeur de la colère de YHWH !
51:46	De peur que votre cœur ne s'amollisse, et que vous n'ayez peur des nouvelles qu'on entendra sur toute la terre. Car dans telle année, la nouvelle viendra, et, après elle, dans telle autre année, il y aura la nouvelle, et violence sur la terre, et un dominateur contre un autre dominateur.
51:47	C'est pourquoi voici, les jours viennent où je punirai les images gravées de Babel, et toute sa terre sera couverte de honte. Tous ses blessés mortellement tomberont au milieu d'elle.
51:48	Les cieux et la Terre, et tout ce qui y est, pousseront des cris de joie contre Babel, parce qu'il viendra du nord des dévastateurs contre elle, – déclaration de YHWH.
51:49	Et comme Babel a fait tomber les blessés à mort d'Israël, ainsi les blessés mortellement de toute la Terre tomberont à Babel.
51:50	Vous qui avez échappé à l'épée, allez, ne vous arrêtez pas ! De loin souvenez-vous de YHWH et que Yeroushalaim vous monte au cœur !
51:51	Nous sommes honteux, car nous avons entendu des insultes. La confusion a couvert nos faces, car des étrangers sont entrés dans les sanctuaires de la maison de YHWH.
51:52	C'est pourquoi, voici, les jours viennent, – déclaration de YHWH –, où je visiterai ses images gravées, et les blessés à mort gémiront sur toute sa terre.
51:53	Quand Babel serait montée jusqu'aux cieux, et qu'elle aurait fortifié le plus haut de sa forteresse, toutefois les dévastateurs y entreront par moi, – déclaration de YHWH<!--Am. 9:2 ; Ab. 1:4.-->...
51:54	Un grand cri s'entend de Babel, et une grande ruine en terre des Chaldéens.
51:55	Parce que YHWH va détruire Babel et faire périr du milieu d'elle la grande voix. Leurs flots mugiront comme de grandes eaux, le vacarme de leur voix retentira.
51:56	Car le dévastateur est venu contre elle, contre Babel. Ses hommes vaillants sont pris, leurs arcs sont brisés. Car YHWH est le El des rétributions, il paie, il paie.
51:57	J'enivrerai ses princes et ses sages, ses gouverneurs, ses magistrats et ses hommes vaillants. Ils dormiront d'un sommeil perpétuel et ils ne se réveilleront plus, – déclaration du Roi dont le Nom est YHWH Tsevaot.
51:58	Ainsi parle YHWH Tsevaot : Les larges murs de Babel seront rasés, ils seront rasés. Et ses hautes portes seront brûlées par le feu. Ainsi les peuples auront travaillé en vain, et les nations se seront épuisées pour du feu.
51:59	Voici l'ordre que Yirmeyah, le prophète, donna à Serayah, fils de Neriyah, fils de Machseyah, quand il alla avec Tsidqiyah, roi de Yéhouda, à Babel, la quatrième année de son règne. Or Serayah était premier chambellan.
51:60	Yirmeyah écrivit dans un livre tout le mal qui devait venir sur Babel, toutes ces paroles qui sont écrites contre Babel.
51:61	Yirmeyah dit à Serayah : Quand tu arriveras à Babel et que tu verras et liras toutes ces paroles,
51:62	tu diras : YHWH ! Tu as parlé contre ce lieu pour qu'il soit retranché et qu'il ne soit plus habité par les humains ni par les bêtes, mais qu'il soit réduit en désolations perpétuelles.
51:63	Il arrivera que quand tu auras achevé de lire ce livre, tu le lieras à une pierre<!--Ap. 18:21.--> et tu le jetteras dans l'Euphrate,
51:64	et tu diras : C'est ainsi que Babel s'enfoncera, elle ne se relèvera pas du malheur que je vais faire venir sur elle, et ils seront lassés. Jusqu'ici sont les paroles de Yirmeyah.

## Chapitre 52

### Chute de Yeroushalaim (Jérusalem) et destruction du temple ; Yéhouda déportée à Babel<!--2 R. 25:1-26 ; Jé. 39:1-10.-->

52:1	Tsidqiyah était fils de 21 ans quand il devint roi, et il régna 11 ans à Yeroushalaim. Sa mère se nommait Hamoutal, elle était fille de Yirmeyah, de Libnah<!--2 R. 24 et 25.-->.
52:2	Il fit ce qui est mal aux yeux de YHWH, comme avait fait Yehoyaqiym.
52:3	Car la colère de YHWH était sur Yeroushalaim et sur Yéhouda, jusqu’à ce qu’il les eût rejetés loin de ses faces. Et Tsidqiyah se rebella contre le roi de Babel.
52:4	Il arriva, la neuvième année de son règne, le dixième jour du dixième mois, que Neboukadnetsar, roi de Babel, vint contre Yeroushalaim, lui et toute son armée. Ils campèrent devant elle et construisirent des retranchements tout autour.
52:5	La ville fut assiégée jusqu'à la onzième année du roi Tsidqiyah.
52:6	Et le neuvième jour du quatrième mois, la famine était si forte dans la ville qu'il n'y avait plus de pain pour le peuple de la terre<!--La. 2:11-12.-->.
52:7	Une brèche fut faite à la ville et tous les hommes de guerre s'enfuirent et sortirent de nuit de la ville par le chemin de la porte entre les deux murailles, près du jardin du roi, pendant que les Chaldéens étaient contre la ville, tout autour. Ils s'en allèrent par le chemin de la région aride.
52:8	L'armée des Chaldéens poursuivit le roi et rattrapa Tsidqiyah dans les régions arides de Yeriycho. Toute son armée se dispersa loin de lui.
52:9	Ils prirent le roi et le firent monter vers le roi de Babel à Riblah, en terre de Hamath, où il prononça contre lui un jugement.
52:10	Le roi de Babel fit tuer les fils de Tsidqiyah sous ses yeux. Il fit aussi tuer tous les chefs de Yéhouda à Riblah.
52:11	Et il creva les yeux de Tsidqiyah et le lia avec une double chaîne de cuivre. Le roi de Babel le fit venir à Babel et le mit dans la maison de garde jusqu'au jour de sa mort.
52:12	Le dixième jour du cinquième mois, la dix-neuvième année du règne de Neboukadnetsar, roi de Babel, Nebouzaradân, le grand bourreau, qui se tenait devant le roi de Babel, entra dans Yeroushalaim.
52:13	Il brûla la maison de YHWH, la maison du roi et toutes les maisons de Yeroushalaim. Il brûla par le feu toutes les maisons des grands.
52:14	Toute l'armée des Chaldéens qui était avec le grand bourreau abattit toutes les murailles qui étaient autour de Yeroushalaim.
52:15	Nebouzaradân, le grand bourreau, emmena captifs quelques-uns des pauvres du peuple, le reste du peuple qui était resté dans la ville, ceux qui étaient tombés dans les mains, tombés dans les mains du roi de Babel, avec le reste de la multitude.
52:16	Nebouzaradân, le grand bourreau, laissa parmi les pauvres de la terre, les vignerons et les laboureurs.
52:17	Les Chaldéens mirent en pièces les colonnes en cuivre qui étaient dans la maison de YHWH, ainsi que les bases et la mer de cuivre qui était dans la maison de YHWH, et ils en transportèrent tout le cuivre à Babel.
52:18	Ils prirent aussi les chaudrons, les pelles, les mouchettes, les cuvettes, les coupes et tous les ustensiles de cuivre avec lesquels on faisait le service.
52:19	Le grand bourreau prit aussi les bassins, les encensoirs, les cuvettes, les chaudrons, les chandeliers, les coupes et les coupes sacrificielles, aussi bien ce qui était en or que ce qui était en argent.
52:20	Quant aux deux colonnes, à la mer et aux 12 bœufs en cuivre qui servaient de bases, ceux que le roi Shelomoh avait faits pour la maison de YHWH, on ne pesa pas le cuivre de tous ces ustensiles-là.
52:21	Quant aux colonnes, la hauteur d'une colonne était de 18 coudées, et un cordon de 12 coudées en mesurait le tour et son épaisseur était de 4 doigts. Elle était creuse.
52:22	Il y avait au-dessus un chapiteau en cuivre. La hauteur d'un des chapiteaux était de 5 coudées ; autour du chapiteau il y avait un treillis et des grenades, le tout en cuivre. Il en était de même pour la seconde colonne avec des grenades<!--1 R. 7:15-20.-->.
52:23	Il y avait 96 grenades sur le côté d'où vient le vent. Toutes les grenades qui étaient autour du treillis étaient au nombre de 100.
52:24	Le grand bourreau prit Serayah, qui était le prêtre en tête, Tsephanyah, qui était le second prêtre et les trois gardiens du seuil.
52:25	De la ville il prit un eunuque qui était commissaire des hommes de guerre, 7 hommes parmi ceux qui voyaient les faces du roi et qui furent trouvés dans la ville, le scribe du chef de l'armée qui enrôlait le peuple de la terre et 60 hommes d'entre le peuple de la terre qui furent trouvés dans la ville.
52:26	Nebouzaradân, le grand bourreau, les prit et les conduisit au roi de Babel à Riblah.
52:27	Le roi de Babel les frappa et les fit mourir à Riblah, en terre de Hamath. Yéhouda fut emmené en exil loin de son sol.
52:28	Voici le peuple que Neboukadnetsar emmena en exil : la septième année, 3 023 Juifs.
52:29	La dix-huitième année de Neboukadnetsar : de Yeroushalaim, 832 âmes.
52:30	La vingt-troisième année de Neboukadnetsar, Nebouzaradân, le grand bourreau, emmena en exil 745 âmes d'entre les Juifs. En tout 4 600 âmes.
52:31	Or il arriva la trente-septième année de la captivité de Yehoyakiyn, roi de Yéhouda, le vingt-cinquième jour du douzième mois, qu'Évil-Merodak, roi de Babel, dans l’année de son règne, éleva la tête de Yehoyakiyn, roi de Yéhouda, et le fit sortir de la maison de détention.
52:32	Il lui parla avec bonté et mit son trône au-dessus du trône des rois qui étaient avec lui à Babel.
52:33	Et après qu'il eut changé ses vêtements de prisonnier, il mangea du pain constamment en sa présence, tous les jours de sa vie.
52:34	Quant à sa ration, une ration continuelle lui fut accordée selon la parole du roi de Babel, jour par jour, tous les jours de sa vie, jusqu'au jour de sa mort.
