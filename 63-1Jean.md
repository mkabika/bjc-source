# 1 Yohanan (1 Jean) (1 Jn.)

Signification : YHWH a fait grâce

Auteur : Yohanan (Jean)

(Gr. : Ioannes)

Thème : La communion fraternelle, la connaissance et l'amour

Date de rédaction : Env. 85 ap. J.-C.

Cette lettre, écrite par Yohanan à Éphèse, était destinée aux assemblées de la province d'Asie qu'il connaissait bien. Il souhaite rendre leur joie parfaite en fortifiant leur foi dans le Mashiah (Christ) et en leur donnant l'assurance de la vie éternelle ; tout en les mettant en garde contre les faux docteurs.

## Chapitre 1

### La Parole personnifiée

1:1	Ce qui était dès le commencement, ce que nous avons entendu, ce que nous avons vu de nos propres yeux, ce que nous avons contemplé et que nos propres mains ont touché concernant la Parole de vie,
1:2	et la vie a été manifestée, et nous avons vu et nous rendons témoignage, et nous vous annonçons la vie éternelle qui était vers le Père et qui nous a été manifestée.

### La communion avec le Père et le Fils

1:3	Ce que nous avons vu et entendu, nous vous l'annonçons, afin que vous aussi vous ayez communion avec nous. Or, notre communion est avec le Père et avec son Fils Yéhoshoua Mashiah.
1:4	Et nous vous écrivons ces choses afin que votre joie soit accomplie.
1:5	Et c’est ici la promesse que nous avons entendue de lui et que nous vous annonçons, c'est qu'Elohîm est lumière et la ténèbre n’est aucunement en lui.
1:6	Si nous disons que nous avons communion avec lui alors que nous marchons dans la ténèbre, nous mentons et nous ne pratiquons pas la vérité.
1:7	Mais si nous marchons dans la lumière, comme lui-même est dans la lumière, nous avons communion les uns avec les autres, et le sang de son Fils Yéhoshoua Mashiah nous purifie de tout péché.

### La confession des péchés, le pardon et la purification

1:8	Si nous disons que nous n'avons pas de péché, nous nous égarons nous-mêmes, et la vérité n'est pas en nous.
1:9	Si nous confessons nos péchés, il est fidèle et juste pour nous les remettre et pour nous purifier de toute injustice.
1:10	Si nous disons que nous n'avons pas péché, nous le faisons menteur et sa parole n'est pas en nous.

## Chapitre 2

### Le Mashiah, notre parakletos (avocat)

2:1	Mes petits enfants, je vous écris ces choses afin que vous ne péchiez pas. Et si quelqu'un a péché, nous avons un parakletos<!--Yéhoshoua, notre parakletos. Le mot grec signifie « convoqué », « appelé aux côtés », « appelé à l'aide » ; « celui qui plaide la cause d'un autre », « un juge », « un plaideur », « un conseil pour la défense », « un assistant légal », « un avocat ». Ce mot se trouve également en Jn. 14 et 16, où il est appliqué au Saint-Esprit. Le Seigneur exerce actuellement la fonction de parakletos ou d'Avocat pour nous au ciel. Voir Ro. 8:33 ; Hé. 7:25.--> auprès du Père, Yéhoshoua Mashiah le Juste.
2:2	Et il est lui-même la propitiation<!--Vient du grec « hilasmos » qui veut dire aussi « un apaisement », « le moyen d'apaiser ». Le propitiatoire était le couvercle de l'arche de l'alliance. Une fois par an, le grand-prêtre en exercice aspergeait le propitiatoire de sang expiatoire pour la purification des péchés.--> au sujet de nos péchés, et non seulement au sujet des nôtres, mais aussi au sujet de ceux de tout le monde.

### Garder les commandements d'Elohîm

2:3	Et en ceci nous savons que nous l'avons connu si nous gardons ses commandements.
2:4	Celui qui dit : Je l'ai connu et qui ne garde pas ses commandements est un menteur, et la vérité n'est pas en lui.
2:5	Mais celui qui garde sa parole, l'amour d'Elohîm est véritablement parfait en lui. C'est par cela que nous savons que nous sommes en lui.
2:6	Celui qui dit qu'il demeure en lui doit aussi marcher comme lui-même marche.
2:7	Frères, je ne vous écris pas un commandement nouveau, mais un commandement ancien, que vous avez eu dès le commencement. Et ce commandement ancien c'est la parole que vous avez entendue dès le commencement.
2:8	De nouveau, je vous écris un commandement nouveau, ce qui est vrai en lui et en vous, parce que la ténèbre est passée et que la véritable lumière brille déjà.
2:9	Celui qui dit être dans la lumière tout en haïssant son frère est dans la ténèbre jusqu'à présent.
2:10	Celui qui aime son frère demeure dans la lumière et il n'y a pas en lui de scandale.
2:11	Mais celui qui hait son frère est dans la ténèbre et il marche dans la ténèbre, et il ne sait pas où il va parce que la ténèbre a aveuglé ses yeux.

### La famille spirituelle

2:12	Petits enfants, je vous écris parce que vos péchés vous sont remis à cause de son Nom.
2:13	Pères, je vous écris parce que vous avez connu celui qui est dès le commencement. Jeunes, je vous écris parce que vous avez remporté la victoire sur le Mauvais. Enfants, je vous écris parce que vous avez connu le Père.
2:14	Pères, je vous ai écrit parce que vous avez connu celui qui est dès le commencement. Jeunes, je vous ai écrit parce que vous êtes forts et que la parole d'Elohîm demeure en vous, et que vous avez remporté la victoire sur l'esprit du Mauvais.

### Les enfants d'Elohîm ne doivent pas aimer le monde

2:15	N'aimez pas le monde, ni les choses qui sont dans le monde. Si quelqu'un aime le monde, l'amour du Père n'est pas en lui.
2:16	Parce que tout ce qui est dans le monde, la convoitise de la chair, et la convoitise des yeux, et l'orgueil de la vie, n'est pas du Père, mais cela est du monde.
2:17	Et le monde passe avec sa convoitise, mais celui qui fait la volonté d'Elohîm demeure pour l'éternité.

### Les enfants d'Elohîm mis en garde contre les anti-mashiah<!--Antichrists.-->

2:18	Enfants, c'est ici la dernière<!--« Dernière » du grec « eschatos » signifie « dernier dans une succession dans le temps ». Voir Ge. 49:1-2.--> heure. Et comme vous avez entendu que l'Anti-Mashiah<!--Antichrist.--> viendra, maintenant aussi beaucoup d'anti-mashiah sont apparus. De là nous savons que c'est la dernière heure.
2:19	Ils sont sortis de chez nous, mais ils n'étaient pas de chez nous, car s'ils avaient été de chez nous, ils seraient restés avec nous. Mais c'est afin qu'il soit manifeste que tous ne sont pas de chez nous.
2:20	Et vous, vous avez l'onction de la part du Saint<!--Yéhoshoua est le Saint dont Iyov (Job) n'avait pas transgressé la loi (Job 6:10). Voir Es. 45:25 ; Mc. 1:24 ; Lu. 4:34. Voir commentaire en Ac. 3:14.-->, et vous connaissez toutes choses.
2:21	Je vous écris, non pas parce que vous ne connaissez pas la vérité, mais parce que vous la connaissez, et parce qu'aucun mensonge n'est de la vérité.
2:22	Qui est le menteur, sinon celui qui nie que Yéhoshoua est le Mashiah ? Celui-là est l'Anti-Mashiah, qui nie le Père et le Fils.
2:23	Quiconque nie le Fils n'a pas non plus le Père. Quiconque confesse le Fils a aussi le Père.
2:24	Vous donc, que ce que vous avez entendu dès le commencement demeure en vous. Si ce que vous avez entendu dès le commencement demeure en vous, vous aussi vous demeurerez dans le Fils et dans le Père.
2:25	Et c’est ici la promesse que lui-même nous a promise : La vie éternelle.
2:26	Je vous ai écrit ces choses au sujet de ceux qui vous égarent.
2:27	Et vous, l'onction que vous avez reçue de lui demeure en vous, et vous n'avez pas besoin qu'on vous enseigne. Mais comme la même onction vous enseigne toutes choses et qu'elle est vraie et n'est pas un mensonge, et selon qu'elle vous l'a enseigné, vous demeurerez en lui.

### Exhortation à demeurer en Yéhoshoua (Jésus)

2:28	Et maintenant, petits enfants, demeurez en lui, afin que quand il apparaîtra, nous ayons de l'assurance, et que nous ne soyons pas couverts de honte, loin de lui, en sa parousie. 
2:29	Si vous savez qu'il est juste, sachez que quiconque pratique la justice a été engendré de lui.

## Chapitre 3

3:1	Voyez quel amour le Père nous a donné, pour que nous soyons appelés enfants d'Elohîm. Raison pour laquelle le monde ne nous connaît pas, parce qu'il ne l'a pas connu.
3:2	Bien-aimés, nous sommes maintenant enfants d'Elohîm, et ce que nous serons n'est pas encore manifesté. Or nous savons que lorsqu'il sera manifesté, nous serons semblables à lui, parce que nous le verrons tel qu'il est.
3:3	Et quiconque a cette espérance en lui se purifie, comme lui-même est pur.

### Les enfants d'Elohîm et les enfants du diable

3:4	Quiconque pratique le péché, pratique aussi la violation de la torah et le péché est la violation de la torah<!--« Violation de la torah », du grec « anomia ». Le terme grec traduit également la condition de celui qui est sans loi parce qu'il en est ignorant. Il signifie aussi « iniquité » ou encore « méchanceté ». Voir 2 Th. 2:7-8.-->.
3:5	Et vous savez que lui, il a été manifesté, afin qu'il ôtât nos péchés et il n'y a pas de péché en lui.
3:6	Quiconque demeure en lui ne pèche pas. Quiconque pèche ne l'a pas vu et ne l'a pas connu.
3:7	Petits enfants, que personne ne vous égare. Celui qui pratique la justice est juste comme lui-même est juste.
3:8	Celui qui pratique le péché est issu du diable, parce que, dès le commencement, le diable pèche. C'est pour cela que le Fils d'Elohîm s'est manifesté<!--Voir 1 Ti. 3:16 ; Hé. 9:8 ; 1 Jn. 1:2.--> afin de détruire les œuvres du diable.
3:9	Quiconque a été engendré d'Elohîm ne pratique pas le péché, parce que la semence d'Elohîm demeure en lui, et il ne peut pécher, parce qu'il a été engendré d'Elohîm.
3:10	En ceci se manifestent les enfants d'Elohîm et les enfants du diable : quiconque ne pratique pas la justice et n'aime pas son frère n'est pas d'Elohîm.
3:11	Car tel est le message que vous avez entendu dès le commencement : que nous nous aimions les uns les autres,
3:12	non pas comme Qayin<!--Caïn.-->, qui était du Mauvais et qui tua son frère. Et pourquoi le tua-t-il ? C'est parce que ses œuvres étaient mauvaises, et que celles de son frère étaient justes.
3:13	Frères, ne vous étonnez pas si le monde vous hait.
3:14	Nous savons que nous sommes passés de la mort à la vie, parce que nous aimons les frères. Celui qui n'aime pas le frère demeure dans la mort.
3:15	Quiconque hait son frère est un meurtrier, et vous savez qu'aucun meurtrier n'a la vie éternelle demeurant en lui.
3:16	En ceci nous avons connu l'amour, c'est que lui, il a déposé sa vie en notre faveur. Nous aussi, nous devons déposer nos vies en faveur de nos frères<!--Jn. 15:13.-->.
3:17	Mais celui qui possède le bien de ce monde, et qui voit son frère dans le besoin, et qui lui ferme ses entrailles, comment l'amour d'Elohîm demeure-t-il en lui ?
3:18	Mes petits enfants, n'aimons pas en parole, ni avec la langue, mais en œuvre et en vérité.
3:19	Et c'est en cela que nous connaissons que nous sommes de la vérité et nous persuaderons nos cœurs devant lui.
3:20	Parce que si notre cœur nous condamne, Elohîm est plus grand que notre cœur et il connaît toutes choses.
3:21	Bien-aimés, si notre cœur ne nous condamne pas, nous avons de l'assurance devant Elohîm.
3:22	Et quoi que nous demandions, nous le recevons de lui, parce que nous gardons ses commandements et que nous faisons les choses qui sont agréables devant lui.
3:23	Et c'est ici son commandement : que nous croyions au Nom de son Fils Yéhoshoua Mashiah, et que nous nous aimions les uns les autres, selon le commandement qu'il nous a donné.
3:24	Et celui qui garde ses commandements demeure en lui, et lui en celui-là. Et par là nous connaissons qu'il demeure en nous par l'Esprit qu'il nous a donné.

## Chapitre 4

### Éprouver les esprits

4:1	Bien-aimés, ne croyez pas tout esprit, mais éprouvez les esprits, s'ils sont d'Elohîm, parce que beaucoup de faux prophètes sont venus dans le monde.
4:2	À ceci vous connaissez l'Esprit d'Elohîm : tout esprit qui confesse que Yéhoshoua Mashiah est venu en chair est d'Elohîm,
4:3	et tout esprit qui ne confesse pas que Yéhoshoua Mashiah est venu en chair n'est pas d'Elohîm : c'est celui de l'Anti-Mashiah<!--Antichrist.-->, dont vous avez entendu dire qu'il vient, et maintenant il est déjà dans le monde.
4:4	Petits enfants, vous êtes d'Elohîm, et vous avez remporté la victoire sur eux, parce que celui qui est en vous est plus grand que celui qui est dans le monde.
4:5	Eux, ils sont du monde. C'est pourquoi ils parlent comme étant du monde, et le monde les écoute.
4:6	Nous, nous sommes issus d'Elohîm. Celui qui connaît Elohîm nous écoute, celui qui n'est pas issu d'Elohîm ne nous écoute pas. C'est par là que nous connaissons l'Esprit de vérité et l'esprit de l'égarement.

### L'amour d'Elohîm

4:7	Bien-aimés, aimons-nous les uns les autres, parce que l'amour est issu d'Elohîm, et quiconque aime a été engendré d'Elohîm et connaît Elohîm.
4:8	Celui qui n’aime pas n’a pas connu Elohîm, parce qu'Elohîm est amour<!--« Agapé » en grec.-->.
4:9	L'amour d'Elohîm a été manifesté envers nous en ce qu'Elohîm a envoyé son Fils unique dans le monde, afin que nous vivions à travers lui.
4:10	En ceci est l'amour, non en ce que nous avons aimé Elohîm, mais que lui nous a aimés, et qu'il a envoyé son Fils en propitiation<!--Du grec « hilasmos » qui signifie « apaisement ». Les Écritures nous parlent aussi du « propitiatoire », c'est-à-dire « le siège de la miséricorde » ou « lieu de l'expiation ». Le propitiatoire était une plaque en or du sommet de l'arche de l'alliance. Le grand-prêtre l'aspergeait sept fois le jour de l'expiation afin de réconcilier symboliquement YHWH et son peuple. Voir Ex. 25:17-22.--> au sujet de nos péchés.
4:11	Bien-aimés, si Elohîm nous a ainsi aimés, nous devons aussi nous aimer les uns les autres.
4:12	Personne n'a jamais vu Elohîm. Si nous nous aimons les uns les autres, Elohîm demeure en nous, et son amour est parfait en nous.
4:13	En ceci nous savons que nous demeurons en lui et lui en nous, parce qu'il nous a donné de son Esprit.
4:14	Et nous, nous avons vu, et nous témoignons que le Père a envoyé le Fils, le Sauveur du monde.
4:15	Quiconque confessera que Yéhoshoua est le Fils d'Elohîm, Elohîm demeure en lui, et lui en Elohîm.
4:16	Et nous, nous avons connu et cru en l'amour qu'Elohîm a pour nous. Elohîm est amour et celui qui demeure dans l'amour demeure en Elohîm, et Elohîm en lui.
4:17	C’est en cela que l’amour est rendu parfait avec nous, afin que nous ayons de l’assurance au jour du jugement, car tel il est, tels nous sommes dans ce monde.
4:18	Il n'y a pas de peur dans l'amour, mais l'amour parfait bannit la peur, parce que la peur implique un châtiment. Et celui qui a peur n'est pas rendu parfait dans l’amour.
4:19	Nous l'aimons, parce qu'il nous a aimés le premier.
4:20	Si quelqu’un dit : J’aime Elohîm, et qu’il haïsse son frère, il est un menteur, car celui qui n’aime pas son frère qu’il a vu, ne peut pas aimer Elohîm qu’il n’a pas vu.
4:21	Et nous avons ce commandement de sa part : que celui qui aime Elohîm, aime aussi son frère.

## Chapitre 5

5:1	Quiconque croit que Yéhoshoua est le Mashiah a été engendré d'Elohîm, et quiconque aime celui qui a engendré, aime aussi celui qui a été engendré de lui.
5:2	À ceci, nous savons que nous aimons les enfants d'Elohîm, lorsque nous aimons Elohîm et que nous gardons ses commandements.
5:3	Car ceci est l'amour d'Elohîm : que nous gardions ses commandements. Et ses commandements ne sont pas pesants<!--Voir Mt. 23:4.-->,

### La foi triomphe du monde

5:4	parce que tout ce qui a été engendré d'Elohîm remporte la victoire<!--Voir Ro. 12:21.--> sur le monde. Et c'est ici la victoire qui a vaincu le monde : notre foi.
5:5	Qui donc est celui qui remporte la victoire sur le monde, sinon celui qui croit que Yéhoshoua est le Fils d'Elohîm ?

### Le témoignage d'Elohîm

5:6	C'est ce Yéhoshoua ha Mashiah qui est venu à travers l'eau et le sang ; non pas avec l'eau seulement, mais avec l'eau et le sang. Et c'est l'Esprit qui rend témoignage, or l'Esprit est la vérité.
5:7	Parce qu’il y en a trois qui rendent témoignage dans le ciel : le Père, la Parole et le Saint-Esprit, et ces trois-là sont un<!--Elohîm est un. Voir De. 6:4.-->.
5:8	Il y en a aussi trois qui rendent témoignage sur la Terre : l'Esprit, l'eau et le sang, et ces trois-là sont pour un.
5:9	Si nous recevons le témoignage des humains, le témoignage d'Elohîm est plus grand, parce que tel est le témoignage d'Elohîm qu'il a rendu au sujet de son Fils.
5:10	Celui qui croit au Fils d'Elohîm a le témoignage en lui-même. Celui qui ne croit pas Elohîm fait de lui un menteur, parce qu'il ne croit pas au témoignage qu'Elohîm a rendu au sujet de son Fils.
5:11	Et c'est ici le témoignage : qu'Elohîm nous a donné la vie éternelle, et cette vie est dans son Fils.
5:12	Celui qui a le Fils a la vie, celui qui n'a pas le Fils d'Elohîm n'a pas la vie.
5:13	Je vous ai écrit ces choses, à vous qui croyez au Nom du Fils d'Elohîm, afin que vous sachiez que vous avez la vie éternelle, et afin que vous croyiez au Nom du Fils d'Elohîm.
5:14	Et c'est ici l'assurance que nous avons auprès de lui, que si nous demandons quelque chose selon sa volonté, il nous écoute.
5:15	Et si nous savons qu'il nous écoute, quoi que ce soit que nous demandions, nous savons que nous avons les requêtes que nous lui avons demandées.
5:16	Si quelqu'un voit son frère péchant d'un péché qui n'est pas vers la mort<!--Le péché qui mène à la mort, c'est le blasphème contre le Saint-Esprit (voir le dictionnaire en annexe).-->, il demandera et il lui donnera la vie : pour ceux dont le péché n'est pas vers la mort. Il y a un péché qui est vers la mort. Ce n'est pas au sujet de celui-là que je dis de prier.
5:17	Toute injustice est un péché, mais il y a un péché qui n'est pas vers la mort.
5:18	Nous savons que quiconque a été engendré d'Elohîm ne pèche pas. Mais celui qui a été engendré d'Elohîm se garde lui-même, et le Mauvais ne le touche pas.
5:19	Nous savons que nous sommes d'Elohîm et que le monde entier se tient<!--« Être couché », « mis dans le pouvoir du mal, c'est-à-dire tenu en soumission par le diable ».--> dans le mal.

### Conclusion

5:20	Mais nous savons que le Fils d'Elohîm est venu, et il nous a donné l'intelligence<!--L'esprit comme faculté de compréhension. Voir Ep. 1:18, 4:18.--> pour connaître le Véritable. Et nous sommes dans le Véritable, en son Fils Yéhoshoua Mashiah. Il est le Véritable<!--Dans Jn. 17:3, le Père est présenté comme le Véritable Elohîm ; le terme grec traduit par « véritable » ou « vrai » dans l'évangile de Yohanan (Jean) est aussi appliqué à Yéhoshoua dans ce passage. Yéhoshoua est donc le vrai Elohîm.--> Elohîm et la vie éternelle.
5:21	Petits enfants, gardez-vous des idoles ! Amen.
