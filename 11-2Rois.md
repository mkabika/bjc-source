# 2 Melakhim (2 Rois) (2 R.)

Signification : Roi, règne

Auteur : Inconnu

Thème : Suite de l'histoire d'Israël et de Yéhouda (Juda)

Date de rédaction : 6ème siècle av. J.-C.

Le livre de 2 Melakhim s'articule autour de la vie d'Éliysha (Élisée), serviteur d'Éliyah (Élie), devenu dorénavant son successeur. On y découvre le service prophétique au travers duquel Elohîm se révéla comme le Tout-Puissant, l'Elohîm compatissant, le Maître des temps et des circonstances, le Libérateur, l'Elohîm de la résurrection, le Puissant Guerrier et aussi le Juge.

Ce livre relate l'histoire des derniers rois, la chute d'Israël et sa captivité, la destruction de Yeroushalaim (Jérusalem) par Neboukadnetsar, roi de Babel (Babylone), en 586 av. J.-C., et la captivité de Yéhouda (Juda).

## Chapitre 1

### Jugement de YHWH sur Achazyah, roi d'Israël

1:1	Moab se révolta contre Israël après la mort d'Achab.
1:2	Or Achazyah tomba par le treillis de sa chambre haute qui était à Samarie, et il en fut malade. Il envoya des messagers et leur dit : Allez, consultez Baal-Zeboub<!--Baal-Zeboub était une divinité des Philistins adorée à Ékron qui se nommait aussi Béelzéboul (Mt. 10:25).-->, elohîm d'Ékron : survivrai-je à cette maladie ?
1:3	Mais l'Ange de YHWH dit à Éliyah<!--Élie. Voir 1 R. 17.--> le Tishbiy : Lève-toi, monte à la rencontre des messagers du roi de Samarie et dis-leur : N'y a-t-il pas d'Elohîm en Israël pour que vous alliez consulter Baal-Zeboub, elohîm d'Ékron ?
1:4	C'est pourquoi ainsi parle YHWH : Tu ne descendras pas du lit sur lequel tu es monté, mais tu mourras, tu mourras<!--Voir commentaire en Ge. 2:16.-->. Et Éliyah s'en alla.
1:5	Les messagers retournèrent vers Achazyah. Et il leur dit : Pourquoi revenez-vous ?
1:6	Ils lui dirent : Un homme est monté à notre rencontre et nous a dit : Allez, retournez vers le roi qui vous a envoyés et dites-lui : Ainsi parle YHWH : N'y a-t-il pas d'Elohîm en Israël pour que tu envoies consulter Baal-Zeboub, elohîm d'Ékron ? À cause de cela, tu ne descendras pas du lit sur lequel tu es monté, mais tu mourras, tu mourras.
1:7	Achazyah leur dit : Comment était cet homme qui est monté à votre rencontre et qui vous a dit ces paroles ?
1:8	Ils lui dirent : C'était un homme qui possédait un vêtement de poil, avec une ceinture de cuir autour des reins. Et Achazyah dit : C'est Éliyah le Tishbiy.

### Affirmation de l'autorité d'Éliyah (Élie)

1:9	Il envoya vers lui un chef de cinquante avec ses 50 hommes. Celui-ci monta vers lui et voici, il demeurait au sommet de la montagne, et il lui dit : Homme d'Elohîm, le roi a dit : Descends !
1:10	Éliyah répondit et dit au chef de cinquante : Si je suis un homme d'Elohîm, que le feu descende des cieux et te consume, toi et tes 50 hommes ! Et le feu descendit des cieux et le consuma, lui et ses 50 hommes.
1:11	De nouveau il envoya vers lui un autre chef de cinquante avec ses 50 hommes. Celui-ci répondit et dit à Éliyah : Homme d'Elohîm, ainsi parle le roi : Hâte-toi de descendre !
1:12	Éliyah répondit et leur dit : Si je suis un homme d'Elohîm, que le feu descende des cieux et te consume, toi et tes 50 hommes ! Et le feu d'Elohîm descendit des cieux et le consuma, lui et ses 50 hommes.
1:13	De nouveau il envoya un troisième chef de cinquante avec ses 50 hommes. Ce troisième chef de 50 hommes monta et vint se mettre à genoux devant Éliyah, le suppliant, en disant : Homme d'Elohîm, s’il te plaît, que mon âme et l’âme de ces 50 hommes, tes serviteurs, soient précieuses à tes yeux !
1:14	Voici, le feu est descendu des cieux et a consumé les deux premiers chefs de cinquante avec leurs 50 hommes. Mais maintenant, que mon âme soit précieuse à tes yeux !
1:15	L'Ange de YHWH dit à Éliyah : Descends avec lui, n'aie pas peur de lui. Éliyah se leva et descendit avec lui vers le roi.
1:16	Il lui dit : Ainsi parle YHWH : Parce que tu as envoyé des messagers pour consulter Baal-Zeboub, elohîm d'Ékron, comme s'il n'y avait pas d'Elohîm en Israël pour consulter sa parole, tu ne descendras pas du lit sur lequel tu es monté, mais tu mourras, tu mourras.

### Mort d'Achazyah ; Yehoram (Yoram) règne sur Israël

1:17	Achazyah mourut selon la parole de YHWH déclarée par Éliyah. Et Yehoram régna à sa place, la seconde année de Yehoram, fils de Yehoshaphat, roi de Yéhouda, parce qu'Achazyah n'avait pas de fils.
1:18	Le reste des actions d'Achazyah et ce qu'il a fait, cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?

## Chapitre 2

### Enlèvement d'Éliyah aux cieux

2:1	Or il arriva lorsque YHWH enleva Éliyah aux cieux dans un tourbillon, qu'Éliyah et Éliysha<!--Élisée.--> partaient de Guilgal.
2:2	Éliyah dit à Éliysha : S'il te plaît, reste ici, car YHWH m'envoie jusqu'à Béth-El. Mais Éliysha dit : YHWH est vivant et ton âme est vivante ! Je ne te quitterai pas ! Ainsi ils descendirent à Béth-El.
2:3	Les fils des prophètes qui étaient à Béth-El sortirent vers Éliysha, et lui dirent : Ne sais-tu pas qu'aujourd'hui YHWH va enlever ton maître au-dessus de ta tête ? Et il dit : Je le sais aussi, taisez-vous !
2:4	Éliyah lui dit : Éliysha, s'il te plaît, reste ici, car YHWH m'envoie à Yeriycho. Mais Éliysha lui dit : YHWH est vivant et ton âme est vivante ! Je ne te quitterai pas ! Ainsi, ils arrivèrent à Yeriycho.
2:5	Les fils des prophètes qui étaient à Yeriycho s'approchèrent d'Éliysha, et lui dirent : Ne sais-tu pas qu'aujourd'hui YHWH va enlever ton maître au-dessus de ta tête ? Et il dit : Je le sais aussi, taisez-vous !
2:6	Éliyah lui dit : Éliysha, s'il te plaît, reste ici, car YHWH m'envoie jusqu'au Yarden. Mais Éliysha dit : YHWH est vivant et ton âme est vivante ! Je ne te quitterai pas ! Ainsi, ils s'en allèrent tous les deux.
2:7	50 hommes d'entre les fils des prophètes arrivèrent et s'arrêtèrent à distance vis-à-vis d'eux, et eux deux s'arrêtèrent au bord du Yarden.
2:8	Éliyah prit son manteau, le roula et en frappa les eaux, qui se divisèrent çà et là, et ils passèrent tous deux à sec.
2:9	Et il arriva, quand ils eurent passé, qu'Éliyah dit à Éliysha : Demande ce que tu veux que je fasse pour toi, avant que je sois enlevé d'avec toi. Éliysha dit : S’il te plaît, que deux bouches<!--Le fils aîné recevait une double bouche « portion » par rapport aux autres fils (De. 21:15-17).--> de ton esprit viennent sur moi !
2:10	Éliyah lui dit : Tu demandes une chose difficile. Mais si tu me vois pendant que je serai enlevé d'auprès de toi, cela t'arrivera. Sinon, cela n'arrivera pas.
2:11	Il arriva, comme ils marchaient et qu’ils parlaient en marchant, voici un char de feu et des chevaux de feu, qui les séparèrent, eux deux, et Éliyah monta aux cieux dans un tourbillon.

### La double portion de l'esprit d'Éliyah sur Éliysha (Élisée)

2:12	Éliysha le regardait et criait : Mon père ! Mon père ! Char d'Israël et ses cavaliers ! Et il ne le vit plus. Puis saisissant ses vêtements, il les déchira en deux morceaux.
2:13	Il releva le manteau qu'Éliyah avait laissé tomber. Puis il retourna et s'arrêta sur le bord du Yarden.
2:14	Il prit le manteau qu'Éliyah avait laissé tomber et il en frappa les eaux et dit : Où est YHWH, l'Elohîm d'Éliyah, YHWH lui-même ? Lui aussi frappa les eaux qui se divisèrent en deux, et Éliysha passa.

### Le service d'Éliysha est reconnu par les hommes

2:15	Quand les fils des prophètes qui étaient à Yeriycho le virent d’en face, ils dirent : L'esprit d'Éliyah repose sur Éliysha ! Ils vinrent à sa rencontre et se prosternèrent contre terre devant lui.
2:16	Ils lui dirent : Voici, il y a parmi tes serviteurs 50 hommes talentueux. Qu’ils aillent, s’il te plaît, chercher ton maître, de peur que l'Esprit de YHWH ne l'ait enlevé et ne l'ait jeté sur quelque montagne ou dans quelque vallée ? Éliysha dit : Ne les envoyez pas.
2:17	Mais ils insistèrent tellement qu'il eut honte de refuser. Il leur dit : Envoyez-les. Ils envoyèrent 50 hommes, qui pendant 3 jours cherchèrent, mais ils ne le trouvèrent pas.
2:18	Ils retournèrent vers Éliysha, qui était à Yeriycho, et il leur dit : Ne vous avais-je pas dit : N'y allez pas !
2:19	Les hommes de la ville dirent à Éliysha : S'il te plaît, voici, la demeure de cette ville est bonne, comme mon seigneur le voit, mais les eaux sont mauvaises et la terre est stérile.
2:20	Il dit : Apportez-moi un vase neuf et mettez-y du sel ! Et ils le lui apportèrent.
2:21	Il alla vers la source des eaux, et il y jeta le sel, et dit : Ainsi parle YHWH : Je guéris<!--Le mot « rapha » en hébreu signifie aussi « rendre salubre », « guérir ». Voir Ez. 47:8-11.--> ces eaux. Elles ne causeront plus ni mort ni stérilité.
2:22	Les eaux furent guéries, jusqu'à ce jour, selon la parole qu'Éliysha avait déclarée.

### Jugement des moqueurs

2:23	De là, il monta à Béth-El. Comme il montait par le chemin, des petits garçons sortirent de la ville et se moquèrent de lui. Ils lui disaient : Monte chauve ! Monte chauve !
2:24	Il se tourna en arrière, et les ayant regardés, il les maudit au nom de YHWH. Et deux ours sortirent de la forêt et déchirèrent 42 de ces enfants.
2:25	De là il alla sur la Montagne de Carmel, d'où il retourna à Samarie.

## Chapitre 3

### Yehoram (Yoram) règne sur Israël

3:1	La dix-huitième année de Yehoshaphat, roi de Yéhouda, Yehoram, fils d'Achab, régna sur Israël à Samarie. Il régna 12 ans.
3:2	Il fit ce qui est mal aux yeux de YHWH, non pas toutefois comme son père et sa mère. Il ôta le monument de Baal que son père avait fait,
3:3	seulement, il s'accrocha aux péchés de Yarobam, fils de Nebath, qui avait fait pécher Israël. Il ne s'en détourna pas.

### Rébellion de Moab ; Israël et Yéhouda s'allient pour combattre

3:4	Or Mésha, roi de Moab, possédait des troupeaux, et il payait au roi d'Israël un tribut de 100 000 agneaux et 100 000 béliers avec leur laine.
3:5	Et il arriva, à la mort d’Achab, que le roi de Moab se révolta contre le roi d’Israël.
3:6	Le roi Yehoram sortit ce jour-là de Samarie et dénombra tout Israël.
3:7	S’étant mis en marche, il envoya dire à Yehoshaphat, roi de Yéhouda : Le roi de Moab s'est rebellé contre moi. Veux-tu venir avec moi faire la guerre à Moab ? Yehoshaphat dit : Je monterai, moi comme toi, mon peuple comme ton peuple, mes chevaux comme tes chevaux.
3:8	Il dit : Par quel chemin monterons-nous ? Il dit : Par le chemin du désert d'Édom.

### Les rois d'Israël, de Yéhouda et d'Édom en marche ; ils consultent Éliysha

3:9	Le roi d'Israël, le roi de Yéhouda et le roi d'Édom, partirent. Ils firent un détour, et après une marche de sept jours, ils manquèrent d'eau pour l'armée et pour les bêtes qui la suivaient.
3:10	Le roi d'Israël dit : Hélas ! YHWH a appelé ces trois rois pour les livrer entre les mains de Moab.
3:11	Yehoshaphat dit : N'y a-t-il ici aucun prophète de YHWH, par qui nous puissions consulter YHWH ? L’un des serviteurs du roi d'Israël répondit et dit : Il y a ici Éliysha, fils de Shaphath, qui versait de l'eau sur les mains d'Éliyah.
3:12	Yehoshaphat dit : La parole de YHWH est avec lui. Le roi d'Israël, Yehoshaphat et le roi d'Édom descendirent vers lui.
3:13	Mais Éliysha dit au roi d'Israël : Qu'y a-t-il entre moi et toi ? Va-t'en vers les prophètes de ton père et vers les prophètes de ta mère. Le roi d'Israël lui dit : Non ! Car YHWH a appelé ces trois rois pour les livrer entre les mains de Moab.
3:14	Éliysha dit : YHWH Tsevaot, en face de qui je me tiens debout est vivant ! Si je n'avais de la considération pour Yehoshaphat, roi de Yéhouda, je ne ferais aucune attention à toi et je ne te regarderais même pas.
3:15	Maintenant, amenez-moi un joueur d'instruments à cordes ! Et il arriva comme le joueur jouait des instruments à cordes, que la main de YHWH vint sur Éliysha.

### Prophétie sur la défaite de Moab

3:16	Il dit : Ainsi parle YHWH : Faites des tranchées dans toute cette vallée.
3:17	Car ainsi parle YHWH : Vous ne verrez ni vent, ni pluie, et néanmoins cette vallée sera remplie d'eaux, et vous boirez, vous et vos bêtes.
3:18	Mais cela est peu de chose aux yeux de YHWH. Il livrera Moab entre vos mains.
3:19	Vous frapperez toutes les villes fortes et toutes les villes d'élite, vous abattrez tous les bons arbres, vous boucherez toutes les sources d'eau et vous ruinerez avec des pierres tous les meilleurs champs.
3:20	Il arriva au matin, à la montée de l’offrande, que voici, des eaux vinrent du chemin d'Édom, et la terre fut remplie d'eaux.
3:21	Tout Moab ayant appris que ces rois étaient montés pour lui faire la guerre, on convoqua tous ceux qui ceignent les ceintures et au-dessus, et ils se tinrent sur la frontière.
3:22	Le lendemain, ils se levèrent tôt le matin, et comme le soleil se levait sur les eaux, Moab vit en face de lui les eaux rouges comme du sang.
3:23	Ils dirent : C'est du sang ! Les rois se sont combattus, ils se sont combattus, et chaque homme a frappé son compagnon. Maintenant, Moab, au butin !
3:24	Ils marchèrent contre le camp d'Israël. Mais Israël se leva et frappa Moab, qui prit la fuite devant eux. Puis ils pénétrèrent sur la terre et frappèrent Moab.
3:25	Ils détruisirent les villes, ils jetèrent chacun des pierres dans les meilleurs champs et les en remplirent, ils bouchèrent toutes les sources d'eaux et abattirent tous les bons arbres. Les frondeurs entourèrent et frappèrent Kir-Haréseth, dont on ne laissa que les pierres.
3:26	Le roi de Moab, voyant qu'il n'était pas le plus fort dans la bataille, prit avec lui 700 hommes tirant l'épée pour se frayer un passage jusqu'au roi d'Édom, mais ils ne le purent pas.
3:27	Il prit son fils premier-né, qui devait régner à sa place, et le fit monter en holocauste sur la muraille. Une grande colère vint sur Israël, qui s'éloigna du roi de Moab et retourna vers sa terre.

## Chapitre 4

### Miracle : Le vase d'huile de la veuve

4:1	Une femme d’entre les femmes des fils des prophètes cria vers Éliysha, en disant : Ton serviteur, mon homme, est mort, et toi, tu sais que ton serviteur avait la crainte de YHWH. Or son créancier est venu pour prendre mes deux enfants afin qu'ils soient ses esclaves.
4:2	Éliysha lui dit : Que puis-je faire pour toi ? Dis-moi ce que tu as à la maison. Et elle dit : Ta servante n'a rien dans toute la maison qu'un vase d'huile.
4:3	Il lui dit : Va, demande des vases dans la rue à tous tes voisins, des vases vides, et n'en demande pas un petit nombre.
4:4	Puis rentre et ferme la porte sur toi et sur tes fils, et verse dans tous ces vases, et tu mettras de côté ceux qui seront pleins.
4:5	Elle s’en alla d’auprès de lui, et ferma la porte sur elle et sur ses fils. Ceux-ci les lui présentaient, et elle versait.
4:6	Il arriva que, quand les vases furent remplis, elle dit à son fils : Présente-moi encore un vase. Mais il dit : Il n'y a plus de vase. Et l'huile s'arrêta.
4:7	Elle alla le raconter à l'homme d'Elohîm, qui lui dit : Va, vends l'huile, et paye ta dette. Vous vivrez, toi et tes fils, de ce qui restera.

### YHWH se souvient de la Shounamite

4:8	Il arriva un jour qu'Éliysha passait par Shouném. Il y avait là une femme importante qui le pressa d'accepter à manger du pain chez elle. Et toutes les fois qu'il passait, il s'y retirait pour manger du pain.
4:9	Elle dit à son homme : Voici, je sais maintenant que cet homme qui passe continuellement chez nous est un saint homme d'Elohîm.
4:10	Faisons-lui, s'il te plaît, une petite chambre haute avec des murs, et mettons-y pour lui un lit, une table, un siège et un chandelier, afin que quand il viendra chez nous, il s'y retire.
4:11	Il arriva un jour qu'il vint là, et il entra dans la chambre haute et s'y coucha.
4:12	Il dit à Guéhazi, son serviteur : Appelle cette Shounamite ! Guéhazi l'appela et elle se tint debout devant lui.
4:13	Il dit à Guéhazi : Dis-lui, s'il te plaît : Voici, tu étais dans l'anxiété pour nous avec tous ces soins anxieux ! Que faire pour toi ? Faut-il parler pour toi au roi ou au chef de l'armée ? Elle dit : Moi-même j’habite au milieu de mon peuple.
4:14	Il dit : Que faire pour elle ? Guéhazi dit : Elle n’a pas de fils, et son homme est vieux !
4:15	Il dit : Appelle-la ! Guéhazi l'appela et elle se tint debout à la porte.
4:16	Éliysha lui dit : L'année prochaine, au temps fixé, tu embrasseras un fils. Elle dit : Mon seigneur, homme d'Elohîm, ne trompe pas, ne trompe pas ta servante !
4:17	Cette femme devint enceinte et enfanta un fils un an après, au temps fixé, comme Éliysha lui avait dit.

### Foi de la Shounamite, résurrection de son fils

4:18	L'enfant grandit. Or il arriva un jour qu’il sortit vers son père, auprès des moissonneurs, 
4:19	il dit à son père : Ma tête ! Ma tête ! Et le père dit au serviteur : Porte-le à sa mère.
4:20	Il le porta et l'amena à sa mère. Et l'enfant resta sur ses genoux jusqu'à midi, puis il mourut.
4:21	Elle monta le coucher sur le lit de l'homme d'Elohîm, ferma la porte derrière lui et sortit.
4:22	Elle appela son homme et dit : Envoie-moi, s'il te plaît, un des serviteurs et une ânesse pour que je coure jusqu’à l'homme d'Elohîm, et que je revienne.
4:23	Il dit : Pourquoi vas-tu vers lui aujourd'hui ? Ce n'est pas la nouvelle lune ni le shabbat. Elle dit : Shalôm !
4:24	Elle fit seller l'ânesse et dit à son serviteur : Conduis-moi et ne m'arrête pas en route sans que je te le dise.
4:25	Elle s'en alla et vint vers l'homme d'Elohîm sur la Montagne de Carmel. L'homme d'Elohîm, l'ayant aperçue, dit à Guéhazi son serviteur : Voilà la Shounamite !
4:26	Maintenant, s’il te plaît, cours à sa rencontre et dis-lui : Shalôm à toi ? Shalôm à ton époux ? Shalôm à ton enfant ? Elle dit : Shalôm !
4:27	Dès qu'elle fut arrivée auprès de l'homme d'Elohîm sur la montagne, elle embrassa ses pieds. Guéhazi s'approcha pour la repousser, mais l'homme d'Elohîm lui dit : Laisse-la, car son âme est dans l'amertume, et YHWH me l'a caché, et ne me l'a pas révélé.
4:28	Elle dit : Ai-je demandé un fils à mon seigneur ? N'ai-je pas dit : Ne me trompe pas ?
4:29	Éliysha dit à Guéhazi : Ceins tes reins, prends mon bâton dans ta main et pars. Si tu rencontres quelqu'un, ne le salue pas, et si quelqu'un te salue, ne lui réponds pas. Tu mettras mon bâton sur les faces du garçon.
4:30	Mais la mère du garçon dit : YHWH est vivant et ton âme est vivante ! Je ne te quitterai pas. Il se leva et s’en alla derrière elle.
4:31	Or Guéhazi était allé devant eux et il avait mis le bâton sur les faces du garçon, mais il n'y eut ni voix ni signe d'attention. Il retourna à la rencontre d'Éliysha et l'en informa, en disant : Le garçon ne s'est pas réveillé.
4:32	Lorsqu'Éliysha entra dans la maison, le garçon, mort, était couché sur son lit.
4:33	Il ferma la porte sur eux deux et pria YHWH.
4:34	Il monta et se coucha sur l'enfant, il mit sa bouche sur la bouche de l'enfant, ses yeux sur ses yeux, ses mains sur ses mains, et il s'étendit sur lui. La chair de l'enfant se réchauffa.
4:35	Il s'éloigna et marcha dans la maison, tantôt ici, tantôt là, puis il monta et s'étendit encore sur lui. Le garçon éternua sept fois et ouvrit ses yeux.
4:36	Il appela Guéhazi et lui dit : Appelle cette Shounamite. Guéhazi l'appela, et elle vint vers Éliysha qui lui dit : Prends ton fils !
4:37	Elle se jeta à ses pieds et se prosterna contre terre. Puis elle prit son fils et sortit.

### Les coloquintes sauvages

4:38	Éliysha revint à Guilgal. Or il y avait une famine<!--Par le passé, Israël a connu plusieurs famines, dont celle relatée en 2 R. 4:38-41. Dans ce passage, l'un des fils des prophètes trouva une vigne sauvage dans un champ et y cueillit des coloquintes sauvages. Il les ajouta au potage qui mijotait dans un pot, ne sachant pas que c'était du poison. Le pot est l'image des assemblées de Laodicée dans lesquelles il y a un mélange mortel de fausses doctrines et de préceptes mondains qui viennent altérer la vérité de la parole d'Elohîm. Ce mélange impur est absorbé par des millions de personnes ignorantes à travers le monde. Celles-ci se rendent compte qu'elles ont été empoisonnées spirituellement, et une fois le mélange ingéré, elles constatent les effets pervers et dévastateurs souvent tardivement. Le champ tout comme la vigne sauvage, selon Mt. 13:38 et Ro. 11:17, symbolise le monde. Il est par ailleurs intéressant de noter que le mot herbe, « owrah » en hébreu, signifie aussi lumière (Ps. 139:12). Cette histoire n'est pas sans nous rappeler le feu étranger introduit par les fils d'Aaron dans le tabernacle, et ce, malgré l'interdiction formelle de YHWH (Ex. 30:9 ; Lé. 10:1-5). C'est exactement ce qui se passe de nos jours. Les assemblées importent de plus en plus en leur sein la lumière luciférienne du monde (musique, marketing, philosophie, etc.). Beaucoup de pasteurs et de musiciens cherchent malheureusement leur inspiration dans le monde à cause de la famine qui sévit dans les assemblées. Ce feu étranger représente la plupart des doctrines et pratiques promues par l'assemblée de Laodicée.--> sur la terre, et les fils des prophètes étaient assis devant lui, il dit à son serviteur : Mets le grand pot et fais cuire du potage pour les fils des prophètes.
4:39	L'un d'eux étant sorti dans les champs pour cueillir des herbes, trouva de la vigne sauvage, et cueillit des coloquintes sauvages plein sa robe, et étant revenu, il les coupa en morceaux dans le pot où était le potage, car on ne les connaissait pas.
4:40	On en versa à ces hommes pour manger. Et il arriva que, comme ils mangeaient de ce potage, ils poussèrent des cris et dirent : Homme d'Elohîm, la mort est dans le pot ! Et ils ne purent en manger.
4:41	Il dit : Prenez de la farine ! Il en jeta dans le pot et dit : Verses-en à ce peuple, afin qu'il mange ! Et il n'y avait plus quelque chose<!--Littéralement : « Il n'y avait plus une parole mauvaise dans le pot. » En effet, le mot « quelque chose » vient de l'hébreu « dabar » qui signifie : « discours », « parole ».--> de mauvais dans le pot.

### Multiplication de pains

4:42	Un homme venant de Baal-Shalisha apporta à l'homme d'Elohîm du pain des prémices, 20 pains d'orge et des épis nouveaux. Il dit : Donne au peuple. Qu’ils mangent ! 
4:43	Son serviteur dit : Comment pourrais-je en donner à 100 hommes ? Mais Éliysha lui dit : Donnes-en au peuple et qu'ils mangent ! Car ainsi parle YHWH : Ils mangeront et il en restera encore.
4:44	Il mit les pains devant eux. Ils mangèrent et en eurent de reste, selon la parole de YHWH.

## Chapitre 5

### Guérison miraculeuse de Naaman

5:1	Naaman, chef de l'armée du roi de Syrie, était devenu un grand homme devant son seigneur et élevé, car c'était par lui que YHWH avait délivré les Syriens. Mais cet homme vaillant et talentueux était lépreux.
5:2	Les Syriens étaient sortis par troupes, et ils avaient emmené prisonnière une petite fille de la terre d'Israël, qui était au service de la femme de Naaman.
5:3	Elle dit à sa maîtresse : Oh ! si mon seigneur était devant le prophète qui est à Samarie ! Alors il le débarrasserait de sa lèpre !
5:4	Naaman le rapporta à son maître, en disant : La fille qui est de la terre d'Israël a dit telle et telle chose.
5:5	Et le roi de Syrie dit : Va, rends-toi à Samarie et j'enverrai une lettre au roi d'Israël. Il s'en alla et prit dans sa main 10 talents d'argent, 6 000 pièces d'or et 10 vêtements de rechange.
5:6	Il porta au roi d'Israël la lettre, où il était dit : Maintenant que cette lettre t'est parvenue, voici, j’envoie auprès de toi Naaman, mon serviteur, afin que tu le débarrasses de sa lèpre.
5:7	Il arriva que, lorsque le roi d’Israël eut lu la lettre, il déchira ses vêtements et dit : Suis-je Elohîm pour faire mourir et pour faire vivre ? Oui, celui-ci m’envoie un homme pour le débarrasser la lèpre ! Oui, sachez et discernez s'il vous plaît ! Oui, il cherche sûrement une occasion contre moi !
5:8	Il arriva qu'aussitôt qu'Éliysha, homme d'Elohîm, apprit que le roi d'Israël avait déchiré ses vêtements, il envoya dire au roi : Pourquoi as-tu déchiré tes vêtements ? Laisse-le venir vers moi et il saura qu'il y a un prophète en Israël.
5:9	Naaman vint avec ses chevaux et son char, et il s'arrêta à la porte de la maison d'Éliysha.
5:10	Éliysha envoya un messager vers lui, pour lui dire : Va ! Lave-toi 7 fois dans le Yarden, et ta chair retournera à toi et tu seras pur.
5:11	Naaman, fâché, s'en alla en disant : Voici, je me disais qu'il sortira, qu'il sortira vers moi, et que, se tenant debout, il invoquera le Nom de YHWH, son Elohîm, qu'il agitera sa main sur la place et qu'il débarrassera le “lépreux” de sa “lèpre”.
5:12	Les fleuves de Damas, l'Amanah et le Parpar ne sont-ils pas meilleurs que toutes les eaux d'Israël ? Ne pourrais-je pas m'y laver et devenir pur ? Ainsi, il s'en retourna et s'en alla avec courroux.
5:13	Mais ses serviteurs s'approchèrent et lui parlèrent en disant : Mon père, si le prophète t’avait dit une grande parole, ne l’aurais-tu pas faite ? À plus forte raison quand il te dit : Lave-toi et tu deviendras pur !
5:14	Il descendit et se plongea 7 fois dans le Yarden, selon la parole de l'homme d'Elohîm. Sa chair redevint comme la chair d'un petit garçon et il fut pur.
5:15	Il retourna vers l'homme d'Elohîm, lui et tout son camp, et il vint se présenter devant lui et dit : Voici, maintenant je sais qu'il n'y a pas d'autre Elohîm sur toute la Terre, si ce n'est en Israël. Maintenant, s'il te plaît, accepte ce présent de ton serviteur.
5:16	Il dit : YHWH, en face de qui je me tiens debout est vivant ! Je ne l'accepterai pas ! Il le pressa de l'accepter, mais il refusa !
5:17	Naaman dit : S'il te plaît, permets que l'on donne de la terre à ton serviteur, une charge de deux mulets, car ton serviteur ne fera plus d'holocauste ni de sacrifice à d'autres elohîm, mais seulement à YHWH.
5:18	Que YHWH pardonne cette chose à ton serviteur : Quand mon maître entre dans la maison de Rimmon pour s'y prosterner et qu'il s'appuie sur ma main, je me prosterne aussi dans la maison de Rimmon. S'il te plaît, que YHWH pardonne cette chose à ton serviteur, quand je me prosternerai dans la maison de Rimmon.

### Convoitise et mensonge de Guéhazi ; jugement d'Elohîm

5:19	Il lui dit : Va en paix ! Quand il fut parti et fut à une certaine distance de la terre,
5:20	Guéhazi<!--Guéhazi, dont le nom hébreu signifie « vallée de la vision ».-->, le serviteur d'Éliysha, homme d'Elohîm, se dit en lui-même : Voici, mon maître a épargné Naaman, ce Syrien, en ne prenant pas de sa main ce qu'il avait apporté. YHWH est vivant ! Je vais lui courir après pour prendre quelque chose de lui.
5:21	Et Guéhazi courut après Naaman. Naaman, le voyant courir après lui, descendit de son char pour aller à sa rencontre. Il dit : Shalôm ?
5:22	Il dit : Shalôm. Mon maître m'envoie te dire : Voici, il vient d'arriver chez moi deux jeunes hommes de la Montagne d'Éphraïm, d'entre les fils des prophètes. S'il te plaît, donne-leur un talent d'argent et 2 vêtements de rechange.
5:23	Et Naaman dit : Consens à prendre 2 talents. Il insista, puis il serra 2 talents d'argent dans deux sacs avec 2 vêtements de rechange et les fit porter devant Guéhazi par deux de ses jeunes hommes.
5:24	Et quand il fut arrivé dans un lieu secret, il les prit de leurs mains, et les déposa dans la maison, et il renvoya ces jeunes hommes qui s'en allèrent.
5:25	Puis il entra et se présenta devant son maître. Éliysha lui dit : D'où viens-tu, Guéhazi ? Et il dit : Ton serviteur n'est allé nulle part.
5:26	Mais il lui dit : Mon cœur non plus n'était pas allé lorsque cet homme a quitté son char pour venir à ta rencontre. Est-ce le temps de prendre de l'argent, de prendre des vêtements, des oliviers, des vignes, des bœufs, des brebis, des serviteurs et des servantes ?
5:27	La lèpre de Naaman s'attachera à toi et à ta postérité à perpétuité ! Il sortit de sa présence avec une lèpre comme de la neige.

## Chapitre 6

### Miracle d'un instrument en fer

6:1	Les fils des prophètes dirent à Éliysha : Regarde, s'il te plaît ! Le lieu où nous sommes assis devant toi est trop étroit pour nous.
6:2	Allons jusqu'au Yarden ! Là, nous prendrons une poutre par homme et nous y ferons un lieu d'habitation. Éliysha répondit : Allez-y !
6:3	Et l'un d'eux dit : Consens, s'il te plaît, à venir avec tes serviteurs. Il dit : Moi, j’irai.
6:4	Il partit avec eux. Arrivés au Yarden, ils coupèrent du bois.
6:5	Mais il arriva que comme l'un d'eux abattait une poutre, le fer tomba dans l'eau. Il s'écria, et dit : Ah ! Mon seigneur ! Je l'avais emprunté !
6:6	L'homme d'Elohîm dit : Où est-il tombé ? Et il lui montra l'endroit. Il coupa un morceau de bois, le jeta au même endroit, et fit surnager le fer.
6:7	Il dit : Enlève-le ! Et il avança la main et le prit.

### YHWH révèle à Éliysha les plans militaires des Syriens

6:8	Le roi de Syrie était en guerre avec Israël. Dans un conseil qu'il tint avec ses serviteurs, il dit : Mon camp sera dans un tel lieu.
6:9	L'homme d'Elohîm envoya dire au roi d'Israël : Garde-toi de passer dans ce lieu, car les Syriens y descendent.
6:10	Le roi d'Israël envoya au lieu au sujet duquel l’homme d'Elohîm lui avait parlé et l’avait averti, et il y fut gardé. Cela eut lieu non pas une fois, ni deux fois.
6:11	Le cœur du roi de Syrie tempêta contre cette affaire. Il appela ses serviteurs et leur dit : Ne voulez-vous pas me déclarer lequel de vous est pour le roi d'Israël ?
6:12	L'un de ses serviteurs dit : Personne ! Roi, mon seigneur ! Mais Éliysha, le prophète qui est en Israël, révèle au roi d'Israël les paroles mêmes que tu déclares dans ta chambre à coucher.
6:13	Il dit : Allez et voyez où il est, et je l'enverrai prendre. On vint lui dire : Voici, il est à Dothan.
6:14	Il envoya là des chevaux, des chars et une grande armée, qui arrivèrent de nuit et qui entourèrent la ville.

### L'armée de YHWH plus grande que celle des Syriens

6:15	Le serviteur de l'homme d'Elohîm se leva de grand matin et sortit. Et voici qu'une armée entourait la ville avec des chevaux et des chars. Le serviteur dit à l'homme d'Elohîm : Ah ! Mon seigneur, comment ferons-nous ?
6:16	Il lui dit : N'aie pas peur, car ceux qui sont avec nous sont plus nombreux que ceux qui sont avec eux.
6:17	Éliysha pria et dit : S'il te plaît, YHWH ! Ouvre ses yeux afin qu'il voie. Et YHWH ouvrit les yeux du serviteur et il vit. Et voici la montagne était pleine de chevaux et de chars de feu autour d'Éliysha.

### Elohîm aveugle les Syriens à la prière d'Éliysha

6:18	Ils descendirent vers Éliysha. Celui-ci pria YHWH, en disant : S'il te plaît, frappe cette nation d'aveuglement ! Et Elohîm les frappa d'aveuglement, selon la parole d'Éliysha.
6:19	Éliysha leur dit : Ce n'est pas ici le chemin et ce n'est pas ici la ville. Suivez-moi et je vous conduirai vers l'homme que vous cherchez. Et il les conduisit à Samarie.
6:20	Et il arriva qu'aussitôt qu'ils furent entrés dans Samarie, Éliysha dit : YHWH, ouvre leurs yeux afin qu'ils voient. Et YHWH ouvrit leurs yeux et ils virent qu'ils étaient au milieu de Samarie.
6:21	En les voyant, le roi d'Israël dit à Éliysha : Frapperai-je, frapperai-je, mon père ?
6:22	Il dit : Tu ne frapperas pas. Frapperais-tu de ton épée et de ton arc ceux que tu as fait prisonniers ? Sers-leur du pain et de l'eau afin qu'ils mangent et boivent, et qu'ils s'en aillent ensuite vers leur seigneur.
6:23	Le roi d'Israël leur fit servir un grand repas et ils mangèrent et burent. Puis il les renvoya et ils s'en allèrent vers leur seigneur. Depuis lors, les armées de Syrie ne revinrent plus en terre d'Israël.

### Siège des Syriens et famine en Samarie

6:24	Et il arriva après cela que Ben-Hadad, roi de Syrie, rassembla toute son armée, monta et assiégea Samarie.
6:25	Il y eut une grande famine<!--Cette histoire est riche en enseignements pour notre génération. Le siège de la Samarie par les étrangers, la famine qui frappait les Hébreux, le cannibalisme de certaines femmes, la cherté des produits alimentaires, la consommation d'excréments d'animaux à cause de la famine, sont des conséquences du péché. Aujourd'hui, beaucoup d'assemblées sont assiégées par les choses du monde, les démons, les fausses doctrines, etc.--> dans Samarie. Ils l'assiégèrent tellement qu'une tête d'âne se vendait 80 pièces d'argent, et le quart d'un kab<!--« Récipient creux », « une mesure de produits secs », « environ 1.5 litres ».--> de fiente de colombe 5 pièces d'argent.
6:26	Il arriva que comme le roi d’Israël passait sur la muraille, une femme lui cria et dit : Roi, mon seigneur ! Sauve-moi.
6:27	Il dit : Si YHWH ne te sauve pas, comment pourrais-je te sauver ? Serait-ce avec le produit de l'aire ou de la cuve ?
6:28	Il lui dit encore : Qu'as-tu ? Elle dit : Cette femme-là m'a dit : Donne ton fils, et mangeons-le aujourd'hui, et nous mangerons mon fils demain<!--Lé. 26:29 ; De. 28:53-57.-->.
6:29	Nous avons fait bouillir mon fils et nous l'avons mangé. Le jour suivant, je lui ai dit : Donne ton fils et nous le mangerons. Mais elle a caché son fils.
6:30	Et il arriva que, lorsque le roi entendit les paroles de cette femme, il déchira ses vêtements et passa sur la muraille. Le peuple vit qu'il avait en dessous un sac sur son corps.
6:31	Il dit : Qu'ainsi me traite Elohîm et qu'ainsi il y ajoute, si aujourd'hui la tête d'Éliysha, fils de Shaphath, reste sur lui.
6:32	Or Éliysha était assis dans sa maison, et les anciens étaient assis avec lui. Le roi envoya un homme devant lui. Mais avant que le messager soit arrivé, il dit aux anciens : Ne voyez-vous pas que le fils de ce meurtrier envoie quelqu'un pour m'ôter la tête ? Lorsque le messager viendra, fermez la porte et repoussez-le avec la porte. N'entendez-vous pas le bruit des pas de son maître derrière lui ?
6:33	Et comme il parlait encore avec eux, voici le messager descendit vers lui, et dit : Voici, ce mal vient de YHWH. Qu'ai-je à espérer encore de YHWH ?

## Chapitre 7

### Prophétie d'Éliysha : les lépreux dans le camp des Syriens

7:1	Éliysha dit : Écoutez la parole de YHWH ! Ainsi parle YHWH : Demain, à cette heure, on aura une mesure de fleur de farine pour un sicle, et 2 mesures d'orge pour un sicle, à la porte de Samarie.
7:2	Mais l'officier sur la main duquel le roi s'appuyait répondit à l'homme d'Elohîm, et dit : Quand YHWH ferait des écluses aux cieux, cette chose arriverait-elle ? Éliysha dit : Voici, tu le verras de tes yeux, mais tu n'en mangeras pas.
7:3	Or il y avait à l'entrée de la porte quatre hommes lépreux<!--Elohîm s'est servi de ces quatre lépreux comme messagers de bonnes nouvelles. Le Seigneur utilise souvent les personnes rejetées et déconsidérées (1 Co. 1:26-31).-->, et ils se dirent chaque homme à son compagnon : Pourquoi resterions-nous ici jusqu'à ce que nous mourions ?
7:4	Si nous disons : Entrons dans la ville, comme la famine est dans la ville, nous y mourrons. Mais si nous restons ici, nous mourrons également. Allons-nous jeter dans le camp des Syriens ! S'ils nous laissent vivre, nous vivrons, et s'ils nous font mourir, nous mourrons.
7:5	Ils se levèrent au crépuscule pour entrer au camp des Syriens. Lorsqu'ils furent arrivés à l'extrémité du camp, voici, il n'y avait personne.
7:6	Adonaï avait fait entendre dans le camp des Syriens un bruit de chars, un bruit de chevaux et un bruit d'une grande armée, de sorte qu'ils s'étaient dit chaque homme à son frère : Voici, le roi d'Israël a payé les rois des Héthiens et les rois des Égyptiens pour venir contre nous.
7:7	Ils s'étaient levés au crépuscule et s'étaient enfuis, abandonnant leurs tentes, leurs chevaux, leurs ânes, et le camp tel qu'il était, et ils s'étaient enfuis pour leur âme.
7:8	Les lépreux arrivèrent jusqu'à l'extrémité du camp. Ils entrèrent dans une tente, mangèrent, burent, emportèrent de l'argent, de l'or, des vêtements, et ils s'en allèrent et les cachèrent. Ils revinrent et entrèrent dans une autre tente. Ils emportèrent de là, s'en allèrent et les cachèrent.
7:9	Ils se dirent chaque homme à son compagnon : Nous n'agissons pas bien ! Ce jour est un jour de bonnes nouvelles. Si nous gardons le silence et si nous attendons jusqu'à la lumière du matin, le châtiment nous atteindra. Venez maintenant et allons informer la maison du roi.
7:10	Ils partirent et appelèrent les portiers de la ville et leur racontèrent, en disant : Nous sommes entrés dans le camp des Syriens, et voici qu’il n’y a là aucun homme, ni aucune voix humaine. Il n'y a que des chevaux attachés, des ânes attachés et les tentes sont comme elles étaient.
7:11	Les portiers appelèrent et l’annoncèrent à l’intérieur de la maison du roi.

### Accomplissement de la prophétie d'Éliysha

7:12	Le roi se leva de nuit et dit à ses serviteurs : Que je vous raconte, s’il vous plaît, ce que les Syriens ont préparé contre nous. Ils savent que nous sommes affamés et ils sont sortis du camp pour se cacher dans les champs, disant : Quand ils sortiront hors de la ville, nous les saisirons vivants et nous entrerons dans la ville.
7:13	L'un des serviteurs du roi répondit et dit : Qu’on prenne, s’il te plaît, cinq des chevaux restants, de ceux qui restent encore dans la ville. Voici qu’ils sont comme toute la foule d’Israël qui y reste. Voici qu’ils sont comme toute la foule d’Israël qui est consumée. Envoyons-les et voyons !
7:14	Ils prirent deux chars avec les chevaux, et le roi envoya des messagers après l'armée des Syriens, en disant : Allez et voyez.
7:15	Ils allèrent après eux jusqu'au Yarden et virent que tout le chemin était plein de vêtements et d'objets que les Syriens avaient jetés dans leur précipitation. Les messagers revinrent et le rapportèrent au roi.
7:16	Le peuple sortit et pilla le camp des Syriens, et l'on eut une mesure de fleur de farine pour un sicle, et 2 mesures d'orge pour un sicle, selon la parole de YHWH.
7:17	Le roi donna à l'officier, sur la main duquel il s'appuyait, la charge de garder la porte. Mais cet officier fut écrasé à la porte par le peuple et il en mourut selon la parole qu'avait prononcée l'homme d'Elohîm, quand le roi était descendu vers lui.
7:18	Il arriva ce que l’homme d'Elohîm avait déclaré au roi en disant : Demain, à cette heure, à la porte de Samarie, la double mesure d’orge sera à un sicle et la mesure de fleur de farine à un sicle.
7:19	L'officier avait répondu à l’homme d'Elohîm, et avait dit : Quand YHWH ferait des écluses aux cieux, cette chose arriverait-elle ? Et il avait dit : Voici, tu le verras de tes yeux, mais tu n'en mangeras pas<!--2 R. 7:1-2.-->.
7:20	C'est en effet ce qui lui arriva : le peuple l'écrasa à la porte et il mourut.

## Chapitre 8

### Éliysha annonce une famine de 7 ans

8:1	Éliysha parla à la femme dont il avait fait revivre le fils, en disant : Lève-toi et va-t'en, toi et ta famille, et séjourne où tu pourras. Car YHWH a appelé la famine, et même elle vient sur la terre pour 7 ans.
8:2	La femme se leva et elle fit selon la parole de l'homme d'Elohîm. Elle s'en alla, elle et sa famille, et séjourna 7 ans en terre des Philistins.

### La Shounamite retrouve ses terres

8:3	Mais il arriva qu'au bout des 7 ans, la femme revint de la terre des Philistins, et alla implorer le roi au sujet de sa maison et de ses champs.
8:4	Le roi parlait à Guéhazi<!--Voir 2 R. 5.-->, serviteur de l'homme d'Elohîm, en disant : S'il te plaît, raconte-moi toutes les grandes choses qu'Éliysha a faites.
8:5	Et il arriva que comme il racontait au roi comment il avait fait revivre un mort, la femme dont il avait fait revivre le fils vint implorer le roi au sujet de sa maison et de ses champs. Guéhazi dit : Roi, mon seigneur, voici la femme et voici son fils qu'Éliysha a fait revivre !
8:6	Le roi interrogea la femme et elle le lui raconta. Le roi lui donna un eunuque auquel il dit : Fais restituer tout ce qui lui appartenait, même tous les revenus de ses champs, depuis le jour où elle a quitté la terre jusqu'à maintenant.

### Prophétie sur le règne d'Hazaël sur la Syrie

8:7	Éliysha se rendit à Damas. Ben-Hadad, roi de Syrie, était malade et on lui fit ce rapport : L'homme d'Elohîm est venu ici.
8:8	Le roi dit à Hazaël : Prends dans ta main un présent et va au-devant de l'homme d'Elohîm, et consulte par lui YHWH, en disant : Guérirai-je de cette maladie ?
8:9	Hazaël alla à sa rencontre et prit dans sa main un présent, tout ce qu'il y avait de meilleur à Damas, la charge de 40 chameaux. Il vint se présenter devant Éliysha et dit : Ton fils Ben-Hadad, roi de Syrie, m'a envoyé vers toi pour te dire : Guérirai-je de cette maladie ?
8:10	Et Éliysha lui dit : Va, dis-lui : Tu guériras ! Tu guériras ! Toutefois, YHWH m'a révélé qu'il mourra, qu'il mourra.
8:11	Il arrêta ses faces et le fixa jusqu’à le faire rougir, et l'homme d'Elohîm pleura.
8:12	Hazaël dit : Pourquoi mon seigneur pleure-t-il ? Et il dit : Parce que je sais le mal que tu feras aux fils d'Israël. Tu mettras le feu à leurs villes fortes, tu tueras avec l'épée leurs jeunes hommes, tu écraseras leurs petits-enfants et tu fendras le ventre de leurs femmes enceintes.
8:13	Hazaël dit : Mais qu'est-ce que ton serviteur, ce chien, pour faire de si grandes choses ? Et Éliysha dit : YHWH m'a révélé que tu seras roi de Syrie.
8:14	Il s'en alla d'auprès d'Éliysha et il vint auprès de son maître, qui lui dit : Que t'a dit Éliysha ? Et il dit : Il m'a dit que tu guériras ! Tu guériras !
8:15	Et il arriva le lendemain qu’il prit une couverture et, l'ayant plongé dans l'eau, il l'étendit sur ses faces et il mourut. Hazaël devint roi à sa place.

### Yehoram (Yoram) règne sur Yéhouda<!--2 Ch. 21:1-7.-->

8:16	La cinquième année de Yoram, fils d'Achab, roi d'Israël, Yehoshaphat était encore roi de Yéhouda et Yehoram, fils de Yehoshaphat, roi de Yéhouda, devint roi sur Yéhouda.
8:17	Il était fils de 32 ans quand il devint roi. Il régna 8 ans à Yeroushalaim.
8:18	Il marcha dans la voie des rois d'Israël comme avait fait la maison d'Achab, car il avait pour femme la fille d'Achab<!--Le mariage de Yehoram (Yoram), fils de Yehoshaphat, avec Athalyah, fille d'Achab, était une grande erreur. Cette union qui était contractée dans le but de favoriser la paix entre les deux royaumes entraîna le déclin de Yéhouda ; or Elohîm est contre les alliances contre nature. Voir Es. 30-31.-->, et il fit ce qui est mal aux yeux de YHWH.
8:19	Mais YHWH ne voulut pas détruire Yéhouda, par amour pour David, son serviteur, selon la promesse qu'il lui avait faite de lui donner toujours une lampe parmi ses fils.

### Révoltes contre le pouvoir de Yéhouda

8:20	De son temps, Édom se révolta de dessous la main de Yéhouda et se donna un roi.
8:21	Yoram passa à Tsaïr avec tous ses chars. Et voici ce qui arriva : il se leva de nuit et frappa les Édomites qui l'entouraient, ainsi que les chefs des chars. Mais le peuple s'enfuit dans ses tentes.
8:22	Édom se révolta de dessous la main de Yéhouda jusqu'à ce jour. En ce même temps, Libnah aussi se révolta.

### Achazyah règne sur Yéhouda<!--2 Ch. 21:18-22:4.-->

8:23	Le reste des discours de Yoram et tout ce qu'il a accompli, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
8:24	Yoram se coucha avec ses pères et il fut enterré avec ses pères dans la cité de David. Et Achazyah, son fils, régna à sa place.
8:25	La douzième année de Yoram, fils d'Achab, roi d'Israël, Achazyah, fils de Yehoram, roi de Yéhouda, devint roi.
8:26	Achazyah était fils de 22 ans quand il devint roi. Il régna un an à Yeroushalaim. Le nom de sa mère était Athalyah, fille d'Omri, roi d'Israël.
8:27	Il marcha dans la voie de la maison d'Achab, il fit ce qui est mal aux yeux de YHWH, comme avait fait la maison d'Achab, car il était gendre de la maison d'Achab.
8:28	Il alla avec Yoram, fils d'Achab, à la guerre contre Hazaël, roi de Syrie, à Ramoth en Galaad. Et les Syriens blessèrent Yoram.
8:29	Le roi Yoram s'en retourna pour se faire guérir à Yizre`e'l des blessures que les Syriens lui avaient faites à Ramah, lorsqu'il se battait contre Hazaël, roi de Syrie. Achazyah, fils de Yehoram, roi de Yéhouda, descendit pour voir Yoram, fils d'Achab, à Yizre`e'l, parce qu'il était malade.

## Chapitre 9

### Yehuw oint roi d'Israël

9:1	Éliysha, le prophète, appela l'un des fils des prophètes et lui dit : Ceins tes reins, prends cette fiole d'huile dans ta main, et va à Ramoth en Galaad.
9:2	Quand tu y seras entré, vois Yehuw, fils de Yehoshaphat, fils de Nimshi. Tu iras le faire lever du milieu de ses frères et tu le conduiras dans une chambre secrète.
9:3	Tu prendras la fiole d'huile, tu la verseras sur sa tête et tu diras : Ainsi parle YHWH : Je t'ai oint pour être roi sur Israël. Puis tu ouvriras la porte, tu t'enfuiras et tu ne t'arrêteras pas.
9:4	Le jeune homme, serviteur du prophète, s'en alla à Ramoth en Galaad.
9:5	Quand il arriva, voici, les chefs de l'armée étaient là assis. Il dit : Chef, j’ai une parole pour toi. Et Yehuw dit : Pour lequel de nous tous ? Et il dit : Pour toi, chef.
9:6	Yehuw se leva et entra dans la maison. Le jeune homme lui répandit l’huile sur la tête et dit : Ainsi parle YHWH, l'Elohîm d'Israël : Je t'ai oint pour être roi sur Israël, le peuple de YHWH.
9:7	Tu frapperas la maison d'Achab, ton maître, et je vengerai les sangs de mes serviteurs les prophètes, et les sangs de tous les serviteurs de YHWH, de la main de Iyzebel<!--1 R. 16:31, 17-19.-->.
9:8	Toute la maison d'Achab périra, et je retrancherai de chez Achab quiconque urine contre un mur, qu'il soit esclave ou libre en Israël.
9:9	Je rendrai la maison d'Achab semblable à la maison de Yarobam, fils de Nebath, et à la maison de Baesha, fils d'Achiyah.
9:10	Les chiens mangeront Iyzebel dans le champ de Yizre`e'l, et il n'y aura personne pour l'enterrer. Puis il ouvrit la porte et s'enfuit.
9:11	Yehuw sortit pour rejoindre les serviteurs de son maître et on lui dit : Shalôm ? Pourquoi ce fou est-il venu vers toi ? Il leur dit : Vous connaissez l'homme et ses rêveries.
9:12	Mais ils dirent : Mensonge ! Raconte-nous, s’il te plaît. Et il dit : Il m'a parlé de telle et telle manière, en disant : Ainsi parle YHWH : Je t'ai oint roi sur Israël.
9:13	Ils se hâtèrent de prendre chaque homme son vêtement qu'ils mirent sous lui, en haut des marches. Ils sonnèrent du shofar et dirent : Yehuw règne !

### Mort de Yehoram

9:14	Yehuw, fils de Yehoshaphat, fils de Nimshi, conspira contre Yoram. Or Yoram et tout Israël défendaient Ramoth en Galaad contre Hazaël, roi de Syrie.
9:15	Le roi Yehoram s'en était retourné pour se faire guérir à Yizre`e'l des blessures que les Syriens lui avaient faites quand il combattait contre Hazaël, roi de Syrie. Yehuw dit : Si cela est en votre âme, qu’aucun fugitif ne sorte de la ville pour aller le rapporter à Yizre`e'l.
9:16	Yehuw monta à cheval et s'en alla à Yizre`e'l, car Yoram était là, malade, et Achazyah, roi de Yéhouda, y était descendu pour visiter Yoram.
9:17	Or la sentinelle qui se tenait debout sur la tour, à Yizre`e'l, vit venir la troupe de Yehuw et dit : Je vois une troupe. Et Yehoram dit : Prends un cavalier et envoie-le à leur rencontre. Qu'il dise : Est-ce la paix ?
9:18	Le cavalier s'en alla à sa rencontre et dit : Ainsi parle le roi : Est-ce la paix ? Et Yehuw dit : Qu’en est-il de toi et de la paix ? Passe derrière moi ! La sentinelle le rapporta, en disant : Le messager est allé jusqu'à eux et il ne revient pas.
9:19	Yehoram envoya un second cavalier, qui arriva jusqu'à eux, et dit : Ainsi parle le roi : Est-ce la paix ? Et Yehuw dit : Qu’en est-il de toi et de la paix ? Passe derrière moi !
9:20	La sentinelle le rapporta et dit : Il est arrivé jusqu'à eux, mais il ne revient pas. La conduite est comme la conduite de Yehuw, fils de Nimshi, car il conduit avec folie.
9:21	Yehoram dit : Attelle ! Et on attela son char. Yehoram, roi d'Israël, sortit avec Achazyah, roi de Yéhouda, chacun dans son char. Ils sortirent à la rencontre de Yehuw, et ils le trouvèrent dans le champ de Naboth, le Yizréélite<!--1 R. 21.-->.
9:22	Et il arriva que, quand Yehoram vit Yehuw, il dit : Est-ce la paix, Yehuw ? Il dit : Quelle paix ! Tant que durent les prostitutions de Iyzebel, ta mère, et la multitude de ses sorcelleries !
9:23	Yehoram tourna sa main et s'enfuit, et il dit à Achazyah : Trahison, Achazyah !
9:24	Mais Yehuw remplit sa main d'un arc et il frappa Yehoram entre ses épaules, et la flèche sortit par le cœur et il tomba sur ses genoux dans son char.
9:25	Il dit à Bidkar, son officier : Prends-le et jette-le dans le champ de Naboth, le Yizréélite. Car souviens-toi, lorsque nous étions à cheval moi et toi, ensemble, derrière Achab, son père, YHWH porta contre lui cette charge :
9:26	N’ai-je pas vu hier le sang de Naboth et le sang de ses fils, déclaration de YHWH ? Je te le rendrai dans ce champ-ci ! – déclaration de YHWH. Maintenant prends-le et jette-le dans le champ, selon la parole de YHWH.

### Mort d'Achazyah<!--2 Ch. 22:7,9.-->

9:27	Achazyah, roi de Yéhouda, ayant vu cela, s'enfuit par le chemin de la maison du jardin, mais Yehuw le poursuivit et dit : Frappez-le sur le char ! Et on le frappa à la montée de Gour, près de Yible`am. Puis il se réfugia à Meguiddo, et il y mourut.
9:28	Ses serviteurs le transportèrent sur un char à Yeroushalaim, et ils l'enterrèrent dans son sépulcre avec ses pères, dans la cité de David.
9:29	La onzième année de Yoram, fils d'Achab, Achazyah régna sur Yéhouda.

### Mort d'Iyzebel (Jézabel)

9:30	Yehuw entra dans Yizre`e'l. Iyzebel, l'ayant appris, mit du fard à ses yeux, orna sa tête et regarda par la fenêtre.
9:31	Comme Yehuw entrait par la porte, elle dit : Est-ce la paix, Zimri, assassin de son maître ?
9:32	Il leva sa tête vers la fenêtre et dit : Qui est avec moi ? Qui ? Deux ou trois des eunuques regardèrent vers lui.
9:33	Il leur dit : Laissez-la tomber ! Ils la laissèrent tomber. Son sang rejaillit sur le mur et sur les chevaux, et il la piétina.
9:34	Et, étant entré, il mangea et but. Puis il dit : S'il vous plaît, prenez soin de cette maudite et enterrez-la, car elle est fille de roi.
9:35	Ils allèrent pour l'enterrer, mais ils ne trouvèrent d'elle que le crâne, les pieds et les paumes des mains.
9:36	Ils retournèrent l'annoncer à Yehuw, qui dit : C'est la parole que YHWH avait déclarée par la main de son serviteur Éliyah<!--1 R. 21:23.-->, le Tishbiy, en disant : Dans le champ de Yizre`e'l les chiens mangeront la chair d'Iyzebel,
9:37	et le cadavre d'Iyzebel deviendra comme du fumier sur les faces des champs, dans le territoire de Yizre`e'l, de sorte qu'on ne pourra dire : C'est Iyzebel.

## Chapitre 10

### Accomplissement du jugement d'Elohîm sur la maison d'Achab

10:1	Achab avait 70 fils dans Samarie. Yehuw écrivit des lettres qu'il envoya à Samarie aux chefs de Yizre`e'l, aux anciens et aux gouverneurs d'Achab, en disant :
10:2	Maintenant, quand cette lettre vous sera parvenue, vous qui avez avec vous les fils de votre maître, et avec vous les chars et les chevaux, la ville forte et les armes,
10:3	regardez lequel des fils de votre seigneur est le meilleur et le plus juste, mettez-le sur le trône de son père et combattez pour la maison de votre seigneur !
10:4	Ils eurent une très grande peur et ils dirent : Voici, les deux rois n'ont pas tenu en face de lui. Comment tiendrons-nous ?
10:5	Et le chef de la maison, le chef de la ville, les anciens et les gouverneurs envoyèrent dire à Yehuw : Nous sommes tes serviteurs, nous ferons tout ce que tu nous diras. Nous ne ferons régner aucun homme, fais ce qui est bon à tes yeux.
10:6	Yehuw leur écrivit une seconde lettre, où il était dit : Si vous êtes pour moi et si vous obéissez à ma voix, prenez les têtes des fils de votre seigneur et venez auprès de moi demain à cette heure-ci, à Yizre`e'l. Or les 70 hommes, fils du roi, étaient avec les plus grands de la ville qui les élevaient.
10:7	Et il arriva que, quand la lettre leur parvint, ils prirent les fils du roi et tuèrent ces 70 hommes. Puis ils mirent leurs têtes dans des corbeilles et les envoyèrent à Yehuw, à Yizre`e'l.
10:8	Un messager vint l'en informer, en disant : Ils ont apporté les têtes des fils du roi. Et il dit : Mettez-les en deux tas à l'entrée de la porte, jusqu'au matin.
10:9	Et il arriva que, le matin, il sortit, et se tint là, et dit à tout le peuple : Vous êtes justes ! Voici, j'ai conspiré contre mon seigneur et je l'ai tué, mais qui a frappé tous ceux-ci ?
10:10	Sachez donc qu’il ne tombera rien à terre de la parole de YHWH<!--1 R. 21:19-24.-->, laquelle YHWH a déclarée contre la maison d'Achab. YHWH accomplit ce qu'il avait déclaré par la main de son serviteur Éliyah.
10:11	Yehuw tua aussi tous ceux qui restaient de la maison d'Achab à Yizre`e'l, tous ses grands, ses familiers et ses prêtres, sans en laisser échapper un seul.

### Mise à mort des frères d'Achazyah et de la lignée d'Achab<!--2 Ch. 22:8.-->

10:12	Il se leva et partit pour aller à Samarie. Et comme il était à Beth-Équed-des-Bergers<!--« Beth-Équed » signifie « Maison pour tondre les moutons ».--> sur le chemin,
10:13	Yehuw trouva les frères d'Achazyah, roi de Yéhouda, et leur dit : Qui êtes-vous ? Ils dirent : Nous sommes les frères d'Achazyah et nous sommes descendus pour la paix des fils du roi et des fils de la reine.
10:14	Il dit : Saisissez-les vivants ! Ils les saisirent vivants et les tuèrent à la citerne de Beth-Équed. Ils étaient 42 hommes. Il n'en laissa pas un seul.
10:15	Étant parti de là, il rencontra Yehonadab, fils de Rékab, qui venait au-devant de lui. Il le salua et lui dit : Ton cœur est-il droit comme mon cœur l'est à ton égard ? Yehonadab dit : Il l'est, il l'est ! Donne-moi ta main ! Il lui donna sa main et le fit monter auprès de lui dans son char.
10:16	Il dit : Viens avec moi et vois mon zèle pour YHWH ! Et il le fit asseoir dans son char.
10:17	Arrivé à Samarie, il frappa tous ceux qui restaient d'Achab à Samarie, jusqu’à ce qu’il les eut exterminés entièrement, selon la parole que YHWH avait déclarée à Éliyah.

### Mise à mort de tous les prophètes de Baal

10:18	Yehuw rassembla tout le peuple et leur dit : Achab a peu servi Baal<!--Jg. 2:13.-->, mais Yehuw le servira beaucoup.
10:19	Maintenant, appelez vers moi tous les prophètes de Baal, tous ses serviteurs et tous ses prêtres ! Que personne ne manque ! Oui, à moi le grand sacrifice pour Baal ! Quiconque manquera ne vivra pas ! Yehuw agissait avec ruse, pour faire périr les serviteurs de Baal.
10:20	Yehuw dit : Consacrez une assemblée pour Baal. Et on la proclama.
10:21	Yehuw envoya des messagers dans tout Israël et tous les serviteurs de Baal arrivèrent, il ne resta pas un homme qui ne vînt. Ils entrèrent dans la maison de Baal, et la maison de Baal fut remplie d'un bout à l'autre.
10:22	Il dit à celui qui avait la charge du vestiaire : Sors des vêtements pour tous les serviteurs de Baal. Et cet homme sortit des vêtements.
10:23	Yehuw et Yehonadab, fils de Rékab, entrèrent dans la maison de Baal. Il dit aux serviteurs de Baal : Cherchez et regardez afin qu'il n'y ait pas ici de serviteurs de YHWH. Prenez garde qu'il n'y ait seulement que les serviteurs de Baal.
10:24	Ils entrèrent pour faire des sacrifices et des holocaustes. Or Yehuw avait placé dehors 80 hommes, et leur avait dit : L’homme qui laissera échapper un de ces hommes que je remets entre vos mains, l’âme de l’un sera à la place de l’âme de l’autre.
10:25	Il arriva que dès qu'on eut achevé de faire l'holocauste, Yehuw dit aux coureurs et aux officiers : Entrez et frappez-les ! Pas un homme ne sortira ! Les coureurs et les officiers les frappèrent à bouche d’épée. Ils les jetèrent là, puis ils allèrent jusqu'à la ville de la maison de Baal.
10:26	Ils sortirent les monuments de la maison de Baal et les brûlèrent.
10:27	Ils démolirent le monument de Baal. Ils démolirent aussi la maison de Baal et en firent un lieu privé jusqu'à ce jour.
10:28	Yehuw extermina Baal d'Israël.

### L'idolâtrie dans la vie de Yehuw

10:29	Seulement, quant aux péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël, Yehuw ne s’en détourna pas : les veaux d'or<!--1 R. 12:28-29.-->, celui qui était à Béth-El et celui qui était à Dan.
10:30	YHWH dit à Yehuw : Parce que tu as bien exécuté ce qui était droit à mes yeux, que tu as fait à la maison d’Achab selon tout ce qui était dans mon cœur, tes fils, jusqu’à la quatrième génération, seront assis sur le trône d’Israël.
10:31	Yehuw ne prit pas garde à marcher de tout son cœur dans la torah de YHWH, l'Elohîm d'Israël, il ne se détourna pas des péchés de Yarobam, par lesquels il avait fait pécher Israël.

### Hazaël règne sur la Syrie

10:32	En ce jour-là, YHWH commença à entamer Israël, et Hazaël les frappa sur toute la frontière d’Israël.
10:33	Depuis le Yarden, jusqu'au soleil levant, il battit toute la terre de Galaad, les Gadites, les Reoubénites et ceux de Menashè, depuis Aroër sur le torrent de l'Arnon, jusqu'à Galaad et à Bashân.

### Yehoachaz règne sur Israël

10:34	Le reste des discours de Yehuw, tout ce qu'il a accompli, et toutes ses actions puissantes, ne sont-ils pas écrits dans le livre des discours du jour des rois d'Israël ?
10:35	Yehuw se coucha avec ses pères, et on l'enterra à Samarie. Et Yehoachaz, son fils, régna à sa place.
10:36	Yehuw avait régné 28 ans sur Israël à Samarie.

## Chapitre 11

### Athalyah fait périr la postérité royale de Yéhouda<!--2 Ch. 22:9-12.-->

11:1	Athalyah, mère d'Achazyah, voyant que son fils était mort, se leva et fit périr toute la postérité royale.
11:2	Yehosheba, fille du roi Yoram, sœur d'Achazyah, prit Yoash, fils d'Achazyah et le déroba du milieu des fils du roi, quand on les fit mourir, lui avec sa nourrice, dans la chambre des lits. On le cacha loin des faces d'Athalyah, de sorte qu'on ne le fit pas mourir.
11:3	Il fut caché 6 ans avec elle dans la maison de YHWH. Et Athalyah régnait sur la terre.

### Yoash devient roi de Yéhouda<!--2 Ch. 23:1-11.-->

11:4	La septième année, Yehoyada envoya chercher les chefs de centaines des Kéréthiens et des coureurs, et il les fit venir auprès de lui dans la maison de YHWH. Il traita alliance avec eux, les fit jurer dans la maison de YHWH, et leur montra le fils du roi.
11:5	Il leur donna cet ordre, en disant : Voici la chose que vous ferez : un tiers d’entre vous, ceux qui entrent le jour du shabbat, feront la garde de la maison du roi,
11:6	un tiers sera à la porte de Sour, et un tiers à la porte derrière les coureurs. Ainsi vous veillerez à la garde de la maison, afin que personne n'y entre par force.
11:7	Vos deux autres parties, tous ceux qui sortent le jour du shabbat feront la garde de la maison de YHWH auprès du roi.
11:8	Vous entourerez le roi de toutes parts, chaque homme ayant ses armes à la main, et l'on mettra à mort quiconque s'avancera dans les rangs. Vous serez avec le roi quand il sortira et quand il entrera.
11:9	Les chefs de centaines firent tout ce que Yehoyada, le prêtre, avait ordonné. Chaque homme prit ses hommes, ceux qui entraient le shabbat avec ceux qui sortaient le jour du shabbat, et ils vinrent auprès de Yehoyada, le prêtre.
11:10	Le prêtre donna aux chefs de centaines les lances et les boucliers du roi David qui étaient dans la maison de YHWH.
11:11	Les coureurs, ayant chacun ses armes à la main, se tenaient depuis le côté droit de la maison jusqu’au côté gauche de la maison, vers l’autel et vers la maison, près du roi, tout autour.
11:12	On fit sortir le fils du roi, et on mit sur lui la couronne<!--Couronne ou consacrer.--> et le témoignage. Ils le firent roi et l'oignirent, et frappant des paumes, ils dirent : Vive le roi !

### Mort d'Athalyah<!--2 Ch. 23:12-15,21.-->

11:13	Athalyah entendit la voix des coureurs et du peuple, et elle vint vers le peuple à la maison de YHWH.
11:14	Elle regarda et vit le roi qui se tenait debout près de la colonne, selon la coutume des rois. Les chefs et les trompettes étaient près du roi. Tout le peuple de la terre se réjouissait, et l'on sonnait des trompettes. Alors Athalyah déchira ses vêtements et cria : Conspiration ! Conspiration !
11:15	Le prêtre Yehoyada donna cet ordre aux chefs de centaines, qui avaient la charge de l'armée : Faites-la sortir hors des rangs et que celui qui la suivra soit mis à mort par l'épée ! Car le prêtre avait dit : Qu'elle ne soit pas mise à mort dans la maison de YHWH !
11:16	On mit la main sur elle, et elle arriva à la maison du roi par le chemin de l'entrée des chevaux : c'est là qu'elle fut tuée.

### Alliance entre Yehoyada, YHWH et le peuple ; réveil sous le règne de Yoash (Yehoash)<!--2 Ch. 23:16-21.-->

11:17	Yehoyada traita alliance entre YHWH et le roi et le peuple, pour qu’ils deviennent le peuple de YHWH, et entre le roi et le peuple.
11:18	Tout le peuple de la terre entra dans la maison de Baal. Ils la démolirent avec ses autels, brisèrent entièrement ses images et tuèrent aussi Matthan, prêtre de Baal, devant les autels. Le prêtre Yehoyada établit des gardiens dans la maison de YHWH.
11:19	Il prit les chefs de centaines, les Kéréthiens et les coureurs, et tout le peuple de la terre, et ils firent descendre le roi de la maison de YHWH. Ils entrèrent dans la maison du roi par le chemin de la porte des coureurs et il s'assit sur le trône des rois.
11:20	Tout le peuple de la terre se réjouissait et la ville était tranquille. On avait fait mourir Athalyah par l'épée dans la maison du roi.

## Chapitre 12

### Yoash (Yehoash) ordonne des réparations dans le temple<!--2 Ch. 24:2.-->

12:1	Yehoash était fils de 7 ans quand il devint roi.
12:2	La septième année de Yehuw, Yehoash devint roi et il régna 40 ans à Yeroushalaim. Le nom de sa mère était Tsibyah, de Beer-Shéba.
12:3	Yehoash fit ce qui est droit aux yeux de YHWH tout le temps que Yehoyada, le prêtre, l’instruisit.
12:4	Seulement les hauts-lieux ne furent pas abolis : le peuple sacrifiait encore et brûlait de l'encens sur les hauts lieux.
12:5	Yehoash dit aux prêtres : Tout l'argent consacré qu'on apporte dans la maison de YHWH, l’argent de tout homme qui passe par le dénombrement, l’argent des âmes selon l’estimation de chacun, tout argent qu’il monte au cœur de chacun d’apporter à la maison de YHWH,
12:6	que les prêtres le prennent, chacun de la part de ses connaissances, et qu'ils fortifient les brèches de la maison partout où l'on trouvera des brèches.
12:7	Il arriva qu'à la vingt-troisième année du roi Yehoash les prêtres n'avaient pas fortifié les brèches de la maison.
12:8	Le roi Yehoash appela le prêtre Yehoyada et les autres prêtres et il leur dit : Pourquoi n'avez-vous pas fortifié les brèches de la maison ? Maintenant, vous ne prendrez plus l'argent de vos connaissances, mais vous le livrerez pour les brèches de la maison.
12:9	Les prêtres consentirent à ne plus prendre l'argent du peuple pour fortifier les brèches de la maison.

### Offrandes volontaires pour réparer le temple<!--2 Ch. 24:8-14.-->

12:10	Le prêtre Yehoyada prit un coffre, le perça dans son couvercle et le plaça à côté de l'autel, à droite, à l'endroit par lequel on entrait à la maison de YHWH. Les prêtres qui avaient la garde du seuil y mettaient tout l'argent qu'on apportait à la maison de YHWH.
12:11	Et il arrivait que, lorsqu’ils voyaient qu'il y avait beaucoup d'argent dans le coffre, le scribe du roi montait avec le grand-prêtre, et ils mettaient dans des sacs l'argent qui se trouvait dans la maison de YHWH, et ils le comptaient.
12:12	Ils donnaient l'argent pesé entre les mains de ceux qui faisaient l’œuvre, des gardiens de la maison de YHWH. Ceux-ci le livraient aux artisans du bois et aux bâtisseurs qui travaillaient à la maison de YHWH,
12:13	pour les maçons et les tailleurs de pierres, pour acheter du bois et des pierres de taille, afin de réparer les brèches de la maison de YHWH, pour tous ceux qui sortaient vers la maison pour sa réparation.
12:14	Mais, avec l'argent qu'on apportait dans la maison de YHWH, on ne fit pour la maison de YHWH ni bassins d'argent ni mouchettes, ni cuvette, ni trompettes, ni aucun autre ustensile d'or, ou ustensile d'argent,
12:15	car on le donnait à ceux qui faisaient l’œuvre et qui fortifiaient la maison de YHWH.
12:16	Et on ne demandait pas de comptes aux hommes entre les mains desquels on donnait l'argent pour qu'ils le donnent à ceux qui faisaient l'ouvrage, car ils agissaient fidèlement.
12:17	L'argent des sacrifices de culpabilité et l'argent des sacrifices pour les péchés n'étaient pas apportés dans la maison de YHWH : c'était pour les prêtres.

### Invasion syrienne évitée ; mort de Yoash

12:18	Alors Hazaël<!--Hazaël envahit Yéhouda à deux reprises. Ce passage fait mention de la première invasion ; la deuxième invasion est relatée en 2 Ch. 24:23.-->, roi de Syrie, monta et fit la guerre à Gath dont il s'empara. Hazaël se mit en face pour monter contre Yeroushalaim.
12:19	Mais Yehoash, roi de Yéhouda, prit tout ce qui était consacré, que Yehoshaphat, Yehoram et Achazyah, ses pères, rois de Yéhouda, avaient consacré, tout ce que lui-même avait consacré et tout l'or qui se trouva dans les trésors de la maison de YHWH et de la maison du roi, et il envoya le tout à Hazaël, roi de Syrie, et celui-ci ne monta pas contre Yeroushalaim.
12:20	Le reste des discours de Yoash, tout ce qu'il a accompli, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
12:21	Ses serviteurs se soulevèrent et se liguèrent, et ils frappèrent Yoash dans la maison de Millo, qui est à la descente de Silla.
12:22	Yozakar, fils de Shimeath, et Yehozabad fils de Shomer, ses serviteurs, le frappèrent et il mourut. On l'enterra avec ses pères dans la cité de David. Et Amatsyah, son fils, régna à sa place.

## Chapitre 13

### Yehoachaz règne sur Israël

13:1	La vingt-troisième année de Yoash, fils d'Achazyah, roi de Yéhouda, Yehoachaz, fils de Yehuw, devint roi sur Israël à Samarie, pour 17 ans.
13:2	Il fit ce qui est mal aux yeux de YHWH, car il marcha après les péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël, il ne s'en détourna pas.

### L'idolâtrie perdure sur la terre

13:3	La colère de YHWH s'enflamma contre Israël, et il les livra entre les mains de Hazaël, roi de Syrie, et entre les mains de Ben-Hadad, fils de Hazaël, tout le temps que ces rois vécurent.
13:4	Mais Yehoachaz supplia les faces de YHWH, et YHWH l’écouta, car il vit l’oppression d’Israël, car le roi de Syrie les opprimait.
13:5	YHWH donna un sauveur à Israël et ils échappèrent aux mains des Syriens. Ainsi les fils d'Israël habitèrent dans leurs tentes comme hier et avant-hier.
13:6	Mais ils ne se détournèrent pas des péchés de la maison de Yarobam par lesquels il avait fait pécher Israël. Ils s'y livrèrent, et même l'asherah<!--Voir commentaire Jg. 2:13.--> resta debout à Samarie.
13:7	Car il n’avait pas laissé de peuple à Yehoachaz, sauf 50 cavaliers, 10 chars et 10 000 hommes de pied, car le roi de Syrie les avait fait périr et les avait rendus semblables à la poussière qu'on foule aux pieds.

### Mort de Yehoachaz ; Yoash (Yehoash) règne sur Israël

13:8	Le reste des discours de Yehoachaz, tout ce qu'il a accompli, et ses actions puissantes, cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
13:9	Yehoachaz se coucha avec ses pères, et on l'enterra à Samarie. Et Yoash, son fils, régna à sa place.
13:10	La trente-septième année de Yoash, roi de Yéhouda, Yehoash, fils de Yehoachaz, devint roi sur Israël à Samarie, pour 16 ans.
13:11	Il fit ce qui est mal aux yeux de YHWH, il ne se détourna d'aucun des péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël, il s'y livra comme lui.

### Mort de Yoash

13:12	Le reste des discours de Yoash, tout ce qu'il a accompli, ses actions puissantes, et la guerre qu'il eut avec Amatsyah, roi de Yéhouda, tout cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
13:13	Yoash se coucha avec ses pères, et Yarobam s'assit sur son trône. Yoash fut enterré à Samarie avec les rois d'Israël.

### Fin de la vie d'Éliysha : récit de la visite de Yoash roi d'Israël

13:14	Éliysha était malade de la maladie dont il mourut. Yoash, roi d'Israël, descendit vers lui, pleura sur ses faces en disant : Mon père ! Mon père ! Char d'Israël et ses cavaliers !
13:15	Éliysha lui dit : Prends un arc et des flèches. Il prit un arc et des flèches.
13:16	Éliysha dit au roi d'Israël : Appuie ta main sur l’arc ! Quand il y eut appuyé sa main, Éliysha mit ses mains sur les mains du roi,
13:17	en lui disant : Ouvre la fenêtre à l'orient ! Et il l'ouvrit. Éliysha lui dit : Tire ! Après qu'il eut tiré, il lui dit : C'est la flèche de la délivrance de la part de YHWH, la flèche de la délivrance contre les Syriens. Tu frapperas les Syriens à Aphek, jusqu'à leur extermination.
13:18	Il dit : Prends les flèches ! Il les prit. Il dit au roi d'Israël : Frappe contre terre ! Il frappa 3 fois et s'arrêta.
13:19	L'homme d'Elohîm se mit dans une très grande colère contre lui et dit : Il fallait frapper 5 ou 6 fois ! Alors tu aurais frappé les Syriens jusqu'à leur extermination. Maintenant, c'est 3 fois seulement que tu frapperas les Syriens.

### Mort d'Éliysha (Élisée) : ses os rendent la vie à un mort

13:20	Éliysha mourut, et on l'enterra. Des troupes de Moabites entrèrent sur la terre à la venue de l’année.
13:21	Il arriva qu'on enterrait un homme et voici qu'on vit une de ces troupes. On jeta l'homme dans le sépulcre d'Éliysha. L'homme alla toucher les os d'Éliysha, il reprit vie et se leva sur ses pieds.

### Fin de l'oppression syrienne

13:22	Hazaël, roi de Syrie, opprima Israël tous les jours de Yehoachaz.
13:23	YHWH eut pitié d’eux et leur fit miséricorde, il se tourna vers eux à cause de son alliance avec Abraham, Yitzhak et Yaacov. Il n’a pas voulu les détruire et il ne les a pas jetés loin de ses faces jusqu'à maintenant.
13:24	Hazaël, roi de Syrie, mourut, et Ben-Hadad, son fils, régna à sa place.
13:25	Yehoash, fils de Yehoachaz, retourna et prit de la main de Ben-Hadad, fils d'Hazaël, les villes que ce dernier avait prises de la main de Yehoachaz, son père pendant la guerre. Yoash le battit 3 fois et recouvra les villes d'Israël.

## Chapitre 14

### Amatsyah règne sur Yéhouda<!--2 Ch. 25:1-4.-->

14:1	La deuxième année de Yoash, fils de Yoachaz, roi d'Israël, Amatsyah, fils de Yoash, roi de Yéhouda, devint roi.
14:2	Il était fils de 25 ans quand il devint roi et il régna 29 ans à Yeroushalaim. Le nom de sa mère était Yehoaddan, de Yeroushalaim.
14:3	Il fit ce qui est droit aux yeux de YHWH, non pas toutefois comme David, son père. Il fit selon tout ce que Yoash son père avait fait.
14:4	Seulement, les hauts lieux ne furent pas ôtés. Le peuple sacrifiait encore et brûlait de l’encens sur les hauts lieux.
14:5	Il arriva que dès que le royaume fut affermi entre ses mains, il frappa ses serviteurs qui avaient tué le roi, son père.
14:6	Mais il ne fit pas mourir les fils de ceux qui l'avaient tué, selon ce qui est écrit dans le livre de la torah de Moshé, où YHWH donne ce commandement, en disant : On ne fera pas mourir les pères pour les fils et l'on ne fera pas mourir les fils pour les pères, mais on fera mourir chaque homme pour son péché<!--De. 24:16 ; Ez. 18:4,20.-->.
14:7	Il frappa 10 000 édomites dans la vallée du sel, et dans cette guerre il prit Séla. Il l'appela du nom de Yoktheel jusqu'à ce jour.
14:8	Alors Amatsyah envoya des messagers vers Yehoash, fils de Yehoachaz, fils de Yehuw, roi d'Israël, pour lui dire : Viens, voyons-nous en face !
14:9	Yehoash, roi d'Israël, envoya dire à Amatsyah, roi de Yéhouda : L'épine du Liban envoya dire au cèdre du Liban : Donne ta fille en mariage à mon fils ! Et les bêtes sauvages qui sont au Liban passèrent et foulèrent l'épine.
14:10	Tu as frappé, frappé Édom et ton cœur s'est élevé ! Sois glorieux et reste dans ta maison. Pourquoi exciterais-tu le mal par lequel tu tomberas, toi et Yéhouda avec toi ?
14:11	Mais Amatsyah n'écouta pas. Yehoash, roi d'Israël, monta et ils se virent en face, lui et Amatsyah, roi de Yéhouda, à Beth-Shémesh, qui est à Yéhouda.
14:12	Yéhouda fut battu en face d'Israël, et ils s'enfuirent, chaque homme vers ses tentes.
14:13	Yehoash, roi d'Israël, prit Amatsyah, roi de Yéhouda, fils de Yehoash, fils d'Achazyah, à Beth-Shémesh. Puis il vint à Yeroushalaim et fit une brèche de 400 coudées dans la muraille de Yeroushalaim, depuis la porte d'Éphraïm, jusqu'à la porte de l'angle.
14:14	Il prit tout l'or et l'argent, tous les objets qui se trouvaient dans la maison de YHWH et dans les trésors de la maison du roi, ainsi que des fils en otage, et il retourna à Samarie.

### Yarobam II règne sur Israël

14:15	Le reste des discours de Yehoash, ce qu'il a accompli, ses actions puissantes et comment il a combattu contre Amatsyah, tout cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
14:16	Yehoash se coucha avec ses pères et fut enterré à Samarie avec les rois d'Israël. Et Yarobam, son fils, régna à sa place.

### Mort d'Amatsyah ; Azaryah (Ouzyah) règne sur Yéhouda<!--2 Ch. 25:26-28.-->

14:17	Amatsyah, fils de Yoash, roi de Yéhouda, vécut 15 ans après la mort de Yehoash, fils de Yehoachaz, roi d'Israël.
14:18	Le reste des actions d'Amatsyah n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
14:19	On forma une conspiration contre lui à Yeroushalaim et il s'enfuit à Lakis, mais on le poursuivit à Lakis où on le fit mourir.
14:20	On le transporta sur des chevaux, et il fut enterré à Yeroushalaim avec ses pères, dans la cité de David.
14:21	Tout le peuple de Yéhouda prit Azaryah, fils de 16 ans, et ils l'établirent roi à la place d'Amatsyah, son père.
14:22	C'est lui qui bâtit Élath et la fit retourner à Yéhouda, après que le roi se coucha avec ses pères.

### Prophétie de Yonah (Jonas) accomplie par Yarobam II (Jéroboam II)

14:23	La quinzième année d'Amatsyah, fils de Yoash, roi de Yéhouda, Yarobam, fils de Yoash, devint roi sur Israël à Samarie et il régna 41 ans.
14:24	Il fit ce qui est mal aux yeux de YHWH, et ne se détourna d'aucun des péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël.
14:25	Il rétablit les frontières d'Israël depuis l'entrée de Hamath, jusqu'à la mer de la région aride, selon la parole de YHWH, l'Elohîm d'Israël, qu'il avait déclarée par la main de son serviteur Yonah<!--Jon. 1:1.-->, fils d'Amitthaï, le prophète, de Gath-Hépher.
14:26	Car YHWH vit que l'affliction d’Israël était très révoltante, que ce qui était détenu et ce qui était libre avaient pris fin, et qu’il n’y avait personne pour secourir Israël.
14:27	Or YHWH n’avait pas dit qu’il effacerait le nom d’Israël de dessous les cieux, et il les délivra par les mains de Yarobam, fils de Yoash.

### Zekaryah règne sur Israël

14:28	Le reste des discours de Yarobam, tout ce qu'il a accompli, ses actions puissantes, comment il a combattu et comment il a ramené à Israël Damas et Hamath, qui avaient appartenu à Yéhouda, cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
14:29	Yarobam se coucha avec ses pères, avec les rois d'Israël. Et Zekaryah, son fils, régna à sa place.

## Chapitre 15

### Yéhouda demeure dans l'idolâtrie sous le règne d'Azaryah (Ouzyah)<!--2 R. 14:21-22 ; 2 Ch. 26:1-15.-->

15:1	La vingt-septième année de Yarobam, roi d'Israël, Azaryah<!--Azaryah (Ouzyah, selon 2 Ch. 26:1-15) fut couronné à 16 ans et mourut à 68 ans.-->, fils d'Amatsyah, roi de Yéhouda, régna.
15:2	Il était fils de 16 ans quand il devint roi et il régna 52 ans à Yeroushalaim. Le nom de sa mère était Yekolyah, de Yeroushalaim.
15:3	Il fit ce qui est droit aux yeux de YHWH, selon tout ce qu'avait fait Amatsyah, son père.
15:4	Seulement, les hauts-lieux ne furent pas abolis : le peuple sacrifiait encore et brûlait de l'encens sur les hauts-lieux.

### Jugement de YHWH sur Azaryah (Ouzyah) par la lèpre<!--2 Ch. 26:16-21.-->

15:5	YHWH frappa le roi, et il devint lépreux jusqu'au jour de sa mort, et il demeura dans une maison séparée. Et Yotham, fils du roi, avait la charge de la maison, jugeant le peuple de la terre.
15:6	Le reste des discours d'Azaryah, tout ce qu'il a accompli, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
15:7	Azaryah se coucha avec ses pères et fut enterré avec ses pères dans la cité de David, et Yotham, son fils, régna à sa place.

### Conspiration de Shalloum contre Zekaryah, roi d'Israël

15:8	La trente-huitième année d'Azaryah, roi de Yéhouda, Zekaryah, fils de Yarobam, devint roi sur Israël à Samarie, pour 6 mois.
15:9	Il fit ce qui est mal aux yeux de YHWH, comme avaient fait ses pères. Il ne se détourna pas des péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël.
15:10	Shalloum, fils de Yabesh, fit une conspiration contre lui, et le frappa devant le peuple. Il le tua et régna à sa place.
15:11	Quant au reste des discours de Zekaryah, voilà, elles sont écrites dans le livre des discours du jour des rois d'Israël.
15:12	C’est là la parole de YHWH qu'il avait déclarée à Yehuw, en disant : Tes fils seront assis sur le trône d'Israël jusqu'à la quatrième génération. Et il en fut ainsi<!--2 R. 10:30.-->.

### Shalloum règne sur Israël ; sa mort

15:13	Shalloum, fils de Yabesh, devint roi la trente-neuvième année d'Ouzyah, roi de Yéhouda. Il régna pendant un mois à Samarie.
15:14	Menahem, fils de Gadi, monta de Tirtsah et vint dans Samarie. Il frappa Shalloum, fils de Yabesh, à Samarie, le fit mourir et régna à sa place.
15:15	Le reste des actions de Shalloum, et la conspiration qu'il forma, cela est écrit dans le livre des discours du jour des rois d'Israël.

### Menahem règne sur Israël

15:16	Alors Menahem frappa Thiphsach et tous ceux qui y étaient, avec son territoire depuis Tirtsah. Il la frappa parce qu'elle ne lui avait pas ouvert ses portes. Il fendit le ventre de toutes les femmes enceintes.
15:17	La trente-neuvième année d'Azaryah, roi de Yéhouda, Menahem, fils de Gadi devint roi sur Israël pour 10 ans, à Samarie.
15:18	Il fit ce qui est mal aux yeux de YHWH. Il ne se détourna pas des péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël.

### Invasion d'Israël par le roi d'Assyrie<!--1 Ch. 5:26.-->

15:19	Poul, roi d'Assyrie, vint contre la terre, et Menahem donna 1 000 talents d'argent à Poul, pour que ses mains soient avec lui et pour affermir son royaume entre ses mains.
15:20	Menahem leva cet argent sur Israël, sur tous les hommes vaillants et talentueux pour le donner au roi d'Assyrie : 50 sicles d'argent pour chaque homme. Ainsi, le roi d'Assyrie s'en retourna et ne s'arrêta pas sur la terre.
15:21	Le reste des actions de Menahem, tout ce qu'il a fait, cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?

### Mort de Menahem ; Peqachyah règne sur Israël

15:22	Menahem se coucha avec ses pères, et Peqachyah, son fils, régna à sa place.
15:23	La cinquantième année d'Azaryah, roi de Yéhouda, Peqachyah, fils de Menahem devint roi sur Israël, à Samarie, pour 2 ans.
15:24	Il fit ce qui est mal aux yeux de YHWH, il ne se détourna pas des péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël.

### Pékach tue Peqachyah et devient roi d'Israël

15:25	Pékach, fils de Remalyah, son officier, conspira contre lui. Il le frappa à Samarie, dans le palais de la maison royale, de même qu'Argob et Arié. Il avait avec lui 50 hommes d'entre les fils des Galaadites. Il le fit mourir et régna à sa place.
15:26	Le reste des discours de Peqachyah, tout ce qu'il a accompli, cela est écrit dans le livre des discours du jour des rois d'Israël.
15:27	La cinquante-deuxième année d'Azaryah, roi de Yéhouda, Pékach, fils de Remalyah devint roi sur Israël, à Samarie, pour 20 ans.
15:28	Il fit ce qui est mal aux yeux de YHWH et ne se détourna pas des péchés de Yarobam, fils de Nebath, par lesquels il avait fait pécher Israël.
15:29	Aux jours de Pékach, roi d'Israël, Tiglath-Piléser, roi d'Assyrie, vint et prit Iyôn, Abel-Beth-Ma'akah, Yanoach, Kédesh, Hatsor, Galaad et la Galilée, et même toute la terre de Nephthali, et il emmena captifs les habitants en Assyrie.

### Hoshea (Osée) conspire contre Pékach et règne sur Israël

15:30	Hoshea, fils d'Élah, forma une conspiration contre Pékach, fils de Remalyah, le frappa et le fit mourir. Il régna à sa place la vingtième année de Yotham, fils d'Ouzyah.
15:31	Le reste des discours de Pékach, tout ce qu'il a accompli, cela est écrit dans le livre des discours du jour des rois d'Israël.

### Yotham règne sur Yéhouda ; sa mort<!--2 R. 15:2 ; 2 Ch. 26:23, 27:1-9.-->

15:32	La seconde année de Pékach, fils de Remalyah, roi d'Israël, Yotham, fils d'Ouzyah, roi de Yéhouda, devint roi.
15:33	Il était fils de 25 ans quand il devint roi. Il régna 16 ans à Yeroushalaim. Le nom de sa mère était Yerousha, fille de Tsadok.
15:34	Il fit ce qui est droit aux yeux de YHWH. Il fit selon tout ce qu’avait fait Ouzyah son père.
15:35	Seulement, les hauts-lieux ne furent pas abolis : le peuple sacrifiait encore et brûlait de l'encens sur les hauts-lieux. C’est lui qui bâtit la porte supérieure de la maison de YHWH.
15:36	Le reste des discours de Yotham, tout ce qu'il a accompli, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
15:37	Dans ces jours-là, YHWH commença à envoyer contre Yéhouda, Retsin, roi de Syrie, et Pékach, fils de Remalyah.
15:38	Yotham se coucha avec ses pères, et il fut enterré dans la cité de David, son père. Et Achaz, son fils, régna à sa place.

## Chapitre 16

### Achaz règne sur Yéhouda<!--2 R. 15:38 ; 2 Ch. 28:1-4.-->

16:1	La dix-septième année de Pékach, fils de Remalyah, Achaz, fils de Yotham, roi de Yéhouda, devint roi.
16:2	Achaz était fils de 20 ans quand il devint roi. Il régna 16 ans à Yeroushalaim. Il ne fit pas ce qui est droit aux yeux de YHWH, son Elohîm, comme David son père.
16:3	Mais il marcha dans la voie des rois d'Israël. Il fit même passer son fils par le feu, selon les abominations des nations que YHWH avait chassées devant les fils d'Israël.
16:4	Il sacrifiait et brûlait de l'encens sur les hauts lieux, sur les collines et sous tout arbre vert.

### Yéhouda envahi par les rois d'Assyrie et d'Israël<!--2 Ch. 28:5-19.-->

16:5	Alors Retsin, roi de Syrie, et Pékach, fils de Remalyah, roi d'Israël, montèrent contre Yeroushalaim pour lui faire la guerre. Ils assiégèrent Achaz mais ne purent en venir à bout par les armes.
16:6	Dans ce même temps, Retsin, roi de Syrie, fit rentrer Élath au pouvoir des Syriens. Il expulsa les Juifs d'Élath et les Syriens vinrent à Élath, où ils ont demeuré jusqu'à ce jour.

### Le roi d'Assyrie vient en aide à Achaz et s'empare de Damas<!--2 Ch. 28:16-25.-->

16:7	Achaz envoya des messagers à Tiglath-Piléser, roi d'Assyrie, pour lui dire : Je suis ton serviteur et ton fils. Monte et délivre-moi de la paume du roi des Syriens et de la paume du roi d'Israël qui s'élèvent contre moi.
16:8	Achaz prit l'argent et l'or qui se trouvaient dans la maison de YHWH, et dans les trésors de la maison royale, et il les envoya en pot-de-vin au roi d'Assyrie.
16:9	Le roi d'Assyrie l'écouta : il monta contre Damas, la prit, emmena les habitants en captivité à Kir et fit mourir Retsin.
16:10	Le roi Achaz s'en alla à la rencontre de Tiglath-Piléser, roi d'Assyrie, à Damas. Et ayant vu l'autel<!--Achaz, roi de Yéhouda, se rendit chez le roi d'Assyrie et fut fasciné par l'autel de son dieu au point de le convoiter. Il demanda au prêtre Ouriyah de fabriquer un autel identique, dont le modèle n'était pas celui que YHWH avait décrit à Moshé. Il introduisit un objet de culte d'origine païenne dans le temple de Yeroushalaim, sous prétexte d'honorer YHWH. Certains « Pères de l'Église », ainsi que les empereurs Constantin Ier (272-337) et Théodose Ier (347-395), se sont comportés comme Achaz en adoptant les pratiques païennes. Les historiens s'accordent à dire que la diffusion de la parole d'Elohîm sous le règne de Constantin Ier (règne : 306-337), empereur de Rome, avait des fins strictement politiques. Cette politique a eu deux conséquences concernant l'influence de l'Assemblée et son fonctionnement de plus en plus éloigné de la parole d'Elohîm :\\- Les peuples païens ont introduit leurs rites idolâtres au sein de l'Assemblée. En effet, les dogmes de l'institution devaient plaire à la majorité.\\- L'Assemblée chrétienne cessant d'être persécutée, son fonctionnement intimiste fondé sur l'implication de chaque croyant et l'exercice de la prêtrise universelle des chrétiens, a changé à cause de l'effet de masse. Devenant numériquement très importants, il a fallu imposer une autorité capable de contenir un nombre de fidèles de plus en plus élevés. Mais à cause de cette augmentation numérique et de la présence de « faux convertis » liées au fait que l'adhésion au christianisme (religion chrétienne fondée par les êtres humains) devenait une obligation, l'étude de la parole, la fraction du pain et la prière ne pouvaient plus perdurer. C'est ainsi que beaucoup d'assemblées ont commencé à subir l'influence du monde.--> qui était à Damas, le roi Achaz envoya au prêtre Ouriyah, la forme et le modèle exact de cet autel.
16:11	Le prêtre Ouriyah construisit un autel entièrement d'après le modèle envoyé de Damas par le roi Achaz, et le prêtre Ouriyah le fit avant que le roi Achaz soit de retour de Damas.
16:12	Quand le roi Achaz revint de Damas et vit l'autel, il s'en approcha et y monta.
16:13	Il fit brûler son holocauste et son sacrifice, versa ses libations et aspergea l'autel avec le sang de ses sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->.
16:14	Quant à l’autel de cuivre qui était devant YHWH, il le fit avancer de devant la maison, d’entre son autel et la maison de YHWH, et il le plaça à côté de cet autel-là, vers le nord.
16:15	Et le roi Achaz donna cet ordre au prêtre Ouriyah : Fais brûler l'holocauste du matin et l'offrande du soir, l'holocauste du roi et son offrande, les holocaustes de tout le peuple de la terre et leurs offrandes, verses-y leurs libations, et asperge-le avec tout le sang des holocaustes et tout le sang des sacrifices. Mais pour ce qui concerne l'autel de cuivre, je m'en occuperai.
16:16	Le prêtre Ouriyah fit tout ce que le roi Achaz lui avait ordonné.
16:17	Le roi Achaz brisa les rebords des bases et en ôta les cuves qui étaient dessus. Il descendit la mer de dessus les bœufs de cuivre qui étaient sous elle et il la posa sur un pavé de pierre.
16:18	Il changea aussi dans la maison de YHWH, à cause du roi d'Assyrie, la structure couverte du shabbat qu'on y avait bâtie et l'entrée extérieure du roi.

### Mort d'Achaz ; Hizqiyah (Ézéchias) devient roi de Yéhouda<!--2 Ch. 28:26-27.-->

16:19	Le reste des discours d'Achaz et tout ce qu'il a accompli, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
16:20	Achaz se coucha avec ses pères et il fut enterré avec ses pères dans la cité de David. Et Hizqiyah, son fils, régna à sa place.

## Chapitre 17

### Hoshea (Osée) devient le dernier roi d'Israël

17:1	La douzième année d'Achaz, roi de Yéhouda, Hoshea, fils d'Élah devint roi sur Israël, à Samarie, pour 9 ans.
17:2	Il fit ce qui est mal aux yeux de YHWH, non pas toutefois comme les rois d'Israël qui avaient été avant lui.

### Hoshea tente de s'affranchir du joug de l'Assyrie

17:3	Salmanasar<!--Le royaume d'Israël a été détruit en 722 av. J.-C., par l'empereur assyrien Salmanasar V (règne : 727 – 722 av. J.-C.), après avoir assiégé trois ans le roi Hoshea (règne : 732 – 722 av. J.-C.) dans sa capitale Samarie. Celui-ci ne payait plus le tribut et essayait d'obtenir l'appui de l'Égypte pour retrouver l'indépendance. Le royaume d'Israël a disparu au début du 8ème siècle av. J.-C., provoquant la dispersion dans le monde de plusieurs Juifs issus des dix tribus. L'origine des Samaritains remonte à cette déportation, après que le royaume du Nord soit tombé aux mains de Salmanasar, roi d'Assyrie. Malgré les déportations, les Assyriens n'avaient pas laissé déserte cette région appelée « Samarie », plusieurs Israélites y étaient restés et des colons d'autres provinces assyriennes vinrent s'y établir. Les Samaritains sont issus du mélange de ces populations, et leur religion est un mélange entre le culte à YHWH avec celui des dieux étrangers.-->[^footnote], roi d'Assyrie, monta contre lui et Hoshea lui fut assujetti et lui paya un tribut.
17:4	Mais le roi d'Assyrie découvrit une conspiration chez Hoshea, qui avait envoyé des messagers vers So, roi d'Égypte, et qui ne payait plus le tribut d'année en année au roi d'Assyrie. C'est pourquoi le roi d'Assyrie le fit enfermer et enchaîner dans une maison d'arrêt.

### Siège de Samarie par le roi d'Assyrie

17:5	Le roi d'Assyrie monta contre toute la terre ; il monta contre Samarie qu'il assiégea pendant 3 ans.

### Les causes de la captivité d'Israël par l'Assyrie

[^footnote]:
17:6	La neuvième année d'Hoshea, le roi d'Assyrie prit Samarie et emmena Israël captif en Assyrie. Il les fit habiter à Chalach, et sur le Chabor, fleuve de Gozan, et dans les villes des Mèdes.
17:7	Cela arriva parce que les fils d'Israël péchèrent contre YHWH, leur Elohîm, qui les avait fait monter hors de la terre d'Égypte, de dessous la main de pharaon, roi d'Égypte, et parce qu'ils craignirent d'autres elohîm.
17:8	Ils marchèrent dans les statuts des nations que YHWH avait chassées devant les fils d'Israël, et ceux des rois d'Israël qu'ils avaient établis.
17:9	Les fils d'Israël firent en secret des choses qui n'étaient pas droites contre YHWH leur Elohîm. Ils se bâtirent des hauts lieux dans toutes leurs villes, depuis la tour des gardes jusqu'aux villes fortes.
17:10	Ils se dressèrent des monuments et des asherah sur toutes les hautes collines et sous tout arbre vert.
17:11	Et là, ils brûlèrent de l'encens sur tous les hauts lieux, comme les nations que YHWH avait chassées devant eux, et ils firent des choses mauvaises pour irriter YHWH.
17:12	Ils servirent les idoles, au sujet desquelles YHWH leur avait dit : Vous ne ferez pas cela<!--1 R. 12:28.--> !
17:13	YHWH fit avertir Israël et Yéhouda par la main de tous ses prophètes, tous les voyants, en disant : Détournez-vous de toutes vos mauvaises voies, revenez, et gardez mes commandements et mes statuts, selon toute la torah que j'ai prescrite à vos pères et que je vous ai envoyée par la main de mes serviteurs les prophètes.
17:14	Mais ils n'écoutèrent pas : ils durcirent leur cou, comme le cou de leurs pères, qui n'avaient pas cru en YHWH, leur Elohîm.
17:15	Ils rejetèrent ses lois, l'alliance qu'il avait traitée avec leurs pères, ainsi que les témoignages par lesquels il les avait avertis. Ils marchèrent derrière la vanité et devinrent vains derrière les nations qui les entouraient, alors que YHWH leur avait ordonné de ne pas agir comme elles.
17:16	Ils abandonnèrent tous les commandements de YHWH, leur Elohîm, ils firent deux veaux en métal fondu, ils fabriquèrent des idoles d'Asherah<!--Voir commentaire en Jg. 2:13.-->, ils se prosternèrent devant toute l'armée des cieux et ils servirent Baal.
17:17	Ils firent aussi passer leurs fils et leurs filles par le feu, ils pratiquèrent la divination et la sorcellerie, et ils se vendirent pour faire ce qui est mal aux yeux de YHWH afin de l'irriter.
17:18	YHWH fut très en colère contre Israël et il les rejeta. Il n'est resté que la seule tribu de Yéhouda.
17:19	Même Yéhouda n'avait pas gardé les commandements de YHWH, son Elohîm, mais ils marchèrent dans les statuts qu'Israël avait établis.
17:20	YHWH rejeta toute la postérité d'Israël. Il les humilia et les livra entre les mains des pillards, jusqu’à ce qu’il les jetât loin de ses faces.
17:21	Car Israël s'était détaché de la maison de David, et avait établi roi Yarobam, fils de Nebath. Yarobam avait détourné Israël de suivre YHWH, et les avait pécher d’un grand péché.
17:22	Les fils d'Israël marchèrent dans tous les péchés que Yarobam avait commis, ils ne s'en détournèrent pas,
17:23	jusqu'à ce que YHWH ait écarté Israël loin de ses faces, comme il l'avait déclaré par la main de tous ses serviteurs les prophètes. Et Israël fut emmené captif loin de son sol, en Assyrie, jusqu'à ce jour.

### Jugement sur les étrangers occupant les villes d'Israël

17:24	Le roi d'Assyrie fit venir des gens de Babel<!--Babylone.-->, de Kouth, d'Avva, de Hamath et de Sepharvaïm. Il les fit habiter dans les villes de Samarie, à la place des fils d'Israël. Ils prirent possession de la Samarie et habitèrent dans ses villes.
17:25	Et il arriva, lorsqu'ils commencèrent à y habiter, qu'ils ne craignaient pas YHWH, et YHWH envoya contre eux des lions qui vinrent les tuer.
17:26	On parla au roi d'Assyrie, en disant : Les nations que tu as transportées et fait habiter dans les villes de Samarie ne connaissent pas les ordonnances<!--« Jugement », « justice », « coutume », « manière ». Voir Lé. 18:4-5.--> de l'Elohîm de la terre, c'est pourquoi il a envoyé contre elles des lions, et voilà que ceux-ci les tuent parce qu'elles ne connaissent pas les ordonnances de l'Elohîm de la terre.

### L'idolâtrie dans les villes occupées

17:27	Le roi d'Assyrie donna cet ordre, en disant : Faites partir là-bas l'un des prêtres que vous avez emmenés de là en captivité. Qu'il parte pour s'y établir et qu'il leur enseigne<!--Mal. 2:7.--> les ordonnances de l'Elohîm de la terre.
17:28	Un des prêtres qui avaient été emmenés captifs de Samarie vint s'établir à Béth-El et leur enseigna comment ils devaient craindre YHWH.
17:29	Il arriva que, nation par nation, elles firent leurs elohîm et les placèrent dans les maisons des hauts lieux bâties par les Samaritains, nation par nation, dans leurs villes, là où elles habitaient.
17:30	Les hommes de Babel firent Soukkoth-Benoth, les hommes de Kouth firent Nergal, et les hommes de Hamath firent Ashima.
17:31	Les Avviens firent Nibchaz et Thartak. Les Sépharviens brûlaient leurs fils par le feu à Adrammélek et Anammélek, les éloah de Sepharvaïm.
17:32	Il arriva qu'ils craignaient aussi YHWH et se firent, parmi eux, des prêtres de hauts lieux qui venaient officier pour eux dans les maisons des hauts lieux.
17:33	Ils craignaient YHWH et ils servaient aussi leurs elohîm, selon les ordonnances des nations d'où on les avait transportés.
17:34	Et jusqu'à ce jour ils agissent selon leurs premières ordonnances. Ils ne craignent pas YHWH. Ils n'agissent pas selon ses statuts et ses ordonnances, ni selon la torah et les commandements prescrits par YHWH Elohîm aux fils de Yaacov qu'il appela du nom d'Israël.
17:35	YHWH avait traité alliance avec eux et leur avait donné cet ordre, en disant : Vous ne craindrez pas d'autres elohîm, vous ne vous prosternerez pas devant eux, vous ne les servirez pas et vous ne leur sacrifierez pas.
17:36	Mais vous craindrez YHWH, qui vous a fait monter hors de la terre d'Égypte avec une grande puissance et à bras étendu. C’est devant lui que vous vous prosternerez, et c’est à lui que vous sacrifierez.
17:37	Vous observerez et mettrez toujours en pratique les statuts, les ordonnances, la torah et les commandements, qu'il a écrits pour vous, et vous ne craindrez pas d'autres elohîm.
17:38	Vous n'oublierez pas l'alliance que j'ai traitée avec vous et vous ne craindrez pas d'autres elohîm.
17:39	Mais vous craindrez YHWH, votre Elohîm, et il vous délivrera de la main de tous vos ennemis.
17:40	Et ils n’écoutèrent pas, mais ils agirent selon leur première coutume.
17:41	C’est ainsi que ces nations craignaient YHWH tout en servant leurs images gravées. Leurs fils et les fils de leurs fils agissent jusqu'à ce jour comme leurs pères ont agi.

## Chapitre 18

### Hizqiyah (Ézéchias) règne sur Yéhouda<!--2 R. 16:20 ; 2 Ch. 29:1-31:21.-->

18:1	Il arriva, la troisième année d'Hoshea, fils d'Élah, roi d'Israël, que Hizqiyah, fils d'Achaz, roi de Yéhouda, devint roi.
18:2	Il était fils de 25 ans quand il devint roi et il régna 29 ans à Yeroushalaim. Le nom de sa mère était Abi, fille de Zekaryah.
18:3	Il fit ce qui est droit aux yeux de YHWH, selon tout ce qu'avait fait David, son père.

### Mouvement de réveil sous Hizqiyah (Ézéchias)<!--2 Ch. 29:3-31:21.-->

18:4	Il abolit les hauts lieux, mit en pièces les monuments, découpa les asherah et il brisa le serpent de cuivre que Moshé avait fait, car les fils d'Israël avaient jusqu'alors brûlé de l'encens devant lui, ils l'appelaient Nehoushtân.
18:5	Il se confia en YHWH, l'Elohîm d'Israël. Parmi tous les rois de Yéhouda qui vinrent après ou qui le précédèrent, il n'y en eut pas de semblable à lui.
18:6	Il s'attacha à YHWH sans se détourner de lui et il observa les commandements que YHWH avait prescrits à Moshé.

### Révolte contre l'Assyrie ; victoire sur les Philistins

18:7	YHWH fut avec Hizqiyah. Partout où il sortait, il prospérait. Il se révolta contre le roi d'Assyrie et ne lui fut plus assujetti.
18:8	Il frappa les Philistins jusqu'à Gaza et ravagea leur territoire depuis les tours des gardes jusqu'aux villes fortes.

### Captivité d'Israël par l'Assyrie<!--2 R. 17:4-6.-->

18:9	Il arriva, la quatrième année du roi Hizqiyah, qui était la septième du règne d'Hoshea, fils d'Élah, roi d'Israël, que Salmanasar, roi d'Assyrie, monta contre Samarie et l'assiégea.
18:10	Il la prit au bout de 3 ans, la sixième année du règne d'Hizqiyah, qui était la neuvième d'Hoshea, roi d'Israël, Samarie fut prise.
18:11	Le roi d'Assyrie emmena Israël en Assyrie et il les établit à Chalach, sur le Chabor, fleuve de Gozan, et dans les villes des Mèdes,
18:12	parce qu'ils n'avaient pas obéi à la voix de YHWH, leur Elohîm, et qu'ils avaient transgressé son alliance, parce qu'ils n'avaient ni écouté ni mis en pratique tout ce qu'avait ordonné Moshé, serviteur de YHWH.

### Invasion de Yéhouda par Sanchérib<!--2 Ch. 32:1-15,30 ; Es. 36:1-10.-->

18:13	La quatorzième année du roi Hizqiyah, Sanchérib, roi d'Assyrie, monta contre toutes les villes fortes de Yéhouda et les prit.
18:14	Hizqiyah, roi de Yéhouda, envoya dire au roi d'Assyrie à Lakis : J'ai commis une faute ! Éloigne-toi de moi. Je payerai tout ce que tu m'imposeras. Et le roi d'Assyrie imposa à Hizqiyah, roi de Yéhouda, 300 talents d'argent et 30 talents d'or.
18:15	Hizqiyah donna tout l'argent qui se trouvait dans la maison de YHWH et dans les trésors de la maison royale.
18:16	En ce temps-là, Hizqiyah enleva les lames d'or dont il avait couvert les portes et les linteaux du temple de YHWH, pour les livrer au roi d'Assyrie.
18:17	Le roi d'Assyrie envoya de Lakis à Yeroushalaim, vers le roi Hizqiyah, Tharthan, Rab-Saris et Rabshaké avec une puissante armée. Ils montèrent et arrivèrent à Yeroushalaim. Lorsqu'ils furent montés et arrivés, ils s'arrêtèrent à l'aqueduc de l'étang supérieur, qui est sur le chemin du champ du foulon.
18:18	Ils appelèrent le roi. Élyakim, fils de Chilqiyah, préposé sur la maison, Shebna, le scribe et Yoach, fils d'Asaph, l'archiviste, sortirent vers eux.
18:19	Rabshaké leur dit : S’il vous plaît, dites à Hizqiyah : Ainsi parle le grand roi, le roi d'Assyrie : Quelle est cette confiance sur laquelle tu t'appuies ?
18:20	Tu as dit : Il faut pour la guerre le conseil et la force. Mais ce ne sont que des paroles. Mais en qui as-tu placé ta confiance, pour te rebeller contre moi ?
18:21	Voici maintenant, tu l'as placée dans l'Égypte, dans ce roseau cassé, qui pénètre et perce la paume de quiconque s'appuie dessus ! Tel est pharaon, roi d'Égypte, pour tous ceux qui se confient en lui.
18:22	Peut-être me direz-vous : Nous nous confions en YHWH, notre Elohîm. Mais n'est-ce pas celui dont Hizqiyah a détruit les hauts lieux et les autels, en disant à Yéhouda et à Yeroushalaim : Vous vous prosternerez devant cet autel à Yeroushalaim ?
18:23	Maintenant, s'il te plaît, donne des otages au roi d'Assyrie, mon maître, et je te donnerai 2 000 chevaux, si tu peux donner autant de cavaliers pour les monter.
18:24	Comment repousserais-tu un seul gouverneur d'entre les serviteurs de mon maître ? Mais tu mets ta confiance dans l'Égypte, à cause des chars et des cavaliers.
18:25	Maintenant, est-ce sans la bouche de YHWH que je suis monté contre ce lieu pour le détruire ? YHWH m'a dit : Monte contre cette terre et détruis-la !

### Menaces de Rabshaké<!--2 Ch. 32:16,18-19 ; Es. 36:11-21.-->

18:26	Élyakim, fils de Chilqiyah, Shebna et Yoach dirent à Rabshaké : S'il te plaît, parle à tes serviteurs en araméen, car nous écoutons, mais ne nous parle pas en hébreu, aux oreilles du peuple qui est sur la muraille.
18:27	Rabshaké leur dit : Est-ce vers ton maître et auprès de toi que mon maître m'a envoyé pour prononcer ces paroles ? N'est-ce pas vers ces hommes assis sur la muraille pour manger avec vous leurs excréments et boire leur urine ?
18:28	Rabshaké, s'étant avancé, cria à grande voix en hébreu, il parla et dit : Écoutez la parole du grand roi, le roi d'Assyrie !
18:29	Ainsi parle le roi : Que Hizqiyah ne vous trompe pas, car il ne pourra pas vous délivrer de ma main.
18:30	Qu'Hizqiyah ne vous amène pas à vous confier en YHWH, en disant : YHWH nous délivrera, il délivrera et cette ville ne sera pas livrée entre les mains du roi d'Assyrie.
18:31	N'écoutez pas Hizqiyah, car ainsi parle le roi d'Assyrie : Faites la paix avec moi et rendez-vous à moi, et chacun de vous mangera de sa vigne, de son figuier et chacun boira de l'eau de sa citerne,
18:32	jusqu'à ce que je vienne et que je vous emmène vers une terre comme votre terre, une terre de blé et de vin nouveau, une terre de pain et de vignes, une terre d'oliviers qui portent de l'huile, et de miel, et vous vivrez et vous ne mourrez pas. Mais n'écoutez pas Hizqiyah, car il pourrait vous séduire, en disant : YHWH nous délivrera.
18:33	Les elohîm des nations ont-ils délivré chacun leur terre de la main du roi d'Assyrie ?
18:34	Où sont les elohîm de Hamath et d'Arpad ? Où sont les elohîm de Sépharvaïm, d'Héna et d'Ivva ? Oui, ont-ils délivré Samarie de ma main ?
18:35	Parmi tous les elohîm de ces terres, quels sont ceux qui ont délivré leur terre de ma main, pour dire que YHWH délivrera Yeroushalaim de ma main ?
18:36	Le peuple se tut et on ne lui répondit pas un mot, car le roi avait donné cet ordre en disant : Vous ne lui répondrez pas.
18:37	Élyakim, fils de Chilqiyah, chef de la maison du roi, et Shebna le scribe, et Yoach, fils d'Asaph, l'archiviste, vinrent auprès d'Hizqiyah, les vêtements déchirés, et ils lui rapportèrent les paroles de Rabshaké.

## Chapitre 19

### Hizqiyah (Ézéchias) demande à Yesha`yah (Ésaïe) de consulter YHWH<!--2 Ch. 32:20-22 ; Es. 36:22-37:5.-->

19:1	Il arriva que lorsque le roi Hizqiyah eut entendu cela, il déchira ses vêtements, se couvrit d'un sac et entra dans la maison de YHWH.
19:2	Il envoya Élyakim, chef de la maison du roi, Shebna, le scribe, et les plus anciens des prêtres, couverts de sacs, vers Yesha`yah, le prophète, fils d'Amots.
19:3	Ils lui dirent : Ainsi parle Hizqiyah : Ce jour est un jour d'angoisse, de châtiment et d'opprobre, car les fils sont venus jusqu'au lieu de brèche, mais il n'y a pas de force pour enfanter.
19:4	Peut-être YHWH, ton Elohîm, a-t-il entendu toutes les paroles de Rabshaké, que le roi d'Assyrie, son maître, a envoyé pour blasphémer Elohîm le Vivant, et peut-être YHWH, ton Elohîm, exercera-t-il ses châtiments à cause des paroles qu'il a entendues. Fais une prière pour le reste qui subsiste encore.
19:5	Les serviteurs du roi Hizqiyah vinrent vers Yesha`yah.

### Réponse de YHWH<!--Es. 37:6-7.-->

19:6	Et Yesha`yah leur dit : Voici ce que vous direz à votre maître : Ainsi parle YHWH : N'aie pas peur en face des paroles que tu as entendues, par lesquelles les serviteurs du roi d'Assyrie m'ont blasphémé.
19:7	Voici, je vais mettre en lui un esprit tel que, sur une nouvelle qu'il entendra, il retournera vers sa terre, et je le ferai tomber par l'épée sur sa terre.

### Défi du roi d'Assyrie à l'Elohîm d'Israël<!--2 Ch. 32:17 ; Es. 37:8-13.-->

19:8	Rabshaké s'étant retiré, trouva le roi d'Assyrie qui attaquait Libnah, car il avait entendu qu'il était parti de Lakis.
19:9	Il entendit dire au sujet de Tirhaka<!--Tirhaka ou Taharqa était le roi de Napata et un pharaon noir de 690 à 664 av. J.-C. À cette époque, il y avait des pharaons noirs qui régnaient en Égypte. C'était la 25ème dynastie, dite « éthiopienne » ou « soudanaise ».-->, roi d'Éthiopie : Voici, il est sorti pour te combattre. C'est pourquoi le roi d'Assyrie retourna vers sa terre, mais il envoya des messagers à Hizqiyah, en leur disant :
19:10	Vous parlerez ainsi à Hizqiyah, roi de Yéhouda, et lui direz : Que ton Elohîm, en qui tu te confies, ne te trompe pas en te disant : Yeroushalaim ne sera pas livrée entre les mains du roi d'Assyrie.
19:11	Voici, tu as entendu ce que les rois d'Assyrie ont fait à toutes les terres, et comment ils les ont dévouées par interdit. Et toi, tu serais délivré ?
19:12	Les elohîm des nations que mes pères ont détruites les ont-ils délivrées ? Gozan, Charan, Retseph et les fils d'Éden qui sont à Telassar ?
19:13	Où sont le roi de Hamath, le roi d'Arpad, et le roi de la ville de Sepharvaïm, d'Héna et d'Ivva ?

### Hizqiyah (Ézéchias) dans le temple, sa prière à YHWH<!--2 Ch. 32:20 ; Es. 37:14-20.-->

19:14	Hizqiyah prit la lettre des mains des messagers et la lut. Puis Hizqiyah monta à la maison de YHWH et la déploya devant YHWH.
19:15	Hizqiyah pria devant YHWH et dit : YHWH, Elohîm d'Israël, qui est assis entre les chérubins, c'est toi qui es le seul Elohîm de tous les royaumes de la Terre, c'est toi qui as fait les cieux et la Terre.
19:16	YHWH ! Incline ton oreille et écoute. Ouvre tes yeux et regarde. Écoute les paroles de Sanchérib, celles qu’il a envoyées pour blasphémer Elohîm le Vivant.
19:17	YHWH, les rois d'Assyrie ont vraiment détruit ces nations et ravagé leurs terres,
19:18	et ils ont jeté dans le feu leurs elohîm, car ce n’étaient pas des elohîm, mais des ouvrages de mains humaines, du bois, et de la pierre, c'est pourquoi ils les ont détruits.
19:19	Maintenant, YHWH, notre Elohîm ! S'il te plaît, délivre-nous de la main de Sanchérib, afin que tous les royaumes de la Terre sachent que c'est toi, YHWH, qui es le seul Elohîm.

### YHWH répond au travers de Yesha`yah<!--Es. 37:21-35.-->

19:20	Yesha`yah, fils d'Amots, envoya dire à Hizqiyah : Ainsi parle YHWH, l'Elohîm d'Israël : Quant à la prière que tu m’as faite au sujet de Sanchérib, roi d'Assyrie, je l’ai entendue.
19:21	Voici la parole que YHWH a déclarée contre lui : Elle te méprise, elle se moque de toi, la fille, vierge de Sion. Elle hoche la tête après toi, la fille de Yeroushalaim.
19:22	Qui as-tu insulté et blasphémé ? Contre qui as-tu élevé la voix ? Tu as porté tes yeux en haut, vers le Saint d'Israël !
19:23	Tu as insulté Adonaï par la main de tes messagers et tu as dit : Avec la multitude de mes chars, de mes chars, je suis monté au sommet des montagnes, aux extrémités du Liban. Je couperai les plus hauts de ses cèdres et les plus beaux de ses cyprès, et j'atteindrai sa dernière cime, la forêt de son verger.
19:24	J'ai creusé des sources, après avoir bu les eaux étrangères et je tarirai avec la plante de mes pieds tous les fleuves de l'Égypte.
19:25	N’as-tu pas entendu que j’ai fait cela dès longtemps, et que je l’ai formé dès les jours d’autrefois ? Maintenant je l’ai fait venir, pour que tu mettes en ruine, en désolation, en tas de pierres et de ruines les villes fortes. 
19:26	Leurs habitants ont la main courte, ils sont épouvantés et honteux. Ils sont devenus comme l’herbe des champs, la végétation d'un jardin potager, l'herbe verte des toits et le champ avant que le blé ne soit sur pied<!--Es. 37:27.-->.
19:27	Ta demeure, ta sortie et ton entrée, je les connais, ainsi que ta rage contre moi.
19:28	Parce que ta rage et ton arrogance sont montées à mes oreilles, je mettrai ma boucle à tes narines, et mon mors entre tes lèvres, et je te ferai retourner par le chemin par lequel tu es venu.
19:29	Ceci sera pour toi le signe : on mangera cette année ce qui provient des grains répandus, et la deuxième année, le grain qui pousse de lui-même. Mais la troisième année, vous sèmerez et vous moissonnerez, vous planterez des vignes et vous en mangerez le fruit.
19:30	Ce qui aura été épargné de la maison de Yéhouda, ce qui sera resté poussera encore des racines par-dessous et produira du fruit par-dessus.
19:31	Car il sortira de Yeroushalaim un reste, et de la Montagne de Sion des rescapés. Voilà ce que fera le zèle de YHWH Tsevaot.
19:32	C'est pourquoi ainsi parle YHWH, sur le roi d'Assyrie : Il n'entrera pas dans cette ville, il n'y lancera aucune flèche, il ne se présentera pas contre elle avec le bouclier et il n'élèvera pas des tertres contre elle.
19:33	Il retournera par le chemin par lequel il est venu, et il n'entrera pas dans cette ville, – déclaration de YHWH.
19:34	Je protègerai cette ville, afin de la sauver, par amour pour moi et par amour pour David, mon serviteur.

### L'ange de YHWH dans le camp des Assyriens<!--Es. 37:36-38.-->

19:35	Il arriva cette nuit-là que l'Ange de YHWH sortit et frappa 185 000 hommes dans le camp des Assyriens. Quand on se leva tôt le matin, voici, ils étaient tous morts.

### Mort de Sanchérib, roi d'Assyrie<!--Es. 37:37-38 ; 2 Ch. 32:21.-->

19:36	Sanchérib, roi d'Assyrie, leva son camp, partit et s'en retourna, et il resta à Ninive.
19:37	Il arriva, comme il était prosterné dans la maison de Nisrok, son elohîm, qu'Adrammélek et Sharetser, ses fils, le tuèrent avec l'épée, puis ils se sauvèrent en terre d'Ararat. Ésar-Haddon, son fils, devint roi à sa place.

## Chapitre 20

### Hizqiyah (Ézéchias) malade puis guéri par YHWH<!--2 Ch. 32:24 ; Es. 38.-->

20:1	En ces jours-là, Hizqiyah fut malade à la mort. Le prophète Yesha`yah, fils d'Amots, vint auprès de lui, et lui dit : Ainsi parle YHWH : Donne tes ordres à ta maison, car tu vas mourir et tu ne vivras plus.
20:2	Il tourna ses faces contre le mur et fit sa prière à YHWH, en disant :
20:3	Oh ! Je te prie, YHWH ! Souviens-toi que j'ai marché devant toi avec fidélité et intégrité de cœur, et que j'ai fait ce qui est agréable à tes yeux ! Et Hizqiyah pleura, il pleura beaucoup.
20:4	Et il arriva que Yesha`yah, étant sorti, et n'étant pas encore arrivé au milieu de la ville, la parole de YHWH lui est apparue, disant :
20:5	Retourne, et dis à Hizqiyah, chef de mon peuple : Ainsi parle YHWH, l'Elohîm de David, ton père : J'ai entendu ta prière, j'ai vu tes larmes. Voici, je te guérirai. Dans trois jours tu monteras à la maison de YHWH.
20:6	J'ajouterai 15 ans à tes jours, je te délivrerai, toi et cette ville, de la paume du roi d'Assyrie. Je protégerai cette ville par amour pour moi et par amour pour mon serviteur David.
20:7	Yesha`yah dit : Prenez un gâteau de figues ! Ils le prirent et le mirent sur l'ulcère, et il vit.
20:8	Hizqiyah avait dit à Yesha`yah : À quel signe connaîtrai-je que YHWH me guérira et qu'au troisième jour, je monterai à la maison de YHWH ?
20:9	Yesha`yah dit : Voici pour toi le signe de YHWH. Oui, YHWH accomplira la parole qu'il a déclarée : L'ombre s'avancera-t-elle de 10 marches, ou reviendra-t-elle de 10 marches ?
20:10	Yehizqiyah<!--Ézéchias.--> dit : C'est peu de chose que l'ombre s'avance de 10 marches ! Non, que l'ombre revienne en arrière de 10 marches !
20:11	Yesha`yah, le prophète, invoqua YHWH, qui fit revenir l'ombre de 10 marches en arrière sur les marches d'Achaz<!--Achaz, père de Yehizqiyah, grand imitateur du culte assyrien (2 R. 16:10,18), avait peut-être fait installer dans son palais un appareil servant à mesurer le temps, qui sous le règne de Yehizqiyah avait gardé son nom : « les marches ou les degrés d’Achaz ». D'après Hérodote (2:109), ce sont les Babyloniens qui ont inventé la division du jour en douze parties et les cadrans solaires.--> où elle était descendue.

### Visite des ambassadeurs babyloniens ; prophétie sur la captivité babylonienne<!--2 Ch. 32:25-31 ; Es. 39.-->

20:12	En ce temps-là, Berodac-Baladan, fils de Baladan, roi de Babel, envoya une lettre avec un présent à Hizqiyah, parce qu'il avait appris la maladie d'Hizqiyah.
20:13	Hizqiyah les écouta et leur montra toute la maison de son trésor, l'argent, l'or, les aromates, l'huile précieuse, toute sa maison d'armes et tout ce qui se trouvait dans ses trésors. Il n'y eut rien que Hizqiyah ne leur montra dans sa maison et dans tout son royaume.
20:14	Yesha`yah, le prophète, vint ensuite auprès du roi Hizqiyah, et lui dit : Qu'ont dit ces hommes ? Et d'où sont-ils venus vers toi ? Hizqiyah dit : Ils sont venus d'une terre très éloignée, ils sont venus de Babel.
20:15	Il dit : Qu'ont-ils vu dans ta maison ? Hizqiyah dit : Ils ont vu tout ce qui est dans ma maison : il n'y a rien dans mes trésors que je ne leur aie montré.
20:16	Yesha`yah dit à Hizqiyah : Écoute la parole de YHWH !
20:17	Voici, les jours viendront où tout ce qui est dans ta maison et ce que tes pères ont amassé dans leurs trésors jusqu'à ce jour, sera emporté à Babel ; il n'en restera rien, dit YHWH<!--La déportation des Juifs à Babel (Babylone). Voir 2 R. 24-25.-->.
20:18	On prendra même de tes fils<!--2 R. 24:12 ; 2 Ch. 33:11 ; Da. 1.--> qui seront sortis de toi, que tu auras engendré, afin qu'ils soient eunuques dans le palais du roi de Babel.
20:19	Hizqiyah dit à Yesha`yah : La parole de YHWH que tu as prononcée est bonne. Et il dit : N'y aura-t-il pas paix et sécurité pendant mes jours ?

### Mort d'Hizqiyah (Ézéchias) ; Menashè (Manassé) règne sur Yéhouda<!--2 Ch. 32:32-33.-->

20:20	Le reste des discours d'Hizqiyah, toutes ses actions puissantes, et comment il a fait l'étang et l'aqueduc par lequel il a fait entrer les eaux dans la ville, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
20:21	Hizqiyah se coucha avec ses pères. Et Menashè, son fils, régna à sa place.

## Chapitre 21

### Abominations et idolâtrie de Menashè<!--2 Ch. 33:1-9.-->

21:1	Menashè, fils de 12 ans devint roi. Il régna 55 ans à Yeroushalaim. Le nom de sa mère était Hephtsiba.
21:2	Il fit ce qui est mal aux yeux de YHWH, selon les abominations des nations que YHWH avait chassées devant les fils d'Israël.
21:3	Il retourna et rebâtit les hauts lieux que Hizqiyah, son père, avait détruits, il éleva des autels pour le Baal et il fit une asherah<!--Voir commentaire en Jg. 2:13.-->, comme avait fait Achab, roi d'Israël, il se prosterna devant toute l'armée des cieux et la servit.
21:4	Il bâtit des autels dans la maison de YHWH, dont YHWH avait dit : C'est à Yeroushalaim que j'établirai mon Nom<!--Voir 2 Ch. 6:6.-->.
21:5	Il bâtit des autels pour toute l'armée des cieux dans les deux parvis de la maison de YHWH.
21:6	Il fit aussi passer son fils par le feu, il pratiquait le spiritisme et la divination. Il établit des gens qui évoquaient les morts et ceux qui avaient un esprit de divination. Il fit de plus en plus ce qui est mal aux yeux de YHWH pour l'irriter.
21:7	Il mit aussi l'idole d'Asherah qu'il avait faite, dans la maison dont YHWH avait dit à David, et à Shelomoh, son fils : C'est dans cette maison, dans Yeroushalaim, que j'ai choisie parmi toutes les tribus d'Israël, que je mettrai mon Nom<!--Voir 2 Ch. 6:6, 7:16.--> à perpétuité.
21:8	Je ne ferai plus errer le pied d'Israël hors de ce sol que j'ai donné à leurs pères, pourvu seulement qu'ils aient soin de mettre en pratique tout ce que je leur ai ordonné et toute la torah que Moshé, mon serviteur, leur a prescrite.
21:9	Mais ils n'obéirent pas, Menashè les égara, jusqu'à faire le mal plus que les nations que YHWH avait exterminées devant les fils d'Israël.

### Jugement de YHWH contre Yéhouda

21:10	YHWH parla par la main de ses serviteurs les prophètes, en disant :
21:11	Parce que Menashè, roi de Yéhouda, a commis ces abominations, parce qu'il a fait pire que tout ce qu'avaient fait avant lui les Amoréens, et parce qu'il a aussi fait pécher Yéhouda par ses idoles,
21:12	à cause de cela, ainsi dit YHWH, l'Elohîm d'Israël : Voici que je fais venir sur Yeroushalaim et sur Yéhouda un malheur tel que les deux oreilles en tinteront à quiconque l'entendra !
21:13	J'étendrai sur Yeroushalaim le cordeau de Samarie et le niveau de la maison d'Achab. Je nettoierai Yeroushalaim comme un plat qu'on nettoie, et qu'on renverse sur son fond après l'avoir nettoyé.
21:14	J'abandonnerai le reste de mon héritage et je les livrerai entre les mains de leurs ennemis. Ils deviendront le butin et la proie de tous leurs ennemis,
21:15	parce qu'ils ont fait ce qui est mal à mes yeux, et qu'ils m'ont irrité depuis le jour où leurs pères sont sortis d'Égypte, jusqu'à ce jour.

### Meurtres de Menashè ; sa mort<!--2 Ch. 33:11-20.-->

21:16	Menashè répandit aussi en très grande quantité le sang innocent, jusqu'à en remplir Yeroushalaim d'extrémité en extrémité, outre son péché par lequel il fit pécher Yéhouda en faisant ce qui est mal aux yeux de YHWH.
21:17	Le reste des discours de Menashè, tout ce qu'il a accompli, les péchés auxquels il se livra, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
21:18	Menashè se coucha avec ses pères et il fut enterré dans le jardin de sa maison, dans le jardin d'Ouzza. Amon, son fils, régna à sa place.

### Amon règne sur Yéhouda ; sa mort<!--2 Ch. 33:20-25.-->

21:19	Amon était fils de 22 ans quand il devint roi. Il régna 2 ans à Yeroushalaim. Le nom de sa mère était Meshoullémeth, fille de Harouts, de Yotbah.
21:20	Il fit ce qui est mal aux yeux de YHWH, comme avait fait Menashè, son père.
21:21	Il marcha dans toute la voie où avait marché son père, il servit les idoles que son père avait servies et se prosterna devant elles.
21:22	Il abandonna YHWH, l'Elohîm de ses pères et il ne marcha pas dans la voie de YHWH.

### Yoshiyah (Josias), roi de Yéhouda<!--2 Ch. 33:24-25.-->

21:23	Les serviteurs d'Amon conspirèrent contre lui et le tuèrent dans sa maison.
21:24	Mais le peuple de la terre frappa tous ceux qui avaient conspiré contre le roi Amon, et le peuple de la terre établit roi son fils Yoshiyah à sa place.
21:25	Le reste des actions d'Amon, ce qu'il a fait, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
21:26	On l'enterra dans son sépulcre, dans le jardin d'Ouzza. Et Yoshiyah, son fils, régna à sa place.

## Chapitre 22

### Droiture de Yoshiyah ; réparations dans le temple<!--2 Ch. 34:2-13.-->

22:1	Yoshiyah était fils de 8 ans quand il devint roi. Il régna 31 ans à Yeroushalaim. Le nom de sa mère était Yediydah, fille d'Adayah, de Botskath.
22:2	Il fit ce qui est droit aux yeux de YHWH et il marcha dans toute la voie de David, son père. Il ne s'en détourna ni à droite ni à gauche.
22:3	Il arriva, la dix-huitième année du roi Yoshiyah, que le roi envoya dans la maison de YHWH, Shaphan, le scribe, fils d'Atsalyah, fils de Meshoullam.
22:4	Il lui dit : Monte vers Chilqiyah, le grand-prêtre, et dis-lui d'amasser l'argent qui a été apporté dans la maison de YHWH et que ceux qui ont la garde du seuil ont recueilli du peuple.
22:5	On le remettra entre les mains de ceux qui font l’ouvrage, et qui sont gardiens de la maison de YHWH. Et ils le remettront à ceux qui font l’ouvrage dans la maison de YHWH pour fortifier les brèches de la maison,
22:6	aux artisans, aux bâtisseurs et aux maçons, pour les achats du bois et des pierres de taille pour réparer la maison.
22:7	Mais on ne leur demandera pas de comptes pour l'argent remis entre leurs mains, parce qu'ils agissent fidèlement.

### Découverte et lecture du livre de la torah<!--2 Ch. 34:14-19.-->

22:8	Chilqiyah, le grand-prêtre, dit à Shaphan, le scribe : J'ai trouvé le livre de la torah dans la maison de YHWH. Et Chilqiyah donna ce livre à Shaphan qui le lut.
22:9	Shaphan, le scribe, alla vers le roi et lui rapporta la chose, et dit : Tes serviteurs ont fondu l'argent qui se trouvait dans la maison et l'ont remis entre les mains de ceux qui font l'ouvrage, les gardiens de la maison de YHWH.
22:10	Shaphan, le scribe, raconta au roi, disant : Le prêtre Chilqiyah m'a donné un livre. Et Shaphan le lut devant le roi.
22:11	Il arriva que dès que le roi eut entendu les paroles du livre de la torah, il déchira ses vêtements.

### Annonce du jugement de YHWH par Houldah<!--2 Ch. 34:20-28.-->

22:12	Il donna cet ordre au prêtre Chilqiyah, à Achikam, fils de Shaphan, à Acbor, fils de Miykayah, à Shaphan, le scribe, et à Asayah, serviteur du roi en disant :
22:13	Allez, consultez YHWH pour moi, pour le peuple et pour tout Yéhouda, au sujet des paroles de ce livre qui a été trouvé. Car grand est le courroux de YHWH qui s'est enflammé contre nous, parce que nos pères n'ont pas obéi aux paroles de ce livre et n'ont pas mis en pratique tout ce qui nous y est prescrit.
22:14	Le prêtre Chilqiyah, Achikam, Acbor, Shaphan et Asayah, allèrent auprès de la prophétesse Houldah, femme de Shalloum, fils de Tiqvah, fils de Harhas, gardien des vêtements. Elle habitait dans un autre quartier de Yeroushalaim.

### YHWH rassure Yoshiyah par la prophétesse Houldah<!--2 Ch. 34:22-28.-->

22:15	Elle leur dit : Ainsi parle YHWH, l'Elohîm d'Israël : Dites à l'homme qui vous a envoyé vers moi :
22:16	Ainsi parle YHWH : Voici, je fais venir le malheur sur cette ville et sur ses habitants, selon toutes les paroles du livre que le roi de Yéhouda a lu.
22:17	Parce qu'ils m'ont abandonné et qu'ils ont brûlé de l’encens à d'autres elohîm, pour m'irriter par toutes les actions de leurs mains, mon courroux s'est enflammé contre cette ville et il ne s'éteindra pas.
22:18	Mais quant au roi de Yéhouda qui vous a envoyé pour consulter YHWH, vous lui direz : Ainsi parle YHWH, l'Elohîm d'Israël, au sujet des paroles que tu as entendues :
22:19	Parce que ton cœur a été touché, et que tu t'es humilié devant YHWH en entendant ce que j'ai déclaré contre cette ville et contre ses habitants, qui deviendront un objet d'épouvante et de malédiction, et parce que tu as déchiré tes vêtements, et que tu as pleuré devant moi, moi aussi, j'ai entendu, – déclaration de YHWH.
22:20	C'est pourquoi voici, je vais te recueillir auprès de tes pères, et tu seras recueilli dans ton sépulcre en paix et tes yeux ne verront pas tout le malheur que je fais venir sur cette ville. Ils rapportèrent toutes ces paroles au roi.

## Chapitre 23

### Le livre de la torah lu au peuple<!--2 Ch. 34:29-30.-->

23:1	Le roi envoya rassembler auprès de lui tous les anciens de Yéhouda et de Yeroushalaim.
23:2	Le roi monta à la maison de YHWH, avec tous les hommes de Yéhouda, tous les habitants de Yeroushalaim, les prêtres, les prophètes, et tout le peuple, depuis le plus petit jusqu'au plus grand. Il lut devant eux toutes les paroles du livre de l'alliance, qui avait été trouvé dans la maison de YHWH.

### Engagement de Yoshiyah et du peuple à suivre la torah de YHWH<!--2 Ch. 34:31-32.-->

23:3	Le roi se tenait debout près de la colonne. Il traita alliance devant YHWH, pour marcher derrière YHWH, pour garder ses ordonnances, ses préceptes et ses statuts de tout son cœur et de toute son âme, en accomplissant les paroles de l'alliance écrites dans ce livre. Et tout le peuple se tint à cette alliance.

### Yoshiyah débarrasse Yéhouda de toutes ses idoles<!--2 Ch. 34:33.-->

23:4	Le roi ordonna à Chilqiyah, le grand-prêtre, aux prêtres du second ordre et à ceux qui gardaient le seuil, de sortir hors du temple de YHWH tous les ustensiles qui avaient été faits pour Baal<!--Voir commentaire en Jg. 2:12.-->, pour Asherah<!--Voir commentaire en Jg. 2:13.-->, et pour toute l'armée des cieux. Il les brûla hors de Yeroushalaim, dans les champs de Cédron, et en fit porter la poussière à Béth-El.
23:5	Il chassa les prêtres idolâtres, que les rois de Yéhouda avaient établis pour brûler de l'encens sur les hauts lieux, dans les villes de Yéhouda et aux environs de Yeroushalaim, et ceux qui brûlaient de l’encens pour Baal, pour le soleil, pour la lune, pour le zodiaque et pour toute l'armée des cieux.
23:6	Il sortit de la maison de YHWH l'idole d'Asherah, qu'il transporta hors de Yeroushalaim vers le torrent de Cédron. Il la brûla au torrent de Cédron, la pulvérisa en poussière et il en jeta les cendres sur le sépulcre des fils du peuple.
23:7	Il détruisit les maisons des hommes prostitués qui se prostituent qui étaient dans la maison de YHWH, maisons où les femmes tissaient des tentes pour Asherah.
23:8	Il fit venir des villes de Yéhouda tous les prêtres et il rendit impurs les hauts lieux où les prêtres brûlaient de l'encens, depuis Guéba jusqu'à Beer-Shéba. Il renversa les hauts lieux des portes : à l'entrée de la porte de Yéhoshoua, chef de la ville, sur la gauche de l'homme, à la porte de la ville.
23:9	Toutefois, les prêtres des hauts lieux ne montaient pas à l'autel de YHWH à Yeroushalaim, mais ils mangeaient des pains sans levain parmi leurs frères.
23:10	Le roi rendit impur Topheth qui est dans la vallée de Ben-Hinnom, pour que nul homme ne fasse plus passer son fils ou sa fille par le feu pour Moloc<!--Lé. 20:2-3.-->.
23:11	Il fit disparaître de l'entrée de la maison de YHWH les chevaux que les rois de Yéhouda avaient consacrés au soleil, près de la chambre de l'eunuque Nethan-Mélek, situé à Parvarim, et il brûla au feu les chars du soleil.
23:12	Le roi brisa les autels qui étaient sur le toit de la chambre haute d'Achaz, que les rois de Yéhouda avaient faits et les autels que Menashè avait faits dans les deux parvis de la maison de YHWH, et de là il courut en jeter la poussière dans le torrent du Cédron.
23:13	Le roi rendit impurs les hauts lieux qui étaient en face de Yeroushalaim, au sud de la montagne de perdition, que Shelomoh, roi d'Israël, avait bâtis à Astarté, l'abomination des Sidoniens, à Kemosh, l'abomination des Moabites, et à Milcom, l'abomination des fils d'Ammon.
23:14	Il brisa aussi les monuments, découpa les asherah et remplit d'ossements humains les lieux où elles étaient.
23:15	Même l'autel qui était à Béth-El, le haut lieu qu'avait fait Yarobam<!--1 R. 12:20-23.-->, fils de Nebath, qui avait fait pécher Israël, même cet autel et le haut-lieu, il les renversa. Il brûla le haut lieu et le pulvérisa en poussière, et il brûla l'Asherah.
23:16	Yoshiyah s'étant tourné et ayant vu les sépulcres qui étaient là dans la montagne, envoya prendre les ossements des sépulcres, et il les brûla sur l'autel et le rendit impur, selon la parole de YHWH proclamée par l'homme<!--1 R. 13:1-2.--> d'Elohîm qui avait proclamé ces choses.
23:17	Le roi dit : Quel est ce monument que je vois ? Et les hommes de la ville lui dirent : C'est le sépulcre de l'homme d'Elohîm qui est venu de Yéhouda et qui a proclamé ces choses que tu as faites contre l’autel de Béth-El<!--1 R. 13:1-2.-->.
23:18	Et il dit : Laissez-le se reposer ! Que nul homme ne remue ses ossements ! Ils sauvèrent ses ossements ainsi que les ossements du prophète qui était venu de Samarie.
23:19	Yoshiyah fit aussi disparaître toutes les maisons des hauts lieux, qui étaient dans les villes de Samarie, et qu'avaient faites les rois d'Israël en provocation. Il fit à leur égard tout comme il avait fait à Béth-El.
23:20	Il tua sur les autels tous les prêtres des hauts lieux qui étaient là, et il y brûla des ossements humains. Puis il retourna à Yeroushalaim.

### Yoshiyah rétablit la fête de la Pâque<!--2 Ch. 35:1-19.-->

23:21	Le roi donna cet ordre à tout le peuple, en disant : Faites la Pâque pour YHWH, votre Elohîm, comme il est écrit dans le livre de cette alliance<!--Yéhoshoua ha Mashiah est notre Pâque. Voir Ex. 12 et 1 Co. 5:7.-->.
23:22	Car il ne s’était pas fait de Pâque comme celle-ci depuis les jours des Juges qui avaient jugé Israël, et pendant tous les jours des rois d'Israël et des rois de Yéhouda.
23:23	Mais la dix-huitième année du roi Yoshiyah on fit cette Pâque pour YHWH à Yeroushalaim.
23:24	Yoshiyah extermina aussi ceux qui évoquaient les morts et ceux qui avaient un esprit de divination, les théraphim, les idoles, et toutes les abominations qui se voyaient en terre de Yéhouda et à Yeroushalaim, afin de mettre en pratique les paroles de la torah écrites dans le livre que Chilqiyah, le prêtre, avait trouvé dans la maison de YHWH.

### Témoignage de Yoshiyah ; confirmation du jugement de YHWH

23:25	Avant Yoshiyah, il n'y eut pas de roi qui, comme lui, revienne à YHWH de tout son cœur, de toute son âme et de toute sa force, selon toute la torah de Moshé. Après lui, il n'en a pas paru de semblable.
23:26	Toutefois, YHWH ne revint pas de la grande colère de sa narine, dont sa narine brûlait contre Yéhouda, à cause de toutes les irritations dont Menashè l'avait irrité.
23:27	YHWH dit : Yéhouda aussi, je l'ôterai de mes faces, comme j'ai ôté Israël. Je rejetterai cette ville de Yeroushalaim que j'avais choisie, et la maison de laquelle j'avais dit : Là sera mon Nom<!--2 Ch. 7:16.-->.
23:28	Le reste des discours de Yoshiyah, tout ce qu'il a accompli, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?

### Mort de Yoshiyah ; Yehoachaz règne sur Yéhouda<!--2 Ch. 35:20-27, 36:1-2.-->

23:29	De son temps, pharaon Néco, roi d'Égypte, monta contre le roi d'Assyrie, vers le fleuve d'Euphrate. Le roi Yoshiyah marcha à sa rencontre, mais dès que pharaon le vit, il le tua à Meguiddo.
23:30	Ses serviteurs l'emportèrent mort sur un char, ils l'amenèrent de Meguiddo à Yeroushalaim et l'enterrèrent dans son sépulcre. Et le peuple de la terre prit Yehoachaz, fils de Yoshiyah, ils l'oignirent et l'établirent roi à la place de son père.

### Yehoachaz mis en prison par pharaon<!--2 Ch. 36:3.-->

23:31	Yehoachaz était fils de 23 ans quand il devint roi. Il régna 3 mois à Yeroushalaim. Le nom de sa mère était Hamoutal, fille de Yirmeyah, de Libnah.
23:32	Il fit ce qui est mal aux yeux de YHWH, selon tout ce qu'avaient fait ses pères.
23:33	Et pharaon Néco l'emprisonna à Riblah, en terre de Hamath, afin qu'il ne règne plus à Yeroushalaim et il imposa sur la terre un tribut de 100 talents d'argent et d'un talent d'or.

### Pharaon établit Yehoyaqiym roi de Yéhouda<!--2 Ch. 36:4-5.-->

23:34	Pharaon Néco établit roi Élyakim, fils de Yoshiyah, à la place de Yoshiyah, son père, et il changea son nom en celui de Yehoyaqiym. Il prit Yehoachaz, qui alla en Égypte, où il mourut.
23:35	Yehoyaqiym donna l'argent et l'or au pharaon, mais il taxa la terre pour donner cet argent selon l'ordre de pharaon. Il exerça une pression démesurée sur le peuple de la terre pour donner l'argent et l'or au pharaon Néco, chacun selon son estimation.
23:36	Yehoyaqiym était fils de 25 ans quand il devint roi. Il régna 11 ans à Yeroushalaim. Le nom de sa mère était Zeboudda, fille de Pedayah, de Rouma.
23:37	Il fit ce qui est mal aux yeux de YHWH, selon tout ce qu'avaient fait ses pères.

## Chapitre 24

### Asservissement de Yehoyaqiym au roi de Babel ; destruction de Yéhouda<!--2 Ch. 36:6-7.-->

24:1	En ses jours, Neboukadnetsar, roi de Babel, monta, et Yehoyaqiym lui fut asservi pendant 3 ans ; mais il se révolta de nouveau contre lui.
24:2	YHWH envoya contre Yehoyaqiym des troupes de Chaldéens, des armées de Syriens, des troupes de Moabites et des troupes des fils d'Ammon. Il les envoya contre Yéhouda pour le détruire, selon la parole de YHWH, dont il avait parlé par la main de ses serviteurs les prophètes.
24:3	C'est seulement par la bouche de YHWH que cela arriva contre Yéhouda, afin de l'ôter de ses faces, à cause de tous les péchés commis par Menashè,
24:4	et aussi du sang innocent qu'il avait répandu, ayant rempli Yeroushalaim du sang innocent. YHWH n'a pas voulu pardonner.

### Mort de Yehoyaqiym ; Yehoyakiyn règne sur Yéhouda<!--2 Ch. 36:8-9.-->

24:5	Le reste des discours de Yehoyaqiym et tout ce qu'il a accompli, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
24:6	Yehoyaqiym se coucha avec ses pères. Et Yehoyakiyn, son fils, régna à sa place.
24:7	Le roi d'Égypte ne sortit plus de sa terre, parce que le roi de Babel avait pris tout ce qui était au roi d'Égypte, depuis le torrent d'Égypte jusqu'au fleuve d'Euphrate.
24:8	Yehoyakiyn était fils de 18 ans quand il devint roi. Il régna 3 mois à Yeroushalaim. Le nom de sa mère était Nehoushta, fille d'Elnathan, de Yeroushalaim.
24:9	Il fit ce qui est mal aux yeux de YHWH, selon tout ce qu'avait fait son père.

### Yeroushalaim (Jérusalem) et son roi en captivité à Babel ; les pauvres restent<!--2 Ch. 36:10.-->

24:10	En ce temps-là, les serviteurs de Neboukadnetsar, roi de Babel, montèrent contre Yeroushalaim, et la ville fut assiégée.
24:11	Neboukadnetsar, roi de Babel, arriva devant la ville, pendant que ses serviteurs l'assiégeaient.
24:12	Yehoyakiyn, roi de Yéhouda, sortit vers le roi de Babel, lui, sa mère, ses serviteurs, ses chefs et ses eunuques. Le roi de Babel l'emmena captif la huitième année de son règne.
24:13	Il sortit de là tous les trésors de la maison de YHWH et les trésors de la maison royale ; il mit en pièces tous les ustensiles en or que Shelomoh, le roi d'Israël, avait faits pour le temple de YHWH, comme YHWH l'avait ordonné.
24:14	Il emmena en exil tout Yeroushalaim<!--Première déportation : 2 R. 24:1-4 et 2 Ch. 36:6-7. La première déportation eut lieu en 597 av. J.-C. pendant le règne de Yehoyaqiym (Jojakim), roi de Yéhouda (Juda). Les premiers exilés furent installés dans la région du fleuve Kebar (Ez. 1:1-3), un canal de 90 km de long reliant l'Euphrate au nord de Babel (Babylone) au même fleuve au sud d'Our en Chaldée. Yirmeyah (Jérémie) savait que leur séjour à l'étranger serait long. Il avait prophétisé qu'il durerait 70 ans (Jé. 25:1,11-12) et leur conseilla de se construire des maisons, de cultiver des jardins et de se multiplier (Jé. 29). Daniye'l et ses compagnons furent déportés à Babel lors de la première déportation (Da. 1). Daniye'l fut déporté environ 8 ans avant Yehezkel (Ézéchiel).-->, tous les chefs et tous les vaillants hommes de guerre, au nombre de 10 000 captifs, tous les artisans et les serruriers, de sorte qu'il ne resta plus que le peuple pauvre de la terre.
24:15	Il emmena en exil Yehoyakiyn à Babel, ainsi que la mère du roi, les femmes du roi, ses eunuques et tous les grands de la terre. Il les fit aller en captivité de Yeroushalaim à Babel.
24:16	Tous les guerriers au nombre de 7 000, les artisans et les serruriers au nombre de 1 000, tous les hommes vaillants, des faiseurs de guerre. Le roi de Babel les fit venir en exil à Babel.

### Neboukadnetsar établit Tsidqiyah (Sédécias) roi de Yéhouda<!--2 Ch. 36:10-12.-->

24:17	Et le roi de Babel établit roi, à la place de Yehoyakiyn, Mattanyah, son oncle, et il changea son nom en celui de Tsidqiyah.
24:18	Tsidqiyah était fils de 21 ans quand il devint roi. Il régna 11 ans à Yeroushalaim. Le nom de sa mère était Hamoutal, fille de Yirmeyah, de Libnah.
24:19	Il fit ce qui est mal aux yeux de YHWH, entièrement comme avait fait Yehoyaqiym.

### Tsidqiyah (Sédécias) se révolte<!--2 Ch. 36:13-16.-->

24:20	Car cela arriva à Yeroushalaim et à Yéhouda, à cause de la colère de YHWH, jusqu'à ce qu'il les rejette loin de ses faces. Et Tsidqiyah se révolta contre le roi de Babel.

## Chapitre 25

### Siège de Yeroushalaim (Jérusalem)<!--Jé. 39:1.-->

25:1	Il arriva dans la neuvième année du règne de Tsidqiyah, le dixième jour du dixième mois, que Neboukadnetsar<!--Yeroushalaim (Jérusalem) fut assiégée pendant deux ans. Lors de ce siège, des femmes juives faisaient cuire leurs enfants pour les consommer (La. 2:20, 4:10).-->, roi de Babel, vint avec toute son armée contre Yeroushalaim ; il campa devant elle et éleva des retranchements tout autour.
25:2	La ville fut assiégée jusqu'à la onzième année du roi Tsidqiyah.
25:3	Le neuvième jour du mois, la famine<!--La. 4:10.--> était forte dans la ville, et il n'y avait plus de pain pour le peuple de la terre.

### Tsidqiyah (Sédécias) lié et emmené à Babel<!--Jé. 39:2-7.-->

25:4	La brèche fut faite à la ville et tous les hommes de guerre s'enfuirent de nuit par le chemin de la porte entre les deux murailles près du jardin du roi, pendant que les Chaldéens environnaient la ville. Les fuyards et le roi prirent le chemin de la région aride.
25:5	Mais l'armée des Chaldéens poursuivit le roi et l'atteignit dans les régions arides de Yeriycho, et toute son armée se dispersa loin de lui.
25:6	Ils saisirent le roi et le firent monter vers le roi de Babel à Riblah. On prononça contre lui un jugement.
25:7	On tua les fils de Tsidqiyah en sa présence, puis on creva les yeux de Tsidqiyah, on le lia avec une double chaîne en cuivre et on le conduisit à Babel.

### Destruction de Yeroushalaim, du temple et des murailles<!--2 Ch. 36:17-21 ; Jé. 39:8-10.-->

25:8	Le septième jour du cinquième mois, c'était la dix-neuvième année du roi Neboukadnetsar, roi de Babel, Nebouzaradân, le grand bourreau, serviteur du roi de Babel<!--Troisième déportation : Le temple fut brûlé, la ville de Yeroushalaim fut totalement rasée et ses habitants furent déportés (De. 28:49-68). Contrairement à ce que l'on pense, il y a eu d'autres déportations. Voir Jé. 52.-->, entra dans Yeroushalaim.
25:9	Il brûla la maison de YHWH, la maison royale et toutes les maisons de Yeroushalaim : il brûla par le feu toutes les grandes maisons.
25:10	Toute l'armée des Chaldéens, qui était avec le grand bourreau, démolit les murailles qui entouraient Yeroushalaim.
25:11	Et le reste du peuple, ceux qui étaient restés dans la ville, les tombés, ceux qui étaient tombés vers le roi de Babel, le reste de la multitude, Nebouzaradân, le grand bourreau, les emmena en exil.
25:12	Le grand bourreau laissa quelques-uns des plus pauvres de la terre comme vignerons et comme laboureurs.
25:13	Les Chaldéens brisèrent les colonnes en cuivre qui étaient dans la maison de YHWH, les bases, la mer en cuivre qui était dans la maison de YHWH, et ils en emportèrent le cuivre à Babel.
25:14	Ils prirent aussi les cendriers, les pelles, les mouchettes, les coupes et tous les ustensiles en cuivre avec lesquels on faisait le service.
25:15	Le grand bourreau prit aussi les encensoirs et les cuvettes, ce qui était en or et ce qui était en argent.
25:16	Les deux colonnes, la mer et les bases que Shelomoh avait faites pour la maison de YHWH, tous ces ustensiles en cuivre avaient un poids inconnu.
25:17	La hauteur d'une colonne était de 18 coudées, et il y avait au-dessus un chapiteau en cuivre dont la hauteur était de 3 coudées ; autour du chapiteau il y avait un treillis et des grenades, le tout en cuivre. Il en était de même pour la seconde colonne avec le treillis.
25:18	Le grand bourreau prit Serayah, le prêtre en tête, Tsephanyah, le second prêtre et les trois gardiens du seuil.
25:19	De la ville il prit un eunuque qui était commissaire des hommes de guerre, 5 hommes parmi ceux qui voyaient les faces du roi et qui furent trouvés dans la ville, il prit aussi le scribe du chef de l'armée qui était chargé d'enrôler le peuple de la terre et 60 hommes du peuple de la terre qui se trouvaient dans la ville.
25:20	Nebouzaradân, le grand bourreau, les prit et les conduisit vers le roi de Babel à Riblah.
25:21	Le roi de Babel les frappa, et les fit mourir à Riblah, en terre de Hamath. Ainsi Yéhouda fut emmené en exil loin de son sol.

### Gedalyah nommé gouverneur de Yéhouda<!--Jé. 40:7-11.-->

25:22	Quant au peuple qui avait été laissé en terre de Yéhouda, celui qu’avait laissé Neboukadnetsar, roi de Babel, il établit sur eux Gedalyah, fils d'Achikam, fils de Shaphan.
25:23	Lorsque tous les chefs des troupes et leurs hommes eurent appris que le roi de Babel avait établi Gedalyah pour gouverneur, ils allèrent trouver Gedalyah à Mitspah, Yishmael, fils de Nethanyah, Yohanan, fils de Karéach, Serayah, fils de Tanhoumeth, de Netophah, Ya`azanyah, fils du Maakathien, eux et leurs hommes.
25:24	Gedalyah leur jura, à eux et à leurs hommes, et leur dit : N'ayez pas peur d'être serviteurs des Chaldéens. Demeurez sur la terre et servez le roi de Babel et vous serez heureux.

### Fuite du peuple en Égypte<!--Jé. 41:1-3, 43:4-7.-->

25:25	Mais il arriva au septième mois, que Yishmael, fils de Nethanyah, fils d'Éliyshama, de semence royale, vint avec 10 hommes et frappèrent Gedalyah. Ils le tuèrent, ainsi que les Juifs et les Chaldéens qui étaient avec lui à Mitspah.
25:26	Tout le peuple, depuis le plus petit jusqu'au plus grand, avec les chefs des troupes, se levèrent et s'en allèrent en Égypte, parce qu'ils avaient peur des Chaldéens.

### Yehoyakiyn à la table du roi de Babel<!--Jé. 52:31-34.-->

25:27	Or il arriva, la trente-septième année de la captivité de Yehoyakiyn, roi de Yéhouda, le vingt-septième jour du douzième mois, qu'Évil-Merodak, roi de Babel, dans l'année de son règne, éleva la tête de Yehoyakiyn, roi de Yéhouda et le fit sortir de la maison d'arrêt.
25:28	Il lui parla avec bonté et il mit son trône au-dessus du trône des rois qui étaient avec lui à Babel.
25:29	Il lui fit changer ses vêtements de prisonnier, et il<!--Yehoyakiyn.--> mangea du pain tout le temps de sa vie en sa présence.
25:30	Quant à sa ration, une ration journalière lui fut accordée selon la parole du roi, jour par jour, tous les jours de sa vie.
